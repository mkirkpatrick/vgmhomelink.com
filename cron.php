<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");
ini_set('max_execution_time', 1800);
set_time_limit(1800);
session_write_close();

use Forbin\Handlers\ErrorHandler;
use Forbin\Library\Classes\Member;
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

class process { 

    //------------------------------
	public function _initProcess() {
	//------------------------------
        $sql = "DELETE FROM `tbl_cron_cache_log`";
        $record = Database::ExecuteRaw($sql);
        $sql = "DELETE FROM `tbl_cron_status`"; 
        $record = Database::ExecuteRaw($sql);
        $sql = "SELECT m_id, m_provider_number 
                FROM `tbl_members` 
                WHERE m_provider_number IS NOT NULL AND m_status = 'Active'
                LIMIT 10000"; 
        $record = Database::Execute($sql);
        $pvids = ""; 
        if($record->Count()) { 
            while($record->MoveNext()) {
                $mid = (int) $record->m_id; 
                $providerNumber = strtoupper($record->m_provider_number);
                if(stripos($providerNumber, '|') !== false) {
                    $providerNumbers = explode('|', $providerNumber); 
                    foreach ($providerNumbers as $provNumber) {
                        if(stripos($pvids, $provNumber) === false && !isNullOrEmpty($provNumber)) {
                            Database::ExecuteRaw("INSERT INTO `tbl_cron_status` (`cs_provider_no`, `m_id`) VALUES ('$provNumber', '$mid');"); 
                            $pvids .= $provNumber . '|'; 
                        }
                    }
                } else {
                            Database::ExecuteRaw("INSERT INTO `tbl_cron_status` (`cs_provider_no`, `m_id`) VALUES ('$providerNumber', '$mid');"); 
                            $pvids .= $providerNumber . '|'; 
                }
           }
        }
        
        die("Complete"); 
    }
    
	//------------------------------
	public function _recentReferrals() {
	//------------------------------
        $sdate = isset($_GET['sdate']) ? date('Y-m-d', strtotime($_GET['sdate'])) : date('Y-m-d', strtotime('700 days')); 
        $edate = isset($_GET['edate']) ? date('Y-m-d', strtotime($_GET['edate'])) : date('Y-m-d'); 
        $time_pre = microtime(true);
        $sql = "SELECT `cs_provider_no`,`m_id`
                FROM `tbl_cron_status` 
                WHERE cs_recent = 0
                ORDER BY cs_provider_no
                LIMIT 1;"; 
        $record = Database::Execute($sql);
        if($record->Count()) {
            $success = '0';
            try {
                $record->MoveNext();
                $mid = $record->m_id; 
                $providerNumber = strtoupper($record->cs_provider_no);
                $cacheKey = $providerNumber . '_WEB_ORDERS_RECENT';
                $dataArray = array(); 
                $orders = ViperAPI::getProviderOrdersByVGMNo($providerNumber, $sdate, $edate);
                if(isset($orders->ReferralNo)) {
                    $orderData['ReferralNumber'] = $orders->ReferralNo; 
                    $orderData['PatientName'] = ucwords(strtolower($orders->PatientName)); 
                    $orderData['SSNNumber'] = !isNullOrEmpty($orders->PatientDOB) ? date('m/d/Y', strtotime($orders->PatientDOB)) : '';
                    $orderData['ServiceDate'] = !isNullOrEmpty($orders->ServiceDate) ? date('m/d/Y', strtotime($orders->ServiceDate)) : ''; 
                    $orderData['EFTCheck'] = $orders->EFTCheckNo; 
                    $orderData['Status'] = $orders->OrderStatus; 
                    $orderData['InvoiceNumber'] = $orders->InvoiceNo; 
                    if($orderData['Status'] <> "Complete") {
                        array_push($dataArray, $orderData);
                    }
                } else {
                    foreach ($orders as $order) {
                        $orderData['ReferralNumber'] = $order->ReferralNo; 
                        $orderData['PatientName'] = ucwords(strtolower($order->PatientName)); 
                        $orderData['SSNNumber'] = !isNullOrEmpty($order->PatientDOB) ? date('m/d/Y', strtotime($order->PatientDOB)) : '';
                        $orderData['ServiceDate'] = !isNullOrEmpty($order->ServiceDate) ? date('m/d/Y', strtotime($order->ServiceDate)) : ''; 
                        $orderData['EFTCheck'] = $order->EFTCheckNo; 
                        $orderData['Status'] = $order->OrderStatus; 
                        $orderData['InvoiceNumber'] = $order->InvoiceNo; 
                        if($orderData['Status'] <> "Complete") {
                            array_push($dataArray, $orderData); 
                        }
                    }
                    $json = json_encode($dataArray); 
                    Cache::set($cacheKey, $json, 500000);
                }
                Database::ExecuteRaw("UPDATE `tbl_cron_status` SET `cs_recent` = '1' WHERE `cs_provider_no` = '$providerNumber';"); 
                $success = '1'; 
            } catch (Exception $e) {
                ErrorHandler::log_exception($e); 
                $success = '0'; 
            } finally {
                $time_post = microtime(true);
                $exec_time = $time_post - $time_pre;
                Database::ExecuteRaw("INSERT INTO `tbl_cron_cache_log` (`pl_provider`,`pl_exec_time`, `pl_cache_key`, `pl_success`, `pl_action`, `m_id`) VALUES ('$providerNumber','$exec_time', '$cacheKey', $success, 'RECENT', '$mid');"); 
                header("Refresh:0");
                exit; 
            }
        } else {
            die("Completed");
        }
    }
    
    //------------------------------
	public function _referralHistory() {
	//------------------------------
        $sdate = isset($_GET['sdate']) ? date('Y-m-d', strtotime($_GET['sdate'])) : date('Y-m-d', strtotime('-3000 days')); 
        $edate = isset($_GET['edate']) ? date('Y-m-d', strtotime($_GET['edate'])) : date('Y-m-d'); 
        $time_pre = microtime(true);
        $sql = "SELECT `cs_provider_no`,`m_id`
                FROM `tbl_cron_status` 
                WHERE cs_history = 0
                ORDER BY cs_provider_no
                LIMIT 1;"; 
        $record = Database::Execute($sql);
        if($record->Count()) {
            $success = '0';
            try {
                $record->MoveNext();
                $mid = $record->m_id; 
                $providerNumber = strtoupper($record->cs_provider_no);
                $cacheKey = $providerNumber . '_WEB_ORDERS_HISTORY';
                $dataArray = array(); 
                $orders = ViperAPI::getProviderOrdersByVGMNo($providerNumber, $sdate, $edate);
                if(isset($orders->ReferralNo)) {
                    $orderData['ReferralNumber'] = $orders->ReferralNo; 
                    $orderData['PatientName'] = ucwords(strtolower($orders->PatientName)); 
                    $orderData['SSNNumber'] = !isNullOrEmpty($orders->PatientDOB) ? date('m/d/Y', strtotime($orders->PatientDOB)) : '';
                    $orderData['ServiceDate'] = !isNullOrEmpty($orders->ServiceDate) ? date('m/d/Y', strtotime($orders->ServiceDate)) : ''; 
                    $orderData['EFTCheck'] = $orders->EFTCheckNo; 
                    $orderData['Status'] = $orders->OrderStatus; 
                    $orderData['InvoiceNumber'] = $orders->InvoiceNo; 
                    if($orderData['Status'] == "Complete") {
                        array_push($dataArray, $orderData);
                    }
                } else {
                    foreach ($orders as $order) {
                        $orderData['ReferralNumber'] = $order->ReferralNo; 
                        $orderData['PatientName'] = ucwords(strtolower($order->PatientName)); 
                        $orderData['SSNNumber'] = !isNullOrEmpty($order->PatientDOB) ? date('m/d/Y', strtotime($order->PatientDOB)) : '';
                        $orderData['ServiceDate'] = !isNullOrEmpty($order->ServiceDate) ? date('m/d/Y', strtotime($order->ServiceDate)) : ''; 
                        $orderData['EFTCheck'] = $order->EFTCheckNo; 
                        $orderData['Status'] = $order->OrderStatus; 
                        $orderData['InvoiceNumber'] = $order->InvoiceNo; 
                        if($orderData['Status'] == "Complete") {
                            array_push($dataArray, $orderData); 
                        }
                    }
                    $json = json_encode($dataArray); 
                    Cache::set($cacheKey, $json, 500000);
                }
                Database::ExecuteRaw("UPDATE `tbl_cron_status` SET `cs_history` = '1' WHERE `cs_provider_no` = '$providerNumber';"); 
                $success = '1'; 
            } catch (Exception $e) {
                ErrorHandler::log_exception($e); 
                $success = '0'; 
            } finally {
                $time_post = microtime(true);
                $exec_time = $time_post - $time_pre;
                Database::ExecuteRaw("INSERT INTO `tbl_cron_cache_log` (`pl_provider`,`pl_exec_time`, `pl_cache_key`, `pl_success`, `pl_action`, `m_id`) VALUES ('$providerNumber','$exec_time', '$cacheKey', $success, 'REFERRAL', '$mid');"); 
                header("Refresh:0");
                exit; 
            }
        } else {
            die("Completed");
        }
    }
    
    //------------------------------
	public function _statistics() {
	//------------------------------
        $time_pre = microtime(true);
        $sql = "SELECT `cs_provider_no`,`m_id`
                FROM `tbl_cron_status` 
                WHERE cs_statistics = 0
                ORDER BY cs_provider_no
                LIMIT 1;"; 
        $record = Database::Execute($sql);
        if($record->Count()) { 
            $success = '0';              
            try {
                $record->MoveNext();
                $providerNumber = strtoupper($record->cs_provider_no);
                $mid = (int) $record->m_id; 
                $cacheKey = $providerNumber . '_STATISTICS'; 
                $statistics[$cacheKey . '_DENIEDREFERRALS'] = ViperAPI::getProviderYTDReferralsDenied($providerNumber); 
                $statistics[$cacheKey . '_ACCEPTEDREFERRALS'] = ViperAPI::getProviderYTDReferralsAccepted($providerNumber);
                $statistics[$cacheKey . '_YTDPAID'] = ViperAPI::getProviderYTDPaid($providerNumber);
                $statistics[$cacheKey . '_PENDINGPAYMENTS'] = ViperAPI::getProviderPendingPayments($providerNumber); 
                $statistics[$cacheKey . '_YTDREFERRALS'] = ViperAPI::getProviderYTDDirectReferrals($providerNumber);
                $statistics[$cacheKey . '_SENTREFERRALS'] = $statistics[$cacheKey . '_DENIEDREFERRALS'] + $statistics[$cacheKey . '_ACCEPTEDREFERRALS']; 
                Cache::setMultiple($statistics, 500000);
                Database::ExecuteRaw("UPDATE `tbl_cron_status` SET `cs_statistics` = '1' WHERE `cs_provider_no` = '$providerNumber';"); 
                $success = '1'; 
            } catch (Exception $e) {
                ErrorHandler::log_exception($e); 
                $success = '0'; 
            } finally {
                $time_post = microtime(true);
                $exec_time = $time_post - $time_pre;
                Database::ExecuteRaw("INSERT INTO `tbl_cron_cache_log` (`pl_provider`,`pl_exec_time`, `pl_cache_key`, `pl_success`, `pl_action`, `m_id`) VALUES ('$providerNumber','$exec_time', '$cacheKey', $success, 'STATISTICS', '$mid');"); 
            }
            header("Refresh:0");
            exit; 
        } else {
             die("Completed"); 
        }
    }
};

$p = new process();
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';
 
switch ($action) {
	case 'recentReferrals':
		$p->_recentReferrals();
		break;
        
	case 'initProcess':
		$p->_initProcess();
		break;
        
	case 'referralHistory':
		$p->_referralHistory();
		break;
        
    case 'statistics': 
        $p->_statistics(); 

    default:
        throw new Exception("An unknown action executed!");
}
die("System Error!");
?>