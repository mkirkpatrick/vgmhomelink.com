<?php 
header("HTTP/1.0 403 Forbidden");
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");
$iNum = 4; 
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/page_core.php"); 
include_once('includes/inc-pgtop.php'); 
include_once('includes/inc-header.php');
include_once('includes/inc-nav.php');
$GLOBALS["pageHeader"] = getPageTitle($iNum); ?>

<main class="content subcontent">
    <div class="container clearfix">
        <?php $layout->publish(); ?>
    </div><!-- eof container -->
</main><!-- eof content -->

<?php include_once('includes/inc-copyright.php'); ?>  
<?php include_once('includes/inc-pgbot.php'); ?>