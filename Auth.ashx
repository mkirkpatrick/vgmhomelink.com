﻿<%@ WebHandler Language="C#" Class="Forbin.VGMHomelink.Authentication.AuthHandler" %>

using System;
using System.Web;
using Forbin.VGMHomelink.Homelink.BusinessLogic;
using Forbin.VGMHomelink.Core.Helpers;

namespace Forbin.VGMHomelink.Authentication
{
    public class AuthHandler : IHttpHandler
    {
        /* 
            objClaimStatusCLManager._DealerNumber = "v6820";
            objClaimStatusCLManager._UserName = "birch193@aol.com";
            objClaimStatusCLManager._Password = _MembersHelper.CalculateMD5Hash("MSK@4210s");        
        */
        public void ProcessRequest(HttpContext context)

        {
            string dealerNo = HttpContext.Current.Request.QueryString["dealerno"];
            string username = HttpContext.Current.Request.QueryString["username"];
            string password = HttpContext.Current.Request.QueryString["password"];
            string fileName = "";

            MembersHelper _MembersHelper = new MembersHelper(); 
            DealerClaimCLManager objClaimStatusCLManager = new DealerClaimCLManager();
            DealerClaimCL objClaimStatusCL = new DealerClaimCL();
            objClaimStatusCLManager._DealerNumber = dealerNo; 
            objClaimStatusCLManager._UserName = username; 
            objClaimStatusCLManager._Password = _MembersHelper.CalculateMD5Hash(password); 
            int intReturn = objClaimStatusCL.fncValidateUser(objClaimStatusCLManager);
            HttpContext.Current.Response.Write(intReturn);
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}