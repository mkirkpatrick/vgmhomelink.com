<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'] . '/EmailReferral';
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/config.php');
$heading = ""; 
$message = ""; 
$accessCode = "1Q243GH09FGQZ7"; 
$userId = "Forbin"; 
$q = isset($_GET['q']) ? $_GET['q'] : '' ; 
$requestId = isset($_GET['webrequestid']) ? $_GET['webrequestid'] : ''; 
$tdReasonCode = isset($_GET['tdrc']) ? $_GET['tdrc'] : 0;
$dbRequestId = Database::quote_smart($requestId); 
$uri = Database::quote_smart($_SERVER['REQUEST_URI']); 
$userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? Database::quote_smart($_SERVER['HTTP_USER_AGENT']) : 'Unknown';
$ip = isset($_SERVER['HTTP_CLIENT_IP']) ? Database::quote_smart($_SERVER['HTTP_CLIENT_IP']) : 'N/A'; 
$turnCodeArray = array(); 

try {
	if($q == "accept" && !isNullOrEmpty($q)) { 
		$heading = "Confirm Accept"; 
		$message = "Please click confirm below to accept. ";
	} elseif($q == "accepted" && !isNullOrEmpty($q) && !isNullOrEmpty($tdReasonCode)) {
		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/Orders.svc?wsdl");
		$request = array(array()); 
		$request['request']['AccessCode'] = $accessCode; 
		$request['request']['WebRequestId'] = $requestId;
		$request['request']['UserId'] = $userId; 
		$response = $client->AcceptOrderByWebRequestId($request);
		$result = $response->AcceptOrderByWebRequestIdResult; 
		$heading = "Referral Response Confirmed"; 
		//$message = "Referral ACCEPTED! Thank you for your participation."; 
		$message = Database::quote_smart($result->AcceptResponse); 
		$jsonResponse = Database::quote_smart(json_encode($result)); 
		$sql = "INSERT INTO `tbl_request_log` (`rl_status`,`rl_message`,`rl_response`, `rl_user_agent`, `rl_ip`, `rl_uri`, `rl_request_id`) VALUES ('$heading','$message','$jsonResponse', '$userAgent', '$ip', '$uri', '$dbRequestId');";  
		Database::ExecuteRaw($sql); 
	} elseif($q == "decline" && !isNullOrEmpty($q)) { 
        $client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/Orders.svc?wsdl");
        $request = array(array()); 
		$request['request']['AccessCode'] = $accessCode; 
		$request['request']['WebRequestId'] = $requestId;
		$request['request']['UserId'] = $userId; 
		$heading = "Decline"; 
		$message = "Please select the decline reason: "; 
		$response = $client->GetDealerTurnDownReasons($request); 
		$turnCodeArray = $response->GetDealerTurnDownReasonsResult->TurnDownReasons; 
	} elseif($q == "declined" && !isNullOrEmpty($q) && !isNullOrEmpty($tdReasonCode)) { 
		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/Orders.svc?wsdl");
		$request = array(array()); 
		$request['request']['AccessCode'] = $accessCode; 
		$request['request']['WebRequestId'] = $requestId;
		$request['request']['UserId'] = $userId; 
		if($tdReasonCode > 0) { 
			$request['request']['TurnDownReasonCode'] = $tdReasonCode; 
		}
		$response = $client->DeclineOrderByWebRequestId($request);
		$result = $response->DeclineOrderByWebRequestIdResult; 
		$jsonResponse = json_encode($result); 
		if($result->OperationStatus == "Success") { 
			$heading = "Declined"; 
			$message = $result->DeclineResponse; 
			$jsonResponse = Database::quote_smart(json_encode($result)); 
			$sql = "INSERT INTO `tbl_request_log` (`rl_status`,`rl_message`,`rl_response`, `rl_user_agent`, `rl_ip`, `rl_uri`, `rl_request_id`, `rl_tdrc`) VALUES ('$heading','$message','$jsonResponse', '$userAgent', '$ip', '$uri', '$dbRequestId', '$tdReasonCode');"; 
			Database::ExecuteRaw($sql); 
		} else { 
			throw new Exception($jsonResponse); 
		}	
	} else {
		$heading = "Invalid Url"; 
		$message = "Your url is invalid. Please try the link again. You may need to directly copy and paste the link from your email. "; 
		$sql = "INSERT INTO `tbl_request_log` (`rl_status`,`rl_message`,`rl_response`, `rl_user_agent`, `rl_ip`, `rl_uri`, `rl_request_id`) VALUES ('$heading','$message',NULL, '$userAgent', '$ip', '$uri', '$dbRequestId');";
		Database::ExecuteRaw($sql); 		
	}
} catch (SoapFault $fault) {
	$message = "Unexpected error while processing. Please try again at a later time. "; 
	$response = Database::quote_smart($fault); 
	$sql = "INSERT INTO `tbl_request_log` (`rl_status`,`rl_message`,`rl_response`, `rl_user_agent`, `rl_ip`, `rl_uri`, `rl_request_id`) VALUES ('Soap Fault','$message','$response', '$userAgent', '$ip', '$uri', '$dbRequestId');"; 
	Database::ExecuteRaw($sql); 
} catch (Exception $e) {
	$message = "Unexpected error while processing. Please try again at a later time. "; 
	$response = Database::quote_smart($e); 
	$sql = "INSERT INTO `tbl_request_log` (`rl_status`,`rl_message`,`rl_response`, `rl_user_agent`, `rl_ip`, `rl_uri`, `rl_request_id`) VALUES ('Exception','$message','$response', '$userAgent', '$ip', '$uri', '$dbRequestId');"; 
	Database::ExecuteRaw($sql); 
} 
?>
<!DOCTYPE html>
<!--[if IE 8]><html xmlns="http://www.w3.org/1999/xhtml" class="no-js ie8" lang="en"><![endif]-->
<!--[if IE 9]><html xmlns="http://www.w3.org/1999/xhtml" class="no-js ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html xmlns="http://www.w3.org/1999/xhtml" class="no-js" lang="en"><!--<![endif]-->
<head>
<title>New Referral from HOMELINK</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Viewport" content="width=device-width, minimum-scale=1, initial-scale=1" />
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/images/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/images/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/images/manifest.json">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="msapplication-config" content="/images/browserconfig.xml">
<meta name="theme-color" content="#e75300">
<link rel="stylesheet" href="styles.css" />
<!--[if lte IE 8]>
<script src="//cdn.forbin.com/global/jquery/html5shiv.js"></script>
<![endif]-->
<script>dataLayer = [];</script>

<style>
	
</style>
</head>
<body class="index">

	<header class="masthead">
		<div class="container clearfix relative">
			<a href="http://www.vgmhomelink.com" class="logo absolute"><img id="_x0000_i1025" src="https://www.vgmhomelink.com/images/logo-square.png" alt="Link to VGMHomelink.com" border="0" /></a>
		</div>
	</header>

	<main class="content subcontent">
		<div class="container clearfix"><br />
			<h1>HomeLink Email Referral</h1><br />
			<h2><?= $message ?></h2>
			<?php if($q == "decline") { ?>
			<div id="TurnDownReasonDiv" style="width: 360px; margin-left: auto; margin-right: auto"><br />
			<form method="get">
			<input type="hidden" name="q" value="declined" />
			<input type="hidden" name="webrequestid" value="<?= $requestId ?>" />
			<select name="tdrc" id="cboTDRC" style="display: inline-block; width: 220px;">
				<option value="">---------- Select -----------</option>
				<?php 
					$html = ""; 
					foreach($turnCodeArray->TurnDownReason as $turnDown) {
						$text = $turnDown->ReasonDescription;
						$value = $turnDown->ReasonCode; 
						$selected = ""; 
						$html .= sprintf("<option value=\"%s\" %s>%s</option>\n", $value, $selected, $text);
					}
					print $html; 
			 ?>
			</select>
			<input type="submit" value="Decline Referral" style="display: inline-block; height: 28px" />
			</form>
			</div>
			<?php } ?>
			<?php if($q == "accept") { ?>
			<div id="TurnDownReasonDiv" style="width: 340px; margin-left: auto; margin-right: auto"><br />
			<div align="center">
			<form method="get">
			<input type="hidden" name="q" value="accepted" />
			<input type="hidden" name="webrequestid" value="<?= $requestId ?>" />
			<input type="hidden" name="tdrc" id="cboTDRC" value="confirm" />
			<input type="submit" value="Confirm" style="display: inline-block; height: 28px" />
			</form>
			</div>
			</div>
			<?php } ?>
		</div>
	</main>
</body>
</html>
