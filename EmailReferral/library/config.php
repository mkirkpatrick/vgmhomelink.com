<?php
$_WEBCONFIG = array(); 

/* Load Site Config */
$configPath = './'; 
while(is_readable($configPath)) { 
	if (file_exists($configPath . 'web.config')) {
		$configXML = simplexml_load_file($configPath . 'web.config');
		if(isset($configXML->{'system.web'})) {
			$_WEBCONFIG['CUSTOMERRORS']['MODE'] = $configXML->{'system.web'}->customErrors->attributes()->mode; 
			$_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT'] = ltrim($configXML->{'system.web'}->customErrors->attributes()->defaultRedirect, "~");  
		}
		if(isset($configXML->appSettings))
		{
			foreach ($configXML->appSettings->add as $setting) {
				if(!isset($_WEBCONFIG[(string)$setting->attributes()->key])) { 
					$_WEBCONFIG[(string)$setting->attributes()->key] = (string)$setting->attributes()->value; 
				}
			}
		}
		if(isset($configXML->connectionStrings))
		{
			foreach ($configXML->connectionStrings->add as $connectionString) {
				if(!isset($_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name])) { 
					$_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name] = array(); 
					$connData = explode(";", (string)$connectionString->attributes()->connectionString); 
					$_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['HOST'] = str_replace("=", "", stristr($connData[0], '='));  
					$_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['DATABASE'] = str_replace("=", "", stristr($connData[1], '='));
					$_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['USERNAME'] = str_replace("=", "", stristr($connData[2], '='));
					$_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['PASSWORD'] = str_replace("=", "", stristr($connData[3], '='));
				}
			}
		}		
	}
	$configPath = '../'. $configPath; 
}

if(count($_WEBCONFIG) == 0) { 
		die("web.config Configuration not setup properly"); 
}

/* Global Settings */ 
ini_set("log_errors", 1);
ini_set("session.cookie_httponly", 1);
ini_set('session.gc_maxlifetime', 30*60);
date_default_timezone_set('US/Central');
ini_set('memory_limit', '256M');
if(isset($_WEBCONFIG['SITE_HAS_SSL']) && $_WEBCONFIG['SITE_HAS_SSL'] == "true") {
	ini_set("session.cookie_secure", 1);
	header('Strict-Transport-Security: max-age=15552000');
}
header('X-Content-Type-Options: nosniff');
header('X-Frame-Options: sameorigin');
header('P3P: CP="Our site does not have a P3P Policy, please see our privacy policy for more information."');
header('X-UA-Compatible: IE=Edge,chrome=1');
header_remove('X-Powered-By');

/* Start Session */
if(!headers_sent() && !isset($_SESSION)) {
	session_start(); 
}

require_once $_SERVER['DOCUMENT_ROOT'] . "/library/functions.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/constants.php";
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "database/class_database.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "database/class_datatable.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "database/class_entity.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "database/class_repository.php");
?>