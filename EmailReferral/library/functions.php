<?
////////////////////////////     PHP EXTENTION FUNCTIONS     \\\\\\\\\\\\\\\\\\\\\\\\\\\

function redirect($link, $httpResponse = "302 Found", $endResponse = true) { 
	if(headers_sent()) {
		echo "<script type='text/javascript'>location.href='$link';</script>";
		echo '<meta http-equiv="refresh" content="10;url='. $link .'" />'; 
	} else {
		header($_SERVER['SERVER_PROTOCOL'] . $httpResponse, true);
		header("Location: $link");
	}
	if($endResponse) exit; 
}
function serverTransfer($path) { 
	global $siteConfig, $_WEBCONFIG;  
	if (strpos($path, '?') !== false) {
			$parts = explode("?", $path); 
			$path = $parts[0]; 
			parse_str($parts[1], $queryStrings);
			$_GET = array_merge($queryStrings, $_GET); 
			$_REQUEST = array_merge($queryStrings, $_REQUEST); 
	}
	$path = $_SERVER['DOCUMENT_ROOT'] . $path; 
	if(stream_resolve_include_path($path) !== false) {
		if (ob_get_length() > 0) { ob_end_clean(); }
		
		require_once($path); 
		exit; 
	} else {
		throw new Exception("Path: " . $path . " does not exist"); 
	}
}
function innerstr($start, $end, $haystack) { 
	$start_pos = strpos($haystack,$start);
	if ($start_pos === FALSE)
	{
		throw new exception("Starting limiter ".$start." not found in ".$haystack);
	}
	$end_pos = strpos($haystack,$end,$start_pos);
	if ($end_pos === FALSE)
	{
		throw new exception("Ending limiter ".$end." not found in ".$haystack);
	}

	return substr($haystack, $start_pos+strlen($start), ($end_pos - strlen($start) - 1)-$start_pos);
}
function str_remove($remove, $haystack) { 
	return str_replace($remove, "", $haystack); 
}
function str_truncate($string, $limit, $break=" ", $pad="...") {
	if(strlen($string) <= $limit) return $string; 

	if(false !== ($breakpoint = strpos($string, $break, $limit))) { 
		if($breakpoint < strlen($string) - 1) { 
			$string = substr($string, 0, $breakpoint) . $pad; 
		}
	} 
	return $string; 
}
function str_replace_inner($needle_start, $needle_end, $replacement, $str) {
    $pos = strpos($str, $needle_start);
    $start = $pos === false ? 0 : $pos + strlen($needle_start);

    $pos = strpos($str, $needle_end, $start);
    $end = $pos === false ? strlen($str) : $pos;

    return substr_replace($str, $replacement, $start, $end - $start);
}
function startsWith($haystack, $needle)
{
	return !strncmp($haystack, $needle, strlen($needle));
}
function endsWith($haystack, $needle)
{
	$length = strlen($needle);
	if ($length == 0) {
		return true;
	}

	return (substr($haystack, -$length) === $needle);
}
function isNullOrEmpty($string) { 
	return (!isset($string) || strlen(trim($string)) == 0);  
}
function xss_sanitize($string, $removeTags = true) { 
	if($removeTags) {
		$string = strip_tags($string); 
	}
	$string = str_remove("javascript:", $string); 
	return htmlEntities($string, ENT_QUOTES, "utf-8"); 
}
function breakPoint($variable, $stopProcessing = true) {
	if($stopProcessing) { ob_end_clean(); }
	print "<pre>"; 
	print_r($variable); 
	print "</pre>"; 
	if($stopProcessing) { exit; }
}
function isPost() { 
	return count($_POST) > 0; 
}
function app_info() {
	global $_WEBCONFIG; 
	ob_end_clean(); 
	print_r($_REQUEST);
	print_r($_SESSION); 
	print_r($_WEBCONFIG); 
	print_r($_SERVER);  
	exit;
}
function create_guid() {
    if (function_exists('com_create_guid') === true) {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

function get_url_status($url, $allowRedirect = false, $timeout = 100) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERAGENT, "VGM Forbin Website");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if($allowRedirect) {
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		}
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

		$response       = curl_exec($ch);
		$errno          = curl_errno($ch);
		$error          = curl_error($ch);

		$info = curl_getinfo($ch);
		curl_close($ch);
		return $info['http_code'];
}

function curl_get_contents($url, $checkSSL = false, $timeout = 100) {
	// Initiate the curl session
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $checkSSL);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $checkSSL);
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, false);
	$output = curl_exec($ch);
	curl_close($ch);
	return $output;
}

////////////////////////////     FILE / IO FUNCTIONS     \\\\\\\\\\\\\\\\\\\\\\\\\\\
	
function filter_file_name( $filename ) {
	return preg_replace("[^\w\s\d\.\-_~,;:\[\]\(\]]", '', $filename); 
}
function display_filesize($filesize){
   
    if(is_numeric($filesize)){
    $decr = 1024; $step = 0;
    $prefix = array('Byte','KB','MB','GB','TB','PB');
       
    while(($filesize / $decr) > 0.9){
        $filesize = $filesize / $decr;
        $step++;
    }
    return round($filesize,2).' '.$prefix[$step];
    } else {

    return 'NaN';
    }
   
}
function filePathCombine() {
    $path = '';
    $arguments = func_get_args();
    $args = array();
    foreach($arguments as $a) if($a !== '') $args[] = $a;//Removes the empty elements
    
    $arg_count = count($args);
    for($i=0; $i<$arg_count; $i++) {
		$folder = trim($args[$i], URL_DIRECTORY_SEPERATOR); 
        if($i != 0 && $folder[0] == DIRECTORY_SEPARATOR) $folder = substr($folder,1); 
        if(substr($folder,-1) == DIRECTORY_SEPARATOR) $folder = substr($folder,0,-1);
        $path .= $folder;
        if($i < ($arg_count-1) || strpos($folder, '.') === false) $path .= DIRECTORY_SEPARATOR; 
    }
    return str_replace(URL_DIRECTORY_SEPERATOR, DIRECTORY_SEPARATOR, $path);
}
function urlPathCombine() {
    $path = '';
    $arguments = func_get_args();
    $args = array();
    foreach($arguments as $a) if($a !== '') $args[] = $a;
    
    $arg_count = count($args);
    for($i=0; $i<$arg_count; $i++) {
		$folder = trim($args[$i], DIRECTORY_SEPARATOR); 
        if($i != 0 && $folder[0] == URL_DIRECTORY_SEPERATOR) $folder = substr($folder,1); 
        if(substr($folder,-1) == URL_DIRECTORY_SEPERATOR) $folder = substr($folder,0,-1); 
        $path .= $folder;
        if($i < ($arg_count-1) || strpos($folder, '.') === false) $path .= URL_DIRECTORY_SEPERATOR;
    }
    return str_replace(DIRECTORY_SEPARATOR, URL_DIRECTORY_SEPERATOR, $path);
}
function saveUpload($path, $uploaderName) {
	if(!move_uploaded_file($_FILES[$uploaderName]['tmp_name'], $path)) {
		throw new exception("There was an error uploading the file, please try again!");
	}
}
function deleteFolder($dir) {
	if (!file_exists($dir)) return true;
	if (!is_dir($dir)) return unlink($dir);
	foreach (scandir($dir) as $item) {
		if ($item == '.' || $item == '..') continue;
		if (!$this->deleteFolder($dir.'/'.$item)) return false;
	}
	return rmdir($dir);
}
function getExtName($file) {
	if(strpos(basename($file), ".") === false)
		return '';

	$file = explode(".",basename($file));
	return $file[count($file)-1];
}
function makeDir($path, $mode = 0777) {
	$old = umask(0);
	$res = @mkdir($path, $mode, true);
	umask($old);
	return $res;
}
function getFiles($str) {
	return glob($str, GLOB_BRACE);
}
function DeleteImage($Params) {
	$files = getFiles($Params);

	foreach ($files as $file) {
		$deleted = @unlink($file);
	}
}//end function
function UploadImage($dirFolder, $fieldName, $Id, $Width, $Height, $Crop, $Prefix='') {
	$res	= '';
	$name	= md5(rand() * time());
	$fieldValue	= $_FILES[$fieldName]['name'];
	$ext	= strtolower(GetExtName($fieldValue));
	if ($Prefix) {
		$orig	= $dirFolder . "/orig_{$Id}{$Prefix}_{$name}.{$ext}"; 
		$dest	= $dirFolder . "/{$Id}{$Prefix}_{$name}.{$ext}"; 
	} else {
		$orig	= $dirFolder . "/orig_{$Id}_{$name}.{$ext}"; 
		$dest	= $dirFolder . "/{$Id}_{$name}.{$ext}"; 
	}

	if (!is_dir($dirFolder)) {
		$results = makeDir($dirFolder);
	} else {
		$results = TRUE;
	}

	if ($results) {
		if (move_uploaded_file($_FILES[$fieldName]['tmp_name'], $orig)) {
			$res = imageResize($orig, $dest, $Width, $Height, $Crop);
		} else {
			$res = "Error uploading $fieldValue.";
		}
	} else {
		$res = "Failed to create folder $dirFolder.";
	}

	return $res;
}//end function
function DeleteFile($Params) {
	$files = getFiles($Params);

	foreach ($files as $file) {
		$deleted = @unlink($file);
	}
}//end function
function UploadFile($dirFolder, $fieldName, $Id, $Prefix='') {
	$res	= '';
	$name	= md5(rand() * time());
	$fieldValue	= $_FILES[$fieldName]['name'];
	$ext	= strtolower(GetExtName($fieldValue));
	if ($Prefix) {
		$dest	= $dirFolder . "/{$Id}{$Prefix}_{$name}.{$ext}"; 
	} else {
		$dest	= $dirFolder . "/{$Id}_{$name}.{$ext}"; 
	}

	if (!is_dir($dirFolder)) {
		$results = makeDir($dirFolder);
	} else {
		$results = TRUE;
	}

	if ($results) {
		if (!move_uploaded_file($_FILES[$fieldName]['tmp_name'], $dest)) {
			$res = "Error uploading $fieldValue.";
		}
	} else {
		$res = "Failed to create folder $dirFolder.";
	}

	return $res;
}//end function
function imageResize($src, $dst, $width, $height, $crop=0) {

  if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";

  $type = strtolower(substr(strrchr($src,"."),1));
  if($type == 'jpeg') $type = 'jpg';
  switch($type){
    case 'bmp': $img = imagecreatefromwbmp($src); break;
    case 'gif': $img = imagecreatefromgif($src); break;
    case 'jpg': $img = imagecreatefromjpeg($src); break;
    case 'png': $img = imagecreatefrompng($src); break;
    default : return "Unsupported picture type!";
  }

  // resize
  if($crop){
    if($w < $width or $h < $height) return "Picture is too small!";
    $ratio = max($width/$w, $height/$h);
    $h = $height / $ratio;
    $x = ($w - $width / $ratio) / 2;
    $w = $width / $ratio;
  }
  else{
    if($w < $width and $h < $height) return "Picture is too small!";
    $ratio = min($width/$w, $height/$h);
    $width = $w * $ratio;
    $height = $h * $ratio;
    $x = 0;
  }

  $new = imagecreatetruecolor($width, $height);

  // preserve transparency
  if($type == "gif" or $type == "png"){
    imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
    imagealphablending($new, false);
    imagesavealpha($new, true);
  }

  imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

  switch($type){
    case 'bmp': imagewbmp($new, $dst); break;
    case 'gif': imagegif($new, $dst); break;
    case 'jpg': imagejpeg($new, $dst); break;
    case 'png': imagepng($new, $dst); break;
  }
  return '';
}//end function
function ImageTagResize($imagePath, $target) { 
	// get image info
	list($width, $height, $type, $attr) = getimagesize($imagePath);
	// takes the larger size of the width and height and applies the formula accordingly...this is so this script will work dynamically with any size image
	$percentage = $width > $height ? ($target / $width) : ($target / $height); 
	//gets the new value and applies the percentage, then rounds the value 
	$width = round($width * $percentage); 
	$height = round($height * $percentage); 
	//returns the new sizes in html image tag format...this is so you can plug this function inside an image tag and just get the 
	return "width=\"$width\" height=\"$height\""; 
} 

////////////////////////////     PHP FORM FIELD GENERATION FUNCTIONS     \\\\\\\\\\\\\\\\\\\\\\\\\\\

function getOptions($query, $sel = '') {
	$list	 = '';
	$records = Database::Execute($query);

	while ($records->MoveNext()) {
		$temp = 0;
		$list .= '<option value="' . $records->$temp . '"';
		if ($records->$temp == $sel) $list .= " selected";
		$temp = 1;
		$list .= ">" . htmlspecialchars($records->$temp) . "</option>\r\n";
	}

	return $list;
}
function getDataOptions($dataFile, $selectedValue = "")
{ 
	$doc = new DOMDocument();
	$doc->load($dataFile);
	$options = $doc->getElementsByTagName("option");
	$html = '<option value="">-- Select --</option>'; 
	foreach( $options as $option) 
	{
		$value = $option->getAttribute('value'); 
		$text = $option->getAttribute('text'); 
		$selected = $selectedValue == $value ? "selected" : ""; 
		$html .= sprintf("<option value=\"%s\" %s>%s</option>\n", $value, $selected, $text);
	}
	return $html; 
}
function buildSelectOptions($tblName, $valueField, $nameField, $index = 0, $query = "") {
		$list	 = '';
		$sql	 = "SELECT $valueField as value, $nameField as name 
					FROM $tblName
					$query 
				    ORDER BY $nameField
					LIMIT 1000"; 
		$record = Database::Execute($sql);

		while ($record->MoveNext()) {
			$list .= '<option value="' . $record->value . '"';
			if ($record->value == $index) $list .= " selected";
			$list .= ">" . htmlspecialchars($record->name) . "</option>\r\n";
		}

		return $list;
}
function buildCategoryOptions($tblName, $catId = 0) {
	
		$categories = array();
		$sql	 = "SELECT cat_id, cat_parent_id, cat_name FROM $tblName
				    ORDER BY cat_parent_id, cat_sort_id, cat_name, cat_id";
		$catData = Database::Execute($sql);

		while ($catData->MoveNext()) {

			if ($catData->cat_parent_id == 0) {
				$categories[$catData->cat_id] = array('name' => $catData->cat_name, 'children' => array());
			} else {
				$categories[$catData->cat_parent_id]['children'][] = array('id' => $catData->cat_id, 'name' => $catData->cat_name);	
			}
		}

		// build combo box options
		$list = '';


		foreach ($categories as $key => $value) {
			$name     = $value['name'];
			$children = $value['children'];

			$list .= "<option value=\"$key\"";
			if ($key == $catId) {
				$list.= " selected";
			}
			$list .= ">$name</option>\r\n";

			foreach ($children as $child) {
				$list .= "<option value=\"{$child['id']}\"";
				if ($child['id'] == $catId) {
					$list.= " selected";
				}
				$list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
			}
		}

		return $list;
}
function buildCategoryOptionsII($tblName, $tblCatName, $catId = 0, $sql2 = '') {

	$Id	= 0;
	$categories = array();
	$sql = "SELECT cat_id, cat_parent_id, cat_name FROM $tblCatName
		ORDER BY cat_parent_id, cat_sort_id, cat_name, cat_id";
	$catData = Database::Execute($sql);

	while ($catData->MoveNext()) {
		$sql = "SELECT COUNT(*) AS Total FROM $tblName WHERE cat_id = {$catData->cat_id} $sql2 ";
		$dt  = Database::Execute($sql);
		$dt->MoveNext();

		if ($dt->Total > 0 && $Id == 0) {
			$Id	= $catData->cat_id;
		}

		if ($catData->cat_parent_id == 0) {
			$categories[$catData->cat_id] = array('name' => $catData->cat_name, 'total' => $dt->Total, 'children' => array());
		} else {
			$categories[$catData->cat_parent_id]['children'][] = array('id' => $catData->cat_id, 'name' => $catData->cat_name, 'total' => $dt->Total);	
		}
	}

	// build combo box options
	$list = '';

	foreach ($categories as $key => $value) {
		$name		= $value['name'];
		$total		= $value['total'];
		$children	= $value['children'];

		if ($total > 0) {
			$list .= "<option value=\"$key\"";
			if ($key == $catId) {
				$list.= " selected";
			}
			$list .= ">$name</option>\r\n";

			foreach ($children as $child) {
				if ($child['total'] > 0) {
					$list .= "<option value=\"{$child['id']}\"";
					if ($child['id'] == $catId) {
						$list.= " selected";
					}
					$list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
				}
			}
		} else {
			if (sizeof($children) > 0) {
				$list .= "<optgroup label=\"$name\">"; 

				foreach ($children as $child) {
					if ($child['total'] > 0) {
						$list .= "<option value=\"{$child['id']}\"";
						if ($child['id'] == $catId) {
							$list.= " selected";
						}
						$list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
					}
				}

				$list .= "</optgroup>";

			} else {

				$list .= "<option value=\"$key\"";
				if ($key == $catId) {
					$list.= " selected";
				}
				$list .= ">$name</option>\r\n";
			}
		}
	}// end foreach

	return array($Id, $list);
}
function buildCategoryMultiOptions($tblName, $catId = array(), $groupOpt = true) {

	$categories = array();
	$sql	 = "SELECT cat_id, cat_parent_id, cat_name FROM $tblName
			ORDER BY cat_parent_id, cat_sort_id, cat_name, cat_id";
	$catData = Database::Execute($sql);

	while ($catData->MoveNext()) {

		if ($catData->cat_parent_id == 0) {
			$categories[$catData->cat_id] = array('name' => $catData->cat_name, 'children' => array());
		} else {
			$categories[$catData->cat_parent_id]['children'][] = array('id' => $catData->cat_id, 'name' => $catData->cat_name);	
		}
	}

	// build combo box options
	$list = '';

	if ($groupOpt) {
		foreach ($categories as $key => $value) {
			$name     = $value['name'];
			$children = $value['children'];

			if (sizeof($children) > 0) {
				$list .= "<optgroup label=\"$name\">"; 

				foreach ($children as $child) {
					$list .= "<option value=\"{$child['id']}\"";
					if (in_array($child['id'], $catId)) {
						$list.= " selected";
					}
					$list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
				}

				$list .= "</optgroup>";

			} else {

				$list .= "<option value=\"$key\"";
				if (in_array($key, $catId)) {
					$list.= " selected";
				}
				$list .= ">$name</option>\r\n";
			}
		}

	  } else {

		foreach ($categories as $key => $value) {
			$name     = $value['name'];
			$children = $value['children'];

			$list .= "<option value=\"$key\"";
			if (in_array($key, $catId)) {
				$list.= " selected";
			}
			$list .= ">$name</option>\r\n";

			foreach ($children as $child) {
				$list .= "<option value=\"{$child['id']}\"";
				if (in_array($child['id'], $catId)) {
					$list.= " selected";
				}
				$list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
			}
		}
	}

	return $list;
}
function buildParentCategoryOptions($tblName, $catId = 0) {
	//------------------------------
	$categories = array();
	$sql	 = "SELECT cat_id, cat_parent_id, cat_name FROM $tblName WHERE cat_parent_id = 0
				ORDER BY cat_parent_id, cat_sort_id, cat_name, cat_id";
	$catData = Database::Execute($sql);

	while ($catData->MoveNext()) {

		if ($catData->cat_parent_id == 0) {
			$categories[$catData->cat_id] = array('name' => $catData->cat_name, 'children' => array());
		} else {
			$categories[$catData->cat_parent_id]['children'][] = array('id' => $catData->cat_id, 'name' => $catData->cat_name);	
		}
	}

	// build combo box options
	$list = '';

	foreach ($categories as $key => $value) {
		$name     = $value['name'];
		$children = $value['children'];

		$list .= "<option value=\"$key\"";
		if ($key == $catId) {
			$list.= " selected";
		}
		$list .= ">$name</option>\r\n";

		foreach ($children as $child) {
			$list .= "<option value=\"{$child['id']}\"";
			if ($child['id'] == $catId) {
				$list.= " selected";
			}
			$list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
		}
	}
	return $list; 
}
function buildSubCategoryOptions($tblName, $parentId, $catId = 0) {

	$categories = array();
	$sql	 = "SELECT cat_id, cat_parent_id, cat_name FROM $tblName
			WHERE cat_parent_id = $parentId 
		ORDER BY cat_sort_id, cat_name, cat_id";
	$catData = Database::Execute($sql);

	while ($catData->MoveNext()) {
		$categories[$catData->cat_id] = array('name' => $catData->cat_name);
	}

	// build combo box options
	$list = '';

	foreach ($categories as $key => $value) {
		$name  = $value['name'];

		$list .= "<option value=\"$key\"";
		if ($key == $catId) {
			$list.= " selected";
		}
		$list .= ">$name</option>\r\n";
	}

	return $list;
}

////////////////////////////     PHP UTILTY FUNCTIONS     \\\\\\\\\\\\\\\\\\\\\\\\\\\

function removeNonAlphaNumeric($value) {
	return preg_replace("/[^a-zA-Z0-9\s]/", "", $value);
}
function createPassword($length, $chars = "234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") {
	$i = 0;
	$password = "";
	while ($i < $length) {
		$password .= $chars{mt_rand(0,strlen($chars) - 1)};
		$i++;
	}
	return $password;
}
function cleanHTML($html) {
	//------------------------------
	// removing all unwanted tags
	$html = preg_replace("#<!--(.|\s)*?-->#", "",$html); 
	$html = preg_replace("'<style[^>]*?>.*?</style>'si", "",$html);
	$html = preg_replace("'<script[^>]*?>.*?</script>'si", "",$html);
	$html = preg_replace("#<(/)?(font|span|xml|style|link|script|meta|del|ins|[ovwxp]:\w+)[^>]*>#","",$html);

	// then run another pass over the html (twice), removing unwanted attributes
	$html = preg_replace("#<([^>]*)(class|lang|size|face|[ovwxp]:\w+)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>#","<\\1>",$html);
	$html = preg_replace("#<([^>]*)(class|lang|size|face|[ovwxp]:\w+)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>#","<\\1>",$html);

	// change macintosh chars to html chars
	$html = str_replace(array('�', '�'), '"', $html);
	$html = str_replace(array('�', "'"), '"', $html);
	$html = str_replace(' � ', ' - ', $html);

	// return clean HTML
	return $html;
}//end function
function timeDifference($date1, $date2) {

        $date1 = is_int($date1) ? $date1 : strtotime($date1);
        $date2 = is_int($date2) ? $date2 : strtotime($date2);
        
        if (($date1 !== false) && ($date2 !== false)) {
            if ($date2 >= $date1) {
                $diff = ($date2 - $date1);
                
                if ($days = intval((floor($diff / 86400))))
                    $diff %= 86400;
                if ($hours = intval((floor($diff / 3600))))
                    $diff %= 3600;
                if ($minutes = intval((floor($diff / 60))))
                    $diff %= 60;
                
                return array($days, $hours, $minutes, intval($diff));
            }
        }
        
        return false;
    }
function exportTableToCsv($table, $filename = 'export.csv')
{
	$csv_terminated = "\n";
	$csv_separator = ",";
	$csv_enclosed = '"';
	$csv_escaped = "\\";
	$sql_query = "select * from $table";
	
	// Gets the data from the database
	$result = mysql_query($sql_query);
	$fields_cnt = mysql_num_fields($result);
	
	$schema_insert = '';
	
	for ($i = 0; $i < $fields_cnt; $i++)
	{
		$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes(mysql_field_name($result, $i))) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
	} // end for
	
	$out = trim(substr($schema_insert, 0, -1));
	$out .= $csv_terminated;
	
	// Format the data
	while ($row = mysql_fetch_array($result))
	{
		$schema_insert = '';
		for ($j = 0; $j < $fields_cnt; $j++)
		{
			if ($row[$j] == '0' || $row[$j] != '')
			{
				
				if ($csv_enclosed == '')
				{
					$schema_insert .= $row[$j];
				} else
				{
					$schema_insert .= $csv_enclosed .
						str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row[$j]) . $csv_enclosed;
				}
			} else
			{
				$schema_insert .= '';
			}
			
			if ($j < $fields_cnt - 1)
			{
				$schema_insert .= $csv_separator;
			}
		} // end for
		
		$out .= str_replace(array('"', '&#039;'), "", htmlspecialchars_decode($schema_insert));
		$out .= $csv_terminated;
	} // end while
	
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Length: " . strlen($out));
	header("Content-type: text/x-csv");
	header("Content-Disposition: attachment; filename=$filename");
	echo $out; 
	exit;
	
} 

function generateRandStr($length) { 
	//------------------------------
		$randstr = "";

		for($i=0; $i<$length; $i++){
			$randnum = mt_rand(0,61);
			if($randnum < 10){
				$randstr .= chr($randnum+48);
			}else if($randnum < 36){
				$randstr .= chr($randnum+55);
			}else{
				$randstr .= chr($randnum+61);
			}
		}

		return $randstr;
	}
	
function format_uri( $string, $separator = '-', $maxChars = 100)
{
	$accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
	$remove = array('&', '+', '@', '%', "'");	
	$string = str_replace($remove, '', $string);
	$string = mb_strtolower( trim( $string ), 'UTF-8' );
	$string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
	$string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
	$string = preg_replace("/[$separator]+/u", "$separator", $string);
	return substr($string, 0, $maxChars);
}

//////////////////////////// FORMATTING FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\

function formatEmailLink($emailAddress, array $params = array()) {
	$parameters = antispambot($emailAddress); 
	$count = 0; 
	foreach($params as $key => $value) { 
		$delimitor = $count == 0 ? '?' : '&amp;'; 
		$parameters .= $delimitor . $key . '=' . antispambot($value); 
		$count++; 
	}
	return '<a class="emailLink" href="mailto: ' . $parameters . '">' . antispambot($emailAddress) . '</a>'; 
}
function antispambot($email) {
    $p = str_split(trim($email));
    $new_mail = '';
    foreach ($p as $val) {
        $new_mail .= '&#'.ord($val).';';
    }
    return $new_mail;
}

//////////////////////////// ENCRYPTION FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\

function DES_Encrypt($buffer, $key) {
	global $_WEBCONFIG; 
	// get the amount of bytes to pad
	$extra = 8 - (strlen($buffer) % 8);
	// add the zero padding
	if($extra > 0) {
		for($i = 0; $i < $extra; $i++) {
			$buffer .= "\0";
		}
	}

	$Encrypt = mcrypt_cbc(MCRYPT_3DES, $key, $buffer, MCRYPT_ENCRYPT, $_WEBCONFIG['IV_ENCRYPT']); 
	return base64_encode($Encrypt);
}

function DES_Decrypt($buffer, $key) {
	global $_WEBCONFIG; 
	$Encrypt = base64_decode($buffer);
	$Decrypt = mcrypt_cbc(MCRYPT_3DES, $key, $Encrypt, MCRYPT_DECRYPT, $_WEBCONFIG['IV_ENCRYPT']);
	return str_replace("\0", "", $Decrypt);
}

////////////////////////////     IMAGE EDITING FUNCTIONS     \\\\\\\\\\\\\\\\\\\\\\\\\\\

function getHeight($image) {
	if(file_exists($image)) { 
		$size	= getimagesize($image);
		$height	= $size[1];
	} else { 
		$height = 0; 
	}
	return $height;
}
function getWidth($image) {
	if(file_exists($image)) { 
		$size	= getimagesize($image);
		$width	= $size[0];
	} else { 
		$width = 0; 
	}
	return $width; 
}
function getProportionalWidth($width, $height, $newHeight) {
	return (int)ceil((double)$newHeight * $width / $height);
}
function getProportionalHeight($width, $height, $newWidth) {
	return (int)ceil((double)$newWidth * $height / $width);
}
function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale) {

	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType		= image_type_to_mime_type($imageType);
	$newImageWidth	= ceil($width * $scale);
	$newImageHeight	= ceil($height * $scale);
	$newImage		= imagecreatetruecolor($newImageWidth, $newImageHeight);

	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
		case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
		case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
	}

	imagecopyresampled($newImage, $source, 0, 0, $start_width, $start_height, $newImageWidth, $newImageHeight, $width, $height);

	switch($imageType) {
		case "image/gif":
			imagegif($newImage, $thumb_image_name); 
			break;
		case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			imagejpeg($newImage, $thumb_image_name, 90); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage, $thumb_image_name);  
			break;
	}

	chmod($thumb_image_name, 0777);
	return $thumb_image_name;
}
function ModifierContrast($hexcolor, $dark = '#000000', $light = '#FFFFFF') {

	return (hexdec($hexcolor) > 0xffffff/2) ? $dark : $light;
}
function HexToRGB($hex) {

	$hex = preg_replace("#", "", $hex);
	$color = array();
	if(strlen($hex) == 3) {
		$color['r'] = hexdec(substr($hex, 0, 1) . $r);
		$color['g'] = hexdec(substr($hex, 1, 1) . $g);
		$color['b'] = hexdec(substr($hex, 2, 1) . $b);
	} else if(strlen($hex) == 6) {
		$color['r'] = hexdec(substr($hex, 0, 2));
		$color['g'] = hexdec(substr($hex, 2, 2));
		$color['b'] = hexdec(substr($hex, 4, 2));
	}
	return $color;
}
function RGBToHex($r, $g, $b) {

	//String padding bug found and the solution put forth by Pete Williams (http://snipplr.com/users/PeteW)
	$hex = "#";
	$hex.= str_pad(dechex($r), 2, "0", STR_PAD_LEFT);
	$hex.= str_pad(dechex($g), 2, "0", STR_PAD_LEFT);
	$hex.= str_pad(dechex($b), 2, "0", STR_PAD_LEFT);
	return $hex;
}
function resizeImageCanvas($output_w, $output_h, $orig_filename, $new_filename = null, array $canvasRGB = array("R" => 255, "G"  => 255, "B" => 255))
{
	if(!file_exists($orig_filename)) {
		throw new exception("File not found or unable to open file -- \"$orig_filename\""); 
	} 
	if(is_null($new_filename)) {
		$new_filename = $orig_filename; 
	}
	list($orig_w, $orig_h) = getimagesize($orig_filename);
	$orig_img = imagecreatefromstring(file_get_contents($orig_filename));
	$scale = $orig_h > $orig_w ? $output_h/$orig_h : $output_w/$orig_w;
	$new_w =  $orig_w * $scale;
	$new_h =  $orig_h * $scale;
	$offest_x = ($output_w - $new_w) / 2;
	$offest_y = ($output_h - $new_h) / 2;
	$new_img = imagecreatetruecolor($output_w, $output_h);
	$bgcolor = imagecolorallocate($new_img, $canvasRGB['R'], $canvasRGB['G'], $canvasRGB['B']); 
	imagefill($new_img, 0, 0, $bgcolor); 
	imagecopyresampled($new_img, $orig_img, $offest_x, $offest_y, 0, 0, $new_w, $new_h, $orig_w, $orig_h);
	imagepng($new_img, $new_filename, 9);
	imagedestroy($new_img); 
}
function SanitizeData($str) {
	if (is_array($str)) {
		return array_map('_clean', $str);
	} else {
		$str = strip_tags($str);
		return str_replace('\\', '\\\\', trim(htmlspecialchars((get_magic_quotes_gpc() ? stripslashes($str) : $str), ENT_QUOTES)));
	}
}
function GetImage($Radio, $Field, $Val) {
	$arImage = array("checkCr_red.gif", "checkCr_empty.gif", "checkBx_red.gif", "checkBx_empty.gif");
	$indx = $Radio ? 0 : 2;

	if (is_array($Field)) {
		$indx = $indx + (in_array($Val, $Field) ? 0 : 1);
	} else {
		$indx = $indx + (strcasecmp($Field, $Val) == 0 ? 0 : 1);
	}

	return '<img src="//' . $_SERVER['HTTP_HOST'] . '/scripts/' . $arImage[$indx] . '" />';
}
function compress_image($source_url, $destination_url, $quality) {
	$info = getimagesize($source_url);
	
	if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
	elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
	elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
	
	//save file
	imagejpeg($image, $destination_url, $quality);
	
	//return destination file
	return $destination_url;
}

//////////////////////////////////// SOCIAL FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

function getVideoID($ytURL) {
	if(strpos($ytURL, "youtu.be") > 0) {
		return substr($ytURL, -11); 
	}
	$idStarts = strpos($ytURL, "?v=");
	if ($idStarts === FALSE) {
		$idStarts = strpos($ytURL, "&v=");
	}
	if ($idStarts === FALSE) {
		return false;
	}

	$idStarts +=3;

	$ytvIDlen = strpos($ytURL, "&", $idStarts);
	if ($ytvIDlen === FALSE) {
		$ytvIDlen = 11; // This is the default length of YouTube's video IDs
	} else {
		$ytvIDlen = $ytvIDlen - $idStarts;
	}

	$ytvID = substr($ytURL, $idStarts, $ytvIDlen);
	return $ytvID;
}
function getWPPosts($feedUrl, $limit = 5) {
			$posts = array(); 
			$xml = simplexml_load_string(curl_get_contents($feedUrl));
			$count = 1;
			foreach($xml->channel->item as $post) {
				if($count <= $limit) {
					array_push($posts, $post);
					$count++; 
				} else {
					break; 
				}
			}
			return $posts; 
} 

function getUserTweets($userName, $limit = 5) {
	$feedUrl = 'https://api.twitter.com/1/statuses/user_timeline.rss?screen_name=' . $userName;
	$tweets = array(); 
	$xml = simplexml_load_string(curl_get_contents($feedUrl));
	$count = 1;
	foreach($xml->channel->item as $twit) {
		if($count <= $limit) {
			$twit->title = str_replace($userName . ': ', '', $twit->title); 
			array_push($tweets, $twit);
			$count++; 
		} else {
			break; 
		}
	}
	return $tweets; 
} 

//////////////////////////////////// FRAMEWORK FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

/* Reverse Captcha Functions */ 
function validateRC() {
	global $_WEBCONFIG;  
		$rcCode =  isset($_SESSION['RC_CODE']) ?  $_SESSION['RC_CODE'] : ''; 
		return ($_POST['RC_Validate1'] == $rcCode
			&& $_POST['RC_Validate2'] == $_WEBCONFIG['FORM_SECURE_CODE']
			&& isNullOrEmpty($_POST['RC_Validate3']) 
			&& isNullOrEmpty($_POST['RC_Validate4'])); 
}
function loadRC() {
	global $_WEBCONFIG; 
	$_SESSION['RC_CODE'] = $_SESSION['RC_CODE'] = md5(uniqid(time())); 
	print '<input id="RC_Validate1" name="RC_Validate1" type="hidden" value="'. $_SESSION['RC_CODE'] .'" />';
	print '<input class="none" id="RC_Validate2" name="RC_Validate2" type="text" value="' . $_WEBCONFIG['FORM_SECURE_CODE'] .'" />'; 
	print '<input class="none" id="RC_Validate3" name="RC_Validate3" type="text" value="" />'; 
	print '<div class="none"><input id="RC_Validate4" name="RC_Validate4" type="email" value="" /></div>';
}
function loadLocalConfig($configPath) {
	global $_WEBCONFIG; 
	if (is_readable($configPath) && file_exists($configPath . '/web.config')) {
		$configXML = simplexml_load_file($configPath . '/web.config');
		if(isset($configXML->appSettings)) {
			foreach ($configXML->appSettings->add as $setting) {
					$_WEBCONFIG[(string)$setting->attributes()->key] = (string)$setting->attributes()->value; 
			}
		}		
	} else { 
		throw new Exception("Unable to locate web.config in $configPath"); 
	}
}
function getWebConfigSettings($configPath) {
	$config = array(); 
	if (is_readable($configPath) && file_exists($configPath . '/web.config')) {
		$configXML = simplexml_load_file($configPath . '/web.config');
		if(isset($configXML->appSettings)) {
			foreach ($configXML->appSettings->add as $setting) {
					$config[(string)$setting->attributes()->key] = (string)$setting->attributes()->value; 
			}
		}		
	} else { 
		throw new Exception("Unable to locate web.config in $configPath"); 
	}
	return $config; 
}
?>