<?php
/* Set server variables and protect from XSS attacks */
if(!isset($_SERVER['REQUEST_URI'])) {
	$_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'];
	if($_SERVER['QUERY_STRING']) {
		$_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
	}
}

if(!isset($_SERVER['HTTP_USER_AGENT'])) {
	$_SERVER['HTTP_USER_AGENT'] = "";
}

$_SERVER['FULL_URI'] =  $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

if(!isset($_SERVER['ABSOLUTE_URI'])) {
	$_SERVER['ABSOLUTE_URI'] =  "https://" . $_SERVER['FULL_URI'];
}

if(!isset($_SERVER['ROOT_URI'])) {
	$_SERVER['ROOT_URI'] =  "https://" . $_SERVER['HTTP_HOST'];
}

if(!isset($_SERVER['HTTP_CLIENT_IP'])) {
	if (getenv('HTTP_X_FORWARDED_FOR')) {
		$_SERVER['HTTP_CLIENT_IP'] = getenv('HTTP_X_FORWARDED_FOR');
	}
	elseif (getenv('HTTP_X_FORWARDED')) {
		$_SERVER['HTTP_CLIENT_IP'] = getenv('HTTP_X_FORWARDED');
	}
	elseif (getenv('HTTP_FORWARDED_FOR')) {
		$_SERVER['HTTP_CLIENT_IP'] = getenv('HTTP_FORWARDED_FOR');
	}
	elseif (getenv('HTTP_FORWARDED')) {
		$_SERVER['HTTP_CLIENT_IP'] = getenv('HTTP_FORWARDED');
	}
	else {
		$_SERVER['HTTP_CLIENT_IP'] = $_SERVER['REMOTE_ADDR'];
	}
}

$_SERVER['PHP_SELF'] = xss_sanitize($_SERVER['PHP_SELF']);
$_SERVER['SCRIPT_NAME'] = xss_sanitize($_SERVER['SCRIPT_NAME']);
$_SERVER['REQUEST_URI'] = xss_sanitize($_SERVER['REQUEST_URI']);
$_SERVER['FULL_URI'] = xss_sanitize($_SERVER['FULL_URI']);
$_SERVER['ABSOLUTE_URI'] = xss_sanitize($_SERVER['ABSOLUTE_URI']);
$_SERVER['ROOT_URI'] = xss_sanitize($_SERVER['ROOT_URI']);
$_SERVER['QUERY_STRING'] = xss_sanitize($_SERVER['QUERY_STRING']);
$_SERVER['REQUEST_PATH'] = xss_sanitize(strtok($_SERVER["REQUEST_URI"],'?'));
$_SERVER['REQUEST_SCRIPT'] = '';
$_SERVER['REQUEST_SLUG_IDENTIFIER'] = '';
$_SERVER['REQUEST_SLUG_TITLE'] = '';

if (isset($_GET['clearoff'])) {
	if (isset($_SESSION['filter']))		unset($_SESSION['filter']);
	if (isset($_SESSION['New']))		unset($_SESSION['New']);
}
if (isset($_GET['clearoff2'])) {
	if (isset($_SESSION['filter']))		unset($_SESSION['filter']);
}

/* Global Constants */
define("URL_DIRECTORY_SEPERATOR", '/');

// Database Settings
define("DATABASE_TYPE", 1); // 1 = MYSQL, 2 = MSSQL, 3 = ODBC
define("DATABASE_HOST", "web17sql");
define("DATABASE_USER", "vgmhomelinkemailreferral_user");
define("DATABASE_PASS", "xxuhnu6536G274EVdL43ytn34sVCR9fxr7p563y7Sa7xx");
define("DATABASE_NAME", "vgm_homelinkemailreferral_db");
?>