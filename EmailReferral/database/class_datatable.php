<?
class DataTable {
	private $data;
	private $count;
	private $index;

	//------------------------------
	public function __construct($result) {
	//------------------------------
		$this->data	 = $result;
		$this->count = count($result);
		$this->index = -1;
	}

	//------------------------------
	public function Row($index) {
	//------------------------------
		if ($index < 0 || $index >= $this->count)
			throw new Exception("Row index ($index) is out of bounds");

		return $this->data[$index];
	}

	//------------------------------
	public function Count() {
	//------------------------------
		return $this->count;
	}

	//------------------------------
	public function Pointer() {
	//------------------------------
		return $this->index;
	}

	//------------------------------
	public function BOF() {
	//------------------------------
		return $this->index == -1;
	}

	//------------------------------
	public function EOF() {
	//------------------------------
		return $this->index == $this->count;
	}

	//------------------------------
	public function Move($index) {
	//------------------------------
        if ($this->count) {
            $this->index = $index;
            return true;
        } else {
            $this->index = -1;
            return false;
        }
    }

	//------------------------------
	public function MoveFirst() {
	//------------------------------
		if ($this->count) {
			$this->index = 0;
			return true;
		} else {
			$this->index = -1;
			return false;
		}
	}

	//------------------------------
	public function MoveNext() {
	//------------------------------
		if ($this->index < $this->count - 1) {
			$this->index++;
			return true;
		} else {
			$this->index = $this->count;
			return false;
		}
	}

	//------------------------------
	public function MovePrevious() {
	//------------------------------
		if ($this->index > 0) {
			$this->index--;
			return true;
		} else {
			$this->index = -1;
			return false;
		}
	}

	//------------------------------
	public function MoveLast() {
	//------------------------------
		if ($this->count) {
			$this->index = $this->count - 1;
			return true;
		} else {
			$this->index = 0;
			return false;
		}
	}

	//------------------------------
	public function __get($attribute) {
	//------------------------------
		if ($this->index < 0 || $this->index >= $this->count)
			throw new Exception("Row index ($this->index) is out of bounds");

		$row = $this->data[$this->index];
		if (!array_key_exists($attribute, $row)) {
			throw new Exception("Column ($attribute) does not exist");
		} else {
			$value = stripslashes($row[$attribute]);
		}

			return $value;
	}

	//------------------------------
	public function __call($method, $param) {
	//------------------------------
		if ($method == "Field") {
			switch (count($param)) {
				case 0:
					throw new Exception("Method Field() does not accept 0 arguments");

				case 1:
					$index	= $this->index;
					$col	= $param[0];
					break;

				default:
					$index	= $param[0];
					$col	= $param[1];
					break;
			}

			$row = $this->Row($index);
			if (!array_key_exists($col, $row))
				throw new Exception("Column ($col) does not exist");
			else
				return $row[$col];
		}
	}
	
	//------------------------------
	public function ConvertToEntity($table) {
	//------------------------------
		$row = $this->data[$this->index];
		$entity = new Entity($table);
		$code = ""; 
		foreach($row as $key => $value) {
			if(!is_numeric($key)) {  
				$value = $value; 
				$entity->$key = $value;
			} 
		}
		return $entity; 
	}
};
?>