<?
abstract class Database {	
	
	const DATE_TIME_FORMAT = 'Y-m-j G:i:s'; 

	public static $db_link = false;
	private static $db_connected = false;

	//------------------------------
	public function __construct() {
	//------------------------------
		self::connect();
	}

	//------------------------------
	public function __destruct() {
	//------------------------------
		self::disconnect();
	}

	//------------------------------
	public static function connect() {
	//------------------------------
		switch (DATABASE_TYPE) {
			case 1:
				if(!self::$db_connected) {
					self::$db_link = mysqli_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME);
					
					if (mysqli_connect_error()) {
						throw new Exception("Unable to connect to SQL Server.<br /><b>SQL ERROR: </b>" . mysqli_connect_error());
					}

					if(!self::$db_link) {
						throw new Exception("Unable to connect to SQL Server.<br /><b>SQL ERROR: </b>" . mysqli_error(self::$db_link));
					}
					self::$db_connected = true;
				}
				break;

			case 2:
				if(!self::$db_connected) {
					self::$db_link = mssql_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASS);

					if(!self::$db_link)
						throw new Exception("Unable to connect to SQL Server.<br /><b>SQL ERROR: </b>" . mssql_get_last_message());

					if(!mssql_select_db(DATABASE_NAME, self::$db_link))
						throw new Exception("Unable to connect to SQL Database.<br /><b>SQL ERROR: </b>" . mssql_get_last_message());

					self::$db_connected = true;
				}
				break;

			case 3:
				if(!self::$db_connected) {
					self::$db_link = odbc_connect("Driver={SQL Server};Server=" . DATABASE_HOST . ";Database=" . DATABASE_NAME . ";", DATABASE_USER, DATABASE_PASS);

					if(!self::$db_link)
						throw new Exception('Database connection error :-(');

					self::$db_connected = true;
				}
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}
	}

	//------------------------------
	public static function disconnect() {
	//------------------------------
		switch (DATABASE_TYPE) {
			case 1:
				if(self::$db_connected)
					mysqli_close(self::$db_link);
				break;

			case 2:
				if(self::$db_connected)
					mssql_close(self::$db_link);
				break;

			case 3:
				if(self::$db_connected)
					odbc_close(self::$db_link);
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}
	}

	//------------------------------
	public static function Execute($sql) {
	//------------------------------
		if(!self::$db_connected)
			self::connect();

		switch (DATABASE_TYPE) {
			case 1:
				$res = mysqli_query(self::$db_link, $sql);
				if(!$res)
					throw new Exception("Invalid SQL Query.<br /><b>SQL ERROR: </b>" . mysqli_error(self::$db_link));

				if (!mysqli_num_fields($res)) {
					return;
				} else {
					$data = array();
					while (($row = mysqli_fetch_array($res))) $data[] = $row;
					return new DataTable($data);
				}
				break;

			case 2:
				$res = @mssql_query($sql, self::$db_link);
				if(!$res)
					throw new Exception("Invalid SQL Query.<br /><b>SQL ERROR: </b>" . mssql_get_last_message());

				if (!@mssql_num_fields($res)) {
					return;
				} else {
					$data = array();
					while (($row = mssql_fetch_array($res))) $data[] = $row;
					return new DataTable($data);
				}
				break;

			case 3:
				$res = @odbc_exec(self::$db_link, $sql);
				if(!$res)
					throw new Exception("Invalid SQL Query.");

				if (!@odbc_num_fields($res)) {
					return;
				} else {
					$data = array();
					while (($row = odbc_fetch_array($res))) $data[] = $row;
					return new DataTable($data);
				}
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}
	}

	//------------------------------
	public static function ExecuteRaw($sql) {
	//------------------------------
		if(!self::$db_connected)
			self::connect();

		switch (DATABASE_TYPE) {
			case 1:
				$res = mysqli_query(self::$db_link, $sql);
				if(!$res)
					throw new Exception("Invalid SQL Query.<br /><b>SQL ERROR: </b>" . mysqli_error(self::$db_link));
				break;

			case 2:
				$res = @mssql_query($sql, self::$db_link);
				if(!$res)
					throw new Exception("Invalid SQL Query.<br /><b>SQL ERROR: </b>" . mssql_get_last_message());
				break;

			case 3:
				$res = @odbc_exec(self::$db_link, $sql);
				if(!$res)
					throw new Exception("Invalid SQL Query.");
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}

		return $res;
	}

	//------------------------------
	public static function insert_bdd($recordClass) {
	//------------------------------
		if(!self::$db_connected)
			self::connect();

		$table = $recordClass->_table;
		if(!$table)
			throw new Exception('Don\'t know what table to use for ' . get_class($recordClass));

		$fields = $recordClass->_fields; 
		if(!$fields)
			throw new Exception('Don\'t know what fields describe ' . $table);

		$tb_champs	= array_fill_keys($fields, 1);
		$txt_fields	= '';
		$txt_values	= '';
		$i			= 0;

		foreach($recordClass as $key => $value) {

			if(isset($tb_champs[$key])) {

				if($i >0) {
					$txt_fields .= ',';
					$txt_values .= ',';
				}

				$txt_fields	.= $key;
				$c			 = self::quote_smart($value);

				if(is_numeric($value))
					$txt_values .= $c;
				elseif(is_null($value))
					$txt_values .= 'NULL';
				else
					$txt_values .= "'$c'";

				$i++;
			}
		}

		$txt_values  = str_replace('\\\\\\', '\\', $txt_values);
		$txt_values	 = htmlspecialchars_decode($txt_values);

		$count = 1;
		while($count)
			$txt_values = str_replace('  ', ' ', $txt_values, $count);

		switch (DATABASE_TYPE) {
			case 1:
				$sql = "INSERT INTO $table ($txt_fields) VALUES ($txt_values)"; 
				$res = mysqli_query(self::$db_link, $sql);

				if(!$res) {
					throw new Exception("Insert SQL Query Error.<br /><b>SQL ERROR: </b>" . mysqli_error(self::$db_link));
				}

				return mysqli_insert_id(self::$db_link);
				break;

			case 2:
				$sql = "INSERT INTO $table ($txt_fields) VALUES ($txt_values)";
				$res = mssql_query($sql, self::$db_link);

				if(!$res) {
					throw new Exception("Insert SQL Query Error.<br /><b>SQL ERROR: </b>" . mssql_get_last_message());
				}

				$result = @mssql_query("SELECT @@identity");
				if (!$result) {
					return -1;
				}

				return mssql_result($result, 0, 0);
				break;


			case 3:
				$sql = "INSERT INTO $table ($txt_fields) VALUES ($txt_values)";
				$res = @odbc_exec(self::$db_link, $sql);

				if(!$res) {
					throw new Exception("Insert SQL Query Error.<br /><b>SQL ERROR: </b>" . mssql_get_last_message());
				}

				$result = @odbc_exec(self::$db_link, "SELECT @@identity");
				if (!$result) {
					return -1;
				}

				return odbc_result($result, 0, 0);
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}
	}

	//------------------------------
	public static function update_bdd($recordClass) {
	//------------------------------
		if(!self::$db_connected)
			self::connect();
		$table = $recordClass->_table;
		if(!$table)
				throw new Exception('Class Table Not Specified');

		$fields = $recordClass->_fields; 
		if(!$fields)
				throw new Exception('Don\'t know what fields describe ' . $table);

		$tb_champs	 = array_fill_keys($fields, 1);
		$txt_requete = 'UPDATE '.$table;
		$i			 = 0;

		foreach($recordClass as $key => $valeur) {
			if(isset($tb_champs[$key])) {
				$txt_requete .= $i == 0 ? ' SET ' : ',';
				$txt_valeur	  = self::quote_smart($valeur);
				$txt_requete .= is_null($valeur) ? "$key=NULL" : "$key='$txt_valeur'";
				$i++;
			}
		}

		$i			= 0;
		$tempKey	= '';
		$primaryKey	= $recordClass->_primaryKey;

		foreach($primaryKey as $key) {
			$txt_requete .= $i == 0 ? ' WHERE ' : ' AND ';
			$tempKey	 .= ',';
			$c			  = self::quote_smart($recordClass->$key); // __get() could throw an exception if this field doesn't exist
			$txt_requete .= "$key='$c'"; // A primary key is never NULL
			$tempKey	 .= "$key='$c'"; // A primary key is never NULL
			$i++;
		}

		$txt_requete .= ';';
		$txt_requete  = str_replace($tempKey, '', $txt_requete);
		$txt_requete  = str_replace('\\\\\\', '\\', $txt_requete);
		$txt_requete  = htmlspecialchars_decode($txt_requete);

		$count = 1;
		while($count)
			$txt_requete = str_replace('  ', ' ', $txt_requete, $count);

		switch (DATABASE_TYPE) {
			case 1:
				$res = mysqli_query(self::$db_link, $txt_requete);

				if(!$res) {
					throw new Exception("Update SQL Query Error.<br /><b>SQL ERROR: </b>" . mysqli_error(self::$db_link));
				}
				break;

			case 2:
				if(!mssql_query($txt_requete, self::$db_link))
					throw new Exception("Update SQL Query Error.<br /><b>SQL ERROR: </b>" . mssql_get_last_message());
				break;

			case 3:
				if(!odbc_exec(self::$db_link, $txt_requete))
					throw new Exception("Update SQL Query Error.");
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}
	}

	//------------------------------
	public static function delete_bdd($recordClass) {
	//------------------------------
		if(!self::$db_connected)
			self::connect();

		$table = $recordClass->_table;
		if(!$table)
			throw new Exception('Don\'t know what table to use for ' . get_class($recordClass));

		$primaryKey = $recordClass->_primaryKey;
		if(!$primaryKey)
			throw new Exception('Don\'t know what are primary key fields for ' . $table);

		$t = array();
		foreach($primaryKey as $key => $value)
			$t[$value] = $recordClass->$value;

		self::deleteDirectly($table, $primaryKey, $t);
	}

	// Static function to remove a record from database
	//------------------------------
	protected static function deleteDirectly($table, $Pkfields, $Pk) {
	//------------------------------
		if(!self::$db_connected)
			self::connect();

		// Cannot use get_class($recordClass) to get the table because we're in a static function, so i'm using a parameter... same thing with $Pkfields
		// To be sure $Pk is filled with enough keys to describe a primary key, we have to verify
		$Pkfields = array_flip($Pkfields);

		if(count(array_intersect_key($Pk, $Pkfields)) != count($Pkfields))
			throw new Exception('Primary key fields does not match those of table ' . $table);

		$txt_requete = 'DELETE FROM '.$table;
		$i			 = 0;

		foreach($Pk as $key => $value) {
			$txt_requete .= $i == 0 ? ' WHERE ' : ' AND ';
			$c			  = self::quote_smart($value);
			$txt_requete .= "$key='$c'"; // A primary key is never NULL
		}

		$txt_requete .= ';';

		switch (DATABASE_TYPE) {
			case 1:
				if(!mysqli_query(self::$db_link, $txt_requete))
					throw new Exception("Delete SQL Query Error.<br /><b>SQL ERROR: </b>" . mysqli_error(self::$db_link));

				if(mysqli_affected_rows(self::$db_link) == 0) // Primary Key does not match any record => Exception
					throw new Exception("Delete SQL Query Error.<br /><b>SQL ERROR: </b>0 Records Affected.");
				break;

			case 2:
				if(!mssql_query($txt_requete, self::$db_link))
					throw new Exception("Delete SQL Query Error.<br /><b>SQL ERROR: </b>" . mssql_get_last_message());
				break;

			case 3:
				if(!odbc_exec(self::$db_link, $txt_requete))
					throw new Exception("Delete SQL Query Error.");
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}
	}

	//------------------------------
	public static function getPrimaryKey($table) {
	//------------------------------
		if(!self::$db_connected)
			self::connect();

		$keys = array();

		switch (DATABASE_TYPE) {
			case 1:
				$result = mysqli_query(self::$db_link, 'SHOW KEYS FROM ' . $table);
				if(!$result)
					throw new Exception('Impossible to get primary key(s) of table ' . $table);

				while($row = mysqli_fetch_assoc($result)) {
					if ($row['Key_name'] == 'PRIMARY')
						$keys[$row['Seq_in_index'] - 1] = $row['Column_name'];
				}
				break;

			case 2:
				$sql 	= "SELECT T.TABLE_NAME, T.CONSTRAINT_NAME, K.COLUMN_NAME, K.ORDINAL_POSITION
						FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS T 
					INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS K ON T.CONSTRAINT_NAME = K.CONSTRAINT_NAME
				WHERE (T.CONSTRAINT_TYPE = 'PRIMARY KEY') AND (T.TABLE_NAME = '$table')
			ORDER BY T.TABLE_NAME, K.ORDINAL_POSITION";
				$result = mssql_query($sql, self::$db_link);
				if(!$result)
					throw new Exception('Impossible to get primary key(s) of table ' . $table);

				while($row = mssql_fetch_assoc($result)) {
					$keys[] = $row['COLUMN_NAME'];
				}

				if (sizeof($keys) == 0 && strlen($tbl_key) > 0) 
					$keys[] = $tbl_key;
				break;

			case 3:
				$sql 	= "SELECT T.TABLE_NAME, T.CONSTRAINT_NAME, K.COLUMN_NAME, K.ORDINAL_POSITION
						FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS T 
					INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS K ON T.CONSTRAINT_NAME = K.CONSTRAINT_NAME
				WHERE (T.CONSTRAINT_TYPE = 'PRIMARY KEY') AND (T.TABLE_NAME = '$table')
			ORDER BY T.TABLE_NAME, K.ORDINAL_POSITION";
				$result = odbc_exec(self::$db_link, $sql);
				if(!$result)
					throw new Exception('Impossible to get primary key(s) of table ' . $table);

				while($row = odbc_fetch_array($result)) {
					$keys[] = $row['COLUMN_NAME'];
				}

				if (sizeof($keys) == 0 && strlen($tbl_key) > 0) 
					$keys[] = $tbl_key;
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}

		return $keys;
	}

	//------------------------------	
	public static function getFields($table) {
	//------------------------------
		if(!self::$db_connected)
			self::connect();

		// Can't use self::$_table ; so here is a nice cheat :
		$tb = array();

		switch (DATABASE_TYPE) {
			case 1:
				$result = mysqli_query(self::$db_link, 'SHOW COLUMNS FROM ' . $table);
				if(!$result)
					throw new Exception('Impossible to get information about table ' . $table);

				while($row = mysqli_fetch_assoc($result))
					$tb[] = $row['Field'];
				break;

			case 2:
				$sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = '$table'";
				$result = mssql_query($sql, self::$db_link);
				if(!$result)
					throw new Exception('Impossible to get information about table ' . $table);

				while($row = mssql_fetch_assoc($result))
					$tb[] = $row['COLUMN_NAME'];
				break;

			case 3:
				$sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = '$table'";
				$result = odbc_exec(self::$db_link, $sql);
				if(!$result)
					throw new Exception('Impossible to get information about table ' . $table);

				while($row = odbc_fetch_array($result))
					$tb[] = $row['COLUMN_NAME'];
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}

		return $tb;
	}

	//------------------------------
	public static function getInsertID() {
	//------------------------------
		switch (DATABASE_TYPE) {
			case 1:
				return mysqli_insert_id(self::$db_link);
				break;

			case 2:
				$result = @mssql_query("SELECT @@identity");
				if (!$result) {
					return -1;
				}
				return mssql_result($result, 0, 0);
				break;

			case 3:
				$result = @odbc_exec(self::$db_link, "SELECT @@identity");
				if (!$result) {
					return -1;
				}
				return odbc_result($result, 0, 0);
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}
	}

	//------------------------------
	public function Count($result) { 
	//------------------------------
		switch (DATABASE_TYPE) {
			case 1:
				return mysqli_num_rows($result); 
				break;

			case 2:
				return mssql_num_rows($result); 
				break;

			case 3:
				return odbc_num_rows($result); 
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}
	} 

	//------------------------------
	public static function TableExists($table) { 
	//------------------------------
		if(!self::$db_connected)
			self::connect();

		switch (DATABASE_TYPE) {
			case 1:
				if( mysqli_num_rows( mysqli_query(self::$db_link, "SHOW TABLES LIKE '".$table."'"))) {
					return TRUE;
				}
				break;

			case 2:
				$sql = "SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME = '$table'";
				$result = mssql_query($sql, self::$db_link);
				if(!$result)
					throw new Exception('Impossible to get information about table ' . $table);

				if (mssql_num_fields($result)) {
					return TRUE;
				}
				break;

			case 3:
				$sql = "SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME = '$table'";
				$result = odbc_exec(self::$db_link, $sql);
				if(!$result)
					throw new Exception('Impossible to get information about table ' . $table);

				if (odbc_num_fields($result)) {
					return TRUE;
				}
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}

		return FALSE;
	}

	//------------------------------
	public static function quote_smart($value) {
	//------------------------------
		if(!self::$db_connected)
			self::connect();

		if(get_magic_quotes_gpc())
			$value = stripslashes($value);

		switch (DATABASE_TYPE) {
			case 1:
				if(!is_numeric($value))
					$value = mysqli_real_escape_string(self::$db_link, $value);
				break;

			case 2:
				if(!is_numeric($value)) {
					$characters = array('/\x00/', '/\x1a/', '/\n/', '/\r/', '/\\\/', '/\'/'); 
					$replace    = array('\\\x00', '\\x1a', '\\n', '\\r', '\\\\', "''"); 
					$value		= preg_replace($characters, $replace, $value); 
				}
				break;

			case 3:
				if(!is_numeric($value)) {
					$characters = array('/\x00/', '/\x1a/', '/\n/', '/\r/', '/\\\/', '/\'/'); 
					$replace    = array('\\\x00', '\\x1a', '\\n', '\\r', '\\\\', "''"); 
					$value		= preg_replace($characters, $replace, $value); 
				}
				break;

			default:
				throw new Exception("THERE WAS AN ERROR IN THE CONNECTION SETTINGS!");
		}

		return $value;
	}
	
	public static function get_version() {
		$record = Database::Execute("SELECT VERSION() AS version"); 
		$record->MoveNext(); 
		return $record->version; 
	}
};
?>