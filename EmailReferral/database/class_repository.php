<?
class Repository {
	
	//------------------------------
	public static function GetById($table, $id) { 
	//------------------------------
		$primary = self::GetSinglePrimaryKey($table);
		$id = Database::quote_smart($id); 
		$sql = "SELECT * FROM ". Database::quote_smart($table)." WHERE $primary = '$id' LIMIT 1";
		$record	= Database::Execute($sql);
		if ($record->Count() == 0) {
			return null; 
		} else { 
			$record->MoveNext();
			$entity = $record->ConvertToEntity($table); 
			return $entity; 
		}
	}
	
	//------------------------------
	public static function LoadById($table, $id) { 
	//------------------------------
		$primary = self::GetSinglePrimaryKey($table);
		$id = Database::quote_smart($id); 
		$sql = "SELECT $primary FROM ". Database::quote_smart($table)." WHERE $primary = '$id' LIMIT 1";
		$record	= Database::Execute($sql);
		if ($record->Count() == 0) {
			return null; 
		} else { 
			$record->MoveNext();
			$entity = $record->ConvertToEntity($table); 
			return $entity; 
		}
	}
	
	//------------------------------
	public static function GetByFieldName($table, $field, $value) { 
	//------------------------------
		$sql = "SELECT * FROM ". Database::quote_smart($table)." WHERE ". Database::quote_smart($field)." = '". Database::quote_smart($value)."' LIMIT 1";
		$record	= Database::Execute($sql);
		if ($record->Count() == 0) {
			return null; 
		} else { 
			$record->MoveNext();
			$entity = $record->ConvertToEntity($table); 
			return $entity; 
		}
	}
	
	//------------------------------
	public static function GetFirst($table, array $parameters = array()) { 
	//------------------------------ 
		$primary = self::GetSinglePrimaryKey($table); 
		$sql = "SELECT * FROM $table ORDER BY $primary ASC LIMIT 1";
		$record	= Database::Execute($sql);
		if ($record->Count() == 0) {
			return null; 
		} else { 
			$record->MoveNext();
			$entity = $record->ConvertToEntity($table); 
			return $entity; 
		}
	}
	
	//------------------------------
	public static function GetLast($table, array $parameters = array()) { 
	//------------------------------
		$primary = self::GetSinglePrimaryKey($table); 
		$params = self::GenerateSQL($parameters, array('WHERE')); 
		$sql = "SELECT * FROM $table $params ORDER BY $primary DESC LIMIT 1";
		$record	= Database::Execute($sql);
		if ($record->Count() == 0) {
			return null; 
		} else { 
			$record->MoveNext();
			$entity = $record->ConvertToEntity($table); 
			return $entity; 
		}
	}
	
	//------------------------------
	public static function GetAll($table, array $parameters = array()) { 
	//------------------------------
		$collection = array(); 
		$params = self::GenerateSQL($parameters);
		$sql = "SELECT * FROM $table $params";
		$record	= Database::Execute($sql);
		
		if ($record->Count() > 0) {
			while($record->MoveNext()) { 
				$entity = $record->ConvertToEntity($table); 
				array_push($collection, $entity); 
			}
		}
		
		return $collection; 
	}
	
	//------------------------------
	public static function Exists($table, $id) { 
	//------------------------------
		$primary = self::GetSinglePrimaryKey($table);
		$id = Database::quote_smart($id); 	
		$sql = "SELECT $primary FROM $table WHERE $primary = '$id' LIMIT 1";
		$record	= Database::Execute($sql);
		return $record->Count() == 1; 
	}
	
	//------------------------------
	public static function ValueExists($table, $field, $value) { 
	//------------------------------
		$sql = "SELECT $field FROM $table WHERE $field = '$value' LIMIT 1";
		$record	= Database::Execute($sql);
		return $record->Count() == 1; 
	}

	//------------------------------
	public static function Insert(Entity $entity) {
	//------------------------------
		return Database::insert_bdd($entity);
	}
	
	//------------------------------
	public static function Copy(Entity $entity) {
	//------------------------------
		eval('$entity->' . $entity->_primaryKey[0] . ' = NULL;'); 
		return Database::insert_bdd($entity);
	}

	//------------------------------
	public static function Update(Entity $entity) {
	//------------------------------
		Database::update_bdd($entity);
	}
	
	//------------------------------
	public static function Save(Entity $entity) {
	//------------------------------
		if($entity->attributeIsset($entity->_primaryKey[0])) { ; 
			Database::update_bdd($entity);   
   return self::GetById($entity->_table,$entity->_primaryKey[0]);
		} else {
			Database::insert_bdd($entity);
			return self::GetById($entity->_table,Database::getInsertID());
		}
	}

	//------------------------------
	public static function Delete(Entity $entity) {
	//------------------------------
		Database::delete_bdd($entity);
	}
	
	//////////////////////////////////////// Tree SQL Functions \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	
	//------------------------------
	public static function GetParentId($table, $id, $fieldPrefix = ''){
	//------------------------------
		$id= (int)$id; 
		$field = $fieldPrefix . 'parent_id'; 
		$sql = "SELECT $field FROM $table where {$fieldPrefix}id = $id LIMIT 1"; 
		$record = Database::Execute($sql);
		if($record->count() > 0) { 
			$record->MoveNext();
			return eval("return (int){$record->$field};");
		} else {
			return null;
		}
	}
	
	//------------------------------
	public static function HasChildren($table, $id, $fieldPrefix = ''){
	//------------------------------
		$id= (int)$id; 
		$sql = "SELECT {$fieldPrefix}id FROM $table where {$fieldPrefix}parent_id = $id AND {$fieldPrefix}parent_id LIMIT 1";
		$record = Database::Execute($sql);
		if($record->count() > 0) { 
			return true;  
		} else {
			return false; 
		}
	}
	
	//------------------------------
	public static function HasAncestor($table, $id, $ancestorId, $fieldPrefix = '') { 
		//------------------------------
		$id = (int) $id;
		while(true) {
			$newId = self::GetParentId($table, $id, $fieldPrefix); 
			if(!is_null($newId) && $newId != $ancestorId) {
				$id = $newId; 
			} else if(!is_null($newId)) {
				return true; 
			} else {
				return false; 
			}
		}
	}
    
    //------------------------------
    public static function getByValue($args) { 
    //------------------------------
    $table = $args['table'];
    $key   = $args['key'];
    $value = $args['value'];

    Database::Execute('select (2+2) as four');  

    $sql = "SELECT * FROM ".Database::quote_smart($table)." WHERE ".Database::quote_smart($key)." = '".Database::quote_smart($value)."' LIMIT 1";
    $record    = Database::Execute($sql);
    
        if ($record->Count() == 0) {
            return false; 
        } 
        else { 
            $record->MoveNext();
            $entity = $record->ConvertToEntity($table); 
            return $entity; 
        }
    }
	
	//------------------------------
	public static function GetGrandParentId($table, $id, $fieldPrefix = '') { 
		//------------------------------
		$parentId = self::getParentId($id); 
		if(!is_null($parentId)) {
			return (int)self::GetParentId($parentId, $table, $fieldPrefix); 
		} else { 
			return null; 
		}
	}
	
	//------------------------------
	public static function GetRootId($table, $id, $fieldPrefix = '') { 
		//------------------------------
		$id = (int) $id;
		while(true) {
			$newId = self::GetParentId($table, $id, $fieldPrefix); 
			if(!is_null($newId) && $newId != 0) {
				$id = $newId; 
			} else {
				break; 
			}
		}
		return $id; 
	}
	
	//------------------------------
	public static function GetRootGrandChildId($table, $id, $fieldPrefix = '') { 
	//------------------------------
		$id = (int) $id;
		$rootId = self::GetRootChildId($table, $id, $fieldPrefix); 
		while(true) {
			$newId = self::GetParentId($table, $id, $fieldPrefix); 
			if(!is_null($newId) && $newId != $rootId) {
				$id = $newId; 
			} else {
				break; 
			}
		}
		return $id; 
	}
	
	//------------------------------
	public static function GetRootChildId($table, $id, $fieldPrefix = '') { 
		//------------------------------
		$id = (int) $id;
		$rootId = self::GetRootId($table, $id, $fieldPrefix); 
		while(true) {
			$newId = self::GetParentId($table, $id, $fieldPrefix); 
			if(!is_null($newId) && $newId != $rootId) {
				$id = $newId; 
			} else {
				break; 
			}
		}
		return $id; 
	}
	
	//------------------------------
	public static function GetVariable($table, $var_name, $id) {
	//------------------------------
		$id = Database::quote_smart($id); 
		$sql = "SELECT $var_name FROM ". Database::quote_smart($table)." WHERE $primary = '$id' LIMIT 1";
		$record	= Database::Execute($sql);
		if ($record->Count() == 0) {
			return null; 
		} else { 
			$record->MoveNext();
			return $record->$$varname;
		}
	}
	
	//////////////////////////////////////// Local Helper Functions \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	
	//------------------------------
	private static function GetSinglePrimaryKey($table) {
	//------------------------------
		if(Database::TableExists($table)) { 
			$keys = Database::getPrimaryKey($table);
		} else {
			throw new exception("Table " . $table . " does not exist"); 
		}
		return $keys[0]; 
	}
	
	//------------------------------
	private static function GenerateSQL(array $parameters = array(), array $allowed = array()) {
		//------------------------------
		$sql = '';		
		if(isset($parameters['WHERE']) && (count($allowed) == 0 || in_array('WHERE', $allowed))) { 
			$where = $parameters['WHERE']; 
			$sql .= "WHERE $where "; 
		}
		if(isset($parameters['ORDER']) && (count($allowed) == 0 || in_array('ORDER', $allowed))) { 
			$order = $parameters['ORDER']; 
			$sql .= "ORDER BY $order "; 
		}
		return $sql; 
	}
};
?>