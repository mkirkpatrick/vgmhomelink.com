<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Libre+Franklin:300,400,800"> 

<script src="//kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>
<script async src="<?php autoCreateVerison('/scripts/stacktable.min.js'); ?>"></script>
<script>var siteType = "<?= $_WEBCONFIG['SITE_TYPE'] ?>";</script>
<script async src="<?php autoCreateVerison('/scripts/global.js'); ?>"></script>
<script async src="<?php autoCreateVerison('/scripts/aos.js'); ?>"></script>
<script>
  var loadDeferredStyles = function() {
	var addStylesNode = document.getElementById("deferred-styles");
	var replacement = document.createElement("div");
	replacement.innerHTML = addStylesNode.textContent;
	document.body.appendChild(replacement)
	addStylesNode.parentElement.removeChild(addStylesNode);
  };
  var raf = requestAnimationFrame || mozRequestAnimationFrame ||
	  webkitRequestAnimationFrame || msRequestAnimationFrame;
  if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
  else window.addEventListener('load', loadDeferredStyles);
</script>

<?php /*?><?php
	if (isset($_SESSION['member_id'])) { ?><?php */
	// This is where kendo was being called
?>	
<?php
if (isset($cms) && $cms->background_image != '') { ?>
    <div class="backgroundImage none"></div>
    <script type="text/javascript">
        var bkgroundImage = '<?= $cms->background_image ?>';
        $('.backgroundImage').html(bkgroundImage);
        var backgroundUrl = $('.backgroundImage img').attr('src');
        var title = document.getElementsByTagName("title")[0].innerHTML;

        if(title.toLowerCase().indexOf("home page") >= 0){
            $('section.marketing.clearfix').css('background', 'url('+backgroundUrl+') center top no-repeat');
        } else {
            $('div.page-title .featured-image-header').css('background', 'url('+backgroundUrl+') center top no-repeat');
            $('div.page-title .featured-image-header').css('background-size','cover');
            $('div.page-title .featured-image-header').css('background-position', '30%');
        }

    </script> <?
}
?>
<?

// Add External Scripts
if(isset($GLOBALS["externalJavascriptTags"])){
    foreach($GLOBALS["externalJavascriptTags"] as $extJscript) {
        print $extJscript;
    }    
}    

// Add Internal Scripts
if(isset($GLOBALS["javascriptTags"])){
    foreach($GLOBALS["javascriptTags"] as $jscript) {
        print '<script src="' . autoCreateVerison($jscript, "return") . '"></script>';
    }    
} ?>
</body>
</html>
<?php
$pageTitle       = isset($GLOBALS["pageTitle"]) && !isNullOrEmpty($GLOBALS["pageTitle"]) ? $GLOBALS["pageTitle"] : $siteConfig['Default_Page_Title'];;
$pageKeywords    = isset($GLOBALS["pageKeywords"]) && !isNullOrEmpty($GLOBALS["pageKeywords"]) ? $GLOBALS["pageKeywords"] : $siteConfig['Default_Page_Keywords'];;
$pageDescription = isset($GLOBALS["pageDescription"]) && !isNullOrEmpty($GLOBALS["pageDescription"]) ? $GLOBALS["pageDescription"] : $siteConfig['Default_Page_Description']; 
$pageUrl         = isset($GLOBALS["pageUrl"]) && !isNullOrEmpty($GLOBALS["pageUrl"]) ? $GLOBALS["pageUrl"] : $_SERVER['ABSOLUTE_URI'];
$pageImage       = isset($GLOBALS["pageImage"]) && !isNullOrEmpty($GLOBALS["pageImage"]) ? $GLOBALS["pageImage"] : $_SERVER['ROOT_URI'] . '/images/logo.png';  
$pageType        = isset($GLOBALS["pageType"]) && !isNullOrEmpty($GLOBALS["pageType"]) ? $GLOBALS["pageType"] : 'website'; 
$ogTitle         = isset($GLOBALS["ogTitle"]) && !isNullOrEmpty($GLOBALS["ogTitle"]) ? $GLOBALS["ogTitle"] : $pageTitle; 
$ogDescription   = isset($GLOBALS["ogDescription"]) && !isNullOrEmpty($GLOBALS["ogDescription"]) ? $GLOBALS["ogDescription"] : $pageDescription;
$pageHeader      = isset($GLOBALS["pageHeader"]) ? $GLOBALS["pageHeader"] : ''; 
$pageBreadCrumbs = isset($GLOBALS["pageBreadCrumbs"]) ? $GLOBALS["pageBreadCrumbs"] : ''; 
$pageHeaderImage = isset($GLOBALS["pageHeaderImage"]) ? $GLOBALS["pageHeaderImage"] : '/images/logo.png';
$pageCssTags     = isset($siteConfig["CSS"]) ? "<style>" . $siteConfig["CSS"] . "</style>" : "";

if(isset($GLOBALS["cssTags"])){
    if(is_array($GLOBALS["cssTags"])) {
        foreach($GLOBALS["cssTags"] as $cssTag) {
            $pageCssTags .= '<link rel="stylesheet" href="' . autoCreateVerison($cssTag, "return") . '">';
        }    
    } else {
        $pageCssTags = '<link rel="stylesheet" href="' . autoCreateVerison($cssTag, "return") . '">';
    }
}

// Add Company Name to Title
$pageTitle = $pageTitle . ' | ' . $siteConfig['Company_Name']; 

$headTags = ''; 
if(!isNullOrEmpty($pageKeywords)) { 
    $headTags .= '<meta name="Keywords" content="' . htmlspecialchars(str_truncate($pageKeywords, 155, ""), ENT_QUOTES) . '">' . PHP_EOL; 
}
if(!isNullOrEmpty($pageDescription)) { 
    $headTags .= '<meta name="Description" content="' . htmlspecialchars(str_truncate($pageDescription, 155, ""), ENT_QUOTES) . '">' . PHP_EOL;  
}
if(!isNullOrEmpty($ogTitle)) { 
    $headTags .= '<meta property="og:title" content="' . htmlspecialchars(str_truncate($ogTitle, 100, ""), ENT_QUOTES) . '">' . PHP_EOL; 
}
if(!isNullOrEmpty($ogDescription)) {  
    $headTags .= '<meta property="og:description" content="' . htmlspecialchars(str_truncate($ogDescription, 200, ""), ENT_QUOTES)  . '">' . PHP_EOL; 
}
if(!isNullOrEmpty($pageUrl)) { 
    $headTags .= '<meta property="og:url" content="' . $pageUrl . '">' . PHP_EOL; 
}
if(!isNullOrEmpty($pageImage)) { 
    $headTags .= '<meta property="og:image" content="' . $pageImage . '">' . PHP_EOL; 
}
if(!isNullOrEmpty($pageType)) { 
    $headTags .= '<meta property="og:type" content="' . $pageType . '">' . PHP_EOL; 
}

$output = ob_get_contents(); 
$output = str_replace('#PAGETITLE#', htmlspecialchars(str_truncate($pageTitle, 69, ""), ENT_QUOTES), $output); 
$output = str_replace('#PAGEHEADER#', htmlspecialchars(str_truncate($pageHeader, 255, ""), ENT_QUOTES), $output); 
$output = str_replace('#PAGEHEADERIMAGE#', htmlspecialchars(str_truncate($pageHeaderImage, 255, ""), ENT_QUOTES), $output); 
$output = str_replace('#PAGEBREADCRUMBS#', $pageBreadCrumbs , $output);
$output = str_replace('#HEADTAGS#', $headTags , $output);
$output = str_replace('#CSSTAGS#', $pageCssTags , $output);
$output = preg_replace('/#[a-zA-Z]+#/','',$output);
ob_end_clean();
echo $output;
?>