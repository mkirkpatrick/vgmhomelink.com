<?php 
ob_start("ob_gzhandler");

require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php"); 

// TWITTER VARIABLES
$twitterFeedActive         = 'No'; // Yes OR No 
$twitterDisclaimer         = 'Yes'; // Yes OR No	
$maxCharacters             = 140;
$limit                     = 2;
$twitterName 			   = ''; // EXAMPLE: Fotobombdolls
$oauth_access_token        = ''; // EXAMPLE: 2520805400-mskmLVzafQ217RPwds4HS8zhW6tyKCYdHDMqxDD
$oauth_access_token_secret = ''; // EXAMPLE: WfJHjp6Tc0QzgZyzmjtI0qjZnE5oG5B0V1CNlUcIYYC9d
$consumer_key              = ''; // EXAMPLE: Y80m8LK7iYMJiFX23YprvYE2F
$consumer_secret           = ''; // EXAMPLE: hOhoGXCUQQmDY9sDjS9uezfjWXsuYcyEvmu2l0fpr3Vk2i1fJs

// FACEBOOK VARIABLES
$facebookFeedActive    	   = 'No'; // Yes OR No     
$facebookDisclaimer        = 'Yes'; // Yes OR No	
$facebookFeedLimit         = 14; 
$facebookShowPostCount     = 1;
$maxConnAttempts           = 10; 
$maxCharacters             = 110;
$facebookShowImages	   	   = true; 
$facebookShowDate          = true; 
$facebookName              = ''; // EXAMPLE: raccoonvalleybank 
$pageId                    = 'YOUR_PAGE_ID_HERE'; // EXAMPLE: 243112972393998 ~~ FIND YOUR FB ID AT THIS LINK: http://findmyfacebookid.com/
$accessKey                 = 'YOUR_ACCESS_TOKEN_HERE'; // EXAMPLE: 536190589847095|HtqX-jAYyCuTuuajKmvboMw_ECk
$cAttempts                 = 0;

// BLOG VARIABLES
$blogFeedActive            = 'No'; // Yes OR No
$blogFeedUrl               = ''; // EXAMPLE: http://blog.raccoonvalleybank.com/feed
$blogDisclaimer		       = 'Yes'; // Yes OR No	
$blogFeedLimit		       = 1;

$cache_file = $_SERVER['DOCUMENT_ROOT'] . '/uploads/cache/Social-Feeds.cache'; 
$cache_duration = 60 * 120; // 2 hours
if (file_exists($cache_file) && (filemtime($cache_file) > (time() - $cache_duration )) || (get_url_status($blogFeedUrl) != 200)) {
    readfile($cache_file); 
} 
else { ?>    
    <script type="text/javascript">
        $(function () {
            // add the class to all external links
            $("a[href*='http://'], a[href*='https://']").not("[data-nodisclaimer='true']").not(".nodisclaimer").each(function () {
                var uri = new URI($(this).attr('href').toLowerCase());
                var url = $(this).attr('href').toLowerCase();
                if (domains.toLowerCase().indexOf(uri.authority) < 0 && url.indexOf('disclaimer?') == -1) { 
                    $(this).addClass('triggerDisclaimer');
                    $(this).attr("target", "_self"); 
                    $(this).attr("href", "/disclaimer?url=" + $(this).attr("href"));  
                } 
            });
        });
    </script> <?php

    // GET AND PRINT TWITTER DATA
    if($twitterFeedActive == 'Yes') {
        $twDisclaimer = strtolower($twitterDisclaimer) == 'no' ? "data-nodisclaimer=\"true\"" : "";
        $tweets = getUserTweets($oauth_access_token, $oauth_access_token_secret, $consumer_key, $consumer_secret, $limit);
        foreach ($tweets as $tweet) {
            echo '<li><a '.$twDisclaimer.' target="_blank" href="https://twitter.com/'. $twitterName . '">' . (substr($tweet, 0, $maxCharacters)) . '</a></li>' . PHP_EOL;
        }
    }

    // GET AND PRINT FACEBOOK DATA
    if($facebookFeedActive == 'Yes') {
        $fbDisclaimer = strtolower($facebookDisclaimer) == 'no' ? "data-nodisclaimer=\"true\"" : "";
        print '<li class="facebook">'; 
        while((!$FBpage = curl_get_contents('https://graph.facebook.com/' . $pageId . '/posts?access_token=' .$accessKey . '&limit=' . $facebookFeedLimit)) && ($cAttempts < $maxConnAttempts)) {
            $cAttempts++; 
        }
        $FBdata = json_decode($FBpage);
        foreach ($FBdata->data as $news ) {
            if($facebookShowPostCount == 0) break;
            $StatusID = explode("_", $news->id);
            $msg = '';
            if(!empty($news->message) && isset($news->status_type)){ 
                $facebookShowPostCount--; $msg = $news->message;
            }
            else {
                continue;
                if(!empty($news->story)) {
                    $msg = $news->story;
                }
            }

            $newsString = strlen($msg) > $maxCharacters ? substr($msg, 0, $maxCharacters) . "&hellip;" : $msg; 

            if(isset($news->link)) {
                $link = $news->link; 
            } else {
                $link = 'http://www.facebook.com/pages/' . $facebookName . '/' . $pageId; 
            }

            print '<li>';  
            if(isset($news->picture) && $facebookShowImages) { 
                print '<span class="fbImage"><img src="' . $news->picture . '" /></span>'; 
            }
            if($facebookShowDate) { 
                print '<span class="fbDate">' . date("F j, Y", strtotime($news->created_time)) . "</span>";
            } 
            print '<span class="fbPost"><a '.$fbDisclaimer.' target="_blank" href="' . $link . '">' . $newsString . '</a></span>' . PHP_EOL;
            print '</li>'; 
        }
    }

    // GET AND PRINT BLOG DATA
    if($blogFeedActive == 'Yes') {
        $blDisclaimer = strtolower($blogDisclaimer) == 'no' ? "data-nodisclaimer=\"true\"" : "";
        print '<li class="wordpress">';
        $posts = getWPPosts($blogFeedUrl, $blogFeedLimit);
        foreach($posts as $blogPost) {
            print '<a '.$blDisclaimer.' target="_blank" href="' . $blogPost->link . '">' . str_truncate($blogPost->title, 80) . '</a>';
        } 
        print '</li>';
    }                     
    file_put_contents($cache_file, ob_get_contents(), LOCK_EX);
}  
?>