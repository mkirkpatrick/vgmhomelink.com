<?php global $_WEBCONFIG; ?>

<ul class="site-options nobullets">
    <li class="title"><span class="siteOptions">Site Options:</span></li>
    <li><a class="print pointer" onclick="window.print()" title="Print Page"><img src="/images/icon-print.png" alt="Print" /></a></li>
    <li class=""><a id="email-button" class="email" title="Send to a Friend" href="mailto:?subject=#PAGETITLE#&body=<?= $_WEBCONFIG['SITE_URL'] . $_SERVER['REQUEST_URI'] ?>"><img src="/images/icon-email.png" alt="Email" /></a></li>
    <li class="socialShare last">
        <img src="/images/icon-share.jpg" alt="Share" />
        <ul class="socials clearfix">
            <li class="facebook" id="FaceBookShare">
                <a class="socialLink" title="Share on Facebook" onclick="showSocialPopup('http://www.facebook.com/share.php?u=<?= $_SERVER['ABSOLUTE_URI'] ?>', 700,400)">Facebook</a>
            </li>
            <li class="twitter" id="TwitterShare">
                <a class="socialLink" title="Tweet" onclick="showSocialPopup('http://twitter.com/intent/tweet?url=<?= $_SERVER['ABSOLUTE_URI'] ?>', 600, 500);">Twitter</a>
            </li>
            <li class="linkedin" id="LinkedInShare">
                <a class="socialLink" onclick="showSocialPopup('http://www.linkedin.com/shareArticle?mini=true&amp;url=<?= $_SERVER['ABSOLUTE_URI'] ?>', 600, 500);">LinkedIn</a>
            </li>
            <li class="google" id="GooglePlusShare">
                <a class="socialLink" onclick="showSocialPopup('https://plus.google.com/share?url=<?= $_SERVER['ABSOLUTE_URI'] ?>', 600, 500);">GooglePlus</a>
            </li>
            <li class="pinterest" id="PinterestShare">
                <a class="socialLink" onclick="showSocialPopup('http://pinterest.com/pin/create/bookmarklet/?url=<?= $_SERVER['ABSOLUTE_URI'] ?>', 600, 500);">Pinterest</a>
            </li>
        </ul>
    </li>

    <li class="textResizer"><a class="smlFnt resizerButton pointer" data-style="text-normal">A</a></li>
    <li class="textResizer"><a class="mdmFnt resizerButton pointer" data-style="text-medium">A</a></li>
    <li class="textResizer last-text"><a class="lrgFnt resizerButton pointer" data-style="text-large">A</a></li>
</ul>