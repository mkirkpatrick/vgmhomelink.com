<ul class="footer-menu clearfix nobullets">
    <li><a href="/">Home</a></li>
    <?php
    $sql = "SELECT cms_id, cms_title, cms_dynamic_link, cms_dynamic_link_target FROM tbl_cms 
            WHERE cms_is_historic = 0 and cms_is_deleted = 0 and cms_is_active = '1' 
            AND cms_show_nav = '1' AND cms_parent_id = '". $_WEBCONFIG['SITE_ROOT_PAGE']."' ORDER BY cms_display_order LIMIT " . ($_WEBCONFIG['SITE_TYPE'] == 'MICROVOX' ? $_WEBCONFIG['PAGEBUILDER_PAGE_LIMIT'] - 1 : $_WEBCONFIG['MAIN_NAV_LIMIT']) . ";";
    $menuItems = Database::Execute($sql); 
    while ($menuItems->MoveNext()) {
        $navLink		= "javascript:return(false);";
        $subNavigation	= ""; 
        $navLink = trim($menuItems->cms_dynamic_link);
        $class = isset($iNum) && $iNum == $menuItems->cms_id ? " current" : "";
        printf("<li id=\"footerNavItem{$menuItems->cms_id}\" class=\"footerNavItem\"><a href=\"%s\" class=\"%s\" target=\"%s\">%s</a></li>", $navLink, "footerNav" . $menuItems->cms_id . $class, $menuItems->cms_dynamic_link_target, $menuItems->cms_title);
    }//end while
    ?>
</ul><!-- eof navigation -->