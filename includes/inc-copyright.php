<footer class="footer">
	<div class="footer-links">
		<div class="container clearfix">
			<div class="footer-lg">
				<div class="container">
					<div class="floatLeft"><a href="/" class="logo-white floatLeft" title="<?= $siteConfig['Company_Name'] ?>"><img src="/images/logo.png" alt="<?= $siteConfig['Company_Name'] ?>" /></a></div>
					<div class="floatRight">
					<ul class="nobullets">
						<li class="footerNavItem"><a href="/" title="Links to our Home Page">Home</a></li>
						<li id="footerNavItem26" class="footerNavItem"><a href="/about-us" title="Links to our About Us page">About Us</a></li>
						<li id="footerNavItem2" class="footerNavItem"><a href="/contact-us" title="Links to our Contact Us page">Contact Us</a></li>
						<li id="footerNavItem115" class="footerNavItem"><a href="/member" title="Links to our Portal Login page" >Portal Login</a></li>
					</ul>
					</div>
				 </div>
			</div>
		</div>
			<div class="container clearfix">
			<div class="fourcol">
				<p>HOMELINK is a proud, employee-owned company.  As employee owners, associates are naturally motivated to help the organization, clients and members achieve great outcomes.</p>
				<p>HOMELINK's mission is to provide access to high quality, cost effective products, vendors and services via an efficient, effective and caring national integrated delivery system.</p>
			</div>
			<div class="twocol footer-social">
				<ul class="social-icons nobullets">
					<li><a href="https://www.facebook.com/Homelink1993/" class="facebook" title="Link opens HOMELINK's Facebook page in a new window" target="_blank">Facebook</a></li>
					<li><a href="https://twitter.com/VGMHomelink" class="twitter" title="Link opens HOMELINK's Twitter page in a new window" target="_blank">Twitter</a></li>
					<li><a href="https://www.linkedin.com/company-beta/1408450/" class="linkedin" title="Link opens HOMELINK's Linkedin page in a new window" target="_blank">LinkedIn</a></li>
				</ul>
			</div>
			<div class="threecol">
				<ul class="copyright nobullets">
					<li><a href="http://careers.vgm.com" target="_blank" title="Link opens VGM Careers website in a new window">Careers</a></li>
					<li><a href="http://www.vgmgroup.com" target="_blank" title="Link opens vgmgroup.com in a new window">VGM Group, Inc.</a></li>
					<li><a href="http://www.vgm.com" target="_blank" title="Link opens up vgm.com in a new window">VGM &amp; Associates</a></li>
					<li><a href="/site-map" title="link takes you to the Site Map of this website">Site Map</a></li>
				</ul>
			</div>
			<div class="threecol last">
				<ul class="copyright nobullets">
					<li><a href="/privacy-policy" title="Link opens the notice of privacy practices/privacy complaints">Notice of Privacy Practices/Privacy Complaints</a></li>
					<li><a href="/docs/non-discrimination-notice.pdf" title="Link opens the HOMELINK Non-discrimination Policy PDF in a new window" target="_blank">Non-Discrimination Policy</a></li>
                    <li><a href="/compliance-hotline" title="Link opens the Compliance Hotline in a new window" target="_blank">Compliance Hotline</a></li>
                    <li><a href="https://issuu.com/vgmhomelink/docs/website_user_guide_2020?fr=sZTg4OTEzMjA4NjY" title="Link opens the website user guide in a new window" target="_blank">Website User Guide</a></li>
				</ul>
			</div>
			</div>
	</div>
	<div class="clearfloat"></div>
	<div class="footer-notice">
		<div class="container">
			<p class="copyright">&#169; <?= date('Y'); ?>  <?= $siteConfig['Company_Name'] ?>. All Rights Reserved.</p>
		</div>
	</div>
	<div class="modal overlay none dynamic-modal" id="alertModal"></div>
    <div class="overlay-mask"></div>
</footer><!-- eof footer -->

	<a href="#Top" class="back-to-top none"><img src="/images/toTop.png" alt="Back to Top" title="Link that scrolls you back to the top of this page"/></a>
	<div class="overlay-mask"></div>
</div><!-- eof pagewrap -->