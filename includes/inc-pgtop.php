<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/config.php');
if($_WEBCONFIG['ENABLE_GZIP_PAGE_COMPRESSION'] == "true") {
    ob_start("ob_gzhandler");
} else {
    ob_start();
}
$page = xss_sanitize(htmlEntities(str_replace('/', ' ', str_replace(array('.php', '?s='), '', ltrim($_SERVER['REQUEST_PATH'], '/'))), ENT_QUOTES));

if($_SERVER['PHP_SELF'] == '/index.php') {
    define('PAGE_CLASS', 'index');
}
else {
    define('PAGE_CLASS', !isNullOrEmpty($page) ? $page : 'index');
}
?>

<!DOCTYPE html>
<!--[if IE 8]><html xmlns="http://www.w3.org/1999/xhtml" class="no-js ie8" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html xmlns="http://www.w3.org/1999/xhtml" class="no-js" lang="en"><!--<![endif]-->
<head>
    <title>#PAGETITLE#</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1">
    #HEADTAGS#
    <meta name="Author" content="<?= $siteConfig['Company_Name'] ?>">
    <meta name="Generator" content="Forbin PageBuilder 2.0">
    <?php // Google WEBMASTER TOOLS Script
    if(isset($siteConfig['Google_Webmaster_Tools_Script']) && strlen($siteConfig['Google_Webmaster_Tools_Script']) > 0 && $siteConfig['Google_Webmaster_Tools_Script'] != '<meta></meta>') {
        print($siteConfig['Google_Webmaster_Tools_Script']);
    }  ?>

    <link rel="dns-prefetch" href="ajax.googleapis.com">
    <link rel="dns-prefetch" href="cdn.forbin.com">
    <link rel="dns-prefetch" href="kendo.cdn.telerik.com">

	<link rel="apple-touch-icon" sizes="152x152" href="/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/images/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/images/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/images/manifest.json">
	<link rel="mask-icon" href="/images/safari-pinned-tab.svg" color="#003594">
	<link rel="shortcut icon" href="/images/favicon.ico">
	<meta name="msapplication-config" content="/images/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">

    <link rel="canonical" href="<?= urlPathCombine($_WEBCONFIG['SITE_URL'], $_SERVER['REQUEST_URI']) ?>">
    <link rel="stylesheet" href="<?php autoCreateVerison('/css/global.css'); ?>">
    <link rel="stylesheet" href="<?php autoCreateVerison('/css/main.css'); ?>">
	#CSSTAGS#

   	<noscript id="deferred-styles">
   		<link rel="stylesheet" href="<?php autoCreateVerison('/css/aos.css'); ?>">
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
		<link rel="stylesheet" href="//kendo.cdn.telerik.com/2017.2.621/styles/kendo.common-fiori.min.css">
		<link rel="stylesheet" href="//kendo.cdn.telerik.com/2017.2.621/styles/kendo.fiori.min.css">
		<link rel="stylesheet" href="//kendo.cdn.telerik.com/2017.2.621/styles/kendo.fiori.mobile.min.css">
	</noscript>

 	<!-- Typekit -->
    <script src="https://use.typekit.net/dgv8csw.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha384-ZvpUoO/+PpLXR1lu4jmpXWu80pZlYUAfxl5NsBMWOEPSjUn/6Z/hRTt8+pR6L4N2" crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="/css/ieonly.css">
    <script src="//cdn.forbin.com/global/jquery/html5shiv.js"></script>
    <script src="//cdn.forbin.com/global/jquery/respond.min.js"></script>
    <![endif]-->

    <!-- <link rel="stylesheet" type="text/css" href="/css/addtohomescreen.css">
		<script src="/scripts/addtohomescreen.min.js"></script>
		<script>addToHomescreen()</script>
    -->

<script type="text/javascript">
var _ss = _ss || [];
_ss.push(['_setDomain', 'https://koi-3QNO0K3PWU.marketingautomation.services/net']);
_ss.push(['_setAccount', 'KOI-4CNQHQDR2G']);
_ss.push(['_trackPageView']);
(function() {
var ss = document.createElement('script');
ss.type = 'text/javascript'; ss.async = true;
ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QNO0K3PWU.marketingautomation.services/client/ss.js?ver=2.4.0';
var scr = document.getElementsByTagName('script')[0];
scr.parentNode.insertBefore(ss, scr);
})();
</script>

</head>
<body id="Top" class="<?= PAGE_CLASS ?>">
<?php if(isset($siteConfig['Google_Tag_Manager_Code']) && strlen($siteConfig['Google_Tag_Manager_Code']) > 0 && $_WEBCONFIG['SITE_STATUS'] != 'STAGING') {  ?>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=<?= $siteConfig['Google_Tag_Manager_Code'] ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?= $siteConfig['Google_Tag_Manager_Code'] ?>');</script>
    <!-- End Google Tag Manager -->
    <?php } ?>

<nav class="mobileonly mobilemenu" aria-label="Mobile Menu">
    <ul class="nobullets">
        <li class="home"><a href="/">Home</a></li>
        <?php
        // Main Navigational Menu
        $sql = "SELECT cms_id, cms_title, cms_sub_title, cms_dynamic_link, cms_dynamic_link_target FROM tbl_cms
                WHERE cms_is_historic = 0 and cms_is_deleted = 0 and cms_is_active = '1'
                AND cms_show_nav = '1' AND cms_parent_id = '". $_WEBCONFIG['SITE_ROOT_PAGE']."' ORDER BY cms_display_order LIMIT " . $_WEBCONFIG['MAIN_NAV_LIMIT'] . ";";
        $menuItems = Database::Execute($sql);
        while ($menuItems->MoveNext()) {
            $navLink		= "javascript:return(false);";
            $subNavigation	= "";
            $title			= $menuItems->cms_title;
            $tooltip		= null;
            if(!isNullOrEmpty($menuItems->cms_sub_title) && $_WEBCONFIG['TOP_LEVEL_NAV_SUBTITLES'] == "DIRECT" || $_WEBCONFIG['TOP_LEVEL_NAV_SUBTITLES'] == "BOTH") {
                $title.= '<span>' . $menuItems->cms_sub_title . '</span>';
            }
            if($_WEBCONFIG['TOP_LEVEL_NAV_SUBTITLES'] == "BOTH" || $_WEBCONFIG['TOP_LEVEL_NAV_SUBTITLES'] == "TOOLTIP") {
                $tooltip = $menuItems->cms_sub_title;
            }
            printf("<li id=\"mobileNavItem{$menuItems->cms_id}\" data-navPgId=\"" . $menuItems->cms_id . "\" class=\"menuItem\"><a href=\"%s\" class=\"%s\" target=\"%s\" title=\"Mobile link for - %s\">%s</a>", $menuItems->cms_dynamic_link, "mobileNav" . $menuItems->cms_id, $menuItems->cms_dynamic_link_target, $title, $title);
            printMobileSubMenu($menuItems->cms_id);
        }//end while
        ?>
        <li class="search"><a href="/search">Search</a></li>
    </ul>
</nav>

<div class="pagewrap clearfix">