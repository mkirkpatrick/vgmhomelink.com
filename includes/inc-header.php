<header class="masthead">
    <a href="#content-anchor" class="skip-to-content reader-focusable">Skip to Content</a>

    <div class="container clearfix relative">
        <a href="/" class="logo floatLeft" title="<?= $siteConfig['Company_Name'] ?>"><img src="/images/logo.png" title="Link to HOMELINK's home page" alt="<?= $siteConfig['Company_Name'] ?>" /></a>
        
        <ul class="secondary-nav nobullets">
        	<li><a href="/members/registration" title="">Open an Account</a></li>
        	<li><a href="/about-us" title="">About Us</a></li>
        	<li><a href="/contact-us" title="">Help</a></li>
        	<li><p class="phone-number">
                <span class="normal-phone"><?= $siteConfig['Contact_Phone']; ?></span>
                <span class="special-phone">888-501-3591</span>
            </p></li>
        	<li><? require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/inc-search.php'; ?></li>
<!--            <a href="/login" class="button green large-only">Login</a>-->
        </ul>
    </div>
</header><!-- eof header -->