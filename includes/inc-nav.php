<?php require_once $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . "pagebuilder/classes/class_helper.php"; ?>
<?php if($_WEBCONFIG['SITE_TYPE'] != "LANDING-PAGE") {  ?>

<nav class="menu" aria-label="Main Navigation">
    <div class="container clearfix">
    	<a href="#" class="openmobile mobileonly"><span>Menu</span></a>
        <ul class="navigation nobullets large-only">
            <?php
            // Main Navigational Menu
            $sql = "SELECT cms_id, cms_title, cms_sub_title, cms_dynamic_link, cms_show_disclaimer, cms_dynamic_link_target FROM tbl_cms 
                    WHERE cms_is_historic = 0 and cms_is_deleted = 0 and cms_is_active = '1' 
                    AND cms_show_nav = '1' AND cms_parent_id = '". $_WEBCONFIG['SITE_ROOT_PAGE']."' ORDER BY cms_display_order LIMIT " . ($_WEBCONFIG['SITE_TYPE'] == 'MICROVOX' ? $_WEBCONFIG['PAGEBUILDER_PAGE_LIMIT'] - 1 : $_WEBCONFIG['MAIN_NAV_LIMIT']) . ";";
            $menuItems = Database::Execute($sql); 
            while ($menuItems->MoveNext()) {
                $navLink		= "javascript:return(false);";
                $subNavigation	= ""; 
                $title			= $menuItems->cms_title; 
                $tooltip		= null; 
                $class          = isset($iNum) && $iNum == $menuItems->cms_id ? " current" : "";
                if(!isNullOrEmpty($menuItems->cms_sub_title) && $_WEBCONFIG['TOP_LEVEL_NAV_SUBTITLES'] == "DIRECT" || $_WEBCONFIG['TOP_LEVEL_NAV_SUBTITLES'] == "BOTH") {
                    $title.= '<span>' . $menuItems->cms_sub_title . '</span>'; 
                }
                if($_WEBCONFIG['TOP_LEVEL_NAV_SUBTITLES'] == "BOTH" || $_WEBCONFIG['TOP_LEVEL_NAV_SUBTITLES'] == "TOOLTIP") {
                    $tooltip = $menuItems->cms_sub_title; 
                }

                if($menuItems->cms_show_disclaimer == 1) {
                    printf("<li id=\"headerNavItem{$menuItems->cms_id}\" data-navPgId=\"" . $menuItems->cms_id . "\" class=\"menuItem\"><a href=\"%s\" class=\"%s\" target=\"%s\" title=\"%s\">%s</a>", $menuItems->cms_dynamic_link, "headerNav" . $menuItems->cms_id . $class, $menuItems->cms_dynamic_link_target, $title, $title);
                    printSubMenu($menuItems->cms_id);
                }
                else {
                    printf("<li id=\"headerNavItem{$menuItems->cms_id}\" data-navPgId=\"" . $menuItems->cms_id . "\" class=\"menuItem\"><a href=\"%s\" class=\"%s\" target=\"%s\" data-nodisclaimer=\"true\" title=\"%s\">%s</a>", $menuItems->cms_dynamic_link, "headerNav" . $menuItems->cms_id . $class, $menuItems->cms_dynamic_link_target, $title, $title);
                    printSubMenu($menuItems->cms_id);    
                }
            }//end while
            ?>
        </ul><!-- eof navigation -->
    </div>
</nav><!-- eof menu -->

<?php } ?>