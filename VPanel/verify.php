<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");
    require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
    
    security::_secureCheck();

    if(UserManager::isWebmaster()) {
        redirect($_WEBCONFIG['VPANEL_PATH']);
    }
    $sql = "SELECT uq1.uq_question AS question1, uq2.uq_question AS question2, uq3.uq_question AS question3
            FROM `tbl_user` AS u
            INNER JOIN tbl_user_questions AS uq1 ON u.`user_password_question_1_id` = uq1.uq_id
            INNER JOIN tbl_user_questions AS uq2 ON u.`user_password_question_2_id` = uq2.uq_id
            INNER JOIN tbl_user_questions AS uq3 ON u.`user_password_question_3_id` = uq3.uq_id
            WHERE user_id = {$_SESSION['user_id']}
            LIMIT 1;";
	$record = Database::Execute($sql);
    if($record->Count() > 0) {
        $record->MoveNext();
        $numbers = range(1, 3);
        shuffle($numbers);
        $qIndex = array_slice($numbers, 0, 2);
    } else {
        throw new Exception("User Security Questions Not Found");
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="robots" content="noindex,nofollow" />
        <title>VPanel :: Verify Identity</title>
        <link type="image/x-icon" href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>favicon.ico" rel="icon" />
        <link href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>apple-touch-icon.png" rel="apple-touch-icon" />
        <link type="text/css" href="css/vpanel.css" rel="stylesheet" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <!--[if lt IE 9]>
        <script src="<?= $_WEBCONFIG['VPANEL_PATH'] ?>jquery/modernizr.js"></script>
        <![endif]-->
    </head>
    <body>

        <header class="masthead">
            <div class="utility-bar">
                <div class="container clearfix">
                    <a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>" class="logo"><img src="/vpanel/images/logo.png" alt="Forbin VPanel"></a>
                </div>
            </div>
        </header>

        <div class="container clearfix">
            <div class="login" style="max-width: 500px">
                <form id="secureForm" method="post" action="process.php" autocomplete="off">
                <input type="hidden" name="actionRun" value="questions" />
                <input type="hidden" name="hidReturnUrl" id="hidReturnUrl" value="<?= isset($_GET['ReturnUrl']) ? htmlentities($_GET['ReturnUrl'], ENT_QUOTES, "utf-8") : '';  ?>"  ?>
                <input type="hidden" name="csrfToken" value="<?= $_SESSION['user_csrf_token'] ?>" />
                <h1>Answer Security Questions</h1>
                <h3>You must verify your identity before continuing, please answer the security questions below.</h3>
                <p><b>Note:</b> Answer passphrases are case sensative and may include special characters and spaces. </p>
                <?php
                if (isset($_SESSION['errMsg'])) {
                    $errorMessage = $_SESSION['errMsg'];
                    print "<div class=\"error2\">$errorMessage</div>\n";
                    unset($_SESSION['errMsg']);
                }
                ?>
                <div id="success" class="success message notification none"></div>
                <ul class="form">
                    <li>
                        <label><?= $record->{"question{$qIndex[0]}"} ?></label>
                        <input type="password" name="txtQuestion<?= $qIndex[0] ?>" id="txtQuestion<?= $qIndex[0] ?>" required="required" autocomplete="off" data-parsley-required-message="Enter The Answer To Your Question." />
                    </li>
                    <li>
                        <label>
                            <?= $record->{"question{$qIndex[1]}"} ?>
                        </label>
                        <input type="password" name="txtQuestion<?= $qIndex[1] ?>" id="txtQuestion<?= $qIndex[1] ?>" required="required" autocomplete="off" data-parsley-required-message="Enter The Answer To Your Question." />
                    </li>
                </ul>
			<div style="padding: 5px; text-align: right">
				<input id="cboShowHide" type="checkbox" /><label for="cboShowHide">Show Answers</label>
			</div>
                <div>
                    <input type="submit" id="login" name="sendLogin" value="Continue" class="button block green submit floatRight" style="display: inline;" />
                </div>

                <div>
                    <p><br><a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>?logout" class="floatLeft">&larr; Back to Login Page</a></p>
                </div>
            </div>
        </div>
        <link rel="stylesheet" href="/css/parsley.css" type="text/css" />
        <script type="text/javascript" src="/scripts/parsley.min.js"></script>
        <script src="<?= $_WEBCONFIG['VPANEL_PATH'] ?>jquery/pages/verify.js"></script>

    </body>
</html>