<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>403 Forbidden | VPanel</title>
<link type="text/css" href="/vpanel/css/vpanel.css" rel="stylesheet" />
<link href="//fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
</head>
<body>

<header class="masthead" role="banner">
	<div class="utility-bar">
        <div class="container clearfix">
            <a href="/VPanel/" title="Control Panel Home" class="logo"><img src="/VPanel/images/logo.png" style="border: 0" alt="VGM Forbin VPanel Administration" class="back"></a>
        </div>
    </div>
</header>

<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-thumbs-down">403 Forbidden</h1>
        <p class="page-desc">You do not have sufficient permissions to access this page</p>
    </div>
</section>

<section class="maincontent">
    <div class="container clearfix">
        <div class="fullcontent">
          <h2>You do not have sufficient permissions to access this page</h2>
          <p>This server could not verify that you are authorized to access the document requested. <br />
		  Either you supplied the wrong credentials (e.g., bad password) or your account does not have the proper permissions to view this document.
		  </p>
          <a class="button silver" href="/VPanel" title="Back to home page">Back to VPanel Home</a>
        </div>
    </div>
</section>

</body>
</html>