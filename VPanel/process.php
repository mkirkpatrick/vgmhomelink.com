<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
require_once 'library/classes/class_visitor.php';

class Process {

	//------------------------------
	public function _login() {
     //------------------------------
        if(validateRC()) {
            $_SESSION['errMsg'] = security::_secureUnlock();
        }
        redirect("/VPanel/login.php");

	}//end function

	//------------------------------
	public function _forgotPassword() {
     //------------------------------
        if(validateRC()) {
			echo security::_ForgotPass();
        } else {
			echo 'Unknown Error Occurred';
		}
		exit;
	}//end function

    //------------------------------
	public function _questions() {
    //------------------------------
        global $_WEBCONFIG;

        if(!isset($_SESSION['user_id'])) {
            redirect("/VPanel/login.php");
        }

        if(!isset($_REQUEST['csrfToken']) || $_REQUEST['csrfToken'] != $_SESSION['user_csrf_token'] )  {
            serverTransfer("/vpanel/401error.php");
        }

        $_SESSION['errMsg'] = null;
        $user = Repository::GetById('tbl_user', $_SESSION['user_id']);

        if(isset($_POST['txtQuestion1'])) {
            if($user->user_password_answer_1 != Security::_hashPassword($_POST['txtQuestion1'], $user->user_password_answer_salt)) {
                $_SESSION['errMsg'] = "One or both of your answers does not match the identify information we have on file. ";
            }
        }

        if(isset($_POST['txtQuestion2'])) {
            if($user->user_password_answer_2 != Security::_hashPassword($_POST['txtQuestion2'], $user->user_password_answer_salt)) {
                $_SESSION['errMsg'] = "One or both of your answers does not match the identify information we have on file. ";
            }
        }

        if(isset($_POST['txtQuestion3'])) {
            if($user->user_password_answer_3 != Security::_hashPassword($_POST['txtQuestion3'], $user->user_password_answer_salt)) {
                $_SESSION['errMsg'] = "One or both of your answers does not match the identify information we have on file. ";
            }
        }
        if(isNullOrEmpty($_SESSION['errMsg'])){
            $_SESSION['user_req_question_auth'] = false;
            Security::_addAuditEvent("Security Questions", "Success", "Successfully Answered", $user->user_username);
            redirect($_WEBCONFIG['VPANEL_PATH']);
        } else {
            $userAttempts = $user->user_attempts + 1;
            Security::_addAuditEvent("Security Questions", "Failure", "Invalid Security Question Answer(s)", $user->user_username);
            $sql = "UPDATE `tbl_user` SET `user_last_attempt` = NOW(), `user_attempts` = $userAttempts WHERE `user_id` = {$user->user_id}";
            Database::ExecuteRaw($sql);
            if($userAttempts >= $_WEBCONFIG['MAX_INVALID_PASSWORD_ATTEMPTS']) {
                $sql = "UPDATE `tbl_user` SET `user_status` = 'Locked' WHERE `user_id` = {$user->user_id}";
                Database::ExecuteRaw($sql);
                $auditDetails = "User account has been locked out due to too many incorrect security question attempts. ";
                Security::_addAuditEvent("Account Locked", "Success", $auditDetails, $user->user_username);
                $_SESSION['errMsg'] = "Sorry, but you have made too many unsuccessful attempts to verify your identity. please contact an administrator.  ";
                redirect("/VPanel/login.php");
            } else {
                redirect("/VPanel/verify.php");
            }
        }

	}//end function

}

if(!isset($_REQUEST['actionRun']))  {
	header("HTTP/1.0 405 Method Not Allowed");
	header("Allow", "POST, GET");
	redirect('index.php');
}

$p = new Process();
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {
	case 'login' :
		$p->_login();
        break;

	case 'questions' :
		$p->_questions();
        break;

	case 'forgotPassword' :
		$p->_forgotPassword();
		break;
}

$_SESSION['errMsg'] = security::_secureUnlock();
redirect("/VPanel/login.php")
?>