// Functionality for hiding update message based on cookie values
var updateMessageDiv = document.getElementById('update-message');

if (updateMessageDiv) {
    var closeButton = updateMessageDiv.getElementsByClassName('xButton')[0];

    closeButton.addEventListener('click', function(evt) {
        $('#' + updateMessageDiv.id).slideUp();

        var currentDate = new Date().toLocaleDateString();

        document.cookie = "update-message-dismissed-on = " + currentDate + ";secure";
    })
}