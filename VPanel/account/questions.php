<?php
if (!isset($siteConfig)) die("System Error!");
$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
if ($form->getNumErrors() > 0) {
    $hidId				= $form->value("hidId");
    $question3 			= $form->value("cboQuestion3");
    $question2 			= $form->value("cboQuestion2"); 
    $question1 			= $form->value("cboQuestion1");
    $answer1 			= $form->value("txtAnswer1");
    $answer2 			= $form->value("txtAnswer2");
    $answer3 			= $form->value("txtAnswer3");
} else {
    $hidId				= '';
    $question3 			= ''; 
    $question2 			= '';
    $question1 			= '';
    $answer1 			= '';
    $answer2 			= '';
    $answer3 			= '';
}

function buildQuestionOptions($groupId, $selectedId = 0) {
    $groupId = (int) $groupId; 
	$sql = "SELECT `uq_id`,`uq_question` 
			FROM `tbl_user_questions` 
			WHERE uq_enabled = 1 AND uq_group = $groupId 
			LIMIT 10";
	$record = Database::Execute($sql);
	$list = '';
	while ($record->MoveNext()) {
		$key = $record->uq_id;
        $value = $record->uq_question;
        $list .= "<option value=\"$key\"";
        if ($key == $selectedId) {
            $list .= " selected";
        }
        $list .= ">$value</option>\r\n";
    }
    return $list;
}
if($_GET['view'] == 'newquestions') { 
    showLimitedView(); 
}
?>
<div class="subcontent right last" style="width: 75%"> 
    <form id="userForm" method="post" action="process.php">
        <input type="hidden" name="actionRun" value="changequestion" />
        <input type="hidden" name="view" value="<?= $_GET['view'] ?>" />
        <input type="hidden" name="hidId" value="<?= $_SESSION['user_id']; ?>" />
        <input type="hidden" name="csrfToken" value="<?= Security::getCsrfToken() ?>" />

        <h2>Choose New Security Questions</h2>
        <p>These questions will be used to verify your identity and recover your password if you ever forget it. </p>
        <p><b>Note: </b>All responses should be a phrase of at least <?= $_WEBCONFIG['MIN_PASSWORD_ANSWER_LENGTH'] ?> characters long (including spaces). Each phrase is chase sensitive, and special characters and spaces will be required when entering your phrase. Please select questions to met these requirements. </p>
        <?php
        if ($form->getNumErrors() > 0) {
            $errors	= $form->getErrorArray();
            foreach ($errors as $err) echo $err;
        } else if (isset($_SESSION['processed'])) {
            echo "<script>addNotify('Security Questions Changed Successfully!')</script>";
            echo "<div class=\"message success okMessage\">Security Questions Changed Successfully!</div>\n";
            unset($_SESSION['processed']);
        } ?>
        <table class="grid-display">
            <tr style="background: none">
                <td style="text-align: left; vertical-align: top; background: none">
                    <select name="cboQuestion1" id="cboQuestion1" style="width: 350px" required="required" data-parsley-required-message="Select A Question.">
                        <option value="">-- Choose A Question --</option>
                        <?= buildQuestionOptions(1, $question1); ?>	 
                    </select>&nbsp;&nbsp;
                    <div class="k-textbox" style="width: 400px">
                        <input type="text" name="txtAnswer1" id="txtAnswer1" maxlength="50" style="width: 400px" required="required"
                            autocomplete="off" placeholder=" -- Enter Answer --" value="<?= $answer1 ?>" data-parsley-required-message="Enter An Answer To Question #1."/></div>
                    <?= $form->error("cboQuestion1");?>
                </td>
            </tr>
            <tr style="background: none">
                <td style="text-align: left; vertical-align: top; background: none">
                    <select name="cboQuestion2" id="cboQuestion2" style="width: 350px" required="required" data-parsley-required-message="Select A Question.">
                        <option value="">-- Choose A Question --</option>
                        <?= buildQuestionOptions(2, $question2); ?>	 
                    </select>&nbsp;&nbsp;
                    <div class="k-textbox" style="width: 400px">
                        <input type="text" name="txtAnswer2" id="txtAnswer2" maxlength="50" style="width: 400px" required="required"
                            autocomplete="off" placeholder=" -- Enter Answer --" value="<?= $answer2 ?>" data-parsley-required-message="Enter An Answer To Question #2."/></div>
                    <?= $form->error("cboQuestion2");?>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; vertical-align: top; background: none" >
                    <select name="cboQuestion3" id="cboQuestion3" style="width: 350px" required="required" data-parsley-required-message="Select A Question.">
                        <option value="">-- Choose A Question --</option>
                        <?= buildQuestionOptions(3, $question3); ?>	 
                    </select>&nbsp;&nbsp;
                    <div class="k-textbox" style="width: 400px">
                        <input type="text" name="txtAnswer3" id="txtAnswer3" maxlength="50" style="width: 400px" required="required"
                            autocomplete="off" placeholder=" -- Enter Answer --" value="<?= $answer3 ?>" data-parsley-required-message="Enter An Answer To Question #3."/></div>
                    <?= $form->error("cboQuestion3");?>
                </td>
            </tr>
        </table>
        <div class="buttons clearfix">
            <input name="btnAdd" type="submit" id="btnAdd" value="Save Questions" class="green button floatRight">
        </div><!--End continue-->

    </form>  
</div>
