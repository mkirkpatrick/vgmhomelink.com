<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php");

security::_secureCheck();

if (isset($_GET['logout'])) {
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

if(UserManager::isWebmaster()) {
    redirect($_WEBCONFIG['VPANEL_PATH']);
}

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

print '<section class="title">
		<div class="container clearfix">
			<h1 class="page-title icon-users">Manage Your Account</h1>
			<p class="page-desc">Update account information, change password, and view activity related to your user account.</p>
		</div>
	</section>';
print '<section class="maincontent"><div class="container clearfix">';
include('navBar.php');

$rows = 0;
$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {

	case 'profile' :
        $script    = array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/profile.js');
		include_once 'profile.php';
		break;

	case 'newpassword' :
	case 'expiredpassword' :
	case 'changepassword' :
	    $script	= array('/jquery/pstrength.js',$_WEBCONFIG['COMPONET_FOLDER'] . 'js/password.js');
		include_once 'change-password.php';
		break;

	case 'newquestions' :
	case 'editquestions' :
        $script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/questions.js');
		include_once 'questions.php';
		break;

    case 'logdata' :
        $script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/logdata.js');
		include_once 'logdata.php';
		break;

	default :
		include_once 'account.php';
}

print '</div></section>
<link rel="stylesheet" href="/css/parsley.css" type="text/css" />
<script type="text/javascript" src="/scripts/parsley.min.js"></script>';
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php"); ?>