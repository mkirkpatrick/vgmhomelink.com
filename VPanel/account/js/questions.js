// JavaScript Document
$(function() {
	
	// add * to required field labels
	$('label.required').append('&nbsp;<span style="color: #990000"><b>*</b></span>&nbsp;');		
	
    $('#cboQuestion1').kendoDropDownList();
    $('#cboQuestion2').kendoDropDownList();
    $('#cboQuestion3').kendoDropDownList();

	$("input[type='submit']").click(function (event) {
	    event.preventDefault();
		var myform = '#' + $(this).closest("form").attr('id');
	    var buttonVal = $(this).val();
		$("html").addClass("wait");
	    $(this).val('Saving...');
	    $(this).attr('disabled', 'disabled');
		$(myform).parsley({excluded: "input[type=button], input[type=submit], input[type=reset], input[type=hidden], input:hidden, textarea[type=hidden], textarea:hidden"});		
		if($(myform).parsley().isValid()) {
			$(myform).submit(); 
		}
		else {
			$("html").removeClass("wait");
			$(this).val(buttonVal);
			$(this).removeAttr('disabled');       
			$(myform).parsley().validate();
		}
	});            
});