$(document).ready(function () {
    $("#grid").kendoGrid({
        dataSource: { pageSize: 50 },
        pageable: { refresh: false, pageSizes: [10, 25, 50, 100] },
        sortable: { mode: "multiple", allowUnsort: true },
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",
                    doesnotcontain: "Does Not Contain",
                    startswith: "Starts With",
                    endswith: "Ends With"
                }
            }
        },
        groupable: false,
        columnMenu: false,
        scrollable: false,
        resizable: true,
        reorderable: true,
        detailTemplate: kendo.template($("#template").html()),
        toolbar: kendo.template($("#filterTemplate").html())
    });
    $('#grid').data("kendoGrid").hideColumn("DetailTemplate");
    $("#cboFilter").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        change: function () {
            if (this.value()) {
                $('#grid').data("kendoGrid").dataSource.filter({ field: "Event", operator: "eq", value: this.value() });
            } else {
                $('#grid').data("kendoGrid").dataSource.filter({});
            }
        }
    });
});

function exportData(user) {
	window.location.href = 'process.php?csrfToken=<?= $_SESSION["user_csrf_token"] ?>&actionRun=export&user=' + user;
}