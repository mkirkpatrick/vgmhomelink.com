<? if (!isset($siteConfig)) die("System Error!"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "/library/classes/class_browser_detection.php");

$browser = new BrowserDetection();
$userBrowserName = $browser->getBrowser();
$userBrowserVer = $browser->getVersion();
$userBrowserPlatform = $browser->getPlatform();
$userBrowserIsMobile = $browser->isMobile() == "" ? "No" : $browser->isMobile();
$userBrowserIsRobot = $browser->isRobot() == "" ? "No" : $browser->isRobot();   

function getSecurityQuestion($id) { 
	$id = (int) $id;
	$question = Repository::GetById('tbl_user_questions', $id);
	return $question->uq_question; 
}
?> 
<div class="subcontent right last">
  <?
		if ($form->getNumErrors() > 0) {
			$errors	= $form->getErrorArray();
			echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
			foreach ($errors as $err) echo $err;
		} else if (isset($_SESSION['processed'])) {
			switch($_SESSION['processed']) {
				case 'added':
					echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
                    echo "<div class=\"message success okMessage\">{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!</div>\n";
					break;
				case 'updated':
					echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                    echo "<div class=\"message success okMessage\">{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!</div>\n";
					break;
				case 'deleted':
					echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                    echo "<div class=\"message success\">{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!</div>\n";
					break;
			}
			unset($_SESSION['processed']);
		}
        $user = Repository::GetById('tbl_user', $_SESSION['user_id']); 
  ?>

  <ul class="db-notifications account-settings" style="width: 49%; float: left; padding-right: 25px;">
  <li>  
	<h2 class="floatLeft inline">Account Information</h2>
    <a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>account/index.php?view=profile" class="silver button floatRight">Edit</a>
	<div class="clearfloat"></div>
  </li>
        <li>
            <strong>Name: </strong>
            <span>
                <?= $user->user_name ?>
            </span>
        </li>
        <li>
            <strong>Username: </strong>
            <span>
            <?= $_SESSION['user_active_directory'] ? "VGMINC\\" : "" ?><?= $user->user_username ?>
            </span>
        </li>
        <li>
            <strong>Email: </strong>
            <span>
                <?= $user->user_email ?>
            </span>
        </li>
        <li>
            <strong>Account Created: </strong>
            <span>
                <?= date("m/d/y g:i a", strtotime($user->user_regdate)); ?>
            </span>
        </li>
        <li><br />
	  	<h3 class="floatLeft inline" style="vertical-align:top;">Permissions / Roles</h3>
	<div class="clearfloat"></div>
	</li>
        <li>
            <ul>
				<?php $roles = UserManager::getUserRoles($user->user_id); 
                    foreach($roles as $roleId) {
                    $role = Repository::GetById('tbl_user_roles', $roleId); ?>
                <li><strong style="display: block"><?= $role->name ?></strong><br /></li>
                <?php } ?>
            </ul>
        </li>
    </ul>
	  <ul class="db-notifications account-settings" style="width: 49%; float: right; padding-right: 25px;">
      <li>
	  	<h2 class="floatLeft inline" style="vertical-align:top;">Security Questions</h2>
        <a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>account/index.php?view=editquestions" class="silver button floatRight">Edit</a>
	<div class="clearfloat"></div>
	</li>
        <li>
            <strong>Question 1: </strong>
            <span>
                <?= getSecurityQuestion($user->user_password_question_1_id) ?>
            </span>
        </li>
	        <li>
            <strong>Question 2: </strong>
            <span>
                <?= getSecurityQuestion($user->user_password_question_2_id) ?>
            </span>
        </li>
		        <li>
            <strong>Question 3: </strong>
            <span>
                <?= getSecurityQuestion($user->user_password_question_3_id) ?>
            </span>
        </li>
	  <li>
	  	<h2 class="floatLeft inline" style="vertical-align:top;" >Account Activity</h2>
        <a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>account/index.php?view=logdata" class="silver button floatRight">View</a>
	<div class="clearfloat"></div>
	</li>
        <li>
            <strong>Last Login Date: </strong>
               <span><?= isNullOrEmpty($_SESSION['user_last_login']) ? "Now" : date("m/d/y g:i a", strtotime($_SESSION['user_last_login'])); ?></span>
        </li>
        <li>
            <strong>Last Password Change: </strong>
            <span>
                <?= is_null($_SESSION['user_last_password_change']) ? 'N/A' : date("m/d/y g:i a", strtotime($_SESSION['user_last_password_change'])); ?>
            </span>
        </li>
        <li>
            <strong>Last Account Questions Change: </strong>
            <span>
                <?= date("m/d/y g:i a", strtotime($user->user_last_question_change)); ?>
            </span>
        </li>
<li>
	<h2 class="floatLeft">User Information</h2>
	<div class="clearfloat"></div>
  </li>
          <li>
            <strong>
                Operating System:
            </strong>
            <span><?= $userBrowserPlatform  ?></span>
        </li>
          <li>
            <strong>
                Browser:
            </strong>
            <span><?= $userBrowserName . ' ' . $userBrowserVer  ?></span>
        </li>
		
        <li>
            <strong>
                Your Public IP Address:
            </strong>
            <span><?= $_SERVER['HTTP_CLIENT_IP'] ?></span>
        </li>
		</ul>
</div>