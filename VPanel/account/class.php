<?php
class process extends Database {

	private $record;
	public $params;

	//------------------------------
	public function __construct() {
	//------------------------------
		parent::__construct();
	}

	//----------------------------------
	public function _updateProfile() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$iNum = (int)$_SESSION['user_id'];
		$this->record = Repository::LoadById($_WEBCONFIG['COMPONET_TABLE'], $iNum);

		$fieldName	= "txtName";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Name is required.");
		} else {
			$this->record->user_name = $fieldValue;
		}

		$fieldName	= "txtEmail";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* E-Mail is required.");
		} else if (!Validate::isValidEmail($fieldValue)) {
			$tempEmail = str_replace("\\'", "", $fieldValue);
			if (!ereg('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z.]{2,5}$', $fieldValue)) {
				$form->setError($fieldName, "** Invalid E-Mail Address.");
			}
		} else {
			$this->record->user_email = $fieldValue;
		}

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
		} else {
			// No Errors
			$ip	 = $_SERVER['HTTP_CLIENT_IP'];
			$this->record->user_ip_addr = $ip;
			Repository::Save($this->record);
			$_SESSION['user_name'] = $this->record->user_name;
			Security::_addAuditEvent("Profile Updated", "Success", "User Updated Account Profile", $_SESSION['user_username']);
			$_SESSION['processed'] = 'updated';
			redirect('index.php?view=profile');
		}
	}

    //-----------------------------------
	public function _changePassword() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$iNum = (int)$_POST['hidId'];
		$this->record = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $iNum);

		$fieldName	= "txtExistingPassword";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Existing Password is required.");
		} else if (Security::_hashPassword($fieldValue, $this->record->user_password_salt) != $this->record->user_password) {
			$form->setError($fieldName, "** Existing Password is invalid. ");
		}

		$fieldName	= "txtPassword";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Password is required.");
		} else if (strlen($fieldValue) < $_WEBCONFIG['MIN_PASSWORD_LENGTH']) {
			$form->setError($fieldName, "** Password must be at least " . $_WEBCONFIG['MIN_PASSWORD_LENGTH'] . " characters.");
		} else if (!isStrongPassword($fieldValue)) {
			$form->setError($fieldName, "** Password must contain at least one lower case letter, one upper case letter, one special character, and one digit.");
		} else if (strcmp($fieldValue, $_POST["txtPassword2"]) != 0) {
			$form->setError($fieldName, "** Your password and confirmation password must match. ");
		} else if(self::_usedPreviousPassword($iNum, $fieldValue)) {
			$form->setError($fieldName, "** You cannot use one of your last four passwords.");
		} else if(stristr($fieldValue, $_SESSION['user_username'])) {
			$form->setError($fieldName, "** Your password cannot contain your username. ");
		} else if(stristr($fieldValue, $_SESSION['user_email']))  {
			$form->setError($fieldName, "** Your password cannot contain your email address. ");
		}

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->record->user_last_password_change = date("Y-m-d G:i:s");
			$this->record->user_require_password_change = 'N';
			$newSalt = Security::_generateRandomSalt(24);
			$password = Security::_hashPassword($fieldValue, $newSalt);
			$this->record->user_password_salt = $newSalt;
			$this->record->user_password = $password;
			$this->record->user_password_generated = null;
			Repository::Update($this->record);
			$sql = "INSERT INTO `tbl_user_password` (`user_id`,`up_password`, `up_salt`) VALUES ($iNum, '$password', '$newSalt')";
			Database::ExecuteRaw($sql);
            if($_REQUEST['view'] == 'expiredpassword') {
                Security::_addAuditEvent("Password Change", "Success", "User Updated Expired Password", $_SESSION['user_username']);
            } else if($_REQUEST['view'] == 'newpassword') {
                Security::_addAuditEvent("Password Change", "Success", "User Updated Auto Generated Password", $_SESSION['user_username']);
            } else {
			    Security::_addAuditEvent("Password Change", "Success", "User Changed Password", $_SESSION['user_username']);
			    $_SESSION['processed'] = 'success';
            }
		}

		$_SESSION['user_last_password_change'] = date("Y-m-d G:i:s");
		if($_SESSION['user_expired_pass'] || $_SESSION['user_require_new_pass']) {
			$_SESSION['user_expired_pass'] = false;
			$_SESSION['user_require_new_pass'] = false;
			redirect("../index.php");
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	//-----------------------------------
	public function _changeQuestion() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$iNum = (int)$_POST['hidId'];
		$this->record = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $iNum);
        $newSalt = Security::_generateRandomSalt(24);
        $this->record->user_password_answer_salt = $newSalt;

		$questionName	= "cboQuestion1";
		$questionValue	= trim(strip_tags($_POST[$questionName]));
        $answerName	    = "txtAnswer1";
		$answerValue	= trim(strip_tags($_POST[$answerName]));
        if (!$questionValue || strlen($questionValue = trim($questionValue)) == 0) {
			$form->setError($questionName, "* Question 1 is required.");
		} else {
            $this->record->user_password_question_1_id = $questionValue;
        }

        if (!$answerValue || strlen($answerValue = trim($answerValue)) == 0) {
			$form->setError($answerName, "* Answer 1 is required.");
		} else if ($_WEBCONFIG['MIN_PASSWORD_ANSWER_LENGTH'] > strlen($answerValue)) {
			$form->setError($answerName, "** Answer 1 must be at least " . $_WEBCONFIG['MIN_PASSWORD_ANSWER_LENGTH'] . " characters long.");
		} else {
			$this->record->user_password_answer_1 = Security::_hashPassword($answerValue, $newSalt);
		}

        $questionName	= "cboQuestion2";
		$questionValue	= trim(strip_tags($_POST[$questionName]));
        $answerName	    = "txtAnswer2";
		$answerValue	= trim(strip_tags($_POST[$answerName]));
        if (!$questionValue || strlen($questionValue = trim($questionValue)) == 0) {
			$form->setError($questionName, "* Question 2 is required.");
		} else {
            $this->record->user_password_question_2_id = $questionValue;
        }

        if (!$answerValue || strlen($answerValue = trim($answerValue)) == 0) {
			$form->setError($answerName, "* Answer 2 is required.");
		} else if ($_WEBCONFIG['MIN_PASSWORD_ANSWER_LENGTH'] > strlen($answerValue)) {
			$form->setError($answerName, "** Answer 2 must be at least " . $_WEBCONFIG['MIN_PASSWORD_ANSWER_LENGTH'] . " characters long.");
		} else {
			$this->record->user_password_answer_2 = Security::_hashPassword($answerValue, $newSalt);
		}

        $questionName	= "cboQuestion3";
		$questionValue	= trim(strip_tags($_POST[$questionName]));
        $answerName	    = "txtAnswer3";
		$answerValue	= trim(strip_tags($_POST[$answerName]));
        if (!$questionValue || strlen($questionValue = trim($questionValue)) == 0) {
			$form->setError($questionName, "* Question 3 is required.");
		} else {
            $this->record->user_password_question_3_id = $questionValue;
        }

        if (!$answerValue || strlen($answerValue = trim($answerValue)) == 0) {
			$form->setError($answerName, "* Answer 3 is required.");
		} else if ($_WEBCONFIG['MIN_PASSWORD_ANSWER_LENGTH'] > strlen($answerValue)) {
			$form->setError($answerName, "** Answer 3 must be at least " . $_WEBCONFIG['MIN_PASSWORD_ANSWER_LENGTH'] . " characters long.");
		} else {
			$this->record->user_password_answer_3 = Security::_hashPassword($answerValue, $newSalt);
		}

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
		} else {
			$this->record->user_last_question_change = date("Y-m-d G:i:s");
			$this->record->user_require_question_change = 'N';
			Repository::Update($this->record);
            $_SESSION['user_require_questions'] = false;
            $_SESSION['processed'] = 'success';
            if($_REQUEST['view'] == "newquestions") {
                Security::_addAuditEvent("Added Security Questions", "Success", "User Added Security Questions", $_SESSION['user_username']);
                redirect($_WEBCONFIG['VPANEL_PATH']);
            } else {
                Security::_addAuditEvent("Updated Security Questions", "Success", "User Changed Security Questions", $_SESSION['user_username']);
            }
		}
        redirect('index.php?view=' . $_REQUEST['view']);
	}

    //----------------------------------
    public function _exportLogData() {
    //------------------------------
        self::exportLogToCsv('tbl_audit_log',$_GET['user']);
        exit;
    }

	//-------------------------------------------------------------
	public function _generatePassword($name, $email, $username) {
	//------------------------------
		global $_WEBCONFIG, $siteConfig;

		$password = security::_generateStrongPassword($_WEBCONFIG['MIN_PASSWORD_LENGTH']);
		$htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "/emailtemplates/new-user.html");
		if(file_exists($htmlFile)) {
			$fh = fopen($htmlFile, 'r');
			$body = fread($fh, filesize($htmlFile));
			fclose($fh);
		} else {
			throw new exception("Email Template File Not Found");
		}



		$mergeFields = array();
		$mergeFields["**USERNAME**"] = $username;
		$mergeFields["**PASSWORD**"] = $password;
		$mergeFields["**SITEURL**"] = $_WEBCONFIG['SITE_URL'];
		$mergeFields["**COMPANYNAME**"] = $siteConfig['Company_Name'];
		$mergeFields["**VPANELURL**"] = urlPathCombine($_WEBCONFIG['SITE_URL'], $_WEBCONFIG['VPANEL_PATH']);
		$mergeFields["**VPANELVERSION**"] = $_WEBCONFIG['VPANEL_VERSION'];

		$body = strtr($body, $mergeFields);

		$mailer = new vMail();
		$mailer->addRecipient($this->record->user_email, $this->record->user_name);
		$mailer->setSubject("{$siteConfig['Company_Name']} VPanel");
		$mailer->setMailType("html");
		$mailer->setFrom($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel");
		$mailer->setReplyTo($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel");
		$mailer->setMessage($body);
		$mailer->sendMail();

		return $password;
	}

	//----------------------------
	private function _verify() {
	//----------------------------
		global $_WEBCONFIG, $form;

		$fieldName	= "txtName";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Name is required.");
		} else {
			$this->record->user_name = $fieldValue;
		}

		$fieldName	= "txtEmail";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* E-Mail is required.");
		} else if (!Validate::isValidEmail($fieldValue)) {
			$tempEmail = str_replace("\\'", "", $fieldValue);
			if (!ereg('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z.]{2,5}$', $fieldValue)) {
				$form->setError($fieldName, "** Invalid E-Mail Address.");
			}
		} else {
			$this->record->user_email = $fieldValue;
		}

		$fieldName	= "txtUsername";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Username is required.");
		} else if (strlen($fieldValue) < 4) {
			$form->setError($fieldName, "** Username is less than four characters.");
		} else {
			$userID = isset($_POST['hidId']) ? (int)$_POST['hidId'] : 0;
			$sql = "SELECT * FROM " . $_WEBCONFIG['COMPONET_TABLE'] . " WHERE user_username = '$fieldValue' AND user_id <> $userID";
			$dt  = Database::Execute($sql);

			if ($dt->Count() > 0) {
				$form->setError($fieldName, "** Username already exists in the system.");
			} else {
				$this->record->user_username = $fieldValue;
			}
		}

		$fieldName	= "optStatus";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Status is required.");
		} else {
			$this->record->user_status = $fieldValue;
		}

		$fieldName	= "optMasterAdmin";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Status is required.");
		} else {
			$this->record->user_admin = $fieldValue;
		}
	}

    //----------------------------------------------------------
    private function _usedPreviousPassword($iNum, $password) {
    //----------------------------------------------------------
		$sql = "SELECT `up_password`,`up_salt`
				FROM `tbl_user_password`
				WHERE `user_id` = $iNum
				ORDER BY `up_datetime` DESC
				LIMIT 4";
		$record = Database::Execute($sql);
		while($record->MoveNext()) {
			$hashedPassword = Security::_hashPassword($password, $record->up_salt);
			if($record->up_password == $hashedPassword) {
				return true;
			}
		}
		return false;
	}

    //--------------------------------------------------------------------------
    private function exportLogToCsv($table, $user, $filename = 'export.csv') {
    //--------------------------------------------------------------------------
    $csv_terminated = "\n";
    $csv_separator = ",";
    $csv_enclosed = '"';
    $csv_escaped = "\\";
    $sql_query = "select * from $table WHERE user = '$user'";

    // Gets the data from the database
    $db_link = mysqli_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME); // CONECT TO THE DATABASE
    $result = mysqli_query($db_link, $sql_query); // SEND QUERY TO THE DATABASE
    $fields_cnt = mysqli_field_count($db_link); // GET COLUMN COUNT OF DATABASE TABLE

    $schema_insert = '';

    // LOOP THROUGH COLUMN NAMES AND ADD THEM TO THE CSV DOC
    for ($i = 0; $i < $fields_cnt; $i++) {
        $columnInfo = mysqli_fetch_field_direct($result, $i); // GET COLUMN INFO
        $fieldName = $columnInfo->name; // GET COLUMN NAME
        $nameCharCount = strpos($fieldName, "_"); // FIND FIRST OCCURANCE OF _ IN COLUMN NAME
        if($nameCharCount < 4) {
            if(($pos = strpos($fieldName, '_')) !== false) {
                $columnName = substr($fieldName, $pos + 1);
            } else {
                $columnName = $fieldName;
            }
        }
        else {
            $columnName = $fieldName;
        }
        // CLEAN UP COLUMN NAME
        $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
            stripslashes($columnName)) . $csv_enclosed;
        $schema_insert .= $l;
        $schema_insert .= $csv_separator;
    } // end for

    $out = trim(substr($schema_insert, 0, -1));
    $out .= $csv_terminated;

    // Format the data
    while ($row = mysqli_fetch_array($result)) {
        $schema_insert = '';
        for ($j = 0; $j < $fields_cnt; $j++) {
            if ($row[$j] == '0' || $row[$j] != '') {
                if ($csv_enclosed == '') {
                    $schema_insert .= $row[$j];
                }
                else {
                    $schema_insert .= $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row[$j]) . $csv_enclosed;
                }
            }
            else {
                $schema_insert .= '';
            }

            if ($j < $fields_cnt - 1) {
                $schema_insert .= $csv_separator;
            }
        } // end for

        $out .= str_replace(array('"', '&#039;'), "", htmlspecialchars_decode($schema_insert));
        $out .= $csv_terminated;
    } // end while

    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Length: " . strlen($out));
    // Output to browser with appropriate mime type, you choose <img src="http://thetechnofreak.com/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
    header("Content-type: text/x-csv");
    //header("Content-type: application/csv");
    header("Content-Disposition: attachment; filename=$filename");
    echo $out;
    exit;
}

};
?>