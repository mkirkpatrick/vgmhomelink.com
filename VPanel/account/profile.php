<?
if (!isset($siteConfig)) die("System Error!");

$iNum	= isset($_SESSION['user_id']) && (int)$_SESSION['user_id'] > 0 ? (int)$_SESSION['user_id'] : 0;

if ($form->getNumErrors() > 0) {
    $txtName			= $form->value("txtName");
    $txtEmail			= $form->value("txtEmail");
    $txtUsername		= $form->value("txtUsername");
} else {
    $user = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $iNum);

    if (is_null($user)) {
        throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
    }
    $userId 			= $user->user_id; 
    $hidId				= $userId;
    $txtName			= htmlspecialchars($user->user_name);
    $txtEmail			= htmlspecialchars($user->user_email);
    $txtUsername		= htmlspecialchars($user->user_username);
}
?>  
<div class="subcontent last right">
    <form id="userForm" method="post" action="process.php">
        <input type="hidden" name="actionRun" value="<?= $view ?>" />
        <input type="hidden" name="csrfToken" value="<?= Security::getCsrfToken() ?>" />


        <h2>User Account</h2>

        <?
        if ($form->getNumErrors() > 0) {
            $errors	= $form->getErrorArray();
            foreach ($errors as $err) echo $err;
        } else if (isset($_SESSION['processed'])) {
            echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
            unset($_SESSION['processed']);
        }
        ?>

        <p>Insert the <?= strtolower(urldecode($_WEBCONFIG['ENTITY_NAME'])) ?> information below and save.</p>

        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td align="right" valign="middle"><label for="txtUsername" class="required"><b>Username</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtUsername" id="txtUsername" value="<?= $txtUsername; ?>" size="25" minlength="4" maxlength="125" required="required" readonly disabled="disabled" />
                    <?= $form->error("txtUsername");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="150" align="right" valign="middle"><label for="txtName" class="required"><b>Name</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtName" id="txtName" value="<?= $txtName; ?>" size="50" required="required" data-parsley-required-message="Enter Your First & Last Name." />
                    <?= $form->error("txtName");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td align="right" valign="middle"><label for="txtEmail" class="required"><b>E-Mail</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="email" name="txtEmail" id="txtEmail" value="<?= $txtEmail; ?>" size="50" maxlength="125" required="required" data-parsley-required-message="Enter Your E-Mail Address." />
                    <?= $form->error("txtEmail");?>
                </td>
            </tr>
        </table>

        <div class="buttons clearfix">
            <a href="./" class="button silver floatLeft">Back</a>
            <input name="btnModify" type="submit" id="btnModify" value="Save" class="button floatRight green">
        </div><!--End continue-->

    </form>   
</div>