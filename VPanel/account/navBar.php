<nav class="subnav left">
	<ul>
        <li><a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>account/index.php?view=profile" class="icon-vcard">Modify Account</a></li>
        <?php if(!$_SESSION['user_active_directory']) { ?>
        <li><a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>account/index.php?view=changepassword" class="icon-key">Change Password</a></li>
        <?php } ?>
        <li><a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>account/index.php?view=editquestions" class="icon-help-circled">Change Security Questions</a></li>
        <li><a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>account/index.php?view=logdata" class="icon-menu">Account Activity</a></li>
    </ul>
</nav>