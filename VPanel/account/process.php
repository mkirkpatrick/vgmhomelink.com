<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once 'class.php'; 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_vMail.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_validate.php");

security::_secureCheck();

if (!isset($_GET['logout'])) {
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}
if(!isset($_REQUEST['csrfToken']) || $_REQUEST['csrfToken'] != $_SESSION['user_csrf_token'] )  {
    serverTransfer($_WEBCONFIG['VPANEL_PATH'], "401error.php");
}

if(!isset($_REQUEST['actionRun']))  {
	header("HTTP/1.0 405 Method Not Allowed");
	header("Allow", "POST, GET"); 
	redirect('index.php'); 
}

$p = new process;
$action = $_REQUEST['actionRun']; 

switch ($action) {
	case 'profile' :
		$p->_updateProfile();
		break;
		
	case 'changepassword' :
		$p->_changePassword();
		break;
		
	case 'changequestion' :
		$p->_changeQuestion();
		break;
        
    case 'export' :
        $p->_exportLogData();
        break;        
	

	default :
		throw new exception("An unknown action executed!");
}

redirect("index.php?{$p->params}");
?>