<?
$showDays = 10; 
$today = date("m/d/Y");
$startDate = isset($_GET['start']) ? $_GET['start'] : date("m/d/Y", strtotime("-$showDays day " . $today));
$endDate = isset($_GET['end']) ? $_GET['end'] : $today; 

$whereFilter = "WHERE date >= '" . date(Database::DATE_TIME_FORMAT, strtotime($startDate)) ."' AND date <='" . date(Database::DATE_TIME_FORMAT, strtotime("+ 1 day " . $endDate)) ."' AND deleted = 0"; 
$whereFilter .= isset($_GET['filter']) && !isNullOrEmpty($_GET['filter']) ? " AND name = '" . Database::quote_smart($_GET['filter']) . "'" : '';

?>
<div class="subcontent last right">
    <div class="floatRight forward">
        <a href="javascript:exportData('<?= $_SESSION['user_username'] ?>')" class="button silver icon-export">Export</a>
    </div>
    
    <h2>Account Activity</h2>
    <p>This tool allows administrators and owners to identify security issues before they become a security problem by keeping a security audit log. </p> 

    <table id="grid">
        <thead> 
            <tr> 
                <th>Event</th>
                <th>Details</th>
                <th style="width: 150px">Log Date</th>
                <th style="width: 25px" data-filterable="false" data-sortable="false">Status</th>
                <th data-field="DetailTemplate">Information</th>
            </tr>
        </thead>
        <tbody><?
            $sql = "SELECT * 
                    FROM tbl_audit_log
                    $whereFilter AND user = '{$_SESSION['user_username']}'
                    ORDER BY date DESC";
            $log = Database::Execute($sql);

            while ($log->MoveNext()) {
                if (($timestamp = strtotime($log->date)) === false) {
                    $logDate = '';
                } else {
                    $logDate = date("m/d/y g:i:s A", $timestamp);
                }

                $info = '<b>Additional Details</b><br />' . $log->additionaldata . '<br /><br />
                <b>User IP Address</b><br />' . $log->ip . 
                '<br /><br /><b>User Agent</b><br />' . $log->useragent . ''; 
                print '<tr>
                <td>' . $log->name . '</td>
                <td>' . $log->additionaldata . '</td>
                <td>' . $logDate . '</td>
                <td><div align="center">' . showDetail($log->detail) . '</div></td>
                <td>' . $info . '</td>
                </tr>
                ';
            }// end while
            ?>
        </tbody>
    </table>
</div>
<script type="text/x-kendo-template" id="template">
    <div class="detailTemplate">
    #= DetailTemplate #
    </div>
</script>
<script type="text/x-kendo-template" id="filterTemplate">
    <div class="toolbar floatRight">
    <?
    $sql = "SELECT DISTINCT name FROM tbl_audit_log";
    $dt  = Database::Execute($sql);

    print '<div>
    Filter: <select name="cboFilter" id="cboFilter" style="width: 200px">
    <option value="">View All</option>';

    while ($dt->MoveNext()) {
        $selected = isset($_GET['filter']) && $dt->name == $_GET['filter'] ? 'selected="selected"' : '';
        print '<option value="'. $dt->name . '" ' . $selected . ' >'. $dt->name . '</option>';
    }

    print "</select></div>";
    ?>
    </div>
</script>
<?
function showDetail($detail) {
    if($detail == 'Success') { 
        return '<img src="../images/icons/tick_circle.png" alt="Success" title="Success"  />'; 
    } else { 
        return '<img src="../images/icons/cross_circle.png" alt="Failure" title="failure" />'; 
    }
}
?>