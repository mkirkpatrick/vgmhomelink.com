CKEDITOR.plugins.add( 'tokens',
{   
   requires : ['richcombo'], //, 'styles' ],
   init : function( editor )
   {
      var config = editor.config,
         lang = editor.lang.format;

    
				
				editor.ui.addRichCombo( 'tokens',
         {
            label : "Link to a Page",
            title :"Link to a Page",
            voiceLabel : "Link to a Page",
            className : 'cke_format super-extra',
            multiSelect : false,

            panel :
            {
               css : [ config.contentsCss, CKEDITOR.getUrl( 'skins/moonocolor/editor.css' ) ],
               voiceLabel : lang.panelVoiceLabel
            },

            init : function()
            {
               this.startGroup( "Select a page to link to" );
               //this.add('value', 'drop_text', 'drop_label');
							 var pageList = pageBuilderPageList.pages; // this is a php include in the header between <script/>'s
							 							 
               for (var this_tag in pageList){
                  this.add(pageList[this_tag][0], '<span style="font-family:helvetica,arial,sans-serif;font-size:12px;">'+pageList[this_tag][1]+'</span>', pageList[this_tag][2]);
               }
            },

            onClick : function( value )
            {         
               editor.focus();
               editor.fire( 'saveSnapshot' ); 
               editor.insertHtml( '<a href="'+value+'">' + pageBuilderPageList.pages[value][2] + '</a>' );
               editor.fire( 'saveSnapshot' );
            }
         });
			
			
			
			
		
   }
});