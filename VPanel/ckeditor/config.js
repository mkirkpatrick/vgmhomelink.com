/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.skin = 'moonocolor';
    config.bodyClass = 'content';
    config.contentsCss = ['/css/main.css', '/VPanel/css/ckeditor-override.css'];
    
    config.filebrowserBrowseUrl = '/VPanel/ckfinder/ckfinder.html?type:files';
    config.filebrowserImageBrowseUrl = '/VPanel/ckfinder/ckfinder.html?type:images';
    config.filebrowserUploadUrl = '/VPanel/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    config.filebrowserImageUploadUrl = '/VPanel/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'; 
     
    config.toolbarCanCollapse = true;
    config.scayt_autoStartup = true;
    config.allowedContent = true;
     
    config.toolbar =
    [
        { name: 'document', items: ['Source'] },
        { name: 'document', items: ['Preview', 'Print', '-', 'Templates'] },
        { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
        { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt', 'AutoCorrect'] },
        { name: 'styles', items: ['Styles', 'Format', 'FontSize'] },
        { name: 'colors', items: ['TextColor', 'BGColor'] }, '/',
        { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
        { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] },
        { name: 'paragraph', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
        { name: 'links', items: ['Link', 'Unlink', 'Anchor', 'tokens'] },
        { name: 'insert', items: ['Image', 'Youtube', 'Iframe', 'Table'] },
        { name: 'insert', items: ['HorizontalRule', 'SpecialChar'] }
    ];    
               
};
