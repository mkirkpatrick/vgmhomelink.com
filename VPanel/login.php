<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");
$returnUrl = isset($_GET['ReturnUrl']) ? str_replace("**","&",htmlentities($_GET['ReturnUrl'], ENT_QUOTES, "utf-8")) : ''; ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="robots" content="noindex,nofollow" />
        <title>VPanel :: Login Page</title>
        <link type="image/x-icon" href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>favicon.ico" rel="icon" />
        <link href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>apple-touch-icon.png" rel="apple-touch-icon" />
        <link type="text/css" href="css/vpanel.css" rel="stylesheet" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <!--[if lt IE 9]>
        <script src="/vpanel/jquery/modernizr.js"></script>
        <![endif]-->
    </head>
    <body>

        <header class="masthead">
            <div class="utility-bar">
                <div class="container clearfix">
                    <a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>" class="logo"><img src="<?= $_WEBCONFIG['VPANEL_PATH'] ?>images/logo.png" alt="Forbin VPanel"></a>
                </div>
            </div>
        </header>

        <div class="container clearfix">
            <div class="login">
                <form id="secureForm" method="post" action="process.php" autocomplete="off">
                    <?php loadRC() ?>
                    <input type="hidden" name="actionRun" value="login" />
                    <input type="hidden" name="hidReturnUrl" id="hidReturnUrl" value="<?= $returnUrl  ?>" />

                    <h1>VPanel Admin Panel Sign In</h1>
                    <p>
                        <strong>For official HOMELINK use only. All other access is strictly forbidden.</strong><br>
                        By successfully signing in you agree to be bound by all rules defined under the <a href="https://www.hhs.gov/hipaa/index.html" title="External link to HIPAA website">HIPAA</a> regulations.
                    </p>
                    <p>Please enter your username and password.</p>
                    <?php
                    if (isset($_SESSION['errMsg'])) {
                        $errorMessage = $_SESSION['errMsg'];
                        print "<div class=\"error2\">$errorMessage</div>\n";
                        unset($_SESSION['errMsg']);
                    }
                    ?>
                    <div id="success" class="success message notification none"></div>
                    <ul class="form">
                        <li>
                            <label for="txtUsername">Username</label>
                            <input title="Username" type="text" name="txtUsername" id="txtUsername" value="" maxlength="125" required="required" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" />
							<!-- disables autocomplete --><input type="text" style="display:none" />
                        </li>
                        <li>
                            <label for="txtPassword">Password</label>
                            <input title="Password" type="password" name="txtPassword" id="txtPassword" value="" maxlength="50" required="required" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" />
                        </li>
                    </ul>

                    <div>
                        <input id="loginBtn" type="submit" id="login" name="sendLogin" value="Sign in" class="button block green submit" style="display: inline;" />
                    </div>

                    <p><br><a class="floatLeft" href="/">&larr; Back to your website</a></p>
                </form>

                <!-- user input dialog -->
                <div class="modal" id="prompt">
                    <h2>Forgot Your Password?</h2>
                    <p>Enter your E-mail associated with your account. <br>To close it click Cancel or use the ESC key.</p>
                    <div id="error" class="error message notification none"></div>
                    <!-- input form. you can press enter too -->
                    <form id="forgotPasswordForm" autocomplete="off">
                        <input type="text" name="txtEmail" id="txtEmail" value="" size="40" maxlength="255" autocomplete="off" required="required" data-parsley-required-message="Enter Your Email Address." />
                        <div class="clearfloat"></div>
                        <input type="submit" id="forgotPasswordBtn" class="submit green button floatLeft" value="OK" />
                        <input type="button" class="close silver button floatLeft" value="Cancel" />
                    </form>
                </div>
                <div class="overlay-mask"></div>
            </div>
        </div>
        <link rel="stylesheet" href="/css/parsley.css" type="text/css" />
        <script type="text/javascript" src="<?= $_WEBCONFIG['VPANEL_PATH'] ?>jquery/pages/login.js"></script>
        <script type="text/javascript" src="/scripts/parsley.min.js"></script>
    </body>
</html>