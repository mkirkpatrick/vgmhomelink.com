<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VPanel :: Page Error</title>
<link type="text/css" href="/vpanel/css/vpanel.css" rel="stylesheet" />
<link href="//fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
</head>
<body>

<header class="masthead" role="banner">
	<div class="utility-bar">
        <div class="container clearfix">
            <a href="/VPanel/" title="Control Panel Home" class="logo"><img src="<?= $_WEBCONFIG['VPANEL_PATH'] ?>images/logo.png" style="border: 0" alt="VGM Forbin VPanel Administration" class="back"></a>
             ?
            <ul class="aux">
                <li id="logout"><a class="logout icon-cancel-squared button red" href="?logout">Logout</a></li>
            </ul>
        </div>
    </div>
</header>

<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-thumbs-down">It's ok!</h1>
        <p class="page-desc">It looks like you've run into a small error.</p>
    </div>
</section>

<section class="maincontent">
	<div class="container clearfix">
        <div class="fullcontent">

    		<h2>Page Error</h2>
    		<p>You have experienced an error. Please try your action again later and contact us if the error persists. We apologize for the inconvenience.</p>
	
			<p>VGM Forbin Customer Support<br />
			Phone: 877-814-7485 <br />
			Email: <a href="mailto: support@forbin.com">support@forbin.com</a>
			</p>
	
       	<h3>Please try the following:</h3>
        <ul>
		  <li>Return to VPanel's <a href="<?= $_WEBCONFIG['VPANEL_PATH'],  ?>index.php">Home Page</a>, and try again.</li>
          <li>Click the <a href="javascript:history.go(-2)">Back</a> button on your browser to try another link.</li>
        </ul>
	</div>
    </div>
    </section>

</body>
</html>