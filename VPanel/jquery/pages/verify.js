(function ($) {
    $.toggleShowPassword = function (options) {
        var settings = $.extend({
            field: "#password",
            control: "#toggle_show_password",
        }, options);

        var control = $(settings.control);
        var field = $(settings.field)

        control.bind('click', function () {
            if (control.is(':checked')) {
                field.attr('type', 'text');
            } else {
                field.attr('type', 'password');
            }
        })
    };
}(jQuery));

(function($){$.fn.dPassword=function(options){var defaults={interval:200,duration:3000,replacement:'%u25CF',prefix:'password_',debug:false}
var opts=$.extend(defaults,options);var checker=new Array();var timer=new Array();$(this).each(function(){if(opts.debug)console.log('init ['+$(this).attr('id')+']');var name=$(this).attr('name');var id=$(this).attr('id');var cssclass=$(this).attr('class');var style=$(this).attr('style');var size=$(this).attr('size');var maxlength=$(this).attr('maxlength');var disabled=$(this).attr('disabled');var tabindex=$(this).attr('tabindex');var accesskey=$(this).attr('accesskey');var value=$(this).attr('value');checker.push(id);timer.push(id);$(this).hide();if(opts.debug){$(this).after('<span id="debug_'+opts.prefix+name+'" style="color: #f00;"></span>');}
$(this).after(' <input name="'+(opts.prefix+name)+'" '+'id="'+(opts.prefix+id)+'" '+'type="text" '+'value="'+value+'" '+
(cssclass!=''?'class="'+cssclass+'"':'')+
(style!=''?'style="'+style+'"':'')+
(size!=''?'size="'+size+'"':'')+
(maxlength!=-1?'maxlength="'+maxlength+'"':'')+
(disabled!=''?'disabled="'+disabled+'"':'')+
(tabindex!=''?'tabindex="'+tabindex+'"':'')+
(accesskey!=undefined?'accesskey="'+accesskey+'"':'')+'autocomplete="off" />');$('label[for='+id+']').attr('for',opts.prefix+id);$(this).attr('tabindex','');$(this).attr('accesskey','');$('#'+opts.prefix+id).bind('focus',function(event){if(opts.debug)console.log('event: focus ['+getId($(this).attr('id'))+']');clearTimeout(checker[getId($(this).attr('id'))]);checker[getId($(this).attr('id'))]=setTimeout("check('"+getId($(this).attr('id'))+"', '')",opts.interval);});$('#'+opts.prefix+id).bind('blur',function(event){if(opts.debug)console.log('event: blur ['+getId($(this).attr('id'))+']');clearTimeout(checker[getId($(this).attr('id'))]);});setTimeout("check('"+id+"', '', true);",opts.interval);});getId=function(id){var pattern=opts.prefix+'(.*)';var regex=new RegExp(pattern);regex.exec(id);id=RegExp.$1;return id;}
setPassword=function(id,str){if(opts.debug)console.log('setPassword: ['+id+']');var tmp='';for(i=0;i<str.length;i++){if(str.charAt(i)==unescape(opts.replacement)){tmp=tmp+$('#'+id).val().charAt(i);}
else{tmp=tmp+str.charAt(i);}}
$('#'+id).val(tmp);}
check=function(id,oldValue,initialCall){if(opts.debug)console.log('check: ['+id+']');var bullets=$('#'+opts.prefix+id).val();if(oldValue!=bullets){setPassword(id,bullets);if(bullets.length>1){var tmp='';for(i=0;i<bullets.length-1;i++){tmp=tmp+unescape(opts.replacement);}
tmp=tmp+bullets.charAt(bullets.length-1);$('#'+opts.prefix+id).val(tmp);}
else{}
clearTimeout(timer[id]);timer[id]=setTimeout("convertLastChar('"+id+"')",opts.duration);}
if(opts.debug){$('#debug_'+opts.prefix+id).text($('#'+id).val());}
if(!initialCall){checker[id]=setTimeout("check('"+id+"', '"+$('#'+opts.prefix+id).val()+"', false)",opts.interval);}}
convertLastChar=function(id){if($('#'+opts.prefix+id).val()!=''){var tmp='';for(i=0;i<$('#'+opts.prefix+id).val().length;i++){tmp=tmp+unescape(opts.replacement);}
$('#'+opts.prefix+id).val(tmp);}}};})(jQuery);

$(document).ready(function() {
	
	// add * to required field labels
	$('label.required').append('&nbsp;<span style="color: #990000"><b>*</b></span>&nbsp;');		
	
	$("input[type='submit']").click(function (event) {
	    event.preventDefault();
		var myform = '#' + $(this).closest("form").attr('id');
	    var buttonVal = $(this).val();
		$("html").addClass("wait");
	    $(this).val('Validating...');
	    $(this).attr('disabled', 'disabled');
		$(myform).parsley({excluded: "input[type=button], input[type=submit], input[type=reset], input[type=hidden], input:hidden, textarea[type=hidden], textarea:hidden"});		
		if($(myform).parsley().isValid()) {
			$(myform).submit(); 
		}
		else {
			$("html").removeClass("wait");
			$(this).val(buttonVal);
			$(this).removeAttr('disabled');       
			$(myform).parsley().validate();
		}
	});            
	
	$.toggleShowPassword({field: '#txtQuestion1', control: '#cboShowHide'});
	$.toggleShowPassword({field: '#txtQuestion2', control: '#cboShowHide'});
	$.toggleShowPassword({field: '#txtQuestion3', control: '#cboShowHide'});
});