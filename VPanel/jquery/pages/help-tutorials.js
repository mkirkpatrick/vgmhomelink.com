// JavaScript Document
$(document).ready(function() {

	// install flowplayer into flowplayer container
	var player = $f("player", "flowplayers/flowplayer-3.2.7.swf");

	// setup button action. it will fire our overlay 
	$("a[rel]").overlay({
		// use the Apple effect for overlay
		mask: { 
			color: '#ebecff', 
			loadSpeed: 200, 
			opacity: 0.9 
		}, 
		// when overlay is opened, load our player
		onLoad: function() {
			player.load();
		},
		// when overlay is closed, unload our player
		onClose: function() {
			player.unload();
		}
	});

	$("#btnPrint").click(function(){
		window.print();
	});

});
