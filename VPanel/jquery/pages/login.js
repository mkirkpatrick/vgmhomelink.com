$(document).ready(function() {
	
	$('#success').hide();
	$('#success').html('');
	// Overlays
	$('a.openoverlay').click(function() {
		var overlayrel = $(this).attr('rel');
		$(''+ overlayrel +'').fadeIn();
		$('.overlay-mask').fadeIn();
		return false;
	});

	$('.modal .close').click(function() {
		$('#prompt').fadeOut();
		$('.overlay-mask').fadeOut();
	});    

	$("#prompt form").submit(function(e) {
		// get user input 
		var input = $("#txtEmail", this).val(); 

		// verify e-mail 
		$.post("process.php", { email: input, actionRun: 'forgotPassword', RC_Validate1: $('#RC_Validate1').val(), RC_Validate2: $('#RC_Validate2').val(), RC_Validate3: $('#RC_Validate3').val(), RC_Validate4: $('#RC_Validate4').val() }, function(data){
			if (data == "OK") {
				$('#success').html("Your new password has been generated and sent to the email provided.");
				$('#success').show();
				$('.close').click();
			} else {
				$('#error').html(data);
				$('#success').hide();
				$('#error').show();                
			}
			$("html").removeClass("wait");
			$('#forgotPasswordBtn').val("OK"); 
			$('#forgotPasswordBtn').removeAttr('disabled');       
		});

		// do not submit the form
		return e.preventDefault();
	});
	
	
	$("input[type='submit']").click(function (event) {
	    event.preventDefault();
		var myform = '#' + $(this).closest("form").attr('id');
	    var buttonVal = $(this).val();
		$("html").addClass("wait");
	    $(this).val('Validating...');
	    $(this).attr('disabled', 'disabled');
		$(myform).parsley({excluded: "input[type=button], input[type=submit], input[type=reset], input[type=hidden], input:hidden, textarea[type=hidden], textarea:hidden"});		
		if($(myform).parsley().isValid()) {   
			$(myform).submit(); 
		}
		else {
			$("html").removeClass("wait");
			$(this).val(buttonVal);
			$(this).removeAttr('disabled');       
			$(myform).parsley().validate();
		}
	});	            
});