(function(A) {
    A.extend(A.fn, {
        pwstrength: function(B) {
            var B = A.extend({
                colors: ["#f00", "#c06", "#f60"],
                common: ["1q2w3e", "1q2w3e4r", "123456","password","12345678","1234","pussy","12345","dragon","qwerty","696969","mustang","letmein","baseball","master","michael","football","shadow","monkey","abc123","pass","6969","jordan","harley","ranger","iwantu","jennifer","hunter","2000","test","batman","trustno1","thomas","tigger","robert","access","love","buster","1234567","soccer","hockey","killer","george","sexy","andrew","charlie","superman","asshole","dallas","jessica","panties","pepper","1111","austin","william","daniel","golfer","summer","heather","hammer","yankees","joshua","maggie","biteme","enter","ashley","thunder","cowboy","silver","richard","orange","merlin","michelle","corvette","bigdog","cheese","matthew","121212","patrick","martin","freedom","ginger","blowjob","nicole","sparky","yellow","camaro","secret","dick","falcon","taylor","111111","131313","123123","bitch","hello","scooter","please","","porsche","guitar","chelsea","black","diamond","nascar","jackson","cameron","654321","computer","amanda","wizard","xxxxxxxx","money","phoenix","mickey","bailey","knight","iceman","tigers","purple","andrea","horny","dakota","aaaaaa","player","sunshine","morgan","starwars","boomer","cowboys","edward","charles","girls","booboo","coffee","xxxxxx","bulldog","ncc1701","rabbit","peanut","john","johnny","gandalf","spanky","winter","brandy","compaq","carlos","tennis","james","mike","brandon","fender","anthony","blowme","ferrari","cookie","chicken","maverick","chicago","joseph","diablo","sexsex","hardcore","666666","willie","welcome","chris","panther","yamaha","justin","banana","driver","marine","angels","fishing","david","maddog","hooters","wilson","butthead","dennis","captain","bigdick","chester","smokey","xavier","steven","viking","snoopy","blue","eagles","winner","samantha","house","miller","flower","jack","firebird","butter","united","turtle","steelers","tiffany","zxcvbn","tomcat","golf","bond007","bear","tiger","doctor","gateway","gators","angel","junior","thx1138","porno","badboy","debbie","spider","melissa","booger","1212","flyers","fish","porn","matrix","teens","scooby","jason","walter","cumshot","boston","braves","yankee","lover","barney","victor","tucker","princess","mercedes","5150","doggie","zzzzzz","gunner","horney","bubba","2112","fred","johnson","xxxxx","tits","member","boobs","donald","bigdaddy","bronco","penis","voyager","rangers","birdie","trouble","white","topgun","bigtits","bitches","green","super","qazwsx","magic","lakers","rachel","slayer","scott","2222","asdf","video","london","7777","marlboro","srinivas","internet","action","carter","jasper","monster","teresa","jeremy","11111111","bill","crystal","peter","pussies","cock","beer","rocket","theman","oliver","prince","beach","amateur","7777777","muffin","redsox","star","testing","shannon","murphy","frank","hannah","dave","eagle1","11111","mother","nathan","raiders","steve","forever","angela","viper","ou812","jake","lovers","suckit","gregory","buddy","whatever","young","nicholas","lucky","helpme","jackie","monica","midnight","college","baby","brian","mark","startrek","sierra","leather","232323","4444","beavis","bigcock","happy","sophie","ladies","naughty","giants","booty","blonde","golden","0","fire","sandra","pookie","packers","einstein","dolphins","0","chevy","winston","warrior","sammy","slut","8675309","zxcvbnm","nipples","power","victoria","asdfgh","vagina","toyota","travis","hotdog","paris","rock","xxxx","extreme","redskins","erotic","dirty","ford","freddy","arsenal","access14","wolf","nipple","iloveyou","alex","florida","eric","legend","movie","success","rosebud","jaguar","great","cool","cooper","1313","scorpio","mountain","madison","987654","brazil","lauren","japan","naked","squirt","stars","apple","alexis","aaaa","bonnie","peaches","jasmine","kevin","matt","qwertyui","danielle","beaver","4321","4128","runner","swimming","dolphin","gordon","casper","stupid","shit","saturn","gemini","apples","august","3333","canada","blazer","cumming","hunting","kitty","rainbow","112233","arthur","cream","calvin","shaved","surfer","samson","kelly","paul","mine","king","racing","5555","eagle","hentai","newyork","little","redwings","smith","sticky","cocacola","animal","broncos","private","skippy","marvin","blondes","enjoy","girl","apollo","parker","qwert","time","sydney","women","voodoo","magnum","juice","abgrtyu","777777","dreams","maxwell","music","rush2112","russia","scorpion","rebecca","tester","mistress","phantom","billy","6666","albert"],
                minchar: 8
            }, B);
            return this.each(function() {
                var id = A(this).attr("id");
                A(this).after("<div class=\"pstrength-info\" style=\"padding-top: 5px\" id=\"" + id + "_text\"></div>");
                A(this).keyup(function() {
                    A.fn.runPassword(A(this).val(), id, B)
                })
            })
        },
        runPassword: function(D, F, C) {
            aReturn = A.fn.checkPassword(D, C);
            var text = "#" + F + "_text";
			var inputField = A(text).prevAll('input');
			if(aReturn == 0) { 
				inputField.css({ border: "1px solid #ccc" });
				A(text).html("")
			} else { 
				if (aReturn == 1) {
					strColor = "#FF0000";
					strText = "Password Is Too Common";
				} else if (aReturn == 2) {
					strColor = "#A52A2A";
					strText = "Password Length Too Short";
				} else if (aReturn == 3) {
					strColor = "#F87217";
					strText = "Password Must Contain Lowercase Letter";
				} else if (aReturn == 4) {
					strColor = "#C35817";
					strText = "Password Must Contain Uppercase Letter";
				} else if (aReturn == 5) {
					strColor = "#E56717";
					strText = "Password Must Contain A Numerical Value";
				} else if (aReturn == 6) {
					strColor = "#0000A0";
					strText = "Password Contains Too Many Consecutive Numbers";
				} else if (aReturn == 7) {
					strColor = "#0000A0";
					strText = "Password Must Contain A Special Character";
				} else { 
					strColor = "#008000";
					strText = "Strong Password";
				}
				inputField.css({ borderColor: strColor });
				inputField.css({ borderWidth: "2px" });
				A(text).html("<span style='color: " + strColor + ";'>" + strText + "</span>"); 
			}
        },
        checkPassword: function(password, B) {
            var response = 10; 
			if(password.length == 0) { 
				return 0; 
			} 
			for (var i = 0; i < B.common.length; i++) {
				if (password.toLowerCase() == B.common[i]) {
					return 1; 
				}
			}
			if (password.length < B.minchar) { 
                response = 2
            } else if (!password.match(/[a-z]/)) {
                response = 3
            } else if (!password.match(/[A-Z]/)) {
                response = 4
            } else if (!password.match(/\d+/)) {
                response = 5
            } else if (!password.match(/(.*[!,@,#,$,%,^,&,%,*,?,_,~])/)) {
                response = 7
            }
            return response; 
        }
    })
})(jQuery)

$(function() {
	$(".pwStrength").pwstrength();
	$("#txtPassword2").on("keyup change blur", function() { 
		if($('#txtPassword').val() != $(this).val()) { 
			$(this).css({ borderColor: "#FF0000" });
			$(this).css({ borderWidth: "2px" });
			$('#validationMessage').css({ color: "#FF0000" });
			$('#validationMessage').html("Passwords Do Not Match"); 
		} else {
			$('#validationMessage').css({ color: "#008000" });
			$(this).css({ border: "2px solid #008000" });
			$('#validationMessage').html("Passwords Match"); 
		}
	}); 
});