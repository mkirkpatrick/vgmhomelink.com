// Tooltips
$(".edit a[title], .published a[title], .remove a[title], .reply a[title], ul.progress a[title], .view a[title]").tooltip({
    effect: 'slide'	
});

var timeleft = sessionTimeout; 
var warningTime = 5;  
var interval = setInterval('sessionNotification()', 60000);
var message = 'Security Warning :: Your online session is about to expire\r\n\r\nFor your protection, you will be automatically logged out of the VPanel in ' + warningTime + ' minutes. \r\nIf you would like to continue using VPanel, click OK below.'; 
function sessionNotification()
{
    timeleft--; 
    if(timeleft <= warningTime) {
        if(confirm(message)) {
            $.ajax({
                url: "/vpanel/?renew", 
                success: function(data){
                    if(data.substring(0, 100).indexOf("<!-- VPanel Login Page -->") >= 0) {
                        window.location = "/vpanel/?logout"; 
                    } 
                }
            });  
            timeleft = sessionTimeout; 
        } else { window.location = "/vpanel/?logout";  }
    }
}

$(document).on('keydown', function(e){
    if(e.ctrlKey && e.which === 83){ // Check for the Ctrl key being pressed, and if the key = [S] (83)
        $("form").submit();
        e.preventDefault();
        return false;
    }
});

/*
addOnloadEvent(myFunctionName);
Or to pass arguments
addOnloadEvent(function(){ myFunctionName('myArgument') });
*/

function addOnloadEvent(fnc){
    if ( typeof window.addEventListener != "undefined" )
        window.addEventListener( "load", fnc, false );
    else if ( typeof window.attachEvent != "undefined" ) {
        window.attachEvent( "onload", fnc );
    }
    else {
        if ( window.onload != null ) {
            var oldOnload = window.onload;
            window.onload = function ( e ) {
                oldOnload( e );
                window[fnc]();
            };
        }
        else 
            window.onload = fnc;
    }
}

// for the search bar above the kendo grid
function getInternetExplorerVersion() {
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

/*
Strip whitespace from the beginning and end of a string
Input : a string
*/
function trim(str)
{
    return str.replace(/^\s+|\s+$/g,'');
}

/*
Check if a form element is empty.
If it is display an alert box and focus
on the element
*/
function isEmpty(formElement, message) 
{
    formElement.value = trim(formElement.value);

    _isEmpty = false;
    if (formElement.value == '') {
        _isEmpty = true;
        alert(message);
        formElement.focus();
    }

    return _isEmpty;
}

function IsNumeric(input)
{
    return (input - 0) == input && input.length > 0;
}

function checkEnter(e) { //e is event object passed from function invocation
    var characterCode;

    if(e && e.which) { //if which property of event object is supported (NN4)
        e = e;
        characterCode = e.which; //character code is contained in NN4's which property
    } else {
        e = event;
        characterCode = e.keyCode; //character code is contained in IE's keyCode property
    }

    if(characterCode == 13) { //if generated character code is equal to ascii 13 (if enter key)
        return false;
    } else {
        return true;
    }
}

//configure status message not to show
var statusmsg=""
function hidestatus(){
    window.status=statusmsg
    return true
}

function isDocument(formElement, message) {
    var OK = new Array ("csv","doc","pdf","ppt","txt","xls");  // if you change this element, please change the config.php

    trimElement = trim(formElement.value);
    if (trimElement == '') {
        return true;
    }

    var ext = getExt(trimElement);
    _isDocument = false;

    for (i=0; i<OK.length; i++) {
        if (OK[i] == ext) {
            _isDocument = true; // one of the file extensions found
        } 
    }

    if (!_isDocument) { 
        if (message!='') alert(message);
        formElement.focus();
    }

    return _isDocument;
}

function isImage(formElement, message) {
    var OK = new Array ("gif","jpg","jpeg","png","wbmp");  // if you change this element, please change the config.php

    trimElement = trim(formElement.value);
    if (trimElement == '') {
        return true;
    }

    var ext = getExt(trimElement);

    _isImage = false;

    for (i=0; i<OK.length; i++) {
        if (OK[i] == ext) {
            _isImage = true; // one of the file extensions found
        } 
    }

    if (!_isImage) { 
        if (message!='') alert(message);
        formElement.focus();
    }

    return _isImage;
}

function isImageVal(formElement) {
    var OK = new Array ("gif","jpg","jpeg","png","wbmp");  // if you change this element, please change the config.php

    if (formElement == '') {
        return true;
    }

    var ext = getExt(formElement);

    _isImage = false;

    for (i=0; i<OK.length; i++) {
        if (OK[i] == ext) {
            _isImage = true; // one of the file extensions found
        } 
    }

    return _isImage;
}

function isVideo(formElement, message) {
    var OK = new Array ("3gp","asf","asx","avi","mov","mp4","mpg","qt","rm","swf","wmv");  // if you change this element, please change the config.php

    trimElement = trim(formElement.value);
    if (trimElement == '') {
        return true;
    }

    var ext = getExt(trimElement);
    _isDocument = false;

    for (i=0; i<OK.length; i++) {
        if (OK[i] == ext) {
            _isDocument = true; // one of the file extensions found
        } 
    }

    if (!_isDocument) { 
        if (message!='') alert(message);
        formElement.focus();
    }

    return _isDocument;
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isDecimalKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode

    if (charCode == 46) // Decimal Period
        return true;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isCommaDecimalKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode

    if (charCode == 44 || charCode == 46) // Coma or Decimal Period
        return true;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isDecPercentKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode

    if (charCode == 37 || charCode == 46) // % or Decimal Period
        return true;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isQuantityKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode

    if (charCode == 43 || charCode == 45) // + or - key
        return true;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isPostalKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode

    if (charCode == 45) // - key
        return true;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function getExt(formElement) {
    var dot_pos = formElement.lastIndexOf(".");
    if(dot_pos == -1)
        return "";
    return formElement.substr(dot_pos+1).toLowerCase();
}

function getURL() {
    // Get URL
    if (location.href.indexOf("https://")!= -1) 
        var prefix = "https://"; 
    else 
        var prefix = "http://"; 

    if (location.href.indexOf('//') !=-1) {
        firstpos = location.href.indexOf('//')+2;
        var tmpHref = location.href.substring(firstpos);
        if (tmpHref.indexOf('/') !=-1) {
            lastpos = tmpHref.indexOf('/');
            var strHref = prefix + tmpHref.substring(0, lastpos);
        } else {
            var strHref = location.href;
        }
    } else {
        if (location.href.indexOf('/') !=-1) {
            lastpos = location.href.indexOf('/');
            var strHref = prefix + location.href.substring(0, lastpos);
        } else {
            var strHref = prefix + location.href;
        }
    }

    return strHref;
}

function getInternetExplorerVersion() {
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}


function clearText(field) {
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}

function toggleBox(szDivID, iState) { // 1 visible, 0 hidden
    var obj = document.layers ? document.layers[szDivID] : document.getElementById ?  document.getElementById(szDivID).style : document.all[szDivID].style;
    obj.display = document.layers ? (iState ? "show" : "hide") : (iState ? "block" : "none");
}

function roundNumber(num, dec) {
    var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
    return pad_with_zeros(result, dec);
}

function pad_with_zeros(rounded_value, decimal_places) {
    // Convert the number to a string
    var value_string = rounded_value.toString();
    // Locate the decimal point
    var decimal_location = value_string.indexOf(".");
    // Is there a decimal point?
    if (decimal_location == -1) {
        // If no, then all decimal places will be padded with 0s
        decimal_part_length = 0;
        // If decimal_places is greater than zero, tack on a decimal point
        value_string += decimal_places > 0 ? "." : "";
    } else {
        // If yes, then only the extra decimal places will be padded with 0s
        decimal_part_length = value_string.length - decimal_location - 1;
    }
    // Calculate the number of decimal places that need to be padded with 0s
    var pad_total = decimal_places - decimal_part_length;

    if (pad_total > 0) {
        // Pad the string with 0s
        for (var counter = 1; counter <= pad_total; counter++) 
            value_string += "0";
    }

    return value_string;
}

// help tips
$(window).load (function() {
    $('.help-tip').hover(
        function () {
            $('.help-tip-content').remove();
            $('.help-tip-content-widget').remove();

            if($(this).attr('asset-type') == 'Widget') {
                ID = $(this).attr('asset-id');
                $('body').append('<div class="help-tip-content-widget"><iframe id="widgetID" src="http://putnambank.forbinhosting.net/VPanel/pagebuilder/assets/asset-preview.php?id=' + ID + '"></iframe></div>');
                $('.help-tip-content-widget').click(stopEventPropigation);
                $('.help-tip-content-widget').css({
                    left: $(this).offset().left - ($('.help-tip-content-widget').width() / 2)
                    , top: $(this).offset().top + ($(this).height()) + 3
                    , opacity: 0.01
                });
                $('.help-tip-content-widget').animate({ opacity: 1 }, { duration: 'medium' });
                resizeIframe($('iframe'));
                // Close Widget DIV
                $('.widget').mouseleave(function () { $('.help-tip-content-widget').fadeOut('slow', function () { $(this).remove(); }) });                          
            }
            else if($(this).attr('asset-type') == 'Calculator') { 
                $('body').append('<div class="help-tip-content-calculator"><img title="Close" class="calcClose" src="/VPanel/images/overlay/close.png" />' + $(this).attr('data-msg') + '</div>');    
                $('.help-tip-content-calculator').click(stopEventPropigation);
                $('.help-tip-content-calculator').css({
                    left: $(this).offset().left - ($('.help-tip-content-calculator').width() / 2)
                    , top: $(this).offset().top + ($(this).height()) + 3
                    , opacity: 0.01
                });
                $('.help-tip-content-calculator').animate({ opacity: 1 }, { duration: 'medium' });
                // Set iFrame Width
                $('iframe').width('600');
                // Close Preview DIV
                $('.calcClose').click(function () {  $('.help-tip-content-calculator').fadeOut("fast","linear", function () { $(this).remove(); }) });
                $('.help-tip-content-calculator').mouseleave(function () { $('.help-tip-content-calculator').fadeOut("fast","linear", function () { $(this).remove(); }) });

            }
            else {
                $('body').append('<div class="help-tip-content">' + $(this).attr('data-msg') + '</div>');    
                $('.help-tip-content').click(stopEventPropigation);
                $('.help-tip-content').css({
                    left: $(this).offset().left - ($('.help-tip-content').width() / 2)
                    , top: $(this).offset().top + ($(this).height()) + 3
                    , opacity: 0.01
                });
                $('.help-tip-content').animate({ opacity: 1 }, { duration: 'medium' }); 
                // Close Other DIV
                $('.other').mouseleave(function () { $('.help-tip-content').fadeOut('slow', function () { $(this).remove(); }) });                                       
                $('.help-tip').mouseleave(function () { $('.help-tip-content').fadeOut('slow', function () { $(this).remove(); }) });                                       
            }
        }
        , function () { }
    );   
});

function showTip(element) {
    $(element).append('<div class="help-tip-content">' + $(element).attr('data-msg') + '</div>');
}

function resizeIframe(iFrames) {
    for (var i = 0, j = iFrames.length; i < j; i++) {
        iFrames[i].style.height = iFrames[i].contentWindow.document.body.offsetHeight + 'px';
    }

    if ($.browser.safari || $.browser.opera) { 

        iFrames.load(function(){
            setTimeout(iResize, 0);
        });

        for (var i = 0, j = iFrames.length; i < j; i++) {
            var iSource = iFrames[i].src;
            iFrames[i].src = '';
            iFrames[i].src = iSource;
        }

    } else {
        iFrames.load(function() {
            this.style.height = this.contentWindow.document.body.offsetHeight + 'px';
        });
    }
}



// this functions ensures only one element gets events 
function stopEventPropigation(e) {
    var e = e || window.event;
    if (jQuery.browser.msie) {
        window.event.cancelBubble = true;
    } else {

        e.stopPropagation();
        //e.preventDefault();
    }
}

$(function() {
    // Overlays
    $('.openoverlay').click(function() {
        var overlay = $(this).data('overlay');
        $(''+ overlay +'').fadeIn();
        $('.overlay-mask').fadeIn();
        return false;
    });

    $('.modal .close').click(function() {
        var overlay = $(this).data('overlay');
        $(''+ overlay +'').fadeOut();
        $('.overlay-mask').fadeOut();
        return false;
    });  
    
    // List view message close
    $("div.message a.close").click(function(){
        $(this).parent().slideUp();
    })      
})

// popup notify msg
var errorTag   = '#ERROR#';
var warningTag = '#WARNING#';
function addNotify(msg){
    var notify = $('.notify');
    var msgClass = 'msg';
    if(msg.indexOf(errorTag)!= -1){
        msgClass = 'error'; 
        msg = msg.replace(errorTag,'');
    } else if(msg.indexOf(warningTag)!= -1){
        msgClass = 'warning'; 
        msg = msg.replace(warningTag,'');        
    } else{ /* DO NOTHING*/ }

    // add notify if it doesn't exist
    if(notify.length <= 0){
        $('body').append('<div class="notify notify-error"><a class="'+msgClass+'">'+msg+'</a></div>');
        notify = $('.notify');
    } else {
        return;
    }
    $(notify).css({'top':'-200px'});    
    $(notify).animate(
        {top:'0px'}
        ,{duration:500, complete: function(){
            $(this).delay(5000).animate({top:'-100px'},{duration:500,complete:function(){$('.notify').remove();}});
    }});
}

function refreshSocialFeed(){
    if (confirm('Are you sure you want to refresh the Home Page Social Feed?')) {
        window.location.href = '/VPanel/config/refreshSocialFeed.php';
    }	
}

function impersonate(value){
    if(value == 'true') {
        if (confirm('You are about to impersonate a user with all roles applied.\nYou will no longer have WebMaster access to items.\nTo exit impersonation, click your Name on the VPanel Overview Page.')) {
            window.location.href = '/VPanel/config/impersonate.php?value=' + value;
        }
    }
    else {
        window.location.href = '/VPanel/config/impersonate.php?value=' + value;	
    }
}