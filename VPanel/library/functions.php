<?php
/* VPANEL Functions  */ 

/* Place Only VPanel functions in this file, all common functions place in site root library, functions */ 

function showHelpTip($message) { 
	$message = addslashes($message); 
	print '<img src="/VPanel/images/icon-help.png" class="help-tip" data-msg="' . $message .'" alt="' . $message .'" />'; 
}

function showInfoTip($message) { 
	$message = addslashes($message); 
	print '<img src="/VPanel/images/info.png" class="help-tip" data-msg="' . $message .'" alt="' . $message .'" />'; 
}

function showLimitedView() {
    print '<style type="text/css">
    .subnav { visibility: hidden }
    .navigation { display: none; }
    section.title { display: none; }
    section.contact { display: none}
</style> '; 
}
?>