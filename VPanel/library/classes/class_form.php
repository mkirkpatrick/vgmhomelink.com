<?php
class Form {

	private $values = array();	//Holds submitted form field values
	private $errors = array();	//Holds submitted form error messages
	private $num_errors;		//The number of errors in submitted form
	private $path;

	/**
	* Get form value and error arrays, used when there
	* is an error with a user-submitted form.
	*/
	//------------------------------
	public function __construct() {
	//------------------------------
		if (!isset($_SESSION))	
			session_start();

		$path		= explode('/', $_SERVER['PHP_SELF']);
		$this->path	= $path[1];

		if (isset($_SESSION['value_array']) && isset($_SESSION['error_array'])) {
			$this->values		= $_SESSION['value_array'];
			$this->errors		= $_SESSION['error_array'];
			$this->num_errors	= count($this->errors);

			unset($_SESSION['value_array']);
			unset($_SESSION['error_array']);
		} else {
			if (isset($_SESSION['arrValue'])) unset($_SESSION['arrValue']);
			$this->num_errors	= 0;
		}
	}


	/**
	* setValue - Records the value typed into the given
	* form field by the user.
	*/
	//------------------------------
	public function setValue($field, $value) {
	//------------------------------
		$this->values[$field]	= $value;
	}


	/**
	* setError - Records new form error given the form
	* field name and the error message attached to it.
	*/
	//------------------------------
	public function setError($field, $errmsg) {
	//------------------------------
		$this->errors[$field]	= "<div class=\"message error\">$errmsg</div>\n";
		$this->num_errors		= count($this->errors);
	}


	/**
	* value - Returns the value attached to the given
	* field, if none exists, the empty string is returned.
	*/
	//------------------------------
	public function value($field, $html='Y') {
	//------------------------------
		if (array_key_exists($field,$this->values)) {
			if (is_array($this->values[$field])) 
				return $this->values[$field];
			else 
				return $html=='Y' ? htmlspecialchars(stripslashes($this->values[$field])) : $this->values[$field];
		} else {
			return "";
		}
	}


	/**
	* error - Returns the error message attached to the
	* given field, if none exists, the empty string is returned.
	*/
	//------------------------------
	public function error($field) {
	//------------------------------
		if (array_key_exists($field,$this->errors)) {
			return "<font size=\"2\" color=\"#ff0000\">" . str_replace('class="errors"', '', $this->errors[$field]) . "</font>";
		} else {
			return "";
		}
	}


   /* getErrorArray - Returns the array of error messages */
	//------------------------------
	public function getErrorArray() {
	//------------------------------
		return $this->errors;
	}


   /* getNumErrors - Returns the number of errors */
	//------------------------------
	public function getNumErrors() {
	//------------------------------
		return $this->num_errors;
	}
};

$form = new Form;
?>