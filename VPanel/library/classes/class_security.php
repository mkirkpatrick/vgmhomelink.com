<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/classes/class_vgm_active_directory.php";

class Security extends Database {

    // set timeout period in seconds
    const TIMEOUT	= 3600;

    //--------------------------------------
    public static function _secureCheck() {
    //--------------------------------------
        global $_WEBCONFIG;

		// the user want to logout
        if (isset($_GET['logout'])) {
            self::_secureLock();
        }

        $returnUrl = isset($_SERVER['ABSOLUTE_URI']) ? '?ReturnUrl=' . urlencode(str_replace("&", "**", htmlspecialchars_decode(str_remove($_SERVER['ROOT_URI'], $_SERVER['ABSOLUTE_URI'])))) : '';
        $homeURL = urlPathCombine($_SERVER['ROOT_URI'], $_WEBCONFIG['VPANEL_PATH'], 'login.php' . $returnUrl);
        if (!isset($_SESSION['user_id'])) {
            redirect($homeURL);
        } else if($_SESSION['user_require_new_pass']
            && !self::isAccountProcessPage() && !self::isSelfPage('account/index.php?view=newpassword')) {
            redirect($_WEBCONFIG['VPANEL_PATH'] . 'account/index.php?view=newpassword');
		} else if($_SESSION['user_expired_pass'] && !$_SESSION['user_require_new_pass'] && !$_SESSION['user_require_questions'] && !$_SESSION['user_req_question_auth']
            && !self::isAccountProcessPage() && !self::isSelfPage('account/index.php?view=expiredpassword')) {
            redirect($_WEBCONFIG['VPANEL_PATH'] . 'account/index.php?view=expiredpassword');
        } else if($_SESSION['user_require_questions'] && !$_SESSION['user_require_new_pass'] && !$_SESSION['user_expired_pass'] && !$_SESSION['user_req_question_auth']
            && !self::isAccountProcessPage() && !self::isSelfPage('account/index.php?view=newquestions')) {
            redirect($_WEBCONFIG['VPANEL_PATH'] . 'account/index.php?view=newquestions');
        } else if(!$_SESSION['user_require_new_pass'] && !$_SESSION['user_expired_pass'] && !$_SESSION['user_require_questions']
            && $_SESSION['user_req_question_auth'] && !self::isAccountProcessPage() && !self::isSelfPage('verify.php')) {
            redirect(urlPathCombine($_WEBCONFIG['VPANEL_PATH'], "verify.php"));
        }

        // check to see if $_SESSION['timeout'] is set
        if (isset($_SESSION['timeout'])) {
            $session_life = time() - $_SESSION['timeout'];
            $timeout = self::TIMEOUT;
            if ($session_life > $timeout) {
                self::_secureLock();
            }
        }

        $_SESSION['timeout'] = time();

		$args = func_get_args();
		$isAuthorized = false;
		if(count($args) > 0) {
			foreach ($args as $arg) {
				if(UserManager::hasRole($arg)) {
					$isAuthorized = true;
				}
			}
		} else {
			$isAuthorized = true;
		}

		if(!$isAuthorized) {
			serverTransfer($_WEBCONFIG['VPANEL_PATH'] . "403error.php");
		}
    }

    //---------------------------------------
    public static function _secureUnlock() {
    //---------------------------------------
        global $_WEBCONFIG;

		$user = parent::quote_smart(substr($_POST['txtUsername'], 0, 50));
        $pass = parent::quote_smart($_POST['txtPassword']);

        if (strlen($user = trim($user)) == 0 || strlen($pass = trim($pass)) == 0) {
            return 'Please enter your Username and Password to log-in.';
        }

        if(!validateRC()) {
            return "401 Unauthorized";
        }

        $sql = "SELECT * FROM tbl_user WHERE user_username = '$user' LIMIT 1";
        $dt	 = parent::Execute($sql);
        $dt->MoveNext();
        if ($dt->Count() > 0 && strcasecmp($dt->user_status, 'Inactive') == 0) {
            self::_addAuditEvent("Login", "Failure", "Account Not Activated", $user);
            return 'Your account is not activated!';
        }
        else if ($dt->Count() > 0 && strcasecmp($dt->user_status, 'Locked') == 0) {
            self::_addAuditEvent("Login", "Failure", "Account Locked", $user);
            return 'Your account has been locked. Please use Forgot Password to reset your password or contact an administrator!';
        }
        else if ($dt->Count() > 0 && $dt->user_active_directory && VGMActiveDirectory::validateActiveDirectoryCredentials($user, $pass)) {
            session_regenerate_id();
            
			$_SESSION['user_csrf_token']     		= md5(uniqid(rand(), true));
            $_SESSION['user_id']		     		= $dt->user_id;
            $_SESSION['user_name']		     		= $dt->user_name;
            $_SESSION['user_username']	     		= $dt->user_username;
            $_SESSION['user_email']		     		= $dt->user_email;
            $_SESSION['user_host']		     		= $dt->user_admin == 'Y';
			$_SESSION['user_require_new_pass'] 	    = false;
			$_SESSION['user_expired_pass']   		= false;
			$_SESSION['user_require_questions'] 	= $dt->user_require_question_change == 'Y';
			$_SESSION['user_last_login'] 	 		= $dt->user_last_login;
            $_SESSION['user_last_password_change'] 	= null;
            $_SESSION['user_active_directory'] 	    = true;
            $_SESSION['site_chief']		     		= false;
            $_SESSION['site_webmaster']	     		= false;
			$_SESSION['user_new_ip'] 				= self::isNewUserIP($_SESSION['user_id']);

            // log the time when the user last login
            $ip	 = $_SERVER['HTTP_CLIENT_IP'];
            $sql = "UPDATE tbl_user SET user_ip_addr = '$ip', user_last_login = Now(), `user_attempts` = 0 WHERE user_id = " . $dt->user_id;
            $res = parent::ExecuteRaw($sql);

			switch ($_WEBCONFIG['AUTHENTICATION_MODE']) {
				case "2SR":
					$_SESSION['user_req_question_auth'] = !$_SESSION['user_require_questions'];
					break;
				case "2SIP":
					$_SESSION['user_req_question_auth'] = $_SESSION['user_new_ip'] && !$_SESSION['user_require_questions'];
					break;
				case "Basic":
					$_SESSION['user_req_question_auth'] = false;
					break;
			}

			if($_SESSION['user_new_ip'] && !$_SESSION['user_req_question_auth']) {
				$sql = "INSERT INTO `tbl_user_ips` (`user_id`,`ui_ip`) VALUES ('{$_SESSION['user_id']}','$ip');";
				Database::ExecuteRaw($sql);
				self::_addAuditEvent("Login", "Success", "Successful Login from New IP", $dt->user_username);
			} else if(!$_SESSION['user_req_question_auth']) {
				self::_addAuditEvent("Login", "Success", "Successful Login from Existing IP", $dt->user_username);
			} else {
				self::_addAuditEvent("Login", "Success", "Successful Login", $dt->user_username);
			}
        }
        else if ($dt->Count() > 0 && strcmp($dt->user_password, self::_hashPassword($pass, $dt->user_password_salt)) != 0) {
            $userAttempts = $dt->user_attempts + 1;
            self::_addAuditEvent("Login", "Failure", "Invalid Password", $user);
            $sql = "UPDATE `tbl_user` SET `user_last_attempt` = NOW(), `user_attempts` = $userAttempts WHERE `user_id` = {$dt->user_id}";
            parent::ExecuteRaw($sql);
            if($userAttempts >= $_WEBCONFIG['MAX_INVALID_PASSWORD_ATTEMPTS']) {
                $sql = "UPDATE `tbl_user` SET `user_status` = 'Locked' WHERE `user_id` = {$dt->user_id}";
                parent::ExecuteRaw($sql);
                $auditDetails = "User account has been locked out due to too many incorrect password attempts. ";
                self::_addAuditEvent("Account Locked", "Success", $auditDetails, $user);
                return 'Your user account has been locked out due to too many incorrect password attempts. Please use Forgot Password to reset your password or contact an administrator!';
            } else {
                return 'Wrong username or password!';
            }
        }
        else if($dt->Count() > 0 && strcmp($dt->user_password, self::_hashPassword($pass, $dt->user_password_salt)) == 0) {
            session_regenerate_id();

			$_SESSION['user_csrf_token']     		= md5(uniqid(rand(), true));
            $_SESSION['user_id']		     		= $dt->user_id;
            $_SESSION['user_name']		     		= $dt->user_name;
            $_SESSION['user_username']	     		= $dt->user_username;
            $_SESSION['user_email']		     		= $dt->user_email;
            $_SESSION['user_host']           		= $dt->user_admin == 'Y';
			$_SESSION['user_expired_pass']   		= strtotime($dt->user_last_password_change . " + {$_WEBCONFIG['PASSWORD_EXPIRE_DAYS']} days") <= strtotime(date("Y-m-d"));
			$_SESSION['user_require_questions'] 	= $dt->user_require_question_change == 'Y';
			$_SESSION['user_require_new_pass'] 	    = $dt->user_require_password_change == 'Y';
			$_SESSION['user_last_login'] 	 		= $dt->user_last_login;
			$_SESSION['user_last_password_change'] 	= $dt->user_last_password_change;
			$_SESSION['site_chief']		     		= false;
            $_SESSION['user_new_ip'] 				= self::isNewUserIP($_SESSION['user_id']);
            $_SESSION['user_active_directory'] 	    = false; 
            $_SESSION['site_webmaster']	     		= false;

            // log the time when the user last login
            $ip	 = $_SERVER['HTTP_CLIENT_IP'];
            $sql = "UPDATE tbl_user SET user_ip_addr = '$ip', user_last_login = Now(), `user_attempts` = 0 WHERE user_id = " . $dt->user_id;
            $res = parent::ExecuteRaw($sql);

			switch ($_WEBCONFIG['AUTHENTICATION_MODE']) {
				case "2SR":
					$_SESSION['user_req_question_auth'] = !$_SESSION['user_require_questions'];
					break;
				case "2SIP":
					$_SESSION['user_req_question_auth'] = $_SESSION['user_new_ip'] && !$_SESSION['user_require_questions'];
					break;
				case "Basic":
					$_SESSION['user_req_question_auth'] = false;
					break;
			}

			if($_SESSION['user_new_ip'] && !$_SESSION['user_req_question_auth']) {
				$sql = "INSERT INTO `tbl_user_ips` (`user_id`,`ui_ip`) VALUES ('{$_SESSION['user_id']}','$ip');";
				Database::ExecuteRaw($sql);
				self::_addAuditEvent("Login", "Success", "Successful Login from New IP", $dt->user_username);
			} else if(!$_SESSION['user_req_question_auth']) {
				self::_addAuditEvent("Login", "Success", "Successful Login from Existing IP", $dt->user_username);
			} else {
				self::_addAuditEvent("Login", "Success", "Successful Login", $dt->user_username);
			}
        }
        else if($_WEBCONFIG['ENABLE_WEBMASTER_LOGIN'] == "true" && ActiveDirectory::validateWebmasterAdmin($user, $pass)) {
            session_regenerate_id();
            $webmaster = ActiveDirectory::getWebmasterInformation($user);
			$datetime = new DateTime($webmaster->LastLoginDate);
			$lastLoginDate = $datetime->format('Y-m-d H:i:s');

			$_SESSION['user_csrf_token']     		= md5(uniqid(rand(), true));
            $_SESSION['user_id']		     		= '0';
            $_SESSION['user_name']		     		= (string)$webmaster->FullName;
            $_SESSION['user_username']	     		= (string)$webmaster->UserName;
            $_SESSION['user_email']		     		= (string)$webmaster->EmailAddress;
            $_SESSION['user_host']		     		= true;
			$_SESSION['user_require_new_pass'] 	    = false;
			$_SESSION['user_expired_pass']   		= false;
			$_SESSION['user_require_questions'] 	= false;
			$_SESSION['user_last_login'] 	 		= $lastLoginDate;
			$_SESSION['user_last_password_change'] 	= null;
            $_SESSION['site_chief']		     		= true;
            $_SESSION['site_webmaster']	     		= true;
            $_SESSION['user_req_question_auth']	    = false;
            $_SESSION['user_active_directory'] 	    = false; 

            self::_addAuditEvent("Login", "Success", "Forbin Webmaster Login", $user);
        }
        else {
            self::_addAuditEvent("Login", "Failure", "Invalid Username", $user);
            return 'Wrong username or password!';
        }
        if(!isset($_SESSION['ReturnURL'])) {
            $returnUrl = isset($_POST['hidReturnUrl']) && !isNullOrEmpty($_POST['hidReturnUrl']) ? urldecode(html_entity_decode($_POST['hidReturnUrl'])) : $_WEBCONFIG['VPANEL_PATH'];
            $_SESSION['ReturnURL'] = $_SERVER['SERVER_NAME'] . $returnUrl;
        }

        $userCount = new visitor($_SESSION['user_username']);
        if($_SESSION['user_req_question_auth']) {
            redirect(urlPathCombine($_WEBCONFIG['VPANEL_PATH'], "verify.php"));
        } else {
			if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
				header('Location: https://' . $_SESSION['ReturnURL']);
				exit;
			} else {
				header('Location: http://' . $_SESSION['ReturnURL']);
				exit;
			}
        }
    }

    //-------------------------------------
    public static function _ForgotPass() {
    //-------------------------------------
        global $_WEBCONFIG;

        if(!isset($_REQUEST['email'])) {
            header("HTTP/1.0 400 Bad Request");
            header("Location: login.php");
            exit;
        }
        
        $email = isset($_POST['email']) ? parent::quote_smart(substr($_POST['email'], 0, 255)) : ''; 
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            self::_addAuditEvent("Forgot Password", "Failure", "Invalid Email Address Entered ($email)");
            return "You must enter a valid email address";
        } else if (!isNullOrEmpty($email)) {

            $sql = "SELECT user_username, user_name, user_id 
                    FROM tbl_user 
                    WHERE user_email = '$email' 
                    LIMIT 1";
            $dt     = parent::Execute($sql);
            $dt->MoveNext();

            if ($dt->Count() == 0) {
                self::_addAuditEvent("Forgot Password", "Failure", "Email not found: $email");
                return 'If the e-mail address you entered is associated with an account in our record, you will receive an e-mail with instructions on resetting your password.';
            } else {
                $newpass = self::_generateStrongPassword($_WEBCONFIG['MIN_PASSWORD_LENGTH']);
                $newSalt = self::_generateRandomSalt(24);
                // update new password
                $sql = "UPDATE tbl_user SET user_require_password_change = 'Y', user_password = '" . self::_hashPassword($newpass, $newSalt) . "',
                        user_password_salt = '" . $newSalt . "', user_attempts = 0, user_status = 'Active'
                        WHERE user_id = " . $dt->user_id;
                $res = parent::ExecuteRaw($sql);

                self::_addAuditEvent("Forgot Password", "Success", "", $dt->user_username);

                // send new password
                $from     = $_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'];
                $recipient = $email;
                $headers = "From: $from\r\n";
                $subject = "Important information about your account.";
                $body     = $dt->user_name . ",\n\n"
                . "We've generated a new password for you for security reasons, "
                . "you can use this new password with your username to log in to the Site.\n\n"
                . "User: " . $dt->user_username . "\n\n"
                . "New Password: " . $newpass . "\n\n"
                . "It is recommended that you change your password to something that is easier to remember, "
                . "which can be done by going to the User page after signing in.\n\n";

                if (mail($recipient, $subject, $body, $headers)) {
                    return 'OK';
                } else {
                    return 'An Unknown Error Has Occured!';
                }
            }
        } else {
            self::_addAuditEvent("Forgot Password", "Failure", "No Email Entered");
            return 'Please enter your email address that is associated with your account.';
        }
    }

    //-------------------------------------
    public static function _secureLock() {
    //-------------------------------------
        global $_WEBCONFIG;

        $returnUrl = isset($_SERVER['HTTP_REFERER']) ? '?ReturnUrl=' . urlencode(str_remove($_SERVER['ROOT_URI'], $_SERVER['HTTP_REFERER'])) : '';
        $returnUrl = str_replace("%26","**", $returnUrl);
        
        // Remove main session variable
        if (isset($_SESSION['user_id'])) {
            unset($_SESSION['user_id']);
        }
        if (isset($_SESSION['user_name'])) {
            unset($_SESSION['user_name']);
        }
        if (isset($_SESSION['user_username'])) {
            unset($_SESSION['user_username']);
        }
        if (isset($_SESSION['user_email'])) {
            unset($_SESSION['user_email']);
        }
        if (isset($_SESSION['user_host'])) {
            unset($_SESSION['user_host']);
        }
        if (isset($_SESSION['site_chief'])) {
            unset($_SESSION['site_chief']);
        }
        if (isset($_SESSION['timeout'])) {
            unset($_SESSION['timeout']);
        }
		if (isset($_SESSION['ReturnURL'])) {
            unset($_SESSION['ReturnURL']);
        }

        session_unset();
        session_destroy();
		session_start();
		session_regenerate_id();

        $homeURL = urlPathCombine($_SERVER['ROOT_URI'], $_WEBCONFIG['VPANEL_PATH'], 'login.php' . $returnUrl);
        redirect($homeURL);
    }

    //----------------------------------------------------
    public static function _generateRandomSalt($length) {
    //----------------------------------------------------
        $seed = str_split('abcdefghijklmnopqrstuvwxyz' . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' . '0123456789!@#%^&*()');
        shuffle($seed);
        $rand = '';
        foreach (array_rand($seed, $length) as $k) $rand .= $seed[$k];
        return $rand;
    }

	//----------------------------------------------------
    private static function isAccountProcessPage() {
    //----------------------------------------------------
        return endsWith($_SERVER['SCRIPT_NAME'], 'process.php');
    }

	//----------------------------------------------------
    private static function isSelfPage($page) {
    //----------------------------------------------------
        global $_WEBCONFIG;
        return strcasecmp($_SERVER['REQUEST_URI'], $_WEBCONFIG['VPANEL_PATH'] . $page) == 0;
    }

    //----------------------------------------------------------------------------------------------
    public static function _hashPassword($password, $salt, $iterations = 64000, $alfo = 'sha512') {
    //----------------------------------------------------------------------------------------------
        return hash_pbkdf2($alfo, $password, $salt, $iterations);
    }

    //----------------------------------------------------------------------------------------------------------
    public static function _generateStrongPassword($length = 12, $add_dashes = false, $available_sets = 'luds') {
    //----------------------------------------------------------------------------------------------------------
        $sets = array();
        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if(strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&?';
        $all = '';
        $password = '';
        foreach($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if(!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while(strlen($password) > $dash_len)
        {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    //----------------------------------------------------------------------------------------
    public static function _addAuditEvent($eventname, $detail, $notes = "", $username = "") {
    //----------------------------------------------------------------------------------------
        global $_WEBCONFIG;

        $eventname = htmlEntities(parent::quote_smart($eventname), ENT_QUOTES, "utf-8");
        $detail    = htmlEntities(parent::quote_smart($detail), ENT_QUOTES, "utf-8");
        $notes     = htmlEntities(parent::quote_smart($notes), ENT_QUOTES, "utf-8");
        $username  = htmlEntities(parent::quote_smart($username), ENT_QUOTES, "utf-8");
        $useragent = isset($_SERVER['HTTP_USER_AGENT']) ? htmlEntities(parent::quote_smart($_SERVER['HTTP_USER_AGENT']), ENT_QUOTES, "utf-8") : '';
        $referrer  = isset($_SERVER['HTTP_REFERRER']) ? htmlEntities(parent::quote_smart($_SERVER['HTTP_REFERER']), ENT_QUOTES, "utf-8") : '';
        $userId    = isset($_SESSION['user_id']) ? (int) $_SESSION['user_id'] : 0;
        $name      = isset($_SESSION['user_name']) ? parent::quote_smart($_SESSION['user_name']) : 'SYSTEM';

        $ip = $_SERVER['HTTP_CLIENT_IP'];
        $user = isset($_SESSION['user_username']) ? $_SESSION['user_username'] : (strlen($username) > 50 ? substr($username, 0, 50) : $username);
        if($_WEBCONFIG['ENABLE_WEBMASTER_LOGIN'] == "true" && ActiveDirectory::isValidWebmaster($user)) {
            $user = "~" . $user;
        }
        else if(!self::userExists($user)) {
            $user = "(" . $user . ")";
        }
        else {
            $sql = "UPDATE `tbl_user` SET `user_last_activity` = NOW() WHERE `user_id` = $userId;";
            parent::ExecuteRaw($sql);
        }

        $sql = "INSERT INTO `tbl_audit_log` (`date`, `name`, `detail`, `ip`, `additionaldata`, `user`, `useragent`, `referrer`, `user_name`)
                VALUES(NOW(), '$eventname', '$detail', '$ip', '$notes', '$user', '$useragent', '$referrer', '$name')";
        parent::ExecuteRaw($sql);
    }

    //-------------------------------------------
    private static function userExists($user) {
    //-------------------------------------------
        global $_WEBCONFIG;

        $user = parent::quote_smart($user);

        $sql = "SELECT user_id FROM tbl_user WHERE user_username = '$user' LIMIT 1";
        $dt = parent::Execute($sql);
        $dt->MoveNext();

        if ($dt->Count() == 0 && $user <> "") {
            return false;
        } else {
            return true;
        }
    }

	//-------------------------------------------
    private static function isNewUserIP($userId) {
    //-------------------------------------------
        global $_WEBCONFIG;

        $userId = (int) $userId;

        $sql = "SELECT `ui_id` FROM `tbl_user_ips` WHERE user_id = '$userId' LIMIT 1";
        $dt = parent::Execute($sql);

        if ($dt->Count() == 1) {
            return false;
        } else {
            return true;
        }
    }

    //--------------------------------------
    public static function _webmasterCheck($return='403error.php') {
    //--------------------------------------
        global $_WEBCONFIG;
        if($_SESSION['site_chief'] && !isset($_SESSION['REMOVE_WEBMASTER'])) {
            return true;
        }
        else {
            serverTransfer($_WEBCONFIG['VPANEL_PATH'] . $return);
        }

    }

    //------------------------------
     public static function _generateResetPassLink($length = 25) {
    //------------------------------
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@-';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    //------------------------------
    public static function validateCsrfToken() {
    //------------------------------
         if(!isset($_REQUEST['csrfToken']) || $_REQUEST['csrfToken'] != $_SESSION['user_csrf_token'] )  {
             serverTransfer($_WEBCONFIG['VPANEL_PATH'] . "403error.php");
         }
     }

     //------------------------------
     public static function getCsrfToken() {
     //------------------------------
         return $_SESSION['user_csrf_token']; 
     }

};
?>