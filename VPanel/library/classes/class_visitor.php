<?php
class visitor extends Database {

    const TIMEOUT = 3600;
    private $count = 0;
	private $ip;
	private $timestamp;

	//------------------------------
	public function __construct($username) {
	//------------------------------
		parent::__construct();
        $this->timestamp = time();
        self::ipCheck();
        self::newUser($username);
        self::deleteUser();
        self::_countUsers();
	}

    //------------------------------
    public function _countUsers() {
        //------------------------------
		$sql = "SELECT DISTINCT vo_ip FROM tbl_visitors";
		$res = $this->ExecuteRaw($sql);
		$num = $this->Count($res);
        return $num;
	} // end function

    /*
    This function checks if user is coming behind proxy server. Why is this important?
    If you have high traffic web site, it might happen that you receive lot of traffic
    from the same proxy server (like AOL). In that case, the script would count them all
        as 1 user.
    This function tryes to get real IP address.
    Note that getenv() function doesn't work when PHP is running as ISAPI module
    */
	//------------------------------
    private function ipCheck() {
	//------------------------------
        if (getenv('HTTP_CLIENT_IP')) {
            $this->ip = getenv('HTTP_CLIENT_IP');
        }
        elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $this->ip = getenv('HTTP_X_FORWARDED_FOR');
        }
        elseif (getenv('HTTP_X_FORWARDED')) {
            $this->ip = getenv('HTTP_X_FORWARDED');
        }
        elseif (getenv('HTTP_FORWARDED_FOR')) {
            $this->ip = getenv('HTTP_FORWARDED_FOR');
        }
        elseif (getenv('HTTP_FORWARDED')) {
            $this->ip = getenv('HTTP_FORWARDED');
        }
        else {
            $this->ip = $_SERVER['REMOTE_ADDR'];
        }
	} // end function

	//------------------------------
    private function newUser($username) {
	//------------------------------
		$sql = "INSERT INTO tbl_visitors(username, vo_ip) VALUES ('$username', '$this->ip')";
		$res = $this->ExecuteRaw($sql);
	} // end function

	//------------------------------
    private function deleteUser() {
	//------------------------------
        $timeSpan = $this->timestamp - self::TIMEOUT;
		$sql = "DELETE FROM tbl_visitors WHERE vo_timestamp < $timeSpan";
		$res = $this->ExecuteRaw($sql);
	} // end function
};
?>