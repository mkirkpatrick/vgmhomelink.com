<?php 
class UserManager { 

	//------------------------------
	public static function autoGeneratePassword($userId, $isNewLogin = false) {
	//------------------------------
		global $_WEBCONFIG, $siteConfig; 
		
		$userId = (int) $userId; 
		$record = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $userId);
		if(isset($record) && !is_null($record)) { 
			$newSalt = Security::_generateRandomSalt(24); 
			$password = Security::_generateStrongPassword(); 
			$record->user_password_salt = $newSalt; 
			$record->user_password = Security::_hashPassword($password, $newSalt);
			$record->user_require_password_change = 'Y'; 
			$record->user_status = 'Active'; 
			$record->user_attempts = 0; 
			$record->user_password_generated = date("Y-m-d G:i:s");
			Repository::Update($record); 
			
			if($isNewLogin) { 
				$htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "/emailtemplates/new-user.html");			
			} else {
				Security::_addAuditEvent("Password Reset", "Success", "Reset Password for username -- '" . $record->user_username . "' (" . $record->user_id .")", $_SESSION['user_username']);
				$htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "/emailtemplates/password-reset.html"); 
			}				
			if(file_exists($htmlFile)) { 
				$fh = fopen($htmlFile, 'r');
				$body = fread($fh, filesize($htmlFile));
				fclose($fh); 
			} else {
				throw new exception("Email Template File Not Found"); 
			}
			
			$mergeFields = array(); 
			$mergeFields["**USERNAME**"] = $record->user_username; 
			$mergeFields["**PASSWORD**"] = $password;
            $mergeFields["**SITEURL**"] = filePathCombine($_WEBCONFIG['SITE_URL'], $_WEBCONFIG['VPANEL_PATH']);
			$mergeFields["**SITEDISPLAYURL**"] = $_WEBCONFIG['SITE_DISPLAY_URL'];
			$mergeFields["**COMPANYNAME**"] = $siteConfig['Company_Name'];
			$mergeFields["**VPANELURL**"] = urlPathCombine($_WEBCONFIG['SITE_URL'], $_WEBCONFIG['VPANEL_PATH']); 
			$mergeFields["**VPANELVERSION**"] = $_WEBCONFIG['VPANEL_VERSION']; 
			
			$body = strtr($body, $mergeFields);
			
			$mailer = new vMail();
			$mailer->addRecipient($record->user_email, $record->user_name);
			$mailer->setSubject("{$siteConfig['Company_Name']} VPanel");
			$mailer->setMailType("html"); 
			$mailer->setFrom($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel");
			$mailer->setReplyTo($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel"); 
			$mailer->setMessage($body); 			
			$mailer->sendMail();
			
			$_SESSION['processed'] = 'reset'; 
		} else {
			throw new Exception("User with ID "  . $userId . " does not exist");
		}
	}
	
    public static function getRoleOptions($userId = 0) {
        if(!is_numeric($userId)) {
            throw new InvalidArgumentException('The first parameter for getRoleOptions() must be an Integer. Input was: '.$userId);
        }
        $sql = "SELECT ur.urole_id, ur.name, ur.description
                FROM tbl_user_roles AS ur ";
        $roles = Database::Execute($sql);
        $html = ''; 
        if ($roles->Count() > 0) {
            while ($roles->MoveNext()) {
                $active = self::hasRole($roles->urole_id, $userId, false) ? 'checked' : ''; 
                $html .= '&nbsp;&nbsp;<img data-msg="' . $roles->description . '" class="help-tip" src="/VPanel/images/icon-help.png">&nbsp;&nbsp;' . PHP_EOL; 
                $html .= '<label><input type="checkbox" name="cboRoles[]" value="' . $roles->urole_id . '" ' . $active . '>&nbsp&nbsp' . $roles->name .'</label><br />' . PHP_EOL; 
            }
        }
        return $html; 
    }

    public static function getUserRoles($userId) {
        if(!is_numeric($userId)) {
            throw new InvalidArgumentException('The first parameter for getUserRoles() must be an Integer. Input was: '.$userId);
        }
        $sql = "SELECT ur.urole_id, ur.name, ur.description
                FROM tbl_user_roles AS ur ";
        $roles = Database::Execute($sql);
        $aRoles = array(); 
        if ($roles->Count() > 0) {
            while ($roles->MoveNext()) {
                if(self::hasRole($roles->urole_id, $userId, true)) {
                    array_push($aRoles, $roles->urole_id);
                }
            }
        }
        return $aRoles; 
    }

    public static function hasRole($roleId, $userId = '', $allowAdmin = true) {
        if(!is_numeric($roleId)) { 
            throw new InvalidArgumentException('The first parameter for hasRole() must be an Integer. Input was: '.$roleId);
        }
        if($allowAdmin && (self::isMasterAdmin())) {
            return true;    
        }  
        else if(strlen($userId) == 0) $userId = $_SESSION['user_id'];     
        $sql   = "SELECT `user_id` FROM `tbl_users_in_roles` WHERE user_id = '$userId' AND urole_id = $roleId LIMIT 1";  
        $roles = Database::Execute($sql); 
        return $roles->Count() > 0; 
    }

    public static function isWebmaster() {
        if(isset($_SESSION['REMOVE_WEBMASTER'])) {
            return false;
        }
        else if($_SESSION['site_webmaster']) {
            return true;    
        } else {
            return false; 
        }

    }
    
    public static function isMasterAdmin() {
        if($_SESSION['user_host']) return true; 
    }

    public static function getRoleId($roleName){
        $sql = "SELECT `urole_id` AS id
                FROM `tbl_user_roles` 
                WHERE `name` = '$roleName'
                LIMIT 1 ";
        $roles = Database::Execute($sql);
        $roles->MoveNext(); 
        return (int) $roles->id; 
    }

    public static function getUsersByRole($id){
        if(!is_numeric($id))
            throw new InvalidArgumentException('The first parameter for getUsersByRole() must be an Integer. Input was: '.$id);
        $id = (int) $id; 
        $sql = "SELECT user_id FROM tbl_users_in_roles WHERE urole_id='". $id."'";        
        $userids = Database::Execute($sql);

        if($userids->Count() <= 0){
            return false;
        }
        else { 
            $userIdList = '';
            while($userids->MoveNext()){ 
                if($userIdList == ''){ $userIdList .= $userids->user_id; }else{ $userIdList .= ','.$userids->user_id;  }
            }
            $sql = "SELECT * FROM tbl_user WHERE user_id IN (".$userIdList.") AND user_status = 'Active' ";        
            $userList = Database::Execute($sql);
            if($userList->Count() <= 0){
                return false;
            }
            else {
                $userStack = Array();
                while($userList->MoveNext()){
                    $userStack[$userList->user_id] = Array(
                        "id"=>$userList->user_id 
                        ,"name"=>$userList->user_name 
                        ,"email"=>$userList->user_email
                        ,"username"=>$userList->user_username 
                        ,"isAdmin"=>($userList->user_admin == 'Y')
                    );
                }
                return $userStack;
            }
        }
    }
}
?>