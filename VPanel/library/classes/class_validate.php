<?php 
class Validate {
	
	const filter_validation = true;
	
	//------------------------------
	public static function isValidEmail($email) {
	//------------------------------
	
    $email =  strtolower($email);
 
    //check if email seems valid
    if (preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $email)) {
      return $email;
    }
    return false;
 
  }
 
	//------------------------------
	public static function isValidPhone($phone, $ext = false) {
	//------------------------------
 
    //remove everything but numbers
    $numbers = preg_replace("%[^0-9]%", "", $phone );
 
    //how many numbers are supplied
    $length = strlen($numbers);
 
    if ( $length == 10 || $length == 7 ) { //Everything is find and dandy
 
      $cleanPhone = $numbers;
 
      if ( $ext ) {
        $clean['phone'] = $cleanPhone;
        return $clean;
      } else {
        return $cleanPhone;
      }
 
    } elseif ( $length > 10 ) { //must be extension
 
      //checks if first number is 1 (this may be a bug for you)
      if ( substr($numbers,0,1 ) == 1 ) {
        $clean['phone'] = substr($numbers,0,11);
        $clean['extension'] = substr($numbers,11);
      } else {
        $clean['phone'] = substr($numbers,0,10);
        $clean['extension'] = substr($numbers,10);
      }
 
      if (!$ext) { //return string
 
        if (!empty($clean['extension'])) {
          $clean = implode("x",$clean);
        } else {
          $clean = $clean['phone'];
        } 
 
        return $clean;
 
 
      } else { //return array
 
        return $clean;
      }
    } 
 
    return false;
 
  }

	//------------------------------
	public static function isValidZipCode($zip) {
	//------------------------------
    return preg_match("/^([0-9]{5})(-[0-9]{4})?$/i",$zip); 
  }

	//------------------------------
	public static function isValidIP($ip) {
	//------------------------------
		if (empty($ip) || $ip == NULL) 
			return FALSE;

		if (self::filter_validation) {
			return filter_var($ip, FILTER_VALIDATE_IP);
		} else {
			return preg_match("/^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])" . 
			"(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$/", $ip );
		}
	}

	//------------------------------
	public static function isValidURL($url) {
	//------------------------------
		if (empty($url) || $url == NULL) 
			return FALSE;

		if (self::filter_validation) {
			return filter_var($url, FILTER_VALIDATE_URL);
		} else {
			$regex = "((https?|ftp)\:\/\/)?"; // Scheme 
			$regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass 
			$regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP 
			$regex .= "(\:[0-9]{2,5})?"; // Port 
			$regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path 
			$regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query 
			$regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor 
			return preg_match("/^$regex$/", $url);
		}
	}

	//------------------------------
	public static function isValidUSSocialSecurity($ssn) {
	//------------------------------
		if (empty($ssn) || $ssn == NULL) 
			return FALSE;

		# eg. 531-63-5334  
		$regex = "/^[\d]{3}-[\d]{2}-[\d]{4}$/";
		return preg_match($regex, $ssn);
	}

	//------------------------------
	public static function isValidCreditCard($cc) {
	//------------------------------
		if (empty($cc) || $cc == NULL) 
			return FALSE;

		# eg. 718486746312031  
		$regex = "/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6011[0-9]{12}|3(?:0[0-5]|[68][0-9])[0-9]{11}|3[47][0-9]{13})$/";
		return preg_match($regex, $cc);
	}

	//------------------------------
	public static function isValidColor($color) {
	//------------------------------
		if (empty($color) || $color == NULL) 
			return FALSE;

		#CCC  
		#FFFFF  
		$regex = "/^#(?:(?:[a-f0-9]{3}){1,2})$/i";
		return preg_match($regex, $color);
	}

	//------------------------------
	public static function isValidPassword($password, $length) {
	//------------------------------
		if (empty($password) || $password == NULL) 
			return FALSE;

		# Must contain ? characters, 1 uppercase, 1 lowercase and 1 number 
		$regex = "/^.*(?=.{" . $length . ",})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/";
		return preg_match($regex, $password);
	}

	//------------------------------
	public static function isMatchedPasswords($password1, $password2) {
	//------------------------------
		if (empty($password1) || $password1 == NULL || empty($password2) || $password2 == NULL) 
			return FALSE;

		# Case sensitive
		return strcmp($password1, $password2) == 0;
	}

	//------------------------------
	public static function isValidDocument($file) {
	//------------------------------
		if (empty($file) || $file == NULL) 
			return FALSE;

		$document_ext_allowed = array("csv","doc","pdf","pps","ppt","txt","xls");
		$ext = strtolower(substr(strrchr($file, "."), 1));
		return in_array($ext, $document_ext_allowed);
	}

	//------------------------------
	public static function isValidImage($file) {
	//------------------------------
		if (empty($file) || $file == NULL) 
			return FALSE;

		$image_ext_allowed = array("jpg", "jpeg", "png", "gif","bmp");
		$ext = strtolower(substr(strrchr($file, "."), 1));
		return in_array($ext, $image_ext_allowed);
	}

	//------------------------------
	public static function isValidMIMEImage($file_info) {
	//------------------------------
		if (empty($file_info) || $file_info == NULL) 
			return FALSE;

		$mime = array('image/gif' => 'gif',
					  'image/jpeg' => 'jpeg',
					  'image/png' => 'png',
					  'application/x-shockwave-flash' => 'swf',
					  'image/psd' => 'psd',
					  'image/bmp' => 'bmp',
					  'image/tiff' => 'tiff',
					  'image/tiff' => 'tiff',
					  'image/jp2' => 'jp2',
					  'image/iff' => 'iff',
					  'image/vnd.wap.wbmp' => 'bmp',
					  'image/xbm' => 'xbm',
					  'image/vnd.microsoft.icon' => 'ico');
		$file_mime = $file_info['mime'];
		return array_key_exists($file_mime, $mime);
	}

	//------------------------------
	public static function isImageExist($url) {
	//------------------------------
		if(@file_get_contents($url,0,NULL,0,1)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
 
}
/** End Validation **/
?>