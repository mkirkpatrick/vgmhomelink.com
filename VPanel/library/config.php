<?php 
/* Global Settings */ 
ini_set("log_errors", 1);
ini_set("session.cookie_httponly", 1);
ini_set('max_execution_time', '30'); 
ini_set('session.gc_maxlifetime', 15*60);
set_time_limit(30);
date_default_timezone_set('US/Central');
ini_set('memory_limit', '16M');

/* Start Session */
if(!headers_sent() && !isset($_SESSION)) {
	session_start(); 
}
$_WEBCONFIG = array(); 

/* Load Site Config */
$configPath = './'; 
while(is_readable($configPath)) { 
	if (file_exists($configPath . 'web.config')) {
		$configXML = simplexml_load_file($configPath . 'web.config');
		if(isset($configXML->appSettings))
		{
			foreach ($configXML->appSettings->add as $setting) {
				if(!isset($_WEBCONFIG[(string)$setting->attributes()->key])) { 
					$_WEBCONFIG[(string)$setting->attributes()->key] = (string) html_entity_decode($setting->attributes()->value); 
				}
			}
		}
		if(isset($configXML->connectionStrings))
		{
			foreach ($configXML->connectionStrings->add as $connectionString) {
				if(!isset($_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name])) { 
					$_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name] = array(); 
					$connData = explode(";", (string)$connectionString->attributes()->connectionString); 
					$_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['HOST'] = str_replace("=", "", stristr($connData[0], '='));  
					$_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['DATABASE'] = str_replace("=", "", stristr($connData[1], '='));
					$_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['USERNAME'] = str_replace("=", "", stristr($connData[2], '='));
					$_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['PASSWORD'] = str_replace("=", "", stristr($connData[3], '='));
				}
			}
		}		
	}
	$configPath = '../'. $configPath; 
} 
if(count($_WEBCONFIG) == 0) { 
		die("web.config Configuration not setup properly"); 
}

require_once $_SERVER['DOCUMENT_ROOT'] . "/library/constants.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/functions.php"; 
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/error_handler.php";
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/database/class_database.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/database/class_datatable.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/database/class_entity.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/database/class_repository.php");
if(stristr($_SERVER['ABSOLUTE_URI'], $_WEBCONFIG['VPANEL_PATH'])) {
	require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/functions.php");
	require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
	require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "users/classes/class_user_manager.php");
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/VPanel/app_offline.htm')) {
		print file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/VPanel/app_offline.htm'); 
		exit; 
	}
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/app_offline.htm')) {
		print file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/app_offline.htm'); 
		exit; 
}

$siteConfig	 = array(); 
$sql		= "SELECT name,value FROM tbl_site_config LIMIT 100";
$siteData	= Database::Execute($sql);
if($siteData->Count() == 0) {
	die("Database Site Configuration not setup properly"); 
}
while($siteData->MoveNext()) {
	$siteConfig[$siteData->name] = $siteData->value; 
}
unset($siteData);
?>