<? 

class process extends Database { 

	private $record;
	public $params;
	
	//------------------------------
	public function __construct() {
	//------------------------------
		parent::__construct();
	}

    //------------------------------
	public function _export() {
	//------------------------------
        $user = isset($_GET['userID']) ? xss_sanitize($_GET['userID']) : "";
		exportAuditTableToCsv('tbl_audit_log',$user); 
		exit; 
	} 
};
?>