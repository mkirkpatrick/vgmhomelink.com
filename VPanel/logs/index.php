<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 

security::_secureCheck(AUDIT_LOG_MANAGER_ROLE);

if (!isset($_GET['logout'])) 
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

print '<section class="title">
		<div class="container clearfix">
			<h1 class="page-title icon-users">Audit Logs</h1>
			<p class="page-desc">User Security Audit Log</p>
		</div>
	</section>';
print '<section class="maincontent"><div class="container clearfix">';
include('navBar.php');

$rows = 0;
$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {   
	case 'logdata' :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/logdata.js');
		include_once 'logdata.php';		
		break;

	default :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/logdata.js');
		include_once 'logdata.php';
		break;
}

print '</div></section>';

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>