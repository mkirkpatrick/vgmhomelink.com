<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'] . "library/classes/class_security.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'] . "library/classes/class_visitor.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'] . "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'] . "includes/inc_header.php");

print '<section class="title">
			<div class="container clearfix">
				<h1 class="page-title icon-tools">Modules</h1>
				<p class="page-desc">Manage all the addons of your website.</p>
			</div>
		</section>';
print '<section class="maincontent"><div class="container clearfix">';

include_once "main.php"; 

print '</div></section>';

include filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'] . "includes/inc_pgbot.php");
?>