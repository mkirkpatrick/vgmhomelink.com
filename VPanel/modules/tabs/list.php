<? if (!isset($siteConfig)) die("System Error!"); ?> 

<div class="subcontent right last">
    <?
    print '<h1>' . $_WEBCONFIG['MODULE_NAME'] . '</h1>';

    if ($form->getNumErrors() > 0) {
        $errors	= $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } 
    else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
                break;
            case 'updated':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                break;
            case 'deleted':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                break;
        }
        unset($_SESSION['processed']);
    }

    $sql = "SELECT * FROM {$_WEBCONFIG['MODULE_TABLE']} WHERE at_id = {$_WEBCONFIG['Asset_ID']} ORDER BY as_name";
    $record	= Database::Execute($sql);

    print $record->Count() == 1 ? "<p>There is currently <b>1</b> " . strtolower($_WEBCONFIG['ENTITY_NAME']) . "</p>\n" : "<p>There are currently <b>" . $record->Count() . "</b> " . strtolower($_WEBCONFIG['ENTITY_NAME']) . "s. </p>\n";
    ?>

    <table id="grid">
        <thead> 
            <tr> 
                <th>Name</th>
                <th>Asset Key</th>
                <th>Status</th>
                <th>Last Modified</th>
                <th width="15%" data-sortable="false" data-filterable="false" style="text-align: center;">Options</th>
            </tr>
        </thead>
        <tbody>

            <?
            if ($record->Count() > 0) {
                while ($record->MoveNext()) {
                    print '<tr> 
                                <td>' . $record->as_name . '</td>
                                <td>' . $record->as_key . '</td>
                                <td>' . $record->as_status . '</td>
                                <td>' . date("m/d/Y g:i A", strtotime($record->as_last_update)) . '</td>
                                <td>
                                    <div align="center">
                                        <a href="javascript:modifyTab(' . $record->as_id . ');" class="green button-slim">Modify</a>
                                        <a href="javascript:deleteTab(' . $record->as_id . ');" class="red button-slim">Delete</a>
                                    </div>
                                </td>
                           </tr>' . PHP_EOL;
                }// end while
            }
            ?>

        </tbody>
    </table>
    <div class="buttons clearfix">
        <a href="javascript:history.back()" class="button blue floatLeft">Back</a>    
        <a href="javascript:addTab()" class="button green floatRight">Add New <?= $_WEBCONFIG['ENTITY_NAME'] ?></a>
    </div>
</div><!--End continue-->

<script type="text/x-kendo-template" id="toolBarTemplate">
    <div class="toolbar" style="float:right;">
        <div>
            <input id="txtSearch" type="text" style="width: 175px" placeholder="Search Grid" />
            <a id="clearTextButton">Clear</a>
        </div>
    </div>
</script>