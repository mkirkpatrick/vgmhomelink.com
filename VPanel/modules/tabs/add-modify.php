<?
if (!isset($siteConfig)) 
    die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0; 

if ($form->getNumErrors() > 0) {
    $hidId        = $form->value("hidId");
    $txtAssetName = $form->value("txtAssetName");
    $hidTabName1  = strlen($form->value("hidTabName1")) > 0 ? $form->value("hidTabName1") : "Tab 1";
    $hidTabName2  = strlen($form->value("hidTabName2")) > 0 ? $form->value("hidTabName2") : "Tab 2";
    $hidTabName3  = strlen($form->value("hidTabName3")) > 0 ? $form->value("hidTabName3") : "Tab 3";
    $hidTabName4  = strlen($form->value("hidTabName4")) > 0 ? $form->value("hidTabName4") : "Tab 4";
    $hidTabName5  = strlen($form->value("hidTabName5")) > 0 ? $form->value("hidTabName5") : "Tab 5";
    $hidTabName6  = strlen($form->value("hidTabName6")) > 0 ? $form->value("hidTabName6") : "Tab 6";
    $hidTabName7  = strlen($form->value("hidTabName7")) > 0 ? $form->value("hidTabName7") : "Tab 7";
    $hidTabName8  = strlen($form->value("hidTabName8")) > 0 ? $form->value("hidTabName8") : "Tab 8";
    $hidTabName9  = strlen($form->value("hidTabName9")) > 0 ? $form->value("hidTabName9") : "Tab 9";
    $hidTabName10 = strlen($form->value("hidTabName10")) > 0 ? $form->value("hidTabName10") : "Tab 10";

    $tabContent1  = $form->value("tabContent1");
    $tabContent2  = $form->value("tabContent2");
    $tabContent3  = $form->value("tabContent3");
    $tabContent4  = $form->value("tabContent4");
    $tabContent5  = $form->value("tabContent5");
    $tabContent6  = $form->value("tabContent6");
    $tabContent7  = $form->value("tabContent7");
    $tabContent8  = $form->value("tabContent8");
    $tabContent9  = $form->value("tabContent9");
    $tabContent10 = $form->value("tabContent10");

    $cboStaff1    = $form->value("cboStaff1");
    $cboStaff2    = $form->value("cboStaff2");
    $cboStaff3    = $form->value("cboStaff3");
    $cboStaff4    = $form->value("cboStaff4");
    $cboStaff5    = $form->value("cboStaff5");
    $cboStaff6    = $form->value("cboStaff6");
    $cboStaff7    = $form->value("cboStaff7");
    $cboStaff8    = $form->value("cboStaff8");
    $cboStaff9    = $form->value("cboStaff9");
    $cboStaff10   = $form->value("cboStaff10");

    $chxStaff1    = $form->value("StaffContacts1");
    $chxStaff2    = $form->value("StaffContacts2");
    $chxStaff3    = $form->value("StaffContacts3");
    $chxStaff4    = $form->value("StaffContacts4");
    $chxStaff5    = $form->value("StaffContacts5");
    $chxStaff6    = $form->value("StaffContacts6");
    $chxStaff7    = $form->value("StaffContacts7");
    $chxStaff8    = $form->value("StaffContacts8");
    $chxStaff9    = $form->value("StaffContacts9");
    $chxStaff10   = $form->value("StaffContacts10");  

    $displayAdd   = true;
    $optStatus    = $form->value("optStatus");
} else if ($iNum > 0) {

    $sql = "SELECT * FROM tbl_cms_asset WHERE as_id = $iNum LIMIT 1";
    $record = Database::Execute($sql);
    $record->MoveNext();

    if ($record->Count() == 0) {
        throw new exception("Unable to find record in the database"); 
    }
    // breakPoint($record->as_data);
    $data = json_decode($record->as_data);
    $hidId        = $iNum;
    $txtAssetName = $record->as_name;

    for($i=0; $i <= $_WEBCONFIG['MAX_LIMIT']; $i++) {
        $v = $i + 1;
        ${'hidTabName' . $v} = isset($data[$i][0]) ? $data[$i][0] : "Tab $i";
        ${'tabContent' . $v} = isset($data[$i][1]) ? str_replace("\r\n", "", $data[$i][1]) : "<p>Tab $i</p>";
        ${'cboStaff' . $v}   = isset($data[$i][2]) && is_array($data[$i][2]) ? 'Y' : "N";
        ${'chxStaff' . $v}   = isset($data[$i][2]) && is_array($data[$i][2]) ? $data[$i][2] : array();        
    }

    $optStatus    = $record->as_status;
    $displayAdd   = true;
} else {
    $hidId        = 0;
    $txtAssetName = "";

    for($v=1; $v <= $_WEBCONFIG['MAX_LIMIT'] + 1; $v++) {
        ${'hidTabName' . $v}  = "Tab $v";
        ${'tabContent' . $v}  = "<p>Tab $v</p>";
        ${'cboStaff' . $v}    = "N";
        ${'chxStaff' . $v}    = array();        
    }

    $optStatus    = 'Active';
    $displayAdd   = true;
}

?>
<!-- VPanel styles -->
<link rel="stylesheet" type="text/css" href="css/styles.css">
<!-- Front end and back end styles -->
<link rel="stylesheet" type="text/css" href="/css/modules/tabbed-content.css">

<div class="subcontent right last">
    <h2><?= $_WEBCONFIG['MODULE_NAME'] ?> -- (<?= ucfirst($view) ?>)</h2>
    <p>Build your tabbed content below and save.</p>
    <?
    if ($form->getNumErrors() > 0) {
        $errors    = $form->getErrorArray();
        echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
        unset($_SESSION['processed']);
    }    ?>

    <form action="process.php" method="post" enctype="multipart/formdata" id="Tabs" name="Tabs-Mod">
        <input type="hidden" id="actionRun" name="actionRun" value="<?= $view ?>" />
        <input type="hidden" id="hidId" name="hidId" value="<?= $hidId ?>" />
        <? for($r=1; $r <= $_WEBCONFIG['MAX_LIMIT'] + 1; $r++) {
            print "<input type=\"hidden\" id=\"hidTabName$r\" name=\"hidTabName$r\" value=\"" . (substr(${'hidTabName' . $r}, 0,3) !== 'Tab' ? ${'hidTabName' . $r} : '') ."\" />";
        } ?>

        <div class="content-tabs">
            <table class="grid-display">
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 175px"><label for="txtAssetName" class="required"><b>Tab Asset Name</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtAssetName" id="txtAssetName" value="<?= $txtAssetName; ?>" required="required" size="55" maxlength="255" />
                        <?= $form->error("txtAssetName");?>
                    </td>
                </tr>
            </table>
            <ul class="tabs clearfix nobullets">
                <li>
                    <ul>
                        <? for ($i = 1; $i <= $_WEBCONFIG['MAX_LIMIT']; $i++) { 
                            if($i == 1) { ?>
                                <li id="tab<?= $i ?>">
                                    <a data-id="<?= $i ?>" class="active pointer myTab" data-tab="tab-div-1" id="aTab<?= $i ?>"><?= ${'hidTabName' . $i} ?></a>
                                    <a class="active" style="display: none;" id="aTab<?= $i ?>-2">
                                        <input data-id="<?= $i ?>" class="tab-input" type="text" id="iTab<?= $i ?>" value="<?= ${'hidTabName' . $i} ?>" />
                                    </a>                                
                                </li>
                                <? } else { 
                                if (substr(${'hidTabName' . $i}, 0,3) === 'Tab') { 
                                    if($displayAdd == true) { ?>
                                        <script type="text/javascript">var tabCount = <?= $i ?>;</script>
                                        <? } ?>
                                    <li id="tab<?= $i ?>" <?= $displayAdd == false ? "style='display:none;'" : "" ?>>
                                        <a class="addTab pointer myTab" data-id="<?= $i ?>" data-tab="tab-div-<?= $i ?>" id="aTab<?= $i ?>">
                                            <img id="tab-<?= $i ?>-image" src="/VPanel/modules/tabs/images/sb-open.png" alt="Add" title="Add New Tab">
                                        </a>
                                        <span id="remove-<?= $i ?>" class="none removeTab pointer" data-id="<?= $i ?>">
                                            <img id="tab-<?= $i ?>-remove-image" src="/VPanel/modules/tabs/images/sb-close.png" alt="Remove" title="Remove Tab">
                                        </span>
                                        <a class="active" style="display: none;" id="aTab<?= $i ?>-2">
                                            <input data-id="<?= $i ?>" class="tab-input" type="text" id="iTab<?= $i ?>" value="Tab <?= $i ?>" />
                                        </a>
                                    </li>
                                    <? $displayAdd = false; ?>
                                    <?      }
                                else { ?>
                                    <li id="tab<?= $i ?>">
                                        <a data-id="<?= $i ?>" class="pointer myTab" data-tab="tab-div-<?= $i ?>" id="aTab<?= $i ?>"><?= ${'hidTabName' . $i} ?></a>
                                        <span id="remove-<?= $i ?>" class="removeTab pointer" data-id="<?= $i ?>"><img id="tab-<?= $i ?>-remove-image" src="/VPanel/modules/tabs/images/sb-close.png" alt=""></span>
                                        <a class="active" style="display: none;" id="aTab<?= $i ?>-2">
                                            <input data-id="<?= $i ?>" class="tab-input" type="text" id="iTab<?= $i ?>" value="<?= ${'hidTabName' . $i} ?>" />
                                        </a>                                
                                    </li>                     
                            <? }
                            }              
                        } ?>               
                    </ul>
                </li>
            </ul>

            <? 
                for ($i = 1; $i <= $_WEBCONFIG['MAX_LIMIT'] + 1; $i++) { 
                    if($i == 1) { ?>
                        <div id="tab-div-<?= $i ?>" class="panel">
                            <textarea id="tabContent<?= $i ?>" name="tabContent<?= $i ?>"><?= ${'tabContent' . $i} ?></textarea>
                            <? if(isset($_WEBCONFIG['STAFF_ENABLED']) && $_WEBCONFIG['STAFF_ENABLED'] == 'true') { ?>
                                <table class="grid-display">
                                    <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                                        <td style="text-align: right; vertical-align: middle; width: 175px"><label for="cboStaff" class="required"><b>This Tab Needs Staff?</b></label></td>
                                        <td style="text-align: left; vertical-align: top;">
                                            <label for="cboStaffY<?= $i ?>"><input class="radioStaff" type="radio" name="cboStaff<?= $i ?>" id="cboStaffY<?= $i ?>" value="Y" <?= ${'cboStaff' . $i} == 'Y' ? "checked " : ""; ?>required="required" /> Yes</label>
                                            <label for="cboStaffN<?= $i ?>"><input class="radioStaff" type="radio" name="cboStaff<?= $i ?>" id="cboStaffN<?= $i ?>" value="N" <?= ${'cboStaff' . $i} == 'N' ? "checked " : ""; ?>required="required" /> No</label>
                                            <?= $form->error("cboStaff" . $i);?>
                                        </td>
                                    </tr>
                                    <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?> Staff-List <?= ${'cboStaff' . $i} == 'Y' ? '' : 'none' ?>">
                                        <td colspan="2" style="text-align: left; vertical-align: top;">                
                                            <ul class="non-insurance product-staff-list clearfix">
                                                <? 
                                                $sql = "SELECT s.st_id, s.st_full_name, s.st_image, s.st_position, s.location_id, l.location_title 
                                                        FROM tbl_staff s, tbl_site_location l
                                                        WHERE st_enabled = '1' AND s.location_id = l.location_id
                                                        ORDER BY st_full_name, s.location_id";
                                                $staff = Database::Execute($sql);            
                                                if ($staff->Count() > 0) {
                                                    while ($staff->MoveNext()) {
                                                        $contacts = "";
                                                        if(is_array(${'chxStaff'.$i}) && count(${'chxStaff'.$i}) > 0) {
                                                            $contacts = '|' . implode("|", ${'chxStaff'.$i}) . '|';    
                                                        }

                                                        $checked = $contacts != "" && (strpos($contacts,'|' . $staff->st_id . '|') !== false) ? "checked='checked'" : ""; 
                                                        print '<li> 
                                                        <label for="staff-' . $staff->st_id . '-' . $i . '" title="' . $staff->st_position . '"><input title="' . $staff->st_position . '" type="checkbox" ' . $checked . ' id="staff-' . $staff->st_id . '-' . $i . '" name="StaffContacts' . $i . '[]" value="' . $staff->st_id . '" />
                                                        ' . $staff->st_full_name . '</label><br><span style="font-size: 9px;">' . str_truncate($staff->st_position, 20) . '</span>
                                                        </li>' . PHP_EOL;  
                                                    }// end while
                                                }                      
                                                ?>
                                            </ul>
                                        </td>
                                    </tr>                                        
                                </table>  
                            <? } ?>                      
                        </div>
                        <? } else { ?>
                        <div id="tab-div-<?= $i ?>" class="panel none">
                            <textarea id="tabContent<?= $i ?>" name="tabContent<?= $i ?>"><?= ${'tabContent' . $i} ?></textarea>
                            <? if(isset($_WEBCONFIG['STAFF_ENABLED']) && $_WEBCONFIG['STAFF_ENABLED'] == 'true') { ?>
                                <table class="grid-display">
                                    <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                                        <td style="text-align: right; vertical-align: middle; width: 175px"><label for="cboStaff" class="required"><b>This Tab Needs Staff?</b></label></td>
                                        <td style="text-align: left; vertical-align: top;">
                                            <label for="cboStaffY<?= $i ?>"><input class="radioStaff" type="radio" name="cboStaff<?= $i ?>" id="cboStaffY<?= $i ?>" value="Y" <?= ${'cboStaff' . $i} == 'Y' ? "checked " : ""; ?>required="required" /> Yes</label>
                                            <label for="cboStaffN<?= $i ?>"><input class="radioStaff" type="radio" name="cboStaff<?= $i ?>" id="cboStaffN<?= $i ?>" value="N" <?= ${'cboStaff' . $i} == 'N' ? "checked " : ""; ?>required="required" /> No</label>
                                            <?= $form->error("cboStaff" . $i);?>
                                        </td>
                                    </tr>
                                    <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?> Staff-List <?= ${'cboStaff' . $i} == 'Y' ? '' : 'none' ?>">
                                        <td colspan="2" style="text-align: left; vertical-align: top;">                
                                            <ul class="non-insurance product-staff-list clearfix">
                                                <? 
                                                $sql = "SELECT s.st_id, s.st_full_name, s.st_image, s.st_position, s.location_id, l.location_title 
                                                        FROM tbl_staff s, tbl_site_location l
                                                        WHERE st_enabled = '1' AND s.location_id = l.location_id
                                                        ORDER BY st_full_name, s.location_id";
                                                $staff = Database::Execute($sql);            
                                                if ($staff->Count() > 0) {
                                                    while ($staff->MoveNext()) {
                                                        $contacts = "";
                                                        if(is_array(${'chxStaff'.$i}) && count(${'chxStaff'.$i}) > 0) {
                                                            $contacts = '|' . implode("|", ${'chxStaff'.$i}) . '|';    
                                                        }

                                                        $checked = $contacts != "" && (strpos($contacts,'|' . $staff->st_id . '|') !== false) ? "checked='checked'" : ""; 
                                                        print '<li> 
                                                                    <label for="staff-' . $staff->st_id . '-' . $i . '" title="' . $staff->st_position . '">
                                                                    <input title="' . $staff->st_position . '" type="checkbox" ' . $checked . ' id="staff-' . $staff->st_id . '-' . $i . '" name="StaffContacts' . $i . '[]" value="' . $staff->st_id . '" />
                                                                    ' . $staff->st_full_name . '</label><br>
                                                                    <span style="font-size: 9px;">' . str_truncate($staff->st_position, 20) . '</span>
                                                                </li>' . PHP_EOL;  
                                                    }// end while
                                                }                      
                                                ?>
                                            </ul>
                                        </td>
                                    </tr>                                        
                                </table>
                            <? } ?>
                        </div>
              <?php }
                } ?>      
        </div>

        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 175px"><label for="optStatus" class="required"><b>Status </b></label></td>
                <td style="text-align: left; vertical-align: top;">
                    <label for="optStatusA"><input type="radio" name="optStatus" id="optStatusA" <?= $optStatus == 'Active' ? 'checked' : ''; ?> value="Active" required="required" /> Active</label>
                    <label for="optStatusI"><input type="radio" name="optStatus" id="optStatusI" <?= $optStatus == 'Inactive' ? 'checked' : ''; ?> value="Inactive" required="required" /> Inactive</label>
                    <?= $form->error("optStatus");?>
                </td>
                <td><em>This status is for the entire set of tabs above. Setting to Inactive will deactivate from the entire site.</em></td>
            </tr>
        </table>        


        <div class="buttons clearfix">
            <a href="javascript:history.back()" class="silver button floatLeft">Back</a>
            <input name="btnModify" type="submit" id="btnModify" class="button floatRight green" value="Save Changes">
        </div>    
    </form>
</div>


<script type="text/javascript">
    $(function() {
        CKEDITOR.config.height = 300;
        CKEDITOR.config.width  = 980;
        CKEDITOR.config.contentsCss = ['/css/global.css', '/css/main.css', '/VPanel/css/ckeditor-override.css'];
        CKEDITOR.config.scayt_autoStartup = false;
        CKEDITOR.config.toolbar =
        [
            { name: 'document', items: ['Source']},
            { name: 'document', items: ['Preview', 'Print', '-', 'Templates'] },
            { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
            { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt', 'AutoCorrect' ] },
            { name: 'styles', items: ['Styles', 'Format', 'FontSize'] },
            { name: 'colors', items: ['TextColor', 'BGColor'] }, '/',
            { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
            { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] }, 
            { name: 'paragraph', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
            { name: 'links', items: ['Link', 'Unlink', 'Anchor', 'tokens'] },
            { name: 'insert', items: ['Image', 'Youtube', 'Iframe', 'Table'] },
            { name: 'insert', items: ['HorizontalRule', 'SpecialChar'] }

        ]; 

        <? for($i = 1; $i <= $_WEBCONFIG['MAX_LIMIT'] + 1; $i++) {
            echo "CKEDITOR.replace('tabContent$i');";
        } ?>    


    });    

    $("input[type='submit']").click(function (event) {
        event.preventDefault();
        if($('#hidTabName1').val() == '' || $('#hidTabName1').val() == 'Tab 1') {
            alert("Please Enter A New Tab Name For Tab 1.");
            $('#aTab1').trigger('dblclick');
        }
        else {
            $("form").submit();
        }
    })
    $(document).ready(function() {
        "use strict";
        $(".panel:first-of-type").show(), $("ul.tabs li a").click(function() {
            var t = $(this).data("tab");
            return $("ul.tabs li a").removeClass("active"), $(this).addClass("active"), $(".panel").hide(), $(".panel#" + t + " ").show(), !1
        }) 
    });

    // ADD ANOTHER TAB
    $('a.myTab').click(function() {
        if($(this).hasClass('addTab')) {
            $(this).removeClass('addTab');
            $(this).removeClass('active');
            $('#aTab' + tabCount).html('Tab ' + tabCount); 
            $('#remove-' + tabCount).show();
            tabCount++;
            $('#tab' + tabCount).show();
            $('#aTab' + tabCount).addClass('addTab');
        }
    });

    // REMOVE TAB AND COPY DATA
    $('.removeTab').click(function() {
        if (confirm('Are you sure you want to remove this tab? All content entered will be lost.')) {
            removedId = $(this).data('id');

            for (i = removedId; i < tabCount; i++) {
                if(i == 2) {
                    CKEDITOR.instances.tabContent2.setData( CKEDITOR.instances.tabContent3.getData() );
                    $('#aTab2').text($('#aTab3').text());
                }
                <? for($i = 3; $i <= $_WEBCONFIG['MAX_LIMIT'] + 1; $i++) {
                    $v = $i + 1;
                    print "else if(i == $i) {
                    CKEDITOR.instances.tabContent$i.setData( CKEDITOR.instances.tabContent$v.getData() );
                    $('#aTab$i').text($('#aTab$v').text());
                    }";
                } ?>                                                                                         
            }
            $('#tab' + tabCount).hide();
            $('#aTab' + tabCount).removeClass('addTab');
            tabCount = tabCount - +1;      
            $('#remove-' + tabCount).hide();
            $('#aTab' + tabCount).html('<img id="tab-' + tabCount + '-image" src="/VPanel/modules/tabs/images/sb-open.png" alt="">');
            $('#aTab' + tabCount).addClass('addTab');
        }    
    })

    // SHOW TEXT BOX TO CHANGE NAME OF TAB
    $('.myTab').dblclick(function() {
        id = $(this).data('id'); 
        $('#aTab' + id + '-2').addClass('active');
        text = $('#aTab' + id).text();
        $('#aTab' + id).hide();
        $('#aTab' + id + '-2').show();
        $('#iTab' + id).val(text);
        $('#iTab' + id).show(); 
        $('#iTab' + id).select();         
    });

    // UPDATE TAB NAME ON TAB
    $('.tab-input').blur(function() {
        value = $(this).val();
        inputId = $(this).attr('id');
        aId = inputId.replace("i", "a");
        $('#' + aId).text(value);
        $('#' + aId).show();
        $('#' + aId + '-2').hide();
        $('#' + inputId).hide();  
        $('#hidTabName' + $(this).data('id')).val(value);          
    });

    $('.radioStaff').click(function() {
        if($(this).val() == 'Y') {
            $('.Staff-List').show();
        }
        else {
            $('.Staff-List').hide();    
        }
    });

    /*    setTimeout(function() {
    $('.radioStaff').each(function() {
    $(this).prop('checked', 'checked');
    });
    },3000);*/

</script>