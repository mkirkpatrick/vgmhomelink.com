$(document).ready(function () {
    $("#grid").kendoGrid({
        dataSource: { pageSize: 50 },
        pageable: { refresh: true, pageSizes: [10, 25, 50, 100, 500] },
        sortable: { mode: "multiple", allowUnsort: true },
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",
                    doesnotcontain: "Does Not Contain",
                    startswith: "Starts With",
                    endswith: "Ends With"
                }
            }
        },
        groupable: false,
        columnMenu: false,
        scrollable: false,
        resizable: true,
        reorderable: true,
		toolbar: kendo.template($("#toolBarTemplate").html())
    });

	if(getInternetExplorerVersion() <= 8 && getInternetExplorerVersion() >= 5) { $('.toolbar').hide(); } 
	else { 
		$("#clearTextButton").kendoButton({icon: "funnel-clear"});
		$("#reorderButton").kendoButton({ icon: "k-icon k-i-refresh" });
		$("#txtSearch").on("keyup keypress blur change click", function () {
			var filter = { logic: "or", filters: [] };
			$searchValue = $(this).val();
			if ($searchValue) {
				$.each($("#grid").data("kendoGrid").columns, function( key, column ) {
					if(column.filterable) { 
						filter.filters.push({ field: column.field, operator:"contains", value:$searchValue});
					}
				});
			}
			$("#grid").data("kendoGrid").dataSource.query({ filter: filter });
		});
		
		$("#clearTextButton").on("click", function () {
			$("#grid").data("kendoGrid").dataSource.query({});
			$("#txtSearch").val('');
		});
	}
});

// Button Functions
function addTab() {
	window.location.href = 'index.php?view=add';
}

function modifyTab(Id) {
	window.location.href = 'index.php?view=modify&id=' + Id;
}

function deleteTab(Id) {
	if (confirm('Are you sure you want to delete? \n\nConsider marking this as inactive if you plan on using it again. ')) {
		window.location.href = 'process.php?actionRun=delete&id=' + Id;
	}
}
