<?
class Process extends Database { 

	private $record;
	private $table;
	public $params;

	//------------------------------
	public function __construct($table) {
	//------------------------------
		global $_WEBCONFIG;
		parent::__construct();
		$this->table = $table;
	}

	//------------------------------
	public function _add() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$this->record = new Entity($this->table);

		self::_verify();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			$this->params = "view=add";
		} else {
			// No Errors
            $this->record->as_key = property_exists($this->record, 'as_key') ? $this->record->as_key : Asset::generateKey($this->record->as_name,1,"tabs");
            $this->record->at_id = $_WEBCONFIG['Asset_ID'];
            $this->record->as_is_editable = 'Y';
            $this->record->as_description = null;
            $this->record->as_date_added = date("Y-m-d G:i:s");
			$this->record->as_last_update = date("Y-m-d G:i:s");
			Repository::Save($this->record); 
			$iNum = Database::getInsertID(); 
			Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Added", "Success", "Added -- '{$this->record->as_name}' (" . $iNum .")", $_SESSION['user_username']);			
			$_SESSION['processed'] = 'added';
			$this->params = "view=list";
		}
	}

	//------------------------------
	public function _modify() {
	//------------------------------
		global $_WEBCONFIG, $form;
        
		$this->record = new Entity($this->table);

		self::_verify();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			$this->params = "view=modify&id={$_POST['hidId']}";
		} else {
			// No Errors
			$this->record->as_id = (int)$_POST['hidId'];
            $this->record->as_last_update = date("Y-m-d G:i:s");
			$this->record->update();
			Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Modified", "Success", "Modified -- '{$this->record->as_name}' (" . $this->record->as_id .")", $_SESSION['user_username']);
			$_SESSION['processed'] = 'updated';
			$this->params = "view=list";
		}
	}

    //------------------------------
    public function _delete() {
    //------------------------------
        global $_WEBCONFIG, $form;
        $iNum    = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
        $this->record = Repository::GetById($this->table, $iNum); 
        Security::_addAuditEvent("Asset Deleted", "Success", "Deleted -- '" . $this->record->as_name . "' (" . $this->record->as_id .")", $_SESSION['user_username']);
        Repository::Delete($this->record); 
        $_SESSION['processed'] = 'deleted';
        $this->params = "view=list";
    }

	//------------------------------
	private function _verify() {
	//------------------------------
		global $_WEBCONFIG, $form;
           
        $fieldName    = "txtAssetName";
        $fieldValue    = trim(strip_tags($_POST[$fieldName]));
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* A Tab Assest Name is required.");
        } else {
            $this->record->as_name = $fieldValue;
        }
        
        $fieldName  = "optStatus";
        $fieldValue = trim(strip_tags($_POST[$fieldName]));
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* A status is required.");
        } else {
            $this->record->as_status = $fieldValue;
        }                
        
        for ($i=1; $i <= $_WEBCONFIG['MAX_LIMIT']; $i++) {
            $fieldName  = "hidTabName$i";
            $fieldValue = isset($_POST[$fieldName]) ? trim(strip_tags($_POST[$fieldName])) : "";
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                if($i == 1) {
                    $form->setError($fieldName, "* A Tab Name is required.");    
                } 
            } else {
                $tabName[$i] = $fieldValue;
            }            
        }
        
        for ($i=1; $i <= $_WEBCONFIG['MAX_LIMIT']; $i++) {
            $fieldName  = "tabContent$i";
            $fieldValue = isset($_POST[$fieldName]) ? trim($_POST[$fieldName]) : "";
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                if($i == 1) {
                    $form->setError($fieldName, "* A Tab Name is required.");    
                }
            } else {
                $tabContent[$i] = $fieldValue;
            }            
        }     
        
        if(isset($_WEBCONFIG['STAFF_ENABLED']) && $_WEBCONFIG['STAFF_ENABLED'] == 'true') {
            for ($i=1; $i <= $_WEBCONFIG['MAX_LIMIT']; $i++) {
                if(isset($_POST['cboStaff' . $i]) && $_POST['cboStaff' . $i] == 'Y') {
                    if(isset($_POST['StaffContacts' . $i]) && count($_POST['StaffContacts' . $i]) > 0) {
                        $staffString[$i] = array();
                        foreach($_POST['StaffContacts' . $i] as $staff) {
                            array_push($staffString[$i], $staff);
                        }    
                    }
                    else {
                        $form->setError($fieldName, "* Select a staff member.");    
                    }
                }            
            }  
        }      
        
        
        $aData = array();
        for($i=1; $i < 11; $i++) {
            if(isset($tabName[$i]) && trim($tabName[$i]) != "" && substr($tabName[$i], 0,3) !== 'Tab') {
                $aData[] = array(str_replace('"', "'", $tabName[$i]),str_replace('"', "'", str_replace(array("\r","\n","\t"),"", $tabContent[$i])),(isset($staffString[$i]) ? $staffString[$i] : ""));    
            }
        }

        $this->record->as_data = json_encode($aData);
        
        if(count($aData) > 0) {
            $this->record->as_value ='<div data-id="' . format_uri($this->record->as_name) . '" class="content-tabs"><ul class="tabs clearfix nobullet"><li><ul>';
            $count = 0;
            foreach($tabName as $key=>$name) {
                $this->record->as_value .= '<li><a' . ($count == 0 ? ' class="active"' : '') . ' data-tab="' . format_uri($this->record->as_name) . '-' . format_uri($key) . '">' . $name . '</a></li>';    
                $count++;
            }
            $this->record->as_value .= '</ul></li></ul>';
                                         
            foreach($tabContent as $key=>$content) {
                $myStaff = array();
                $this->record->as_value .= '<div class="panel" id="' . format_uri($this->record->as_name) . '-' . format_uri($key) . '">' . $content;
                
                if(isset($_WEBCONFIG['STAFF_ENABLED']) && $_WEBCONFIG['STAFF_ENABLED'] == 'true') {
                    if(isset($staffString[$key]) && count($staffString[$key]) > 0 && is_array($staffString[$key])) {
                        foreach($staffString[$key] as $contact) {
                            if(strlen($contact) > 0) {
                                $sql = "SELECT s.*, l.* FROM tbl_staff s, tbl_site_location l WHERE st_id = '$contact' AND s.location_id = l.location_id AND st_enabled = 1 LIMIT 1";
                                $staff = Database::Execute($sql);
                                if($staff->Count() > 0) {
                                    $this->record->as_value .= '<div class="clearfix staff-personnel">';
                                    $staff->MoveNext();
                                    if(strlen($staff->st_image) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/staff/' . $staff->st_image)) {
                                        $this->record->as_value .= '<div class="twocol"><img title="' . $staff->st_full_name . '" alt="' . $staff->st_full_name . '\'s Image" src="/uploads/staff/' . $staff->st_image . '" /></div>';
                                    }
                                    else {
                                        $this->record->as_value .= '<div class="twocol"><img title="" alt="" src="/modules/staff/images/no-image.jpg" /></div>';    
                                    }
                                    $this->record->as_value .= '<div class="tencol last">';
                                    $this->record->as_value .= 
                                    '<h3>' . $staff->st_full_name . '</h3>' .
                                    '<p class="staff-position">' . $staff->st_position . '</p>'  .
                                    '<p class="staff-location"><strong>Location:</strong> ' . $staff->location_title . '</p>' .
                                    '<p class="staff-phone"><strong>Phone:</strong> ' . $staff->st_phone . '</p>';
                                    $this->record->as_value .= '</div>';
                                    $this->record->as_value .= '</div>'; 
                                }  
                            }
                        }
                    }
                }   
                $this->record->as_value .= '</div>';                             
            }
			$this->record->as_value .= '</div>';
        }
        else {
            $this->record->as_value = "ERROR";    
        }
	}

};
?>