<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "pagebuilder/assets/classes/class_asset.php");

security::_secureCheck();

require_once "class.php";

$p = new Process($_WEBCONFIG['MODULE_TABLE']);
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {
	case 'add' :
		$p->_add();
		break;

	case 'modify' :
		$p->_modify();
		break;

	case 'delete' :
		$p->_delete();
		break;

	default :
		throw new exeption("An unknown action executed!");
}

redirect("index.php?{$p->params}");
?>