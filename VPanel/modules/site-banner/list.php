<?php
if (!isset($siteConfig)) die("System Error!");
?>
<style type="text/css">
    span.icon-text {font-size:10px;padding-left:5px;font-weight:700;}
    span.icon-text.delete {color:red;}
    tr.current { background: #FFBF38; }

    p.legend span.current { width: 16px; height:16px; background: #FFBF38; display: inline-table }
    p.legend span.text { vertical-align: text-bottom; font-weight: 700; }

</style>
<div class="subcontent right last">
    <?php
    if($_WEBCONFIG['FRIENDLY_NAME_ON'] == "true"){
        print '<h1>' . $_WEBCONFIG['FRIENDLY_NAME'] . '</h1>';
    } else {
        print '<h1>' . $_WEBCONFIG['MODULE_NAME'] . '</h1>';
    }

    if ($form->getNumErrors() > 0) {
        $errors = $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    }
    else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Created Successfully!')</script>";
                break;
            case 'updated':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                break;
            case 'saved':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Saved Successfully!')</script>";
                break;
            case 'pending-delete':
                echo "<script>addNotify('#ERROR# {$_WEBCONFIG['ENTITY_NAME']} Moved To Pending Delete Successfully!')</script>";
                break;
            case 'cancel-pending-delete':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Pending Delete Canceled Successfully!')</script>";
                break;
            case 'published':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Published Successfully!')</script>";
                break;
            case 'deleted':
                echo "<script>addNotify('#ERROR# {$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                break;
        }
        unset($_SESSION['processed']);
    } ?>
    <p class="legend">
        People logging in will see notifications in order from oldest to most current.
    </p>
    <?php
    // Get All ROOT Site Banner Messages that are not deleted
    $sql = "SELECT *
    FROM {$_WEBCONFIG['MODULE_TABLE']}
    WHERE hn_historic_id IS NULL AND hn_status <> 'Deleted'
    ORDER BY hn_last_update DESC, hn_status, hn_title";
    $record = Database::Execute($sql);

    if($_WEBCONFIG['FRIENDLY_NAME_ON'] == "true"){
        print $record->Count() == 1 ? "<p>There is currently <b>1</b> " . $_WEBCONFIG['FRIENDLY_NAME'] . "</p>\n" : "<p>There are currently <b>" . $record->Count() . "</b> " . $_WEBCONFIG['FRIENDLY_NAME'] . "s. </p>\n";
    } else {
        print $record->Count() == 1 ? "<p>There is currently <b>1</b> " . $_WEBCONFIG['ENTITY_NAME'] . "</p>\n" : "<p>There are currently <b>" . $record->Count() . "</b> " . $_WEBCONFIG['ENTITY_NAME'] . "s. </p>\n";
    }
    ?>

    <table id="grid" class="none">
        <thead>
            <tr>
                <th data-field="historicIsPublished">Published</th>
                <th data-field="historicIsActive">Status</th>
                <th data-field="title">Title</th>
                <th data-field="type">Type</th>
                <th data-field="changes">Changes</th>
                <th data-field="status">Status</th>
                <th data-field="endTime">Last Updated Time</th>
                <th data-field="options">Options</th>
            </tr>
        </thead>

        <tbody>

            <?php
            $deleteIcon = '<img src="/VPanel/' . $_WEBCONFIG['MODULE_FOLDER'] . 'images/recycle_bin_trash_mini.png" alt="Pending Delete"><span  class="icon-text delete">Pending Delete</span>';
            $unpublishedIcon = '<img src="/VPanel/' . $_WEBCONFIG['MODULE_FOLDER'] . 'images/unpublished-mini.png" alt="Unpublished Changes"><span class="icon-text">Unpublished</span>';
            $publishedIcon = '';
            //check if new entry
            //check if revision
            //otherwise it's published
            if ($record->Count() > 0) {
                while ($record->MoveNext()) {
                    $sql = "SELECT *
                    FROM {$_WEBCONFIG['MODULE_TABLE']}
                    WHERE hn_historic_id = $record->hn_id
                    ORDER BY hn_id DESC
                    LIMIT 1";
                    $revisions = Database::Execute($sql);
                    $revisions->MoveNext();

                    if($revisions->Count() == 0){
                        if($record->hn_pending_delete == 1) {
                            $icon = $deleteIcon;
                        } elseif($record->hn_is_published == 0){
                            $icon = $unpublishedIcon;
                        } else {
                            $icon = $publishedIcon;
                        }

                        $status     = $record->hn_status;
                        $title      = $record->hn_title;
                        $endDate    = date("m/d/Y g:i A", strtotime($record->hn_last_update));
                        $id         = $record->hn_id;
                        $bannerType = $record->hn_promo_or_alert;
                        $pendingDelete = $record->hn_pending_delete;

                    } elseif(strtotime($revisions->hn_date_added) > strtotime($record->hn_date_added)) {
                        $icon       = $revisions->hn_pending_delete == 1 ? $deleteIcon : $unpublishedIcon;
                        $status     = $revisions->hn_status;
                        $title      = $revisions->hn_title;
                        $endDate    = date("m/d/Y g:i A", strtotime($revisions->hn_last_update));
                        $id         = $revisions->hn_id;
                        $bannerType = $revisions->hn_promo_or_alert;
                        $pendingDelete = $revisions->hn_pending_delete;
                    } else {
                        $status     = $record->hn_status;
                        $title      = $record->hn_title;
                        $endDate    = date("m/d/Y g:i A", strtotime($record->hn_last_update));
                        $id         = $record->hn_id;
                        $icon       = $publishedIcon;
                        $bannerType = $record->hn_promo_or_alert;
                        $pendingDelete = $record->hn_pending_delete;
                    }

                    ?>

                    <tr>
                        <td><?= $record->hn_is_published ?></td>
                        <td><?= $record->hn_status ?></td>
                        <td><?= $title ?></td>
                        <td><?= ucwords($bannerType) ?></td>
                        <td><?= $icon ?></td>
                        <td><?= $status ?></td>
                        <td><?= $endDate ?></td>
                        <?php
                        if(UserManager::hasRole(CONTENT_AUTHOR_ROLE) && UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) {
                            print '<td><a href="javascript:modifyAlert(' . $id . ');" class="green button-slim">Modify</a>';
                            if($pendingDelete == 1) {
                                print '<a href="javascript:cancelPendingDeleteAlert(' . $id . ');" title="Cancel Pending Delete" class="yellow button-slim">Cancel</a>';
                                print '<a href="javascript:deleteAlert(' . $id . ');" class="red button-slim">Delete</a></td>';
                            }
                            else {
                                print '<a href="javascript:deleteAlert(' . $id . ');" class="red button-slim">Delete</a></td>';
                            }
                        }
                        elseif(UserManager::hasRole(CONTENT_AUTHOR_ROLE)) {
                            print '<td><a href="javascript:modifyAlert(' . $id . ');" class="green button-slim">Modify</a>';
                            if($pendingDelete != 1) {
                                print '<a href="javascript:pendingDeleteAlert(' . $id . ');" class="red button-slim">Delete</a></td>';
                            }
                            else {
                                print '<a href="javascript:cancelPendingDeleteAlert(' . $id . ');" title="Cancel Pending Delete" class="yellow button-slim">Cancel</a></td>';
                            }
                        }
                        elseif (UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) {
                            print '<td><a href="javascript:modifyAlert(' . $id . ');" class="green button-slim">View</a>';
                            if($pendingDelete == 1) {
                                print '<a href="javascript:deleteAlert(' . $id . ');" class="red button-slim">Delete</a></td>';
                            }
                        }?>
                    </tr>
                    <?php
                } //end while
            } //end if
            ?>
        </tbody>
    </table>

    <div class="buttons clearfix">
        <a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>modules/" class="button blue floatLeft">Back</a>
        <?php
        if(UserManager::hasRole(CONTENT_AUTHOR_ROLE)) {
            if($_WEBCONFIG['FRIENDLY_NAME_ON'] == "true"){ ?>
                <a href="javascript:addAlert()" class="button green floatRight">Add New <?= $_WEBCONFIG['FRIENDLY_NAME'] ?></a>
                <?php
            } else {?>
                <a href="javascript:addAlert()" class="button green floatRight">Add New <?= $_WEBCONFIG['ENTITY_NAME'] ?></a>
                <?php
            }
        } ?>
    </div>
</div><!--End continue-->

<script type="text/x-kendo-template" id="searchBarTemplate">
    <div class="toolbar floatRight">
    <div>
    <input id="txtSearch" placeholder="Search Grid" type="text" style="width: 175px" />
    <a id="clearTextButton">Clear</a>
    </div>
    </div>
</script>