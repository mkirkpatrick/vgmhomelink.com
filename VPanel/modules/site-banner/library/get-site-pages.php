<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");

if(Database::TableExists('tbl_cms_content')){
    //OLD TABLE STRUCTURE
    $query = "SELECT cmsc_dynamic_link as cms_dynamic_link
              FROM tbl_cms_content cmsc
              LEFT JOIN tbl_cms cms ON cms.cms_id = cmsc.cms_id
              WHERE cmsc_dynamic_link IS NOT NULL AND (cms_is_deleted = 0 AND cms_is_historic = 0 AND cmsc_is_published = 1 AND cmsc.cms_id <> 1 AND cms_is_system_page = 0 && cms_is_template = 0)
              OR (cms_is_deleted = 0 AND cms_is_historic = 0 AND cmsc_is_published = 1 AND cmsc.cms_id <> 1 AND cms_is_system_page = 1 AND cms_is_template = 0)
              ORDER BY cmsc_dynamic_link";
} else {
    $query = "SELECT cms_dynamic_link
              FROM tbl_cms
              WHERE cms_dynamic_link IS NOT NULL AND (cms_is_deleted = 0 AND cms_is_historic = 0 AND cms_is_published = 1 AND cms_id <> 1 AND cms_is_system_page = 0 && cms_is_template = 0)
              OR (cms_is_deleted = 0 AND cms_is_historic = 0 AND cms_is_published = 1 AND cms_id <> 1 AND cms_is_system_page = 1 AND cms_is_editable = 1 AND cms_is_template = 0)
              ORDER BY cms_dynamic_link";
}
$link = mysqli_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASS);

mysqli_select_db($link, DATABASE_NAME);

$arr = array();
$rs = mysqli_query($link, $query);

while($obj = mysqli_fetch_object($rs)) {
    $arr[] = $obj;
}
header("Content-type: application/json; charset=utf-8");
echo json_encode($arr);

exit();