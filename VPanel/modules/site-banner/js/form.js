// jQuery Mask Plugin v1.13.3
// github.com/igorescobar/jQuery-Mask-Plugin
"use strict";!function(t){"function"==typeof define&&define.amd?define(["jquery"],t):"object"==typeof exports?module.exports=t(require("jquery")):t(jQuery||Zepto)}(function(t){var a=function(a,e,n){a=t(a);var r,s=this,o=a.val();e="function"==typeof e?e(a.val(),void 0,a,n):e;var i={invalid:[],getCaret:function(){try{var t,e=0,n=a.get(0),r=document.selection,s=n.selectionStart;return r&&-1===navigator.appVersion.indexOf("MSIE 10")?(t=r.createRange(),t.moveStart("character",a.is("input")?-a.val().length:-a.text().length),e=t.text.length):(s||"0"===s)&&(e=s),e}catch(o){}},setCaret:function(t){try{if(a.is(":focus")){var e,n=a.get(0);n.setSelectionRange?n.setSelectionRange(t,t):n.createTextRange&&(e=n.createTextRange(),e.collapse(!0),e.moveEnd("character",t),e.moveStart("character",t),e.select())}}catch(r){}},events:function(){a.on("input.mask keyup.mask",i.behaviour).on("paste.mask drop.mask",function(){setTimeout(function(){a.keydown().keyup()},100)}).on("change.mask",function(){a.data("changed",!0)}).on("blur.mask",function(){o===a.val()||a.data("changed")||a.triggerHandler("change"),a.data("changed",!1)}).on("blur.mask",function(){o=a.val()}).on("focus.mask",function(a){n.selectOnFocus===!0&&t(a.target).select()}).on("focusout.mask",function(){n.clearIfNotMatch&&!r.test(i.val())&&i.val("")})},getRegexMask:function(){for(var t,a,n,r,o,i,c=[],l=0;l<e.length;l++)t=s.translation[e.charAt(l)],t?(a=t.pattern.toString().replace(/.{1}$|^.{1}/g,""),n=t.optional,r=t.recursive,r?(c.push(e.charAt(l)),o={digit:e.charAt(l),pattern:a}):c.push(n||r?a+"?":a)):c.push(e.charAt(l).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&"));return i=c.join(""),o&&(i=i.replace(new RegExp("("+o.digit+"(.*"+o.digit+")?)"),"($1)?").replace(new RegExp(o.digit,"g"),o.pattern)),new RegExp(i)},destroyEvents:function(){a.off(["input","keydown","keyup","paste","drop","blur","focusout",""].join(".mask "))},val:function(t){var e,n=a.is("input"),r=n?"val":"text";return arguments.length>0?(a[r]()!==t&&a[r](t),e=a):e=a[r](),e},getMCharsBeforeCount:function(t,a){for(var n=0,r=0,o=e.length;o>r&&t>r;r++)s.translation[e.charAt(r)]||(t=a?t+1:t,n++);return n},caretPos:function(t,a,n,r){var o=s.translation[e.charAt(Math.min(t-1,e.length-1))];return o?Math.min(t+n-a-r,n):i.caretPos(t+1,a,n,r)},behaviour:function(a){a=a||window.event,i.invalid=[];var e=a.keyCode||a.which;if(-1===t.inArray(e,s.byPassKeys)){var n=i.getCaret(),r=i.val(),o=r.length,c=o>n,l=i.getMasked(),u=l.length,h=i.getMCharsBeforeCount(u-1)-i.getMCharsBeforeCount(o-1);return i.val(l),!c||65===e&&a.ctrlKey||(8!==e&&46!==e&&(n=i.caretPos(n,o,u,h)),i.setCaret(n)),i.callbacks(a)}},getMasked:function(t){var a,r,o=[],c=i.val(),l=0,u=e.length,h=0,f=c.length,v=1,p="push",d=-1;for(n.reverse?(p="unshift",v=-1,a=0,l=u-1,h=f-1,r=function(){return l>-1&&h>-1}):(a=u-1,r=function(){return u>l&&f>h});r();){var k=e.charAt(l),g=c.charAt(h),m=s.translation[k];m?(g.match(m.pattern)?(o[p](g),m.recursive&&(-1===d?d=l:l===a&&(l=d-v),a===d&&(l-=v)),l+=v):m.optional?(l+=v,h-=v):m.fallback?(o[p](m.fallback),l+=v,h-=v):i.invalid.push({p:h,v:g,e:m.pattern}),h+=v):(t||o[p](k),g===k&&(h+=v),l+=v)}var y=e.charAt(a);return u!==f+1||s.translation[y]||o.push(y),o.join("")},callbacks:function(t){var r=i.val(),s=r!==o,c=[r,t,a,n],l=function(t,a,e){"function"==typeof n[t]&&a&&n[t].apply(this,e)};l("onChange",s===!0,c),l("onKeyPress",s===!0,c),l("onComplete",r.length===e.length,c),l("onInvalid",i.invalid.length>0,[r,t,a,i.invalid,n])}};s.mask=e,s.options=n,s.remove=function(){var t=i.getCaret();return i.destroyEvents(),i.val(s.getCleanVal()),i.setCaret(t-i.getMCharsBeforeCount(t)),a},s.getCleanVal=function(){return i.getMasked(!0)},s.init=function(e){if(e=e||!1,n=n||{},s.byPassKeys=t.jMaskGlobals.byPassKeys,s.translation=t.jMaskGlobals.translation,s.translation=t.extend({},s.translation,n.translation),s=t.extend(!0,{},s,n),r=i.getRegexMask(),e===!1){n.placeholder&&a.attr("placeholder",n.placeholder),"oninput"in t("input")[0]==!1&&"on"===a.attr("autocomplete")&&a.attr("autocomplete","off"),i.destroyEvents(),i.events();var o=i.getCaret();i.val(i.getMasked()),i.setCaret(o+i.getMCharsBeforeCount(o,!0))}else i.events(),i.val(i.getMasked())},s.init(!a.is("input"))};t.maskWatchers={};var e=function(){var e=t(this),r={},s="data-mask-",o=e.attr("data-mask");return e.attr(s+"reverse")&&(r.reverse=!0),e.attr(s+"clearifnotmatch")&&(r.clearIfNotMatch=!0),"true"===e.attr(s+"selectonfocus")&&(r.selectOnFocus=!0),n(e,o,r)?e.data("mask",new a(this,o,r)):void 0},n=function(a,e,n){n=n||{};var r=t(a).data("mask"),s=JSON.stringify,o=t(a).val()||t(a).text();try{return"function"==typeof e&&(e=e(o)),"object"!=typeof r||s(r.options)!==s(n)||r.mask!==e}catch(i){}};t.fn.mask=function(e,r){r=r||{};var s=this.selector,o=t.jMaskGlobals,i=t.jMaskGlobals.watchInterval,c=function(){return n(this,e,r)?t(this).data("mask",new a(this,e,r)):void 0};return t(this).each(c),s&&""!==s&&o.watchInputs&&(clearInterval(t.maskWatchers[s]),t.maskWatchers[s]=setInterval(function(){t(document).find(s).each(c)},i)),this},t.fn.unmask=function(){return clearInterval(t.maskWatchers[this.selector]),delete t.maskWatchers[this.selector],this.each(function(){var a=t(this).data("mask");a&&a.remove().removeData("mask")})},t.fn.cleanVal=function(){return this.data("mask").getCleanVal()},t.applyDataMask=function(a){a=a||t.jMaskGlobals.maskElements;var n=a instanceof t?a:t(a);n.filter(t.jMaskGlobals.dataMaskAttr).each(e)};var r={maskElements:"input,td,span,div",dataMaskAttr:"*[data-mask]",dataMask:!0,watchInterval:300,watchInputs:!0,watchDataMask:!1,byPassKeys:[9,16,17,18,36,37,38,39,40,91],translation:{0:{pattern:/\d/},9:{pattern:/\d/,optional:!0},"#":{pattern:/\d/,recursive:!0},A:{pattern:/[a-zA-Z0-9]/},S:{pattern:/[a-zA-Z]/}}};t.jMaskGlobals=t.jMaskGlobals||{},r=t.jMaskGlobals=t.extend(!0,{},r,t.jMaskGlobals),r.dataMask&&t.applyDataMask(),setInterval(function(){t.jMaskGlobals.watchDataMask&&t.applyDataMask()},r.watchInterval)});

var popupWindowOptions = [
    'location=no',
    'menubar=no',
    'toolbar=no',
    'dependent=yes',
    'minimizable=no',
    'modal=yes',
    'alwaysRaised=yes',
    'resizable=yes',
    'scrollbars=yes',
    'width=800',
    'height=600'
].join( ',' );

window.CKFinder = {
    _popupOptions: {
        'popup-1-config': { // Config ID for first popup
            chooseFiles: true,
            onInit: function( finder ) {
                finder.on( 'files:choose', function( evt ) {
                    var file = evt.data.files.first();
                    var output = document.getElementById('txtImageUrl');
                    output.value = decodeURIComponent(file.getUrl().toLowerCase());
                    document.getElementById( 'browse-server-preview' ).innerHTML = '<img class="previewImage" src="' + file.getUrl() + '" alt="" />';
                } );
            }
        }
    }
};
// Set Up Kendo Auto Complete
var dataSource = new kendo.data.DataSource({
    transport: {
        read: {
            url: "/VPanel/modules/site-banner/library/get-site-pages.php",
            dataType: "json"
        }
    }
});

$(function() {
    $("#ckfinder-popup-1").click(function(){
        window.open( '/VPanel/ckfinder/ckfinder.html?popup=1&configId=popup-1-config', 'CKFinderPopup1', popupWindowOptions );
        return false;
    })

    $('.birthDatePicker, .datePicker').mask('00/00/0000');

    //Date Picker
    $(".birthDatePicker").datepicker( {
        dateFormat: "mm/dd/yy",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-100:+0",
        showAnim: 'slideDown'
    });

    //Date Picker
    $(".datePicker").datepicker( {
        dateFormat: "mm/dd/yy",
        showAnim: 'slideDown'
    });

    $("#txtStartTime, #txtEndTime").kendoDateTimePicker({
        dateInput: true
    });
    // add * to required field labels
    $('label.required').append('&nbsp;<span style="color: #990000"><b>*</b></span>&nbsp;');

    // PREVIEW setup
    var previewWindow = false;
    $('#btnPreview').click(function (e) {
        for (k in CKEDITOR.instances) { CKEDITOR.instances[k].updateElement(); }
        var urlSubmit = $(this).attr('href');
        var form = $('form#entryForm')[0];
        var oldAction = $(form).attr('action');
        var specs = "width=1200,height=700,location=1,resizeable=1,status=1,scrollbars=1,toolbar=1,menubar=1";

        // if it's a static link / file go there instead and don't submit
        var linkValue = $('#txtLink').val();
        if ($('#txtLink').length > 0 && linkValue != '') {
            urlSubmit = linkValue;
            window.open(urlSubmit, 'PagePreview', specs);
            return false;
        }

        if (previewWindow !== false) {
            previewWindow.close();
        }

        previewWindow = window.open(urlSubmit, 'PagePreview', specs);
        $(form).attr("target", "PagePreview");
        $(form).attr("action", urlSubmit);
        form.submit();
        $(form).removeAttr("target");
        $(form).attr("action", oldAction);

        return false;

    });


    $('*[data-maxchars]').on("focus keyup load paste", function () {
        var max = parseInt($(this).data('maxchars'));
        var messageId = $(this).attr("id") + "Message";
        if ($(this).val().length > max) {
            $(this).val($(this).val().substr(0, $(this).data('maxchars')));
        }
        if ($('#' + messageId).length == 0) {
            $(this).after("<div id='" + messageId + "' class='maxDisplay'></div>");
        }
        $('#' + messageId).html((max - $(this).val().length) + ' Characters Remaining');
    });
    $('*[data-maxchars]').trigger("load");

    $(".save").click(function (event) {
        validate();
    });

    $(".publish").click(function (event) {
        $('input[name="actionRun"]').val('publish');
        validate();
    });

    $(".revision").click(function (event) {
        $("#revision-history").toggle();
    });

    $("input[name='optPromoAlert']").change(function(){
        if($('input[name=optPromoAlert]:checked').val() == 'alert'){
            $('#Image').hide();
            $('#txtImageUrl').val('');
            $('#txtImageUrl').removeAttr('data-parsley-required-message');
        } else {
            $('#Image').show();
            $('#txtImageUrl').attr('data-parsley-required-message', 'An image is needed!');
        }
    });


    var showNoShow = $('input[name=optPromoAlert]:checked').val();
    if(showNoShow == "promo"){
        $('#Image').show();
        $('#txtImageUrl').attr('data-parsley-required-message', 'An image is needed!');
    } else {
        $('#Image').hide();
        $('#txtImageUrl').removeAttr('data-parsley-required-message');
    }

    //create AutoComplete UI component
    $("#txtUrl").kendoAutoComplete({
        dataSource: dataSource,
        dataTextField: "cms_dynamic_link",
        filter: "contains"
    });

    $("#txtUrl").focus(function() { $(this).select(); } );

    $("#txtUrl").blur(function(){
        var x = $(this).val();

        // Make Sure Field is not blank
        if(x.length > 0) {
            // Domain Array Extensions
            var domainArray = [".aaa",".aarp",".abb",".abbott",".abbvie",".able",".abogado",".abudhabi",".ac",".academy",".accenture",".accountant",".accountants",".aco",".active",".actor",".ad",".adac",".ads",".adult",".ae",".aeg",".aero",".aetna",".af",".afl",".ag",".agakhan",".agency",".ai",".aig",".airbus",".airforce",".airtel",".akdn",".al",".alibaba",".alipay",".allfinanz",".ally",".alsace",".alstom",".am",".amica",".amsterdam",".an",".analytics",".android",".anquan",".anz",".ao",".apartments",".app",".apple",".aq",".aquarelle",".ar",".aramco",".archi",".army",".arpa",".art",".arte",".as",".asia",".associates",".at",".attorney",".au",".auction",".audi",".audible",".audio",".author",".auto",".autos",".avianca",".aw",".aws",".ax",".axa",".az",".azure","",".ba",".baby",".baidu",".band",".bank",".bar",".barcelona",".barclaycard",".barclays",".barefoot",".bargains",".bauhaus",".bayern",".bb",".bbc",".bbva",".bcg",".bcn",".bd",".be",".beats",".beer",".bentley",".berlin",".best",".bet",".bf",".bg",".bh",".bharti",".bi",".bible",".bid",".bike",".bing",".bingo",".bio",".biz",".bj",".bl",".black",".blackfriday",".blanco",".blog",".bloomberg",".blue",".bm",".bms",".bmw",".bn",".bnl",".bnpparibas",".bo",".boats",".boehringer",".bom",".bond",".boo",".book",".boots",".bosch",".bostik",".bot",".boutique",".bq",".br",".bradesco",".bridgestone",".broadway",".broker",".brother",".brussels",".bs",".bt",".budapest",".bugatti",".build",".builders",".business",".buy",".buzz",".bv",".bw",".by",".bz",".bzh",".ca",".cab",".cafe",".cal",".call",".cam",".camera",".camp",".cancerresearch",".canon",".capetown",".capital",".car",".caravan",".cards",".care",".career",".careers",".cars",".cartier",".casa",".cash",".casino",".cat",".catering",".cba",".cbn",".cc",".cd",".ceb",".center",".ceo",".cern",".cf",".cfa",".cfd",".cg",".ch",".chanel",".channel",".chase",".chat",".cheap",".chintai",".chloe",".christmas",".chrome",".church",".ci",".cipriani",".circle",".cisco",".citic",".city",".cityeats",".ck",".cl",".claims",".cleaning",".click",".clinic",".clinique",".clothing",".cloud",".club",".clubmed",".cm",".cn",".co",".coach",".codes",".coffee",".college",".cologne",".com",".commbank",".community",".company",".compare",".computer",".comsec",".condos",".construction",".consulting",".contact",".contractors",".cooking",".cookingchannel",".cool",".coop",".corsica",".country",".coupon",".coupons",".courses",".cr",".credit",".creditcard",".creditunion",".cricket",".crown",".crs",".cruises",".csc",".cu",".cuisinella",".cv",".cw",".cx",".cy",".cymru",".cyou",".cz",".dabur",".dad",".dance",".date",".dating",".datsun",".day",".dclk",".dds",".de",".deal",".dealer",".deals",".degree",".delivery",".dell",".deloitte",".delta",".democrat",".dental",".dentist",".desi",".design",".dev",".dhl",".diamonds",".diet",".digital",".direct",".directory",".discount",".dj",".dk",".dm",".dnp",".do",".docs",".dog",".doha",".domains",".doosan",".dot",".download",".drive",".dtv",".dubai",".dunlop",".dupont",".durban",".dvag",".dz",".earth",".eat",".ec",".edeka",".edu",".education",".ee",".eg",".eh",".email",".emerck",".energy",".engineer",".engineering",".enterprises",".epost",".epson",".equipment",".er",".ericsson",".erni",".es",".esq",".estate",".et",".eu",".eurovision",".eus",".events",".everbank",".exchange",".expert",".exposed",".express",".extraspace",".fage",".fail",".fairwinds",".faith",".family",".fan",".fans",".farm",".farmers",".fashion",".fast",".fedex",".feedback",".ferrero",".fi",".film",".final",".finance",".financial",".fire",".firestone",".firmdale",".fish",".fishing",".fit",".fitness",".fj",".fk",".flickr",".flights",".flir",".florist",".flowers",".flsmidth",".fly",".fm",".fo",".foo",".foodnetwork",".football",".ford",".forex",".forsale",".forum",".foundation",".fox",".fr",".fresenius",".frl",".frogans",".frontdoor",".frontier",".ftr",".fund",".furniture",".futbol",".fyi",".ga",".gal",".gallery",".gallo",".gallup",".game",".games",".garden",".gb",".gbiz",".gd",".gdn",".ge",".gea",".gent",".genting",".gf",".gg",".ggee",".gh",".gi",".gift",".gifts",".gives",".giving",".gl",".glass",".gle",".global",".globo",".gm",".gmail",".gmbh",".gmo",".gmx",".gn",".gold",".goldpoint",".golf",".goo",".goodyear",".goog",".google",".gop",".got",".gov",".gp",".gq",".gr",".grainger",".graphics",".gratis",".green",".gripe",".group",".gs",".gt",".gu",".guardian",".gucci",".guge",".guide",".guitars",".guru",".gw",".gy",".hamburg",".hangout",".haus",".hdfcbank",".health",".healthcare",".help",".helsinki",".here",".hermes",".hgtv",".hiphop",".hisamitsu",".hitachi",".hiv",".hk",".hkt",".hm",".hn",".hockey",".holdings",".holiday",".homedepot",".homes",".honda",".horse",".host",".hosting",".hoteles",".hotmail",".house",".how",".hr",".hsbc",".ht",".htc",".hu",".hyundai",".ibm",".icbc",".ice",".icu",".id",".ie",".ifm",".iinet",".ikano",".il",".im",".imamat",".imdb",".immo",".immobilien",".in",".industries",".infiniti",".info",".ing",".ink",".institute",".insurance",".insure",".int",".international",".investments",".io",".ipiranga",".iq",".ir",".irish",".is",".iselect",".ismaili",".ist",".istanbul",".it",".itau",".itv",".iwc",".jaguar",".java",".jcb",".jcp",".je",".jetzt",".jewelry",".jlc",".jll",".jm",".jmp",".jnj",".jo",".jobs",".joburg",".jot",".joy",".jp",".jpmorgan",".jprs",".juegos",".kaufen",".kddi",".ke",".kerryhotels",".kerrylogistics",".kerryproperties",".kfh",".kg",".kh",".ki",".kia",".kim",".kinder",".kindle",".kitchen",".kiwi",".km",".kn",".koeln",".komatsu",".kosher",".kp",".kpmg",".kpn",".kr",".krd",".kred",".kuokgroup",".kw",".ky",".kyoto",".kz",".la",".lacaixa",".lamborghini",".lamer",".lancaster",".land",".landrover",".lanxess",".lasalle",".lat",".latrobe",".law",".lawyer",".lb",".lc",".lds",".lease",".leclerc",".legal",".lego",".lexus",".lgbt",".li",".liaison",".lidl",".life",".lifeinsurance",".lifestyle",".lighting",".like",".limited",".limo",".lincoln",".linde",".link",".lipsy",".live",".living",".lixil",".lk",".loan",".loans",".locker",".locus",".lol",".london",".lotte",".lotto",".love",".lr",".ls",".lt",".ltd",".ltda",".lu",".lupin",".luxe",".luxury",".lv",".ly",".ma",".madrid",".maif",".maison",".makeup",".man",".management",".mango",".market",".marketing",".markets",".marriott",".mattel",".mba",".mc",".md",".me",".med",".media",".meet",".melbourne",".meme",".memorial",".men",".menu",".meo",".metlife",".mf",".mg",".mh",".miami",".microsoft",".mil",".mini",".mk",".ml",".mlb",".mls",".mm",".mma",".mn",".mo",".mobi",".mobily",".moda",".moe",".moi",".mom",".monash",".money",".montblanc",".mormon",".mortgage",".moscow",".motorcycles",".mov",".movie",".movistar",".mp",".mq",".mr",".ms",".mt",".mtn",".mtpc",".mtr",".mu",".museum",".mutual",".mutuelle",".mv",".mw",".mx",".my",".mz",".na",".nadex",".nagoya",".name",".natura",".navy",".nc",".ne",".nec",".net",".netbank",".netflix",".network",".neustar",".new",".news",".next",".nextdirect",".nexus",".nf",".nfl",".ng",".ngo",".nhk",".ni",".nico",".nikon",".ninja",".nissan",".nissay",".nl",".no",".nokia",".northwesternmutual",".norton",".now",".nowruz",".nowtv",".np",".nr",".nra",".nrw",".ntt",".nu",".nyc",".nz",".obi",".office",".okinawa",".olayan",".olayangroup",".ollo",".om",".omega",".one",".ong",".onl",".online",".ooo",".oracle",".orange",".org",".organic",".orientexpress",".origins",".osaka",".otsuka",".ott",".ovh",".pa",".page",".pamperedchef",".panerai",".paris",".pars",".partners",".parts",".party",".passagens",".pccw",".pe",".pet",".pf",".pg",".ph",".pharmacy",".philips",".photo",".photography",".photos",".physio",".piaget",".pics",".pictet",".pictures",".pid",".pin",".ping",".pink",".pioneer",".pizza",".pk",".pl",".place",".play",".playstation",".plumbing",".plus",".pm",".pn",".pnc",".pohl",".poker",".politie",".porn",".post",".pr",".praxi",".press",".prime",".pro",".prod",".productions",".prof",".progressive",".promo",".properties",".property",".protection",".ps",".pt",".pub",".pw",".pwc",".py",".qa",".qpon",".quebec",".quest",".racing",".re",".read",".realestate",".realtor",".realty",".recipes",".red",".redstone",".redumbrella",".rehab",".reise",".reisen",".reit",".ren",".rent",".rentals",".repair",".report",".republican",".rest",".restaurant",".review",".reviews",".rexroth",".rich",".richardli",".ricoh",".rio",".rip",".ro",".rocher",".rocks",".rodeo",".room",".rs",".rsvp",".ru",".ruhr",".run",".rw",".rwe",".ryukyu",".sa",".saarland",".safe",".safety",".sakura",".sale",".salon",".samsung",".sandvik",".sandvikcoromant",".sanofi",".sap",".sapo",".sarl",".sas",".save",".saxo",".sb",".sbi",".sbs",".sc",".sca",".scb",".schaeffler",".schmidt",".scholarships",".school",".schule",".schwarz",".science",".scor",".scot",".sd",".se",".seat",".security",".seek",".select",".sener",".services",".seven",".sew",".sex",".sexy",".sfr",".sg",".sh",".sharp",".shaw",".shell",".shia",".shiksha",".shoes",".shop",".shopping",".shouji",".show",".shriram",".si",".silk",".sina",".singles",".site",".sj",".sk",".ski",".skin",".sky",".skype",".sl",".sm",".smile",".sn",".sncf",".so",".soccer",".social",".softbank",".software",".sohu",".solar",".solutions",".song",".sony",".soy",".space",".spiegel",".spot",".spreadbetting",".sr",".srl",".ss",".st",".stada",".star",".starhub",".statebank",".statefarm",".statoil",".stc",".stcgroup",".stockholm",".storage",".store",".stream",".studio",".study",".style",".su",".sucks",".supplies",".supply",".support",".surf",".surgery",".suzuki",".sv",".swatch",".swiss",".sx",".sy",".sydney",".symantec",".systems",".sz",".tab",".taipei",".talk",".taobao",".tatamotors",".tatar",".tattoo",".tax",".taxi",".tc",".tci",".td",".tdk",".team",".tech",".technology",".tel",".telecity",".telefonica",".temasek",".tennis",".teva",".tf",".tg",".th",".thd",".theater",".theatre",".tickets",".tienda",".tiffany",".tips",".tires",".tirol",".tj",".tk",".tl",".tm",".tmall",".tn",".to",".today",".tokyo",".tools",".top",".toray",".toshiba",".total",".tours",".town",".toyota",".toys",".tp",".tr",".trade",".trading",".training",".travel",".travelchannel",".travelers",".travelersinsurance",".trust",".trv",".tt",".tube",".tui",".tunes",".tushu",".tv",".tvs",".tw",".tz",".ua",".ubs",".ug",".uk",".um",".unicom",".university",".uno",".uol",".ups",".us",".uy",".uz",".va",".vacations",".vana",".vc",".ve",".vegas",".ventures",".verisign",".versicherung",".vet",".vg",".vi",".viajes",".video",".vig",".viking",".villas",".vin",".vip",".virgin",".vision",".vista",".vistaprint",".viva",".vlaanderen",".vn",".vodka",".volkswagen",".vote",".voting",".voto",".voyage",".vu",".vuelos",".wales",".walter",".wang",".wanggou",".warman",".watch",".watches",".weather",".weatherchannel",".webcam",".weber",".website",".wed",".wedding",".weibo",".weir",".wf",".whoswho",".wien",".wiki",".williamhill",".win",".windows",".wine",".wme",".wolterskluwer",".woodside",".work",".works",".world",".ws",".wtc",".wtf",".xbox",".xerox",".xihuan",".xin"];

            // Remove all but the domain name
            var domain = x.replace("www.",""); // Remove WWW from field
            var domain = domain.replace("http://",""); // Remove http://
            var domain = domain.replace("https://",""); // Remove https://
            var domain = domain.split('/')[0]; // No WWW. | No http:// | No https:// | No /the-rest-of-my-domain

            // Check if domain has a "." in it
            var dotExist = domain.lastIndexOf('.');
            if(dotExist > -1) {
                var domainExt = domain.substring((domain.lastIndexOf('.') - 1) + 1);
            } else {
                var domainExt = "";
            }

            // Check if value has current domain in it and remove it
            if(x.indexOf(location.protocol+'//'+location.hostname) > -1 || x.indexOf(location.hostname) > -1) {
                var newDomain = x.replace(location.hostname,"");
                $(this).val(newDomain.replace(location.protocol+'//',""));
                // Check if value has a domain extension in it
            } else if(domainExt.length > 0){
                if(domainArray.indexOf(domainExt) > -1) {
                    if(isSubdomain(domain)) {
                        if(x.indexOf("://") > -1) {
                            // do nothing
                        } else {
                            $(this).val("http://" + x);
                        }
                    } else {
                        if(x.indexOf("://") > -1) {
                            // do nothing
                        } else {
                            $(this).val("http://www." + domain);
                        }
                    }
                }
            } else if (x.charAt(0) != "/") {
                $(this).val("/" + x);
            } else {
                // dont worry, be happy
            }
        }
    })

    var isSubdomain = function(url) {
        var regex = new RegExp(/^([a-z]+\:\/{2})?([\w-]+\.[\w-]+\.\w+)$/);
        return !!url.match(regex); // make sure it returns boolean
    }
});

function validate(){
    var myform = '#entryForm';
    var buttonVal = $(this).html();
    $(this).html('PROCESSING ....');
    $(myform).parsley({excluded: "input[type=button], input[type=submit], input[type=reset], input[type=hidden], input:hidden, textarea[type=hidden], textarea:hidden"});
    if($(myform).parsley().isValid()) {
        $(myform).submit();
    }
    else {
        $(this).html(buttonVal);
        $(myform).parsley().validate();
    }
};
function truncateText(textbox) {
    var max = parseInt($(textbox).data('maxchars'));
    var messageId = $(textbox).attr("id") + "Message";
    if ($(textbox).val().length > max) {
        $(textbox).val($(textbox).val().substr(0, $(textbox).data('maxchars')));
    }
    if ($('#' + messageId).length == 0) {
        $(textbox).after("<div id='" + messageId + "' class='maxDisplay'></div>");
    }
    $('#' + messageId).html((max - $(textbox).val().length) + ' Characters Remaining');
}

function imagePath(){
    $('#browse-server-preview').html("");
    var filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '')
    document.getElementById('txtImageUrl').value = filename;
}

function btnTextDisplay(){
    if($('#txtUrl').val().length > 0){
        $('#button-text-div').show();
    } else {
        $('#button-text-div').hide();
    }
}