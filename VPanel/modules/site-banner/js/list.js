$(document).ready(function () {

    $("#grid").before('<div id="grid-loader" class="k-loading-mask" style="width:80%;height:38%"><span class="k-loading-text">Loading...</span><div class="k-loading-image"><div class="k-loading-color"></div></div></div>');

    var kendoPageSize = 25;
    $("#grid").kendoGrid({
        dataSource: {
            schema: {
                model: {
                    id:"Id",
                    fields: {
                        historicIsPublished: { type: "string" },
                        historicIsActive: { type: "string" },
                        title: { type: "string" },
                        type: { type: "string" },
                        changes: { type: "string" },
                        status: { type: "string" },
                        startTime: { type: "date" },
                        endTime: { type: "date" },
                        options: { type: "string" }
                    }
                }
            },
            pageSize: kendoPageSize
        },
        columns: [
            { field: "historicIsPublished", width: 0, filterable: false },
            { field: "historicIsActive", width: 0, filterable: false },
            { field: "title", width: 250, filterable: true },
            { field: "type", width: 100, filterable: true },
            { field: "changes", width: 120, filterable: false },
            { field: "status", width: 90, filterable: true },
            { field: "endTime", width: 175, filterable: false, format: "{0:MMM dd, yyyy h:mm tt}" },
            { field: "options", width: 150, filterable: false, sortable: false, attributes: {'class':'alignCenter'} }
        ],
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",
                    eq: "Exact Match",
                    startswith: "Starts With",
                    endswith: "Ends With",
                    doesnotcontain: "Does Not Contain",
                    neq: "Does Not Match"
                }
            }
        },
        pageable: { pageSizes: [25, 50, 100, 500] },
        columnMenu: false,
        groupable: false,
        sortable: true,
        scrollable: true,
        resizable: true,
        reorderable: true,
        dataBound: function(e){

            $("#grid").data("kendoGrid").hideColumn(0);
            $("#grid").data("kendoGrid").hideColumn(1);
            $("#grid-loader").hide();
            $("#grid").fadeIn();

            var dataView = this.dataSource.view();
            var dateArray = [];
            for (var i = 0; i < dataView.length; i++) {
                var start = new Date(dataView[i].startTime)
                var end = new Date(dataView[i].endTime)
                var current = new Date()
                var isBetween = current >= start && current <= end;
                if (isBetween && dataView[i].historicIsPublished == '1' && dataView[i].historicIsActive == 'Active') {
                    var uid = dataView[i].uid;
                    var valueToPush = new Array();
                    valueToPush[0] = uid;
                    valueToPush[1] = start;
                    dateArray.push(valueToPush);
                }
            }

            var count = 0;
            var now = new Date();
            var closest = new Array();
            var valueToPush = new Array();
            valueToPush[0] = 0;
            valueToPush[1] = now;
            closest.push(valueToPush);

            dateArray.forEach(function(d) {
                var date = new Date(d[1]);
                if (date <= now) {
                    if(count == 0){
                        closest[0] = d;
                    } else if(date > closest[0][1]){
                        closest[0] = d;
                    }
                }
                count++;
            });

            $("#grid tbody").find("tr[data-uid=" + closest[0][0] + "]").addClass("current");
        },
        toolbar: kendo.template($("#searchBarTemplate").html())
    });

    if(getInternetExplorerVersion() <= 8 && getInternetExplorerVersion() >= 5) {
        $('.toolbar').hide();
    } else {
        $("#clearTextButton").kendoButton({icon: "funnel-clear"});
        $("#txtSearch").on("keyup keypress onpaste", function () {
            var filter = { logic: "or", filters: [] };
            $searchValue = $(this).val();
            if ($searchValue.length > 1) {
                $.each($("#grid").data("kendoGrid").columns, function( key, column ) {
                    if(column.filterable) {
                        filter.filters.push({ field: column.field, operator:"contains", value:$searchValue});
                    }
                });

                $("#grid").data("kendoGrid").dataSource.query({ filter: filter });

                var count = $("#grid").data("kendoGrid").dataSource.total();
                $(".k-pager-info").html("1 - " + count + " of " + count + " items");

            } else if($searchValue == "") {
                $("#grid").data("kendoGrid").dataSource.query({
                    page: 1,
                    pageSize: kendoPageSize
                });
            }
        });

        $("#clearTextButton").on("click", function () {
            $("#grid").data("kendoGrid").dataSource.query({
                page: 1,
                pageSize: kendoPageSize
            });
            $("#txtSearch").val('');
        });
    }
});

// Button Functions
function addAlert() {
    window.location.href = 'index.php?view=add';
}

function modifyAlert(Id) {
    window.location.href = 'index.php?view=modify&id=' + Id;
}

function pendingDeleteAlert(Id) {
    if (confirm('Are you sure you want to delete? \n\nConsider marking this as inactive if you plan on using it again. ')) {
        window.location.href = 'process.php?actionRun=pendingDelete&id=' + Id;
    }
}

function cancelPendingDeleteAlert(Id) {
    if (confirm('Are you sure you want to remove this alert message from pending delete status?')) {
        window.location.href = 'process.php?actionRun=cancelDelete&id=' + Id;
    }
}

function deleteAlert(Id) {
    if (confirm('Are you sure you want to delete this alert message?')) {
        window.location.href = 'process.php?actionRun=delete&id=' + Id;
    }
}