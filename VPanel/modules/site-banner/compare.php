<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");

$cId = isset($_GET['cid']) && (int)$_GET['cid'] > 0 ? (int)$_GET['cid'] : 0;
$rId = isset($_GET['rid']) && (int)$_GET['rid'] > 0 ? (int)$_GET['rid'] : 0;

$currentBanner  = Repository::getById('tbl_site_banner', $cId);
$revisionBanner = Repository::getById("tbl_site_banner", $rId); ?>

<!DOCTYPE html>
<html>
    <head>
        <title>VPanel :: Site Banner Compare Tool</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link type="image/x-icon" rel="shortcut icon" href="<?= $_WEBCONFIG['VPANEL_PATH'];?>images/favicon.ico" />
        <link type="text/css" href="<?php autoCreateVersion($_WEBCONFIG['VPANEL_PATH'] . 'css/vpanel.css'); ?>" rel="stylesheet" />
        <link type="text/css" href="<?php autoCreateVersion($_WEBCONFIG['VPANEL_PATH'] . '/css/print.css'); ?>" rel="stylesheet" media="print" />
        <style type="text/css">
            body {
                overflow-x:hidden;
                color: #434343;
                font-family: "Myriad Pro",Helvetica,Arial,sans-serif;
            }
            #container {
                margin-left: auto;
                margin-right: auto;
                margin-left: 10px;
            }
            iframe {
                -moz-transform: scale(0.51, 0.51);
                -webkit-transform: scale(0.51, 0.51);
                -o-transform: scale(0.51, 0.51);
                -ms-transform: scale(0.51, 0.51);
                transform: scale(0.51, 0.51);
                -moz-transform-origin: top left;
                -webkit-transform-origin: top left;
                -o-transform-origin: top left;
                -ms-transform-origin: top left;
                transform-origin: top left;
                height: 1000px;
                width:1150px;
                border:5px solid #747170;
            }
            .previewContainer {
                display: inline-block;
                width: 48%;
                margin: 5px;
                max-height: 500px;
                overflow: hidden;
            }
        </style>
        <link type="text/css" href="/VPanel/css/vpanel.css" rel="stylesheet" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            $(window).on("load resize", function() {
                var zoom = (window.innerWidth/1280 * 51)/100;
                $('iframe').css("transform", "scale(" + zoom + ", " + zoom + ")");
            });
        </script>
    </head>
    <body>
        <div id="container" >
            <div class="previewContainer">
                <h1>Live Site Banner</h1>
                <iframe id="liveFrame" src="<?= urlPathCombine($_SERVER['ROOT_URI'], "index.php?hnid=" . $cId); ?>" seamless="seamless">
                </iframe>
            </div>
            <div class="previewContainer">
                <h1>Selected Site Banner Revision</h1>
                <iframe id="revisionFrame" src="<?= urlPathCombine($_SERVER['ROOT_URI'], "index.php?hnid=" . $rId); ?>" seamless="seamless"></iframe>
            </div>
        </div>
    </body>
</html>