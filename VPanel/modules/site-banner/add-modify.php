<?php
if (!isset($siteConfig)) die("System Error!");

$iNum = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
?>
<script type="text/javascript">var iNum = <?= $iNum ?>;</script>

<?php
if ($form->getNumErrors() > 0) {
    $hidId          = $form->value("hidId");
    $historicID     = $form->value("hidHistoricId");
    $optPromoAlert  = $form->value("optPromoAlert");
    $txtTitle       = $form->value("txtTitle");
    $txtText        = $form->value("txtText");
    $txtUrl	        = $form->value("txtUrl");
    $txtButtonText  = $form->value("txtButtonText");
    // $txtStartTime   = $form->value("txtStartTime");
    // $txtEndTime     = $form->value("txtEndTime");
    $optStatus      = $form->value("optStatus");
    $hidImage       = $form->value("hidImage");
    $optType        = $form->value("optType");
} elseif ($iNum > 0) {
    $notification = Repository::GetById($_WEBCONFIG['MODULE_TABLE'], $iNum);
    if (is_null($notification)) {
        throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
    }//end if
    $hidId          = $notification->hn_id;
    $historicID     = $notification->hn_historic_id;
    $optPromoAlert  = $notification->hn_promo_or_alert;
    $txtTitle       = htmlspecialchars($notification->hn_title);
    $txtText        = htmlspecialchars($notification->hn_text);
    // $txtStartTime   = date("m/d/Y g:i A", strtotime($notification->hn_time_start));
    // $txtEndTime     = date("m/d/Y g:i A", strtotime($notification->hn_time_end));
    $txtUrl         = $notification->hn_url;
    $txtButtonText  = $notification->hn_button_text;
    $optStatus      = $notification->hn_status;
    $hidImage       = htmlspecialchars($notification->hn_image);
    $optType        = $notification->hn_type;
} else {
    $hidId         = '';
    $historicID    = NULL;
    $optPromoAlert = 'promo';
    $txtTitle      = '';
    $txtText       = '';
    $txtUrl	       = '';
    $txtButtonText = 'Learn More';
    // $txtStartDate  = '';
    // $txtStartTime  = '';
    // $txtEndDate    = '';
    // $txtEndTime    = '';
    $optStatus     = 'Active';
    $hidImage      = '';
    $optType       = 0;
}

if(UserManager::hasRole(CONTENT_AUTHOR_ROLE) && UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) {
    $readonly = "";
} elseif(UserManager::hasRole(CONTENT_AUTHOR_ROLE)) {
    $readonly = "";
} elseif (UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) {
    $readonly = "readonly='readonly'";
} else {
    $readonly = "readonly='readonly'";
}//end if
?>
<link href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>css/parsley.css" rel="stylesheet" >
<link href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>modules/site-banner/css/site-banner.css" rel="stylesheet" >
<form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">
    <input type="hidden" name="actionRun" value="<?= $_GET['view'] ?>" />
    <input type="hidden" name="hidId" value="<?= $hidId; ?>" />
    <input type="hidden" name="hidHistoricId" value="<?= $historicID; ?>" />
    <input type="hidden" name="hidImage" value="<?= $hidImage; ?>" />

    <div class="subcontent right last">
        <?php
        if($_GET['view'] != 'add'){ ?>
            <a id="btnPublish" class="publish floatRight green button icon-publish publish btnpublish" title="Publish Alert Banner changes.">Publish</a>
            <?php
        }//end if ?>
        <a class="floatRight yellow button icon-floppy save btnsave" id="btnSave" title="Save alert banner changes.">Save</a>
        <a id="btnPreview2" class="button floatRight blue icon-search preview">Preview</a>
        <a id="btnRevision2" class="button floatRight silver icon-retweet revision btnRevision2">Revisions</a>
        <?php
        if($_WEBCONFIG['FRIENDLY_NAME_ON'] == "true"){
            print '<h2>' . $_WEBCONFIG['FRIENDLY_NAME'] . ' -- (' . ucfirst($view) . ')</h2>';
        } else {
            print '<h2>' . $_WEBCONFIG['MODULE_NAME'] . ' -- (' . ucfirst($view) . ')</h2>';
        }//end if ?>
        <p>Insert the record information below and save.</p>
        <div id="revision-history" class="revision-history sc-right-column sc-right-column-low rh">
            <div class="header"><h3>Revision History</h3></div>
            <ul class="revision-history">
                <li>
                    <span class="date"><strong>Revision Date</strong></span>
                    <span class="user"><strong>Save Username</strong></span>
                    <span class="user"><strong>Publish Username</strong></span>
                    <span class="options"><strong>Options</strong></span>
                </li>
                <?php
                $id = strlen($historicID) > 0 ? $historicID : $iNum;
                $sql = "SELECT *
                FROM {$_WEBCONFIG['MODULE_TABLE']}
                WHERE (hn_historic_id = $id OR hn_id = $id)
                ORDER BY hn_last_update DESC";
                $oldRevisions = Database::Execute($sql);
                if($oldRevisions->Count() > 0) {
                    while($oldRevisions->MoveNext()) {
                        if($oldRevisions->hn_id == $iNum) {
                            continue;
                        }//end if
                        $saveUser = strlen($oldRevisions->hn_user_save) > 0 ? $oldRevisions->hn_user_save : '';
                        $publishUser = strlen($oldRevisions->hn_user_publish) > 0 ? $oldRevisions->hn_user_publish : '';
                        print '<li>
                        <span class="date">' . $oldRevisions->hn_last_update  . '</span>
                        <span class="user">' . $saveUser . '</span>
                        <span class="user">' . $publishUser . '</span>
                        <ul>
                        <li><a target="_blank" href="/index.php?hnid=' . $oldRevisions->hn_id . '">Preview</a></li>
                        <li><a class="openLink" href="/VPanel/modules/site-banner/compare.php?cid=' . $id . '&rid=' . $oldRevisions->hn_id . '" target="_blank">Compare</a></li>
                        </ul>';
                    }//end while
                }//end if?>
            </ul>
        </div>
        <?php
        if (isset($_SESSION['processed'])) {
            echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
            unset($_SESSION['processed']);
        }//end if
        ?>
        <table class="grid-display">
            <tr>
                <td class="lbl">
                    <label for="optPromoAlert" class="required"><strong>Promo / Alert</strong></label>
                </td>
                <td class="fields">
                    <input name="optPromoAlert" type="radio" data-parsley-required-message="Select an option" data-parsley-multiple="optPromoAlert" id="optPromo" value="promo" checked <?= $optPromoAlert == 'promo' ? 'checked' : ''; ?> <?= $readonly ?> />
                    <label for="optPromo">Promo </label> &nbsp;&nbsp;
                    <input name="optPromoAlert" type="radio" data-parsley-required-message="Select an option" data-parsley-multiple="optPromoAlert" id="optAlert" value="alert" <?= $optPromoAlert == 'alert' ? 'checked' : ''; ?> <?= $readonly ?> />
                    <label for="optAlert">Alert </label> &nbsp;&nbsp;
                    <?= $form->error("optPromoAlert");?>
                </td>
            </tr>
            <tr>
                <td class="lbl">
                    <label for="txtTitle" class="required"><strong>Title</strong></label>
                </td>
                <td class="fields">
                    <input type="text" name="txtTitle" data-maxchars="50" id="txtTitle" value="<?= $txtTitle; ?>" maxlength="50" required="required" data-parsley-required-message="Enter a title" <?= $readonly ?> />
                    <?= $form->error("txtTitle");?>
                </td>
            </tr>
            <tr>
                <td class="lbl">
                    <label for="txtText" class="required"><strong>Text</strong></label>
                </td>
                <td class="fields">
                    <textarea rows="3" name="txtText" id="txtText" data-parsley-required-message="Enter text" required="required" class="txtText" data-maxchars="250" <?= $readonly ?> ><?= $txtText; ?></textarea>
                    <?= $form->error("txtText");?>
                </td>
            </tr>
            <tr>
                <td class="lbl">
                    <label for="txtUrl"><strong>Url</strong></label>
                </td>
                <td class="fields">
                    <input type="text" name="txtUrl" id="txtUrl" value="<?= $txtUrl; ?>" class="txturl" onchange="btnTextDisplay()" maxlength="255" <?= $readonly ?> /><br>
                    <?= $form->error("txtUrl");?>
                    <br>
                    <div id="button-text-div" class="<?= !isNullOrEmpty($txtUrl) ? '' : 'none' ?>">
                        <label for="txtButtonText" class=""><strong>Button Text</strong></label>&nbsp;&nbsp;
                        <input type="text" name="txtButtonText" id="txtButtonText" value="<?= $txtButtonText; ?>" data-maxchars="20" size="63" maxlength="20" class="" />
                    </div>
                    <?= $form->error("txtButtonText");?>
                </td>
            </tr>

            <tr>
                <td class="lbl">
                    <label for="optStatus" class="required"><strong>Status</strong></label>
                </td>
                <td class="fields">
                    <input name="optStatus" type="radio" data-parsley-required-message="Select an option" data-parsley-multiple="optStatus" id="optAStatus" value="Active" <?= $optStatus == 'Active' ? 'checked' : ''; ?> <?= $readonly ?> />
                    <label for="optAStatus">Active </label> &nbsp;&nbsp;
                    <input name="optStatus" type="radio" data-parsley-required-message="Select an option" data-parsley-multiple="optStatus" id="optIStatus" value="Inactive" <?= $optStatus == 'Inactive' ? 'checked' : ''; ?> <?= $readonly ?> />
                    <label for="optIStatus">Inactive </label> &nbsp;&nbsp;
                    <?= $form->error("optStatus");?>
                </td>
            </tr>
            <tr>
                <td class="lbl">
                    <label for="optType" class="required"><strong>Type</strong></label>
                </td>
                <td class="fields">
                    <input name="optType" type="radio" data-parsley-required-message="Select an option" data-parsley-multiple="optType" id="optAStatus" value="Both" <?= $optType == 0 ? 'checked' : ''; ?> <?= $readonly ?> />
                    <label for="optAStatus">Both </label> &nbsp;&nbsp;
                    <input name="optType" type="radio" data-parsley-required-message="Select an option" data-parsley-multiple="optType" id="optBStatus" value="Claims Professional" <?= $optType == 1 ? 'checked' : ''; ?> <?= $readonly ?> />
                    <label for="optBStatus">Claims Professional </label> &nbsp;&nbsp;
                    <input name="optType" type="radio" data-parsley-required-message="Select an option" data-parsley-multiple="optType" id="optCStatus" value="Provider Portal" <?= $optType == 2 ? 'checked' : ''; ?> <?= $readonly ?> />
                    <label for="optCStatus">Provider Portal </label> &nbsp;&nbsp;
                    <?= $form->error("optType");?>
                </td>
            </tr>

            <tr id="Image">
                <td class="lbl">
                    <label class="required" for="fleLeftImage"><strong>Image</strong></label>
                </td>
                <td class="fields">
                    <div id="browse-server-preview">
                        <img src="<?= !isNullOrEmpty($hidImage) ? $_WEBCONFIG['UPLOAD_FOLDER'] . $hidImage: '';  ?>" class="previewImage" />
                    </div>

                    <input name="txtImageUrl" id="txtImageUrl" type="text" data-parsley-required-message="An Image is needed!" required="required" class="sbimage" value="<?= !isNullOrEmpty($hidImage) ? $_WEBCONFIG['UPLOAD_FOLDER'] . $hidImage : '';  ?>" style="width:650px" placeholder="Select an image from server or upload a new one" readonly />
                    <br><br>
                    <a href="#" id="ckfinder-popup-1" class="button silver">Browse Server</a>
                    <a href="#" id="uploadImage" class="button silver"  onclick="document.getElementById('fleImage').click();">Upload</a>

                    <input name="fleImage" type="file" onchange="imagePath()" id="fleImage" size="50" maxlength="255" imgtype="optional" class="fileUpload" />
                    <?= $form->error("fleImage");?>


                </td>
            </tr>
        </table>
        <div class="buttons clearfix">
            <a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>modules/site-banner" class="silver button floatLeft">Back</a>
        </div>
    </div><!--End continue-->
</form>
<?php
if(UserManager::hasRole(CONTENT_AUTHOR_ROLE) && UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) { ?>
    <script type="text/javascript">
        $('.publish').show();
        $('.save').show();

        $(".preview").click(function (event) {
            $('#entryForm').attr('action', '/index.php?a=' + iNum);
            $('#entryForm').attr('target', '_blank');
            $('#entryForm').submit();
            $('#entryForm').removeAttr('target');
            $('#entryForm').attr('action', 'process.php');
        });
    </script>
    <?php
} elseif(UserManager::hasRole(CONTENT_AUTHOR_ROLE)) {  ?>
    <script type="text/javascript">
        $('.publish').hide();
        $('.save').show();

        $(".preview").click(function (event) {
            $('#entryForm').attr('action', '/index.php?a=' + iNum);
            $('#entryForm').attr('target', '_blank');
            $('#entryForm').submit();
            $('#entryForm').removeAttr('target');
            $('#entryForm').attr('action', 'process.php');
        });
    </script>
    <?php
} elseif(UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) {  ?>
    <script type="text/javascript">
        $('.save').hide();
        $('.publish').show();
        CKEDITOR.config.readOnly = true;

        $(".publish").click(function (event) {
            $('#optPromo').removeAttr('disabled');
            $('#optAlert').removeAttr('disabled');
            $('#txtTitle').removeAttr('disabled');
            $('#txtText').removeAttr('disabled');
            $('#optAStatus').removeAttr('disabled');
            $('#optIStatus').removeAttr('disabled');
            $('#txtUrl').removeAttr('disabled');
        });

        $(".preview").click(function (event) {
            $('#txtTitle').removeAttr('disabled');
            $('#txtName').removeAttr('disabled');
            $('#txtWebsite').removeAttr('disabled');
            $('#optYDisclaimer').removeAttr('disabled');
            $('#optNDisclaimer').removeAttr('disabled');
            $('#entryForm').attr('action', '/index.php?a=' + iNum);
            $('#entryForm').attr('target', '_blank');
            $('#entryForm').submit();
            $('#txtName').attr('disabled', 'disabled');
            $('#txtWebsite').attr('disabled', 'disabled');
            $('#optYDisclaimer').attr('disabled', 'disabled');
            $('#optNDisclaimer').attr('disabled', 'disabled');
            $('#entryForm').removeAttr('target');
            $('#entryForm').attr('action', 'process.php');
        });
    </script>
<?php
}
if($view == "modify") {
    $sql = "SELECT * FROM {$_WEBCONFIG['MODULE_TABLE']} WHERE hn_id = $iNum ORDER BY hn_id DESC LIMIT 1";
    $revisions = Database::Execute($sql);
    $revisions->MoveNext();

    if($revisions->hn_promo_or_alert == 'alert'){?>
        <script>
            $('#Image').hide();
            $('#txtImageUrl').removeAttr('data-parsley-required-message');
        </script>
        <?php
    }

    if($revisions->hn_is_published == 1) {
        if(UserManager::hasRole(CONTENT_AUTHOR_ROLE) && UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) { ?>
            <script type="text/javascript">
                $('.publish').show();
            </script>
            <?php
        } else { ?>
            <script type="text/javascript">
                $('.publish').hide();
            </script>
            <?php }
    }
} ?>