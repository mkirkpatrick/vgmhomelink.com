<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_form.php");

security::_secureCheck();

require_once "class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "{$_WEBCONFIG['VPANEL_PATH']}library/ThumbLib.inc.php";
$p = new Process($_WEBCONFIG['MODULE_TABLE'], $_WEBCONFIG['UPLOAD_FOLDER']);
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';
$returnUrl = isset($_GET['returnUrl']) ? $_GET['returnUrl'] : '';

switch ($action) {
	case 'add' :
		$p->_add();
		break;

	case 'modify' :
		$p->_save();
		break;

    case 'publish' :
        $p->_publish();
        break;

	case 'delete' :
		$p->_delete();
		break;

    case 'pendingDelete' :
        $p->_pendingDelete();
        break;

    case 'cancelDelete' :
        $p->_cancelDelete();
        break;

    case 'removeInstallFiles' :
        $p->_removeInstallFiles($returnUrl);
        break;

	default :
		die("An unknown action executed!");
}

redirect("index.php?{$p->params}");
