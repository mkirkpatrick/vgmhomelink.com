<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_form.php");

security::_secureCheck();

if (!isset($_GET['logout'])) {
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

print '<section class="title">
            <div class="container clearfix">';
            if($_WEBCONFIG['FRIENDLY_NAME_ON'] == "true"){
                print '<h1 class="page-title icon-' . $_WEBCONFIG['MODULE_ICON'] . '">' . $_WEBCONFIG['FRIENDLY_NAME'] . '</h1>
                <p class="page-desc">Modify ' . $_WEBCONFIG['FRIENDLY_NAME'] . '</p>';
            } else {
                print '<h1 class="page-title icon-' . $_WEBCONFIG['MODULE_ICON'] . '">' . $_WEBCONFIG['MODULE_NAME'] . '</h1>
                 <p class="page-desc">' . $_WEBCONFIG['MODULE_DESCRIPTION'] . '</p>';
            }

     print '</div>
        </section>';
print '<section class="maincontent"><div class="container clearfix">';

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/modules/modulesNavBar.php');

if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $_WEBCONFIG['MODULE_INSTALL_FOLDER'])) {

    $uploadFolder = $_WEBCONFIG['UPLOAD_FOLDER'];
    $view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

    switch ($view) {
    	case 'add' :
        case 'modify' :
            $script = array($_WEBCONFIG['MODULE_FOLDER'] . 'js/form.js', '/jquery/parsley.min.js');
            include 'add-modify.php';
            break;
    	default :
    		$script	= array($_WEBCONFIG['MODULE_FOLDER'] . 'js/list.js');
    		include 'list.php';
    }
} elseif(!is_writable($_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['UPLOADS_DIRECTORY'] . '/')) {
    print '
    <div class="alignCenter">
        <br><h3>Uploads folder does not have the correct permissions. Please correct the permissions before continuing.</h3><br>
        <a href="' . $_SERVER['PHP_SELF'] . '" class="button yellow" style="margin-right: 5px;">Refresh Page</a>
    </div>';
} elseif(UserManager::isWebmaster()) { // Install folder exists and user is a webmaster
    print '
    <div class="alignCenter">
        <h3>Please click the button below to install the ' . $_WEBCONFIG['MODULE_NAME'] . '</h3><br>
        <a href="' . $_WEBCONFIG['MODULE_INSTALL_FOLDER'] . 'install.php" class="button yellow" style="margin-right: 5px;">Install ' . $_WEBCONFIG['MODULE_NAME'] . '</a>
    </div>';
} else { // Install folder exists and user is NOT a webmaster
    print $_WEBCONFIG['MODULE_NAME'] . " has not been installed yet.";
}

print '</div></section>';

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");