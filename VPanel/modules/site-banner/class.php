<?php
class Process extends Database {


    private $record;
    private $table;
    public $params;
    private $upload_dir;

    //------------------------------
    public function __construct($table, $uploadsDir) {
        //------------------------------
        global $_WEBCONFIG;
        parent::__construct();
        $this->table = $table;
        $this->upload_dir = $uploadsDir;
    }

    //------------------------------
    public function _add() {
        //------------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify(1);

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=add";
        } else {
            // No Errors
            $this->record->hn_date_added = date("Y-m-d G:i:s");
            $this->record->hn_user_save = $_SESSION['user_username'];
            $newId = $this->record->insert();

            Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Created", "Success", "Created -- '{$this->record->hn_title}' (" . $newId .")", $_SESSION['user_username']);
            $_SESSION['processed'] = 'added';
            $this->params = "view=modify&id=". $newId;
        }
    }

    //------------------------------
    public function _save() {
        //------------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify(2, $_POST['hidHistoricId']);

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=modify&id={$_POST['hidId']}";
        } else {
            // No Errors

            $this->record->hn_user_save = $_SESSION['user_username'];
            $this->record->hn_date_added = date("Y-m-d G:i:s");
            $this->record->hn_historic_id = strlen(trim($_POST['hidHistoricId'])) > 0 ? (int)$_POST['hidHistoricId'] : (int)$_POST['hidId'];
            $newId = $this->record->insert();

            Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Saved", "Success", "Saved -- '{$this->record->hn_title}' (" . $newId .")", $_SESSION['user_username']);
            $_SESSION['processed'] = 'saved';
            $this->params = "view=modify&id=". $newId;
        }
    }

    //----------------------------------
    public function _savePublish() {
        //------------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify(2, $_POST['hidHistoricId']);

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $newId = "";
        } else {
            // No Errors
            $this->record->hn_user_save = $_SESSION['user_username'];
            $this->record->hn_date_added = date("Y-m-d G:i:s");
            $this->record->hn_historic_id = strlen(trim($_POST['hidHistoricId'])) > 0 ? (int)$_POST['hidHistoricId'] : (int)$_POST['hidId'];
            $newId = $this->record->insert();
            Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Saved", "Success", "Saved -- '{$this->record->hn_title}' (" . $newId .")", $_SESSION['user_username']);
        }

        return $newId;
    }

    //------------------------------
    public function _publish() {
        //------------------------------
        global $_WEBCONFIG, $form;

        // Save Record
        $newId = self::_savePublish();

        if(!isNullOrEmpty($newId)){

            $sql = "SELECT hn_id, hn_historic_id
            FROM {$this->table}
            WHERE hn_id = {$newId}
            LIMIT 1";
            $record = Database::Execute($sql);

            if($record->Count() > 0){
                $record->MoveNext();

                $historicID = (int)$record->hn_historic_id;

                $sql = "SELECT *
                FROM $this->table
                WHERE hn_id = $historicID
                LIMIT 1";
                $historicRecord = Database::Execute($sql);
                $historicRecord->MoveNext();

                //Delete the Live Message
                Database::ExecuteRaw("DELETE FROM $this->table WHERE hn_id = $historicID");


                // Update current revision to become the live one
                $sql = "UPDATE $this->table
                SET hn_is_published = 1, hn_id = {$historicID}, hn_historic_id = NULL, hn_user_publish = '{$_SESSION['user_username']}'
                WHERE hn_id = {$record->hn_id}
                LIMIT 1";
                Database::ExecuteRaw($sql);

                // Insert Revision
                $this->record = new Entity($this->table);
                $this->record->hn_historic_id       = $historicID;
                $this->record->hn_promo_or_alert    = $historicRecord->hn_promo_or_alert;
                $this->record->hn_title             = $historicRecord->hn_title;
                $this->record->hn_text              = $historicRecord->hn_text;
                $this->record->hn_url               = $historicRecord->hn_url;
                $this->record->hn_status            = $historicRecord->hn_status;
                $this->record->hn_pending_delete    = $historicRecord->hn_pending_delete;
                $this->record->hn_is_published      = $historicRecord->hn_is_published;
                $this->record->hn_user_save         = $_SESSION['user_username'];
                $this->record->hn_user_publish      = $historicRecord->hn_user_publish;
                $this->record->hn_date_added        = $historicRecord->hn_date_added;
                $this->record->hn_last_update       = $historicRecord->hn_last_update;
                // $this->record->hn_time_start        = $historicRecord->hn_time_start;
                // $this->record->hn_time_end          = $historicRecord->hn_time_end;
                $this->record->hn_image             = $historicRecord->hn_image;
                $this->record->hn_button_text       = $historicRecord->hn_button_text;
                $this->record->insert();

                Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Published", "Success", "Published -- '{$newId}' (" . $newId .")", $_SESSION['user_username']);
                $_SESSION['processed'] = 'published';
                $this->params = "view=list";
            }

        } else {
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=modify&id={$_POST['hidId']}";
        }
    }


    //------------------------------
    public function _pendingDelete() {
        //------------------------------
        global $_WEBCONFIG, $form;
        $iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

        $sql = "UPDATE $this->table SET hn_pending_delete = 1 WHERE hn_id = $iNum";
        $res = Database::ExecuteRaw($sql);

        $this->record = Repository::GetById($this->table, $iNum);
        Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Pending Delete", "Success", "Pending Delete -- '{$this->record->hn_title}' (" . $this->record->hn_id .")", $_SESSION['user_username']);
        $_SESSION['processed'] = 'pending-delete';
        $this->params = "view=list";
    }

    //------------------------------
    public function _cancelDelete() {
        //------------------------------
        global $_WEBCONFIG, $form;
        $iNum    = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

        $sql = "UPDATE $this->table SET hn_pending_delete = 0 WHERE hn_id = $iNum";
        $res = Database::ExecuteRaw($sql);

        $this->record = Repository::GetById($this->table, $iNum);
        Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Cancel Delete", "Success", "Cancel Delete -- '{$this->record->hn_title}' (" . $this->record->hn_id .")", $_SESSION['user_username']);
        $_SESSION['processed'] = 'cancel-pending-delete';
        $this->params = "view=list";
    }

    //------------------------------
    public function _delete() {
        //------------------------------
        global $_WEBCONFIG, $form;
        $iNum    = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

        $this->record = Repository::GetById($this->table, $iNum);

        if(isNullOrEmpty($this->record->hn_historic_id)){
            $historicId = 0;
        } else {
            $historicId = $this->record->hn_historic_id;
        }

        $sql = "UPDATE $this->table SET hn_pending_delete = 0, hn_status = 'Deleted' WHERE hn_id = $iNum OR hn_historic_id = $iNum OR hn_id = " . $historicId;
        $res = Database::ExecuteRaw($sql);

        Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Deleted", "Success", "Deleted -- '{$this->record->hn_title}' (" . $iNum .")", $_SESSION['user_username']);
        $_SESSION['processed'] = 'deleted';
        $this->params = "view=list";
    }

    //------------------------------
    private function _verify($ID, $historicID = 0) {
        //------------------------------
        global $_WEBCONFIG, $form;

        $fieldName  = "optPromoAlert";
        $fieldValue = isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Type is required.");
        } else {
            $this->record->hn_promo_or_alert = $fieldValue;
        }
        $fieldName  = "txtTitle";
        $fieldValue = strip_tags($_POST[$fieldName], HTML_FORMATTING_TAGS);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Title is required.");
        } else {
            $this->record->hn_title = $fieldValue;
        }

        $fieldName  = "txtText";
        $fieldValue = strip_tags($_POST[$fieldName], HTML_FORMATTING_TAGS);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Text is required.");
        } else {
            $this->record->hn_text = $fieldValue;
        }

        $fieldName  = "txtUrl";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if(!isNullOrEmpty($fieldValue)) {
            $this->record->hn_url = $fieldValue;
        } else {
            $this->record->hn_url = null;
        }

        $fieldName  = "txtButtonText";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->hn_button_text = "Learn More";
        } else {
            $this->record->hn_button_text = $fieldValue;
        }

        if($this->record->hn_promo_or_alert == 'promo'){
            $fieldName  = "txtImageUrl";
            $fieldValue = strip_tags($_POST[$fieldName], HTML_FORMATTING_TAGS);

            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                $form->setError($fieldName, "* Promo image is required.");
            } else {
                if(stripos($fieldValue, '/uploads/userfiles/files/') !== false) {
                    if(stripos($fieldValue, $_WEBCONFIG['UPLOAD_FOLDER']) === false) {
                        self::_copyToPath($fieldValue);
                    } else {
                        $this->record->hn_image = str_replace($_WEBCONFIG['UPLOAD_FOLDER'],'',$fieldValue);
                    }
                } else {
                    if(isset($_FILES['fleImage']) && $_FILES['fleImage']['name'] != ""){
                        self::_uploadImage("fleImage");
                    }  else {
                        $this->record->hn_image = "";
                    }
                }
            }
        } else {
            $this->record->hn_image = "";
        }

        // if(isNullOrEmpty($_POST['txtStartTime'])) {
        //     $this->record->hn_time_start = date('Y-m-d H:i:s');
        // } else {
        //     $timestamp = strtotime($_POST['txtStartTime']);
        //     $this->record->hn_time_start = date('Y-m-d H:i:s', $timestamp);
        // }

        // if(isNullOrEmpty($_POST['txtEndTime'])) {
        //     $this->record->hn_time_end = date('Y-m-d H:i:s',strtotime("+ 1 years"));
        // } else {
        //     $timestamp = strtotime($_POST['txtEndTime']);
        //     $this->record->hn_time_end = date('Y-m-d H:i:s', $timestamp);
        // }

        $fieldName  = "optStatus";
        $fieldValue = isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Status is required.");
        } else {
            $this->record->hn_status = $fieldValue;
        }

        $fieldName  = "optType";
        $fieldValue = isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Type is required.");
        } else {
            if($fieldValue == "Both"){
                $this->record->hn_type = 0;
            } elseif($fieldValue == "Claims Professional"){
                $this->record->hn_type = 1;
            } elseif($fieldValue == "Provider Portal"){
                $this->record->hn_type = 2;
            } else {
                $form->setError($fieldName, "* Invalid Type.");
            }
        }
    }

    //------------------------------
    private function _uploadImage($fieldName) {
        //------------------------------
        global $form, $_WEBCONFIG;

        if (isset($_FILES[$fieldName]) && strlen($_FILES[$fieldName]['name']) > 0) {
            list($width, $height, $type, $attr) = getimagesize($_FILES[$fieldName]['tmp_name']);
            $maxWidth   = '150';
            $maxHeight  = '150';

            $fieldValue = $_FILES[$fieldName]['name'];
            $file_ext   = strtolower(getExtName($fieldValue));

            if(stristr($_WEBCONFIG['VALID_IMGS'], $file_ext)) {
                $imagePath  = md5(rand() * time()) . ".{$file_ext}";
                $options = array('resizeUp' => true, 'jpegQuality' => 100, 'correctPermissions' => true);

                $thumb = PhpThumbFactory::create($_FILES[$fieldName]['tmp_name'], $options);
                $thumb->resize($maxWidth, $maxHeight);
                $thumb->save(filePathCombine($_SERVER['DOCUMENT_ROOT'], $this->upload_dir, $imagePath));

                if (isset($_POST['hidImage'])) {
                    $_POST['hidImage'] = $imagePath;
                }

                $this->record->hn_image = $imagePath;
            } else {
                $form->setError($fieldName, "** $fieldValue Has An Invalid Format.");
            }//end if
        }
        else if (isset($_POST['hidImage']) && strlen($_POST['hidImage']) > 0) {
            $this->record->hn_image = $_POST['fleImage'];
        }
    }

    //------------------------------
    private function _copyToPath($fieldValue) {
        //------------------------------
        global $form, $_WEBCONFIG;
        $fieldValue = strtolower($fieldValue);
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $fieldValue)) {
            $file_ext   = strtolower(getExtName($_SERVER['DOCUMENT_ROOT'] . $fieldValue));
            $imagePath  = md5(rand() * time()) . ".{$file_ext}";
            if(stripos($fieldValue, $this->upload_dir) !== false){
                $this->record->hn_image = str_replace($this->upload_dir, "", $fieldValue);
            } else {
                copy($_SERVER['DOCUMENT_ROOT'] . $fieldValue, $_SERVER['DOCUMENT_ROOT'] . $this->upload_dir . $imagePath);
                $this->record->hn_image = $imagePath;
            }

        } else {
            $this->record->hn_image = "";
        }
    }

    //---------------------------------------
    public function _removeInstallFiles($returnUrl) {
        //---------------------------------------
        global $_WEBCONFIG, $form;

        if(file_exists($_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['MODULE_INSTALL_FOLDER'])) {
            deleteIntallFolder($_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['MODULE_INSTALL_FOLDER']);
        }
        if(strlen($returnUrl) > 0) {
            redirect($returnUrl);
        } else {
            $this->params = "view=list";
        }
    }
};

function deleteIntallFolder($target) {
    if(is_dir($target)){
        $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
        foreach( $files as $file ){
            deleteFolder( $file );
        }
        rmdir( $target );
    } elseif(is_file($target)) {
        unlink( $target );
    }
}