<?php 
        $dir = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "modules/");
        if (is_dir($dir)) {  
            if ($dh = opendir($dir)) {  
                $count = 0;
                print '<nav class="subnav left">
                        <ul>'; 
                while (($directory = readdir($dh)) !== false) {  
                    $row = $count % 2 ? "odd" : "even"; 
                    $path =  $dir . "/" . $directory; 
                    if(filetype($path) == 'dir' && $directory != ".." && $directory != ".") {
						if(file_exists($path . "/web.config")) { 
							$settings = getWebConfigSettings($path);
							if($settings['MODULE_NAME'] != 'Form Builder') {  ?>
								<li><a href="<?= $_WEBCONFIG['VPANEL_PATH']?><?= $settings['MODULE_FOLDER'] ?>" class="icon-<?= $settings['MODULE_ICON'] ?>"><?= $settings['MODULE_NAME'] ?></a></li>
	<?php                    }
						}
                    }
                }
                print '</ul></nav>';  
                closedir($dh);  
            }  
        }  
    ?>