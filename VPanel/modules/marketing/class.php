<? 
class Process extends Database { 

    private $record;
    private $table;
    private $upload_dir;
    public $params;

    //--------------------------------------------------
    public function __construct($table, $upload_dir) {
    //--------------------------------------------------
        global $_WEBCONFIG;
        parent::__construct();
        $this->table        = $table;
        $this->upload_dir    = $upload_dir; 
    }

    //-----------------------
    public function _add() {
    //-----------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=add";
        } else {
            // No Errors
            $this->record->mk_date_added = date("Y-m-d G:i:s");
            Repository::Save($this->record); 
            $iNum = Database::getInsertID(); 
            Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Added", "Success", "Added -- '{$this->record->mk_name}' (" . $iNum .")", $_SESSION['user_username']);            
            $_SESSION['processed'] = 'added';
            $this->params = "view=list";
        }
    }

    //---------------------------
    public function _modify() {
    //---------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=modify";
        } else {
            // No Errors
            $this->record->mk_id = (int)$_POST['hidId'];
            $this->record->update();
            Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Modified", "Success", "Modified -- '{$this->record->mk_name}' (" . $this->record->mk_id .")", $_SESSION['user_username']);
            $_SESSION['processed'] = 'updated';
            $this->params = "view=list";
        }
    }

    //---------------------------
    public function _delete() {
    //---------------------------
        global $_WEBCONFIG, $form;
        $iNum    = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

        $res = self::_deleteImage($iNum);

        $this->record = Repository::GetById($this->table, $iNum); 
        Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Deleted", "Success", "Deleted -- '{$this->record->mk_name}' (" . $this->record->mk_id .")", $_SESSION['user_username']);
        Repository::Delete($this->record); 
        $_SESSION['processed'] = 'deleted';
        $this->params = "view=list";
    }

    //-------------------------
    public function _sort() {
    //-------------------------
        $indexCounter     = 1;
        $updRecordsArray = $_POST['recordsArray'];

        foreach($updRecordsArray as $recordIDValue) {
            $sql = "UPDATE {$this->table} SET mk_sort_id = $indexCounter WHERE mk_id = $recordIDValue";
            $res = Database::ExecuteRaw($sql);
            $indexCounter++;    
        }
        print "Records were re-ordered!";
    }
    
    //---------------------------------------
    public function _removeInstallFiles($returnUrl) {
    //---------------------------------------
        global $_WEBCONFIG, $form;
        
        if(file_exists($_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['MODULE_INSTALL_FOLDER'])) {
            deleteFolder($_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['MODULE_INSTALL_FOLDER']);   
        }
        if(strlen($returnUrl) > 0) {
            redirect($returnUrl);
        } else {
            $this->params = "view=list";    
        }
        
    }    

    //----------------------------
    private function _verify() {
    //----------------------------
        global $_WEBCONFIG, $form;

        $fieldName    = "txtName";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Title is required.");
        } else {
            $this->record->mk_name = $fieldValue;
        }

        $fieldName    = "txtTagLine";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Tag Line is required.");
        } else {
            $this->record->mk_tagline = $fieldValue;
        }

        $fieldName    = "txtWebsite";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if (!isNullOrEmpty($fieldValue)) { 
            $this->record->mk_url = $fieldValue;
        }

        $fieldName  = "cboTarget";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->mk_target = "self";    
        } else {
            $this->record->mk_target = $fieldValue;
        }

        $fieldName  = "optPage";
        $fieldValue = strip_tags(implode('|', $_POST[$fieldName]));
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->mk_display_page = "H";    
        } else {
            $this->record->mk_display_page = $fieldValue;
        }
        
        $fieldName  = "txtButtonText";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->mk_button_text = "Learn More";    
        } else {
            $this->record->mk_button_text = $fieldValue;
        }        

        self::_uploadImage("fleImage");

        $fieldName    = "optStatus";
        $fieldValue    = isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';

        $iNum =  isset($_POST['hidId']) ? (int)$_POST['hidId'] : 0;

        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Status is required.");
        } else if ($fieldValue != "Inactive" && self::getActiveCount($iNum) >= $_WEBCONFIG['ACTIVE_LIMIT']) {  
            $form->setError($fieldName, "* Only " . $_WEBCONFIG['ACTIVE_LIMIT'] . " Marketing Messages can be active at one time, please disable 1 or more to continue. ");
        } else {
            $this->record->mk_status = $fieldValue;
        }

        if (isset($_WEBCONFIG['SITE_TYPE']) && $_WEBCONFIG['SITE_TYPE'] == 'BANK') {  
            $fieldName    = "optDisclaimer";
            $fieldValue    = isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';
            $this->record->mk_disclaimer = $fieldValue;
        }
    }

    //------------------------------------------
    private function _uploadImage($fieldName) {
    //------------------------------------------
        global $form, $_WEBCONFIG;

        if (isset($_FILES[$fieldName]) && strlen($_FILES[$fieldName]['name']) > 0) {
            list($width, $height, $type, $attr) = getimagesize($_FILES[$fieldName]['tmp_name']);
            $maxWidth    = $width; 
            $maxHeight    = $height; 
            $fieldValue    = $_FILES[$fieldName]['name'];
            $file_ext    = strtolower(getExtName($fieldValue)); 
            if(stristr($_WEBCONFIG['VALID_IMGS'], $file_ext)) {
                $imagePath    = md5(rand() * time()) . ".{$file_ext}";
                $options = array('resizeUp' => true, 'jpegQuality' => 100, 'correctPermissions' => true);
                $thumb = PhpThumbFactory::create($_FILES[$fieldName]['tmp_name'], $options);
                $thumb->resize($maxWidth, $maxHeight);
                $thumb->save(filePathCombine($_SERVER['DOCUMENT_ROOT'], $this->upload_dir, $imagePath));

                if (isset($_POST['hidImage'])) 
                    $res = self::_deleteImageFile($_POST['hidImage']);

                $_POST['hidImage']                = $imagePath;
                $this->record->mk_image    = $imagePath;

                if (isset($_POST['hidId'])) {
                    $res = self::_deleteImage((int)$_POST['hidId']);
                }
            } else {
                $form->setError($fieldName, "** $fieldValue Has An Invalid Format.");
            }
        } 
        else if (isset($_POST['hidImage']) && strlen($_POST['hidImage']) > 0) {
            $this->record->mk_image = $_POST['hidImage'];
        }
    }

    //--------------------------------------------
    private function getActiveCount($iNum = 0) {
    //--------------------------------------------
        global $_WEBCONFIG;
        $sql     = "SELECT count(*) as Count FROM {$this->table} WHERE mk_status = 'Active' AND mk_id != $iNum";
        $record     =  Database::Execute($sql);
        $record->MoveNext();

        return $record->Count; 
    }

    //----------------------------------------
    public function _checkDomainValidity() {
    //----------------------------------------
        $site = isset($_POST['site']) ? $_POST['site'] : "";

        if(strlen($site) > 0) {
            if(strpos($site, "http")) {}
            print get_url_status($site);
        } else {
            print "";
        }
    }

    //--------------------------------------
    private function _deleteImage($iNum) {
    //--------------------------------------
        $deleted = false; 
        $sql     = "SELECT mk_image FROM {$this->table} WHERE mk_id = $iNum";
        $record     =  Database::Execute($sql);
        $record->MoveNext();
        if ($record->Count() > 0 && $record->mk_image) {
            $deleted = self::_deleteImageFile($record->mk_image);
        }

        return $deleted;
    }

   //-----------------------------------------------
    private function _deleteImageFile($hidImage) {
    //----------------------------------------------
        global $_WEBCONFIG;
        $deleted = false;

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $this->upload_dir . $hidImage)) {
            //$deleted = unlink($_SERVER['DOCUMENT_ROOT'] . $this->upload_dir . $hidImage);
        }

        return true;
    }

};
?>