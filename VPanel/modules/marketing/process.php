<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/ThumbLib.inc.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 

security::_secureCheck();

require_once "class.php";

$p = new Process($_WEBCONFIG['MODULE_TABLE'], $_WEBCONFIG['UPLOAD_FOLDER']);
$action    = isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';
$returnUrl = isset($_GET['returnUrl']) ? $_GET['returnUrl'] : '';

switch ($action) {
    case 'add' :
        $p->_add();
        break;

    case 'modify' :
        $p->_modify();
        break;

    case 'delete' :
        $p->_delete();
        break;

    case 'sort' :
        $p->_sort();
        exit();
        break;
        
    case 'removeInstallFiles' :
        $p->_removeInstallFiles($returnUrl);
        break;        

    case 'checkDomainValidity' :
        $p->_checkDomainValidity();
        exit();
        break;        

    default :
        throw new exeption("An unknown action executed!");
}

redirect("index.php?{$p->params}");
?>