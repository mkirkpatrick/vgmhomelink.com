<?php if (!isset($siteConfig)) die("System Error!"); ?> 

<div class="subcontent right last">
    <div class="floatRight forward">
        <a href="index.php?view=sort" class="button silver icon-arrows-ccw">Reorder</a>
    </div>

    <?php
    print '<h1>' . $_WEBCONFIG['MODULE_NAME'] . '</h1>';

    if ($form->getNumErrors() > 0) {
        $errors	= $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
                break;
            case 'updated':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                break;
            case 'deleted':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                break;
        }
        unset($_SESSION['processed']);
    }

    $sql = "SELECT * FROM {$_WEBCONFIG['MODULE_TABLE']} ORDER BY mk_sort_id, mk_name";
    $record	= Database::Execute($sql);

    print $record->Count() == 1 ? "<p>There is currently <b>1</b> " . $_WEBCONFIG['ENTITY_NAME'] . "</p>\n" : "<p>There are currently <b>" . $record->Count() . "</b> " . $_WEBCONFIG['ENTITY_NAME'] . "s. </p>\n";
    ?>

    <table id="grid" class="none">
        <thead> 
            <tr> 
                <th data-field="title">Title</th>
                <th data-field="tagLine">Tag Line</th>
                <th data-field="page">Page</th>
                <th data-field="status">Status</th>
                <th data-field="lastDate">Last Modified Date</th>
                <th data-field="options" style="text-align: center;">Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($record->Count() > 0) {
                while ($record->MoveNext()) {
                    print '
                    <tr> 
                        <td>' . $record->mk_name . '</td>
                        <td>' . str_truncate($record->mk_tagline, 75) . '</td>
                        <td>' . $record->mk_display_page . '</td>
                        <td>' . $record->mk_status . '</td>
                        <td>' . date("m/d/Y g:i A", strtotime($record->mk_last_update)) . '</td>
                        <td>
                            <div align="center">
                                <a href="javascript:modifyMarketing(' . $record->mk_id . ');" class="green button-slim">Modify</a>
                                <a href="javascript:deleteMarketing(' . $record->mk_id . ');" class="red button-slim">Delete</a>
                            </div>
                        </td>
                    </tr>';
                }// end while
            }
            ?>
        </tbody>
    </table>

    <div class="buttons clearfix">
        <a href="javascript:history.back()" class="button blue floatLeft">Back</a>
        <a href="javascript:addMarketing()" class="button green floatRight">Add New <?= $_WEBCONFIG['ENTITY_NAME'] ?></a>
    </div>
</div><!--End continue-->

<script type="text/x-kendo-template" id="searchBarTemplate">
    <div class="toolbar floatRight">
        <div>
            <input id="txtSearch" placeholder="Search Grid" type="text" style="width: 175px" />
            <a id="clearTextButton">Clear</a>
        </div>
    </div>
</script>