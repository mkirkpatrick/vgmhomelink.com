<?php
// this file reports formbuilder json
// most of this is asset management
// the saving and restoration is in the template / page builder backend


require_once 'FormBuilder.php' ; // formbuilder class
require_once $_SERVER['DOCUMENT_ROOT'] .'/VPanel/modules/forms/classes/class_asset.php'; // Asset class
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';// Asset class

security::_secureCheck();

$do = isset($_GET['do']) ? $_GET['do'] : '';

switch($do){
    // returns widgets serialized json string  
    case 'getWidgets': 
        endOnSuccess(FormBuilder::getWidgets()); break;
    case 'saveForm':  
        $asset =(FormBuilder::saveForm()); 
        endOnSuccess($asset); 
        break; 
    case 'getLastFormId':  
        endOnSuccess(FormBuilder::getLastFormEdited()); 
        break; 
    default:
}


function endHow($success,$data){die(json_encode( array('success'=>$success,'data'=>$data)));}
function endOnError($data){ endHow(false,$data); }
function endOnSuccess($data){ endHow(true,$data);  }


?>