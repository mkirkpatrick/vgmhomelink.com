var kludgeHeightCell = function(){ $('.form-builder').not('.helper').find('.cll').css({"height": $(window).height() - $('.form-builder > .header').height() - $('body > .topheader').height() -$('body > .subnav').height()+'px' }); };
var FormBuilder = function(){
    kludgeHeightCell();
    $(window).resize(kludgeHeightCell);
    this.startup();
};

FormBuilder.prototype.sorting = function(){
    var fBuilder = this;
    this.getForm().sortable({
        items: '> .widget', distance: 2, cursor: 'move', start: function (e) { $('.edit-helper').blur(); }, update: function () {
            fBuilder.decorateInputs();
			fBuilder.closeButtonEvents();
			fBuilder.duplicateEvents();
            fBuilder.save();
        }
    });
};

FormBuilder.prototype.loadForm = function(data){
    var key = data.key;
    var name = data.name;
    var settings = data.settings;
    var val = data.value;
    var fBuilder = this;

    // set form name
    $('.form-builder .header > li:eq(1)').html(name);

    // settings
    //$('#fbSettingsEmailResultsTo').val(settings.emailResultsTo);
    //$('#fbSettingsEmailResultsTo').blur(function(){ fBuilder.save(); });
    //$('#fbSettingsSuccessPageSelect').val(settings.successPage);


    // set form content
    this.getForm().html(this.getFormDisplayHTML(val));
    this.decorateInputs();
    this.closeButtonEvents();
    this.duplicateEvents();
    this.sorting();

    this.settingsEmailReceiptListRefresh();

    if(typeof (settings.receiptField) != 'undefined') $('#fbSettingsReceipt').val(settings.receiptField);
};

FormBuilder.prototype.loadQuickForm = function(){
    if(quickForm){
        this.loadForm(quickForm);
    } else {
        this.loadLastForm();
    }
};
// get id of last edited form and load it
FormBuilder.prototype.loadLastForm= function(){
    var fBuilder = this;
    this.chatter(
        function(data){
            if(typeof data.success != "undefined" && data.success === true) {
                fBuilder.loadForm(data.data);
            }else {}
        }
        ,'getLastFormId'
        ,{}
    );

};
FormBuilder.prototype.startup = function(){
    // global markup templates

    this.dragListItemMarkupTemplate =  '<li class="button-bg-basic" data-widget="#NAME#">#NAME#</li>';

    // settings open close toggle


    //$('.fb-settings-button > a.slap').click(toggleSettings);
    // load widgets from back
    this.getWidgets();

    // save widget list
    this.widgetList = $('.widget-list');

    // prevent selection
    document.onselectstart = function(e){return false;};

    // PSEUDO load last edited  do event handlers
    this.loadQuickForm(); // this.loadLastForm();

    // editable form name
    this.editMe($('.form-builder .header li:eq(1)'));


};

// setup helpers for dragdrop
FormBuilder.prototype.setupHelpers = function(){
    // clone button
    $('body').append($('.form-builder').clone().addClass('helper'));
    $('.form-builder.helper').find('.widget-list .list li:gt(0), .header').remove();

    // add

};
// is a point inside an element
FormBuilder.prototype.hitTest = function(Vec2, element){
    return (Vec2[0] >= $(element).offset().left && Vec2[0] <= $(element).offset().left + $(element).width())
    &&(Vec2[1] >= $(element).offset().top && Vec2[1] <= $(element).offset().top + $(element).height())
    ;

};
FormBuilder.prototype.getFormBuilder = function(){
    return $('.form-builder').not('.helper');
};

FormBuilder.prototype.getForm = function(){
    return this.getFormBuilder().find('.form-builder-canvas > li.cell:eq(0)');
};

FormBuilder.prototype.getFormName = function(){
    return this.getFormBuilder().find('.header > li:eq(1)').text();
};

FormBuilder.prototype.settingsEmailReceiptListRefresh = function(){
    var form = this.getForm();
    var fBuilder = this;
    var receiptField = $('#fbSettingsReceipt').val();


    // field to send emails receipt to
    $('#fbSettingsReceipt').html('<option val="">-don\'t send a receipt email-</option>');
    form.find('input[type="email"]').each(function(k,v){
        var thisInput = $(v);
        var cleanName = $.trim((thisInput[0].name+'').replace(/[^a-z0-9 ]/ig,''));
        var sel = '';


        if(cleanName == receiptField) {
            sel = ' selected="selected" ';
        }
        $('#fbSettingsReceipt').append('<option '+sel+' val="'+cleanName+'">'+cleanName+'</option>');
    });
    $('#fbSettingsReceipt').change(function(e){fBuilder.save();});
};

FormBuilder.prototype.save = function(){
    popLoading();
    var form = this.getForm();
    var formName = this.getFormName();
    var fBuilder = this;

    this.settingsEmailReceiptListRefresh();

    this.chatter(function(data){
        if(typeof data.success != "undefined" && data.success === true) {}
        else {
            fBuilder.warning('The server isn\'t accepting changes. Try reloading the page.');
        }
        }
        ,'saveForm'
        ,{
            "key":formName
            ,"value":fBuilder.getFormCurrentHTML(form.html())
            ,"settings":{}
        }
    );

};

FormBuilder.prototype.getFormDisplayHTML = function(formHtml){
    var retHtml =  $($(formHtml).find('.cell:eq(0)'));
    retHtml.find('.vHidden, input[type="hidden"]').remove();
    retHtml = retHtml.html();
    return retHtml;
};


FormBuilder.prototype.getFormCurrentHTML = function(formHtml){
    // strip selected off of selects
    var markup = '<div class="form-builder">'
    + '<form class="formBuilderForm" action="/library/FormProcessor.php" method="POST" id="fbForm" enctype="multipart/form-data">'
    + '<input id="hidFormName" name="hidFormName" value="'+this.getFormName()+'" type="hidden">'
    + '<ul class="cell pbform clearfix nobullets">'
    + '%%PHP_RC_CAPTCHA%%'
    + formHtml
    + '</ul>'
    + '<ul class="cell pbform clearfix nobullets">'
    + '<li class="twelvecol button-row">'
    + '<div class="vHidden g-recaptcha" data-sitekey="6LfIam0UAAAAAMZEgetSGyu2LHIDvMMW_n0xcZxL"></div>'
    + '<input class="vHidden fbForm button blue" value="Submit Form" id="btnSubmit" type="submit">'
    + '</li>'
    + '</ul>'
    + '</form></div>';

    var tree = $('<div>'+markup+'</div>');
    tree.find('.ui-sortable-placeholder').remove();
    tree.find('.widget.basic-form-styles').css({"top":0,"left":0,"position":"relative"});
    tree.find('select option[selected]').removeAttr('selected');
    markup = tree.html();

    return markup;
};

// makes unique fieldname from li
FormBuilder.prototype.newFieldName = function(li, markupInProgress){
    var form = this.getForm();
    var name = li.find('> label:eq(0)').text(); // 1st label in li is name

    var findAGuid = function(markup, maybeName, level){
        var proposed = maybeName+level;
        var howManyAlreadyExist = $(markup).find('[name="'+proposed+'"]').length;
        if($(markupInProgress).find('[name="'+proposed+'"]').length > howManyAlreadyExist) {
            howManyAlreadyExist = $(markupInProgress).find('[name="'+proposed+'"]').length;
        }
        if( howManyAlreadyExist == 0)	{
            return proposed;
        } else {
            return findAGuid(markup, maybeName, level+1);
        }
    };

    return findAGuid(form.html(), name,0);
};
FormBuilder.prototype.dimensions = function(element){
    var theDiv = $(element);
    var totalWidth = theDiv.width();


    totalWidth += parseInt(theDiv.css("padding-left").replace('px','').replace('none',''), 10) + parseInt(theDiv.css("padding-right").replace('px','').replace('none',''), 10); //Total Padding Width
    totalWidth += parseInt(theDiv.css("margin-left").replace('px','').replace('none',''), 10) + parseInt(theDiv.css("margin-right").replace('px','').replace('none',''), 10); //Total Margin Width
    totalWidth += parseInt(theDiv.css("borderLeftWidth").replace('px','').replace('none',''), 10) + parseInt(theDiv.css("borderRightWidth").replace('px','').replace('none',''), 10); //Total Border Width
    var totalHeight = theDiv.height();
    totalHeight += parseInt(theDiv.css("padding-top").replace('px','').replace('none',''), 10) + parseInt(theDiv.css("padding-bottom").replace('px','').replace('none',''), 10); //Total Padding Width
    totalHeight += parseInt(theDiv.css("margin-top").replace('px','').replace('none',''), 10) + parseInt(theDiv.css("margin-bottom").replace('px','').replace('none',''), 10); //Total Margin Width
    totalHeight += parseInt(theDiv.css("borderTopWidth").replace('px','').replace('none',''), 10) + parseInt(theDiv.css("borderBottomWidth").replace('px','').replace('none',''), 10); //Total Border Width

    return {'width':totalWidth,'height':totalHeight};
};

FormBuilder.prototype.editMe = function(element){
    var fBuilder = this;
    var editingClass = 'is-editing';
    $element = $(element);

    // special processing for select box editing
    if($element.is('select')){
        // get current selected


        $element.change(
            function(e){
                var optionToEdit = $element.find('option:selected');

                // if is first $element give up
                if(optionToEdit.is(':first')) {return;}
                e.preventDefault();
                var editingNow = $(this);
                $('.edit-helper').remove();
                $('body').append('<textarea class="edit-helper" data-orgtext="'+optionToEdit.html()+'"></textarea>');
                $('.edit-helper')
                .css(
                    {
                        "left":editingNow.offset().left+'px'
                        ,"top":editingNow.offset().top+'px'
                        ,'font-size': getStyle(editingNow[0],'fontSize')//document.defaultView.getComputedStyle(editingNow[0], null).fontSize
                    }
                )
                .val(optionToEdit.html())
                .focus(function(e){$(this)[0].select();})
                .focus()
                .blur(
                    function(){
                        var newText = $.trim($(this).val()); if(newText == '') newText = $(this).attr('data-orgtext'); if(newText == '') newText = 'Untitled Field';

                        optionToEdit.html(newText).val(newText);
                        $(this).remove();
                        fBuilder.decorateInputs();
                        fBuilder.save();
                    }
                )
                .keyup(
                    function(e){

                        // esc and enter to save
                        if(e.which == 27 || (e.which == 13 && !e.shiftKey)){
                            $(this).blur();
                        }
                        // ctrl+a
                        if(e.which = 65 && e.ctrlKey){
                            $(this)[0].select();
                        }

                        // shift+enter = new line
                        if(e.which == 13 && e.shiftKey ) {
                            var val = $(this).val();
                            var caret = fBuilder.getCaret(this);
                            $(this).val(
                                val.substring(0,caret)
                                +"\n"
                                +val.substring(caret,val.length-1)
                            );
                        }

                        // enter means save field
                        if(e.which == 13 && !e.shiftKey ) {$(this).blur(); }
                });

        });

        return;
    }
    var orgText = $element.text();


    $element.on("click", function(e){
        e.preventDefault();
        var editingNow = $(this);
        $('.edit-helper').remove();
        $('body').append('<textarea class="edit-helper" data-orgtext="'+editingNow.text()+'"></textarea>');
        $('.edit-helper')
        .css({"left":editingNow.offset().left+'px',"top":editingNow.offset().top+'px','width':fBuilder.dimensions(editingNow).width+'px','height':fBuilder.dimensions(editingNow).height+'px','font-size': getStyle(editingNow[0],'fontSize')})
        .val(editingNow.text())
        .focus(function(e){$(this)[0].select();})
        .focus()
        .blur(
            function(){
                var newText = $.trim($(this).val());
				if(newText == '' && $(this).attr('data-orgtext') != '') {
					newText = $(this).attr('data-orgtext');
				} else if(newText == '') {
					newText = 'Untitled';
				}
                editingNow.html(newText);
                $(this).remove();
                fBuilder.decorateInputs();
                fBuilder.save();
            }
        )
        .keyup(
            function(e){

                // esc and enter to save
                if(e.which == 27 || (e.which == 13 && !e.shiftKey)){
                    $(this).blur();
                }
                // ctrl+a
                if(e.which = 65 && e.ctrlKey){
                    $(this)[0].select();
                }



                // shift+enter = new line
                if(e.which == 13 && e.shiftKey ) {
                    var val = $(this).val();
                    var caret = fBuilder.getCaret(this);
                    $(this).val(
                        val.substring(0,caret)
                        +"\n"
                        +val.substring(caret,val.length-1)
                    );
                }

                // enter means save field
                if(e.which == 13 && !e.shiftKey ) {$(this).blur(); }
        });

    });
};

FormBuilder.prototype.getCaret = function(element){
    if (element.selectionStart) {
        return element.selectionStart;
    } else if (document.selection) {
        element.focus();
        var r = document.selection.createRange();

        if (r == null) { return 0; }

        var re = element.createTextRange(),
        rc = re.duplicate();
        re.moveToBookmark(r.getBookmark());
        rc.setEndPoint('EndToStart', re);

        return rc.text.length;
    }
    return 0;
};
// adds names
FormBuilder.prototype.decorateInputs = function() {
    var form = this.getForm();
    var fBuilder = this;
    var keys = {'exampleKeyName':0};

    var radioKeys = {'esampleKeyName':0};

    // loop thru radio groups and give them the 1st name
    $(form).find('.widget.basic-form-styles').each(function(k,v){
        var thisWidget = $(this);
        var isRadio = thisWidget.find('input').is('[type="radio"]');
        var isCheckbox = thisWidget.find('input').is('[type="checkbox"]');

        if(isCheckbox) {
            // CHECKIEDS ONLIES
            fBuilder.editMe(thisWidget.find('.radio-super-li'));
            fBuilder.editMe(thisWidget.find('li > label'));
            //var dupieName = thisWidget.find('.radio-super-li').html();
            dupieName = thisWidget.find('label').html();
            var index = '';
            if(typeof keys[dupieName] != 'undefined') {
                keys[dupieName]++;
                index = ' '+keys[dupieName];
            } else {
                keys[dupieName]=0;
            }

            thisWidget.find('input').attr('name', sanitizeAttribute(dupieName) +''+index+'[]');
            thisWidget.find('input').attr('title',dupieName);
            thisWidget.find('input').attr('id', "cbo" + sanitizeAttribute(dupieName) + index);
            thisWidget.find('li').each(function(k123,v123){
            // copy value from label to input
            // $(this).find('input').attr('value',$(this).find('> label:eq(0)').html());
            });
            thisWidget.find('li').each(function(k123,v123){
                // copy value from label to input
                $(this).find('input').attr('value',$(this).find('> span.fbEditable').html());
            });

        } else if(isRadio){
            dupieName = thisWidget.find('label').html();
            var index = '';
            if(typeof keys[dupieName] != 'undefined'){
                keys[dupieName]++;
                index = ' '+keys[dupieName];
            } else {
                keys[dupieName]= 0;
            }


            thisWidget.find('input').attr('name', sanitizeAttribute(dupieName) +''+index+'[]');
            thisWidget.find('input').attr('title',dupieName);
            thisWidget.find('input').attr('id', "opt" + sanitizeAttribute(dupieName) + index);

            thisWidget.find('li').each(function(k123,v123){
                // copy value from label to input
                $(this).find('input').attr('value',$(this).find('> span.fbEditable').html());
            });
        } else { // TEXTAREA && TEXTBOXES && SELECT BOXES
            $(thisWidget).find('div').each(function(kk,vv){
                var li = $(vv);
                fBuilder.editMe('label, h2, p, .radio-super-label, .fbEditable');
                // make it typeable
                li.find('input,select,textarea').each(function(kkk,vvv0){
                    var input = $(vvv0);
					var label = input.prevAll("label");
                    var key = label.text();

                    var index = '';
                    if(typeof keys[key] != 'undefined'){
						keys[key]++; index = ' '+keys[key];
					} else {
						keys[key]=0;
					}

                    var newName = sanitizeAttribute(key) +index;
					var uniqueID = 'txt' + sanitizeAttribute(newName);
					label.attr('for', uniqueID);
                    input[0].name = newName;
					input[0].title = key;
					input[0].id = uniqueID;
					input[0].setAttribute("data-parsley-required-message", "Please enter " + key);
                });
            });
        }
    });

    // loop thru dupie selects and do editme on changes
    $(form).find('li.fb-dupie').find('select').each(function(){
        fBuilder.editMe($(this));
    });
};

FormBuilder.prototype.duplicateEvents = function(){
    var fBuilder = this;
    $('.fb-duplicate-helper a.fb-duplicate-add, .fb-duplicate-helper a.fb-duplicate-delete').unbind('click').click(function(){
        var dupHelper = $(this).parent();
        var thisWidget = dupHelper.parent();
        var lastli = thisWidget.find('li:last');
        var isDuplicate = true;
        var dupTarget = 'dup-target';
        if($(this).is('.fb-duplicate-delete')){
            isDuplicate = false;
        }
        // if dup target lastli is some other element
        if($(thisWidget).find('.dup-target').length > 0){
            lastli = $(thisWidget).find('.dup-target');
        }
        if(isDuplicate){
            lastli.removeClass(dupTarget).after(lastli.clone().addClass(dupTarget));
            // force an editme
            if(lastli.is('option')){
                lastli.parent().find('option:last').html('Enter new value name').val('Enter new value name').attr('selected','selected');
                lastli.parent().change();
            }
        } else {
            if(confirm("Are you sure you want to remove the last item?")) {
                if(thisWidget.find('li').length > 1 || (lastli.is('option') && lastli.parent().find('option').length > 1)) {
                    lastli.prev().addClass(dupTarget).attr('selected','selected');
                    lastli.remove();
                }
            }
        }
        fBuilder.decorateInputs();
        fBuilder.save();
    });
};
FormBuilder.prototype.closeButtonEvents = function(){
    var fBuilder = this;

	$('.close-button').on("click", function(e) {
		$(this).parent().remove();
		fBuilder.save();
	});

	$('.copy-button').on("click", function(e) {
		$clone = $(this).parent().clone(true);
        //console.log($clone);
        $(this).parent().after($clone);
		fBuilder.save();
	});

    // do other stuff like required-switch
    $('.required-switch').on("click", function(e){
        if($(this).hasClass('on')){
            $(this).parent().find('select,input,textarea').removeAttr('required');
            $(this).parent().find('select,input,textarea').removeAttr('data-required');
            $(this).parent().find('li').removeClass('required');
            $(this).removeClass('on');
        } else {
            $(this).parent().find('select,input,textarea').attr('required','required');
            $(this).parent().find('li').addClass('required');
            $(this).addClass('on');
        }
        fBuilder.save();
    });
};

// for dragging / dropping keep placeholder under mouse
FormBuilder.prototype.trackMouse = function () {
    var fBuilder = this;
    var widgetHtml;
    $("ul.list li").draggable({
        connectToSortable: 'li.ui-sortable',
        helper: function () {
            var widgetType = $(this).attr('data-widget');
            fBuilder.settings.thisWidgetType = widgetType;
            var markup = fBuilder.settings.thisWidgetContent = fBuilder.settings.widgets[widgetType].markup;
            widgetHtml = fBuilder.widgetWrap(markup);
            return $("<div></div>").append(widgetHtml);
        },
        drag: function (e, ui) {
            $(this).attr("data-lastDragged", "");
        },
        stop: function (e, ui) {
			$(this).removeAttr("data-lastDragged");
			$("[data-lastDragged]").html(widgetHtml);
			$("[data-lastDragged]").removeAttr("data-widget");
			$("[data-lastDragged]").addClass("widget");
			$("[data-lastDragged]").addClass("widget basic-form-styles");
			$("[data-lastDragged]").removeAttr("data-lastDragged");
			fBuilder.decorateInputs();
			fBuilder.closeButtonEvents();
			fBuilder.duplicateEvents();
			fBuilder.save();
        }
    });
};

FormBuilder.prototype.widgetWrap = function(innerMarkup){
 // add dupie if needed
	var dupie = '';
	if(innerMarkup.indexOf('fb-dupie') !== -1) {
		dupie = '<div class="fb-duplicate-helper"><span class="fbAdmin">VALUE</span><a class="fb-duplicate-add" title="Add Another">+</a><a class="fb-duplicate-delete" title="Remove Last">-</a></div>';
	}

	if(innerMarkup.indexOf("DATATYPE: FORMTEXT") !== -1) {
		return innerMarkup+dupie+'<a class="close-button">x</a></div>';
	} else {
		return innerMarkup+dupie+'<a class="copy-button" title="Copy Field">*</a><a class="close-button" title="Delete Field">X</a><a class="required-switch">Required</a>';
	}
};

FormBuilder.prototype.endDrag = function(fBuilder){
    $('.form-builder.helper').hide();
};

//just positions helper... perhaps do collision detection for dropping here too
FormBuilder.prototype.widgetApplyDrag = function(fBuilder){
    var kludgeOffset = fBuilder.settings.widgetDragOffsetKludge;
    if($('.settingsContainer').is(':visible')) {
        kludgeOffset = [fBuilder.settings.widgetDragOffsetKludge[0],0];
    }

    // MUST FIX BUGS WHEN SCROLLED!!!!!!!!!! HERE
    $('.form-builder.helper').css({
        "left":(fBuilder.settings.mouse[0] -fBuilder.settings.widgetDragOffset[0] + kludgeOffset[0]-Math.ceil($('.helper .cell').height()/20))+'px'
        ,"top":(fBuilder.settings.mouse[1] -fBuilder.settings.widgetDragOffset[1] + kludgeOffset[1]-Math.ceil($('.helper .cell').width()/20))+'px'
    });
};
FormBuilder.prototype.mouseDidLeaveWindow = function(fBuilder){
    fBuilder.endDrag(fBuilder);
};

FormBuilder.prototype.settings =  {
    "controllerPath":'/vpanel/modules/forms/form-builder/controller.php'
    ,"mouse":[0,0]
    ,"widgets":[] // list of widgets available
    ,"thisWidgetContent": ""
    ,"thisWidgetType":false
    ,"canvas":{
        "selector":'.form-builder-canvas'
        ,"cellSelector":'.form-builder-canvas .column'
        ,"cells":[] // cells are spots fBuilder can receive widgets
    }
    ,"serialized":{} // json representing form state
    ,"widgetList":false
    ,"widgetDragOffset":[0,0] // offset into widget li when dragstart
    ,"widgetDragOffsetKludge":[-20,1] // kludge since there seems to be some constant wrong x,y I can't account for
};

// this is our generic send data / process response function
FormBuilder.prototype.chatter = function(callback, doWhat, withWhat){
    withWhat = withWhat || '';
    jQuery.ajax({
        "type":'POST'
        ,"success":callback
        ,"url":this.settings.controllerPath + '?do=' + doWhat
        ,"dataType":'json'
        ,"data":withWhat
    })
};

// error reporters
FormBuilder.prototype.fatal = function(msg){ alert(msg); };
FormBuilder.prototype.warning = function(){ if(console && console.log) console.log(msg); };

// setup widget picker
FormBuilder.prototype.widgetPickerStartup = function(data){
    var widgetList = $('.form-builder .form-builder-canvas .widget-list > .list');

    widgetList.html('');
    for(var k in data){
        var widget = data[k];
        var buttonMarkup =
        this.dragListItemMarkupTemplate
        .replace( /#NAME#/g, widget.name );

        widgetList.append(buttonMarkup);

    }

    // begin dragging support
    this.trackMouse();
};

FormBuilder.prototype.markupFromHtmlCommentedTemplate = function(id){
    return '<li class="button-bg-basic" data-widget="#NAME#">#NAME#</li>';
};
// spinup getting the widget data from the back
// use fBuilder to build UI and setup formbuilder model
FormBuilder.prototype.getWidgets = function(){
    var fBuilder = this;

    this.chatter(

        function(data){
            if(data.success === true) {
                var widgetList = data.data;
                fBuilder.settings.widgets = widgetList; // save widgets into self
                fBuilder.widgetPickerStartup(widgetList)
            }
            else {
                fBuilder.fatal('Something went wrong. Refresh the page and try again');
            }
        }

        ,'getWidgets'
    );

};



function getStyle(el, cssprop){
    if (el.currentStyle) //IE
        return el.currentStyle[cssprop]
    else if (document.defaultView && document.defaultView.getComputedStyle) //Firefox
        return document.defaultView.getComputedStyle(el, "")[cssprop]
        else //try and get inline style
            return el.style[cssprop]
}

function sanitizeAttribute(value) {
    return value.replace(/ /g,"_").replace(/[^a-zA-Z0-9%_]/g, '');
}


var toggleSettings =  function(e){

    var settingsBox = $('.form-builder:not(.helper) > .settings');
    if($('#PayAmount').val() == "" && $('#chkPayment').prop("checked")) {
        alert("Your must enter a payment amount");
        $('#PayAmount').focus();
    } else {
        settingsBox.toggle();
    }

    if(settingsBox.is(':visible')){
        settingsBox.parent().bind(
            "clickoutside"
            ,function(e){ toggleSettings(); settingsBox.parent().unbind('clickoutside'); });
    }
};