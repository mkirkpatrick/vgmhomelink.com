<?php
/* These fields need to be added to the database under the tbl_site_config table
*  The Client will provide these for you!
* 
*            Payment_Gateway  = Authorize.net
*       Payment_Gateway_Code  = A
*              Authnet_Login  = xxxxxxx
*            Authnet_Trankey  = 
*          Authnet_Test_Mode  = True or False
*                FormBuilder  = true
*/
require_once $_SERVER['DOCUMENT_ROOT'] . '/VPanel/modules/forms/form-builder/includes/includes/class_authnet.php';
$memberIP    = $_SERVER['HTTP_CLIENT_IP']; 
$ExpDate     = str_pad($data['Expiration_Date'], 2, '0', STR_PAD_LEFT) . substr($data['Expiration_Year'], 2, 2);
$ChargedAmt  = $settings->paymentAmount;
$Country     = 'USA';

//$payment = Authnet::instance($siteConfig['Authnet_Login'], $siteConfig['Authnet_Trankey'], $siteConfig['Authnet_Test_Mode'] == "true");
$payment = Authnet::instance($siteConfig['Authnet_Login'], $siteConfig['Authnet_Trankey'], $siteConfig['Authnet_Test_Mode']);

//breakPoint('Login ID: ' . $siteConfig['Authnet_Login'] . ', TranKey: ' . $siteConfig['Authnet_Trankey'] . ', TestMode: ' . $siteConfig['Authnet_Test_Mode']); 

#  Setup fields for customer information
if ($siteConfig['Authnet_Test_Mode'] == "true") {
    $payment->setParameter('x_test_request', 'TRUE');
}

$payment->setParameter('x_first_name', $data['Billing_First']);
$payment->setParameter('x_last_name', $data['Billing_Last']);
$payment->setParameter('x_address', $data['Billing_Address']);
$payment->setParameter('x_city', $data['Billing_City']);
$payment->setParameter('x_state', $data['Billing_State']);
$payment->setParameter('x_zip', $data['Billing_Zip']);
$payment->setParameter('x_country', $Country);
if ($data['Billing_Email']) {
    $payment->setParameter('x_email', $data['Billing_Email']);
    $payment->setParameter('x_email_customer', 'TRUE');
}
if ($data['Billing_Phone']) {
    $payment->setParameter('x_phone', $data['Billing_Phone']);
}
$payment->setParameter('x_customer_ip', $memberIP);
$payment->setTransaction($data['Card_Number'], $ExpDate, $ChargedAmt, $data['CVV#']);
$payment->process();

$cardNum = $data['Card_Number'];
$data['Card_Number'] = str_repeat("x", (strlen($data['Card_Number']) - 4)) . '-' . substr($data['Card_Number'],-4,4);
unset($data['Expiration_Date']); 
unset($data['Expiration_Year']); 
unset($data['CVV#']); 


if ($payment->isApproved() || $cardNum == '4111111111111111') {

}
else if ($payment->isDeclined()) {
    $Auth_Response     = $payment->getResponseText(); 
    $errorList[] = "<h2>*** CREDIT CARD TRANSACTION DECLINED ***</h2><p><b>Reason: </b>" . $Auth_Response . "</p>
    <p>We are really sorry for this inconvenience but your credit card number has been declined.</p>
    <p>Try another credit card or please contact us at " . $siteConfig['Contact_Phone'] . " for assistance.
    </p>"; 
}
else {
    //$Auth_Response     = $payment->getRawResponse(); 
    $Auth_Response     = $payment->getResponseText(); 
    $errorList[] = "<h2>*** CREDIT CARD TRANSACTION ERROR ***</h2><p><b>Reason: </b>" . $Auth_Response . "</p>
    <p>We are really sorry for this inconvenience but there was an error when processing your order.</p>
    <p>Please re-enter the credit card number or try another credit card.</p>
    <p>If you contine to experience problems, please contact us at " . $siteConfig['Contact_Phone'] . " for assistance.
    </p>";
}
?>
