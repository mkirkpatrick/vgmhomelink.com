<?php
/* These fields need to be added to the database under the tbl_site_config table
*  The Client will provide these for you!
* 
*            Payment_Gateway  = VirtualMerchant
*       Payment_Gateway_Code  = V
*            VIRTMERCHANT_ID  = xxxxxxx
*        VIRTMERCHANT_USERID  = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
*           VIRTMERCHANT_PIN  = XXXXXXXX
*                FormBuilder  = true
*/
if($settings->paymentCheckbox == "true") { 
    global $_WEBCONFIG, $form, $siteConfig;

    $memberIP    = $_SERVER['HTTP_CLIENT_IP']; 
    $ChargedAmt  = $settings->paymentAmount; 

    $ExpDate   = str_pad($data['Expiration_Date'], 2, '0', STR_PAD_LEFT) . substr($data['Expiration_Year'], 2, 2);
    $cardNum4 = substr($data['Card_Number'], 12, 16);

    $PostData  =  array('ssl_merchant_id'         => $siteConfig['VIRTMERCHANT_ID'],
        'ssl_user_id'             => $siteConfig['VIRTMERCHANT_USERID'],
        'ssl_pin'                 => $siteConfig['VIRTMERCHANT_PIN'],
        'ssl_test_mode'           => $siteConfig['VIRTMERCHANT_TESTMODE'],
        'ssl_transaction_type'    => $siteConfig['VIRTMERCHANT_CARD_TRANSTYPE'],
        'ssl_amount'              => $ChargedAmt,
        'ssl_salestax'            => '0',
        'ssl_card_number'         => $data['Card_Number'],
        'ssl_exp_date'            => $ExpDate,
        'ssl_cvv2cvc2_indicator'  => '1',
        'ssl_cvv2cvc2'            => $data['CVV#'],
        'ssl_customer_code'       => 1,
        'ssl_invoice_number'      => 10150,
        'ssl_first_name'          => substr($data['Billing_First'], 0, 20),
        'ssl_last_name'           => substr($data['Billing_Last'], 0, 30),
        'ssl_avs_address'         => substr($data['Billing_Address'], 0, 30),
        'ssl_city'                => substr($data['Billing_City'], 0, 30),
        'ssl_state'               => substr($data['Billing_State'], 0, 30),
        'ssl_avs_zip'             => substr($data['Billing_Zip'], 0, 9),
        'ssl_phone'               => $data['Billing_Phone'],
        'ssl_email'               => substr($data['Billing_Email'], 0, 100),
        'ssl_show_form'           => 'FALSE',
        'ssl_result_format'       => 'ASCII',
        'ssl_result'              => '0');

    $response = _sendVirtualMerchant($PostData);

    if (isset($response['errorCode'])) {
        // SEND E-MAIL TO FORBIN
        $mailer = new vMail();

        $mailer->addRecipient('jberning@forbin.com');
        $mailer->setSubject("{$siteConfig['Company_Name']} Event RSVP Error");
        $mailer->setMailType("html");
        $mailer->setFrom("{$siteConfig['Company_Name']}, {$siteConfig['Default_From_Email']}");
        $mailer->setReplyTo($siteConfig['Default_Reply_To_Email'], "{$siteConfig['Company_Name']} . Website"); 

        // FIND HTML FILE TO FORMAT EMAIL
        $htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], "/emailtemplates/donation-email.html");
        if(file_exists($htmlFile)) { 
            $fh = fopen($htmlFile, 'r');
            $body = fread($fh, filesize($htmlFile));
            fclose($fh); 
        } else {
            throw new exception("HTML Email Template File Not Found"); 
        }

        $_SESSION['CCmessage']   = "<div class=\"errors\"><center>\n";
        $_SESSION['CCmessage']  .= "Unable to process your credit card for the following reason: ";
        $_SESSION['CCmessage']  .= "<p>&bull;&bull;&bull; " . $response['errorName'] . " &bull;&bull;&bull;</p>";
        $_SESSION['CCmessage']  .= "<p>&bull;&bull;&bull; " . str_replace(' browser BACK button ', ' Go Back button ', $response['errorMessage']) . " &bull;&bull;&bull;</p>";
        $_SESSION['CCmessage']  .= "<p>RESPONSE DATA:<br /><pre>" . print_r($response, true) . "</pre>.</p></center>";                
        $_SESSION['CCmessage']  .= "</div>\n";

        $errorList[]   = "<div class=\"errors\"><center>\n
        Unable to process your credit card for the following reason: 
        <p>&bull;&bull;&bull; " . $response[ssl_result_message] . " &bull;&bull;&bull;</p></center></div>\n";

        // SET THE MAIL MERGE VARIABLES    
        $mergeFields = array(); 
        $mergeFields["**SITENAME**"] = $siteConfig['Company_Name']; 
        $mergeFields["**FORMNAME**"] = "RSVP Form"; 
        $mergeFields["**TABLEDATA**"] = $_SESSION['CCmessage'];

        // SET THE BODY OF THE EMAIL    
        $body = strtr($body, $mergeFields);

        // SET THE MESSAGE
        $mailer->setMessage($body);  

        $mailer->sendMail();                  

    } else if (isset($response['ssl_result_message'])) {
        // Check if OK Transaction
        if ($response['ssl_result'] == 0 && (strcasecmp($response['ssl_result_message'], "APPROVAL") == 0 || strcasecmp($response['ssl_result_message'], "APPROVED") == 0)) {

        } 
        else {
            // SEND E-MAIL TO FORBIN
            $mailer = new vMail();

            $mailer->addRecipient('jberning@forbin.com');
            $mailer->setSubject("{$siteConfig['Company_Name']} RSVP Error");
            $mailer->setMailType("html");
            $mailer->setFrom("{$siteConfig['Company_Name']}, {$siteConfig['Default_From_Email']}");
            $mailer->setReplyTo($siteConfig['Default_Reply_To_Email'], "{$siteConfig['Company_Name']} . Website"); 

            // FIND HTML FILE TO FORMAT EMAIL
            $htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], "/emailtemplates/donation-email.html");
            if(file_exists($htmlFile)) { 
                $fh = fopen($htmlFile, 'r');
                $body = fread($fh, filesize($htmlFile));
                fclose($fh); 
            } else {
                throw new exception("HTML Email Template File Not Found"); 
            }
            // SET THE VARIABLE FOR THE MAIL BODY                    
            $_SESSION['CCmessage']   = "<div class=\"errors\"><center>\n";
            $_SESSION['CCmessage']  .= "<h2>** TRANSACTION DECLINED **</h2>";
            $_SESSION['CCmessage']  .= "<p>Unable to process your credit card for the following reason: ";
            $_SESSION['CCmessage']  .= "<b>" . $response['ssl_result_message'] . ".</b></p>";
            $_SESSION['CCmessage']  .= "</center></div>\n";


            $errorList[]   = "<div class=\"errors\"><center>\n
            Unable to process your credit card for the following reason: 
            <p>&bull;&bull;&bull; " . $response['ssl_result_message'] . " &bull;&bull;&bull;</p></center></div>\n";

            // SET THE MAIL MERGE VARIABLES    
            $mergeFields = array(); 
            $mergeFields["**SITENAME**"] = $siteConfig['Company_Name']; 
            $mergeFields["**FORMNAME**"] = "RSVP Form"; 
            $mergeFields["**TABLEDATA**"] = $_SESSION['CCmessage'];

            // SET THE BODY OF THE EMAIL    
            $body = strtr($body, $mergeFields);

            // SET THE MESSAGE
            $mailer->setMessage($body);  

            $mailer->sendMail();                    
        }
    } 
    else {             
        // SEND E-MAIL TO FORBIN
        $mailer = new vMail();

        $mailer->addRecipient('jberning@forbin.com');
        $mailer->setSubject("{$siteConfig['Company_Name']} Donation Error");
        $mailer->setMailType("html");
        $mailer->setFrom("{$siteConfig['Company_Name']}, {$siteConfig['Default_From_Email']}");
        $mailer->setReplyTo($siteConfig['Default_Reply_To_Email'], "{$siteConfig['Company_Name']} . Website"); 

        // FIND HTML FILE TO FORMAT EMAIL
        $htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], "/emailtemplates/donation-email.html");
        if(file_exists($htmlFile)) { 
            $fh = fopen($htmlFile, 'r');
            $body = fread($fh, filesize($htmlFile));
            fclose($fh); 
        } else {
            throw new exception("HTML Email Template File Not Found"); 
        }


        $message   = "<div class=\"errors\"><center>\n";
        $message  .= "Unable to process your credit card for the following reason: ";
        $message  .= "<p>&bull;&bull;&bull; " . $response['ssl_result_message'] . " &bull;&bull;&bull;</p>";               
        $message  .= "</div>\n";

        $errorList[]   = "<div class=\"errors\"><center>\n
        Unable to process your credit card for the following reason: 
        <p>&bull;&bull;&bull; " . $response['ssl_result_message'] . " &bull;&bull;&bull;</p>
        <p>RESPONSE DATA:<br /><pre>" . print_r($response, true) . "</pre>.</p></center></div>\n";

        // SET THE MAIL MERGE VARIABLES    
        $mergeFields = array(); 
        $mergeFields["**SITENAME**"] = $siteConfig['Company_Name']; 
        $mergeFields["**FORMNAME**"] = "RSVP Form"; 
        $mergeFields["**TABLEDATA**"] = $message;

        // SET THE BODY OF THE EMAIL    
        $body = strtr($body, $mergeFields);

        // SET THE MESSAGE
        $mailer->setMessage($body);  

        $mailer->sendMail();                 
    }
}    

//------------------------------
function _sendVirtualMerchant($PostData) {
    //------------------------------
    $url = 'https://www.myvirtualmerchant.com/VirtualMerchant/process.do';
    //$url = 'https://demo.myvirtualmerchant.com/VirtualMerchantDemo/process.do';
    $postdata = ""; 
    while (list($key, $value) = each($PostData)) {
        $postdata .= $key .'='. urlencode(str_replace(',', '', $value)) .'&';
    }

    $postdata = substr($postdata, 0, -1);

    $refer = $_SERVER["HTTP_REFERER"];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_REFERER, $refer);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_NOPROGRESS, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,0);

    $authorize = curl_exec($ch);

    if (empty($authorize)) {
        parent::ThrowException("<p>Unable to process payment. Please try again in a few seconds</p>");
    }

    if ($error = curl_error($ch)) {
        parent::ThrowException($error);
    }
    curl_close($ch);

    $authorize = trim($authorize);
    $rawResponse = $authorize;
    $authorize = str_replace("\r", "", $authorize);
    $authorize = str_replace("\n", "&", $authorize);

    // Put the results into an associative array.
    parse_str($authorize, $response);

    return $response;
}