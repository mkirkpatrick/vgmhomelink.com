<?php
/* These fields need to be added to the database under the tbl_site_config table
*  The Client will provide these for you!
* 
*            Payment_Gateway  = Element
*       Payment_Gateway_Code  = E
*          Element_AccountID  = xxxxxxx
*       Element_AccountToken  = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
*         Element_AcceptorID  = XXXXXXXX
*                FormBuilder  = true
*/
if($settings->paymentCheckbox == "true") { 

    $memberIP    = $_SERVER['HTTP_CLIENT_IP']; 
    //$ExpDate    = str_pad($data['Expiration_Date'], 2, '0', STR_PAD_LEFT) . substr($data['Expiration_Year'], 2, 2);
    $ChargedAmt    = $settings->paymentAmount; 
    $x  = '
    <CreditCardSale xmlns="https://transaction.elementexpress.com">
    <Credentials>
    <AccountID>' . $siteConfig['Element_AccountID'] . '</AccountID>
    <AccountToken>' . $siteConfig['Element_AccountToken'] . '</AccountToken>
    <AcceptorID>' . $siteConfig['Element_AcceptorID'] . '</AcceptorID>
    </Credentials>
    <Application>
    <ApplicationID>1761</ApplicationID>
    <ApplicationName>Forbin Payment Provider</ApplicationName>
    <ApplicationVersion>1.0</ApplicationVersion>
    </Application>
    <Terminal>
    <TerminalID>01</TerminalID>
    <TerminalType>2</TerminalType>
    <CardPresentCode>3</CardPresentCode>
    <CardholderPresentCode>7</CardholderPresentCode>
    <CardInputCode>4</CardInputCode>
    <CVVPresenceCode>0</CVVPresenceCode>
    <TerminalCapabilityCode>5</TerminalCapabilityCode>
    <TerminalEnvironmentCode>6</TerminalEnvironmentCode>
    <MotoECICode>7</MotoECICode>
    <CVVResponseType>0</CVVResponseType>
    <ConsentCode>3</ConsentCode>
    </Terminal>
    <Card>
    <CardNumber>' . $data['Card_Number'] . '</CardNumber>
    <ExpirationMonth>' . str_pad($data['Card_Month'], 2, '0', STR_PAD_LEFT) . '</ExpirationMonth>
    <ExpirationYear>' . substr($data['Card_Year'], 2, 2) . '</ExpirationYear>
    <CardholderName>'.$data['Cardholder_Name'] .'</CardholderName>
    <CVV>'.$data['CVV_Number'] .'</CVV>
    </Card>
    <Transaction>
    <TransactionAmount>' . $ChargedAmt. '</TransactionAmount>
    <DuplicateCheckDisableFlag>1</DuplicateCheckDisableFlag>
    <DuplicateOverrideFlag>0</DuplicateOverrideFlag>
    <RecurringFlag>0</RecurringFlag>
    <ReferenceNumber>1234567890</ReferenceNumber>
    <TicketNumber>1234567890</TicketNumber>
    <MarketCode>3</MarketCode>
    </Transaction>
    <Address>
    <BillingName>'. $data['Billing_First'] . ' ' . $data['Billing_Last'] . '</BillingName>
    <BillingAddress1>'. $data['Billing_Address'] . '</BillingAddress1>
    <BillingCity>'. $data['Billing_City']. '</BillingCity>
    <BillingState>'. $data['Billing_State'] . '</BillingState>
    <BillingZipcode>'. $data['Billing_Zip'] . '</BillingZipcode>
    <BillingEmail>'. $data['Billing_Email'] . '</BillingEmail>
    <BillingPhone>'. $data['Billing_Phone'] . '</BillingPhone>
    </Address>
    </CreditCardSale>';              
    $header = array(
        'POST /express.asmx HTTP/1.1',
        'Content-Type: text/xml;charset=UTF-8',
        'SOAPAction: "https://transaction.elementexpress.com/CreditCardSale"',
        'User-Agent: Forbin Payment Agent 1.0',
        'Host: transaction.elementexpress.com',
        'Content-Length: ' . strlen($x)
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://transaction.elementexpress.com/" );
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 900);
    curl_setopt($ch, CURLOPT_TIMEOUT,        30);
    curl_setopt($ch, CURLOPT_FAILONERROR,    true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_POST,           true );
    curl_setopt($ch, CURLOPT_POSTFIELDS,     $x);
    curl_setopt($ch, CURLOPT_HTTPHEADER,     $header);
    curl_setopt($ch, CURLOPT_HEADER, false);

    if(($results = curl_exec($ch)) === false) {
        $err = ' Curl error: ' . curl_error($ch);
        print $err;
    }

    curl_close($ch);            
    $xml = simplexml_load_string($results); 
    $string = $results;
    $string = str_replace('<S:', '<', $string);
    $string = str_replace('</S:', '</', $string);
    $string = str_replace('<ns2:', '<', $string);
    $string = str_replace('</ns2:', '</', $string);
    $response = Database::quote_smart($string); 
    $xml = simplexml_load_string($string);
    $rspCode = $xml->Response->ExpressResponseCode; 
    $message = $xml->Response->ExpressResponseMessage; 
    $gwyTranId = $xml->Response->Transaction->TransactionID; 
    $data['Card_Number'] = str_repeat("x", (strlen($data['Card_Number']) - 4)) . '-' . substr($data['Card_Number'],-4,4);
    $data['TransactionID'] = (int) $xml->Response->Transaction->TransactionID; 
    $data['Transaction_Link'] = "<a href='https://www.coremanagementsystem.com/virtualterminal/view/TranDetails.aspx?tranid=" . $xml->Response->Transaction->TransactionID . "'>View Transaction</a>";  
    //$data['Response_Message'] = (string) $xml->Response->ExpressResponseMessage; 
    //$data['Response_Code'] = (int) $xml->Response->ExpressResponseCode; 

    unset($data['Card_Month']); 
    unset($data['Card_Year']); 
    unset($data['CVV_Number']); 

    if($rspCode == 20) {
        $errorList[] = "<h2>*** CREDIT CARD TRANSACTION DECLINED ***</h2>
        <p>We are really sorry for this inconvenience but your credit card number is declined.</p>
        <p>Try another credit card or please contact us at " . $siteConfig['Contact_Phone'] . " for assistance.
        </p>"; 
    } else if($rspCode != 0) {
        $errorList[] = "
        <h2>*** CREDIT CARD TRANSACTION ERROR ***</h2><P><b>Reason: </b>" . (string) $xml->Response->ExpressResponseMessage . "</p>
        <p>We are really sorry for this inconvenience but there was an error when processing your order.</p>
        <p>Try another credit card or please contact us at " . $siteConfig['Contact_Phone'] . " for assistance.
        </p>"; 
    }
}

?>