<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_vMail.php"); 

class FormBuilder{
    // path relative to site base
    private static $WIDGET_PATH       = '/vpanel/modules/forms/form-builder/widgets';
    private static $WIDGET_CLASS_PATH = '/vpanel/modules/forms/form-builder/widgets';
    private static $FIELD_LIMIT = 200;
    private static $SETTINGS_FIELD_PREFIX = 'SETTINGS-';
    // return json array of widget markup / js
    public static function getWidgets(){  
        return self::widgetPaths();
    }

    // return json array of widget markup / js
    public static function saveForm(){  
        $key = $_REQUEST['key'];
        $value = $_REQUEST['value'];
        Security::_addAuditEvent("Online Form Modified", "Success", "Modified -- " . $key, $_SESSION['user_username']);
        return Asset::putForm(array('k'=>$key, 'v'=>$value, 'settings'=> array())); 
    }

    private static function widgetPaths(){
        //$aMarkups = self::widgetMarkupPathList();

        $sql = "SELECT `fw_id`, `fw_name`, `fw_type`,`fw_html`,`fw_icon_class`
        FROM `tbl_form_widget` 
        WHERE `fw_status` = 'Active'
		ORDER BY `fw_sort_order` 
        LIMIT 100";
        $record    = Database::Execute($sql);
        if ($record->Count() > 0) {
            while ($record->MoveNext()) { 
                $aWidgets[$record->fw_name] = array('name'=> $record->fw_name, 'markup'=>$record->fw_html, 'class'=>$record->fw_icon_class);   
            } 
        } 
        return $aWidgets;
    }

    private static function basePath(){ return $_SERVER['DOCUMENT_ROOT']; }
    private static function widgetMarkupPathList(){ $base = self::basePath(); return self::filesAt($base.self::$WIDGET_PATH, 'html'); }


    private static function filesAt($path, $withExtension = false){     
        $aPaths = array();
        if($h = opendir($path)){

            while(false !== ($file = readdir($h))){  

                $fullPath = $path . '/'. $file;
                $pathInfo = pathinfo($fullPath, PATHINFO_EXTENSION);

                if($file == '.' || $file == '..') continue; // skip up dir and current dir shortcuts

                if($withExtension !== false && $pathInfo  ==  $withExtension){
                    $name =basename($file,'.'.$pathInfo);
                    $aPaths[$name] = array(
                        "name"=>$name
                        ,"file"=>$file
                        ,"extension"=>$pathInfo
                        ,"content"=>file_get_contents($fullPath)
                    ); 
                }else if($withExtension === false){ $aPaths[] = $fullPath; }    
            }
            return $aPaths;
        } else {
            return false; 
        }
    }


    // is this name a settings name
    public static function isSetting($k){
        if(strpos($k, self::$SETTINGS_FIELD_PREFIX) === 0){ return true; }   
        return false;
    }

    // gets attributes from dom node or returns false
    public static function attr($node, $k){ 
        return gettype($node->attributes->getNamedItem($k)) != 'NULL' ? $node->attributes->getNamedItem($k)->value: false;
    }

    public static function getLastFormEdited(){
        $lastAsset = Asset::getLastEditedByType(Asset::$ASSET_TYPE_FORM);
        return 
        array( 'key'=>$lastAsset->{Asset::$TAG_COLUMN}
            ,'name'=>$lastAsset->{Asset::$NAME_COLUMN}
            ,'settings'=>$lastAsset->{Asset::$DATA_COLUMN}
            ,'value'=>$lastAsset->{Asset::$VALUE_COLUMN}
        );
    }

    public static function cleanKey($string){
        $string = str_replace(array("\r", "\r\n", "\n", "[]", "\n\n", "&#10;"), '', $string); 
        return htmlentities($string, ENT_QUOTES, 'utf-8');
    }
}
?>