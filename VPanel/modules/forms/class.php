<?php
class process extends Database { 

    private $record;
    private $table;
    public $params;

    //------------------------------
    public function __construct($table) {
        //------------------------------
        parent::__construct();
        $this->table		= $table;
    }

    //------------------------------
    public function _modify() {
        //------------------------------
        global $form;

        $iNum = (int)$_POST['hidId'];
        $formId = (int)$_POST['hidFormId'];
        $this->record = Repository::loadByID($this->table, $iNum);
        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=results-answers&id=" . $iNum;
        } else {
            // No Errors
            $this->record->update();
            $_SESSION['processed'] = 'updated';
            $this->params = "view=results&id=" . $formId;
        }
    }

    //------------------------------
    public function _delete() {
        //------------------------------
        global $_WEBCONFIG; 

        $iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
        $this->record = Repository::getByID($this->table, $iNum);
        if(isset($this->record)) { 
            $settings = json_decode($this->record->as_data); 
            $cfId = (int) $settings->cf_id; 
            Database::ExecuteRaw("DELETE FROM `tbl_form_custom` WHERE `cf_id` = '$cfId';"); 
        }
        Database::ExecuteRaw("DELETE FROM {$this->table} WHERE `as_id` = $iNum");

        $_SESSION['processed'] = 'deleted';
        $this->params = "view=list";
    }

    //------------------------------
    private function _verify() {
        //------------------------------
        global $form;

        $fieldName	= "mtxAdminComments";
        $fieldValue	= strip_tags($_POST[$fieldName]);
        $this->record->fs_admin_comments = $fieldValue;

        $fieldName	= "optStatus";
        $fieldValue	= (int) $_POST[$fieldName];
        $this->record->fs_reviewed = $fieldValue;
    }
};
?>