<?php
class Asset{

    public static $ASSET_TYPE_FORM = 5;
    public static $TABLE = 'tbl_cms_asset';
    public static $TAG_COLUMN = 'as_key';
    public static $NAME_COLUMN = 'as_name';
    public static $VALUE_COLUMN = 'as_value';
    public static $DATA_COLUMN = 'as_data';
    public static $TYPE_COLUMN = 'at_id';
    public static $TAG_DELIMITER = '**';
    public static $DATEFMT = 'Y-m-d G:i:s';

    public static function putForm($args){
        return self::put(array('k'=>$args['k'],'v'=>$args['v'],'type'=>self::$ASSET_TYPE_FORM,'settings'=>$args['settings']));
    }
    public static function put($args){
        $k = $args['k'];
        $v = $args['v'];

        $type = $args['type'];
        $settings = $args['settings'];     // UPDATE FORM
        if( false !== ( $entity = self::exists($args)) ){
            $newTag = self::keyToTag(array('k'=>$k));
            $form = '<div class="userMessage"><?= displayUserMessage() ?></div>' . str_replace("%%PHP_RC_CAPTCHA%%", "<?= loadRC() ?>", $v)  . PHP_EOL . "<?php include_once(\$_SERVER['DOCUMENT_ROOT'] . '/modules/formbuilder/library/include.php') ?>";
            $entity->as_name = $k;
            $entity->{self::$TAG_COLUMN} = $newTag;
            $entity->as_value  = $form;
            $entity->at_id = $type;
            $entity->as_last_update = date("Y-m-d G:i:s");
            Repository::Save($entity);
        }
        else{ // NEW FORM
            $entity = new Entity(self::$TABLE);
            $form = '<div class="userMessage"><?= displayUserMessage() ?></div>' . str_replace("%%PHP_RC_CAPTCHA%%", "<?= loadRC() ?>", $v)  . PHP_EOL . "<?php include_once(\$_SERVER['DOCUMENT_ROOT'] . '/modules/formbuilder/library/include.php') ?>";
            $entity->as_name = $k;
            $newTag = self::keyToTag(array('k'=>$k));

            $sql = "INSERT INTO `tbl_form_custom` (
            `cf_name`,
            `cf_primary_field`,
            `cf_secondary_field`,
            `cf_teriary_field`,
            `cf_required_fields`,
            `cf_recipients`,
            `cf_date_added`,
            `cf_last_updated`
            )
            VALUES
            (
            '" . Database::quote_smart($entity->as_name) . "',
            'Name',
            'Email',
            'Phone',
            'Name',
            'homelinkinfo@vgm.com',
            '" . date("Y-m-d G:i:s") . "',
            '" . date("Y-m-d G:i:s") . "'
            );";
            Database::ExecuteRaw($sql);
            $settings["cf_id"] = Database::getInsertID();
            $entity->{self::$TAG_COLUMN} = $newTag;
            $entity->as_value  = $form;
            $entity->at_id = $type;
            $entity->as_data = json_encode($settings);
            Repository::Save($entity);
        }

        return true;
    }

    public static function randNewTag(){
        $length = 19;
        $chars = "abcdefghijklmnopqrstuvwxyzABC_DEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $size = strlen( $chars );
        $str = '';
        for( $i = 0; $i < $length; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str;

    }

    public static function val($asset){
        return $asset->{self::$VALUE_COLUMN};
    }



    public static function data($asset){
        return $asset->{self::$DATA_COLUMN};
    }

    public static function pair($asset){
        return array($asset->{self::$TAG_COLUMN},$asset->{self::$VALUE_COLUMN});
    }

    public static function generateKey($name, $count = 1) {
        $name =  $count > 1 ? $name . $count : $name;
        $args['k'] = $name;
        $key = self::keyToTag($args);
        if(Repository::ValueExists(self::$TABLE, self::$TAG_COLUMN, $key)) {
            $count++;
            $key = self::generateKey($name, $count);
        }
        return $key;
    }

    public static function keyToTag($args){
        return self::$TAG_DELIMITER
        .  strtoupper(str_replace(' ', '-', removeNonAlphaNumeric($args['k'])))
        . self::$TAG_DELIMITER;
    }


    public static function getLastEditedByType($type){  // ALIAS OF EXISTS
        $result = Database::Execute('select * from '.self::$TABLE.' where '.self::$TYPE_COLUMN.' = '.$type.' order by as_last_update desc limit 1');
        if($result->Count() ==1){
            $result->MoveNext();
            return $result;
        }
        return false;
    }



    public static function getById($id){
        $parms = array('table'=>self::$TABLE, 'key'=>'as_id', 'value'=>$id);

        return Repository::getByValue($parms);
    }

    public static function getByKey($key){  // ALIAS OF EXISTS
        $parms = array('table'=>self::$TABLE, 'key'=>self::$TAG_COLUMN, 'value'=>$key);

        return Repository::getByValue($parms);
    }

    public static function exists($args){
        $k=$args['k'];

        $parms = array('table'=>self::$TABLE, 'key'=>self::$TAG_COLUMN, 'value'=>self::keyToTag(array('k'=>$k)));

        return Repository::getByValue($parms);
    }
}
?>