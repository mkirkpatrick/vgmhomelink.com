// JavaScript Document
$(document).ready(function () {
    $("#grid").kendoGrid({
        dataSource: { pageSize: 50 },
        pageable: { refresh: false, pageSizes: [10, 25, 50, 100] },
        sortable: { mode: "multiple", allowUnsort: true },
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",
                    doesnotcontain: "Does Not Contain",
                    startswith: "Starts With",
                    endswith: "Ends With"
                }
            }
        },
        groupable: false,
        columnMenu: false,
        scrollable: false,
        resizable: true,
        reorderable: true
    });
});



// Button Functions
function add() {
    window.location.href = 'index.php?view=add';
}

function modify(id) {
    window.location.href = 'index.php?view=modify&id=' + id;
}

function remove(id) {
    if (confirm('Are You Sure?')) {
        window.location.href = 'process.php?actionRun=deleteForm&id=' + id;
    }
}
