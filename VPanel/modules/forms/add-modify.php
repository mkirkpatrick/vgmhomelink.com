<?
if (!isset($siteConfig)) die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

// Set Default Setting Values
$hidId				= null;
$txtPrimary			= 'Name';
$txtSecondary		= 'Email';
$txtTeriary         = 'Phone';
$txtReplyTo			= 'Email';
$txtRequiredFields  = 'name|phone|email|comments';
$txtFieldLimit 		= 25;
$txtReceipients		= 'forbinprogrammers@forbin.com';
$txtDelimitor		= '<br />';
$mtxSuccessMessage	= '<p>Your contact request has been submitted successfully. A representative will be contacting you soon. </p>';

if ($form->getNumErrors() > 0) {
    $hidId				= $form->value("hidId");
    $txtName			= $form->value("txtName");
    $txtDesc			= $form->value("txtDesc");
    $optStatus			= $form->value("optStatus");
} else if ($iNum > 0) {
    $asset = Repository::GetById($_WEBCONFIG['MODULE_TABLE'], $iNum);

    if (is_null($asset)) {
        throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
    }
    $settings		    = json_decode(stripslashes($asset->as_data));
    $hidId				= $asset->as_id;
    $txtName			= htmlspecialchars($asset->as_name);
    $txtDesc			= $asset->as_value;
    $optStatus			= $asset->as_status;
    if(isset($settings->cf_id)) {
        $formConfig = Repository::GetById('tbl_form_custom', $settings->cf_id);
        if (is_null($formConfig)) {
            throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
        }

        $hidId				= $formConfig->cf_id;
        $txtPrimary			= $formConfig->cf_primary_field;
        $txtSecondary		= $formConfig->cf_secondary_field;
        $txtTeriary         = $formConfig->cf_teriary_field;
        $txtReplyTo			= $formConfig->cf_reply_to_field;
        $mtxSuccessMessage	= $formConfig->cf_success_message;
        $txtRequiredFields	= $formConfig->cf_required_fields;
        $txtDelimitor		= $formConfig->cf_delimitor;
        $txtFieldLimit		= $formConfig->cf_field_limit;
        $txtReceipients		= $formConfig->cf_recipients;
    }



    $quickAsset = array(
        "key" => $asset->as_key,
        "name" => $asset->as_name,
        "settings" => json_decode($asset->as_data),
        "value" => $asset->as_value
    );

} else {
    $hidId				= '';
    $txtName			= '';
    $txtDesc 			= '';
    $optStatus			= 'Active';


    $quickAsset  = array(
        'key'=>'**UNTITLED-FORM**',
        'name'=>'Untitled Form',
        'settings'=> array(),
        'value'=>''
    );
}
?>
<link rel="stylesheet" href="/VPanel/modules/forms/form-builder/form-builder.css?v=2">

<div class="form-builder clearfix">
    <ul class="header">
        <li></li>
        <li class="titlebar">Untitled Forms</li>
        <li></li>
		<?php if(isset($settings->cf_id)) { ?>
        <li class="fb-settings-button"><a class="slap" href="/VPanel/forms/index.php?view=modify&id=<?= $settings->cf_id ?>"></a></li>
		<?php } ?>
    </ul>

    <ul class="form-builder-canvas clearfix">
        <li class="widget-list"><ul class="list"></ul></li>
        <li class="settings-list"><ul class="list"></ul></li>
        <li class="cell clearfix">
        </li>
    </ul>

    <div class="subcontent last right settings">

        <table class="grid-display" style="width: 100%; border: 0; border-collapse: collapse;" cellpadding="5">
            <tr>
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtPrimary" class="required"><b>Primary Field</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                	<input type="text" name="txtPrimary" id="txtPrimary" value="<?= $txtPrimary; ?>" size="40" maxlength="255" required="required" />

                	<div class="help-tooltip-wrapper">
						<img src="/vpanel/images/icon-help.png" width="18" height="18" alt="Info" class="block">
						<div class="help-tooltip">This is where the user will enter their name.</div>
					</div>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtSecondary"><b>Secondary Field</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                	<input type="text" name="txtSecondary" id="txtSecondary" value="<?= $txtSecondary; ?>" size="40" maxlength="255" />

                	<div class="help-tooltip-wrapper">
						<img src="/vpanel/images/icon-help.png" width="18" height="18" alt="Info" class="block">
						<div class="help-tooltip">This is where the user will enter their email address.</div>
					</div>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtTeriary"><b>Tertiary Field</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                	<input type="text" name="txtTeriary" id="txtTeriary" value="<?= $txtTeriary; ?>" size="40" maxlength="255" />
                	<div class="help-tooltip-wrapper">
						<img src="/vpanel/images/icon-help.png" width="18" height="18" alt="Info" class="block">
						<div class="help-tooltip">This is where the user will enter their phone number.</div>
					</div>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtReplyTo"><b>Reply To Field</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                	<input type="text" name="txtReplyTo" id="txtReplyTo" value="<?= $txtReplyTo; ?>" size="40" maxlength="255" />
                	<div class="help-tooltip-wrapper">
						<img src="/vpanel/images/icon-help.png" width="18" height="18" alt="Info" class="block">
						<div class="help-tooltip">This is where the user indicates which email should receive the reply message.</div>
					</div>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtRequiredFields"><b>Required Fields</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                	<input type="text" name="txtRequiredFields" id="txtRequiredFields" value="<?= $txtRequiredFields; ?>" size="40" maxlength="255" />
                	<div class="help-tooltip-wrapper">
						<img src="/vpanel/images/icon-help.png" width="18" height="18" alt="Info" class="block">
						<div class="help-tooltip">This is where the person creating the form will designate which fields of the form will be required for the user to complete.</div>
					</div>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtDelimitor" class="required"><b>Form Delimitor</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                	<input type="text" name="txtDelimitor" id="txtDelimitor" value="<?= $txtDelimitor; ?>" size="40" maxlength="255" required="required" />
                	<div class="help-tooltip-wrapper">
						<img src="/vpanel/images/icon-help.png" width="18" height="18" alt="Info" class="block">
						<div class="help-tooltip">This represents the divider between the form fields.</div>
					</div>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtFieldLimit" class="required"><b>Form Field Limit</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                	<input type="number" name="txtFieldLimit" id="txtFieldLimit" value="<?= $txtFieldLimit; ?>" size="5" maxlength="255" required="required" />
                	<div class="help-tooltip-wrapper">
						<img src="/vpanel/images/icon-help.png" width="18" height="18" alt="Info" class="block">
						<div class="help-tooltip">This represents the limit on the number of fields allowed per form.</div>
					</div>
                </td>
            </tr>

            <tr>
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtReceipients" class="required"><b>Recipients</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                   	<input type="text" name="txtReceipients" id="txtReceipients" value="<?= $txtReceipients; ?>" size="56" maxlength="255" required="required" />
                   	<div class="help-tooltip-wrapper">
						<img src="/vpanel/images/icon-help.png" width="18" height="18" alt="Info" class="block">
						<div class="help-tooltip">This is where the person creating the form enters the email addresses of who will receive notification emails one the form has been submitted.</div>
					</div>

					<div class="clearfloat"></div>

                    <label valign="center">NOTE: For each additional email separate by a semi colon (;)</label>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; vertical-align: top; width: 125px">
                    <label for="mtxSuccessMessage" class="required"><b>Success Message</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                    <textarea name="mtxSuccessMessage" id="mtxSuccessMessage" style="vertical-align:middle;"><?= $mtxSuccessMessage; ?></textarea>
                    <div class="help-tooltip-wrapper">
						<img src="/vpanel/images/icon-help.png" width="18" height="18" alt="Info" class="block">
						<div class="help-tooltip">This is the message the user will receive after they have submitted their form.</div>
					</div>
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="buttons clearfix" style="margin-left: 20px">
    <a class="button blue floatLeft" href="javascript:history.back()">Back</a>
</div>

<div class="loading">Auto Saving</div>
<script>
    var popLoading = function(){
        $('.loading').stop();
        $('.loading').css({"opacity":0,}).animate({'opacity':1},{"duration":350,"complete":function(){

            $(this).delay(1100).animate({"opacity":0},{"duration":350});
        }});
    };
    var paymentFields = '<label style="float: left; width: 50%"> <span>Billing First</span> <input type="text" name="Billing First" placeholder="" data-required="true" /> </label> <label style="float: left; width: 50%"> <span>Billing Last</span> <input type="text" name="Billing Last" placeholder="" data-required="true" /> </label> <label> <span>Billing Address</span> <input type="text" name="Billing Address" placeholder="" data-required="true" /> </label> <label> <span>Billing City</span> <input type="text" name="Billing City" placeholder="" data-required="true" /> </label> <label> <span>Billing State</span> <select style="width: 150px" name="Billing State" data-required="true"> <option value="" selected="selected">Select a State</option> <option value="AL">Alabama</option> <option value="AK">Alaska</option> <option value="AZ">Arizona</option> <option value="AR">Arkansas</option> <option value="CA">California</option> <option value="CO">Colorado</option> <option value="CT">Connecticut</option> <option value="DE">Delaware</option> <option value="DC">District Of Columbia</option> <option value="FL">Florida</option> <option value="GA">Georgia</option> <option value="HI">Hawaii</option> <option value="ID">Idaho</option> <option value="IL">Illinois</option> <option value="IN">Indiana</option> <option value="IA">Iowa</option> <option value="KS">Kansas</option> <option value="KY">Kentucky</option> <option value="LA">Louisiana</option> <option value="ME">Maine</option> <option value="MD">Maryland</option> <option value="MA">Massachusetts</option> <option value="MI">Michigan</option> <option value="MN">Minnesota</option> <option value="MS">Mississippi</option> <option value="MO">Missouri</option> <option value="MT">Montana</option> <option value="NE">Nebraska</option> <option value="NV">Nevada</option> <option value="NH">New Hampshire</option> <option value="NJ">New Jersey</option> <option value="NM">New Mexico</option> <option value="NY">New York</option> <option value="NC">North Carolina</option> <option value="ND">North Dakota</option> <option value="OH">Ohio</option> <option value="OK">Oklahoma</option> <option value="OR">Oregon</option> <option value="PA">Pennsylvania</option> <option value="RI">Rhode Island</option> <option value="SC">South Carolina</option> <option value="SD">South Dakota</option> <option value="TN">Tennessee</option> <option value="TX">Texas</option> <option value="UT">Utah</option> <option value="VT">Vermont</option> <option value="VA">Virginia</option> <option value="WA">Washington</option> <option value="WV">West Virginia</option> <option value="WI">Wisconsin</option> <option value="WY">Wyoming</option> </select> </label> <label> <span>Billing Zip</span> <input style="width: 100px" type="text" name="Billing Zip" placeholder="" data-inputmask="zip" data-validate="zip" data-required="true" /> </label> <label> <span>Billing Phone</span> <input type="text" name="Billing Phone" placeholder="" data-inputmask="phone" data-validate="phone" data-required="true" /> </label> <label> <span>Billing Email</span> <input type="email" name="Billing Email" placeholder="" data-validate="email" data-required="true"/> </label> <label> <span>Select Payment Method</span> <select title="Payment Method" required="required" class="text" id="PaymentMethod" name="Payment Method" data-required="true"> <option value="">--Please Select--</option> <option value="Discover">Discover</option> <option value="Amex">American Express</option> <option value="MasterCard">MasterCard</option> <option value="Visa">Visa</option> </select> </label> <label> <span>Cardholder Name</span> <input type="text" placeholder="Name as it appears on your card" title="Cardholder Name" maxlength="150" value="" autocomplete="off" id="CardholderName" name="Cardholder Name" data-required="true"> </label> <label> <span>Card Number</span> <input type="text" placeholder="#### #### #### ####" title="Card Number" maxlength="16" value="" autocomplete="off" id="CardNumber" name="Card Number" data-required="true"> </label> <label> <span>Expiration Date</span> <select style="width: 200px" title="Expiration Month" id="CardMonth" name="Expiration Date" data-required="true"> <option value="">--Month--</option> <option value="01">January - (01)</option> <option value="02">February - (02)</option> <option value="03">March - (03)</option> <option value="04">April - (04)</option> <option value="05">May - (05)</option> <option value="06">June - (06)</option> <option value="07">July - (07)</option> <option value="08">August - (08)</option> <option value="09">September - (09)</option> <option value="10">October - (10)</option> <option value="11">November - (11)</option> <option value="12">December - (12)</option> </select> </label> <label> <span style="display:none;">Expiration Year</span><select style="width: 100px" title="Expiration Year" id="CardYear" name="Expiration Year" data-required="true"> <option selected="selected" value="2300">--Year--</option> <option value="2013">2013</option> <option value="2014">2014</option> <option value="2015">2015</option> <option value="2016">2016</option> <option value="2017">2017</option> <option value="2018">2018</option> <option value="2019">2019</option> <option value="2020">2020</option> <option value="2021">2021</option> <option value="2022">2022</option> </select> </label> <label> <span>CVV#</span> <input type="text" title="CVV Number" maxlength="5" data-required="true" value="" id="CardCode" name="CVV#"> </label>';
</script>

<script src="/VPanel/jquery/jquery.stickykit.min.js"></script>
<script src="/VPanel/modules/forms/form-builder/FormBuilder.js"></script>
<script>
	// Sidebar Follow
	$('.widget-list').stick_in_parent();

    // force close sidebar
    // setup select select pages

    var quickForm = <?= json_encode($quickAsset); ?>;

    for(var i in pageBuilderPageList.pages){
        var url = i;
        var name = pageBuilderPageList.pages[i][1];
        $('#fbSettingsSuccessPageSelect').append(
            '<option value="'+i+'">'+name+'</option>'
        );
    }

    var thisFormBuilder = new FormBuilder();

    $('#fbSettingsSuccessPageSelect').change(function(){thisFormBuilder.save();});
    // leave window event support
    var addEvent = function(obj, evt, fn) {
        if (obj.addEventListener) {
            obj.addEventListener(evt, fn, false);
        }
        else if (obj.attachEvent) {
            obj.attachEvent("on" + evt, fn);
        }
    };

    addEvent(document, "mouseout", function(e) {
        e = e ? e : window.event;
        var from = e.relatedTarget || e.toElement;
        if (!from ){//|| from.nodeName == "HTML") {
            // LEFT WINDOW, notify FormBuilder
            thisFormBuilder.mouseDidLeaveWindow(thisFormBuilder);
        }
    });
    function showMe (box) {

        var chboxs = document.getElementsByName("chkPayment");
        var vis = "none";

        for(var i=0;i<chboxs.length;i++) {
            if(chboxs[i].checked){
                vis = "block";
                $('#PayAmount').attr('required', 'required');
                break;
            }
            if (chboxs[i].unchecked) {
                vis = "none";
                $('#PayAmount').removeAttr('required');
            }
        }
        document.getElementById(box).style.display = vis;



    }

    $('.settingsContainer').hide();
    $('.pagetitle .toolbar a.settings').parent().removeClass('current');
</script>
