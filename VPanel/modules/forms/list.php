<?php if (!isset($siteConfig)) die("System Error!"); ?> 
<div class="fullcontent">
    <?
    print '<h1>Manage Forms</h1>';

    if ($form->getNumErrors() > 0) {
        $errors    = $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<div class=\"message success okMessage\">" . $_WEBCONFIG['ENTITY_NAME'] . " Added Successfully!</div>\n";
                break;
            case 'updated':
                echo "<div class=\"message success okMessage\">" . $_WEBCONFIG['ENTITY_NAME'] . " Updated Successfully!</div>\n";
                break;
            case 'deleted':
                echo "<div class=\"message success okMessage\">" . $_WEBCONFIG['ENTITY_NAME'] . " Deleted Successfully!</div>\n";
                break;
        }
        unset($_SESSION['processed']);
    }

    $sql = "SELECT * FROM {$_WEBCONFIG['MODULE_TABLE']} WHERE at_id = '{$_WEBCONFIG['ASSET_TYPE_ID']}' ORDER BY {$_WEBCONFIG['TABLE_PREFIX']}_name";
    $record    = Database::Execute($sql);

    print $record->Count() == 1 ? "<p>There is currently <b>1</b>  " . strtolower($_WEBCONFIG['ENTITY_NAME']) . ".</p>\n" : "<p>There are currently <b>" . $record->Count() . "</b> " . strtolower($_WEBCONFIG['ENTITY_NAME_PLURAL']) . ".</p>\n";
    ?>

    <table id="grid">
        <thead> 
            <tr> 
                <th>Name</th>
                <th>Asset Key</th>
                <th>Last Modified Date</th>
                <th>Status</th>
                <th data-sortable="false" data-filterable="false" style="text-align: center; width: 225px">Options</th>
            </tr>
        </thead>

        <tbody>
            <?
            if ($record->Count() > 0) {
                while ($record->MoveNext()) { 
                    $settings = json_decode($record->as_data); 

                    print '<tr> 
                    <td>' . $record->as_name . '</td>
                    <td>' . $record->as_key . '</td>
                    <td>' . date("m/d/Y", strtotime($record->as_last_update)) . '</td>
                    <td>' . $record->as_status . '</td>';
                    if(isset($settings->cf_id)) { 
                        print '<td align="center"><a href="/VPanel/forms/index.php?view=list&id='. $settings->cf_id .'" class="btn_submissions silver button-slim">Submissions</a>';
                    }
					print '<a href="/VPanel/forms/index.php?view=modify&id=' . $settings->cf_id . '" class="blue button-slim">Config</a>'; 
                    print '<a href="?view=modify&id=' . $record->as_id . '" class="btn_edit green button-slim">Modify</a>';
                    print '<a href="javascript:remove(' . $record->as_id . ');" class="btn_delete red button-slim">Delete</a>';
                    echo '</td></tr>' . "\n";
                }// end while
            }
            ?>
        </tbody>
    </table>

    <div class="buttons clearfix">
        <a href="javascript:history.back()" class="button silver floatLeft">Back</a>
        <a href="javascript:add()" class="button green floatRight">Add New Form</a>
    </div>
</div><!--End continue-->
