<?php
if (!isset($siteConfig))
    die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
if ($iNum > 0) {
    $appeal = Repository::GetById($_WEBCONFIG['MODULE_TABLE'], $iNum);
    if (is_null($appeal)) {
        throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
    }
    $hidId				= $appeal->ap_id;
    $txtProvider        = $appeal->ap_pv_name;
    $txtReferralNo      = $appeal->ap_referral_no;
    $txtProviderNo      = $appeal->ap_provider_no;
    $txtName            = $appeal->ap_name;
    $txtPhone           = $appeal->ap_phone;
    $txtEmail           = $appeal->ap_email;
    $txtCTName          = $appeal->ap_ct_name;
    $txtCTEmail         = $appeal->ap_ct_email;
    $txtCTPhone         = $appeal->ap_ct_phone;
    $txtCTFax           = $appeal->ap_ct_fax;
    $txtReason          = $appeal->ap_reason;
    $txtExplanation     = $appeal->ap_explanation;
} else {
    redirect("/VPanel/modules/appeals/index.php");
}
?>
<style type="text/css">
    span.invalid { color: red; }
    span.valid { color: green; }
</style>

<div class="subcontent right last">
    <form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">
        <input type="hidden" name="actionRun" value="<?= $_GET['view'] ?>" />
        <input type="hidden" name="hidId" value="<?= $hidId; ?>" />


        <?php
        print '<h2>' . $_WEBCONFIG['MODULE_NAME'] . ' -- (' . ucfirst($view) . ')</h2>';

        if ($form->getNumErrors() > 0) {
            $errors	= $form->getErrorArray();
            echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
            foreach ($errors as $err) echo $err;
        } else if (isset($_SESSION['processed'])) {
            echo "<div class=\"valid\">Record Updated Successfully!</div>\n";
            unset($_SESSION['processed']);
        }
        ?>

        <p>Insert the record information below and save.</p>

        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtProvider"><b>Referral No</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtReferralNo ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtProvider"><b>Provider No</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtProviderNo ?></td>
            </tr>

            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtProvider"><b>Provider</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtProvider ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtName"><b>Name</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtName ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtPhone"><b>Phone</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtPhone ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtEmail"><b>Email</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtEmail ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtContactName"><b>Contact Name</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtCTName ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtEmail"><b>Contact Email</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtCTEmail ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtContactPhone"><b>Contact Phone</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtCTPhone ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtContactFax"><b>Contact Fax</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtCTFax ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtReason"><b>Reason</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtReason ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtExplanation"><b>Explanation</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $txtExplanation ?></td>
            </tr>


            <!--<tr class="<?//= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px;"><label for="txtTageLine" class="required"><b>Sub Text</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtTagLine" id="txtTagLine" value="<?//= $txtDesc; ?>" size="85" maxlength="255" required="required" />
            <img data-msg="Please provide a brief description of this marketing message, this will display in the tooltip area of the message. " class="help-tip" src="/VPanel/images/icon-help.png" />
            <?//= $form->error("txtTagLine");?>
            </td>
            </tr>
            <tr class="<?//= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtWebsite"><b>Url</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input style="width: 300px" type="text" name="txtWebsite" id="txtWebsite" value="<?//= $txtWebsite; ?>" size="63" maxlength="255" autocomplete="off" />
            <img data-msg="Please select a page from your website in the drop down list or enter in a url (EX: http://www.forbin.com)" class="help-tip" src="/VPanel/images/icon-help.png" />
            <br><span id="webSpan" class="none"></span>
            <?//= $form->error("txtWebsite");?>
            </td>
            </tr>
            <tr class="<?//= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label><b>Target Window</b></label></td>
            <td style="text-align: left; vertical-align: top;">
            <select name="cboTarget" id="cboTarget">
            <option value="">--  SELECT TARGET WINDOW --</option>
            <option value="blank" <?//= $cboTarget == 'blank' ? "selected='selected'" : ""; ?> >New Window</option>
            <option value="self" <?//= $cboTarget == 'self' ? "selected='selected'" : ""; ?> >Same Window</option>
            </select>
            <?//= $form->error("cboTarget");?>
            </td>
            </tr>-->
        </table>
        <br />
        <h2>Notes</h2>
        <p>Document the progress of this appeal.</p>

        <?
        $sql = "SELECT * FROM tbl_appeals_notes
        WHERE ap_id = $hidId ORDER BY apn_date_created";
        $notes = Database::Execute($sql);
        if ($notes->Count() > 0) {
            ?>
            <table class="grid-display">
                <thead>
                    <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                        <th style="text-align: center; vertical-align: middle;"><b>User</b></th>
                        <th style="text-align: center; vertical-align: middle;"><b>Date</b></th>
                        <th style="text-align: center; vertical-align: middle; width: 50%;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?
                    while ($notes->MoveNext()) {
                        ?>
                        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                            <td style="text-align: center; vertical-align: middle;"><?= $notes->apn_user ?></td>
                            <td style="text-align: center; vertical-align: middle;"><?= date("M d, Y @g:ia", strtotime($notes->apn_date_created)) ?></td>
                            <td style="text-align: left; vertical-align: top;"><?= $notes->apn_note ?></td>
                        </tr>
                        <?
                    }
                    ?>
                    <tr>

                    </tr>
                </tbody>
            </table>
            <?
        } else {
            print '<h3>No notes found for this appeal.</h3>' . PHP_EOL;
        }
        ?>



        <br />
        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px">
                    <label for="mtxNote"><b>Add New Note</b></label>
                </td>
                <td style="text-align: left; vertical-align: top;">
                    <textarea id="mtxNote" name="Note"></textarea>
                    <?= $form->error("mtxNote");?>
                </td>
            </tr>
        </table>



        <div class="buttons clearfix">
            <a href="javascript:history.back()" class="silver button floatLeft">Back</a>
            <input name="btnModify" type="submit" id="btnModify" class="button floatRight green" value="Save Changes">
        </div>
    </form>
</div><!--End continue-->
