<? 
class Process extends Database { 

    private $record;
    private $table;
    private $notes;
    public $params;

    //--------------------------------------------------
    public function __construct($table, $notes) {
    //--------------------------------------------------
        global $_WEBCONFIG;
        parent::__construct();
        $this->table = $table;
        $this->notes = $notes;
    }

    //-----------------------
    public function _add() {
    //-----------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=add";
        } else {
            // No Errors
            $this->record->mk_date_added = date("Y-m-d G:i:s");
            Repository::Save($this->record); 
            $iNum = Database::getInsertID(); 
            Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Added", "Success", "Added -- '{$this->record->mk_name}' (" . $iNum .")", $_SESSION['user_username']);            
            $_SESSION['processed'] = 'added';
            $this->params = "view=list";
        }
    }

    //-----------------------
    public function _view() {
    //-----------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);
        $this->newNote = new Entity($this->notes);
        
        self::_verify();
        
        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=view";
        } else {
            // No Errors
            Repository::Update($this->record);
            
            $this->newNote->apn_user = isset($_SESSION['user_username']) && strlen($_SESSION['user_username']) > 0 ? Database::quote_smart($_SESSION['user_username']) : 'Unregistered User';
            $this->newNote->apn_date_created = date("Y-m-d G:i:s");
            if ($this->newNote->apn_note != 'apn_id') {
                Repository::Save($this->newNote);     
            }
            
            Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Modified", "Success", "Modified -- '{$this->newNote->apn_user}' (" . $this->record->ap_id .")", $this->newNote->apn_user);            
            $_SESSION['processed'] = 'updated';
            $this->params = "view=list";
        }
    }
    
    //---------------------------
    public function _modify() {
    //---------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=modify";
        } else {
            // No Errors
            $this->record->mk_id = (int)$_POST['hidId'];
            $this->record->update();
            Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Modified", "Success", "Modified -- '{$this->record->mk_name}' (" . $this->record->mk_id .")", $_SESSION['user_username']);
            $_SESSION['processed'] = 'updated';
            $this->params = "view=list";
        }
    }

    //---------------------------
    public function _delete() {
    //---------------------------
        global $_WEBCONFIG, $form;
        $iNum    = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

        $res = self::_deleteImage($iNum);

        $this->record = Repository::GetById($this->table, $iNum); 
        Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Deleted", "Success", "Deleted -- '{$this->record->mk_name}' (" . $this->record->mk_id .")", $_SESSION['user_username']);
        Repository::Delete($this->record); 
        $_SESSION['processed'] = 'deleted';
        $this->params = "view=list";
    }
    
    //---------------------------
    public function _archive() {
    //---------------------------
        global $_WEBCONFIG, $form;
        $iNum    = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
        
        $this->record = Repository::GetById($this->table, $iNum);
        $this->record->ap_status = 'Archived';
        Repository::Update($this->record); 
        Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Archived", "Success", "Archived -- '{$this->record->ap_name}' (" . $this->record->ap_id .")", $_SESSION['user_username']);
        $_SESSION['processed'] = 'archived';
        $this->params = "view=list";
    }
    
    //----------------------------
    private function _verify() {
    //----------------------------
        global $_WEBCONFIG, $form;

        $fieldName  = "hidId";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || (int)$fieldValue == 0) {
            $form->setError($fieldName, "* Record ID does not match.");
        } else {
            $this->record->ap_id = (int)$fieldValue;
            $this->newNote->ap_id = (int)$fieldValue;
        }
        
        $fieldName  = "optStatus";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Status is required.");
        } else {
            $this->record->ap_status = $fieldValue;
        }
        
        $fieldName  = "Note";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            //$form->setError($fieldName, "* Status is required.");
        } else {
            $this->newNote->apn_note = $fieldValue;
        }
    }
    
};
?>