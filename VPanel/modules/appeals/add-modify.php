<?php
if (!isset($siteConfig)) 
    die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
if ($form->getNumErrors() > 0) {
    $hidId				= $form->value("hidId");
    $txtName			= $form->value("txtName");
    $txtDesc			= $form->value("txtTagLine");
    $txtWebsite			= $form->value("txtWebsite");
    $hidImage			= $form->value("hidImage");
    $cboTarget          = $form->value("cboTarget");
    $optPage            = $form->value("optPage");
    $txtButtonText      = $form->value("txtButtonText");    
    $optStatus			= $form->value("optStatus");
    $optDisclaimer      = $form->value("optDisclaimer");

} else if ($iNum > 0) {
    $marketing = Repository::GetById($_WEBCONFIG['MODULE_TABLE'], $iNum);

    if (is_null($marketing)) {
        throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
    }
    $hidId				= $marketing->mk_id;
    $txtName			= htmlspecialchars($marketing->mk_name);
    $txtDesc			= htmlspecialchars($marketing->mk_tagline);
    $txtWebsite			= htmlspecialchars($marketing->mk_url);
    $hidImage			= $marketing->mk_image;
    $cboTarget          = $marketing->mk_target;
    $optPage            = $marketing->mk_display_page;
    $txtButtonText      = $marketing->mk_button_text;    
    $optStatus			= $marketing->mk_status;
    $optDisclaimer      = $marketing->mk_disclaimer;
} else {
    $hidId				= '';
    $txtName			= '';
    $txtDesc			= '';
    $txtWebsite			= '';
    $hidImage		    = '';
    $cboTarget          = 'self';
    $optPage            = 'H';
    $txtButtonText      = "Learn More";    
    $optStatus			= 'Active';
    $optSite			= 1;
    $optDisclaimer      = 0;
}
?> 
<style type="text/css">
    span.invalid { color: red; }
    span.valid { color: green; }
</style>

<div class="subcontent right last">
    <form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">
        <input type="hidden" name="actionRun" value="<?= $_GET['view'] ?>" />
        <input type="hidden" name="hidId" value="<?= $hidId; ?>" />
        <input type="hidden" name="hidImage" value="<?= $hidImage; ?>" />


        <?php
        print '<h2>' . $_WEBCONFIG['MODULE_NAME'] . ' -- (' . ucfirst($view) . ')</h2>';

        if ($form->getNumErrors() > 0) {
            $errors	= $form->getErrorArray();
            echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
            foreach ($errors as $err) echo $err;
        } else if (isset($_SESSION['processed'])) {
            echo "<div class=\"valid\">Record Updated Successfully!</div>\n";
            unset($_SESSION['processed']);
        }
        ?>

        <p>Insert the record information below and save.</p>

        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtName" class="required"><b>Title</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtName" id="txtName" value="<?= $txtName; ?>" size="40" maxlength="60" required="required" />
                    <?= $form->error("txtName");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px;"><label for="txtTageLine" class="required"><b>Sub Text</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtTagLine" id="txtTagLine" value="<?= $txtDesc; ?>" size="85" maxlength="255" required="required" />
                    <img data-msg="Please provide a brief description of this marketing message, this will display in the tooltip area of the message. " class="help-tip" src="/VPanel/images/icon-help.png" />
                    <?= $form->error("txtTagLine");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtWebsite"><b>Url</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input style="width: 300px" type="text" name="txtWebsite" id="txtWebsite" value="<?= $txtWebsite; ?>" size="63" maxlength="255" autocomplete="off" />
                    <img data-msg="Please select a page from your website in the drop down list or enter in a url (EX: http://www.forbin.com)" class="help-tip" src="/VPanel/images/icon-help.png" />
                    <br><span id="webSpan" class="none"></span>    
                    <?= $form->error("txtWebsite");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label><b>Target Window</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                    <select name="cboTarget" id="cboTarget">
                        <option value="">--  SELECT TARGET WINDOW --</option>
                        <option value="blank" <?= $cboTarget == 'blank' ? "selected='selected'" : ""; ?> >New Window</option>
                        <option value="self" <?= $cboTarget == 'self' ? "selected='selected'" : ""; ?> >Same Window</option>
                    </select>
                    <?= $form->error("cboTarget");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label><b>Display Page</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                    <ul style="list-style-type: none;">
                        <li class="threecol">
                            <input type="checkbox" id="optPageH" name="optPage[]" value="H"<?= strstr($optPage, 'H') ? ' checked' : '' ?> />
                            <label for="optPageH"> Homepage</label>
                        </li>
                        <li class="threecol">
                            <input type="checkbox" id="optPageCM" name="optPage[]" value="CM"<?= strstr($optPage, 'CM') ? ' checked' : '' ?> />
                            <label for="optPageCM"> Case Manager Portal</label>
                        </li>
                        <li class="threecol last">
                            <input type="checkbox" id="optPageP" name="optPage[]" value="P"<?= strstr($optPage, 'P') ? ' checked' : '' ?> />
                            <label for="optPageP"> Provider Portal</label>
                        </li>
                    </ul>
                    <?= $form->error("chkPage");?>
                </td>
            </tr>
            <!--<tr class="<?//= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label><b>Display Page</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                    <select name="cboPage" id="cboPage">
                        <option value="">--  SELECT DISPLAY PAGE --</option>
                        <option value="H" <?//= $cboPage == '/' ? "selected='selected'" : ""; ?> >Homepage</option>
                        <option value="CM" <?//= $cboPage == '/members/cm-dashboard.php' ? "selected='selected'" : ""; ?> >Case Manager Dashboard</option>
                        <option value="P" <?//= $cboPage == '/members/provider-dashboard.php' ? "selected='selected'" : ""; ?> >Provider Dashboard</option>
                    </select>
                    <?//= $form->error("cboPage");?>
                </td>
            </tr>-->
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtButtonText"><b>Button Text</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtButtonText" id="txtButtonText" value="<?= $txtButtonText; ?>" size="63" maxlength="255" />
                    <?= $form->error("txtButtonText");?>
                </td>
            </tr>        
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label class="required" for="fleLeftImage"><b>Image</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                    <?php
                    if ($hidImage && file_exists(filePathCombine($_SERVER['DOCUMENT_ROOT'],$uploadFolder,$hidImage))) {
                        print "<div>\n";
                        print '<img src="' . $uploadFolder . $hidImage . '" border="1" alt="" height="160" />';
                        print "</div>\n";
                    }
                    
                    if ($hidImage != '') {
                        print '<input name="fleImage" type="file" id="fleImage" title="See note of allowed logo formats" size="50" maxlength="255" imgtype="optional" />';
                    } else {
                        print '<input name="fleImage" required="required" type="file" id="fleImage" title="See note of allowed logo formats" size="50" maxlength="255" imgtype="optional" />';
                    }
                    
                    $form->error("fleImage");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="optStatus" class="required"><b>Status</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                    <input name="optStatus" type="radio" id="optAStatus" value="Active" <?= $optStatus == 'Active' ? 'checked' : ''; ?> />
                    <label for="optAStatus">Active </label> &nbsp;&nbsp;
                    <input name="optStatus" type="radio" id="optIStatus" value="Inactive" <?= $optStatus == 'Inactive' ? 'checked' : ''; ?> />
                    <label for="optIStatus">Inactive </label> &nbsp;&nbsp;
                    <?= $form->error("optStatus");?>
                </td>
            </tr>
            <?php
            if (isset($_WEBCONFIG['SITE_TYPE']) && $_WEBCONFIG['SITE_TYPE'] == 'BANK') {  ?>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 125px"><label for="optDisclaimer" class="required"><b>Disclaimer</b></label></td>
                    <td style="text-align: left; vertical-align: top;">
                        <input name="optDisclaimer" type="radio" id="optYDisclaimer" value="1" <?= $optDisclaimer == 1 ? 'checked' : ''; ?> />
                        <label for="optYDisclaimer">Yes </label> &nbsp;&nbsp;
                        <input name="optDisclaimer" type="radio" id="optNDisclaimer" value="0" <?= $optDisclaimer == 0 ? 'checked' : ''; ?> />
                        <label for="optNDisclaimer">No </label> &nbsp;&nbsp;
                        <?= $form->error("optDisclaimer");?>
                    </td>
                </tr>    
      <?php } ?>

        </table>

        <div class="buttons clearfix">
            <a href="javascript:history.back()" class="silver button floatLeft">Back</a>
            <input name="btnModify" type="submit" id="btnModify" class="button floatRight green" value="Save Changes">
        </div>
    </form>
</div><!--End continue-->
