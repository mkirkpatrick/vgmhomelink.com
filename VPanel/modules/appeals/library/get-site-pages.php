<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");

$query = "SELECT cms_dynamic_link FROM tbl_cms 
          WHERE (cms_is_deleted = 0 and cms_is_historic = 0 and cms_is_published = 1 AND cms_id <> 1 AND cms_is_system_page = 0)
          OR (cms_is_deleted = 0 and cms_is_historic = 0 and cms_is_published = 1 AND cms_id <> 1 AND cms_is_system_page = 1 AND cms_is_editable = 1)
          ORDER BY cms_dynamic_link";              
$link = mysqli_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASS);

mysqli_select_db($link, DATABASE_NAME);

$arr = array();
$rs = mysqli_query($link, $query);
while($obj = mysqli_fetch_object($rs)) { 
    $arr[] = $obj; 
}

header("Content-type: application/json; charset=utf-8");
echo json_encode($arr);
exit();

?>