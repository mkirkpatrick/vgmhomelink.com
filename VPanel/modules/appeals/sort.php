<?
if (!isset($siteConfig)) 
    die("System Error!");

$sql = "SELECT mk_id, mk_name FROM tbl_marketing ORDER BY mk_sort_id, mk_name";
$record = Database::Execute($sql);

$rows = 0;
?> 
<div class="subcontent right last">
    <form id="sortForm" method="post" action="process.php">
        <input type="hidden" name="actionRun" value="sort" />
        <?= '<h2>' . $_WEBCONFIG['MODULE_NAME'] . ' -- (' . ucfirst($view) . ')</h2>'; ?>
        <p>Drag and drop the records in the order you desire them.</p>

        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtName" class="input required"><b>List of Items</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                    <div id="contentOrder">
                        <ul>
                            <?
                            while ($record->MoveNext()) {
                                print "<li id=\"recordsArray_{$record->mk_id}\">$record->mk_name</li>\n";
                            }
                            ?>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <div class="clearfix buttons">
            <a href="javascript: history.back()" class="floatLeft button silver">Back</a>
        </div>
    </form>
</div>