<?
if (!isset($siteConfig)) die("System Error!");

    $sql = "SELECT faq_sort_id FROM tbl_faq";
    $sort = Database::Execute($sql);
    
    if($sort->Count() > 0) {
        $sort->MoveLast();
        $sortID = $sort->faq_sort_id;
        $sortID = $sortID + 10;
    }
    else {
        $sortID = 0;
    }

	
$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
if ($form->getNumErrors() > 0) {
	$hidId				= $form->value("hidId");
	$txtQuestion		= $form->value("txtQuestion");
	$mtxAnswer			= $form->value("mtxAnswer");
	$txtOrder			= $form->value("txtOrder");
	$optStatus			= $form->value("optStatus");
} else if ($iNum > 0) {
	$faq = Repository::GetById($_WEBCONFIG['MODULE_TABLE'], $iNum);

	if (is_null($faq)) {
		throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
	}
	$hidId				= $faq->faq_id;
	$txtQuestion		= htmlspecialchars($faq->faq_question);
	$mtxAnswer			= $faq->faq_answer;
	$txtOrder			= $faq->faq_sort_id;
	$optStatus			= $faq->faq_status;
} else {
	$hidId				= '';
	$txtQuestion		= '';
	$mtxAnswer			= '';
	$txtOrder			= $sortID;
	$optStatus			= 'Active';
}
?> 

<form id="entryForm" method="post" action="process.php">
<input type="hidden" name="actionRun" value="<?= $_GET['view'] ?>" />
<input type="hidden" name="hidId" value="<?= $hidId; ?>" />

<div class="subcontent right last">

	<?
	print '<h1>' . $_WEBCONFIG['MODULE_NAME'] . ' -- (' . ucfirst($view) . ')</h1>';

	if ($form->getNumErrors() > 0) {
		$errors	= $form->getErrorArray();
		echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
		foreach ($errors as $err) echo $err;
	} else if (isset($_SESSION['processed'])) {
        echo "<div class=\"valid\">Record Updated Successfully!</div>\n";
		unset($_SESSION['processed']);
	}
	?>

    <p>Insert the record information below and save.</p>

    <table class="grid-display">
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtQuestion" class="required"><b>Question</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtQuestion" id="txtQuestion" value="<?= $txtQuestion; ?>" size="160" maxlength="1000" required="required" style="width: 450px;" />
                <?= $form->error("txtQuestion");?>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label class="required" for="mtxAnswer"><b>Answer</b></label></td>
            <td style="text-align: left; vertical-align: top;"><textarea name="mtxAnswer" id="mtxAnswer"><?= $mtxAnswer; ?></textarea>
                <?= $form->error("mtxAnswer");?>
            </td>
        </tr>
	    <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
			<td style="text-align: right; vertical-align: middle; width: 125px"><b>Order</b></td>
			<td style="text-align: left; vertical-align: top;">
				<input name="txtOrder" type="text" value="<?= $txtOrder; ?>" size="2" maxlength="5" onfocus="this.select();" onkeypress="return isNumberKey(event)" /> 
                <?= $form->error("txtOrder");?>
				<i>(Lowest number is closest to top)</i>
			</td>
		</tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="optStatus" class="required"><b>Status</b></label></td>
            <td style="text-align: left; vertical-align: top;">
                <input name="optStatus" type="radio" id="optAStatus" value="Active" <?= $optStatus == 'Active' ? 'checked' : ''; ?> />
                <label for="optAStatus">Active </label> &nbsp;&nbsp;
                <input name="optStatus" type="radio" id="optIStatus" value="Inactive" <?= $optStatus == 'Inactive' ? 'checked' : ''; ?> />
                <label for="optIStatus">Inactive </label> &nbsp;&nbsp;
                <?= $form->error("optStatus");?>
            </td>
        </tr>
    </table>

    <div class="buttons clearfix">
        <a href="javascript:history.back()" class="silver button floatLeft">Back</a>
           <input name="btnModify" type="submit" id="btnModify" class="button floatRight green" value="Save Changes">
    </div>
</div><!--End continue-->

</form>
