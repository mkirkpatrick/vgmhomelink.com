<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 

security::_secureCheck();

if (!isset($_GET['logout'])) {
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

print '<section class="title">
            <div class="container clearfix">
                <h1 class="page-title icon-' . $_WEBCONFIG['MODULE_ICON'] . '">' . $_WEBCONFIG['MODULE_NAME'] . '</h1>
                <p class="page-desc">' . $_WEBCONFIG['MODULE_DESCRIPTION'] . '</p>
            </div>
        </section>';
print '<section class="maincontent"><div class="container clearfix">';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/modules/modulesNavBar.php');

$rows = 0;

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'add' :
	case 'modify' :
		$script	= array($_WEBCONFIG['MODULE_FOLDER'] . 'js/form.js');
		include 'add-modify.php';		
		break;

	case 'sort' :
		$script	= array('jquery/jquery-ui-1.7.2.custom.min.js', $_WEBCONFIG['MODULE_FOLDER'] . 'js/sort.js');
		include 'sort.php';		
		break;

	default :
		$script	= array($_WEBCONFIG['MODULE_FOLDER'] . 'js/list.js');
		include 'list.php';		
}

print '</div></section>';

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>