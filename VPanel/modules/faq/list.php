<?
if (!isset($siteConfig)) { 
    die("System Error!");
}
?>

<div class="subcontent right last">
    <div class="floatRight forward">
        <a href="index.php?view=sort" class="button silver icon-arrows-ccw">Reorder</a>
    </div>

    <?
    print '<h1>' . $_WEBCONFIG['MODULE_NAME'] . '</h1>';
    
    if ($form->getNumErrors() > 0) {
        $errors	= $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } 
    else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
                break;
            case 'updated':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                break;
            case 'deleted':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                break;
        }
        unset($_SESSION['processed']);
    }
    $sql = "SELECT * FROM {$_WEBCONFIG['MODULE_TABLE']} ORDER BY faq_sort_id, faq_question";
    $record	= Database::Execute($sql);
    print $record->Count() == 1 ? "<p>There is currently <b>1</b> " . $_WEBCONFIG['ENTITY_NAME'] . "</p>\n" : "<p>There are currently <b>" . $record->Count() . "</b> " . $_WEBCONFIG['ENTITY_NAME'] . "s. </p>\n";
    ?>
    <table id="grid">
        <thead> 
            <tr> 
                <!--<th>Title</th>-->
                <th>Question</th>
                <th style="width: 50px">Order</th>
                <th style="width: 50px">Status</th>
                <th style="width: 140px">Last Modified Date</th>
                <th data-sortable="false" data-filterable="false" style="text-align: center; width: 125px">Options</th>
            </tr>
        </thead>
        <tbody>
            <?
            if ($record->Count() > 0) {
                while ($record->MoveNext()) {
                    print '<tr> 
                                <td>' . $record->faq_question . '</td>
                                <td style="text-align: center">' . $record->faq_sort_id . '</td>
                                <td style="text-align: center">' . $record->faq_status . '</td>
                                <td>' . date("m/d/Y g:i:s A", strtotime($record->faq_last_update)) . '</td>
                                <td><a href="javascript:modifyFAQ(' . $record->faq_id . ');" class="green button-slim">Modify</a>
                                <a href="javascript:deleteFAQ(' . $record->faq_id . ');" class="red button-slim">Delete</a></td>
                            </tr>' . "\n";
                }// end while
            }
            ?>
        </tbody>
    </table>

    <div class="buttons clearfix">
        <a href="javascript:history.back()" class="button blue floatLeft">Back</a>
        <a href="javascript:addFAQ()" class="button green floatRight">Add New <?= $_WEBCONFIG['ENTITY_NAME'] ?></a></div>
    </div><!--End continue-->

</div><!--End content-->