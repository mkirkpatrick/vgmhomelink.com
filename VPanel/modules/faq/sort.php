<?
if (!isset($siteConfig)) die("System Error!");

$sql = "SELECT faq_id, faq_question FROM {$_WEBCONFIG['MODULE_TABLE']} ORDER BY faq_sort_id, faq_question";
$record = Database::Execute($sql);

$rows = 0;
?> 

<form id="sortForm" method="post" action="process.php">
<input type="hidden" name="actionRun" value="<?= $view ?>" />

<div class="subcontent right last">

	<?= '<h1>' . $_WEBCONFIG['MODULE_NAME'] . ' -- (' . ucfirst($view) . ')</h1>'; ?>

    <p>Drag and drop the records in the order you desire them.</p>

    <table class="grid-display">
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtName" class="input required"><b>List of Items</b></label></td>
            <td style="text-align: left; vertical-align: top;">
				<div id="contentOrder">
					<ul>
					<?
					while ($record->MoveNext()) {
						print "<li id=\"recordsArray_{$record->faq_id}\">$record->faq_question</li>\n";
					}
					?>
					</ul>
				</div>
            </td>
        </tr>
     </table>
    <p class="tipBubble"><b>Tip:</b> To move a record to the top of the list drag it to the top.</p>

<div class="clearfix buttons">
    <a href="javascript: history.back()" class="floatLeft button silver">Back</a>
</div>
</div>
</form>