<?
class Process extends Database { 

    private $record;
    private $table;
    public $params;

    //------------------------------
    public function __construct($table) {
    //------------------------------
        global $_WEBCONFIG;
        parent::__construct();
        $this->table        = $table;
    }

    //------------------------------
    public function _add() {
    //------------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=add";
        } else {
            // No Errors
            $this->record->faq_date_added = date("Y-m-d G:i:s");
            Repository::Save($this->record); 
            $iNum = Database::getInsertID(); 
            Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Added", "Success", "Added -- '{$this->record->faq_question}' (" . $iNum .")", $_SESSION['user_username']);            
            $_SESSION['processed'] = 'added';
            $this->params = "view=list";
        }
    }

    //------------------------------
    public function _modify() {
    //------------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=modify";
        } else {
            // No Errors
            $this->record->faq_id = (int)$_POST['hidId'];
            $this->record->update();
            Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Modified", "Success", "Modified -- '{$this->record->faq_question}' (" . $this->record->faq_id .")", $_SESSION['user_username']);
            $_SESSION['processed'] = 'updated';
            $this->params = "view=list";
        }
    }

    //------------------------------
    public function _delete() {
    //------------------------------
        global $_WEBCONFIG, $form;
        $iNum    = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
        $this->record = Repository::GetById($this->table, $iNum); 
        Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Deleted", "Success", "Deleted -- '{$this->record->faq_question}' (" . $this->record->faq_id .")", $_SESSION['user_username']);
        Repository::Delete($this->record); 
        $_SESSION['processed'] = 'deleted';
        $this->params = "view=list";
    }

    //------------------------------
    public function _sort() {
    //------------------------------
        $indexCounter     = 1;
        $updRecordsArray = $_POST['recordsArray'];

        foreach($updRecordsArray as $recordIDValue) {
            $sql = "UPDATE {$this->table} SET faq_sort_id = $indexCounter WHERE faq_id = $recordIDValue";
            $res = Database::ExecuteRaw($sql);
            $indexCounter++;    
        }

        print "Records were re-ordered!";
    }

    //------------------------------
    private function _verify() {
    //------------------------------
        global $_WEBCONFIG, $form;

        $fieldName	= "txtQuestion";
        $fieldValue	= strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Question is required.");
        } else {
            $this->record->faq_question = $fieldValue;
        }

		$fieldName	= "mtxAnswer";
		$fieldValue = strip_tags($_POST[$fieldName], '<img>');
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$this->record->faq_answer = '';
		} else {
			$this->record->faq_answer = cleanHTML($_POST[$fieldName]);
		}

        $fieldName	= "txtOrder";
        $fieldValue	= strip_tags($_POST[$fieldName]);
        if (strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->faq_sort_id = 99999;
        } else {
            $this->record->faq_sort_id = $fieldValue;
        }

        $fieldName	= "optStatus";
        $fieldValue	= isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Status is required.");
        } else {
            $this->record->faq_status = $fieldValue;
        }
    }

};
?>