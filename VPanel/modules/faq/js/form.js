$(function() {
	// add * to required field labels
	$('label.required').append('&nbsp;<span style="color: #990000"><b>*</b></span>&nbsp;');

    CKEDITOR.config.bodyClass = 'content';
    CKEDITOR.config.resize_enabled = true; 
    CKEDITOR.config.toolbarCanCollapse = true; 
    CKEDITOR.config.contentsCss = [ '/css/main.css','/VPanel/css/ckeditor-override.css'];
    CKEDITOR.config.toolbar =
    [
		    { name: 'clipboard',   items : [ 'Source','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		    { name: 'basicstyles', items : [  'Bold','Italic','Underline','Strike','Subscript','Superscript','-', 'TextColor','BGColor', 'RemoveFormat' ] }

    ];

    CKEDITOR.replace( 'mtxAnswer',{
	    removePlugins : 'maximize,resize',
	    extraPlugins : 'tokens',
			
	    filebrowserBrowseUrl : '/VPanel/kcfinder/browse.php?type:files',
	    filebrowserImageBrowseUrl : '/VPanel/kcfinder/browse.php?type:images',
	    filebrowserUploadUrl : '/VPanel/kcfinder/upload.php?type:files',
	    filebrowserImageUploadUrl : '/VPanel/kcfinder/upload.php?type:images',
    } );
    CKEDITOR.config.width = '750';
    CKEDITOR.config.height = '150';
});