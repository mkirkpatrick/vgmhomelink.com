$(document).ready(function () {
    $("#grid").before('<div id="grid-loader" class="k-loading-mask" style="width:80%;height:38%"><span class="k-loading-text">Loading...</span><div class="k-loading-image"><div class="k-loading-color"></div></div></div>');
    var kendoPageSize = 20;
    $("#grid").kendoGrid({
        dataSource: {
            schema: {
                model: {
                    id:"Id",
                    fields: {
                        name: { type: "string" },
                        number: { type: "string" },
                        amount: { type: "string" },
                        status: { type: "string" },
                        submitDate: { type: "date" },
                        options: { type: "string" }
                    }
                }
            },
            pageSize: kendoPageSize
        },
        columns: [
            { field: "name", width: 150, filterable: true },
            { field: "number", width: 110, filterable: true },
            { field: "amount", width: 85, filterable: true },
            { field: "status", width: 85, filterable: false },
            { field: "submitDate", width: 130, filterable: false, format: "{0:MMM dd, yyyy @ h:mm tt}" },
            { field: "options", width: 140, filterable: false, sortable: false }
        ],
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",
                    eq: "Exact Match",
                    startswith: "Starts With",
                    endswith: "Ends With",
                    doesnotcontain: "Does Not Contain",
                    neq: "Does Not Match"
                }
            }
        },
        pageable: { pageSizes: [25, 50, 100, 500] },
        columnMenu: false,
        groupable: false,
        sortable: true,
        scrollable: true,
        resizable: true,
        reorderable: true,
        dataBound: function(e){
            $("#grid-loader").hide();    
            $("#grid").fadeIn();
        },
        toolbar : [
            { template: kendo.template($("#dateFilterTemplate").html()) },
            { template: kendo.template($("#searchBarTemplate").html()) }
        ], 
    });


    if(getInternetExplorerVersion() <= 8 && getInternetExplorerVersion() >= 5) {
        $('.toolbar').hide();
    } else {
        $("#clearTextButton").kendoButton({icon: "funnel-clear"});
        $("#txtSearch").on("keyup keypress onpaste", function () {
            var filter = { logic: "or", filters: [] };
            $searchValue = $(this).val();
            if ($searchValue.length > 1) {
                $.each($("#grid").data("kendoGrid").columns, function( key, column ) {
                    if(column.filterable) {
                        filter.filters.push({ field: column.field, operator:"contains", value:$searchValue});
                    }
                });

                $("#grid").data("kendoGrid").dataSource.query({ filter: filter });

                var count = $("#grid").data("kendoGrid").dataSource.total();
                $(".k-pager-info").html("1 - " + count + " of " + count + " items");

            } else if($searchValue == "") {
                $("#grid").data("kendoGrid").dataSource.query({
                    page: 1,
                    pageSize: kendoPageSize
                });              
            }
        });

        $("#clearTextButton").on("click", function () {
            $("#grid").data("kendoGrid").dataSource.query({
                page: 1,
                pageSize: kendoPageSize
            });
            $("#txtSearch").val('');
        });
    }
    
    $("#sdate").kendoDatePicker();
    $("#edate").kendoDatePicker();    
     
});

// Button Functions
function viewPayment(Id) {
    window.location.href = 'index.php?view=view&id=' + Id;
}

function deletePayment(Id) {
    window.location.href = 'process.php?actionRun=archive&id=' + Id;
}
