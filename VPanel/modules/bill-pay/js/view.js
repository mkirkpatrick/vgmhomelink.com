$(function() {
    CKEDITOR.config.resize_enabled = false; 
    CKEDITOR.config.toolbarCanCollapse = true;
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.height = 500;
    CKEDITOR.config.autoGrow_maxHeight = 500;
    CKEDITOR.config.autoGrow_minHeight = 500;
    CKEDITOR.config.toolbar =
    [
        { name: 'document',    items : [ 'Source' ] },
        { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
        { name: 'editing',     items : [ 'SelectAll','-','SpellChecker', 'Scayt' ] },
        { name: 'styles',      items : [ 'Styles','Format','FontSize' ] },
        { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript' ] },
        { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
        { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
        { name: 'insert',      items : [ 'Image','Table','HorizontalRule','SpecialChar' ] },
        { name: 'basicstyles', items : [ 'MediaEmbed'] }

    ];


    CKEDITOR.replace( 'mtxContent',{
        removePlugins : 'maximize,resize',
        extraPlugins: 'tokens,MediaEmbed',
    } );

    $("#txtPublishedDate").kendoDateTimePicker();

    var window = $("#thumbnailDiv"); 
    $(".thumbNailLink").on("click", function() {
        $('.overlay-mask').fadeIn();
        window.data("kendoWindow").center();
        window.data("kendoWindow").open();
        $("body").css("overflow", "hidden");
    });
    if (!window.data("kendoWindow")) {
        window.kendoWindow({
            width: "900px",
            height: "600px",
            actions: ["Close"],
            resizable: false,
            title: "Thumbnail Image Upload",
            close: function() {
                $('.overlay-mask').fadeOut();
                $('#imgThumbnail').attr("src", $("#thumbnailUploaderFrame").contents().find("#imgPhotoThumbnail").attr("src"));
                $("body").css("overflow", ""); 
            }
        });
    }

    var fullWindow = $("#fullFeaturedDiv"); 
    $(".fullFeaturedLink").on("click", function() {
        $('.overlay-mask').fadeIn();
        fullWindow.data("kendoWindow").center();
        fullWindow.data("kendoWindow").open();
        $("body").css("overflow", "hidden");
    });

    if (!fullWindow.data("kendoWindow")) {
        fullWindow.kendoWindow({
            width: "900px",
            height: "600px", 
            actions: ["Close"],
            resizable: false,
            title: "Featured Header Image Upload",
            close: function() {
                $('.overlay-mask').fadeOut();
                $('#imgFeatured').attr("src", $("#fullUploaderFrame").contents().find("#imgPhotoFeatured").attr("src"));
                $("body").css("overflow", ""); 
            }/*, 
            open: function() { 
            $("#fullUploaderFrame").scrollTop(0);
            }*/
        });
    }

    var assetWindow = $("#assetListWindow").kendoWindow({
        draggable: true,
        resizable: false,
        width: "600px",
        actions: ["Close"],
        title: "PageBuilder Asset Library",
        modal: true,
        visible: false,
        close: function() {
            $('.overlay-mask').fadeOut();
            $("body").css("overflow", "");
        }        
    });

    $("span.insertBtn").on('click', function (e) {
        var html = ""; 
        if($(this).data('addcontainer')) { 
            html += '<div class="pbAsset">\n'; 
            html += "<!-- PageBuilder Asset -->\n"; 
            html += $(this).data('key');
            html += "<!-- End PageBuilder Asset -->\n"; 
            html += '</div>'; 
        } else {
            html += '<span class="pbAsset">\n'; 
            html += "<!-- PageBuilder Asset -->\n"; 
            html += $(this).data('key'); 
            html += "<!-- End PageBuilder Asset -->\n"; 
            html += '</span>\n'; 
        }
        CKEDITOR.instances['mtxContent'].insertHtml(html);
        CKEDITOR.instances['mtxContent'].updateElement();
        $('.overlay-mask').fadeOut();
        $("body").css("overflow", "");
        assetWindow.data("kendoWindow").close();
    });

    $("#btnViewAsset").on("click", function () {
        //console.log('made it to btnViewAsset');
        $('html, body').animate({ scrollTop: 250 }, 0);
        $('.overlay-mask').fadeIn();
        assetWindow.data("kendoWindow").center().open();
        $("body").css("overflow", "hidden");
    });

});

$(window).load(function() {
    $('.k-window-action').removeAttr('href');
})

$('.module > h3').click(function () {
    $(this).toggleClass('open');
    $(this).next('.body').slideToggle();
    return false;
});

$('#SlugEditButton').click(function () {
    if ($(this).text() == "Edit") {
        $('#spanSlugUrl').attr("contenteditable", "true");
        $('#spanSlugUrl').css("background-color", "#fffbcc");
        $(this).text("OK");
    } else {
        $('#spanSlugUrl').attr("contenteditable", "false");
        if (postID == 0) {
            $(this).hide();
        } else {
            $(this).text("Edit");
        }
        if ($('#spanSlugUrl').html().length > 0) {
            var text = $('#spanSlugUrl').html();
            text = text.toLowerCase();
            text = text.replace(/[^a-zA-Z0-9]+/g, '-');
            text = text.replace(/nbsp/g, '');
            $('#spanSlugUrl').html(text);
            $('.search-url').html($('#spanSlugUrl').html());
            $('#hidSlug').val($('#spanSlugUrl').html());
            checkSEOKeywords();
        }
    }
});

$('#txtTitle').on("paste click keyup blur load", function () {
    if (postID == 0) {
        if ($(this).val().length > 0) {
            var text = $(this).val();

            $('#txtMetaTitle').val(text); 
            $('.search-link').html(text);

            text = text.toLowerCase();
            text = text.replace(/[^a-zA-Z0-9]+/g, '-');
            text = text.replace(/nbsp/g, '');
            $('#spanSlugUrl').html(text);
            $('.search-url').html($('#spanSlugUrl').html());
            $('#hidSlug').val($('#spanSlugUrl').html());
            $('#SlugEditButton').show(); 

        }
    }
    checkSEOKeywords();
});



$('#txtMetaTitle').on("paste click keyup blur load", function () {
    if ($(this).val().length > 0) {
        $('.search-link').html($(this).val());
        checkSEOKeywords();
    }
});

$('#mtxMetaDescription').on("paste click keyup blur load", function () {
    if ($(this).val().length > 0) {
        $('.search-description').html($(this).val());
        checkSEOKeywords();
    }
});

$('#txtKeywords').on("paste click blur keyup load blur ", function () {
    checkSEOKeywords();
});

var isEdit = typeof $.url().param('id') != 'undefined';
if (isEdit) {
    $('.icon-search').show();
    $('.icon-docs').show();
    $('.icon-check').html('Publish Changes');
    $('#actionSpan').html('Modify');
}
else {
    $('.icon-docs').hide();
    $('.icon-check').html('Create New Post');
    $('#actionSpan').html('Create New');
}

$('*[data-maxchars]').on('paste keyup load', function () { truncateText(this); });

$('#ScheduleEditButton').click(function (event) {
    $(this).text() == "Cancel" ? $(this).text("Edit") : $(this).text("Cancel");
    $('#SchedulerDiv').slideToggle("slow");
});


$('#SchedulerOKButton').click(function (event) {
    var datetime = $('#txtPublishedDate').val();
    $('#ScheduleEditButton').text("Edit");
    $('#PublishTime').text(datetime).val();
    $('#SchedulerDiv').slideToggle("slow");
});

$('#AddTagButton').on('click load', function (e) {
    e.preventDefault();
    var tag = $('#AddTagTextInput').val().trim();
    if (tag.length > 0) {
        $.post( "process.php", { actionRun: "addTag", tag: tag }); 
        $('#hidTags').val($('#hidTags').val() + "|" + tag);
        $('#TagsDiv').append('<span class="tag"><span>' + tag + '</span><a style="cursor: pointer" title="Click to remove tag!">x</a></span>');
        $('#tagLibrary option[value="' + tag + ']"').remove();
        $('#AddTagTextInput').val("");
        $('#AddTagTextInput').focus();
    }
    checkSEOKeywords();
});

$('#btnAddCategory').on('click load', function (e) {
    e.preventDefault();
    var category = $('#txtCategoryName').val().trim();		
    if(category.length > 0) {
        $.post( "process.php", { actionRun: "addCategory", categoryName: category }, function( data ) {
            if(data.indexOf("EXISTS") == -1) { 
                var iNum = data; 
                $('#listCategory').append('<li><label for="category' + iNum + '"><input type="checkbox" id="category' + iNum + '" name="cboCategories[]" checked="checked" value="' + iNum + '" /> ' + category + '</label></li>');
            } else {
                $array = data.split("::");
                $catId = $array[1]; 
                $('#category' + $catId).prop("checked", true); 
            }
            $('#txtCategoryName').val("");
            $('#txtCategoryName').focus();
        });
    }
});

$(document).on('click', 'span.tag a', function (e) {
    $('#hidTags').val($('#hidTags').val().replace("|" + $(this).prev().text(), ''));
    $('#tagLibrary').append("<option value='" + $(this).prev().text() + "></option>");
    $(this).parent().remove();
    checkSEOKeywords();
});



$("#txtTitle").on("click paste load keyup", function () {
    if (postID.length == 0 && $('#spanSlugUrl').attr("contenteditable") == "false") {
        var text = $(this).val();
        text = text.toLowerCase();
        text = text.replace(/[^a-zA-Z0-9]+/g, '-');
        $('#spanSlugUrl').html(text);
    }
    if($("#fullUploaderFrame").contents().find("#txtAltText").data("allowdefault")) { 
        $("#fullUploaderFrame").contents().find("#txtAltText").val($(this).val());
    }
    if($("#thumbnailUploaderFrame").contents().find("#txtAltText").data("allowdefault")) { 
        $("#thumbnailUploaderFrame").contents().find("#txtAltText").val($(this).val());
    }
    checkSEOKeywords();
});

$("#cke_wysiwyg_frame cke_reset").on("click paste load keyup", function () {
    checkSEOKeywords();
});

function truncateText(textbox) {
    var max = parseInt($(textbox).data('maxchars'));
    var recommended = parseInt($(textbox).data('rchars'));
    var messageId = $(textbox).attr("id") + "Message";
    var currentCharacters = recommended - $(textbox).val().length;
    var cssClass = currentCharacters < 0 ? "red" : "green";
    if ($(textbox).val().length > max) {
        $(textbox).val($(textbox).val().substr(0, $(textbox).data('maxchars')));
    }
    if ($('#' + messageId).length == 0) {
        $(textbox).after("<p class='small' id='" + messageId + "'></p>");
    }
    var tip = 'You are limited to ' + recommended + ' characters (<span class="' + cssClass + '">' + currentCharacters + '</span> characters left)';
    $('p#' + messageId).html(tip);
}

function showPreview() {
    var url = "/" + blogSlug + "/post/?preview=true"; 
    var form = $('form#entryForm')[0];
    var oldAction = $(form).attr('action');
    var specs = "width=1200,height=700,location=1,resizeable=1,status=1,scrollbars=1,toolbar=1,menubar=1";
    window.open(url, 'PagePreview', specs);
    $(form).attr("target", "PagePreview");
    $(form).attr("action", url);
    form.submit();
    $(form).removeAttr("target");
    $(form).attr("action", oldAction);
    return false;
}

function filterCheck(text) {
    if(text === undefined) { 
        return ""; 
    } else { 
        text = text.toLowerCase();
        text = text.replace(/[^a-zA-Z0-9]+/g, '-');
        return text;
    }
}

function checkSEOKeywords() {
    $('#mtxContent').each(function () {
        var $textarea = $(this);
        if(CKEDITOR.instances[$textarea.attr('name')] !== undefined) {

            $textarea.val(CKEDITOR.instances[$textarea.attr('name')].getData());
        }
    });
    var keywordPhrase = $('#txtKeywords').val();
    if(0 !== keywordPhrase.trim().length) { 
        if (filterCheck($('#txtMetaTitle').val()).indexOf(filterCheck(keywordPhrase)) >= 0) {
            $('#titleCheck').attr("class", "good");
            $('#titleCheck').attr("title", getWordCount(filterCheck($('#txtMetaTitle').val()), filterCheck(keywordPhrase)));
        } else {
            $('#titleCheck').attr("class", "bad");
        }
        if (filterCheck($('#mtxMetaDescription').val()).indexOf(filterCheck(keywordPhrase)) >= 0) {
            $('#descriptionCheck').attr("class", "good");
        } else {
            $('#descriptionCheck').attr("class", "bad");
        }

        if (filterCheck($('#txtTitle').val()).indexOf(filterCheck(keywordPhrase)) >= 0) {
            $('#headingCheck').attr("class", "good");
        } else {
            $('#headingCheck').attr("class", "bad");
        }
        if (filterCheck($('#spanSlugUrl').text()).indexOf(filterCheck(keywordPhrase)) >= 0) {
            $('#urlCheck').attr("class", "good");
        } else {
            $('#urlCheck').attr("class", "bad");
        }

        if (filterCheck($('#mtxContent').val()).indexOf(filterCheck(keywordPhrase)) >= 0) {
            $('#contentCheck').attr("class", "good");
        } else {
            $('#contentCheck').attr("class", "bad");
        }

        if (filterCheck($('#TagsDiv').text()).indexOf(filterCheck(keywordPhrase)) >= 0) {
            $('#tagCheck').attr("class", "good");
        } else {
            $('#tagCheck').attr("class", "bad");
        }
    }
}

function getWordCount(haystack, needle) {
    return haystack.split(needle).length - 1;
}

$("#AddTagTextInput").keypress(function (e) {
    if (e.keyCode == 13) {
        e.preventDefault(); 
        $('#AddTagButton').trigger("click");
    }
});

$('#spanSlugUrl').trigger("load");
$('#txtMetaTitle').trigger("load");
$('#txtKeywords').trigger("load");
$('#mtxMetaDescription').trigger("load");
$('#AddTagButton').trigger("load");
$('*[data-maxchars]').trigger("load");