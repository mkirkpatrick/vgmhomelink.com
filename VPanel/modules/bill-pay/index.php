<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 

security::_secureCheck();

if (!isset($_GET['logout'])) {
    $_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php"); ?>

<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-<?= $_WEBCONFIG['MODULE_ICON'] ?>"><?= $_WEBCONFIG['MODULE_NAME'] ?></h1>
        <p class="page-desc"><?= $_WEBCONFIG['MODULE_DESCRIPTION'] ?></p>
    </div>      
</section>

<section class="maincontent">
    <div class="container clearfix">
        <?php
        include_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/modules/modulesNavBar.php');
        $rows = 0;
        $view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

        switch ($view) {
            case 'list' :
                $script = array($_WEBCONFIG['MODULE_FOLDER'] . 'js/list.js');
                include 'list.php';      
                break;
            case 'view' :
                $script = array($_WEBCONFIG['MODULE_FOLDER'] . 'js/view.js');
                include 'view.php';      
                break;                
            case 'export' :
                $script = array($_WEBCONFIG['MODULE_FOLDER'] . 'js/list.js');
                include 'list.php';      
                break;                                    
            default :
                $script = array($_WEBCONFIG['MODULE_FOLDER'] . 'js/list.js');
                include 'list.php';        
        }       
        ?>
    </div>
</section>
<?php include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php"); ?>