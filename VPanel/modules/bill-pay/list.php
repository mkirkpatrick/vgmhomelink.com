<? if (!isset($siteConfig)) die("System Error!"); 

$filterId = isset($_GET['status']) ? $_GET['status'] : "Approved";
$sdate = isset($_GET['sdate']) ? $_GET['sdate'] : date('m/d/Y', strtotime('-3 months')); 
$edate = isset($_GET['edate']) ? $_GET['edate'] : date('m/d/Y'); 
 
switch ($filterId) {
    case "Declined":
    case "Error":
        $filterSQL = "bp_status = 'Error' || bp_status = 'Declined'";
        break;
    case "Archived":
        $filterSQL = "bp_archived = 1";
        break;        
    default:
        $filterSQL = "bp_status = 'Approved'"; 
        break;
}
?> 

<div class="subcontent right last">
    <div class="floatRight forward">
        <label>Filter Payments By:</label><br>
        <select id="cboFilter" name="cboFilter">
            <option value="Approved" <?= $filterId == "Approved" ? "selected" : "" ?>>Approved</option>
            <option value="Declined" <?= $filterId == "Declined" ? "selected" : "" ?>>Declined</option>
            <option value="Archived" <?= $filterId == "Archived" ? "selected" : "" ?>>Archived</option>
        </select>    
    </div>
    <?
    print '<h1>' . $_WEBCONFIG['MODULE_NAME'] . '</h1>';

    if ($form->getNumErrors() > 0) {
        $errors	= $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } 
    else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<div class=\"message success relative\"><h3>" . $_WEBCONFIG['ENTITY_NAME'] . " Added Successfully!</h3></div>\n";
                break;
            case 'updated':
                echo "<div class=\"message success relative\"><h3>" . $_WEBCONFIG['ENTITY_NAME'] . " Updated Successfully!</h3></div>\n";
                break;
            case 'archived':
                echo "<div class=\"message error relative\"><h3>" . $_WEBCONFIG['ENTITY_NAME'] . " Deleted Successfully!</h3></div>\n";
                break;
        }        
        unset($_SESSION['processed']);
    }
    
    if(isset($_GET['sdate']) && isset($_GET['edate']) && !isNullOrEmpty($_GET['sdate']) && !isNullOrEmpty($_GET['edate'])) { 
        $startDate = Database::quote_smart($_GET['sdate']); 
        $endDate = Database::quote_smart($_GET['edate']); 
        $startDate = date('Y-m-d', strtotime($startDate)); 
        $endDate = date('Y-m-d', strtotime($endDate)); 
        $endDate = date('Y-m-d', strtotime('+1 day' . $endDate)); 
        $filterSQL .= " AND bp_date_added BETWEEN '$startDate' AND '$endDate'"; 
    }    

    $sql = "SELECT bp_id, bp_account_first, bp_account_last, bp_account_number, bp_payment_amounts, bp_payment_total, bp_status, bp_date_added 
			FROM tbl_bill_pay
            WHERE $filterSQL
			ORDER BY bp_date_added DESC
			LIMIT 1000";
    $record	= Database::Execute($sql);

    print $record->Count() == 1 ? "<p>There is currently <b>1</b> " . strtolower($_WEBCONFIG['ENTITY_NAME']) . "</p>\n" : "<p>There are currently <b>" . $record->Count() . "</b> " . strtolower($_WEBCONFIG['ENTITY_NAME']) . "s. </p>\n";
    ?>

    <table id="grid">
        <thead> 
            <tr> 
                <th data-field="name">Account Name</th>
				<th data-field="number">Account Number</th>
                <th data-field="amount" style="text-align: center;">Amount</th>
				<th data-field="status">Status</th>
                <th data-field="submitDate">Date</th>
                <th data-field="options" style="text-align: center;">Options</th>
            </tr>
        </thead>
        <tbody>

            <?
            if ($record->Count() > 0) {

                while ($record->MoveNext()) {
                    print '<tr> 
                                <td>
                                    <div align="center">
                                        ' . $record->bp_account_first . ' ' . $record->bp_account_last . '
                                    </div>
                                </td>
								<td>
                                    <div align="center">
                                        ' . $record->bp_account_number . '
                                    </div>
                                </td>
                                <td>
                                    <div align="center">
                                        $' . $record->bp_payment_total . '
                                    </div>
                                </td>
                                <td>
                                    <div align="center">
                                        ' . $record->bp_status . '
                                    </div>
                                </td>
                                <td>' . $record->bp_date_added . '</td>                                                                
                                <td>
                                    <div align="center">
                                        <a href="javascript:viewPayment(' . $record->bp_id . ');" title="View Payment Submission" class="green button-slim">View</a>
                                        <a href="javascript:deletePayment(' . $record->bp_id . ');" title="Delete Payment Submission" class="red button-slim">Archive</a>
                                    </div>
                                </td>
                            </tr>' . PHP_EOL;
                }// end while

            }
            ?>
        </tbody>
    </table>

    <div class="buttons clearfix">
        <a href="javascript:history.back()" class="button blue floatLeft">Back</a>
        <form id="ExportForm" method="get" action="exportData.php">
            <input type="hidden" name="sdate" value="<?= $sdate ?>"  />
            <input type="hidden" name="edate" value="<?= $edate ?>" />
            <input type="hidden" name="fid" value="<?= $filterId ?>" />
            <input class="button silver icon-export floatRight" value="Export Data" type="submit" />
        </form>
    </div>  
    
</div><!--End continue-->

<script type="text/x-kendo-template" id="searchBarTemplate">
	<div class="toolbar floatRight">
		<div>
            <input id="txtSearch" type="text" style="width: 175px" placeholder="Search Grid" />
            <a id="clearTextButton">Clear</a>
        </div>
	</div>
</script>

<script type="text/x-kendo-template" id="dateFilterTemplate">
    <div class="toolbar floatLeft">
        <div>
            <input style="width:150px;" type="text" name="sdate" id="sdate" value="<?= $sdate ?>" /> - <input style="width:150px;" type="text" name="edate" id="edate" value="<?= $edate ?>" />
            <a id="selectDateButton" data-role="button" class="k-button k-button-icontext" role="button" aria-disabled="false" tabindex="0">Submit</a>
        </div>
    </div>    
</script>

<script type="text/javascript">
    $(window).load(function() { 
        $("#cboFilter").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var query = '?view=list&sdate=' + $('#sdate').val() + '&edate=' + $('#edate').val();
                window.location.href = window.location.pathname + query + "&status=" + this.value(); 
            }
        });

        $("#selectDateButton").click(function() {
            var query = '?view=list&status=<?= $filterId ?>&sdate=' + $('#sdate').val() + '&edate=' + $('#edate').val();
            window.location.href = window.location.pathname + query; 
        });
    }); 
</script>
