<?php
if (!isset($siteConfig)) 
    die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

if ($form->getNumErrors() > 0) {
    $hidId			  = $form->value("hidId");
    $txtAdminComments = $form->value("txtName");
} else {
    $sql = "SELECT *
    FROM tbl_bill_pay
    WHERE bp_id = $iNum
    LIMIT 1";
    $record	= Database::Execute($sql);
    $record->MoveNext();

    if ($record->Count() == 0) {
        redirect("index.php?view=list"); 
    }

    $hidId			  = $iNum; 
    $mtxAdminComments = $record->bp_admin_comments; 	
} ?> 

<div class="subcontent last right">

    <form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">
        <input type="hidden" name="actionRun" value="view" />
        <input type="hidden" name="hidId" value="<?= $hidId; ?>" />
		
       	
       	<div class="print-section">
        <?
        print '<h2>View Bill Pay Submission</h2>';
        if ($form->getNumErrors() > 0) {
            $errors    = $form->getErrorArray();
            foreach ($errors as $err) echo $err;
        } 
        else if (isset($_SESSION['processed'])) {
            switch($_SESSION['processed']) {
                case 'added':
                    echo "<div class=\"message success relative\"><h3>" . $_WEBCONFIG['ENTITY_NAME'] . " Added Successfully!</h3></div>\n";
                    break;
                case 'updated':
                    echo "<div class=\"message success relative\"><h3>" . $_WEBCONFIG['ENTITY_NAME'] . " Updated Successfully!</h3></div>\n";
                    break;
                case 'archived':
                    echo "<div class=\"message error relative\"><h3>" . $_WEBCONFIG['ENTITY_NAME'] . " Deleted Successfully!</h3></div>\n";
                    break;
            }        
            unset($_SESSION['processed']);
        }
        ?>

        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Date Submitted</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= date("m/d/Y @ g:i A", strtotime($record->bp_date_added)) ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Agreed to Terms</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_agree_to_terms ?></td>
            </tr>            
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Payment Status</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_status ?></td>
            </tr>
            <? if($record->bp_status != 'Approved') {
                $reason = explode("|", $record->bp_raw_cc_response);?>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Payment Failed Reason</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><?= isset($reason[3]) ? $reason[3] : "Reason Not Provided" ?></td>
                </tr>            
                <? } ?>            
        </table>
		</div>
       
       	<div class="print-section">
        <h2>Account Information</h2>
        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Account Name</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_account_first . ' ' . $record->bp_account_middle . ' ' . $record->bp_account_last ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Account Company</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_account_company ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Account Address</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_account_address . ' ' . $record->bp_account_address_2 ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Account City</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_account_city ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Account State</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_account_state ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Account Zip Code</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_account_zip ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Account Phone</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_account_phone ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Account Email</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_account_email ?></td>
            </tr>
        </table>
		</div>
		
       	<div class="print-section">
        <h2>Billing Information</h2>
        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Name</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_billing_first . ' ' . $record->bp_billing_last ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Address</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_billing_address . ' ' . $record->bp_billing_address2 ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>City</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_billing_city ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>State</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_billing_state ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Zip Code</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_billing_zip ?></td>
            </tr>
        </table>
		</div>        
		
       	<div class="print-section">
        <h2>Payment Information</h2>
        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Account Number</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_account_number ?></td>
            </tr>
            
            <?
                $tempInv = explode('|', $record->bp_invoice_numbers);
                $invDisplay = '';
                foreach ($tempInv as $key => $value) {
                    $invDisplay .= strlen($value) > 0 ? "$value, " : ' ,';
                }
                $invDisplay = rtrim($invDisplay, ', ');
            ?>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Invoice Number(s)</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $invDisplay ?></td>
            </tr>
            
            <?
                $tempPay = explode('|', $record->bp_payment_amounts);
                $payDisplay = '';
                foreach ($tempPay as $key => $value) {
                    $payDisplay .= "$$value, ";
                }
                $payDisplay = rtrim($payDisplay, ', ');
            ?>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Payment Amounts</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $payDisplay ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Payment Total</b></label></td>
                <td style="text-align: left; vertical-align: top;">$<?= $record->bp_payment_total ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Payment Type</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_payment_type ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Credit Card #</b></label></td>
                <td style="text-align: left; vertical-align: top;">XXXX XXXX XXXX <?= $record->bp_payment_card ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Payment Confirmation #</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= strlen($record->bp_payment_confirmation_number) > 0 ? $record->bp_payment_confirmation_number : "-" ?></td>
            </tr>                        
        </table>
		</div>
       
       	<div class="print-section">
        <h2>Recurring Payment Details</h2>
        <table class="grid-display">            
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="185" style="text-align: right; vertical-align: middle;"><label><b>Recurring Payment Setup?</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->bp_recurring_payment_setup ?></td>
            </tr> 
            <? if($record->bp_recurring_payment_setup == 'Yes') { ?>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Contact Phone</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><?= $record->bp_recurring_payment_phone ?></td>
                </tr>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td width="175" style="text-align: right; vertical-align: middle;"><label><b>Contact Email</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><?= $record->bp_recurring_payment_email ?></td>
                </tr>
                <? } ?>
        </table>
		</div>
       
        <h2>Admin Comments</h2>
        <table class="grid-display"> 
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: top;"><label for="mtxAdminName"><b>Admin Comments</b></label></td>
                <td style="text-align: left; vertical-align: top;"><textarea rows="3" cols="70" name="mtxAdminComments" id="mtxAdminComments"><?= $mtxAdminComments; ?></textarea>
                    <?= $form->error("mtxAdminComments");?>
                </td>
            </tr>
        </table>

        <div class="buttons clearfix">
            <a href="javascript:history.back()" class="floatLeft button silver">Back</a>
			<a href="javascript:window.print()" class="floatLeft button silver">Print</a>
            <input name="btnModify" class="button green floatRight" type="submit" id="btnModify" value="Save Comments">
        </div>
    </form>
</div><!--End continue-->
