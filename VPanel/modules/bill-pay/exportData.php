<?php
define('SKIP_ERROR_HANDLER', "N");
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/PHPExcel/Classes/PHPExcel.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/PHPExcel/Classes/PHPExcel/IOFactory.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 

security::_secureCheck();

ini_set('max_execution_time', 480); 
set_time_limit(480);

$debugMode = false; /* Set to true to show error messages */
$filterId  = isset($_GET['status']) ? $_GET['status'] : "Approved";
$sdate     = isset($_GET['sdate']) ? $_GET['sdate'] : date('m/d/Y', strtotime('-3 months')); 
$edate     = isset($_GET['edate']) ? $_GET['edate'] : date('m/d/Y'); 

switch ($filterId) {
    case "Declined":
    case "Error":
        $subject    = "Bill Pay Submissions ~ Declined Only";
        $filterSQL = "bp_status = 'Error' || bp_status = 'Declined'";
        break;
    case "Archived":
        $subject    = "Bill Pay Submissions ~ Archived Only";
        $filterSQL = "bp_archived = 1";
        break;        
    default:
        $subject    = "Bill Pay Submissions ~ Approved Only";
        $filterSQL = "bp_status = 'Approved'"; 
        break;
}

if(isset($_GET['sdate']) && isset($_GET['edate']) && !isNullOrEmpty($_GET['sdate']) && !isNullOrEmpty($_GET['edate'])) { 
    $startDate = Database::quote_smart($_GET['sdate']); 
    $endDate = Database::quote_smart($_GET['edate']); 
    $startDate = date('Y-m-d', strtotime($startDate)); 
    $endDate = date('Y-m-d', strtotime($endDate)); 
    $endDate = date('Y-m-d', strtotime('+1 day' . $endDate)); 
    $filterSQL .= " AND bp_date_added BETWEEN '$startDate' AND '$endDate'"; 
    $dateSpan = date("m/d/Y", strtotime($startDate)) . " - " . date("m/d/Y", strtotime($endDate));
} else {
    $dateSpan = date("m/d/Y");    
}  

$sql = "SELECT * 
        FROM tbl_bill_pay
        WHERE $filterSQL
        ORDER BY bp_date_added DESC";
$record  = Database::Execute($sql);

if ($record->Count() == 0) {
    throw new Exception("Unable to find record in the database");
}

if(!$debugMode) { 
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");;
    header("Content-Disposition: attachment;filename={$_WEBCONFIG['SITE_DISPLAY_URL']}.xlsx");
    header("Content-Transfer-Encoding: binary "); 
}

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("VPanel")
->setLastModifiedBy("VPanel")
->setTitle("Norco Online Bill Pay")
->setSubject($subject)
->setDescription($subject . " document for Office 2007 XLSX.");
$objPHPExcel->setActiveSheetIndex(0);

// Create first header
$objPHPExcel->getActiveSheet()->setCellValue('A1', $subject . ' ~ ' . $dateSpan);
$objPHPExcel->getActiveSheet()->getStyle('J1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFill()->getStartColor()->setARGB('00853E');
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setName('Candara');
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('J1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->mergeCells('A1:AA1');

$arHeader = array("Date Submitted",
                    "Account Name",
                    "Account Company",
                    "Account Address",
                    "Account City",
                    "Account State",
                    "Account Zip Code",
                    "Account Phone",
                    "Account Email",
                    "Billing Name",
                    "Billing Address",
                    "Billing City",
                    "Billing State",
                    "Billing Zip Code",
                    "Account Number",
                    "Invoice Number(s)",
                    "Payment Amount",
                    "Payment Type",
                    "Credit Card",
                    "Payment Confirmation #",
                    "Recurring Payment Setup",
                    "Recurring Contact Phone",
                    "Recurring Contact Email",
                    "Agreed to Terms",
                    "Payment Status",
                    "Admin Comments");
$hCtr = sizeof($arHeader);
$row  = 2;
for ($col=0; $col<$hCtr; $col++) {
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $arHeader[$col]);
    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array(
        'font' => array('bold' => true),
        'color' => array('argb' => '094C7D'),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
    ));
}//end for

$arData        = array_flip($arHeader);

if ($record->Count() > 0) {
    $col = 0;

    while ($record->MoveNext()) {
        $col = 0;
        $row++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, date("m/d/Y @ g:i A", strtotime($record->bp_date_added)));
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_account_first . ' ' . $record->bp_account_middle . ' ' . $record->bp_account_last);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_account_company);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_account_address . ' ' . $record->bp_account_address_2);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_account_city);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_account_state);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;
                        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_account_zip);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_account_phone);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_account_email);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_billing_first . ' ' . $record->bp_billing_last);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_billing_address . ' ' . $record->bp_billing_address2);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_billing_city);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;
                                                
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_billing_state);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_billing_zip);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_account_number);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_invoice_numbers);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_payment_amounts);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_payment_total);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_payment_type);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "XXXX XXXX XXXX " . $record->bp_payment_card);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_payment_confirmation_number);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;  
        
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_recurring_payment_setup);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_recurring_payment_phone);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;
        
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_recurring_payment_email);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $record->bp_agree_to_terms);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row,$record->bp_status);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
        $col++;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, preg_replace("#<br\s*/?>#i", "\n", $record->bp_admin_comments));
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));        
        $col++;                                                                       
    }//end while

    // Auto Size all columns
    for ($col=0; $col<$hCtr; $col++) {
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
    }

    // Freeze panes
    $objPHPExcel->getActiveSheet()->freezePane('A3');
} else {
    // No records founc
    $objPHPExcel->getActiveSheet()->setCellValue('B3', "THERE'S NO INFORMATION AVAILABLE AT THIS MOMENT!");
}
$objPHPExcel->getActiveSheet()->setTitle("Norco Inc Online Bill Pay");
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>