<?php
class process extends Database { 

	private $record;
	private $table;
	public $params;

	//------------------------------
	public function __construct($table) {
	//------------------------------
		parent::__construct();
		$this->table		= $table;
	}

	//------------------------------
	public function _view() {
	//------------------------------
		global $form;
		
		$iNum = (int)$_POST['hidId'];
		$this->record = Repository::getByID($this->table, $iNum); 
        
		$fieldName	= "mtxAdminComments";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		$this->record->bp_admin_comments = $fieldValue;

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			$this->params = "view=view&id=$iNum";
		} else {
			// No Errors
			$this->record->update();
			$_SESSION['processed'] = 'updated';
			$this->params = "view=view&id=$iNum";
		}
	}
	
	//------------------------------
	public function _archive() {
	//------------------------------
		$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
		Database::ExecuteRaw("UPDATE " . $this->table . " SET `bp_archived` = 1 WHERE `bp_id` = $iNum LIMIT 1;");	
		$_SESSION['processed'] = 'archived';
		$this->params = "view=list";
	}

	//------------------------------
	public function _unarchive() {
	//------------------------------
		$iNum	 = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
		$formId = isset($_GET['fId']) && (int)$_GET['fId'] > 0 ? (int)$_GET['fId'] : 0;

		Database::ExecuteRaw("UPDATE `tbl_form_submission` SET `fs_archived` = 0 WHERE `fs_id` = $iNum LIMIT 1;");
						
		$_SESSION['processed'] = 'unarchived';
		$this->params = "view=list&id=" . $formId;
	}

};
?>