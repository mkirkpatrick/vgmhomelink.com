<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once 'class.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php"); 

$p = new process($_WEBCONFIG['MODULE_TABLE']);
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {

		
	case 'view' :
		$p->_view();
		break;
		
	case 'archive' :
		$p->_archive();
		break;

	case 'unarchive' :
		$p->_unarchive();
		break;
		
	default :
		throw new Exception("An unknown action executed!");
}
redirect("index.php?{$p->params}");
?>