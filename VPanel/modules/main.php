<div class="fullcontent right">

    <h2>Site Modules</h2>
    <?php 
        $dir = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "modules/");
        if (is_dir($dir)) {  
            if ($dh = opendir($dir)) {  
                $count = 0; 
                while (($directory = readdir($dh)) !== false) {  
                    $row = $count % 2 ? "odd" : "even"; 
                    $path =  $dir . "/" . $directory; 
                    if(filetype($path) == 'dir' && $directory != ".." && $directory != ".") { 
                        if(file_exists($path . "/web.config")) { 
                            $settings = getWebConfigSettings($path);
                            if($settings['MODULE_NAME'] != 'Form Builder') {  
                            ?>
                                <div class="clearfix assetOverview <?= $row ?>">
                                    <div class="btn_view">
                                        <?php if(isset($settings['MODULE_INSTALL_FOLDER']) && file_exists($_SERVER['DOCUMENT_ROOT'] . $settings['MODULE_INSTALL_FOLDER'])) { ?>
                                            <a href="<?php echo $settings['MODULE_INSTALL_FOLDER'] . 'install.php' ?>" class="button yellow">Install</a>
                                        <? } ?>
                                        <a href="<?php echo $_WEBCONFIG['VPANEL_PATH']; ?>modules/<?= $directory ?>/" class="button blue">Manage</a>
                                    </div>
                                        <h2><?= $settings['MODULE_NAME'] ?></h2>
                                        <p><?= $settings['MODULE_DESCRIPTION'] ?>.</p>
                                </div>
                                <?php 
                                $count++; 
                            }
                        }
                    }
                }  
                closedir($dh);  
            }  
        }  
    ?>
</div>