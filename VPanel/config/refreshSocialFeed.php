<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

$cache_file = $_SERVER['DOCUMENT_ROOT'] . '/uploads/cache/Social-Feeds.cache'; 

if (file_exists($cache_file)) {
   unlink($cache_file); 
} 

header("Location: {$_SERVER['HTTP_REFERER']}");
exit;
?>