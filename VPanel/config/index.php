<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 

security::_secureCheck();
security::_webmasterCheck();

if (!isset($_GET['logout'])) 
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

print '<section class="title">
            <div class="container clearfix">
                <h1 class="page-title icon-' . $_WEBCONFIG['COMPONET_ICON'] . '">' . $_WEBCONFIG['COMPONET_NAME'] . '</h1>
                <p class="page-desc">' . $_WEBCONFIG['COMPONET_DESCRIPTION'] . '</p>
            </div>
        </section>';
print '<section class="maincontent"><div class="container clearfix">';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/config/configNavBar.php');


include "form.php";

print '</div></section>';

$script	= array('jquery/pages/config.js');
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>