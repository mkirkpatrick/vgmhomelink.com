<?php
class process extends Database { 

	//------------------------------
	public function __construct() {
	//------------------------------
		parent::__construct();
		self::_modify();
	}

	//------------------------------
	private function _modify() {
	//------------------------------
		global $_WEBCONFIG;

		foreach($_POST as $name => $value) {	
			$configName = str_replace("txt", "", $name); 
			$entity = Repository::GetByFieldName($_WEBCONFIG['COMPONET_TABLE'], "name", $configName);
			if(!is_null($entity)) { 
				$entity->value = $value;
				Repository::Save($entity);
			}
		}

		$_SESSION['processed'] = 'updated';
	}
};
?>