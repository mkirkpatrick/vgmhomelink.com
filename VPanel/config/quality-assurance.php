<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 


security::_secureCheck();

if(!UserManager::isWebmaster()) {
    redirect($_WEBCONFIG['VPANEL_PATH']);
}

if (!isset($_GET['logout'])) 
    $_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];


if($_WEBCONFIG['SITE_TYPE'] == 'BANK') {
    $siteType = 'Bank';
} elseif($_WEBCONFIG['SITE_TYPE'] == 'CUSTOM') {
    $siteType = 'Custom';    
} elseif($_WEBCONFIG['SITE_TYPE'] == 'ECOMMERCE' || $_WEBCONFIG['SITE_TYPE'] == 'CATALOG') {
    $siteType = 'Custom';    
} elseif($_WEBCONFIG['SITE_TYPE'] == 'LANDING') {
    $siteType = 'Landing Page';    
} else {
    $siteType = 'Custom';    
}    

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php"); ?>

<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-battery">Quality Assurance</h1>
        <p class="page-desc">The Key To Our Success</p>
    </div>
</section>

<section class="maincontent">
    <div class="container clearfix">
        <aside class="left sidebar">
            <?php include_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], 'includes/inc_home_sidebar.php'); ?>
        </aside>
        <div class="right home clearfix subcontent last">
            <?= curl_get_contents("https://vguard.forbin.com/api/quality/getChecklist.php?token=CmwGRp9o5a6df6592Byd2hms9Q33&type=$siteType&site=" . urlencode($_WEBCONFIG['SITE_URL']) . "&username=" . urlencode($_SESSION['user_name'])); ?>
        </div>
    </div>
</section>
<? 
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>