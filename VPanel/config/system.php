<?
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 

security::_secureCheck();
security::_webmasterCheck();

if (!isset($_GET['logout'])) 
    $_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");
?>

<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-database">System Information</h1>
        <p class="page-desc">A little information about the system.</p>
    </div>
</section>

<section class="maincontent">
    <div class="container clearfix">
        <aside class="left sidebar">
            <?php include_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], 'includes/inc_home_sidebar.php'); ?>
        </aside>
        <div class="right home clearfix subcontent last">
            <ul class="system-info">
                <li>
                    <h3>Site Information</h3>
                    <ul>
						<li>Site Type: <strong><?= $_WEBCONFIG['SITE_TYPE'] ?></strong></li>
                        <li>Site Status: <strong><?= $_WEBCONFIG['SITE_STATUS'] ?></strong></li>
						<li>Site Access Username: <strong><?= $_WEBCONFIG['SITE_ACCESS_USER'] ?></strong></li>
						<li>Site Access Password: <strong><?= $_WEBCONFIG['SITE_ACCESS_PASSWORD'] ?></strong></li>
						<li>PageBuilder Version: <strong><?= $_WEBCONFIG['PAGEBUILDER_VERSION'] ?></strong></li>
						<li>PageBuilder Nav Limit: <strong><?= $_WEBCONFIG['MAIN_NAV_LIMIT'] ?></strong></li>
						<li>PageBuilder Page Limit: <strong><?= $_WEBCONFIG['PAGEBUILDER_PAGE_LIMIT'] ?></strong></li>
						<li>Has Member Logins: <strong><?= $_WEBCONFIG['HAS_MEMBER_LOGINS'] == "true" ? "Yes" : "No" ?></strong></li>
                    </ul>
                </li>
				
                <li>
                    <h3>VPanel Information</h3>
                    <ul>
                        <li>VPanel Version: <strong><?= $_WEBCONFIG['VPANEL_VERSION'] ?></strong></li>
                        <li>Culture Code: <strong><?= $_WEBCONFIG['CULTURE_CODE'] ?></strong></li>
                        <li>Currency: <strong><?= $_WEBCONFIG['CURRENCY'] ?></strong></li>
                        <li>Timezone: <strong><?= $_WEBCONFIG['TIMEZONE'] ?></strong></li>
                        <li>Debug Setting: <strong><?= $_WEBCONFIG['DEBUG_SETTING'] ?></strong></li>
                        <li>Debug Warning Level: <strong><?= $_WEBCONFIG['DEBUG_WARNING_LEVEL'] ?></strong></li>
                        <li>Developer Email: <strong><?= $_WEBCONFIG['DEVELOPER_EMAIL'] ?></strong></li>
						<li>Minimum Password Length: <strong><?= $_WEBCONFIG['MIN_PASSWORD_LENGTH'] ?> Characters</strong></li>
						<li>Max Password Attempts: <strong><?= $_WEBCONFIG['MAX_INVALID_PASSWORD_ATTEMPTS'] ?></strong></li>
						<li>Password Expiration: <strong><?= $_WEBCONFIG['PASSWORD_EXPIRE_DAYS'] ?> Days</strong></li>
						<li>Webmaster Login Enabled: <strong><?= $_WEBCONFIG['ENABLE_WEBMASTER_LOGIN'] == "true" ? "Yes" : "No" ?></strong></li>
                    </ul>
                </li>
				
                <li>
                    <h3>User Information</h3>
                    <ul>
                        <li>Public IP Address: <strong><a target="_blank" title="Get more information on this address" href="http://whois.domaintools.com/<?= $_SERVER['HTTP_CLIENT_IP'] ?>"><?= $_SERVER['HTTP_CLIENT_IP'] ?></a></strong></li>
                        <li>User Agent: <strong><?= filter_var( $_SERVER['HTTP_USER_AGENT'], FILTER_SANITIZE_STRING ); ?></strong></li>
                        <li>Browser Compression Supported: <strong><?= filter_var( $_SERVER['HTTP_ACCEPT_ENCODING'], FILTER_SANITIZE_STRING ); ?></strong></li>
                    </ul>
                </li>

                <li>
                <h3>File System Information</h3>
                <ul>
                    <li>Website Root Folder: <strong><?= $_SERVER['HTTP_HOST']; ?></strong></li>
                    <li>Document Root Path: <strong><?= filter_var( $_SERVER['DOCUMENT_ROOT'], FILTER_SANITIZE_STRING ); ?></strong></li>
                </ul>

                <li>

                    <?php
                    $sql = "SELECT table_schema 'DB', SUM( data_length + index_length ) / 1024 / 1024 'Size' 
                    FROM information_schema.TABLES
                    WHERE table_schema = '" . DATABASE_NAME . "';";
                    $sqlSize = Database::Execute($sql);
                    if($sqlSize->Count() > 0) {
                        $sqlSize->MoveNext();
                        $dbSize = number_format($sqlSize->Size, 5) . ' MB';
                    }
                    else {
                        $dbSize = "Error Getting DB Size.";
                    } 
                    ?>    

                    <h3>Database Information</h3>
                    <ul>
                        <li>MySQL Database Version: <strong><?= Database::get_version() ?></strong></li>
                        <li>MySQL Client Version: <strong><?= mysqli_get_client_info(); ?></strong></li>
                        <li>Database Host: <strong><?= DATABASE_HOST; ?></strong></li>
                        <li>Database Name: <strong><?= DATABASE_NAME; ?></strong></li>
                        <li>Database Size: <strong><?= $dbSize; ?></strong></li>
                    </ul>
                </li>

                <li>
                    <h3>Server Information</h3>
                    <?php $server_addr = array_key_exists('SERVER_ADDR',$_SERVER) ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR']; ?>
                    <ul>
                        <li>Server / Website IP Address: <strong><a target="_blank" title="Get more information on this address" href="http://whois.domaintools.com/<?= $server_addr; ?>"><?= $server_addr; ?></a></strong></li>
                        <li>Server Type: <strong><?= filter_var( filter_var( $_SERVER['SERVER_SOFTWARE'], FILTER_SANITIZE_STRING ), FILTER_SANITIZE_STRING ); ?></strong></li>
                        <li>Operating System: <strong><?= PHP_OS; ?></strong></li>
                        <li>Server Name: <strong><?= isset($_SERVER['COMPUTERNAME']) ? $_SERVER['COMPUTERNAME'] : 'Unknown'; ?></strong></li>
                        <li>App Pool: <strong><?= isset($_SERVER['APP_POOL_ID']) ? $_SERVER['APP_POOL_ID'] : 'N/A'; ?></strong></li>
                        <li>Processor: <strong><?= isset($_SERVER['PROCESSOR_IDENTIFIER']) ? $_SERVER['PROCESSOR_IDENTIFIER'] : 'N/A'; ?></strong>
                        <li>Processor Architecture: <strong><?= isset($_SERVER['PROCESSOR_ARCHITECTURE']) ? $_SERVER['PROCESSOR_ARCHITECTURE'] : 'N/A'; ?></strong></li>
                        <li>Number of Processors: <strong><?= isset($_SERVER['NUMBER_OF_PROCESSORS']) ? $_SERVER['NUMBER_OF_PROCESSORS'] : 'N/A'; ?></strong>
                    </ul>
                </li>

                <li>
                    <h3>PHP Information</h3>
                    <ul>
                        <li>PHP Version: <strong><?= PHP_VERSION; ?></strong></li>
                        <li>PHP Memory Usage: <strong><?= round(memory_get_usage() / 1024 / 1024, 2);?> MB</strong> </li>
                        <?php 
                        if ( ini_get( 'memory_limit' ) ) {
                            $memory_limit = filter_var( ini_get( 'memory_limit' ), FILTER_SANITIZE_STRING ); 
                        } else {
                            $memory_limit = 'N/A';
                        }
                        ?>
                        <li>PHP Memory Limit: <strong><?= $memory_limit; ?></strong></li>
                        <?php 
                        if ( ini_get( 'upload_max_filesize' ) ) {
                            $upload_max = filter_var( ini_get( 'upload_max_filesize' ), FILTER_SANITIZE_STRING );
                        } else 	{
                            $upload_max = 'N/A';
                        }
                        ?>
                        <li>PHP Max Upload Size: <strong><?= $upload_max; ?></strong></li>
                        <?php 
                        if ( ini_get( 'post_max_size' ) ) {
                            $post_max = filter_var( ini_get( 'post_max_size' ), FILTER_SANITIZE_STRING );
                        } else {
                            $post_max = 'N/A';
                        }
                        ?>
                        <li>PHP Max Post Size: <strong><?= $post_max; ?></strong></li>
                        <?php 
                        if ( ini_get( 'safe_mode' ) ) {
                            $safe_mode = 'On';
                        } else {
                            $safe_mode = 'Off';
                        }
                        ?>
                        <li>PHP Safe Mode: <strong><?= $safe_mode; ?></strong></li>
                        <?php 
                        if ( ini_get( 'allow_url_fopen' ) ) {
                            $allow_url_fopen = 'On';
                        } else {
                            $allow_url_fopen = 'Off';
                        }
                        ?>
                        <li>PHP Allow URL fopen: <strong><?= $allow_url_fopen; ?></strong></li>
                        <?php 
                        if ( ini_get( 'allow_url_include' ) ) {
                            $allow_url_include = 'On';
                        } else {
                            $allow_url_include = 'Off';
                        }
                        ?>
                        <li>PHP Allow URL Include: <strong><?= $allow_url_include; ?></strong></li>
                        <?php 
                        if ( ini_get( 'display_errors' ) ) {
                            $display_errors = 'On';
                        } else {
                            $display_errors = 'Off';
                        }
                        ?>
                        <li>PHP Display Errors: <strong><?= $display_errors; ?></strong></li>
                        <?php 
                        if ( ini_get( 'display_startup_errors' ) ) {
                            $display_startup_errors = 'On';
                        } else {
                            $display_startup_errors = 'Off';
                        }
                        ?>
                        <li>PHP Display Startup Errors: <strong><?= $display_startup_errors; ?></strong></li>
                        <?php 
                        if ( ini_get( 'expose_php' ) ) {
                            $expose_php = 'On';
                        } else {
                            $expose_php = 'Off';
                        }
                        ?>
                        <li>PHP Expose PHP: <strong><?= $expose_php; ?></strong></li>
                        <?php 
                        if ( ini_get( 'register_globals' ) ) {
                            $register_globals = 'On';
                        } else {
                            $register_globals = 'Off';
                        }
                        ?>
                        <li>PHP Register Globals: <strong><?= $register_globals; ?></strong></li>
                        <?php 
                        if ( ini_get( 'max_execution_time' ) ) {
                            $max_execute = ini_get( 'max_execution_time' );
                        } else {
                            $max_execute = 'N/A';
                        }
                        ?>
                        <li>PHP Max Script Execution Time: <strong><?= $max_execute; ?> Seconds</strong></li>
                        <?php 
                        if ( ini_get( 'magic_quotes_gpc' ) ) {
                            $magic_quotes_gpc = 'On';
                        } else {
                            $magic_quotes_gpc = 'Off';
                        }
                        ?>
                        <li>PHP Magic Quotes GPC: <strong><?= $magic_quotes_gpc; ?></strong></li>
                        <?php 
                        if ( ini_get( 'open_basedir' ) ) {
                            $open_basedir = 'On';
                        } else {
                            $open_basedir = 'Off';
                        }
                        ?>
                        <li>PHP open_basedir: <strong><?= $open_basedir; ?></strong></li>
                        <?php 
                        if ( is_callable( 'xml_parser_create' ) ) {
                            $xml = 'Yes';
                        } else {
                            $xml = 'No';
                        }
                        ?>
                        <li>PHP XML Support: <strong><?= $xml; ?></strong></li>
                        <?php 
                        if ( is_callable( 'iptcparse' ) ) {
                            $iptc = 'Yes';
                        } else {
                            $iptc = 'No';
                        }
                        ?>
                        <li>PHP IPTC Support: <strong><?= $iptc; ?></strong></li>
                        <?php 
                        if ( is_callable( 'exif_read_data' ) ) {
                            $exif = 'Yes' . " ( V" . substr(phpversion( 'exif' ),0,4) . ")" ;
                        } else {
                            $exif = 'No';
                        }
                        ?>
                        <li>PHP Exif Support: <strong><?= $exif; ?></strong></li>
                    </ul>
                </li>
        </ul> </div>
    </div>
</section><?php 
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>