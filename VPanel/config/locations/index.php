<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 

security::_secureCheck(CONTENT_AUTHOR_ROLE, CONTENT_PUBLISHER_ROLE);
	
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

print '<section class="title">
            <div class="container clearfix">
                <h1 class="page-title icon-' . $_WEBCONFIG['COMPONET_LOCATION_ICON'] . '">' . $_WEBCONFIG['COMPONET_LOCATION_NAME'] . '</h1>
                <p class="page-desc">' . $_WEBCONFIG['COMPONET_LOCATION_DESCRIPTION'] . '</p>
            </div>
        </section>';
print '<section class="maincontent"><div class="container clearfix">';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/config/locations/locationsNavBar.php');

$rows = 0; 
$uploadFolder = $_WEBCONFIG['UPLOAD_FOLDER']; 

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'add' :
	case 'modify' :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/form.js');
		include 'add-modify.php';		
		break;

	case 'sort' :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/sort.js');
		include 'sort.php';		
		break;
		
	case 'photo' :
		$script	= array('jquery/jquery.imgareaselect.min.js', $_WEBCONFIG['COMPONET_FOLDER'] . 'js/photo.js');
		include 'photo.php';		
		break;

	default :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/list.js');
		include 'list.php';		
}

print '</div></section>';

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>