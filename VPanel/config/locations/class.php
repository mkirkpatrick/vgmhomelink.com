<?php
class Process extends Database {

    private $record;
    private $table;
    private $upload_dir;
    public $params;

    //------------------------------
    public function __construct($table, $upload_dir) {
    //------------------------------
        global $_WEBCONFIG;
        parent::__construct();
        $this->table        = $table;
        $this->upload_dir    = $upload_dir;
    }

    //------------------------------
    public function _add() {
    //------------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=add";
        } else {
            // No Errors
            Repository::Save($this->record);
            $iNum = Database::getInsertID();
            $_SESSION['processed'] = 'added';
            Security::_addAuditEvent("Location Added", "Success", "Added --'" . $this->record->location_title . "' (" . $iNum .")", $_SESSION['user_username']);
            $this->params = "view=list";
        }
    }

    //------------------------------
    public function _modify() {
    //------------------------------
        global $_WEBCONFIG, $form;
        $iNum = (int)$_POST['hidId'];
        $this->record = Repository::GetById($this->table, $iNum);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=modify";
        } else {
            // No Errors
            Repository::Save($this->record);
            $_SESSION['processed'] = 'updated';
            Security::_addAuditEvent("Location Modified", "Success", "Modified -- '" . $this->record->location_title . "' (" . $this->record->location_id .")", $_SESSION['user_username']);
            $this->params = "view=list";
        }
    }

    //------------------------------
    public function _delete() {
    //------------------------------
        $iNum    = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

        $res = self::_deleteImage($iNum);

        $this->record = Repository::GetById($this->table, $iNum);
        Security::_addAuditEvent("Location Deleted", "Success", "Deleted -- '" . $this->record->location_title . "' (" . $this->record->location_id .")", $_SESSION['user_username']);
        Repository::Delete($this->record);
        $_SESSION['processed'] = 'deleted';
        $this->params = "view=list";
    }

    //------------------------------
    public function _sort() {
    //------------------------------
        $indexCounter     = 1;
        $updRecordsArray = $_POST['recordsArray'];

        foreach($updRecordsArray as $recordIDValue) {
            $sql = "UPDATE {$this->table} SET location_sort_id = $indexCounter WHERE location_id = $recordIDValue";
            $res = Database::ExecuteRaw($sql);
            $indexCounter++;
        }

        print "Your Records Have Been Re-Ordered!";
    }

    //------------------------------
    private function _verify() {
    //------------------------------
        global $_WEBCONFIG, $form;

        $fieldName    = "txtName";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Title is required.");
        } else {
            $this->record->location_title = $fieldValue;
        }

        $fieldName    = "txtAddress";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->location_address = '';
        } else {
            $this->record->location_address = $fieldValue;
        }

        $fieldName    = "txtCity";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->location_city = '';
        } else {
            $this->record->location_city = $fieldValue;
        }

        $fieldName    = "cboState";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->location_state = '';
        } else {
            $this->record->location_state = $fieldValue;
        }

        $fieldName    = "txtZip";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->location_postal = '';
        } else if(!Validate::isValidZipCode($fieldValue)) {
            $form->setError($fieldName, "* Valid Zip Code is required.");
        } else {
            $this->record->location_postal = $fieldValue;
        }

        $fieldName    = "txtMailingAddress";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if(!isNullOrEmpty($fieldValue)) {
            $this->record->location_mailing_address = $fieldValue;
        }

        $fieldName    = "txtMailingCity";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if(!isNullOrEmpty($fieldValue)) {
            $this->record->location_mailing_city = $fieldValue;
        }

        $fieldName    = "cboMailingState";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if(!isNullOrEmpty($fieldValue)) {
            $this->record->location_mailing_state = $fieldValue;
        }

        $fieldName    = "txtMailingZip";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if(!isNullOrEmpty($fieldValue) && !Validate::isValidZipCode($fieldValue)) {
            $form->setError($fieldName, "* Valid Zip Code is required.");
        } else {
            $this->record->location_mailing_postal = $fieldValue;
        }

        $fieldName    = "txtPhone";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if(!isNullOrEmpty($fieldValue) && !Validate::isValidPhone($fieldValue)) {
            $form->setError($fieldName, "* Valid Phone Code is required.");
        } else {
            $this->record->location_phone = $fieldValue;
        }

        $fieldName    = "txtFax";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        $this->record->location_fax = $fieldValue;

        $fieldName    = "txtTollFree";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if(!isNullOrEmpty($fieldValue) && !Validate::isValidPhone($fieldValue)) {
            $form->setError($fieldName, "* Valid TollFree Code is required.");
        } else {
            $this->record->location_toll_free = $fieldValue;
        }

        $fieldName    = "txtWebsite";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if(!isNullOrEmpty($fieldValue) && !Validate::isValidUrl($fieldValue)) {
            $form->setError($fieldName, "* Valid Website is required.");
        } else {
            $this->record->location_website = $fieldValue;
        }

        $fieldName    = "txtEmail";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        if(!isNullOrEmpty($fieldValue) && !Validate::isValidEmail($fieldValue)) {
            $form->setError($fieldName, "* Valid Email Address is required.");
        } else {
            $this->record->location_email = $fieldValue;
        }

        $fieldName    = "mtxHours";
        $fieldValue    = isset($_POST[$fieldName]) ? cleanHTML(strip_tags($_POST[$fieldName], HTML_FORMATTING_TAGS)) : null;
        $this->record->location_hours = $fieldValue;

        if($_WEBCONFIG['ENABLE_SERVICES'] == 'true') {
            $fieldName = "cboServices";
            if(isset($_POST[$fieldName])) {
                $services = (array)$_POST[$fieldName];
                if(count($services) > 0) {
                    $this->record->location_services = implode("|", $services);
                }
                else {
                    $this->record->location_services = null;
                }
            }
            elseif ($_WEBCONFIG['SITE_TYPE'] == 'BANK') {
                $this->record->location_services = null;
             }
            else {
                // DO NOTHING
            }
        }

        $fieldName    = "optStatus";
        $fieldValue    = isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Status is required.");
        } else {
            $this->record->location_status = $fieldValue;
        }

        $this->record->site_id = 1;
    }

    //------------------------------
    public function _photo() {
        //------------------------------
        global $_WEBCONFIG, $form;

        $iNum            = (int)$_POST['hidId'];
        $maxWidth        = 550;
        $maxHeight        = GetProportionalHeight($_WEBCONFIG['IMAGE_WIDTH'], $_WEBCONFIG['IMAGE_HEIGHT'], $maxWidth);
        $fieldName        = "fleImage";
        $userfile_size    = $_FILES[$fieldName]['size'];
        $userfile_type    = $_FILES[$fieldName]['type'];
        $filename        = basename($_FILES[$fieldName]['name']);
        $file_ext        = strtolower(substr($filename, strrpos($filename, '.') + 1));

        //Only process if the file is a JPG, PNG or GIF and below the allowed limit
        if ((!empty($_FILES[$fieldName])) && ($_FILES[$fieldName]['error'] == 0)) {
            if(!stristr($_WEBCONFIG['VALID_IMGS'], $file_ext)) {
                $form->setError($fieldName, "Only <strong>" . $_WEBCONFIG['VALID_IMGS'] . "</strong> images accepted for upload");
            }

            //check if the file size is above the allowed limit
            if ($userfile_size > ($_WEBCONFIG['MAX_UPLOAD_FILESIZE_MB'] * 1048576)) {
                $form->setError($fieldName, "Image must be under " . $_WEBCONFIG['MAX_UPLOAD_FILESIZE_MB'] . "MB in size");
            }

        } else {
            $form->setError($fieldName, "Select an image to upload");
        }

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
        } else {
            //Everything is ok, so we can upload the image.
            if (isset($_FILES[$fieldName]['name'])) {
                $imagePath    = "{$iNum}_" . (md5(rand() * time())) . ".{$file_ext}";
                $tempPath = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['UPLOADS_TEMP_DIRECTORY']);
                $options = array('resizeUp' => true, 'jpegQuality' => 100, 'correctPermissions' => true);
                $thumb = PhpThumbFactory::create($_FILES[$fieldName]['tmp_name'], $options);
                $thumb->resize($maxWidth, $maxHeight);
                $thumb->save(filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['UPLOADS_TEMP_DIRECTORY'], $imagePath));
                $_SESSION['UPLOADED_IMAGE'] = $imagePath;
                $_SESSION['processed'] = 'added';
            }
        }

        $this->params = "view=photo&id=$iNum";
    }

    //------------------------------
    public function _thumb() {
        //------------------------------
        global $_WEBCONFIG, $form;

        $iNum            = (int)$_POST['hidId'];
        $image            = $_POST['hidImage'];
        $thumbWidth        = (int)$_POST['hidThumbWidth'];
        $thumbHeight    = (int)$_POST['hidThumbHeight'];
        $x1                = $_POST["x1"];
        $y1                = $_POST["y1"];
        $x2                = $_POST["x2"];
        $y2                = $_POST["y2"];
        $w                = $_POST["w"];
        $h                = $_POST["h"];

        //Image Locations
        $arImage    = explode('.', $image);
        $thumb_name = "{$iNum}_" . (md5(rand() * time())) . "." . $arImage[count($arImage)-1];
        $temp_image_location = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['UPLOADS_TEMP_DIRECTORY'], $image);
        $thumb_image_location = filePathCombine($_SERVER['DOCUMENT_ROOT'], $this->upload_dir, $thumb_name);


        //Scale the image to the thumb_width set above
        $scale     = $thumbWidth / $w;
        $cropped = resizeThumbnailImage($thumb_image_location, $temp_image_location, $w, $h, $x1, $y1, $scale);
        $this->_deleteTempImageFile();
        if (file_exists($cropped)) {
            $sql = "UPDATE {$this->table} SET location_image = '$thumb_name' WHERE location_id = $iNum";
            $res = Database::ExecuteRaw($sql);
        }

        $_SESSION['processed'] = 'updated';
        $this->params = "view=photo&id=$iNum";
    }

    //------------------------------
    public function _deleteImage() {
    //------------------------------
        global $action;

        $deleted = false;
        $iNum = isset($_GET['id']) ? (int) $_GET['id'] : 0;
        $location = Repository::GetById($this->table, $iNum);

        if (!is_null($location) && !isNullOrEmpty($location->location_image)) {
            $deleted = self::_deleteImageFile($location->location_image);
            $location->location_image = null;
            Repository::Save($location);
        }
        if($action == "deleteImage") {
            $_SESSION['processed'] = 'deleted';
            $this->params = "view=photo&id=" . $iNum;
        } else {
            return $deleted;
        }
    }

    //------------------------------
    private function _deleteImageFile($txtImage) {
    //------------------------------
        global $_WEBCONFIG;
        $deleted = false;
        $path = filePathCombine($_SERVER['DOCUMENT_ROOT'], $this->upload_dir, $txtImage);
        if (file_exists($path)) {
            $deleted = unlink($path);
        }

        return $deleted;
    }

    //------------------------------
    public function _deleteTempImageFile() {
    //------------------------------
        global $_WEBCONFIG;
        $deleted = false;
        if(isset($_SESSION['UPLOADED_IMAGE']) && !isNullOrEmpty($_SESSION['UPLOADED_IMAGE'])) {
            $tempImage = $_SESSION['UPLOADED_IMAGE'];
            $path = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['UPLOADS_TEMP_DIRECTORY'], $tempImage);
            if (file_exists($path)) {
                $deleted = unlink($path);
            }
            unset($_SESSION['UPLOADED_IMAGE']);
        }
        $this->params = "view=photo";
        return $deleted;
    }

};
?>