// JavaScript Document
$(function() {

	$("#contentOrder ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
		var order = $(this).sortable("serialize") + '&actionRun=sort'; 
		$.post("process.php", order, function(theResponse){
			addNotify(theResponse);
		}); 															 
	}								  
	});

	$("#cboFilter").change(function() { 
		var id = $("#cboFilter").val();

		if (id == "") {
			window.location.href = 'index.php?view=sort'; 
		} else {
			window.location.href = 'index.php?view=sort&catId=' + id;
		}
	});

});