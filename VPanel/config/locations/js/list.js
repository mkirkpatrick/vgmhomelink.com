$(document).ready(function () {
    var kendoPageSize = 10;
    $("#grid").kendoGrid({
        dataSource: { pageSize: kendoPageSize },
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",
                    eq: "Exact Match",
                    startswith: "Starts With",
                    endswith: "Ends With",
                    doesnotcontain: "Does Not Contain",
                    neq: "Does Not Match"
                }
            }
        },
        columnMenu: false,
        scrollable: false,
        resizable: true,
        reorderable: true,
        sortable: true,
        pageable: { pageSizes: [10, 25, 50, 100] },
        toolbar: kendo.template($("#toolBarTemplate").html())
    });
    
    if(getInternetExplorerVersion() <= 8 && getInternetExplorerVersion() >= 5) {
        $('.toolbar').hide();
    } else {
        $("#clearTextButton").kendoButton({icon: "funnel-clear"});
        $("#txtSearch").on("keyup keypress onpaste", function () {
            var filter = { logic: "or", filters: [] };
            $searchValue = $(this).val();
            if ($searchValue.length > 1) {
                $.each($("#grid").data("kendoGrid").columns, function( key, column ) {
                    if(column.filterable) {
                        filter.filters.push({ field: column.field, operator:"contains", value:$searchValue});
                    }
                });

                $("#grid").data("kendoGrid").dataSource.query({ filter: filter });

                var count = $("#grid").data("kendoGrid").dataSource.total();
                $(".k-pager-info").html("1 - " + count + " of " + count + " items");

            } else if($searchValue == "") {
                $("#grid").data("kendoGrid").dataSource.query({
                    page: 1,
                    pageSize: kendoPageSize
                });
            }
        });

        $("#clearTextButton").on("click", function () {
            $("#grid").data("kendoGrid").dataSource.query({
                page: 1,
                pageSize: kendoPageSize
            });
            $("#txtSearch").val('');
        });
    }    
    
});

// Button Functions
function add() {
	window.location.href = 'index.php?view=add';
}

function modify(id) {
	window.location.href = 'index.php?view=modify&id=' + id;
}

function remove(id) {
	if (confirm('Are You Sure?')) {
		window.location.href = 'process.php?csrfToken=<?= $_SESSION["user_csrf_token"] ?>&actionRun=delete&id=' + id;
	}
}

function photo(Id) {
	window.location.href = 'index.php?view=photo&id=' + Id;
}
