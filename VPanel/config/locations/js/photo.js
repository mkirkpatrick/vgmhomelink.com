// JavaScript Document
var large_image_width;
var large_image_height;
var small_image_width;
var small_image_height;

$(function() {

	// add * to required field labels
	$('label.required').append('&nbsp;<span style="color: #990000"><b>*</b></span>&nbsp;');

	$('#save_thumb').click(function() {
		var x1	= $('#x1').val();
		var y1	= $('#y1').val();
		var x2	= $('#x2').val();
		var y2	= $('#y2').val();
		var w	= $('#w').val();
		var h	= $('#h').val();
		if (x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h=="") {
			alert("You must make a selection first");
			return false;
		} else {
			return true;
		}
	});

});

function preview(img, selection) { 
	var scaleX = small_image_width / selection.width; 
	var scaleY = small_image_height / selection.height; 

	$('#thumbnail + div > img').css({ 
		width: Math.round(scaleX * large_image_width) + 'px', 
		height: Math.round(scaleY * large_image_height) + 'px',
		marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
		marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
	});

	$('#x1').val(selection.x1);
	$('#y1').val(selection.y1);
	$('#x2').val(selection.x2);
	$('#y2').val(selection.y2);
	$('#w').val(selection.width);
	$('#h').val(selection.height);
} 

function deleteTempImage(Id) {
		window.location.href = 'index.php?view=photo&id=' + Id;
}

function deleteImage(Id) {
	if (confirm('Are you sure you want to delete this image?')) {
		window.location.href = 'process.php?csrfToken=<?= $_SESSION["user_csrf_token"] ?>&actionRun=deleteImage&id=' + Id;
	}
}
