<?php
if (!isset($siteConfig)) die("System Error!");

$parameters = array('ORDER' => 'location_sort_id, location_title'); 
$locations = Repository::GetAll($_WEBCONFIG['COMPONET_TABLE'], $parameters); 

$rows = 0;
?> 

<form id="sortForm" method="post" action="process.php">
<input type="hidden" name="actionRun" value="sort" />

<div class="subcontent right last">

	<?= '<h2>' . $_WEBCONFIG['COMPONET_NAME'] . ' -- (' . ucfirst($view) . ')</h2>'; ?>

    <p>Drag and Drop the records in the order you desire them.</p>

	<table class="grid-display">
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtName" class="input required"><b>List of Items</b></label></td>
            <td style="text-align: left; vertical-align: top;">
				<div id="contentOrder">
					<ul>
					<?php
					foreach($locations as $location) {
						print "<li id=\"recordsArray_{$location->location_id}\">$location->location_title</li>\n";
					}
					?>
					</ul>
				</div>
            </td>
        </tr>
	 </table>

<div class="clearfix buttons">
    <a href="./" class="floatLeft button silver">Back</a>
</div>
</div>
</form>