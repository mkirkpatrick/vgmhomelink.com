<?php if (!isset($siteConfig)) die("System Error!"); ?> 

<div class="subcontent right last">
    <div class="forward">
        <a href="index.php?view=sort" class="silver button floatRight icon-arrows-ccw">Reorder</a>
    </div>
    <?php
    print '<h2>' . $_WEBCONFIG['COMPONET_NAME'] . '</h2>';

    if ($form->getNumErrors() > 0) {
        $errors	= $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
                //echo "<div class=\"message success okMessage\">" . $_WEBCONFIG['ENTITY_NAME'] . " Added Successfully!</div>\n";
                break;
            case 'updated':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                //echo "<div class=\"message success okMessage\">" . $_WEBCONFIG['ENTITY_NAME'] . " Updated Successfully!</div>\n";
                break;
            case 'deleted':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                //echo "<div class=\"message success okMessage\">" . $_WEBCONFIG['ENTITY_NAME'] . " Deleted Successfully!</div>\n";
                break;
        }
        unset($_SESSION['processed']);
    }

    $parameters = array('ORDER' => 'location_sort_id, location_title'); 
    $locations = Repository::GetAll($_WEBCONFIG['COMPONET_TABLE'], $parameters); 

    print count($locations) == 1 ? "<p>There is currently <b>1</b>  " . strtolower($_WEBCONFIG['ENTITY_NAME']) . ".</p>\n" : "<p>There are currently <b>" . count($locations) . "</b> " . strtolower($_WEBCONFIG['ENTITY_NAME_PLURAL']) . ".</p>\n";
    ?>

    <table id="grid">
        <thead> 
            <tr> 
                <th>Title</th>
                <th>Address</th>
                <th>Location</th>		
                <th>Status</th>
                <th>Last Modified Date</th>		
                <th data-sortable="false" data-filterable="false" style="text-align: center; width: 190px">Options</th>
            </tr>
        </thead>

        <tbody>

            <?php

            foreach($locations as $location) {		
                $image = '<img src="' . $_WEBCONFIG['VPANEL_PATH'] . 'images/icon_camera.gif" border="0" title="Upload an image for this location" title="Upload">';
                if ($location->location_image && file_exists(filePathCombine($_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['UPLOAD_FOLDER'] . $location->location_image))) {
                    $image = '<img src="' . $_WEBCONFIG['VPANEL_PATH'] . 'images/icon_image.gif" border="0" title="Upload a new image for this location" title="Upload">';
                }
                print '<tr> 
                <td>' . $location->location_title . '</td>
                <td>' . $location->location_address . '</td>
                <td>' . $location->location_city . ', ' . $location->location_state . '</td>
                <td>' . $location->location_status . '</td>
                <td>' . date("m/d/y g:i:s A", strtotime($location->location_last_update)) . '</td>
                <td><div align="center"><a href="javascript:photo('. $location->location_id . ');" class="blue button-slim">Image</a>
                <a href="javascript:modify(' . $location->location_id . ');" class="green button-slim">Modify</a>
                <a href="javascript:remove(' . $location->location_id . ');" class="red button-slim">Delete</a></div></td>
                </tr>' . "\n";
            }

            ?>

        </tbody>
    </table>

    <div class="clearfix buttons">
        <a href="javascript:add()" class="green button floatRight">Add New <?= $_WEBCONFIG['ENTITY_NAME'] ?></a>
    </div><!--End continue-->
</div>

<script type="text/x-kendo-template" id="toolBarTemplate">
    <div class="toolbar floatRight">
        <div>
            <input id="txtSearch" placeholder="Search Grid" type="text" style="width: 175px" />
            <a id="clearTextButton">Clear</a>
        </div>
    </div>
</script>