<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/ThumbLib.inc.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_validate.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once "class.php"; 

security::_secureCheck();

$p = new Process($_WEBCONFIG['COMPONET_TABLE'], $_WEBCONFIG['UPLOAD_FOLDER']);
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {
	case 'add' :
        $p->_add();
        break;

	case 'modify' :
		$p->_modify();
		break;

	case 'delete' :
		$p->_delete();
		break;
		
	case 'deleteImage' :
		$p->_deleteImage();
		break;

	case 'sort' :
		$p->_sort();
		exit();
		break;
		
	case 'photo' :
		$p->_photo();
		break;
		
	case 'thumb' :
		$p->_thumb();
		break;

	default :
		throw new exception("An unknown action executed!");
}

redirect("index.php?{$p->params}");
?>