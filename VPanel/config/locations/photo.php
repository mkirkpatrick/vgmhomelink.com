<?php
if (!isset($siteConfig)) die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

$max_width			 = "640";						// Max width allowed for the large image
$max_height			 = "400";						// Max height allowed for the large image

$location = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $iNum); 

if (is_null($location)) {
	throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
} 

if(isset($_SESSION['UPLOADED_IMAGE'])) { 
	$temp_image_location		= $_WEBCONFIG['UPLOADS_TEMP_DIRECTORY'] . '/' .  $_SESSION['UPLOADED_IMAGE'];
	$temp_photo_exists			= "<img src=\"$temp_image_location\" alt=\"Large Image\" class=\"ImgFrame\" />";
	$thumb_photo_exists			= "";
	$current_large_image_width	= getWidth($_SERVER['DOCUMENT_ROOT'] . $temp_image_location);
	$current_large_image_height	= getHeight($_SERVER['DOCUMENT_ROOT'] . $temp_image_location);
} else if(!isNullOrEmpty($location->location_image)) {
	$image_location				= $_WEBCONFIG['UPLOAD_FOLDER'] . '/' . $location->location_image;
	$temp_photo_exists			= "";
	$thumb_photo_exists			= "<img src=\"$image_location\" alt=\"Large Image\" class=\"ImgFrame\" />";
	$current_large_image_width	= 100;
	$current_large_image_height	= 100;
} else { 
	$temp_photo_exists			= "";
	$thumb_photo_exists			= "";
	$current_large_image_width	= 0;
	$current_large_image_height	= 0;
}
?>
<script type="text/javascript">
	large_image_width	= <?= $current_large_image_width;?>;
	large_image_height	= <?= $current_large_image_height;?>;
	small_image_width	= <?= $_WEBCONFIG['IMAGE_WIDTH'];?>;
	small_image_height	= <?= $_WEBCONFIG['IMAGE_HEIGHT'];?>;

	<?php if (strlen($temp_photo_exists) > 0) { ?>
	$(window).load(function () { 
		$('#thumbnail').imgAreaSelect({ x1: 0, y1: 0, x2: <?= $_WEBCONFIG['IMAGE_WIDTH']; ?>, y2: <?= $_WEBCONFIG['IMAGE_HEIGHT']; ?>, aspectRatio: '<?= $_WEBCONFIG['IMAGE_WIDTH']; ?>:<?= $_WEBCONFIG['IMAGE_HEIGHT']; ?>', onSelectChange: preview, handles: true }); 
	});
	<?php } ?>
</script>

<form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">

<div class="subcontent right last">

	<?php 	print '<h2>Location Photo -- (' . $location->location_title . ')</h2>';

	if ($form->getNumErrors() > 0) {
		$errors	= $form->getErrorArray();
		foreach ($errors as $err) echo $err;
	} else if (isset($_SESSION['processed'])) {
		switch($_SESSION['processed']) {
			case 'added':
				echo "<script>addNotify('Image Added Successfully!')</script>";
                //echo "<div class=\"valid\">Image Added Successfully!</div>\n";
				break;
			case 'updated':
				echo "<script>addNotify('Image Updated Successfully!')</script>";
                //echo "<div class=\"valid\">Image Updated Successfully!</div>\n";
				break;
			case 'deleted':
				echo "<script>addNotify('Image Deleted Successfully!')</script>";
                //echo "<div class=\"valid\">Image Deleted Successfully!</div>\n";
				break;
			case 'removed':
				echo "<script>addNotify('Thumbnail Deleted Successfully!')</script>";
                //echo "<div class=\"valid\">Thumbnail Deleted Successfully!</div>\n";
				break;
		}
		unset($_SESSION['processed']);
	}
	?>

	<?php if (strlen($thumb_photo_exists) > 0) { ?>

    <p>To change the current image select a file and upload below: </p>
	
	<input type="hidden" name="actionRun" value="photo" />
	<input type="hidden" name="hidId" value="<?= $iNum; ?>" />
	<input type="hidden" name="hidMaxWidth" value="<?= $max_width; ?>" />
	<input type="hidden" name="hidMaxHeight" value="<?= $max_height; ?>" />

	<table class="grid-display">
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
			<td style="text-align: right; vertical-align: middle; width: 125px"><label for="fleImage" class="required"><b>Current Image</b></label></td>
            <td align="left" valign="bottom">
                <?= $thumb_photo_exists;?>
            </td>
        </tr>
	<tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
	<td style="text-align: right; vertical-align: middle; width: 125px"><label for="fleImage" class="required"><b>Upload New Image</b></label></td>
	<td style="text-align: left; vertical-align: top;">
	<input name="fleImage" type="file" id="fleImage" accept="gif|jpg|png|wbmp" title="See note of allowed image formats" size="50" maxlength="255" />
	</td>
	</tr>
		        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: left; vertical-align: top;" colspan="2">
	<a class="red button-slim" href="javascript:deleteImage(<?= $iNum;?>);">Remove Photo</a>
            </td>
        </tr>
	</table>

	<?php } else if (strlen($temp_photo_exists) > 0) { ?>

    <input type="hidden" name="actionRun" value="thumb" />
    <input type="hidden" name="hidId" value="<?= $iNum; ?>" />
	<input type="hidden" name="hidImage" value="<?= $_SESSION['UPLOADED_IMAGE'] ?>" />
    <input type="hidden" name="hidThumbWidth" value="<?= $_WEBCONFIG['IMAGE_WIDTH']; ?>" />
    <input type="hidden" name="hidThumbHeight" value="<?= $_WEBCONFIG['IMAGE_HEIGHT']; ?>">
    <input type="hidden" name="x1" id="x1" value="0" />
    <input type="hidden" name="y1" id="y1" value="0" />
	<input type="hidden" name="x2" id="x2" value="<?= $_WEBCONFIG['IMAGE_WIDTH']; ?>" />
	<input type="hidden" name="y2" id="y2" value="<?= $_WEBCONFIG['IMAGE_HEIGHT']; ?>" />
    <input type="hidden" name="w" id="w" value="" />
    <input type="hidden" name="h" id="h" value="" />

    <p>In order to fit the correct dimentions for your website, you will need to crop your image below and click save. </p>

	<table class="grid-display">
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: left; vertical-align: top;">
                <img src="<?= $temp_image_location;?>" style="float: left; margin-right: 10px;" id="thumbnail" alt="Create Thumbnail" />
                <div style="border:1px #e5e5e5 solid; float:left; position:relative; overflow:hidden; width:<?= $_WEBCONFIG['IMAGE_WIDTH'];?>px; height:<?= $_WEBCONFIG['IMAGE_HEIGHT'];?>px;">
                    <img src="<?= $temp_image_location;?>" style="position: relative;" alt="Thumbnail Preview" />
                </div>
                <br style="clear:both;"/>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: left; vertical-align: top;">
	<a class="green button-slim" href="javascript:deleteImage(<?= $iNum;?>);">Upload Another</a>
            </td>
        </tr>
	</table>

	<?php } else { ?>

    <input type="hidden" name="actionRun" value="photo" />
    <input type="hidden" name="hidId" value="<?= $iNum; ?>" />
    <input type="hidden" name="hidMaxWidth" value="<?= $max_width; ?>" />
    <input type="hidden" name="hidMaxHeight" value="<?= $max_height; ?>" />

    <p>Click the Browse button and select an image to upload. Then hit Save Image button.</p>

	<table class="grid-display">
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="fleImage" class="required"><b>Image</b></label></td>
<td style="text-align: left; vertical-align: top;">
<input name="fleImage" type="file" id="fleImage" accept="gif|jpg|png|wbmp" title="See note of allowed image formats" size="50" maxlength="255" />
</td>
</tr>
</table>

	<?php } ?>

<div class="clearfix buttons">
	<a href="./" class="floatLeft button silver">Back</a>
	<?php if (strlen($temp_photo_exists) > 0 && strlen($thumb_photo_exists) > 0) { ?>

	<?php } else if (strlen($temp_photo_exists) > 0) { ?>
	<input name="btnModify" type="submit" id="btnModify" value="Save Thumbnail" class="button floatRight green">
	<?php } else { ?>
	<input name="btnModify" type="submit" id="btnModify" value="Upload Image" class="button green floatRight">
	<?php } ?>
</div><!--End continue-->
</div>

</form>

<?php unset($_SESSION['UPLOADED_IMAGE']); ?>
