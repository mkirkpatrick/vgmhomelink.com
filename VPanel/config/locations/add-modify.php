<style type="text/css">
.tripleField input, .tripleField select { max-width: 30%;}
</style>

<?php
if (!isset($siteConfig)) die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
if ($form->getNumErrors() > 0) {
	$hidId				= $form->value("hidId");
	$txtName			= $form->value("txtName");
	$txtAddress			= $form->value("txtAddress");
	$txtCity			= $form->value("txtCity");
	$cboState			= $form->value("cboState");
	$txtZip 			= $form->value("txtZip");
	$txtMailingAddress	= $form->value("txtMailingAddress");
	$txtMailingCity		= $form->value("txtMailingCity");
	$cboMailingState	= $form->value("cboMailingState");
	$txtMailingZip 		= $form->value("txtMailingZip");
	$txtPhone 			= $form->value("txtPhone");
	$txtFax 			= $form->value("txtFax");
	$txtTollFree 		= $form->value("txtTollFree");
	$txtOrder			= $form->value("txtOrder");
	$txtEmail			= $form->value("txtEmail");
	$txtWebsite			= $form->value("txtWebsite");
	$optStatus			= $form->value("optStatus");
	$mtxHours 			= $form->value("mtxHours");
	$optSite			= $form->value("optSite");
    if($_WEBCONFIG['ENABLE_SERVICES'] == 'true') {
        $services       = (array)$form->value("optServices");
    }
} else if ($iNum > 0) {
	$location = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $iNum);

	if (is_null($location)) {
		throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
	}
	$hidId				= $location->location_id;
	$txtName			= $location->location_title;
	$txtAddress			= $location->location_address;
	$txtCity			= $location->location_city;
	$cboState			= $location->location_state;
	$txtZip 			= $location->location_postal;
	$txtMailingAddress	= $location->location_mailing_address;
	$txtMailingCity		= $location->location_mailing_city;
	$cboMailingState	= $location->location_mailing_state;
	$txtMailingZip 		= $location->location_mailing_postal;
	$txtOrder			= $location->location_sort_id;
	$txtPhone 			= $location->location_phone;
	$txtFax				= $location->location_fax;
	$txtTollFree		= $location->location_toll_free;
	$txtEmail			= $location->location_email;
	$txtWebsite			= $location->location_website;
	$optStatus			= $location->location_status;
	$mtxHours 			= $location->location_hours;
	$optSite			= $location->site_id;

    if($_WEBCONFIG['ENABLE_SERVICES'] == 'true' && $_WEBCONFIG['SITE_TYPE'] == 'BANK') {

        $services       = explode("|", $location->location_services);
    }
} else {
	$hidId				= '';
	$txtName			= '';
	$txtAddress			= '';
	$txtCity			= '';
	$txtZip 			= '';
	$cboState 			= '';
	$txtMailingAddress	= '';
	$txtMailingCity		= '';
	$txtMailingZip 		= '';
	$cboMailingState 	= '';
	$hidImage			= '';
	$txtOrder			= '';
	$txtPhone 			= '';
	$txtFax				= '';
	$txtTollFree		= '';
	$txtEmail			= '';
	$txtWebsite			= '';
	$optStatus			= 'Active';
	$mtxHours 			= '';
	$optSite			= 0;

    if($_WEBCONFIG['ENABLE_SERVICES'] == 'true' && $_WEBCONFIG['SITE_TYPE'] == 'BANK') {
        $services       = array();
    }
}
?>

<form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">
<input type="hidden" name="actionRun" value="<?= $_GET['view'] ?>" />
<input type="hidden" name="hidId" value="<?= $hidId; ?>" />

<div class="subcontent right last">

	<?php
	print '<h2>' . $_WEBCONFIG['COMPONET_NAME'] . ' -- (' . ucfirst($view) . ')</h2>';

	if ($form->getNumErrors() > 0) {
		$errors	= $form->getErrorArray();
		foreach ($errors as $err) echo $err;
	} else if (isset($_SESSION['processed'])) {
        echo "<div class=\"valid\">" . $_WEBCONFIG['ENTITY_NAME'] . " Updated Successfully!</div>\n";
		unset($_SESSION['processed']);
	}
	?>

    <p><?= $view == "modify" ? 'Edit' : 'Insert' ?> the information below and save.</p>

	<table class="grid-display">
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 135px"><label for="txtName" class="required"><b>Title</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtName" id="txtName" value="<?= $txtName; ?>" size="50" maxlength="50" required="required" />
                <?= $form->error("txtName");?>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtAddress"><b>Address</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtAddress" id="txtAddress" value="<?= $txtAddress; ?>" size="50" maxlength="100" />
                <?= $form->error("txtAddress");?>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtCity"><b>City / State / Zip</b></label></td>
            <td style="text-align: left; vertical-align: top;" class="tripleField">
				<input type="text" name="txtCity" id="txtCity" value="<?= $txtCity; ?>" size="25" maxlength="50" placeholder="City" />
				<?= HTML_SPACE ?>
				<select name="cboState" id="cboState">
				<?= getDataOptions(filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/data/', 'states.xml'), $cboState); ?>
				</select>
				<?= HTML_SPACE ?>
				<input type="text" name="txtZip" id="txtZip" value="<?= $txtZip; ?>" size="10" maxlength="250" placeholder="Zip Code" />
                <?= $form->error("txtCity");?>
				<?= $form->error("cboState"); ?>
                <?= $form->error("txtZip");?>
            </td>
        </tr>
		<tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="optMailingAddress"><b>Alternate Mailing Address</b></label></td>
            <td style="text-align: left; vertical-align: top;"><label>Check if your mailing address is different than your physical address.
				<input type="checkbox" id="optMailingAddress" name="optMailingAddress" /></label>
            </td>
        </tr>
		</table>
		<div id="MailingAddress" style="display: none">
		<table class="grid-display">
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtMailingAddress"><b>Mailing Address</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtMailingAddress" id="txtMailingAddress" value="<?= $txtMailingAddress; ?>" size="100" maxlength="250" />
                <?= $form->error("txtMailingAddress");?>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtMailingCity"><b>Mailing Location</b></label></td>
            <td style="text-align: left; vertical-align: top;">
				<input type="text" name="txtMailingCity" id="txtMailingCity" value="<?= $txtMailingCity; ?>" size="25" maxlength="100" />
				<?= HTML_SPACE ?>
                <select name="cboMailingState" id="cboState">
				<?= getDataOptions(filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/data/', 'states.xml'), $cboState); ?>
				</select>
				<?= HTML_SPACE ?>
				<input type="text" name="txtMailingZip" id="txtMailingZip" value="<?= $txtMailingZip; ?>" size="10" maxlength="250" />
				<?= $form->error("txtMailingCity");?>
                <?= $form->error("cboMailingState");?>
                <?= $form->error("txtMailingZip");?>
            </td>
        </tr>
		</table>
		</div>

		<table class="grid-display">
		<tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label class="required" for="txtPhone"><b>Phone</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="tel" name="txtPhone" id="txtPhone" value="<?= $txtPhone; ?>" size="50" maxlength="255" required="required" />
                <?= $form->error("txtPhone");?>
            </td>
        </tr>
		<tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtTollFree"><b>Toll Free</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="tel" name="txtTollFree" id="txtTollFree" value="<?= $txtTollFree; ?>" size="50" maxlength="255" />
                <?= $form->error("txtTollFree");?>
            </td>
        </tr>
		<tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label class="" for="txtFax"><b>Fax</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="tel" name="txtFax" id="txtFax" value="<?= $txtFax; ?>" size="50" maxlength="255" />
                <?= $form->error("txtFax");?>
            </td>
        </tr>
		<tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label class="" for="txtEmail"><b>Email</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtEmail" id="txtEmail" value="<?= $txtEmail; ?>" size="50" maxlength="255" />
                <?= $form->error("txtEmail");?>
            </td>
        </tr>
		<tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtWebsite"><b>Website</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtWebsite" id="txtWebsite" value="<?= $txtWebsite; ?>" size="50" maxlength="255" />
                <?= $form->error("txtWebsite");?>
            </td>
        </tr>
		<tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td width="125" style="text-align: right; vertical-align: top;"><label class="" for="mtxHours"><b>Hours</b></label></td>
            <td style="text-align: left; vertical-align: top;">
				<textarea style="width: 350px; height: 125px; " id="mtxHours" name="mtxHours"><?= $mtxHours ?></textarea>
                <?= $form->error("txtHours");?>
            </td>
        </tr>
        <? if($_WEBCONFIG['ENABLE_SERVICES'] == 'true' && $_WEBCONFIG['SITE_TYPE'] == 'BANK') { ?>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label class="required"><b>Services</b></label></td>
            <td style="text-align: left; vertical-align: top;">
                <input type="hidden" name="cboServices[]" value="" />
                <label><input type="checkbox" name="cboServices[]" value="Branch" <?= in_array("Branch", $services) ? 'checked' : ''; ?> /> Branch</label>&nbsp;&nbsp;&nbsp;
                <label><input type="checkbox" name="cboServices[]" value="Drive-Up" <?= in_array("Drive-Up", $services) ? 'checked' : ''; ?> /> Drive-Up</label>&nbsp;&nbsp;&nbsp;
                <label><input type="checkbox" name="cboServices[]" value="ATM" <?= in_array("ATM", $services) ? 'checked' : ''; ?> /> ATM</label>&nbsp;&nbsp;&nbsp;
                <?= $form->error("cboServices");?>
            </td>
        </tr>
        <? } ?>

        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="optStatus" class="required"><b>Status</b></label></td>
            <td style="text-align: left; vertical-align: top;">
                <input name="optStatus" type="radio" id="optAStatus" value="Active" <?= $optStatus == 'Active' ? 'checked' : ''; ?> />
                <label for="optAStatus">Active </label> &nbsp;&nbsp;
                <input name="optStatus" type="radio" id="optIStatus" value="Inactive" <?= $optStatus == 'Inactive' ? 'checked' : ''; ?> />
                <label for="optIStatus">Inactive </label> &nbsp;&nbsp;
                <?= $form->error("optStatus");?>
            </td>
        </tr>
	</table>

<div class="clearfix buttons">
	<a href="./" class="button silver floatLeft">Back</a>
    <input type="submit" id="btnSave" value="Save Changes" class="button floatRight green">
</div>
</div><!--End continue-->

</form>