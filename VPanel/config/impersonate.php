<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 

security::_secureCheck();

$value = isset($_GET['value']) ? $_GET['value'] : 'true';

if($value == 'true') {
    $_SESSION['REMOVE_WEBMASTER'] = true;    
}
else {
    unset($_SESSION['REMOVE_WEBMASTER']);
}


header("Location: {$_SERVER['HTTP_REFERER']}");
exit;
?>