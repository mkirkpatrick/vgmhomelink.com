<?php
if (!isset($siteConfig)) die("System Error!");    

$rows = 0; 
?> 
<form id="configForm" method="post" action="process.php">

<div class="subcontent right last" style="width:900px;">

    <h2>Site Configuration</h2><?php
    if ($form->getNumErrors() > 0) {
        $errors    = $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        echo "<script>addNotify('Configuration Updated Successfully!')</script>";
        unset($_SESSION['processed']);
    } ?>
    <p>Edit the site configuration below and save. </p>
         
<table class="grid-display">
        <tr>
            <th><div align="center" style="font-size: large;">Configuration Name</div></th>
            <th><div align="left" style="font-size: large; padding-left: 133px;">Value</div></th>
        </tr>         
<?php      $params = array("WHERE" => "allow_direct_mod = 1", "ORDER" => "sort_order"); 
        $configSettings = Repository::GetAll($_WEBCONFIG['COMPONET_TABLE'], $params); 
        foreach($configSettings as $setting) {
            if (strpos($setting->name,'Recipient') === false) {
                print '<tr class="' . ($rows++ % 2 ? 'odd' : 'even') . '">
                            <td width="235" align="right" valign="middle">
                                <label for="txt' . $setting->name .'" class="required"><b>' . ucwords(str_replace("_", " ", $setting->name)) .'</b></label></td>
                            <td style="text-align: left; vertical-align: top;" width="">
                                <input type="text" name="txt' . $setting->name .'" id="txt' . $setting->name .'" value="' . str_replace('"', "'", $setting->value) .'" size="65" maxlength="255">
                            </td>
                        </tr>' . PHP_EOL;
                $rows++;
            }
        }
?>
    </table>

<div class="buttons clearfix">
    <input name="btnModify" type="submit" id="btnModify" value="Save Changes" class="floatRight button green">
</div>
</div><!--End continue-->

</form>