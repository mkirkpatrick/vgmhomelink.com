<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once 'class.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 

security::_secureCheck();

$p = new process;
redirect('index.php');
?>