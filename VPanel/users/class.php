<?php
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_validate.php");

class process extends Database {

	private $record;
	public $params;

	//------------------------------
	public function __construct() {
	//------------------------------
		parent::__construct();
	}

	//------------------------------
	public function _add() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$this->record = new Entity($_WEBCONFIG['COMPONET_TABLE']);
		self::_verify();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=add";
		} else {
			// No Errors
			$ip	 = $_SERVER['HTTP_CLIENT_IP'];
			$newSalt = Security::_generateRandomSalt(24);
			$this->record->user_regdate = date("Y-m-d G:i:s");
            $this->record->user_last_role_change = date("Y-m-d G:i:s");
			$this->record->user_last_password_change = date("Y-m-d G:i:s");
			$this->record->user_ip_addr = $ip;
			$iNum = (int) $this->record->insert();
			Security::_addAuditEvent("Added User", "Success", "Added User -- '" . $this->record->user_username . "' (" . $iNum .")", $_SESSION['user_username']);
			$roles = isset($_POST['cboRoles']) ? (array)$_POST['cboRoles'] : array();
			foreach($roles as $roleId) {
				if(strlen($roleId) > 0) {
					$sql = "INSERT INTO `tbl_users_in_roles` (`user_id`, `urole_id`) VALUES ($iNum, '$roleId');";
					Database::ExecuteRaw($sql);
				}
			}
			$_SESSION['processed'] = 'added';
            $this->params = "";
		}
	}

	//------------------------------
	public function _modify() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$iNum = (int)$_POST['hidId'];
		$this->record = Repository::LoadById($_WEBCONFIG['COMPONET_TABLE'], $iNum);

		self::_verify();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=modify&id=$iNum";
		} else {
			// No Errors
			$ip	 = $_SERVER['HTTP_CLIENT_IP'];
            $this->record->user_ip_addr = $ip;
			$this->record->user_last_role_change = date("Y-m-d G:i:s");
			Repository::Save($this->record);
			Security::_addAuditEvent("Modified User", "Success", "Modified User -- '" . $this->record->user_username . "' (" . $this->record->user_id .")", $_SESSION['user_username']);
			$sql = "DELETE FROM tbl_users_in_roles WHERE user_id = " . $this->record->user_id;
			Database::ExecuteRaw($sql);
			$roles = isset($_POST['cboRoles']) ? (array)$_POST['cboRoles'] : array();
			foreach($roles as $roleId) {
				if(strlen($roleId) > 0) {
					$sql = "INSERT INTO `tbl_users_in_roles` (`user_id`, `urole_id`) VALUES ({$this->record->user_id}, '$roleId');";
					Database::ExecuteRaw($sql);
				}
			}
			$this->params = "view=modify&id=$iNum";
			$_SESSION['processed'] = 'updated';
		}
	}

	//------------------------------
	public function _delete() {
	//------------------------------
		$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
		$this->record = Repository::GetById($GLOBALS['_WEBCONFIG']['COMPONET_TABLE'], $iNum);
		Security::_addAuditEvent("Deleted User", "Success", "Deleted User -- '" . $this->record->user_username . "' (" . $this->record->user_id .")", $_SESSION['user_username']);
		Repository::Delete($this->record);
		$sql = "DELETE FROM `tbl_users_in_roles` WHERE`user_id` = $iNum";
		Database::ExecuteRaw($sql);
		$sql = "UPDATE `tbl_audit_log` SET `deleted` = 1 WHERE `user` = '{$this->record->user_username}'";
		Database::ExecuteRaw($sql);
		$_SESSION['processed'] = 'deleted';
	}

	//------------------------------
	private function _verify() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$fieldName	= "txtName";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Name is required.");
		} else {
			$this->record->user_name = $fieldValue;
		}

		$fieldName	= "txtEmail";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* E-Mail is required.");
		} else if (!Validate::isValidEmail($fieldValue)) {
			$form->setError($fieldName, "** Invalid E-Mail Address.");
		} else {
            $userID = isset($_POST['hidId']) ? (int)$_POST['hidId'] : 0;
			$sql = "SELECT user_id FROM " . $_WEBCONFIG['COMPONET_TABLE'] . " WHERE user_email = '$fieldValue' AND user_id <> $userID LIMIT 1";
			$dt  = Database::Execute($sql);

			if ($dt->Count() > 0) {
				$form->setError($fieldName, "** Email Address already exists in the system.");
			} else {
				$this->record->user_email = strtolower($fieldValue);
			}

		}

		$fieldName	= "txtUsername";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Username is required.");
		} else if (strlen($fieldValue) < 4) {
			$form->setError($fieldName, "** Username must be at least 4 characters.");
		} else if (Validate::isValidEmail($fieldValue)) {
			$form->setError($fieldName, "** Username cannot be an Email Address");
		} else {
			$userID = isset($_POST['hidId']) ? (int)$_POST['hidId'] : 0;
			$sql = "SELECT user_id FROM " . $_WEBCONFIG['COMPONET_TABLE'] . " WHERE user_username = '$fieldValue' AND user_id <> $userID LIMIT 1";
			$dt  = Database::Execute($sql);

			if ($dt->Count() > 0) {
				$form->setError($fieldName, "** Username already exists in the system.");
			} else {
				$this->record->user_username = strtolower($fieldValue);
			}
		}

		$fieldName	= "optStatus";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Status is required.");
		} else {
			$this->record->user_status = $fieldValue;
		}

		$fieldName	= "optReqQuestions";
		$fieldValue	= isset($_POST[$fieldName]) ? strip_tags($_POST[$fieldName]) : '';
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$this->record->user_require_question_change = 'Y';
		} else {
			$this->record->user_require_question_change = $fieldValue;
		}

		$fieldName	= "optReqPassword";
		$fieldValue	= isset($_POST[$fieldName]) ? strip_tags($_POST[$fieldName]) : '';
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$this->record->user_require_password_change = 'Y';
		} else {
			$this->record->user_require_password_change = $fieldValue;
		}

		$fieldName	= "optMasterAdmin";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Status is required.");
		} else {
			$this->record->user_admin = $fieldValue;
		}
	}

	//------------------------------
	public function _passwordReset() {
	//------------------------------

		$iNum	= isset($_GET['id']) ? (int)$_GET['id'] : 0;
		UserManager::autoGeneratePassword($iNum);
	}
};
?>