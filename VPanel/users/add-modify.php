<?php
if (!isset($siteConfig)) die("System Error!");

if(!UserManager::hasRole(USER_MANAGER_ROLE)) { 
    redirect($_WEBCONFIG['VPANEL_PATH']); 
}

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

if ($form->getNumErrors() > 0) {
	$hidId				= $form->value("hidId");
	$txtName			= $form->value("txtName");
	$txtEmail			= $form->value("txtEmail");
	$txtUsername		= $form->value("txtUsername");
	$optStatus			= $form->value("optStatus");
	$userId 			= 0;
    $optMasterAdmin     = $form->value("optMasterAdmin");
    $optReqPassword	    = $form->value("optReqPassword");
    $optReqQuestions    = $form->value("optReqQuestions");
    $optVgmAssociate    = $form->value("optVgmAssociate");
} else if ($iNum > 0) {
	$user = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $iNum);
	

	if (is_null($user)) {
		throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
	}
	$userId 			= $user->user_id; 
	$hidId				= $userId;
	$txtName			= htmlspecialchars($user->user_name);
	$txtEmail			= htmlspecialchars($user->user_email);
	$txtUsername		= htmlspecialchars($user->user_username);
    $optStatus          = $user->user_status;
	$optMasterAdmin     = $user->user_admin;
    $optReqPassword     = $user->user_require_password_change; 
    $optReqQuestions    = $user->user_require_question_change; 
    $optVgmAssociate    = $user->user_password == "VGMAD" ? 'Y' : 'N'; 
} else {
	$hidId				= '';
	$txtName			= '';
	$txtEmail			= '';
	$txtUsername		= '';
	$userId 			= 0; 
    $optStatus          = 'Active';
	$optMasterAdmin		= 'N';
    $optReqPassword	    = 'Y';
    $optReqQuestions    = 'Y';
    $optVgmAssociate    = 'N'; 
}
?> 

<form id="userForm" method="post" action="process.php">
<input type="hidden" name="actionRun" value="<?= $view ?>" />
<input type="hidden" name="csrfToken" value="<?= $_SESSION['user_csrf_token'] ?>" />
<input type="hidden" name="hidId" value="<?= $hidId; ?>" />

<div class="subcontent right last">

	<h2>Administration Panel Users -- (<?= ucfirst($view) ?>)</h2>

	<?php
	if ($form->getNumErrors() > 0) {
		$errors	= $form->getErrorArray();
		echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
		foreach ($errors as $err) echo $err;
	} else if (isset($_SESSION['processed'])) {
        echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
		unset($_SESSION['processed']);
	}
	?>

    <p>Insert the <?= strtolower(urldecode($_WEBCONFIG['ENTITY_NAME'])) ?> information below and save.</p>

	<table class="grid-display"> 
          <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?> none">
                <td width="200"  align="right" valign="middle"><label class="required"><b>VGM Associate</b></label></td>
            <td align="left" valign="bottom">
                <input name="optVgmAssociate" type="radio" value="Y" <?= $optVgmAssociate == 'Y' ? 'checked' : ''; ?> />
                <label for="optYVgmAssociate">Yes </label>
                <input name="optVgmAssociate" type="radio" value="N" <?= $optVgmAssociate == 'N' ? 'checked' : ''; ?> />
                <label for="optNVgmAssociate">No </label>
                <?= $form->error("optVgmAssociate");?>
            </td>
        </tr> 
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td width="200"  align="right" valign="middle"><label class="required"><b>Master Admin</b></label></td>
            <td align="left" valign="bottom">
                <input name="optMasterAdmin" type="radio" value="Y" <?= $optMasterAdmin == 'Y' ? 'checked' : ''; ?> />
                <label for="optYMasterAdmin">Yes </label>
                <input name="optMasterAdmin" type="radio" value="N" <?= $optMasterAdmin == 'N' ? 'checked' : ''; ?> />
                <label for="optNMasterAdmin">No </label>
                <?= $form->error("optMasterAdmin");?>
            </td>
        </tr> 
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td align="right" valign="middle"><label for="txtName" class="required"><b>Name</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtName" id="txtName" value="<?= $txtName; ?>" size="50" required="required" />
                <?= $form->error("txtName");?>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td align="right" valign="middle"><label for="txtEmail" class="required"><b>E-Mail</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="email" name="txtEmail" id="txtEmail" value="<?= $txtEmail; ?>" size="50" maxlength="125" required="required" />
                <?= $form->error("txtEmail");?>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td align="right" valign="middle"><label for="txtUsername" class="required"><b>Username</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtUsername" id="txtUsername" value="<?= $txtUsername; ?>" size="50" minlength="4" maxlength="125" required="required" />
                <?= $form->error("txtUsername");?>
            </td>
        </tr>
		<tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td align="right" valign="middle"><label for="optRoles"><b>Roles / Permissions</b></label></td>
            <td style="text-align: left; vertical-align: top;"><?= UserManager::getRoleOptions($userId); ?>
            </td>
        </tr>
		<? if($view == "modify") { ?>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>" style="display: none">
                <td align="right" valign="middle">
                    <label class="required">
                        <b>Require Password Change</b>
                    </label>
                </td>
                <td align="left" valign="bottom">
                    <input name="optReqPassword" type="radio" value="Y" <?= $optReqPassword == 'Y' ? 'checked' : ''; ?> />
                    <label for="optYReqPassword">Yes </label>
                    <input name="optReqPassword" type="radio" value="N" <?= $optReqPassword == 'N' ? 'checked' : ''; ?> />
                    <label for="optNReqPassword">No </label>
                    <?= $form->error("optReqPassword");?>
                </td>
            </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>" style="display: none">
            <td align="right" valign="middle"><label class="required"><b>Reset Questions</b></label></td>
            <td align="left" valign="bottom">
                <input name="optReqQuestions" type="radio" value="Y" <?= $optReqQuestions == 'Y' ? 'checked' : ''; ?> />
                <label for="optYReqQuestions">Yes </label>
                <input name="optReqQuestions" type="radio" value="N" <?= $optReqQuestions == 'N' ? 'checked' : ''; ?> />
                <label for="optNReqQuestions">No </label>
                <?= $form->error("optReqQuestions");?>
            </td>
        </tr> 
		<? } ?>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td align="right" valign="middle"><label for="optAStatus" class="required"><b>Status</b></label></td>
                <td align="left" valign="bottom">
                    <input name="optStatus" type="radio" value="Active" <?= $optStatus == 'Active' ? 'checked' : ''; ?> />
                    <label for="optAStatus">Active </label>
                    <input name="optStatus" type="radio" value="Inactive" <?= $optStatus == 'Inactive' ? 'checked' : ''; ?> />
                    <label for="optIStatus">Inactive </label>
				    <input name="optStatus" type="radio" value="Locked" <?= $optStatus == 'Locked' ? 'checked' : ''; ?> />
                    <label for="optLStatus">Locked </label>
                    <?= $form->error("optStatus");?>
                </td>
        </tr>
	</table>

<div class="clearfix buttons">
    <a href="./" class="silver button floatLeft">Back</a>
    <input name="btnModify" class="button floatRight green" type="submit" id="btnModify" value="Save">
</div><!--End continue-->
</div>

</form>