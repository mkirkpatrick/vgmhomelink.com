<?php  
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 

security::_secureCheck();

    $today          = date("m/d/Y", time());
    $mainDocument   = "Users_$today.csv";
    $content        = "";
    $comma          = ",";

    header("Content-Type: application/csv");
    header("Content-Disposition: attachment; filename=\"$mainDocument\"");
    header("Pragma: no-cache");
    header("Expires: 0");

    $content  .= "Name";
    $content .= $comma . '"User Email"';
    $content .= $comma . '"User Name"';
    $content .= $comma . '"User Roles"';
    $content .= $comma . '"Last Role Change"';
    $content .= $comma . '"Last Login"';
    $content .= $comma . '"Last Password Change"';    
    $content .= $comma . '"Register Date"';
    $content .= $comma . '"IP Address"';
    $content .= $comma . '"Status"';
    $content .= "\n";
        
    $sql = "SELECT * FROM tbl_user";
    $users = Database::Execute($sql);
 
    if($users->Count() > 0) {
        while($users->MoveNext()) {
            $sql = "SELECT ur.*, uir.* FROM tbl_user_roles ur, tbl_users_in_roles uir WHERE uir.user_id = $users->user_id AND uir.urole_id = ur.urole_id";
            $usersRoles = Database::Execute($sql);
            
            $roles = '';
            while($usersRoles->MoveNext()) {
                $roles .= $usersRoles->name . ' | ';
            }
            
            $roles = rtrim($roles, " | ");
            
            
            $content .= $users->user_name;
            $content .= $comma . $users->user_email;
            $content .= $comma . $users->user_username;
            $content .= $comma . $roles;
            $content .= $comma . date("m-d-Y", strtotime($users->user_last_role_change));
            $content .= $comma . date("m-d-Y", strtotime($users->user_last_login));
            $content .= $comma . date("m-d-Y", strtotime($users->user_last_password_change));
            $content .= $comma . date("m-d-Y", strtotime($users->user_regdate));
            $content .= $comma . $users->user_ip_addr;
            $content .= $comma . $users->user_status;
            $content .= "\n";                
        }    
        print $content;
    }

?>