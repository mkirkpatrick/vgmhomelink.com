<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once 'class.php'; 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_vMail.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php");

security::_secureCheck(USER_MANAGER_ROLE);

if (!isset($_GET['logout'])) {
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

if(!isset($_REQUEST['actionRun']))  {
	serverTransfer(filePathCombine($_WEBCONFIG['VPANEL_PATH'], "405error.php")); 
}
if(!isset($_REQUEST['csrfToken']) || $_REQUEST['csrfToken'] != $_SESSION['user_csrf_token'] )  {
    serverTransfer(filePathCombine($_WEBCONFIG['VPANEL_PATH'], "403error.php")); 
}

$p = new process;
$action = $_REQUEST['actionRun']; 

switch ($action) {
	case 'add' :
		$p->_add();
		break;

	case 'modify' :
		$p->_modify();
		break;
		
	case 'reset' :
		$p->_passwordReset();
		break;

	case 'delete' :
		$p->_delete();
		break;
		
	case 'export' :
		$p->_export();
		break;

	default :
		throw new exception("An unknown action executed!");
}

redirect("index.php?{$p->params}");
?>