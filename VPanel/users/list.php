<?php
if (!isset($siteConfig)) die("System Error!");

/* if(!UserManager::hasRole(SITE_MANAGER_ROLE)) {
    redirect("/vpanel"); 
} */ 
?> 

<div class="subcontent right last">

  <h2>Manage <?= $_WEBCONFIG['ENTITY_NAME_PLURAL'] ?></h2>

    <?php
    if ($form->getNumErrors() > 0) {
        $errors	= $form->getErrorArray();
        echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
                break;
            case 'updated':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                break;
            case 'reset':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Password Reset Successfully!')</script>";
                break;
            case 'deleted':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                break;
        }
        unset($_SESSION['processed']);
    }

    $sql = "SELECT * FROM tbl_user ORDER BY user_name"; 
    $users	= Database::Execute($sql);

    if ($users->Count() == 1) {
        print "<p>There is currently <b>1</b> record. </p>\n";
    } else {
        $counter = $_SESSION['site_chief'] ? $users->Count() : $users->Count() - 2;
        print "<p>There are currently <b>$counter</b> records.</p>\n";
    }
    ?>

    <table id="grid">
        <thead> 
            <tr>
                <th>Username</th>
                <th>Administrator Name</th>
                <th>Email Address</th>
                <th>Status</th>
                <th>Last Login Date</th>
                <th data-sortable="false" data-filterable="false" style="text-align: center; width: 290px;">Options</th>
            </tr>
        </thead>

        <tbody>

            <?
            $logButton = '';
            while ($users->MoveNext()) {
                if (($timestamp = strtotime($users->user_last_login)) === false) {
                    $lastLogin = '-';
                } else {
                    $lastLogin = date("m/d/y g:i A", $timestamp);
                }
                if(UserManager::hasRole(AUDIT_LOG_MANAGER_ROLE)) {
                    $logButton = '<a href=\'javascript:userLogs("' . $users->user_username . '");\' class="blue button-slim" style="width: 100px">Logs</a>';
                }
                print '<tr>
                            <td>' . $users->user_username . '</td>
                            <td>' . $users->user_name . '</td>
                            <td>' . $users->user_email . '</td>
                            <td>' . $users->user_status . '</td>
                            <td><div align="center">' . $lastLogin . '</div></td>
                            <td>
                                <div align="center">
                                ' . ($_SESSION['user_id'] == $users->user_id ? '' : 
                                  
                                    '<a href="javascript:modifyUser(' . $users->user_id . ');" class="green button-slim">Modify</a>
                                    <a href="javascript:deleteUser(' . $users->user_id . ');" class="red button-slim">Delete</a>') . '
                                </div>
                            </td>
                        </tr>';
            }// end while
            ?>

        </tbody>
    </table>

    <div class="clearfix buttons">
        <a onclick="javascript: window.open('/VPanel/users/export.php')" class="button floatLeft blue">Export Users</a>
        <?php if(UserManager::hasRole(SITE_MANAGER_ROLE)) { ?>
            <a href="javascript:addUser()" class="button floatRight green">New <?= $_WEBCONFIG['ENTITY_NAME'] ?></a>
            <?php } ?>
    </div><!--End continue-->
</div>

<script type="text/x-kendo-template" id="toolBarTemplate">
    <div class="toolbar floatRight">
        <div>
            <input id="txtSearch" placeholder="Search Grid" type="text" style="width: 175px" />
            <a id="clearTextButton">Clear</a>
        </div>
    </div>
</script>