// JavaScript Document
$(document).ready(function() {

	// add * to required field labels
	$('label.required').append('&nbsp;<span style="color: #990000"><b>*</b></span>&nbsp;');
	$('input[name="optMasterAdmin"]').on("change", function() { 
		if($('input[name="optMasterAdmin"]:checked').val() == "Y") { 
			$('input[name="cboRoles[]"]').prop('disabled',true);
			$('input[name="cboRoles[]"]').prop('checked',true);
		} else {
			$('input[name="cboRoles[]"]').prop('disabled',false);
			$('input[name="cboRoles[]"]').prop('checked',false);
		}
	}); 
	
	if($('input[name="optMasterAdmin"]:checked').val() == "Y") { 
			$('input[name="cboRoles[]"]').prop('disabled',true);
			$('input[name="cboRoles[]"]').prop('checked',true);
	}
});

function resetPassword(Id) {
	if (confirm('Are You Sure?')) {
		window.location.href = 'process.php?csrfToken=<?= $_SESSION["user_csrf_token"] ?>&actionRun=reset&id=' + Id;
	}
}
