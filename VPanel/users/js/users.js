$(document).ready(function () {
    var kendoPageSize = 25;
    $("#grid").kendoGrid({
        dataSource: { pageSize: kendoPageSize },
        pageable: { pageSizes: [25, 50, 100] },         
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",                    
                    doesnotcontain: "Does Not Contain",
                    startswith: "Starts With",
                    endswith: "Ends With"
                }
            }
        },        
        columnMenu: false,
        scrollable: false,
        resizable: true,
        reorderable: true,
        sortable: true,
        toolbar: kendo.template($("#toolBarTemplate").html())
    });
    
    if(getInternetExplorerVersion() <= 8 && getInternetExplorerVersion() >= 5) {
        $('.toolbar').hide();
    } else {
        $("#clearTextButton").kendoButton({icon: "funnel-clear"});
        $("#txtSearch").on("keyup keypress onpaste", function () {
            var filter = { logic: "or", filters: [] };
            $searchValue = $(this).val();
            if ($searchValue.length > 1) {
                $.each($("#grid").data("kendoGrid").columns, function( key, column ) {
                    if(column.filterable) {
                        filter.filters.push({ field: column.field, operator:"contains", value:$searchValue});
                    }
                });

                $("#grid").data("kendoGrid").dataSource.query({ filter: filter });

                var count = $("#grid").data("kendoGrid").dataSource.total();
                $(".k-pager-info").html("1 - " + count + " of " + count + " items");

            } else if($searchValue == "") {
                $("#grid").data("kendoGrid").dataSource.query({
                    page: 1,
                    pageSize: kendoPageSize
                });
            }
        });

        $("#clearTextButton").on("click", function () {
            $("#grid").data("kendoGrid").dataSource.query({
                page: 1,
                pageSize: kendoPageSize
            });
            $("#txtSearch").val('');
        });
    }    
    
});

// Button Functions
function addUser() {
	window.location.href = 'index.php?view=add';
}

function modifyUser(Id) {
	window.location.href = 'index.php?view=modify&id=' + Id;
}

function exportUsers() {
    id = 1;
    window.location.href = '/VPanel/users/export.php';
}

function deleteUser(Id) {
	if (confirm('Are You Sure?')) {
		window.location.href = 'process.php?csrfToken=' + UserCsrfToken + '&actionRun=delete&id=' + Id;
	}
}

function resetPassword(Id) {
	if (confirm('Are You Sure?')) {
		window.location.href = 'process.php?csrfToken=' + UserCsrfToken + '&actionRun=reset&id=' + Id;
	}
}

function userLogs(user) {
    window.location.href = '/vpanel/logs/index.php?view=logdata&user=' + user;
}

function exportData() {
	window.location.href = 'process.php?csrfToken=' + UserCsrfToken + '&actionRun=export';
}
