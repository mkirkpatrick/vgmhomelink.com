<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php");

security::_secureCheck(USER_MANAGER_ROLE);

if (!isset($_GET['logout']))
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

print '<section class="title">
		<div class="container clearfix">
			<h1 class="page-title icon-users">Users</h1>
			<p class="page-desc">Add / Edit / Remove users and view activity.</p>
		</div>
	</section>';
print '<section class="maincontent"><div class="container clearfix">';
include('navBar.php');

$rows = 0;
$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'add' :
	case 'modify' :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/users-form.js');
		include_once $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . $_WEBCONFIG['COMPONET_FOLDER'] . 'add-modify.php';
		break;

    case 'export' :
        $script   = array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/users-form.js');
        include_once $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . $_WEBCONFIG['COMPONET_FOLDER'] . 'export.php';
        break;

	default :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/users.js');
		include_once $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . $_WEBCONFIG['COMPONET_FOLDER'] . 'list.php';
}

print '</div></section>';
print '<script>var UserCsrfToken = "' . $_SESSION["user_csrf_token"] . '"</script>';
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>