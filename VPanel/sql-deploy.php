<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
security::_secureCheck();


Database::ExecuteRaw("ALTER TABLE `tbl_members`
ADD COLUMN `m_prev_password` VARCHAR(500) NULL AFTER `m_password_salt`,
ADD COLUMN `m_reset_link` VARCHAR(255) NULL AFTER `m_prev_password`,
ADD COLUMN `m_reset_link_date` DATETIME NULL AFTER `m_reset_link`");

die("Deploy Complete");
?>