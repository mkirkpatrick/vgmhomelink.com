<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . 'library/classes/class_security.php';
require_once $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . 'library/classes/class_visitor.php';
security::_secureCheck();

if($_WEBCONFIG['SITE_TYPE'] == 'LANDING-PAGE') {
    redirect(filePathCombine($_WEBCONFIG['VPANEL_PATH'], 'pagebuilder'));
}

if (!isset($_GET['logout']))
    $_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

if(isset($_GET['renew'])) {
    die('VPanel Session Renewed');
}


include_once $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . 'includes/inc_pgtop.php';
include_once $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . 'includes/inc_header.php';
?>

<style>
    .change-log {
        padding: 0 1rem;
    }

    .change-log .version-block {
        margin: 1rem;
        padding-top: 1rem;
    }


    .change-log .version-block:nth-child(1n+2) {
        border-top: 1px solid #ebebeb;
    }

    .change-list {
        list-style: none;
        margin-top: .25rem;
        margin-bottom: .25rem;
        margin-right: .25rem;
        margin-left: .75rem;
    }

    .change-list--section ul {
        margin-top: .25rem;
        margin-bottom: .25rem;
        list-style-type: square;
    }
    .change-list--section .title {
        font-weight: bold;
        font-size: 1rem;
    }
</style>

<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-book-open">Version History</h1>
    </div>
</section>

<section class="maincontent" role="main">
    <div class="container clearfix">
        <div class="change-log">
            <div class="version-block">
                <h3 class="inline">v1.2.0</h3>
                <ul class="change-list">
                    <li class="change-list--section"><span class="change-list--section title">Added - 08/31/2020</span>
                        <ul>
                            <li>Added the ability for master admin VPanel users to bulk edit member provider numbers via the Create / Edit View</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="version-block">
                <h3 class="inline">v1.1.0</h3>
                <ul class="change-list">
                    <li class="change-list--section"><span class="change-list--section title">Added - 03/09/2020</span>
                        <ul>
                            <li>Added the ability for master admin VPanel users to modify location records</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="version-block">
                <h3 class="inline">v1.0.0</h3>
                <ul class="change-list">
                    <li class="change-list--section"><span class="change-list--section title">Added</span>
                        <ul>
                            <li>Version History section added to VPanel</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section><!--End content-->

<?php
    include_once $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . 'includes/inc_pgbot.php';
    include_once $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . 'includes/joyRide/jquery-JoyRide.php';
?>