<div class="subcontent right last">
    <form id="supportForm" method="post" action="process.php">
        <input type="hidden" name="actionRun" value="<?= $view ?>" />


        <h1><?= $_WEBCONFIG['COMPONET_NAME']; ?> (<?= ucfirst($view);  ?>)</h1>

        <?php
        if ($form->getNumErrors() > 0) {
            $errors	= $form->getErrorArray();
            foreach ($errors as $err) echo $err;
        } else if (isset($_SESSION['processed'])) {
            echo "<div class=\"message success okMessage\">You have successfully submitted your suggestion to VGM Forbin. </div>\n";
            unset($_SESSION['processed']);
        }
        ?>

        <p>We're continuously improving VPanel. Enter your suggestion below and we'll try to get back with you as soon as possible. </p>

        <table class="grid-display">
            <tr class="<?= ($rows++ % 2 ? 'even' : 'odd'); ?>">
                <td width="110" align="right" valign="middle"><label for="txtName" class="required"><b>Site Name</b></label></td>
                <td style="text-align: left; vertical-align: top;"> <?= $_SERVER['ROOT_URI'];?> </td>
            </tr>
            <tr class="<?= ($rows++ % 2 ? 'even' : 'odd'); ?>">
                <td align="right" valign="middle"><label for="txtPosted" class="required"><b>Posted By</b></label></td>
                <td style="text-align: left; vertical-align: top;"> <?= $_SESSION['user_name'];?> </td>
            </tr>
            <tr class="<?= ($rows++ % 2 ? 'even' : 'odd'); ?>">
                <td style="text-align: right; vertical-align: top;"><label for="mtxDescription" class="required"><b>Message</b></label></td>
                <td style="text-align: left; vertical-align: top;"><textarea name="mtxDescription" id="mtxDescription" style="width: 500px; height: 200px; "><?= $mtxDescription; ?></textarea>
                    <div>
                        <input type="checkbox" name="chkCopy" id="chkCopy" value="Y" /> 
                        <label for="chkCopy"> Send me a copy</label>
                    </div>
                </td>
            </tr>
        </table>

        <div class="clearfix buttons">
            <a href="./" class="button silver floatLeft">Back</a>
            <input type="submit" id="btnSave" value="Submit Suggestion" class="button floatRight green">
        </div>        

    </form>
</div><!--End continue-->