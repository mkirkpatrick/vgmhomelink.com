<div class="subcontent right last">
    <form id="supportForm" method="post" action="process.php">
        <input type="hidden" name="actionRun" value="<?= $view ?>" />
        <h1><?= $_WEBCONFIG['COMPONET_NAME']; ?> (<?= ucfirst($view);  ?>)</h1>

        <?php
        if ($form->getNumErrors() > 0) {
            $errors	= $form->getErrorArray();
            foreach ($errors as $err) echo $err;
        } else if (isset($_SESSION['processed'])) {
            echo "<div class=\"message success okMessage\">You have successfully submitted your technical issue to VGM Forbin. Please be patient as we will troubleshoot the issue prior to making recommendations.</div>\n";
            unset($_SESSION['processed']);
        }
        ?>

        <p>We pride ourselves in producing problem free solutions, however, issues arise and mistakes happen. <br>Please be as specific as possible when explaining the problem that you are experiencing including the page that you were on and the time of day.</p>

        <table class="grid-display">
            <tr class="<?= ($rows++ % 2 ? 'even' : 'odd'); ?>">
                <td width="100" align="right" valign="middle"><label for="txtName" class=""><b>Site Name</b></label></td>
                <td style="text-align: left; vertical-align: top;"> <?= $_SERVER['ROOT_URI'];?> </td>
            </tr>
            <tr class="<?= ($rows++ % 2 ? 'even' : 'odd'); ?>">
                <td align="right" valign="middle"><label for="txtPosted" class=""> <b>Posted By</b></label></td>
                <td style="text-align: left; vertical-align: top;"> <?= $_SESSION['user_name'];?> </td>
            </tr>
            <tr class="<?= ($rows++ % 2 ? 'even' : 'odd'); ?>">
                <td align="right" valign="middle"><label for="mtxDescription" class="required"><b>Message</b></label></td>
                <td style="text-align: left; vertical-align: top;"><textarea name="mtxDescription" id="mtxDescription" style="width: 500px; height: 200px; "><?= $mtxDescription; ?></textarea>
                    <div>
                        <input type="checkbox" name="chkCopy" id="chkCopy" value="Y" /> 
                        <label for="chkCopy"> Send me a copy</label>
                    </div>
                </td>
            </tr>
        </table>

        <div class="clearfix buttons">
            <a href="./" class="button silver floatLeft">Back</a>
            <input type="submit" id="btnSave" value="Submit Issue" class="button floatRight green">
        </div>

    </form>
</div><!--End continue-->