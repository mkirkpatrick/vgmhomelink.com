<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'] , $_WEBCONFIG['VPANEL_PATH'] , "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'] , $_WEBCONFIG['VPANEL_PATH'] , "includes/inc_header.php");


print '<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-compass">Contact VGM Forbin</h1>
        <p class="page-desc">Contact VGM Forbin with questions about your site.</p>
    </div>
</section>';
print '<section class="maincontent"><div class="container clearfix">';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/contact/navBar.php');

if ($form->getNumErrors() > 0) {
	$mtxDescription		= $form->value("mtxDescription", 'N');
} else {
	$mtxDescription		= '';
}

$rows = 0;

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'technical' :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/users-form.js');
		include_once 'technical.php';	
		break;
	
	case 'suggestion' :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/users-form.js');
		include_once 'suggestion.php';		
		break;
		
	case 'quote' :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/users-form.js');
		include_once 'quote.php';		
		break;

	default :
		include_once 'technical.php';		
}

print '</div></section>';

$script	= array('jquery/pages/config.js');
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>