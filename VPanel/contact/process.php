<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once 'class.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_vMail.php"); 

security::_secureCheck();

$p = new process;
$action = isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {
	case 'technical' :
		$p->_technical();
		break;
		
	case 'suggestion' :
		$p->_suggestion();
		break;
		
	case 'quote' :
		$p->_quote();
		break;

	default :
		throw new exception("An unknown action executed!");
}


redirect("index.php?view={$action}");
?>