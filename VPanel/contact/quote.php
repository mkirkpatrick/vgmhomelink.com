<div class="subcontent right last">
    <form id="supportForm" method="post" action="process.php">
        <input type="hidden" name="actionRun" value="<?= $view ?>" />
        <h1><?= $_WEBCONFIG['COMPONET_NAME']; ?> (<?= ucfirst($view);  ?>)</h1>

        <?php
        if ($form->getNumErrors() > 0) {
            $errors	= $form->getErrorArray();
            foreach ($errors as $err) echo $err;
        } else if (isset($_SESSION['processed'])) {
            echo "<div class=\"message success okMessage\">You have successfully submitted your request to VGM Forbin; a sales representative will be in contact with you shortly.  </div>\n";
            unset($_SESSION['processed']);
        }
        ?>

        <p>Upgrade and add to your current website with a VPanel Module. Modules are designed to fit into your current site's design and layout.<br>
            Fill out the form below and a Forbin Sales representative will be in contact with you shortly.  </p>

        <table class="grid-display">
            <tr class="<?= ($rows++ % 2 ? 'even' : 'odd'); ?>">
                <td width="110" align="right" valign="middle"><label for="txtName" class="required"><b>Site Name</b></label></td>
                <td style="text-align: left; vertical-align: top;"> <?= $_SERVER['ROOT_URI'];?> </td>
            </tr>
            <tr class="<?= ($rows++ % 2 ? 'even' : 'odd'); ?>">
                <td align="right" valign="middle"><label for="txtPosted" class="required"><b>Module</b></label></td>
                <td style="text-align: left; vertical-align: top;">		                    
                    <select name="optModule" id="optModule">
                        <option selected="selected" value="Custom / Other">Custom / Other</option>
                        <option value="About Us / Meet Our Team">About Us / Meet Our Team</option>
                        <option value="Blog / Journal">Blog / Journal</option>
                        <option value="Careers / Job Postings">Careers / Job Postings</option>
                        <option value="Content Management System">Content Management System</option>
                        <option value="Custom Form">Custom Form</option>
                        <option value="Customer Survey Form">Customer Survey Form</option>
                        <option value="Ecommerce / Online Store">Ecommerce / Online Store</option>
                        <option value="Events Calendar">Events Calendar</option>
                        <option value="Facebook Tabs">Facebook Tabs</option>
                        <option value="FAQs">FAQs</option>
                        <option value="Featured Graphic">Featured Graphic</option>
                        <option value="Forms Library">Forms Library</option>
                        <option value="Mailing List">Mailing List</option>
                        <option value="Marketing Message">Marketing Message</option>
                        <option value="Members / User Logins">Members / User Logins</option>
                        <option value="Mobile Site">Mobile Site</option>
                        <option value="News Package">News Package</option>
                        <option value="Online Bill Pay">Online Bill Pay</option>
                        <option value="Online Poll">Online Poll</option>
                        <option value="Online RX Refills">Online RX Refills</option>
                        <option value="Patient Referral Form">Patient Referral Form</option>
                        <option value="Pay-Per-Click">Pay-Per-Click</option>
                        <option value="Photo Gallery">Photo Gallery</option>
                        <option value="Prescription / Product Refill">Prescription / Product Refill</option>
                        <option value="Printable Coupon">Printable Coupon</option>
                        <option value="Questionnaire Form">Questionnaire Form</option>
                        <option value="Social Media">Social Media</option>
                        <option value="Testimonials">Testimonials</option>
                        <option value="Twitter Feed">Twitter Feed</option>
                        <option value="Video Gallery">Video Gallery</option>
                        <option value="Web Marketing Form">Web Marketing Form</option>
                    </select></td>
            </tr>
            <tr class="<?= ($rows++ % 2 ? 'even' : 'odd'); ?>">
                <td align="right" valign="middle"><label for="txtPosted" class="required"><b>Posted By</b></label></td>
                <td style="text-align: left; vertical-align: top;"> <?= $_SESSION['user_name'];?> </td>
            </tr>
            <tr class="<?= ($rows++ % 2 ? 'even' : 'odd'); ?>">
                <td style="text-align: right; vertical-align: top;"><label for="mtxDescription" class="required"><b>Message</b></label></td>
                <td style="text-align: left; vertical-align: top;"><textarea name="mtxDescription" id="mtxDescription" style="width: 500px; height: 200px; "><?= $mtxDescription; ?></textarea>
                    <div>
                        <input type="checkbox" name="chkCopy" id="chkCopy" value="Y" /> 
                        <label for="chkCopy"> Send me a copy</label>
                    </div>
                </td>
            </tr>
        </table>

        <div class="clearfix buttons">
            <a href="./" class="button silver floatLeft">Back</a>
            <input type="submit" id="btnSave" value="Save Changes" class="button floatRight green">
        </div>

    </form>
</div><!--End continue-->