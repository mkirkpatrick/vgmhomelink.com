<?php
class process extends Database { 

	//------------------------------
	public function _technical() {
	//------------------------------
		global $_WEBCONFIG, $form, $siteConfig;

		$fieldName	= "mtxDescription";
		$fieldValue	= nl2br(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Please fill in the description. ");
		}

		// Errors exist
		if ($form->getNumErrors() > 0) {
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
		} // No Errors
		else {
			$htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "/emailtemplates/contact-email.html");
			if(file_exists($htmlFile)) { 
				$fh = fopen($htmlFile, 'r');
				$body = fread($fh, filesize($htmlFile));
				fclose($fh); 
			} else {
				throw new exception("Email Template File Not Found"); 
			}
			
			$mergeFields = array(); 
            $mergeFields["**GUEST**"]         = "Forbin Customer Care"; 
			$mergeFields["**USERNAME**"]      = $_SESSION['user_name']; 
			$mergeFields["**WEBSITE**"]       = $_WEBCONFIG['SITE_URL']; 
			$mergeFields["**SITEURL**"]       = $_WEBCONFIG['SITE_URL'];
            $mergeFields["**COMPANY**"]       = $siteConfig["Company_Name"]; 
			$mergeFields["**DESCRIPTION**"]   = $fieldValue;  
			$mergeFields["**CONTACTTYPE**"]   = "Technical Issue"; 
			$mergeFields["**EMAILADDRESS**"]  = $_SESSION['user_email']; 
			$mergeFields["**VPANELVERSION**"] = $_WEBCONFIG['VPANEL_VERSION']; 
			
			$body = strtr($body, $mergeFields);
			
			$mailer = new vMail();
			$mailer->addRecipient($_WEBCONFIG['TECHNICAL_EMAIL_RECEIPIENT'], "Forbin Customer Care");
			if(isset($_POST['chkCopy'])) { 
				$mailer->addCarbonCopy($_SESSION['user_email'], $_SESSION['user_name']);
			}
			$mailer->setSubject("{$siteConfig['Company_Name']} VPanel :: Technical Support Email");
			$mailer->setMailType("html"); 
			$mailer->setFrom($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel");
			$mailer->setReplyTo($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel"); 
			$mailer->setMessage($body); 			
			$mailer->sendMail();
			
			$_SESSION['processed'] = "sent"; 
		}
	}
	
	//------------------------------
	public function _suggestion() {
	//------------------------------
		global $_WEBCONFIG, $form, $siteConfig;

		$fieldName	= "mtxDescription";
		$fieldValue	= nl2br(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Please fill in the description. ");
		}

		// Errors exist
		if ($form->getNumErrors() > 0) {
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
		} // No Errors
		else {
			$htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "/emailtemplates/contact-email.html");
			if(file_exists($htmlFile)) { 
				$fh = fopen($htmlFile, 'r');
				$body = fread($fh, filesize($htmlFile));
				fclose($fh); 
			} else {
				throw new exception("Email Template File Not Found"); 
			}
			
            $mergeFields = array(); 
            $mergeFields["**GUEST**"]         = "Forbin Customer Care"; 
            $mergeFields["**USERNAME**"]      = $_SESSION['user_name']; 
            $mergeFields["**WEBSITE**"]       = $_WEBCONFIG['SITE_URL']; 
            $mergeFields["**SITEURL**"]       = $_WEBCONFIG['SITE_URL']; 
            $mergeFields["**COMPANY**"]       = $siteConfig["Company_Name"];
            $mergeFields["**DESCRIPTION**"]   = $fieldValue;  
            $mergeFields["**CONTACTTYPE**"]   = "Suggestion Email"; 
            $mergeFields["**EMAILADDRESS**"]  = $_SESSION['user_email']; 
            $mergeFields["**VPANELVERSION**"] = $_WEBCONFIG['VPANEL_VERSION']; 
			
			$body = strtr($body, $mergeFields);
			
			$mailer = new vMail();
			$mailer->addRecipient($_WEBCONFIG['SUGGESTION_EMAIL_RECEIPIENT'], "Forbin Customer Care");
			if(isset($_POST['chkCopy'])) { 
				$mailer->addCarbonCopy($_SESSION['user_email'], $_SESSION['user_name']);
			}
			$mailer->setSubject("{$siteConfig['Company_Name']} VPanel :: Suggestion Email");
			$mailer->setMailType("html"); 
			$mailer->setFrom($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel");
			$mailer->setReplyTo($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel"); 
			$mailer->setMessage($body); 	
			$mailer->sendMail();
			
			$_SESSION['processed'] = "sent"; 
		}
	}
	
	//------------------------------
	public function _quote() {
		//------------------------------
		global $_WEBCONFIG, $form, $siteConfig;

		$fieldName	= "mtxDescription";
		$fieldValue	= nl2br(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Please fill in the description. ");
		}

		// Errors exist
		if ($form->getNumErrors() > 0) {
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
		} // No Errors
		else {
			$htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "/emailtemplates/contact-email.html");
			if(file_exists($htmlFile)) { 
				$fh = fopen($htmlFile, 'r');
				$body = fread($fh, filesize($htmlFile));
				fclose($fh); 
			} else {
				throw new exception("Email Template File Not Found"); 
			}
            
            $mergeFields = array(); 
            $mergeFields["**GUEST**"]         = "Forbin Customer Care"; 
            $mergeFields["**USERNAME**"]      = $_SESSION['user_name']; 
            $mergeFields["**WEBSITE**"]       = $_WEBCONFIG['SITE_URL']; 
            $mergeFields["**COMPANY**"]       = $siteConfig["Company_Name"]; 
            $mergeFields["**SITEURL**"]       = $_WEBCONFIG['SITE_URL']; 
            $mergeFields["**DESCRIPTION**"]   = 'This user has requested a <b>' . $_POST['optModule'] . ' </b>module for their site: ' . $fieldValue;  
            $mergeFields["**CONTACTTYPE**"]   = "Sales Quote Request"; 
            $mergeFields["**EMAILADDRESS**"]  = $_SESSION['user_email']; 
            $mergeFields["**VPANELVERSION**"] = $_WEBCONFIG['VPANEL_VERSION'];             
			
			$body = strtr($body, $mergeFields);
			
			$mailer = new vMail();
			$mailer->addRecipient($_WEBCONFIG['SALES_QUOTE_EMAIL_RECEIPIENT'], "Forbin Sales");
			if(isset($_POST['chkCopy'])) { 
				$mailer->addCarbonCopy($_SESSION['user_email'], $_SESSION['user_name']);
			}
			$mailer->setSubject("{$siteConfig['Company_Name']} VPanel :: Sales Quote Request");
			$mailer->setMailType("html"); 
			$mailer->setFrom($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel");
			$mailer->setReplyTo($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel"); 
			$mailer->setMessage($body); 			
			$mailer->sendMail();
			
			$_SESSION['processed'] = "sent"; 
		}
	}

};
?>