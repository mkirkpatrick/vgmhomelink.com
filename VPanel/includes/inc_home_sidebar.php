<? if(isset($_SESSION['REMOVE_WEBMASTER'])) {  ?>
    <h2><a style="text-decoration: none; cursor: pointer;" title="Click Here To Return To Non-Impersonation Mode" class="floatLeft" onclick="impersonate('false')"><?= $_SESSION['user_name'] ?></a></h2><br>
    <p class="url"><a href="/" target="_blank"><?= $_SERVER['ROOT_URI'];?></a></p>
    <? } else { ?>
    <h2 style="font-size: 18px"><?= $siteConfig['Company_Name'] ?></h2>
    <p class="url"><a href="/" target="_blank"><?= $_SERVER['ROOT_URI'];?></a></p>  
    <? } ?>
<? if(UserManager::isWebmaster()) { ?>
    <ul class="db-notifications">
        <li><h3 class="icon-key">Webmaster Tools</h3></li>
        <li><a style="cursor: pointer;" onclick="refreshSocialFeed();" title="Reset Home Page Social Feeds">Refresh Social Feed</a></li>
        <li><a style="cursor: pointer;" onclick="impersonate('true');" title="Hide Webmaster Only Items">Impersonate</a></li> 
        <li><a style="cursor: pointer;" href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>config/system.php" title="View System Information">System Information</a></li>               
        <li><a style="cursor: pointer;" href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>config/" title="View/Edit Site Configuration">Site Configuration</a></li>
        <li><a style="cursor: pointer;" href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>config/quality-assurance.php" title="View/Edit Quality Assurance Checklist">Quality Checklist</a></li>                              
    </ul>          
    <? } else {?>
    <ul class="db-notifications account-settings">
        <li>
            <a href="account/index.php" style="border-bottom: none"><h3 class="icon-user">Account Information</h3></a>
        </li>
        <li>
            <strong>Name: </strong>
            <span>
                <?= $_SESSION['user_name'] ?>
            </span>
        </li>
        <li>
            <strong>Username: </strong>
            <span>
                <?= $_SESSION['user_username'] ?>
            </span>
        </li>
        <li>
            <strong>Last Password Change: </strong>
            <? if(isset($_SESSION['REMOVE_WEBMASTER'])) { ?>
                <span><?= date("m/d/y g:i a", strtotime("-30 days")) ?>
                <? } else { ?>
                <span><?= time_elapsed_string($_SESSION['user_last_password_change']); ?>
                    <? } ?>

            </span>
        </li>
        <li>
            <strong>Last Login Date: </strong>
            <? if(isset($_SESSION['REMOVE_WEBMASTER'])) { ?>
                <span><?= date("m/d/y g:i a", strtotime("-3 days")) ?>
                <? } else { ?>
                <span><?= isNullOrEmpty($_SESSION['user_last_login']) ? "Now" : date("m/d/y g:i a", strtotime($_SESSION['user_last_login'])); ?></span>
                <? } ?>            

        </li>
    </ul>
    <? } ?>        
<? if(UserManager::hasRole(FORM_VIEWER_ROLE)) { ?>           
    <ul class="db-notifications">
        <li><a href="forms/index.php"><h3 class="icon-mic">Form Notifications</h3></a></li>
        <?  $sql = "SELECT * FROM tbl_form_custom WHERE cf_status = 'Active' ORDER BY cf_name";
        $record2 = Database::Execute($sql);
        while($record2->MoveNext()) { 
            $sql = "SELECT *
            FROM tbl_form_submission
            WHERE fs_archived <> 1 AND fs_viewed <> 1 AND cf_id = {$record2->cf_id}";
            $record = Database::Execute($sql); ?>
            <li><a href="/vpanel/forms/index.php?view=list&id=<?= $record2->cf_id ?>"><?= $record2->cf_name ?> <span><?= $record->Count() ?> New</span></a></li>    
            <?  } ?>

        <? 
        if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/VPanel/modules/forms/form-builder/FormBuilder.php')) {
            $sql = "SELECT * FROM tbl_cms_asset WHERE at_id = 7";
            $formBuilder = Database::Execute($sql);
            if($formBuilder->Count() > 0) {
                while($formBuilder->MoveNext()) {
                    $sql = "SELECT *
                    FROM tbl_cms_form_submissions
                    WHERE fs_reviewed <> 1 AND k = '{$formBuilder->as_key}'";
                    $record = Database::Execute($sql); ?>
                    <li><a href="/VPanel/modules/forms/index.php?view=results&id=<?= $formBuilder->as_id ?>"><?= $formBuilder->as_name ?> <span><?= $record->Count() ?> New</span></a></li>                            
                    <? }                       
            } 
        }

        if(isset($_WEBCONFIG['HAS_VPRESS_BLOG']) && $_WEBCONFIG['HAS_VPRESS_BLOG'] == 'true') { 
            $sql = "SELECT bpc_id FROM tbl_blog_post_comments WHERE bpc_status = 'Pending'";
            $commentCount = Database::Execute($sql); ?>
            <li><a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>modules/blog/comments/index.php?view=list&status=pending">Blog Comments <span><?= $commentCount->Count() ?> New</span></a></li>                                        
            <?  }    

        ?>                
    </ul>
    <? } ?>
<?php if(UserManager::hasRole(USER_MANAGER_ROLE) || UserManager::hasRole(AUDIT_LOG_MANAGER_ROLE)) { ?>
    <ul class="db-admin">
        <li><h3 class="icon-flash">User Administration</h3></li> 
        <?php 
        if(UserManager::hasRole(USER_MANAGER_ROLE)) {  
            print '<li><a href="/vpanel/users/index.php">Manage Users</a></li>
            <li><a href="/vpanel/users/index.php?view=add">Create Users</a></li>';
        }
        if(UserManager::hasRole(AUDIT_LOG_MANAGER_ROLE)) { 
            print '<li><a href="/vpanel/logs/index.php?view=logdata">Audit Log</a></li>';
        } ?>  
    </ul>
    <? } ?>