<?php 
if ($_SERVER['PHP_SELF'] == "/VPanel/index.php") { ?>

    <script type="text/javascript">

        $(window).load(function() {
            $(".tip-content-vpanel-overview").joyride({
                autoStart: true, 
                cookieMonster: true,
                cookieName: 'JoyRide',
                cookieDomain: false,
                modal: true

            });
        });  

        $(function() {
            $(".tutorials").click(function () {
                $.removeCookie('JoyRide');
                if ($.cookie('JoyRide') == null) {
                    instructions();
                }
            });
        });

        function instructions(){
            $('.tip-content-vpanel-overview').joyride({
                autoStart : true,
                'cookieMonster': true,           // true/false for whether cookies are used
                'cookieName': 'JoyRide',
                modal: true,
                expose: true,
                'postRideCallback' : function () {

                }
            });
        };
    </script>
    <?php }

if ($_SERVER['REQUEST_PATH'] == "/VPanel/pagebuilder/index.php") { ?>

    <script type="text/javascript">
        $(function() {
            $(".pagebuilderHelp").click(function () {
                $.removeCookie('JoyRide');
                if ($.cookie('JoyRide') == null) {             
                    $('.tip-content-pagebuilder').joyride({
                        autoStart : true,
                        'cookieMonster': true,           // true/false for whether cookies are used
                        'cookieName': 'JoyRide-PageBuilder',
                        modal: true
                    }); 
                }
            });
        });
    </script>

<?php } ?>

<!-- VPanel Overview -->
<ol class="tip-content-vpanel-overview" style="display: none;">
    <li data-class="icon-gauge" data-text="Next" style="widows: 250px;">
        <h2>Navigation</h2>
        <p>Add content with PageBuilder, view form submissions, edit modules, edit users, and much more with these navigation items.</p>
    </li>

    <li data-class="user-center" data-text="Next">
        <h2>Your Account Center</h2>
        <p>Manage your account, change your password, update your security questions, or even view your log of logins and changes.</p>
    </li>

    <li data-class="db-notifications" data-text="Next" data-options="tipLocation:left">
        <h2>Notifications and Admin Links</h2>
        <p>View notifications for your site, manage users or view the audit log here.</p>
    </li>

    <li data-id="user-analytics" data-text="Next" data-options="tipLocation:right">
        <h2>Analytics &amp; More</h2>
        <p>Here is where you can view analytics about your site including visits, popular pages, traffic Sources and more!</p>
    </li>

    <li data-id="technical" data-text="Close" data-options="tipLocation:top;">
        <h2>Submit a problem</h2>
        <p>If you run into any problems, errors or bugs please don't hesitate to submit the issue so we can quickly fix it.</p>
    </li>
</ol>	
	
<!-- Joyride Tip Content -->
<ol class="tip-content" style="display: none;">
    <li data-class="navigation" data-text="Next" style="widows: 250px;">
        <h2>Navigation</h2>
        <p>View analytics, add content with PageBuilder, view Contact messages, edit modules, and much more with these navigation items.</p>
    </li>

    <li data-class="user-center" data-text="Next">
        <h2>Your User Center</h2>
        <p>Manage your account, your password or email Forbin with any inquiries.</p>
    </li>

    <li data-class="db-notifications" data-text="Next" data-options="tipLocation:left">
        <h2>Notifications and Admin Links</h2>
        <p>View notifications for your site, manage users or view the audit log here.</p>
    </li>

    <li data-id="useranalytics" data-text="Next" data-options="tipLocation:right">
        <h2>Analytics &amp; More</h2>
        <p>Here is where you can view analytics about your site including visits, popular pages, traffic Sources and more!</p>
    </li>

    <li data-id="technical" data-text="Close" data-options="tipLocation:top;">
        <h2>Submit a problem</h2>
        <p>If you run into any problems, errors or bugs please don't hesitate to submit the issue so we can quickly fix it.</p>
    </li>
</ol>


<ol class="tip-content-pagebuilder" style="display: none;">
    <li data-class="home-page" data-text="Next" style="widows: 250px;">
        <h2>Pages</h2>
        <p>Change order, Edit, Delete, and add a subpage here</p>
    </li>

    <li data-class="user-center" data-text="Next">
        <h2>Your User Center</h2>
        <p>Manage your account, your password or email Forbin with any inquiries.</p>
    </li>

    <li data-class="db-notifications" data-text="Next" data-options="tipLocation:left">
        <h2>Notifications and Admin Links</h2>
        <p>View notifications for your site, manage users or view the audit log here.</p>
    </li>

    <li data-id="useranalytics" data-text="Next" data-options="tipLocation:right">
        <h2>Analytics &amp; More</h2>
        <p>Here is where you can view analytics about your site including visits, popular pages, traffic Sources and more!</p>
    </li>

    <li data-id="technical" data-text="Close" data-options="tipLocation:top;">
        <h2>Submit a problem</h2>
        <p>If you run into any problems, errors or bugs please don't hesitate to submit the issue so we can quickly fix it.</p>
    </li>
</ol>