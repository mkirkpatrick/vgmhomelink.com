<?php
if (isset($script)) {
    print  PHP_EOL . '<script type="text/javascript">' . PHP_EOL;
	for ($i = 0; $i < count($script); $i++) {
		if ($script[$i] != '') {
            print '<!-- JS File ' . filePathCombine($_WEBCONFIG['VPANEL_PATH'], $script[$i]) . ' -->' . PHP_EOL . PHP_EOL;
			include_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], $script[$i]);
		}
	}
    print '</script>';
}
?>

<?php /* Site Down Message */
if($_WEBCONFIG['SITE_DOWN_ACTIVE'] == "true" && !isset($_SESSION['SITE_DOWN_MESSAGE_SHOWN'])) {
	print '<!-- Site Down Message -->' . PHP_EOL;
	print '<script type="text/javascript">alert("' . $_WEBCONFIG['SITE_DOWN_MESSAGE'] . '"); </script>' . PHP_EOL;
	$_SESSION['SITE_DOWN_MESSAGE_SHOWN'] = true;
}
?>

<section class="contact" id="technical">
	<div class="container clearfix">
    	<h4 class="floatLeft">Has something gone haywire with VPanel? Try submitting a technical issue so we can look at it.</h4>


        <a  class="floatRight button yellow" style="margin-left: 5px" href="<?= $_WEBCONFIG['VPANEL_PATH']; ?>contact/index.php?view=technical">Submit a technical issue</a>

        <br /><br /><br />
        <a class="button yellow" href="/changelog.txt" target="_blank">VPanel Release Notes</a>
    </div>
</section>
<div class="overlay-mask"></div>
<script src="/VPanel/js/update-message.js"></script>
</body>
</html>
<?php ob_end_flush(); ?>