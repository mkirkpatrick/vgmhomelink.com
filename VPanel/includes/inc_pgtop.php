<?php   
security::_secureCheck();

global $siteConfig; 
define('PAGE_TITLE', $siteConfig['Company_Name']);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-us">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="Viewport" content="width=device-width" />
    <meta name="Application-Name" content="VPanel" />
    <meta name="Robots" content="noindex, nofollow, noarchive" />
    <title>VPanel :: <?= PAGE_TITLE;?></title>
    <noscript><meta HTTP-EQUIV="refresh" CONTENT="0; url=<?= $_WEBCONFIG['VPANEL_PATH'];?>nojs.php"></noscript>
    <script type="text/javascript">var sessionTimeout=<?= $_WEBCONFIG['SESSION_TIMEOUT'] ?>;</script>

    <link type="image/x-icon" rel="shortcut icon" href="<?= $_WEBCONFIG['VPANEL_PATH'];?>images/favicon.ico" />
    <link type="text/css" href="<?php autoCreateVerison($_WEBCONFIG['VPANEL_PATH'] . 'css/vpanel.css'); ?>" rel="stylesheet" />
    <link type="text/css" href="<?php autoCreateVerison($_WEBCONFIG['VPANEL_PATH'] . '/css/print.css'); ?>" rel="stylesheet" media="print" />
    <link href="//fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//kendo.cdn.telerik.com/2016.1.412/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="//kendo.cdn.telerik.com/2016.1.412/styles/kendo.default.min.css" />

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js"></script>
    <script type="text/javascript" src="//kendo.cdn.telerik.com/2016.1.412/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="<?php autoCreateVerison($_WEBCONFIG['VPANEL_PATH'] . 'jquery/jquery.tools.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php autoCreateVerison($_WEBCONFIG['VPANEL_PATH'] . 'jquery/js/common.js'); ?>"></script>
    <?php if(!UserManager::hasRole(DELETE_PERMISSION)) { ?>
    <style>
       .red { display: none; }
    </style>
    <?php } ?>
    <!-- Jquery Joy Ride Includes -->
    <script src="<?php autoCreateVerison($_WEBCONFIG['VPANEL_PATH'] . 'jquery/joyride/jquery.cookie.js'); ?>"></script>
    <script src="<?php autoCreateVerison($_WEBCONFIG['VPANEL_PATH'] . 'jquery/joyride/jquery.joyride-2.1.js'); ?>"></script>

    <script type="text/javascript" src="<?php autoCreateVerison($_WEBCONFIG['VPANEL_PATH'] . 'jquery/jquery.PrintArea.js'); ?>"></script>
    <script type="text/javascript" src="<?php autoCreateVerison($_WEBCONFIG['VPANEL_PATH'] . 'ckeditor/ckeditor.js'); ?>"></script>
    <script type="text/javascript" src="<?php autoCreateVerison($_WEBCONFIG['VPANEL_PATH'] . 'ckeditor/adapters/jquery.js'); ?>"></script>
</head>
<body class="">
    <?php ob_start(); ?>
    <a id="top"></a>