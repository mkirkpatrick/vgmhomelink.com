<nav class="navigation">
    <div class="container clearfix">
        <ul class="clearfix">
            <?php
			$className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}index.php") === false ? 'tab' : 'tab on';
			printf("<li id =\"overview\" class=\"nav\"><a href=\"%sindex.php\" class=\"%s icon-gauge\">Overview</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);

            if($_WEBCONFIG['SITE_TYPE'] == 'ECOMMERCE') {
                if($_WEBCONFIG['HAS_PAGEBUILDER_ACCESS'] == "true" || UserManager::isWebmaster()) {
                    $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}pagebuilder") === false ? 'tab' : 'tab on';
                    printf("<li class=\"nav\"><a href=\"%spagebuilder/index.php\" class=\"%s icon-doc-text\">Page Builder</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
                }

                $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}ecommerce") === false ? 'tab' : 'tab on';
                printf("<li class=\"nav\"><a href=\"%secommerce/index.php\" class=\"%s icon-basket\">E-Commerce</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
            } elseif($_WEBCONFIG['SITE_TYPE'] == 'CATALOG') {
                if(UserManager::hasRole(CONTENT_AUTHOR_ROLE) || UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) {
                    if($_WEBCONFIG['HAS_PAGEBUILDER_ACCESS'] == "true" || UserManager::isWebmaster()) {
                        $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}pagebuilder") === false ? 'tab' : 'tab on';
                        printf("<li class=\"nav\"><a href=\"%spagebuilder/index.php\" class=\"%s icon-doc-text\">Page Builder</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
                    }

                    $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}ecommerce") === false ? 'tab' : 'tab on';
                    printf("<li class=\"nav\"><a href=\"%secommerce/category/index.php\" class=\"%s icon-box\">Catalog</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);

                    if($_WEBCONFIG['HAS_MEMBER_LOGINS'] == "true") {
                        $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}members") === false ? 'tab' : 'tab on';
                        printf("<li class=\"nav\"><a href=\"%smembers/index.php\" class=\"%s icon-users\">Portal Users</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
                    }

                }
            } else {
                if(UserManager::hasRole(CONTENT_AUTHOR_ROLE) || UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) {
                    if($_WEBCONFIG['HAS_PAGEBUILDER_ACCESS'] == "true" || UserManager::isWebmaster()) {
                        $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}pagebuilder") === false ? 'tab' : 'tab on';
                        printf("<li class=\"nav\"><a href=\"%spagebuilder/index.php\" class=\"%s icon-doc-text\">Page Builder</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
                    }



                }
                if(UserManager::hasRole(8)) {
                    if($_WEBCONFIG['HAS_MEMBER_LOGINS'] == "true") {
                        $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}members") === false ? 'tab' : 'tab on';
                        printf("<li class=\"nav\"><a href=\"%smembers/index.php\" class=\"%s icon-users\">Portal Users</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
                    }
                }
            }

            if(UserManager::hasRole(FORM_VIEWER_ROLE)) {
                if(file_exists($_SERVER['DOCUMENT_ROOT'] . '\VPanel\modules\forms\index.php')) {
                    $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}modules/forms") === false ? 'tab' : 'tab on';
                    printf("<li class=\"nav\"><a href=\"%smodules/forms/index.php\" class=\"%s icon-mail\">Form Builder</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
                }
                $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}forms") === false ? 'tab' : 'tab on';
                printf("<li class=\"nav\"><a href=\"%sforms/index.php\" class=\"%s icon-mail\">Forms</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
            }
            // check to see if there are any modules, if not, don't show
            global $_WEBCONFIG;
            $count = 0;
            $modules = array();
            $dir = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "modules/");
            if (is_dir($dir)) {
                if ($dh = opendir($dir)) {
                    $count = 0;
                    while (($directory = readdir($dh)) !== false) {
                        $row = $count % 2 ? "odd" : "even";
                        $path =  $dir . "/" . $directory;
                        if(filetype($path) == 'dir' && $directory != ".." && $directory != ".") {
                            $settings = getWebConfigSettings($path);
                            //had to add second conditional below because Blog was showing up twice in header
                            if(($settings['MODULE_NAME'] != 'Form Builder') && ($settings['MODULE_NAME'] != 'Blog')) {
                                $count++;
                                $modules[] = $settings;
                            }
                        }
                    }
                    closedir($dh);
                }
            }
            // breakPoint($modules);

            // added this if block because the vpress tab wasn't showing up in the nav
            if ($_WEBCONFIG['HAS_VPRESS_BLOG'] === 'true') {
                $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}modules") !== false && strpos($_SERVER['PHP_SELF'], "inductee") == false  ? 'tab on' : 'tab';
                printf("<li class=\"nav\"><a href=\"%smodules/blog/index.php\" class=\"%s icon-doc-text\">VPress</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
            }

            if($count > 2){
                if(UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) {
                    if($_WEBCONFIG['SITE_TYPE'] != 'LANDING-PAGE') {
                        $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}modules") !== false && strpos($_SERVER['PHP_SELF'], "inductee") == false  ? 'tab on' : 'tab';
                        printf("<li class=\"nav\"><a href=\"%smodules/index.php\" class=\"%s icon-tools\">Modules</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
                    }
                    else {
                        $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}modules/marketing") !== false && strpos($_SERVER['PHP_SELF'], "inductee") == false  ? 'tab on' : 'tab';
                        printf("<li class=\"nav\"><a href=\"%smodules/marketing/index.php\" class=\"%s icon-chart-area\">Marketing</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
                    }
                }
            }
            elseif($count == 1 || $count == 2){
                if(UserManager::hasRole(CONTENT_AUTHOR_ROLE) || UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) {
                    if($_WEBCONFIG['SITE_TYPE'] != 'LANDING-PAGE') {

                        foreach ($modules as $key => $value) {
                            // breakPoint($value);
                            printf("<li class=\"nav\"><a href=\"%sindex.php\" class=\"%s icon-".$value['MODULE_ICON'] ."\">". $value['MODULE_NAME'] ."</a></li>\n", $_WEBCONFIG['VPANEL_PATH'] . $value['MODULE_FOLDER'], $className);
                            // <a href="/VPanel/modules/staff/index.php" class="icon-users">Staff</a>
                        }
                    }
                    else {
                        $className = strpos($_SERVER['PHP_SELF'], "{$_WEBCONFIG['VPANEL_PATH']}modules/marketing") !== false && strpos($_SERVER['PHP_SELF'], "inductee") == false  ? 'tab on' : 'tab';
                        printf("<li class=\"nav\"><a href=\"%smodules/marketing/index.php\" class=\"%s icon-chart-area\">Marketing</a></li>\n", $_WEBCONFIG['VPANEL_PATH'], $className);
                    }
                }
            }

			if(UserManager::hasRole(USER_MANAGER_ROLE)) {
				print ' <li class="nav"><a href="/vpanel/users/" class="icon-users">Users</a></li>';
			}

            if(UserManager::isMasterAdmin()) {
                print '<li class="nav"><a href="/vpanel/config/locations" class="icon-compass">Locations</a></li>';
            }

            if($_WEBCONFIG['SITE_TYPE'] != 'LANDING-PAGE') {
                //print '<li class="nav"><a class="icon-help-circled tutorials pagebuilderHelp" style="cursor: pointer">Help &amp; Guides</a></li>';
            }
            ?>
        </ul>
    </div>
</nav>