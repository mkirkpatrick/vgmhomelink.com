<header class="masthead" role="banner">
    <div class="utility-bar">
        <div class="container clearfix">
            <a id="VPanel Logo" href="<?= $_WEBCONFIG['VPANEL_PATH'];?>" title="Control Panel Home" class="logo"><img src="<?= $_WEBCONFIG['VPANEL_PATH'];?>images/logo.png" style="border: 0" alt="VGM Forbin VPanel Administration" class="back" /></a>
            <ul class="aux" style="float: left">
                <li style="margin: 15px 0 0 30px"><a href="<?= $_WEBCONFIG['SITE_URL'] ?>" target="_blank" title="View Site"> <?= $siteConfig['Company_Name'] ?></a></li>
            </ul>
            <ul class="aux">
                <?php    
                if(!UserManager::isWebmaster() && !isset($_SESSION['REMOVE_WEBMASTER'])) { ?>
                    <li id="UserName" class="user-center">
                    <a href="/vpanel/account/index.php" title="Manage Account" class="userinfo icon-down-custom">
                        <?= $_SESSION['user_username'] ?>
                    </a>
                    <ul class="clearfix">
                        <li><a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>account/index.php?view=profile" class="icon-right-open-mini">Update Account</a></li>
                        <li><a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>account/index.php?view=changepassword" class="icon-right-open-mini">Change Password</a></li>
                        <li><a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>account/index.php?view=editquestions" class="icon-right-open-mini">Change Security Questions</a></li>
                        <li><a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>account/index.php?view=logdata" class="icon-right-open-mini">Account Activity</a></li>
                    </ul>
                    <?php 
                } else { ?>
                    <li id="UserName" class="user-center">
                        <a title="Webmaster Tools" class="userinfo icon-down-custom pointer">
                            Hello <?= $_SESSION['user_username'] ?>
                        </a>
                        <?php
                        if(!isset($_SESSION['REMOVE_WEBMASTER'])) { ?>
                            <ul class="clearfix">
                                <li>
                                    <a style="cursor: pointer;" onclick="refreshSocialFeed();" class="icon-right-open-mini" title="Reset Home Page Social Feeds">Refresh Social Feed</a>
                                </li>
                                <li>
                                    <a style="cursor: pointer;" onclick="impersonate('true');" class="icon-right-open-mini" title="Hide Webmaster Only Items">Impersonate</a>
                                </li>
                                <li>
                                    <a style="cursor: pointer;" href="/vpanel/config/system.php" class="icon-right-open-mini" title="View System Information">System Information</a>
                                </li>
                            </ul> 
                            <?php 
                        } else { ?>
                            <ul class="clearfix">
                                <li><a class="icon-right-open-mini default-pointer">Update Account</a></li>
                                <li><a class="icon-right-open-mini default-pointer">Change Password</a></li>
                                <li><a class="icon-right-open-mini default-pointer">Change Security Questions</a></li>
                                <li><a class="icon-right-open-mini default-pointer">Account Activity</a></li>
                            </ul>                                
                            <?php
                        } ?>
                    </li>
                    <?php
                } ?>

                <li id="logout"><a class="logout icon-cancel-squared button red" href="?logout">Logout</a></li>
                <li><p id="qa-percent--header"></p></li>
            </ul>
        </div>
    </div>

    <?php 
    if(stripos($_SERVER['PHP_SELF'], "/VPanel/ecommerce") !== false && $_WEBCONFIG['SITE_TYPE'] == 'ECOMMERCE') {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/VPanel/includes/inc-ecommerce-nav.php');    
    } else {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/VPanel/includes/inc-nav.php'); 
    } ?>
</header>

<?php //include 'includes/joyRide/jquery-JoyRide.php';
if($_WEBCONFIG['SITE_STATUS'] != 'LIVE' && UserManager::isWebmaster()) {
    if($_WEBCONFIG['SITE_TYPE'] == 'BANK') {
        $siteType = 'Bank';
    } elseif($_WEBCONFIG['SITE_TYPE'] == 'CUSTOM') {
        $siteType = 'Custom';    
    } elseif($_WEBCONFIG['SITE_TYPE'] == 'ECOMMERCE' || $_WEBCONFIG['SITE_TYPE'] == 'CATALOG') {
        $siteType = 'Custom';    
    } elseif($_WEBCONFIG['SITE_TYPE'] == 'LANDING') {
        $siteType = 'Landing Page';    
    } else {
        $siteType = 'Custom';    
    }?>

    <script type="text/javascript">
        $(function(){
            $.ajax({
                type: "POST",
                url: "https://vguard.forbin.com/api/quality/get-percentage-complete.php",
                data: { token: 'CmwGRp9o5a6df6592Byd2hms9Q33', site: '<?= urlencode($_WEBCONFIG['SITE_URL']) ?>', type: '<?= $siteType ?>' },
                success: function(data){
                    if(data.length > 0){
                        $("#qa-percent--header").html("<a href=\"/VPanel/config/quality-assurance.php\">" + data + "%</a>");    
                    }                    
                },
                dataType: "text"
            });
        })
    </script>
    <?php 
} ?>