<?php
try {
	require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
	require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");

	$res = security::_ForgotPass();
	echo $res;

} catch (Exception $ex) {
    echo "Exception Caught: " . $ex->getMessage() .
         "<p>Trace: " . $ex->getTraceAsString() . "</p>";
}
?>