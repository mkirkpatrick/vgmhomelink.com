<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
require_once 'library/classes/class_visitor.php';
security::_secureCheck();

if($_WEBCONFIG['SITE_TYPE'] == 'LANDING-PAGE') {
    redirect(filePathCombine($_WEBCONFIG['VPANEL_PATH'], 'pagebuilder'));
}

if (!isset($_GET['logout']))
    $_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

if(isset($_GET['renew'])) {
    die('VPanel Session Renewed');
}

$rows = 0;

include_once 'includes/inc_pgtop.php';
include_once 'includes/inc_header.php';

//Functionality for hiding and showing the update message based off cookie
// Get date message was dismissed
$updateDismissedOn = isset($_COOKIE['update-message-dismissed-on']) ? new DateTime($_COOKIE['update-message-dismissed-on']) : null;

// Get timestamp of update file
$updateFile = $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . 'updates/index.php';
$updateFileUpdatedOn = file_exists($updateFile) ? new DateTime(date('m/d/Y', filemtime($updateFile))) : null;

$showUpdateMessage = true;

if ($updateDismissedOn && $updateFileUpdatedOn) {
    $dateDiff = $updateDismissedOn->diff($updateFileUpdatedOn);
    $daysSinceDismissal = (int)$dateDiff->format('%r%a');

    $showUpdateMessage = $daysSinceDismissal > 0 ? true : false;
}

?>
<section class="title">
    <div class="container clearfix">
        <?php if ($showUpdateMessage) { ?>
            <div class="notification information message" id="update-message">
                <p>
                    <strong class="msg-title">New VPanel Updates</strong>
                    <a href="/vpanel/updates">Click here</a> to view latest updates. Click the X to dismiss.
                    <input type="image" alt="X" src="/VPanel/images/icon-delete.png" class="xButton" title="Hide Notification">
                </p>
            </div>
        <? } ?>
        <h1 class="page-title icon-gauge">Overview</h1>
        <p class="page-desc">View your website traffic, sources, views and social statistics.</p>
        <a id="helper-button" class="tutorials floatRight pointer icon-help-circled" title="Tutorial: VPanel Overview"></a>
    </div>
</section>

<section class="maincontent" role="main">
    <div class="container clearfix">
        <aside class="left sidebar">
            <?php include_once($_SERVER['DOCUMENT_ROOT'] . '/VPanel/includes/inc_home_sidebar.php'); ?>
        </aside>

        <div id="user-analytics" class="right home clearfix subcontent last">
<!--    DONT REMOVE ME, PLEASE
         <div class="notification information message">
                <p>
                    <strong class="msg-title">Introducing VGuard</strong> Your option for the highest security, now with NetSparker Scanning Tools and SSL!
                    &nbsp;&nbsp;<a href="">View More Info</a>.
                    <input type="image" alt="X" src="/VPanel/images/icon_remove.gif" class="xButton" title="Hide Notification">
                </p>
            </div>-->
            <h2 class="heading icon-chart-line">30 Days Visits</h2>
            <div id="pageVisits" class="stats" style="height: 155px;">No Google Profile ID</div>

            <div class="fourcol">
                <h2 class="heading icon-rocket">Weekly Page Views</h2>
                <div id="pageViews" class="stats" style="height: 155px;">No Google Profile ID</div>
            </div>
            <div class="fourcol">
                <h2 class="heading icon-user">Traffic Sources</h2>
                <div id="traffic" class="stats" style="height: 155px;">No Google Profile ID</div>
            </div>

            <div class="fourcol last">
                <h2 class="heading icon-chart-bar">Top Visited Pages</h2>
                <div id="topVisited" class="stats" style="height: 155px;">No Google Profile ID</div>
            </div>
        </div>
    </div>
</section><!--End content-->

<?php if(strlen($siteConfig['Google_Profile_Id']) > 0 && $siteConfig['Google_Profile_Id'] != 'XXXXXXXX') { ?>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="https://services.forbin.com/VCharts/Service.aspx?profileid=<?= $siteConfig['Google_Profile_Id'] ?>&credentials=<?= $siteConfig['Google_Profile_Credentials'] ?>"></script>
    <script type="text/javascript">
        pageVisits30Days("pageVisits");
        weeklyPageViews("pageViews");
        trafficSources("traffic");
        topVisitedPages("topVisited")
    </script>
<?php }

include_once 'includes/inc_pgbot.php';
include_once $_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . 'includes/joyRide/jquery-JoyRide.php'; ?>