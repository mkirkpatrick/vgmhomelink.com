<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Page Error</title>
<link type="text/css" href="/vpanel/css/vpanel.css" rel="stylesheet" />
<link href="//fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
</head>
<body>

<header class="masthead" role="banner">
	<div class="utility-bar">
        <div class="container clearfix">
            <a href="/VPanel/" title="Control Panel Home" class="logo"><img src="/VPanel/images/logo.png" style="border: 0" alt="VGM Forbin VPanel Administration" class="back"></a>
            
            <ul class="aux">
                <li id="logout"><a class="logout icon-cancel-squared button red" href="?logout">Logout</a></li>
            </ul>
        </div>
    </div>
</header>

<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-code">No Javascript?</h1>
        <p class="page-desc">We'll need you to flip that on quick.</p>
    </div>
</section>

<section class="maincontent">
	<div class="container clearfix">
        <div class="fullcontent">

    		<h2>Page Error</h2>

    <p>You have experienced an error. Please try your action again later and contact us if the error persists. We apologize for the inconvenience.</p>

    <div style="font-weight:bold;">JavaScript is turned off in your web browser. 
    Turn it on to take full advantage of the website.</div>
	
			<p>VGM Forbin Customer Support<br />
			Phone: 877-814-7485 <br />
			Email: <a href="mailto: support@forbin.com">support@forbin.com</a>
			</p>
	
</div>
</div>
</section>

</body>
</html>

