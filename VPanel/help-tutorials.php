<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once 'library/classes/class_security.php';
require_once 'library/classes/class_visitor.php';

security::_secureCheck();

if (!isset($_GET['logout'])) 
    $_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

include 'includes/inc_pgtop.php';
include 'includes/inc_header.php';
?>
<style>
#player {
    height:450px;
    display:block;
}
</style>

<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-book">Help &amp; Guides</h1>
        <p class="page-desc">Get more information on how the VPanel works.</p>
    </div>
</section>

<section class="maincontent">
<div class="container clearfix">
<div class="fullcontent">

  <h2>Administration Panel Help &amp; Tutorials</h2>
  
  <div class="assetOverview">
    <div class="btn_view"><a href="<?= $_WEBCONFIG['VPANEL_PATH'];?>help/index.html" target="_blank" class="button blue">View</a></div>
    <h2>Online Help Guide & Search</h2>
        View the online VPanel help guide. 
    </div>
  <div class="assetOverview even">
    <div class="btn_view"><a href="<?= $_WEBCONFIG['VPANEL_PATH'];?>help/VPanelHelpGuide.pdf" target="_blank" class="button blue">View</a></div>
    <h2>PDF Help Guide</h2>
        View or download the PDF version of the help guide.
    </div>
    <br /><a href="http://get.adobe.com/reader/" target="_blank"><img src="<?= $_WEBCONFIG['VPANEL_PATH'];?>help/images/get_adobe_reader.gif" alt="Get Adobe Reader" /></a>
      
</div><!--End content-->
</div>
</section>

<?php
$script    = array('jquery/flowplayer-3.2.6.min.js', 'jquery/pages/help-tutorials.js');
include 'includes/inc_pgbot.php';
?>