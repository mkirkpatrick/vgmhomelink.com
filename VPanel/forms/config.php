<?php 
if (!isset($siteConfig)) die("System Error!");
$myForms = Repository::getAll($_WEBCONFIG['COMPONET_TABLE']); 
?>

	<div class="subcontent right last">
	    <h2>Site Form Configurations</h2>
	    <p>Below is a list of your available forms on your site.</p>
	    <?php foreach($myForms as $form) { ?>
	        <div class="assetOverview even">
	            <div class="buttons clearfix">
	                <a href="index.php?view=modify&id=<?= $form->cf_id ?>" class="button floatRight green">Manage</a>
	            </div>
	            <h2><?= $form->cf_name ?></h2><br />
	        </div>
	    <?php } ?>
	</div>
