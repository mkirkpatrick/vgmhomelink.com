<?php
define('SKIP_ERROR_HANDLER', "N");
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/PHPExcel/Classes/PHPExcel.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/PHPExcel/Classes/PHPExcel/IOFactory.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_vEncryption.php");

security::_secureCheck();

ini_set('max_execution_time', 480);
set_time_limit(480);

$debugMode = false; /* Set to true to show error messages */
$dataEncrypted = isset($_WEBCONFIG['ENABLE_FORM_ENCRYPTION']) && $_WEBCONFIG['ENABLE_FORM_ENCRYPTION'] == "true";

if($dataEncrypted) {
    $valueEncrypt  = new vEncryption(FORM_ENCRYPTION_KEY);
}

function buildExcelHeader($sql) {
    // Create sub headers
    $arHeader = array();
    $submission	= Database::Execute($sql);

    if ($submission->Count() > 0) {
        if (!in_array("Date Submitted", $arHeader))
            $arHeader[] = "Date Submitted";
        if (!in_array("Admin Comments", $arHeader))
            $arHeader[] = "Admin Comments";
        $submission->MoveNext();
        $data = json_decode($submission->fs_form_data);
        foreach($data as $key=>$value) {
            $arHeader[] = $key;
        } // end for each
    }

    return $arHeader;
}//end function

$iNum		= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
$filterId	= isset($_GET['fid']) ? (int) $_GET['fid'] : 1;

$sql = "SELECT cf_name AS name
        FROM tbl_form_custom
        WHERE cf_id = $iNum
        LIMIT 1";
$form = Database::Execute($sql);
$form->MoveNext();

if ($form->Count() == 0) {
    throw new Exception("Unable to find record in the database");
}

switch ($filterId) {
    case 1:
        $subject	= $form->name . ' - Unread Only';
        $filterSQL	= "AND fs_archived = 0";
        break;
    case 2:
        $subject	= $form->name . ' - Archived Only';
        $filterSQL	= "AND fs_archived = 1";
        break;
    default:
        $subject	= $form->name . ' - All';
        $filterSQL	= "";
        break;
}

if(isset($_GET['sdate']) && isset($_GET['edate']) && !isNullOrEmpty($_GET['sdate']) && !isNullOrEmpty($_GET['edate'])) {
    $startDate = Database::quote_smart($_GET['sdate']);
    $endDate = Database::quote_smart($_GET['edate']);
    $startDate = date('Y-m-d', strtotime($startDate));
    $endDate = date('Y-m-d', strtotime($endDate));
    $endDate = date('Y-m-d', strtotime('+1 day' . $endDate));
    $filterSQL .= " AND fs_date_submitted BETWEEN '$startDate' AND '$endDate'";
}
if(!$debugMode) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");;
    header("Content-Disposition: attachment;filename={$_WEBCONFIG['SITE_DISPLAY_URL']}.xlsx");
    header("Content-Transfer-Encoding: binary ");
}

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("VPanel")
->setLastModifiedBy("VPanel")
->setTitle($form->name)
->setSubject($subject)
->setDescription($subject . " document for Office 2007 XLSX.");
$objPHPExcel->setActiveSheetIndex(0);

// Create first header
$objPHPExcel->getActiveSheet()->setCellValue('A1', $subject . ' - ' . date("m/d/Y"));
$objPHPExcel->getActiveSheet()->getStyle('J1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFill()->getStartColor()->setARGB('D1AB2F');
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setName('Candara');
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('J1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->mergeCells('A1:AA1');

$sql = "SELECT `fs_form_data`
        FROM `tbl_form_submission`
        WHERE cf_id = $iNum $filterSQL
        ORDER BY `fs_date_submitted` DESC
        LIMIT 1;";
$arHeader = buildExcelHeader($sql);
$hCtr	  = sizeof($arHeader);
$row	  = 2;
for ($col=0; $col<$hCtr; $col++) {
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $arHeader[$col]);
    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array(
        'font' => array('bold' => true),
        'color' => array('argb' => '094C7D'),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
    ));
}//end for

$arData		= array_flip($arHeader);

$sql = "SELECT `fs_id`, `cf_id`, `fs_admin_comments` as admin_comments, `fs_date_submitted` as date_submitted, fs_archived, `fs_form_data`
        FROM `tbl_form_submission`
        WHERE cf_id = $iNum $filterSQL
        ORDER BY `fs_date_submitted` DESC;";
$submission	= Database::Execute($sql);
if ($submission->Count() > 0) {
    $col = 0;

    while ($submission->MoveNext()) {
        $col = 0;
        $row++;
        $arData["Date Submitted"] = date("m/d/Y g:i:s A", strtotime($submission->date_submitted));
        $arData["Admin Comments"] = preg_replace("#<br\s*/?>#i", "\n", $submission->admin_comments);

        $data = json_decode($submission->fs_form_data);

        if ($data) {
            foreach($data as $key=>$value) {
                if (array_key_exists($key, $arData)) {
                    if($dataEncrypted && strlen(trim($value)) > 0) {
                        $value = $valueEncrypt->decrypt($value);
                    }
                    $arData[$key] = preg_replace("#<br\s*/?>#i", "\n", $value);
                }
            }

            foreach ($arData as $Data) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $Data);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array(
                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                ));
                $col++;
            }//end foreach
        }
    }//end while submission

    // Auto Size all columns
    for ($col=0; $col<$hCtr; $col++) {
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
    }

    // Freeze panes
    $objPHPExcel->getActiveSheet()->freezePane('A3');
} else {
    // No records founc
    $objPHPExcel->getActiveSheet()->setCellValue('B3', "THERE'S NO INFORMATION AVAILABLE AT THIS MOMENT!");
}
$objPHPExcel->getActiveSheet()->setTitle(substr($form->name, 0, 30));
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>