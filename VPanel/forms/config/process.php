<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once 'class.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 

$p = new process('tbl_form_custom');
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {

    case 'add' :
        $p->_add();
        break;
        
	case 'modify' :
		$p->_modify();
		break;
		
	case 'view' :
		$p->_view();
		break;

	case 'delete' :
		$p->_delete();
		break;
		
	default :
		throw new Exception("An unknown action executed!");
}
redirect("index.php?{$p->params}");
?>