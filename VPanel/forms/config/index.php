<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 

security::_secureCheck();
security::_webmasterCheck(filePathCombine($_WEBCONFIG['VPANEL_PATH'], 'forms/index.php'));

if (!isset($_GET['logout'])) 
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

print '<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-' . $_WEBCONFIG['COMPONENT_ICON'] . '">' . $_WEBCONFIG['ENTITY_NAME_PLURAL'] . '</h1>
        <p class="page-desc">' . $_WEBCONFIG['COMPONET_DESCRIPTION'] . '</p>';
          if(UserManager::isWebmaster()) {
                print '<a href="' . $_WEBCONFIG['VPANEL_PATH'] . 'forms/config/index.php" class="floatRight button yellow" style="margin-right: 5px;">Manage Forms</a>';
          }
  print '</div>
</section>';
print '<section class="maincontent"><div class="container clearfix">';

$rows = 0;
$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'view' :
        require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/forms/formsNavBar.php');
		include_once 'view.php';		
		break;
	
	case 'add' :	
	case 'modify' :
        require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/forms/formsNavBar.php');
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/form.js');
		include_once 'add-modify.php';		
		break;
		
	case 'list':
        require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/forms/formsNavBar.php'); 
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/list.js');
		include_once 'list.php';
		break; 
		
	case 'config': 
		include_once 'config.php';
		break; 

	default :
        require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/forms/formsNavBar.php'); 
        $script    = array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/list.js');
        include_once 'list.php';
        break;
}

print '</div></section>';

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>