// JavaScript Document
$(document).ready(function () {
    $("#grid").kendoGrid({ 
        dataSource: { pageSize: 10 },
        groupable: false,
        filterable: { messages: {
                info: "Search Filter:", 
                filter: "Filter", 
                clear: "Clear"
            }, 
            extra: false,
            operators: {
                string: {
                    contains: "Contains", 
                    eq: "Exact Match",
                    startswith: "Starts With", 
                    endswith: "Ends With", 
                    doesnotcontain: "Does Not Contain", 
                    neq: "Does Not Match"
                }
            }
        }, 
        columnMenu: false,
        resizable: true,
        reorderable: true,
        sortable: { mode: "multiple", allowUnsort: true},
        pageable: { refresh: true, pageSizes: [10, 25, 50, 100] }
    });
});

// Button Functions
function modifyForm(Id) {
	window.location.href = 'index.php?view=modify&id=' + Id ;
}

function deleteForm(Id) {
	if (confirm('Are you sure you want to delete this form?')) {
		window.location.href = 'process.php?actionRun=delete&id=' + Id;
	}
}

function addForm(Id, formID) {
	window.location.href = 'index.php?view=add';
}