// JavaScript Document
$(function() {
	// add * to required field labels
    $('label.required').append('&nbsp;<font color="#990000"><b>*</b></font>&nbsp;');

    CKEDITOR.config.bodyClass = 'content';
	CKEDITOR.config.autoGrow_maxHeight = 150;
	CKEDITOR.config.autoGrow_minHeight = 80;
	CKEDITOR.config.resize_enabled = true; 
	CKEDITOR.config.toolbarCanCollapse = true; 
	CKEDITOR.config.contentsCss = [ '/css/main.css','<?= $_WEBCONFIG["VPANEL_PATH"] ?>css/ckeditor-override.css'];
	CKEDITOR.config.toolbar =
	[
			{ name: 'document',    items : [ 'Source' ] },
			{ name: 'clipboard',   items : [ 'Cut','Copy','Paste' ] },
			{ name: 'editing', items: ['SelectAll', '-', 'SpellChecker', 'Scayt', 'Bold','Italic','Underline','Strike', 'TextColor', 'BGColor' ] },

	];

	CKEDITOR.replace( 'mtxSuccessMessage',{
		removePlugins : 'maximize,resize'
	} );
	CKEDITOR.config.width = '620';
	CKEDITOR.config.height = '80';
});
