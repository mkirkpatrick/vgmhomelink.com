<?php
if (!isset($siteConfig)) die("System Error!");

?>

<div class="subcontent right last">

  <?php
	$sql = "SELECT *
			FROM tbl_form_custom
            WHERE cf_status = 'Active'";
	$form = Database::Execute($sql);
		
	print '<h2>Form Management</h2>';
	
		  if ($form->getNumErrors() > 0) {
  			$errors	= $form->getErrorArray();
  			foreach ($errors as $err) echo $err;
		  } else if (isset($_SESSION['processed'])) {
  			switch($_SESSION['processed']) {
  				case 'added':
                    echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
  					break;
  				case 'updated':
                    echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
  					break;
  				case 'archive':
                    echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Archived Successfully!')</script>";                                    
  					break;
  				case 'unarchive':
                    echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Un-Archived Successfully!')</script>";
  					break;
  				case 'deleted':
                    echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
  					break;
  			}
  			unset($_SESSION['processed']);
		  }

		print $form->Count() == 1 ? "<p>There is currently <b>1</b> form.</p>\n" : "<p>There are currently <b>" . $form->Count() . "</b> forms.</p>\n";
  ?>

  <table id="grid">
	<thead> 
	  <tr> 
		<th>ID</th>
        <th>Form Name</th>
		<th>Form Recipients</th>
        <th>Date Added</th>
		<th>Last Modified</th>
        <th data-sortable="false" data-filterable="false" style="text-align: center;">Options</th>
	  </tr>
	</thead>

	<tbody>

  <?php
		if ($form->Count() > 0) {
			while ($form->MoveNext()) {
                $dateAdded    = date("m/d/y g:i A", strtotime($form->cf_date_added));
  				$dateModified = date("m/d/y g:i A", strtotime($form->cf_last_updated));
				print '<tr>'; 
                 print '<td>' . $form->cf_id . '</td>';
                 print '<td>' . $form->cf_name . '</td>';
                 print '<td>' . $form->cf_recipients . '</td>';
                 print '<td>' . $dateAdded . '</td>';
                 print '<td>' . $dateModified . '</td>';
                 print '<td>
                            <div align="center">
                                <a href="javascript:modifyForm(' . $form->cf_id . ');" class="blue button-slim">Modify</a>
                                <a href="javascript:deleteForm(' . $form->cf_id . ');" class="red button-slim">Delete</a>       
				           </div>
                        </td></tr>' . PHP_EOL;
			}
		}
  ?>

	</tbody>
  </table>

    <div class="buttons clearfix">
	    <a href="javascript: history.back()" class="floatLeft button silver">Back</a>
        <a href="javascript:addForm()" class="floatRight button green">Create New Form</a>        
    </div><!--End continue-->

</div><!--End content-->