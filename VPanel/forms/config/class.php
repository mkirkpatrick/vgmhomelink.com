<?php
class process extends Database { 

	private $record;
	private $table;
	public $params;

	//------------------------------
	public function __construct($table) {
	//------------------------------
		parent::__construct();
		$this->table = $table;
	}

    //------------------------------
    public function _add() {
    //------------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=add";
        } else {
            // No Errors
            $this->record->cf_date_added   = date("Y-m-d G:i:s");
            $this->record->cf_last_updated = date("Y-m-d G:i:s");
            Repository::Save($this->record); 
            $iNum = Database::getInsertID(); 
            Security::_addAuditEvent("Form Added", "Success", "Added -- '{$this->record->cf_name}' (" . $iNum .")", $_SESSION['user_username']);            
            $_SESSION['processed'] = 'added';
            $this->params = "view=list";
        }
    }
    	
	//------------------------------
	public function _modify() {
	//------------------------------
		global $_WEBCONFIG, $form;
		
		$iNum = (int)$_POST['hidId'];
		$this->record = Repository::getByID('tbl_form_custom', $iNum); 
		self::_verify();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			$this->params = "view=modify&id=$iNum";
		} else {
			// No Errors 
            $this->record->cf_last_updated = date("Y-m-d G:i:s", time());
			$this->record->update();
            Security::_addAuditEvent("Form Modified", "Success", "Modified -- '{$this->record->cf_name}' (" . $iNum .")", $_SESSION['user_username']);
			$_SESSION['processed'] = 'updated';
			$this->params = "view=list";
		}
	}

	//------------------------------
	public function _delete() {
	//------------------------------
		$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

        $sql = "SELECT * FROM tbl_form_custom WHERE cf_id = $iNum LIMIT 1";
        $form = Database::Execute($sql);
        $form->MoveNext();
        
		Database::ExecuteRaw("UPDATE `tbl_form_custom` SET cf_status = 'Deleted' WHERE `cf_id` = $iNum");
		Security::_addAuditEvent("Form Deleted", "Success", "Deleted -- '{$form->cf_name}' (" . $iNum .")", $_SESSION['user_username']);				
		$_SESSION['processed'] = 'deleted';
		$this->params = "view=list";
	}

	//------------------------------
	private function _verify() {
	//------------------------------
		global $form;

		$fieldName	= "txtName";
		$fieldValue	= $_POST[$fieldName];
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Form Name is required.");
		} else {
			$this->record->cf_name = $fieldValue;
		}
		
		$fieldName	= "txtPrimary";
		$fieldValue	= $_POST[$fieldName];
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Primary Field is required.");
		} else {
			$this->record->cf_primary_field = $fieldValue;
		}
		
		$fieldName	= "txtSecondary";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		$this->record->cf_secondary_field = $fieldValue;
		
		$fieldName	= "txtTeriary";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		$this->record->cf_teriary_field = $fieldValue;
        
        $fieldName  = "txtReplyTo";
        $fieldValue = strip_tags($_POST[$fieldName]);
        $this->record->cf_reply_to_field = $fieldValue;        
		
		$fieldName	= "txtRequiredFields";
		$fieldValue	= $_POST[$fieldName];
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$this->record->cf_required_fields = null; 
		} else {
			$this->record->cf_required_fields = $fieldValue;
		}
		
		$fieldName	= "txtDelimitor";
		$fieldValue	= $_POST[$fieldName];
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Delimitor is required.");
		} else {
			$this->record->cf_delimitor = $fieldValue;
		}
		
		$fieldName	= "txtFieldLimit";
		$fieldValue	= (int) $_POST[$fieldName];
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Field Limit is required.");
		} else {
			$this->record->cf_field_limit = $fieldValue;
		}
        
        $fieldName  = "txtReceipients";
        $fieldValue = $_POST[$fieldName];
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Form Recipients are required.");
        } else {
            $this->record->cf_recipients = $fieldValue;
        }        
                             
		$fieldName	= "mtxSuccessMessage";
		$fieldValue	= $_POST[$fieldName];
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Success Message is required.");
		} else {
			$this->record->cf_success_message = $fieldValue;
		}
	}
};
?>