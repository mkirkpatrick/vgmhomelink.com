<?php
if (!isset($siteConfig)) die("System Error!");
$iNum = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
$view = isset($_GET['view']) ? $_GET['view'] : '';

if ($form->getNumErrors() > 0) {
	$hidId				= $form->value("hidId");
	$txtName			= $form->value("txtName");
	$txtPrimary			= $form->value("txtPrimary");
	$txtSecondary		= $form->value("txtSecondary");
    $txtTeriary         = $form->value("txtTeriary");
	$txtReplyTo			= $form->value("txtReplyTo");
	$txtRequiredFields	= $form->value("txtRequiredFields");
	$txtDelimitor		= $form->value("txtDelimitor");
	$txtFieldLimit		= $form->value("txtFieldLimit");
	$txtReceipients		= $form->value("txtReceipients");
    $mtxSuccessMessage	= $form->value("mtxSuccessMessage", 'N');
} else if ($iNum > 0) {
	$formConfig = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $iNum);
	if (is_null($formConfig)) {
		throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
	}
    
	$hidId				= $formConfig->cf_id;
	$txtName			= $formConfig->cf_name;
	$txtPrimary			= $formConfig->cf_primary_field; 
	$txtSecondary		= $formConfig->cf_secondary_field; 
    $txtTeriary         = $formConfig->cf_teriary_field; 
	$txtReplyTo			= $formConfig->cf_reply_to_field; 
    $mtxSuccessMessage	= $formConfig->cf_success_message;
	$txtRequiredFields	= $formConfig->cf_required_fields; 
	$txtDelimitor		= $formConfig->cf_delimitor; 
	$txtFieldLimit		= $formConfig->cf_field_limit; 
	$txtReceipients		= $formConfig->cf_recipients; 
} else {
	$hidId				= null; 
	$txtName			= '';
	$txtPrimary			= 'Name';
	$txtSecondary		= 'Email';
    $txtTeriary         = 'Phone';
	$txtReplyTo			= 'Email';
	$txtRequiredFields  = 'name|phone|email|comments';
	$txtFieldLimit 		= 25; 
	$txtReceipients		= 'forbinprogrammers@forbin.com';
	$txtDelimitor		= '<br />'; 
    $mtxSuccessMessage	= '<p>Your contact request has been submitted successfully. A representative will be contacting you soon. </p>';
}
?>
<?php if(UserManager::isWebmaster()) { ?> 
    <form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">
    <input type="hidden" name="actionRun" value="<?= $view ?>" />
    <input type="hidden" name="hidId" value="<?= $hidId; ?>" />
    <div class="subcontent last right">
    <?php
	    print '<h2>' . $_WEBCONFIG['ENTITY_NAME'] . ' -- (' . ucfirst($view) . ')</h2>';
        
	    if ($form->getNumErrors() > 0) {
		    $errors	= $form->getErrorArray();
		    foreach ($errors as $err) echo $err;
	    } else if (isset($_SESSION['processed'])) {
            echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
		    unset($_SESSION['processed']);
	    }
	    ?>
		    <p>Insert the record information below and save.</p>

		    <table class="grid-display" style="width: 100%; border: 0; border-collapse: collapse;" cellpadding="5">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 145px"><label for="txtName" class="required"><b>Form Name</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtName" id="txtName" value="<?= $txtName; ?>" size="40" maxlength="255" required="required" <?= $view == "modify" ? 'readonly="readonly"' : '' ?> />
                    <?= $form->error("txtName");?>
                </td>
            </tr>
		    <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtPrimary" class="required"><b>Primary Field</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtPrimary" id="txtPrimary" value="<?= $txtPrimary; ?>" size="40" maxlength="255" required="required" />
                    <?= $form->error("txtPrimary");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtSecondary"><b>Secondary Field</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtSecondary" id="txtSecondary" value="<?= $txtSecondary; ?>" size="40" maxlength="255" />
                    <?= $form->error("txtSecondary");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtTeriary"><b>Teriary Field</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtTeriary" id="txtTeriary" value="<?= $txtTeriary; ?>" size="40" maxlength="255" />
                    <?= $form->error("txtTeriary");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtReplyTo"><b>Reply To Field</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtReplyTo" id="txtReplyTo" value="<?= $txtReplyTo; ?>" size="40" maxlength="255" />
                    <?= $form->error("txtReplyTo");?>
                </td>
            </tr>            
		    <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtRequiredFields"><b>Required Fields</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtRequiredFields" id="txtRequiredFields" value="<?= $txtRequiredFields; ?>" size="40" maxlength="255" />
                    <?= $form->error("txtRequiredFields");?>
                </td>
            </tr>
		    <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtDelimitor" class="required"><b>Form Delimitor</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtDelimitor" id="txtDelimitor" value="<?= $txtDelimitor; ?>" size="40" maxlength="255" required="required" />
                    <?= $form->error("txtDelimitor");?>
                </td>
            </tr>
		    <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtFieldLimit" class="required"><b>Form Field Limit</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="number" name="txtFieldLimit" id="txtFieldLimit" value="<?= $txtFieldLimit; ?>" size="5" maxlength="255" required="required" />
                    <?= $form->error("txtFieldLimit");?>
                </td>
            </tr>
		    
		    <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtReceipients" class="required"><b>Recipients</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtReceipients" id="txtReceipients" value="<?= $txtReceipients; ?>" size="56" maxlength="255" required="required" />
                    <?= $form->error("txtReceipients");?><label valign="center">NOTE: For each additional email separate by a semi colon (;)</label>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
			    <td style="text-align: right; vertical-align: top; width: 125px">
					    <label for="mtxSuccessMessage" class="required"><b>Success Message</b></label></td>
                <td style="text-align: left; vertical-align: top;">
					    <textarea name="mtxSuccessMessage" id="mtxSuccessMessage"><?= $mtxSuccessMessage; ?></textarea>
                    <?= $form->error("mtxSuccessMessage");?>
                </td>
            </tr>
	    </table>



    <div class="buttons clearfix">
	    <a href="javascript: history.back()" class="floatLeft silver button">Back</a>
        <input name="btnModify" type="submit" class="button green floatRight" id="btnModify" value="Save Changes">
    </div>
    </div><!--End continue-->

    </form>
<?php }
   else {
        print '<div class="notification message error"><p>You Must Be A Forbin Employee to View this Section!</p></div>';     
   } ?>