$(document).ready(function () {
/* Add Loader to grid while its loading */
    $("#grid").before('<div id="grid-loader" class="k-loading-mask" style="width:80%;height:38%"><span class="k-loading-text">Loading...</span><div class="k-loading-image"><div class="k-loading-color"></div></div></div>');
    
    var kendoPageSize = 10;
    $("#grid").kendoGrid({
        dataSource: {
            schema: {
                model: {
                    id:"Id",
                    fields: {
                        field1: { type: "string" },
                        field2: { type: "string" },
                        field3: { type: "string" },
                        lastDate: { type: "date" },
                        options: { type: "string" }
                    }
                }
            },
            pageSize: kendoPageSize
        },
        columns: [
            { field: "field1", width: 150, filterable: true },
            { field: "field2", width: 150, filterable: true },
            { field: "field3", width: 150, filterable: true },
            { field: "lastDate", width: 150, filterable: false, format: "{0:MMM dd, yyyy @ h:mm tt}" },
            { field: "options", width: 125, filterable: false, sortable: false }
        ],
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",
                    eq: "Exact Match",
                    startswith: "Starts With",
                    endswith: "Ends With",
                    doesnotcontain: "Does Not Contain",
                    neq: "Does Not Match"
                }
            }
        },
        pageable: { pageSizes: [25, 50, 100, 500] },
        columnMenu: false,
        groupable: false,
        sortable: true,
        scrollable: true,
        resizable: true,
        reorderable: true,
        toolbar : [
            { template: kendo.template($("#categoryTemplate").html()) },
            { template: kendo.template($("#searchTemplate").html()) }
        ], 
        dataBound: function(e){
            setTimeout(function(){
                $("#grid-loader").hide();    
                $("#grid").fadeIn();    
            }, 1000);
        } 
    });
    
    if(getInternetExplorerVersion() <= 8 && getInternetExplorerVersion() >= 5) {
        $('.toolbar').hide();
    } else {
        $("#clearTextButton").kendoButton({icon: "funnel-clear"});
        $("#txtSearch").on("keyup keypress onpaste", function () {
            var filter = { logic: "or", filters: [] };
            $searchValue = $(this).val();
            if ($searchValue.length > 1) {
                $.each($("#grid").data("kendoGrid").columns, function( key, column ) {
                    if(column.filterable) {
                        filter.filters.push({ field: column.field, operator:"contains", value:$searchValue});
                    }
                });

                $("#grid").data("kendoGrid").dataSource.query({ filter: filter });

                var count = $("#grid").data("kendoGrid").dataSource.total();
                $(".k-pager-info").html("1 - " + count + " of " + count + " items");

            } else if($searchValue == "") {
                $("#grid").data("kendoGrid").dataSource.query({
                    page: 1,
                    pageSize: kendoPageSize
                });
            }
        });

        $("#clearTextButton").on("click", function () {
            $("#grid").data("kendoGrid").dataSource.query({
                page: 1,
                pageSize: kendoPageSize
            });
            $("#txtSearch").val('');
        });
    }    
    
	
	$("#sdate").kendoDatePicker();
	$("#edate").kendoDatePicker();
});

// Button Functions
function modify(Id, formID) {
	window.location.href = 'index.php?view=view&id=' + Id + '&fId=' + formID;
}

function archive(Id, formID) {
	if (confirm('Are you sure you want to move this submission into the archived folder?')) {
		window.location.href = 'process.php?actionRun=archive&id=' + Id + '&fId=' + formID;
	}
}

function unarchive(Id, formID) {
	if (confirm('Are you sure you want to mark this submission as new?')) {
		window.location.href = 'process.php?actionRun=unarchive&id=' + Id + '&fId=' + formID;
	}
}

function remove(Id, formID) {
	if (confirm('Are You Sure?')) {
		window.location.href = 'process.php?actionRun=delete&id=' + Id + '&fId=' + formID;
	}
}
