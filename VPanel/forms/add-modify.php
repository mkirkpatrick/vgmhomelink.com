<?php
if (!isset($siteConfig)) die("System Error!");
$iNum    = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
if ($form->getNumErrors() > 0) {
    $hidId                = $form->value("hidId");
    $txtReceipients        = $form->value("txtReceipients");
    $mtxSuccessMessage    = $form->value("mtxSuccessMessage", 'N');
} else if ($iNum > 0) {
    $formConfig = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $iNum);
    if (is_null($formConfig)) {
        throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
    }    
    
    $hidId                = $formConfig->cf_id;
    $mtxSuccessMessage    = $formConfig->cf_success_message;
    $txtReceipients       = $formConfig->cf_recipients; 
} else {
    $hidId                = null; 
    $txtReceipients       = ''; 
    $mtxSuccessMessage    = '<p>Your contact request has been submitted successfully. A representative will be contacting you soon. </p>';
}
?> 
<form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">
<input type="hidden" name="actionRun" value="<?= $_GET['view'] ?>" />
<input type="hidden" name="hidId" value="<?= $hidId; ?>" />
<div class="subcontent last right">
    <?php
    print '<h2>Form Configuration</h2>';
    if ($form->getNumErrors() > 0) {
        $errors    = $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
        unset($_SESSION['processed']);
    }
    ?>
    <p>Insert the record information below and save.</p>

    <table class="grid-display" style="width: 100%; border: 0; border-collapse: collapse;" cellpadding="5">
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 145px"><label for="txtReceipients" class="required"><b>Recipients</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtReceipients" id="txtReceipients" value="<?= $txtReceipients; ?>" size="56" maxlength="255" required="required" />
                <?= $form->error("txtReceipients");?><label valign="center"> NOTE: For each additional email separate by a semi colon (;)</label>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px">
                    <label for="mtxSuccessMessage" class="required"><b>Success Message</b></label></td>
            <td style="text-align: left; vertical-align: top;">
                    <textarea name="mtxSuccessMessage" id="mtxSuccessMessage"><?= $mtxSuccessMessage; ?></textarea>
                <?= $form->error("mtxSuccessMessage");?>
            </td>
        </tr>
    </table>

<div class="buttons clearfix">
    <a href="javascript: history.back()" class="floatLeft silver button">Back</a>
    <input name="btnModify" type="submit" class="button green floatRight" id="btnModify" value="Save Changes">
</div>
</div><!--End continue-->

</form>
