<?php 
if (!isset($siteConfig)) die("System Error!");
$parms = array("WHERE" => "cf_status = 'Active'", "ORDER" => "cf_name");
$forms = Repository::getAll($_WEBCONFIG['COMPONET_TABLE'], $parms);
?>

	<div class="fullcontent right last">

	<h2>Site Form Submissions</h2>
	<p>Below is a list of your available forms on your site.</p>
	<?php 
	foreach($forms as $form) {
		if(isNullOrEmpty($form->cf_role) && UserManager::hasRole(FORM_VIEWER_ROLE) || !isNullOrEmpty($form->cf_role) && UserManager::hasRole(UserManager::getRoleId($form->cf_role))) {
			$sql = "SELECT *
					FROM tbl_form_submission 
					WHERE fs_archived <> '1' AND cf_id = '$form->cf_id' AND fs_viewed <> '1'";  
			$entry = Database::Execute($sql);   
			
			$i = 0;
			while($entry->MoveNext()) {
				$i++;
			} ?>
		
		<div class="assetOverview clearfix even nodesc">
			<h2><?= $form->cf_name ?> <?php $i > 0 ? print'(' . $i . ')' : "" ?></h2>
			
			<div class="btn_view">
				<a href="index.php?view=list&id=<?= $form->cf_id ?>" class="button blue floatRight">Manage</a>
			</div>
		</div>
		<?php } } ?>
	</div>
