<?php
class process extends Database { 

	private $record;
	private $table;
	public $params;

	//------------------------------
	public function __construct($table) {
	//------------------------------
		parent::__construct();
		$this->table		= $table;
	}
	
	//------------------------------
	public function _modify() {
	//------------------------------
		global $form;
		
		$iNum = (int)$_POST['hidId'];
		$this->record = Repository::getByID('tbl_form_custom', $iNum); 
		self::_verify();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			$this->params = "view=modify&id=$iNum";
		} else {
			// No Errors
            $this->record->cf_last_updated = date("Y-m-d G:i:s", time());
            $nameField = str_replace(' ', '_', trim($this->record->cf_name)) . '_Recipients';
            $receipients = Database::quote_smart($_POST['txtReceipients']); 
            Database::ExecuteRaw("UPDATE tbl_site_config SET value = '$receipients' WHERE name = '$nameField'");
			$this->record->update();
			$_SESSION['processed'] = 'updated';
			$this->params = "view=list&id=" . $this->record->cf_id;
		}
	}

	//------------------------------
	public function _view() {
	//------------------------------
		global $form;
		
		$iNum = (int)$_POST['hidId'];
		$this->record = Repository::getByID($this->table, $iNum); 
        
		$fieldName	= "mtxAdminComments";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		$this->record->fs_admin_comments = $fieldValue;
		
		$fieldName	= "optStatus";
		$fieldValue	= (int) $_POST[$fieldName];
		$this->record->fs_archived = $fieldValue;

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			$this->params = "view=list&id=" . $this->record->cf_id;
		} else {
			// No Errors
			$this->record->update();
			$_SESSION['processed'] = 'updated';
			$this->params = "view=list&id=" . $this->record->cf_id;
		}
	}
	
	//------------------------------
	public function _archive() {
	//------------------------------
		$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
		$formId	= isset($_GET['fId']) && (int)$_GET['fId'] > 0 ? (int)$_GET['fId'] : 0;

		Database::ExecuteRaw("UPDATE `tbl_form_submission` SET `fs_archived` = 1 WHERE `fs_id` = $iNum LIMIT 1;");
						
		$_SESSION['processed'] = 'archived';
		$this->params = "view=list&id=" . $formId;
	}

	//------------------------------
	public function _unarchive() {
	//------------------------------
		$iNum	 = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
		$formId = isset($_GET['fId']) && (int)$_GET['fId'] > 0 ? (int)$_GET['fId'] : 0;

		Database::ExecuteRaw("UPDATE `tbl_form_submission` SET `fs_archived` = 0 WHERE `fs_id` = $iNum LIMIT 1;");
						
		$_SESSION['processed'] = 'unarchived';
		$this->params = "view=list&id=" . $formId;
	}

	//------------------------------
	public function _delete() {
	//------------------------------
		$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
		$formId	= isset($_GET['fId']) && (int)$_GET['fId'] > 0 ? (int)$_GET['fId'] : 0;

		Database::ExecuteRaw("DELETE FROM `tbl_form_submission` WHERE fs_id = $iNum LIMIT 1;");
						
		$_SESSION['processed'] = 'deleted';
		$this->params = "view=list&id=" . $formId;
	}

	//------------------------------
	private function _verify() {
	//------------------------------
		global $form;
		
        $fieldName  = "txtReceipients";
        $fieldValue = $_POST[$fieldName];
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Form Recipients are required.");
        } else {
            $this->record->cf_recipients = $fieldValue;
        }        
        
		$fieldName	= "mtxSuccessMessage";
		$fieldValue	= $_POST[$fieldName];
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Success Message is required.");
		} else {
			$this->record->cf_success_message = $fieldValue;
		}
	}
};
?>