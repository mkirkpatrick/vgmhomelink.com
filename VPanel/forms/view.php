<?php
if (!isset($siteConfig)) 
    die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
$formId	= isset($_GET['fId']) && (int)$_GET['fId'] > 0 ? (int)$_GET['fId'] : null;
$mailto = '';
$dataEncrypted = isset($_WEBCONFIG['ENABLE_FORM_ENCRYPTION']) && $_WEBCONFIG['ENABLE_FORM_ENCRYPTION'] == "true"; 
$valueEncrypt = null;
if($dataEncrypted) {
    $valueEncrypt  = new vEncryption(FORM_ENCRYPTION_KEY);
}

if ($form->getNumErrors() > 0) {
    $hidId			  = $form->value("hidId");
    $txtAdminComments = $form->value("txtName");
    $optStatus		  = $form->value("optStatus");
    $replyToValue     = $form->value("hidReplyToValue");
} else {
    $sql = "SELECT cf_name as form_name, cf_reply_to_field, `fs_admin_comments` as admin_comments,
                 `fs_archived` as archived,`fs_date_submitted` as submitted_date, cf_reply_to_field,
                 `fs_primary_value`,`fs_secondary_value`,`fs_teriary_value`,`fs_form_data`,`fs_reply_to_value`
            FROM `tbl_form_submission` fs
            INNER JOIN tbl_form_custom cf ON fs.cf_id = cf.cf_id
            WHERE fs_id = $iNum
            LIMIT 1";
    $submission	= Database::Execute($sql);
    $submission->MoveNext();

    if ($submission->Count() == 0) {
        throw new Exception("Unable to find record in the database");
    }

    $hidId			  = $iNum; 
    $mtxAdminComments = $submission->admin_comments; 
    $optStatus		  = 1;
    $replyToValue     = $submission->fs_reply_to_value;
    $formData         = json_decode($submission->fs_form_data);	
}

Database::ExecuteRaw("UPDATE tbl_form_submission SET fs_viewed = '1' WHERE fs_id = '$iNum' LIMIT 1");

if(strlen($replyToValue) > 0) {
    $mailto = $replyToValue;
} ?> 

<form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">
    <input type="hidden" name="actionRun" value="view" />
    <input type="hidden" name="hidId" value="<?= $hidId; ?>" />
    <input type="hidden" name="hidReplyToValue" value="<?= $replyToValue; ?>" />

    <div class="subcontent last right">
        <?php if(strlen(trim($mailto)) > 0) { ?>
            <div class="floatRight forward">
                <a href="mailto:<?= $mailto ?>?subject=Thank%20You%20For%20Your%20Contact%20Submission." class="button silver icon-mail">Reply To Customer</a>
            </div>
            <?php }
        print '<h2>View '. $submission->form_name . ' Submission</h2>';
        if ($form->getNumErrors() > 0) {
            $errors	= $form->getErrorArray();
            echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
            foreach ($errors as $err) echo $err;
        } else if (isset($_SESSION['processed'])) {
            echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
            unset($_SESSION['processed']);
        }
        ?>

        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 175px"><label><b>Date Submitted</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= date("m/d/y g:i:s A", strtotime($submission->submitted_date)); ?></td>
            </tr>
            <?php 

            foreach($formData as $key => $value) {
                if($dataEncrypted && strlen(trim($value)) > 0) {
                    $value = $valueEncrypt->decrypt($value); 
                } 
                $class = $rows++ % 2 ? 'even' : 'odd'; 
                print '<tr class="' . $class . '">'; 
                print '<td style="text-align: right; vertical-align: top;"><b>' . $key . '</b></td>'; 
                print '<td style="text-align: left; vertical-align: top;">' . $value . '</td></tr>';                                
            }
            ?>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: top; width: 175px"><label for="mtxAdminName"><b>Admin Comments</b></label></td>
                <td style="text-align: left; vertical-align: top;"><textarea rows="3" cols="70" name="mtxAdminComments" id="mtxAdminComments"><?= $mtxAdminComments; ?></textarea>
                    <?= $form->error("mtxAdminComments");?>
                </td>
            </tr>
            <tr>
                <td align="right" valign="middle"><label for="optAStatus" class="required"><b>Status</b></label></td>
                <td align="left" valign="bottom">
                    <label><input name="optStatus" type="radio" value="1" <?= $optStatus == '1' ? 'checked' : ''; ?> />
                        Mark As Read</label>&nbsp;
                    <label><input name="optStatus" type="radio" value="0" <?= $optStatus == '0' ? 'checked' : ''; ?> />
                        Mark As Unread</label>
                    <?= $form->error("optStatus");?>
                </td>
            </tr>
        </table>


        <div class="buttons clearfix">
            <a href="./?view=list&id=<?= $formId ?>" class="floatLeft button silver">Back</a>
            <input name="btnModify" class="button green floatRight" type="submit" id="btnModify" value="Save Comments">
        </div>
    </div><!--End continue-->

</form>
