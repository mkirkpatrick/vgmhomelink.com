<style type="text/css">
.k-grid-content { min-height: 75px!important; }
</style>

<?php
if (!isset($siteConfig)) die("System Error!");
$sdate = isset($_GET['sdate']) ? $_GET['sdate'] : date('m/d/Y', strtotime('-3 months')); 
$edate = isset($_GET['edate']) ? $_GET['edate'] : date('m/d/Y'); 
$iNum = isset($_GET['id']) ? (int) $_GET['id'] : 0; 
?>
<script type="text/javascript">
    var iNum = <?= $iNum ?>; 
</script>
<?php

$filterId = isset($_GET['fid']) ? (int) $_GET['fid'] : 1; 
switch ($filterId) {
    case 1:
        $filterSQL = "AND fs_archived = 0";
        break;
    case 2:
        $filterSQL = "AND fs_archived = 1";
        break;
    default:
        $filterSQL = ""; 
        break;
}

?> 
<div class="subcontent right last">
    <div class="floatRight forward">
        <label>Filter By:</label><br>
        <select id="cboFilter" name="cboFilter">
            <option value="1" <?= $filterId == "1" ? "selected" : "" ?>>View Unread </option>
            <option value="2" <?= $filterId == "2" ? "selected" : "" ?>>View Archived</option>
            <option value="3" <?= $filterId == "3" ? "selected" : "" ?>>View All</option>
        </select>    
    </div>
    <?php

    $sql = "SELECT cf_name AS name, cf_primary_field AS `primary`, cf_secondary_field AS `secondary`, cf_teriary_field AS `teriary`
            FROM tbl_form_custom 
            WHERE cf_id = $iNum
            LIMIT 1";
    $form = Database::Execute($sql);
    $form->MoveNext(); 

    print '<h2>' . $form->name . ' Management</h2>';

    if ($form->getNumErrors() > 0) {
        $errors    = $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
                break;
            case 'updated':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                break;
            case 'archive':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Archived Successfully!')</script>";                                    
                break;
            case 'unarchive':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Un-Archived Successfully!')</script>";
                break;
            case 'deleted':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                break;
        }
        unset($_SESSION['processed']);
    }

    if(isset($_GET['sdate']) && isset($_GET['edate']) && !isNullOrEmpty($_GET['sdate']) && !isNullOrEmpty($_GET['edate'])) { 
        $startDate = Database::quote_smart($_GET['sdate']); 
        $endDate = Database::quote_smart($_GET['edate']); 
        $startDate = date('Y-m-d', strtotime($startDate)); 
        $endDate = date('Y-m-d', strtotime($endDate)); 
        $endDate = date('Y-m-d', strtotime('+1 day' . $endDate)); 
        $filterSQL .= " AND fs_date_submitted BETWEEN '$startDate' AND '$endDate'"; 
    }

    $sql = "SELECT `fs_id`, `cf_id`, `fs_date_submitted` as date_submitted, fs_archived,fs_primary_value,fs_secondary_value,fs_teriary_value
            FROM `tbl_form_submission` 
            WHERE cf_id = $iNum $filterSQL
            ORDER BY `fs_date_submitted` DESC
            LIMIT 10000;";
    $submission = Database::Execute($sql);

    print $submission->Count() == 1 ? "<p>There is currently <b>1</b> form submission.</p>" : "<p>There are currently <b>" . $submission->Count() . "</b> form submissions.</p>";
    ?>
    <div class="clearfix"></div> 

    <table id="grid" class="none">
        <thead> 
            <tr> 
                <?= strlen($form->primary) > 0 ? '<th data-field="field1">' . $form->primary . '</th>' : '' ?>
                <?= strlen($form->secondary) > 0 ? '<th data-field="field2">' . $form->secondary . '</th>' : '' ?>
                <?= strlen($form->teriary) > 0 ? '<th data-field="field3">' . $form->teriary . '</th>' : '' ?>
                <th data-field="lastDate">Date Submitted</th>
                <th data-field="options" style="text-align: center;">Options</th>
            </tr>
        </thead>

        <tbody>

            <?php
            if ($submission->Count() > 0) {

                while ($submission->MoveNext()) {
                    $date = date("m/d/y g:i A", strtotime($submission->date_submitted));
                    print '<tr>'; 

                    if(strlen($form->primary) > 0) {        
                        print '<td>' . $submission->fs_primary_value  . '</td>'; 
                    }
                    if(strlen($form->secondary) > 0) {                                                          
                        print '<td>' . $submission->fs_secondary_value  . '</td>';                 
                    }
                    if(strlen($form->teriary) > 0) {                                                            
                        print '<td>' . $submission->fs_teriary_value  . '</td>';                    
                    }
                    print '<td>' . date("m/d/Y g:i A", strtotime($submission->date_submitted)) . '</td>
                    <td>
                    <div align="center">
                    <a href="javascript:modify(' . $submission->fs_id . ', ' . $iNum . ');" class="green button-slim">View</a>';
                    if($submission->fs_archived) {
                        print '<a href="javascript:unarchive(' . $submission->fs_id . ', ' . $iNum . ');" class="blue button-slim">Mark New</a>';
                    } else {
                        print '<a href="javascript:archive(' . $submission->fs_id . ', ' . $iNum . ');" class="red button-slim">Archive</a>';
                    }       
                    print '</div>
                    </td>
                    </tr>' . PHP_EOL;
                }
            }
            ?>

        </tbody>
    </table>

    <div class="buttons clearfix">
        <a href="javascript: history.back()" class="floatLeft button silver">Back</a>
        <a href="?view=modify&id=<?= $iNum ?>" class="floatRight button green">Form Settings</a>         
        <form id="ExportForm" method="get" action="exportForms.php">
            <input type="hidden" name="sdate" value="<?= $sdate ?>"  />
            <input type="hidden" name="edate" value="<?= $edate ?>" />
            <input type="hidden" name="id" value="<?= $iNum ?>"  />
            <input type="hidden" name="fid" value="<?= $filterId ?>" />
            <input class="button silver icon-export floatRight" value="Export Data" type="submit" />
        </form>      
    </div><!--End continue-->

</div><!--End content-->

<script type="text/x-kendo-template" id="categoryTemplate">
    <div class="toolbar floatLeft">
        <div>
            <input type="text" name="sdate" id="sdate" value="<?= $sdate ?>" /> - <input type="text" name="edate" id="edate" value="<?= $edate ?>" />
            <a id="selectDateButton" data-role="button" class="k-button k-button-icontext" role="button" aria-disabled="false" tabindex="0">Submit</a>
        </div>
    </div>    
</script>

<script type="text/x-kendo-template" id="searchTemplate">
    <div class="toolbar floatRight">
        <div>
            <input id="txtSearch" placeholder="Search Grid" type="text" style="width: 175px" />
            <a id="clearTextButton">Clear</a>
        </div>
    </div>
</script>

<script type="text/javascript">
    $(window).load(function() { 
        $("#cboFilter").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var query = '?view=list&id=' + iNum + '&sdate=' + $('#sdate').val() + '&edate=' + $('#edate').val();
                window.location.href = window.location.pathname + query + "&fid=" + this.value(); 
            }
        });

        $("#selectDateButton").click(function() {
            var query = '?view=list&id=<?= $iNum ?>&fid=<?= $filterId ?>&sdate=' + $('#sdate').val() + '&edate=' + $('#edate').val();
            window.location.href = window.location.pathname + query; 
        });
    }); 
</script>

