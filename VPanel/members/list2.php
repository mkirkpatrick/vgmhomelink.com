<?php if(!isset($siteConfig)) die("System Error!"); ?> 
<div class="subcontent right last">
<?php
  print '<h1>Provider Users</h1>';
  
  if ($form->getNumErrors() > 0) {
  	$errors	= $form->getErrorArray();
  	foreach ($errors as $err) echo $err;
  } else if (isset($_SESSION['processed'])) {
  	switch($_SESSION['processed']) {
  		case 'added':
  			echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
  			break;
  		case 'updated':
  			echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
  			break;
  		case 'deleted':
  			echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
  			break;
  	}
  	unset($_SESSION['processed']);
  }

	$sql = "SELECT m_id, m_displayname, m_username, m_status, m_regdate
			FROM {$_WEBCONFIG['MODULE_TABLE']}
            WHERE m_role = 'Provider'
			ORDER BY m_status DESC, m_displayname, m_username";
	$record	= Database::Execute($sql);
	print $record->Count() == 1 ? "<p>There is currently <b>1</b> user.</p>\n" : "<p>There are currently <b>" . $record->Count() . "</b> users. </p>\n";
  ?>

  <table id="grid">
	<thead> 
	  <tr> 
        <th>Display Name</th>
        <th>Username</th>
        <th>Registered</th>
        <th>Status</th>
        <th data-sortable="false" data-filterable="false" style="text-align: center; width: 130px">Options</th>
	  </tr>
	</thead>
	<tbody>
  <?php
	if ($record->Count() > 0) {

		while ($record->MoveNext()) {
			$tempDate = ($timestamp = strtotime($record->m_regdate)) === false ? '' : date('m/d/Y', $timestamp);
			print '<tr> 
					<td>' . $record->m_displayname . '</td>
					<td>' . $record->m_username . '</td>
					<td>' . $tempDate . '</td>
					<td>' . $record->m_status . '</td>
					<td><div align="center"><a href="javascript:modifyCustomer(' . $record->m_id . ');" class="green button-slim">Modify</a>
					<a href="javascript:deleteCustomer(' . $record->m_id . ');" class="red button-slim">Delete</a></div></td>
				 </tr>' . PHP_EOL;
		}// end while
	}
  ?>
	</tbody>
  </table>
</div><!--End continue-->