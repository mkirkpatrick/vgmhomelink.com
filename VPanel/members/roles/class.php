<?
class Process extends Database { 

    private $record;
    private $table;
    public $params;

    //------------------------------
    public function __construct($table) {
    //------------------------------
        global $_WEBCONFIG;
        parent::__construct();
        $this->table        = $table;
    }

    //------------------------------
    public function _add() {
    //------------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=add";
        } else {
            // No Errors
            $this->record->date_added = date("Y-m-d G:i:s");
			$this->record->last_update = date("Y-m-d G:i:s");
            Repository::Save($this->record); 
            $iNum = Database::getInsertID();        
            $_SESSION['processed'] = 'added';
            $this->params = "view=list";
        }
    }

    //------------------------------
    public function _modify() {
    //------------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=modify";
        } else {
            // No Errors
			$this->record->last_update = date("Y-m-d G:i:s");
            $this->record->urole_id = (int)$_POST['hidId'];
            $this->record->update();
            $_SESSION['processed'] = 'updated';
            $this->params = "view=list";
        }
    }

    //------------------------------
    public function _delete() {
    //------------------------------
        global $_WEBCONFIG, $form;
        $iNum    = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
        $this->record = Repository::GetById($this->table, $iNum); 
        Repository::Delete($this->record); 
        $_SESSION['processed'] = 'deleted';
        $this->params = "view=list";
    }

    //------------------------------
    private function _verify() {
    //------------------------------
        global $_WEBCONFIG, $form;

        $fieldName    = "txtName";
        $fieldValue    = trim(strip_tags($_POST[$fieldName]));
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Role Name is required.");
        } else {
            $this->record->name = $fieldValue;
        }
		
		$fieldName    = "mtxDescription";
		$fieldValue    = trim(strip_tags($_POST[$fieldName]));
		if (!isNullOrEmpty($fieldValue)) {
			$this->record->description = $fieldValue;
		} else {
			$this->record->description = null; 
		}
    }
};
?>