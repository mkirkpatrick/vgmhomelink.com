<?
if (!isset($siteConfig)) die("System Error!");

$catId = isset($_GET['catId']) && (int)$_GET['catId'] > 0 ? (int)$_GET['catId'] : 0;
?> 
<div class="subcontent right last">
  <?
  $thisCatHeadName = '';
    $sql = "SELECT name FROM {$_WEBCONFIG['COMPONET_TABLE']} WHERE urole_id = $catId";
    $thisCat = Database::Execute($sql);
    if ($thisCat->Count() > 0)
    {
        $thisCat->MoveNext();
        $thisCatHeadName = " - $thisCat->name";
    }
	print '<h1>' . $_WEBCONFIG['COMPONET_NAME'] . '</h1>';

		if ($form->getNumErrors() > 0) {
			$errors	= $form->getErrorArray();
			foreach ($errors as $err) echo $err;
		} else if (isset($_SESSION['processed'])) {
          switch($_SESSION['processed']) {
              case 'added':
                  echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
                  break;
              case 'updated':
                  echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                  break;
              case 'deleted':
                  echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                  break;
          }
			unset($_SESSION['processed']);
		}

		$sql = "SELECT * FROM {$_WEBCONFIG['COMPONET_TABLE']} ORDER BY name";
		$record	= Database::Execute($sql);

  print $record->Count() == 1 ? "<p>There is currently <b>1</b> " . $_WEBCONFIG['ENTITY_NAME'] . "</p>\n" : "<p>There are currently <b>" . $record->Count() . "</b> " . $_WEBCONFIG['ENTITY_NAME_PLURAL'] . ". </p>\n";
  ?>

  <table id="grid">
    <thead> 
      <tr> 
        <th>Role Name</th>        
		<th style="width: 140px">Created Date</th>
		<th style="width: 140px">Last Modified Date</th>
		<th data-sortable="false" data-filterable="false" style="text-align: center; width: 130px">Options</th>
      </tr>
    </thead>

    <tbody>

  <?
		if ($record->Count() > 0) {

			while ($record->MoveNext()) {
				
				print '<tr> 						
						<td>' . $record->name . '</td>
						<td>' . date("m/d/Y g:i A", strtotime($record->date_added)) . '</td>
						<td>' . date("m/d/Y g:i A", strtotime($record->last_update)) . '</td>
						<td><a href="javascript:modifyRole(' . $record->urole_id . ');" class="green button-slim">Modify</a>
						<a href="javascript:deleteRole(' . $record->urole_id . ');" class="red button-slim">Delete</a></td>
					 </tr>' . "\n";
			}// end while

		}
  ?>

	</tbody>
  </table>

    <div class="buttons clearfix">
        <a class="button blue floatLeft" href="javascript:history.back()">Back</a>
        <a href="javascript:addRole()" class="button green floatRight">Add New <?= $_WEBCONFIG['ENTITY_NAME'] ?></a>
    </div>
</div><!--End continue-->