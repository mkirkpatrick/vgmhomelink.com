$(document).ready(function () {
$("#grid").kendoGrid({
        dataSource: { pageSize: 10 },
        groupable: false,
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",

                }
            }
        },
        columnMenu: false,
        scrollable: false,
        resizable: true,
        reorderable: true,
        sortable: { mode: "multiple", allowUnsort: true },
        pageable: { refresh: true, pageSizes: [10, 25, 50, 100] }
    });
}); 
// Button Functions
function addRole() {
	window.location.href = 'index.php?view=add';
}

function modifyRole(Id) {
	window.location.href = 'index.php?view=modify&id=' + Id;
}

function deleteRole(Id) {
	if (confirm('Are you sure you want to delete? \n\nConsider marking this as inactive if you plan on using it again. ')) {
		window.location.href = 'process.php?actionRun=delete&id=' + Id;
	}
}
