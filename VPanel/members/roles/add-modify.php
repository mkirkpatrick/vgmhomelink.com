<?php
if (!isset($siteConfig)) die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
$catId    = isset($_GET['catId']) && (int)$_GET['catId'] > 0 ? (int)$_GET['catId'] : 0;

if ($form->getNumErrors() > 0) {
    $txtName         = $form->value("txtName");
    $mtxDescription  = $form->value("mtxDescription", 'N');
	$txtOrder        = $form->value("txtOrder");
    $optStatus		 = $form->value("optStatus");
	$hidId           = $form->value("hidId");
} else if($iNum > 0) {

	$sql	= "SELECT * FROM {$_WEBCONFIG['COMPONET_TABLE']} WHERE urole_id = $iNum LIMIT 1";
	$record	= Database::Execute($sql);
	$record->MoveNext();

	if ($record->Count() == 0) {
		throw new Exception("Unabled to find record in the database");
	}

	$hidId				= $record->urole_id;
	$txtName			= htmlspecialchars($record->name);
    $mtxDescription		= $record->description;
	//$optStatus			= $record->cat_status;
} else {
	$hidId				= ''; 
    $txtName            = '';
    $mtxDescription     = '';
    $optStatus          = 'Active';
}
?> 

<form id="entryForm" method="post" action="process.php">
<input type="hidden" name="hidId" value="<?= $hidId ?>" />
<input type="hidden" name="actionRun" value="<?= $view ?>" />
<input type="hidden" name="hidCatId" value="<?= $catId; ?>">

<div class="subcontent right last">

    <?php
    print '<h1>' . $_WEBCONFIG['COMPONET_NAME'] . ' -- (' . ucfirst($view) . ')</h1>';

    if ($form->getNumErrors() > 0) {
        $errors    = $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        echo "<div class=\"valid\">Record Added Successfully!</div>\n";
        unset($_SESSION['processed']);
    }
    ?>

    <p>Insert the record information below and save.</p>

    <table class="grid-display">
        <tr class="<?php echo $rows++ % 2 ? 'even' : 'odd';?>">
            <td width="125" align="right" valign="middle"><label for="txtName" class="required"><b>Name</b></label></td>
            <td align="left" valign="top"><input type="text" name="txtName" id="txtName" value="<?php echo $txtName; ?>" size="45" maxlength="50" required="required" />
                <?php echo $form->error("txtName");?>
            </td>
        </tr>
        <tr class="<?php echo $rows++ % 2 ? 'even' : 'odd';?>">
            <td width="125" align="right" valign="middle"><label for="mtxDescription"><b>Description</b></label></td>
            <td align="left" valign="top"><textarea name="mtxDescription" cols="50" rows="4" id="mtxDescription"><?php echo $mtxDescription; ?></textarea></td>
        </tr>
    </table>
    
    <div class="buttons clearfix">
        <a href="javascript:history.back()" class="silver button floatLeft">Back</a>
        <input name="btnModify" type="submit" id="btnModify" class="button floatRight green" value="Save Changes">
    </div>
</div><!--End continue-->

</form>
