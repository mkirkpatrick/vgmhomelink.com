<?php
use Forbin\Library\Classes\Member;

class process extends Database {

	private $record;
	private $addressRecord;
	private $table;
	private $table_addr;
	public $params;

	//------------------------------
	public function __construct($table, $table_addr) {
	//------------------------------
		global $_WEBCONFIG;
		parent::__construct();
		$this->table		= $table;
		$this->table_addr	= $table_addr;
		$this->record = null;
		$this->addressRecord = null;
	}

	//------------------------------
	public function _modify() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$this->record = Repository::GetById($this->table, (int)$_POST['hidId']);

		if($_WEBCONFIG['ENABLE_CONTACT_INFORMATION'] == "true") {
			$this->addressRecord = Repository::getById($this->table_addr, (int)$_POST['hidId2']);
		}

		self::_verify();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			$this->params = "view=modify";
		} else {
			// No Errors
			$this->record->update();
			if($_WEBCONFIG['ENABLE_CONTACT_INFORMATION'] == "true") {
				if ((int)$_POST['hidId2'] != 0) {
                    $this->addressRecord->ma_date = date("Y-m-d G:i:s");
                    $this->addressRecord->update();
                }
			}

			Security::_addAuditEvent("Modified Member User", "Success", "Modified Member User -- '" . $this->record->m_username . "' (" . $this->record->m_id .")", $_SESSION['user_username']);
			$sql = "DELETE FROM tbl_members_in_roles WHERE m_id = " . $this->record->m_id;
			Database::ExecuteRaw($sql);
			$roles = isset($_POST['cboRoles']) ? (array)$_POST['cboRoles'] : array();
			foreach($roles as $roleId) {
				if(strlen($roleId) > 0) {
					$sql = "INSERT INTO `tbl_members_in_roles` (`m_id`, `role_id`) VALUES ($iNum, '$roleId');";
					Database::ExecuteRaw($sql);
				}
			}


            if($_POST['hidStatus'] == 'Inactive' && $this->record->m_status == 'Active') {
                self::_emailUser($this->record->m_id);
            }

			$_SESSION['processed'] = 'updated';
            redirect("index.php?view=modify&id=". $_POST['hidId']);
		}
	}

	//------------------------------
	public function _delete() {
	//------------------------------
		$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

		$this->record = new Entity($this->table);
		$this->record->m_id = $iNum;
		$this->record->delete();

		Database::ExecuteRaw("DELETE FROM `tbl_members_addresses` WHERE `m_id` = '$iNum';");

		$_SESSION['processed'] = 'deleted';
		$this->params = "view=list";
	}

    //------------------------------
    public function _emailUser($id) {
    //------------------------------
    global $siteConfig, $_WEBCONFIG;
        //die("GOT HERE");
        $sql = "SELECT m.*, ma.*
                FROM tbl_members m, tbl_members_addresses ma
                WHERE m.m_id = $id AND m.m_id = ma.m_id";
        $member = Database::Execute($sql);

        if($member->Count() > 0) {
            $member->MoveNext();

            $sql = "SELECT mir.*, mr.*
                    FROM tbl_members_in_roles mir, tbl_members_roles mr
                    WHERE mir.m_id = $id AND mir.role_id = mr.urole_id";
            $role = Database::Execute($sql);
            $role->MoveNext();

            /*$htmlFile = $_SERVER['DOCUMENT_ROOT'] . "/emailtemplates/membership-approved-email.html";
            if(file_exists($htmlFile)) {
                $fh = fopen($htmlFile, 'r');
                $body = fread($fh, filesize($htmlFile));
                fclose($fh);
            } else {
                throw new exception("Email Template File Not Found");
            }

            $mergeFields = array();
            $mergeFields["**GUEST**"] =  $member->ma_firstname . ' ' . $member->ma_lastname;
            $mergeFields["**MEMBERSHIP-TYPE**"] =  $role->name;
            $mergeFields["**USERNAME**"] =  $member->m_username;
            $mergeFields["**CONTACT-PHONE**"] =  $siteConfig['Contact_Phone'];
            $mergeFields["**CONTACT-EMAIL**"] =  $siteConfig['Contact_Email'];
            $mergeFields["**COMPANYNAME**"] =  $siteConfig['Company_Name'];
            $mergeFields["**SITE_URL**"] =  $_WEBCONFIG['SITE_URL'];
            $mergeFields["**DISPLAY_URL**"] =  $_WEBCONFIG['SITE_DISPLAY_URL'];

            $location = Database::Execute("SELECT * FROM tbl_site_location WHERE location_id = 1 LIMIT 1");

            if($location->Count() > 0) {
                $location->MoveNext();

                $mergeFields["**ADDRESS**"]     = $location->location_address;
                $mergeFields["**LOCATION**"]    = $location->location_city . ' ' . $location->location_state . ', ' . $location->location_postal;
                $mergeFields["**PHONE**"]       = $location->location_phone;
            }
            else {
                $mergeFields["**ADDRESS**"]     = 'PO Box 389';
                $mergeFields["**LOCATION**"]    = 'Wild Rose, WI 54984';
                $mergeFields["**PHONE**"]       = '715-366-7500';
            }



            $body = strtr($body, $mergeFields);

            $mailer = new vMail();

            $mailer->addRecipient($member->m_username);
            $mailer->addBlindCopy('jberning@forbin.com');
            $mailer->setSubject($siteConfig['Company_Name'] . ' Membership Approved');
            $mailer->setMailType("html");
            $mailer->setFrom($siteConfig['Default_From_Email'], $siteConfig['Company_Name']);
            $mailer->setReplyTo($siteConfig['Default_Reply_To_Email'], $siteConfig['Company_Name']);
            $mailer->setMessage($body);
            //$mailer->sendMail(); */

        }

    }

	//------------------------------
	public function _generatePassword($name, $email, $username) {
		//------------------------------
		global $_WEBCONFIG, $siteConfig;

		$password = createPassword($_WEBCONFIG['MIN_PASSWORD_LENGTH']);
		$htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "/emailtemplates/new-member.html");
		if(file_exists($htmlFile)) {
			$fh = fopen($htmlFile, 'r');
			$body = fread($fh, filesize($htmlFile));
			fclose($fh);
		} else {
			throw new exception("Email Template File Not Found");
		}
		$mergeFields = array();
		$mergeFields["**USERNAME**"] = $username;
		$mergeFields["**PASSWORD**"] = $password;
		$mergeFields["**SITEURL**"] = $_WEBCONFIG['SITE_URL'];
		$mergeFields["**MEMBERURL**"] = urlPathCombine($_WEBCONFIG['SITE_URL'], "/members/");
		$mergeFields["**COMPANYNAME**"] = $siteConfig['Company_Name'];

		$body = strtr($body, $mergeFields);

		$mailer = new vMail();
		$mailer->addRecipient($this->record->m_username, $this->record->m_displayname);
		$mailer->setSubject("{$siteConfig['Company_Name']} VPanel");
		$mailer->setMailType("html");
		$mailer->setFrom($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel");
		$mailer->setReplyTo($_WEBCONFIG['NOTIFICATION_EMAIL_SENDER'], "{$siteConfig['Company_Name']} VPanel");
		$mailer->setMessage($body);
		//$mailer->sendMail();

		return $password;
	}

	//------------------------------
	private function _userNameExists($username, $currentUser) {
	//------------------------------
		$sql = "SELECT 	`m_id` FROM `tbl_members` WHERE m_username = '$username' AND m_id <> $currentUser LIMIT 1;";
		$record = Database::Execute($sql);
		return $record->Count() > 0;
	}
	//------------------------------
	private function _verify() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$fieldName	= "txtDisplayName";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Display Name is required.");
		} else {
			$this->record->m_displayname = $fieldValue;
		}

		$fieldName	= "txtUsername";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$this->record->m_username = null;
		} else {
			if (!isset($_POST['hidId']) && self::_userNameExists($fieldValue, $_POST['hidId'])) {
				$form->setError($fieldName, "** Email already exists in our database.");
			} else {
				$this->record->m_username = $fieldValue;
			}
		}

        $fieldName  = "txtProviderNum";
        $fieldValue = trim(strip_tags($_POST[$fieldName]));
        $fieldValue = $str = preg_replace('#\s+#','|',trim($fieldValue));
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->m_provider_number = null;
        } else {
            $this->record->m_provider_number = $fieldValue;
        }

		$fieldName	= "txtPassword";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (strlen($fieldValue) < 6) {
			//
		} else if (strcmp($fieldValue, $_POST["txtPassword2"]) != 0) {
			$form->setError($fieldName, "** Passwords are not matching.");
		} else {
			$newSalt = member::_generateRandomSalt(24);
            $this->record->m_password_salt = $newSalt;
            $this->record->m_password = member::_hashPassword($fieldValue, $newSalt);
		}

		$fieldName	= "optStatus";
		$fieldValue	= isset($_POST[$fieldName]) ? $_POST[$fieldName] : 'Inactive';
		$this->record->m_status = $fieldValue;

		if($_WEBCONFIG['ENABLE_CONTACT_INFORMATION'] == "true") {
			if ((int)$_POST['hidId2'] != 0) {
                $this->addressRecord->ma_firstname        = isset($_POST["txtFirst"]) ? strip_tags($_POST['txtFirst']) : '';
                $this->addressRecord->ma_lastname         = isset($_POST["txtLast"]) ? strip_tags($_POST['txtLast']) : '';
                $this->addressRecord->ma_business         = isset($_POST["txtBusiness"]) ? strip_tags($_POST['txtBusiness']) : '';
                //$this->addressRecord->ma_country        = strip_tags($_POST['cboCountry']);
                $this->addressRecord->ma_address1         = isset($_POST["txtAddress"]) ? strip_tags($_POST['txtAddress']) : '';
                $this->addressRecord->ma_address2         = isset($_POST["txtAddress2"]) ? strip_tags($_POST['txtAddress2']) : '';
                $this->addressRecord->ma_city             = isset($_POST["txtCity"]) ? strip_tags($_POST['txtCity']) : '';
                $this->addressRecord->ma_state            = isset($_POST["txtState"]) ? strip_tags($_POST['txtState']) : '';
                $this->addressRecord->ma_postal_code   	  = isset($_POST["txtPostal"]) ? strip_tags($_POST['txtPostal']) : '';
                $this->addressRecord->ma_phone            = isset($_POST["txtPhone"]) ? strip_tags($_POST['txtPhone']) : '';
                $this->addressRecord->ma_ext              = isset($_POST["txtExtension"]) ? strip_tags($_POST['txtExtension']) : '';
                $this->addressRecord->ma_email            = isset($_POST["txtUsername"]) ? strip_tags($_POST['txtUsername']) : '';
                //$this->addressRecord->ma_fax            = strip_tags($_POST['txtFax']);
                //$this->addressRecord->ma_website        = strip_tags($_POST['txtWebsite']);
                $this->addressRecord->ma_status            = 'P';

//            if (strlen($this->addressRecord->ma_country = trim($this->addressRecord->ma_country)) == 0) {
//                $form->seterror($fieldname, "* country is required.");
//            } else {


               /*  if (strcasecmp($this->addressRecord->ma_country, "united states of america") == 0) {
                    if (strlen($this->addressRecord->ma_phone = trim($this->addressRecord->ma_phone)) > 0) {
                        $regex = '/^(?:1(?:[. -])?)?(?:\((?=\d{3}\)))?([2-9]\d{2})'
                            .'(?:(?<=\(\d{3})\))? ?(?:(?<=\d{3})[.-])?([2-9]\d{2})'
                            .'[. -]?(\d{4})(?: (?i:ext)\.? ?(\d{1,5}))?$/';

                        if(preg_match($regex, $this->addressRecord->ma_phone, $matches)) {
                            $this->addressRecord->ma_phone = "($matches[1]) $matches[2]-$matches[3]";
                        } else {
                            $form->setError($fieldName, "* phone number is invalid.");
                        }
                    }
                } */
            //}
            }


		}
	}
};
?>