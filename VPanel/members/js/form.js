$(function () {
    // add * to required field labels
    $('label.required').append('&nbsp;<font color="#990000"><b>*</b></font>&nbsp;');
	$('#txtPhone').mask('(000) 000-0000');
	$('#txtFax').mask('(000) 000-0000');
	$('#txtPostal').mask('00000-0000');
	$('#txtExtension').mask('00000');    
}); 

