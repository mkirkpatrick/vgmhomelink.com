<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/library/classes/class_form.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/classes/class_vMail.php';
require_once __DIR__ . "/class.php";

security::_secureCheck();

$p = new process($_WEBCONFIG['MODULE_TABLE'], $_WEBCONFIG['ADDRESS_TABLE']);
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {
	case 'modify' :
		$p->_modify();
		break;

	case 'delete' :
		$p->_delete();
		break;

	default :
		throw new Exception("An unknown action executed!");
}
redirect("index.php?{$p->params}");
?>