<?php
use Forbin\Library\Classes\Member;
if (!isset($siteConfig)) die("System Error!");
$iNum = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

if($iNum == 0 && $view == "modify") {
    redirect("/VPanel/members/?view=list2");
}

if ($form->getNumErrors() > 0) {
    $hidId				= $form->value("hidId");
    $hidId2				= $form->value("hidId2");
    $txtDisplayName     = $form->value("txtDisplayName");
    $txtProviderNum		= $form->value("txtProviderNum");
    $txtFirst			= $form->value("txtFirst");
    $txtLast			= $form->value("txtLast");
    $txtBusiness		= $form->value("txtBusiness");
    $txtEmail			= $form->value("txtEmail");
    $txtUsername		= $form->value("txtUsername");
    $txtPassword		= $form->value("txtPassword");
    $txtPassword2		= $form->value("txtPassword2");
    $cboCountry			= $form->value("cboCountry");
    $txtAddress			= $form->value("txtAddress");
    $txtAddress2		= $form->value("txtAddress2");
    $txtCity			= $form->value("txtCity");
    $txtState			= $form->value("txtState");
    $txtPostal			= $form->value("txtPostal");
    $txtPhone			= $form->value("txtPhone");
    $txtExtension		= $form->value("txtExtension");
    $txtFax				= $form->value("txtFax");
    $txtWebsite			= $form->value("txtWebsite");
    $optStatus			= $form->value("optStatus");
} else if($iNum > 0) {
    $sql = "SELECT *
    FROM {$_WEBCONFIG['MODULE_TABLE']} m
    LEFT JOIN {$_WEBCONFIG['ADDRESS_TABLE']} ma ON m.m_id = ma.m_id
    WHERE m.m_id = $iNum
    LIMIT 1";
    $record	= Database::Execute($sql);
    $record->MoveNext();

    if ($record->Count() == 0) {
        throw new exception("Unable to find record in the database");
    }

    //$hidId              = $record->m_id;
    $hidId				= $iNum;
    $txtDisplayName		= $record->m_displayname;
    $txtUsername		= $record->m_username;
    $txtProviderNum     = $record->m_provider_number;
    $txtPassword		= '';
    $txtPassword2		= '';
    if($_WEBCONFIG['ENABLE_CONTACT_INFORMATION'] == "true") {
        $hidId2             = strlen($record->ma_id) > 0 ? (int)$record->ma_id : 0;
        $txtFirst           = htmlspecialchars($record->ma_firstname);
        $txtLast            = htmlspecialchars($record->ma_lastname);
        $txtBusiness        = htmlspecialchars($record->ma_business);
        $txtEmail           = htmlspecialchars($record->ma_email);
        $cboCountry			= htmlspecialchars($record->ma_country);
        $txtAddress			= htmlspecialchars($record->ma_address1);
        $txtAddress2		= htmlspecialchars($record->ma_address2);
        $txtCity			= htmlspecialchars($record->ma_city);
        $txtState			= htmlspecialchars($record->ma_state);
        $txtPostal			= $record->ma_postal_code;
        $txtPhone			= $record->ma_phone;
        $txtExtension		= $record->ma_ext;
        $txtFax				= $record->ma_fax;
        $txtWebsite			= $record->ma_website;
    } else {
        $hidId2                = '';
        $txtFirst            = '';
        $txtLast            = '';
        $txtBusiness        = '';
        $txtEmail            = '';
    }
    $optStatus			= $record->m_status;
} else {
    $hidId				= '';
    $hidId2				= '';
    $txtDisplayName     = '';
    $txtProviderNum		= '';
    $txtFirst			= '';
    $txtLast			= '';
    $txtBusiness		= '';
    $txtEmail			= '';
    $txtPassword		= '';
    $txtPassword2		= '';
    $txtUsername		= '';
    if($_WEBCONFIG['ENABLE_CONTACT_INFORMATION'] == "true") {
        $cboCountry			= 'United States of America';
        $txtAddress			= '';
        $txtAddress2		= '';
        $txtCity			= '';
        $txtState			= '';
        $txtPostal			= '';
        $txtPhone			= '';
        $txtExtension		= '';
        $txtFax				= '';
        $txtWebsite			= '';
    }
    $optStatus			= 'Active';
}
?>
<script type="text/javascript">
var provNum		= 0;
var provId		= 1;
var startFStr	= '';

function addProviderField(strName) {
	var Content	= startFStr;

	Content += "<div id='rowF" + provNum + "'>";
	Content += "<b>Provider No:&nbsp;&nbsp;&nbsp;</b> <input style='width: 150px' type='text' name='txtProvName[]' id='txtProvName" + provNum + "' value='" + strName + "' maxlength='20' />&nbsp;&nbsp;&nbsp; ";
	Content += "<a style='text-decoration: none' href=\"javascript:removeProviderField(" + provNum + ");\"><img src='/VPanel/images/wizard-removeBtn.png' alt='X' title='Remove Provider' border='0' /></a> ";
	Content += "</div>";

	$("#moreProviders").append(Content);
	$('#rowF' + provNum).fadeIn({
		speed:1000
	});

    $("#txtProvName" + provNum).focus();

	provId = provNum;
	provNum++;
	$("#txtProvName" + provNum).focus();
}

function removeProviderField(id) {
    if(confirm("Are you sure you want to delete this product Provider?")) {
        $("#rowF" + id).remove();

    }
}
</script>
<form id="entryForm" method="post" action="process.php">
    <input type="hidden" name="actionRun" value="<?= $view ?>" />
    <input type="hidden" name="hidId" value="<?= $hidId; ?>" />
    <input type="hidden" name="hidId2" value="<?= $hidId2; ?>" />
    <input type="hidden" name="hidStatus" value="<?= $optStatus; ?>" />

    <div class="subcontent right last">

        <?php
        print '<h1>' . ucfirst($view) . '</h1>';

        if ($form->getNumErrors() > 0) {
            $errors	= $form->getErrorArray();
            foreach ($errors as $err) echo $err;
        } else if (isset($_SESSION['processed'])) {
            echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
            echo "<div class=\"valid\">{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!</div>\n";
            unset($_SESSION['processed']);
        }
        ?>
        <table class="grid-display">
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtDisplayName" class="required"><b>Account Type</b></label></td>
                <td style="text-align: left; vertical-align: top;"><?= $record->m_role ?></td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtDisplayName" class="required"><b>Display Name</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtDisplayName" id="txtDisplayName" value="<?= $txtDisplayName; ?>" size="30" maxlength="125" required="required" /><?= showHelpTip("Enter person's name for person or business name for business. This will be displayed when the user logs in.");  ?>
                    <?= $form->error("txtDisplayName");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtUsername" class="required"><b>Email Address</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="email" name="txtUsername" id="txtUsername" value="<?= $txtUsername; ?>" size="30" maxlength="125" required="required" /><?= showHelpTip("This will change the user's username as well."); ?>
                    <?= $form->error("txtUsername");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>" style="display: none">
                <td  align="right" valign="middle"><label for="txtPassword"><b>Password</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="password" name="txtPassword"  id="txtPassword" value="<?= $txtPassword; ?>"  size="20" maxlength="50" /> &nbsp;<b>Note: </b>Leave blank to auto-generate password and email to user.
                    <?= $form->error("txtPassword");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>" style="display: none">
                <td align="right" valign="middle"><label for="txtPassword2" class="input"><b>Confirm Password</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="password" name="txtPassword2"  id="txtPassword2" value="<?= $txtPassword2; ?>" size="20" maxlength="50" data-equals="txtPassword" />
                    <?= $form->error("txtPassword2");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>" <?= $record->m_role == "Claims Professional" ? 'style="display: none"' : '' ?>>
                <td style="text-align: right; vertical-align: top; width: 155px"><label for="txtProviderNum"><b>Provider Number(s)</b></label></td>
                <td style="text-align: left; vertical-align: top;"><textarea name="txtProviderNum"  id="txtProviderNum" rows="15"><?php
                    $providerNumbers = explode('|', $txtProviderNum);
                    foreach ($providerNumbers as $prov) {
                        print $prov . '&#13;&#10;';
                    } ?></textarea>
                <?= $form->error("txtProviderNum");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 155px"><label for="optStatus" class="required"><b>Status</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                    <input name="optStatus" type="radio" id="optAStatus" value="Active" <?= $optStatus == 'Active' ? 'checked' : ''; ?> />
                    <label for="optAStatus">Active </label> &nbsp;&nbsp;
                    <input name="optStatus" type="radio" id="optIStatus" value="Inactive" <?= $optStatus == 'Inactive' ? 'checked' : ''; ?> />
                    <label for="optIStatus">Inactive </label> &nbsp;&nbsp;
                    <input name="optStatus" type="radio" id="optLStatus" value="Locked" <?= $optStatus == 'Locked' ? 'checked' : ''; ?> />
                    <label for="optLStatus">Locked </label> &nbsp;&nbsp;
                    <input name="optStatus" type="radio" id="optPStatus" value="Pending" <?= $optStatus == 'Pending' ? 'checked' : ''; ?> />
                    <label for="optPStatus">Pending Approval</label> &nbsp;&nbsp;
                    <?= $form->error("optStatus");?>
                </td>
            </tr>
        </table>
        <div style="display: none">
        <?php if($_WEBCONFIG['ENABLE_CONTACT_INFORMATION'] == "true") { ?>
            <h2>Primary Contact Information</h2>
            <table class="grid-display">
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtFirst"><b>First Name</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtFirst" id="txtFirst" value="<?= $txtFirst; ?>" size="30" maxlength="255" />
                        <?= $form->error("txtFirst");?>
                    </td>
                </tr>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtLast"><b>Last Name</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtLast" id="txtLast" value="<?= $txtLast; ?>" size="30" maxlength="255" />
                        <?= $form->error("txtLast");?>
                    </td>
                </tr>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtBusiness"><b>Business</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtBusiness" id="txtBusiness" value="<?= $txtBusiness; ?>" size="30" maxlength="255" />
                        <?= $form->error("txtBusiness");?>
                    </td>
                </tr>
                <!--<tr class="<?//= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 155px"><label for="cboCountry" class="required"><b>Residing Country</b></label></td>
                    <td style="text-align: left; vertical-align: top;">
                        <select name="cboCountry" id="cboCountry" style="width:300px;">
                            <?//= getDataOptions(urlPathCombine($_SERVER['ROOT_URI'], $_WEBCONFIG['VPANEL_PATH'], "/data/north-america.xml"), 'US'); ?>
                        </select>
                        <?//= $form->error("cboCountry");?>
                    </td>
                </tr>-->
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?> none">
                    <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtAddress" class="required"><b>Street Address</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtAddress" id="txtAddress" value="<?= $txtAddress; ?>" size="30" maxlength="255" />
                        <?= $form->error("txtAddress");?>
                    </td>
                </tr>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?> none">
                    <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtAddress2"><b>Address 2</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtAddress2" id="txtAddress2" value="<?= $txtAddress2; ?>" size="30" maxlength="100" />
                        <?= $form->error("txtAddress2");?>
                    </td>
                </tr>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?> none">
                    <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtCity" class="required"><b>City / Town</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtCity" id="txtCity" value="<?= $txtCity; ?>" size="30" maxlength="100" />
                        <?= $form->error("txtCity");?>
                    </td>
                </tr>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?> none">
                    <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtState" class="required"><b>State / Providence</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtState" id="txtState" value="<?= $txtState; ?>" size="30" maxlength="32" />
                        <?= $form->error("txtState");?>
                    </td>
                </tr>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?> none">
                    <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtPostal" class="required"><b>Zip / Postal Code</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtPostal" id="txtPostal" value="<?= $txtPostal; ?>" size="30" maxlength="10" />
                        <?= $form->error("txtPostal");?>
                    </td>
                </tr>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtPhone"><b>Phone Number</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtPhone" id="txtPhone" value="<?= $txtPhone; ?>" size="30" maxlength="32" />
                        <?= $form->error("txtPhone");?>
                    </td>
                </tr>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 155px"><label for="txtExtension"><b>Phone Extension</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtExtension" id="txtExtension" value="<?= $txtExtension; ?>" size="30" maxlength="10" />
                        <?= $form->error("txtExtension");?>
                    </td>
                </tr>

                <!--<tr class="<?//= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtFax"><b>Fax Number</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtFax" id="txtFax" value="<?//= $txtFax; ?>" size="30" maxlength="32" />
                        <?//= $form->error("txtFax");?>
                    </td>
                </tr>
                <tr class="<?//= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtWebsite"><b>Website</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="url" name="txtWebsite" id="txtWebsite" value="<?//= $txtWebsite; ?>" size="30" maxlength="32" />
                        <?//= $form->error("txtWebsite");?>
                    </td>
                </tr>-->
            </table>
            </div>
            <? } ?>

        <div class="buttons clearfix">
            <a href="javascript:history.back()" class="silver button floatLeft">Back</a>
            <input name="btnModify" type="submit" id="btnModify" class="button floatRight green" value="Save Changes">
        </div>
    </div><!--End continue-->

</form>

<script src="/modules/forms/js/mask.js"></script>

