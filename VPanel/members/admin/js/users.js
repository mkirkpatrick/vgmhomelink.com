$(document).ready(function () {
    $("#grid").kendoGrid({
				toolbar: ["excel", "pdf"],
		excel: {
			fileName: "export.xlsx",
			filterable: true, 
			allPages: true
		},
		pdf: {
			fileName: "export.pdf",
			allPages: true
		},
        dataSource: { pageSize: 25 },
        pageable: { refresh: false, pageSizes: [ 25, 50, 100] },         
        sortable: { mode: "multiple", allowUnsort: true },
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",                    
                    doesnotcontain: "Does Not Contain",
                    startswith: "Starts With",
                    endswith: "Ends With"
                }
            }
        },        
        groupable: false,
        columnMenu: false,
        scrollable: false,
        resizable: true,
        reorderable: true
    });
});

// Button Functions
function addAdmin() {
	window.location.href = 'index.php?view=add';
}

function modifyAdmin(Id) {
	window.location.href = 'index.php?view=modify&id=' + Id;
}

function deleteAdmin(Id) {
	if (confirm('Are You Sure?')) {
		window.location.href = 'process.php?actionRun=delete&id=' + Id;
	}
}

function resetPassword(Id) {
	if (confirm('Are You Sure?')) {
		window.location.href = 'process.php?actionRun=reset&id=' + Id;
	}
}

function exportData() {
	window.location.href = 'process.php?actionRun=export';
}
