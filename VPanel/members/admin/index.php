<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_member.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 

security::_secureCheck();

if (!isset($_GET['logout'])) 
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

print '<section class="title">
            <div class="container clearfix">
                <h1 class="page-title icon-' . $_WEBCONFIG['MODULE_ICON'] . '">' . $_WEBCONFIG['MODULE_NAME'] . '</h1>
                <p class="page-desc">' . $_WEBCONFIG['MODULE_DESCRIPTION'] . '</p>
            </div>
        </section>';
print '<section class="maincontent"><div class="container clearfix">';

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/members/membersNavBar.php');

$rows = 0;
$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'add' :
	case 'modify' :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/users-form.js');
		include_once 'add-modify.php';		
		break;

	default :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/users.js');
		include_once 'list.php';		
}

print '</div></section>';

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>