<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once 'class.php'; 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 

security::_secureCheck();

if (!isset($_GET['logout'])) {
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

if(!isset($_REQUEST['actionRun']))  {
	header("HTTP/1.0 405 Method Not Allowed");
	header("Allow", "POST, GET"); 
	redirect('index.php'); 
}

$p = new process;
$action = $_REQUEST['actionRun']; 

switch ($action) {
	case 'add' :
		$p->_add();
		break;

	case 'modify' :
		$p->_modify();
		break;

	case 'delete' :
		$p->_delete();
		break;     

	default :
		throw new exception("An unknown action executed!");
}

redirect("index.php?view={$action}&{$p->params}");
?>