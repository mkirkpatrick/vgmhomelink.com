<?php 

class process extends Database { 

    private $record;
    public $params;

    //------------------------------
    public function __construct() {
        //------------------------------
        parent::__construct();
    }

    //------------------------------
    public function _add() {
        //------------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($_WEBCONFIG['COMPONET_TABLE']);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
        } else {
            // No Errors
            $this->record->am_regdate = date("Y-m-d G:i:s");
            $iNum = (int) $this->record->insert();
            Security::_addAuditEvent("Added Admin", "Success", "Added Admin -- '" . $this->record->am_username . "' (" . $iNum .")", $_SESSION['user_username']);
            $_SESSION['processed'] = 'added';
        }
    }

    //------------------------------
    public function _modify() {
        //------------------------------
        global $_WEBCONFIG, $form;

        $iNum = (int)$_POST['hidId']; 
        $this->record = Repository::LoadById($_WEBCONFIG['COMPONET_TABLE'], $iNum); 

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
        } else {
            // No Errors
            Repository::Save($this->record); 
            Security::_addAuditEvent("Modified Admin", "Success", "Modified Admin -- '" . $this->record->am_username . "' (" . $this->record->am_id .")", $_SESSION['user_username']);
            $this->params = "id=$iNum"; 
            $_SESSION['processed'] = 'updated';
        }
    }

    //------------------------------
    public function _delete() {
        //------------------------------
        $iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
        $this->record = Repository::GetById($GLOBALS['_WEBCONFIG']['COMPONET_TABLE'], $iNum); 
        Security::_addAuditEvent("Deleted Admin", "Success", "Deleted Admin -- '" . $this->record->am_username . "' (" . $this->record->am_id .")", $_SESSION['user_username']);
        Repository::Delete($this->record); 
        $_SESSION['processed'] = 'deleted';
    }

    //------------------------------
    private function _verify() {
        //------------------------------
        global $_WEBCONFIG, $form;

        $fieldName	= "txtName";
        $fieldValue	= strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Name is required.");
        } else {
            $this->record->am_name = $fieldValue;
        }

        $fieldName	= "txtUserName";
        $fieldValue	= strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Username is required.");
        } else if (strlen($fieldValue) < 4) {
            $form->setError($fieldName, "** Username is less than four characters.");
        } else {
            $adminID = isset($_POST['hidId']) ? (int)$_POST['hidId'] : 0;
            $sql = "SELECT * FROM " . $_WEBCONFIG['COMPONET_TABLE'] . " WHERE am_username = '$fieldValue' AND am_id <> $adminID LIMIT 1";
            $dt  = Database::Execute($sql);

            if ($dt->Count() > 0) {
                $form->setError($fieldName, "** Username already exists in the system.");
            } else {
                $this->record->am_username = $fieldValue;
            }
        }
        
        $fieldName	= "optProvider";
        $fieldValue	= strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->am_provider = 0;
        } else {
            $this->record->am_provider = (int) $fieldValue;
        }
        
        $fieldName	= "optCaseManager";
        $fieldValue	= strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->am_case_manager = 0;
        } else {
            $this->record->am_case_manager = (int) $fieldValue;
        }

        $fieldName	= "optStatus";
        $fieldValue	= strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Status is required.");
        } else {
            $this->record->am_status = $fieldValue;
        }
    }
};