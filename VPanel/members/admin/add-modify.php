<?
if (!isset($siteConfig)) die("System Error!");
	
$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

if ($form->getNumErrors() > 0) {
	$hidId				= $form->value("hidId");
	$txtName			= $form->value("txtName");
	$txtUserName		= $form->value("txtUserName");
	$optStatus			= $form->value("optStatus");
    $optProvider		= $form->value("optProvider");
    $optCaseManager		= $form->value("optCaseManager");
	$userId 			= 0; 
} else if ($iNum > 0) {
	$user = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $iNum);
	

	if (is_null($user)) {
		throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
	}
	$userId 			= $user->am_id; 
	$hidId				= $userId;
	$txtName			= htmlspecialchars($user->am_name);
	$txtUserName		= htmlspecialchars($user->am_username);
	$optStatus			= htmlspecialchars($user->am_status);
    $optProvider        = (int) $user->am_provider; 
    $optCaseManager     = (int) $user->am_case_manager; 
} else {
	$hidId				= '';
	$txtName			= '';
	$txtUserName		= '';
	$userId 			= 0; 
	$optStatus			= 'Active';
    $optProvider        = 0;
    $optCaseManager     = 0;
}
?> 

<form id="adminForm" method="post" action="process.php">
<input type="hidden" name="actionRun" value="<?= $view ?>" />
<input type="hidden" name="hidId" value="<?= $hidId; ?>" />

<div class="subcontent right last">

	<h2>Admins -- (<?= ucfirst($view) ?>)</h2>

	<?
	if ($form->getNumErrors() > 0) {
		$errors	= $form->getErrorArray();
		echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
		foreach ($errors as $err) echo $err;
	} else if (isset($_SESSION['processed'])) {
        echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
		unset($_SESSION['processed']);
	}
	?>

    <p>Insert the <?= strtolower(urldecode($_WEBCONFIG['ENTITY_NAME'])) ?> information below and save.</p>

	<table class="grid-display">
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td width="170" align="left" valign="middle"><label for="txtName" class="required"><b>Name</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtName" id="txtName" value="<?= $txtName; ?>" size="50" required="required" />
                <?= $form->error("txtName");?>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td align="left" valign="middle"><label for="txtUserName" class="required"><b>Username</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtUserName" id="txtUserName" value="<?= $txtUserName; ?>" size="50" minlength="4" maxlength="125" required="required" />
                <?= $form->error("txtUserName");?>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td align="left" valign="middle"><label for="optAProvider" class="required"><b>Provider Access</b></label></td>
            <td align="left" valign="bottom">
                <input name="optProvider" type="radio" value="1" <?= $optProvider == '1' ? 'checked' : ''; ?> />
                <label for="optAProvider">Yes</label>
                <input name="optProvider" type="radio" value="0" <?= $optProvider == '0' ? 'checked' : ''; ?> />
                <label for="optIProvider">No</label>
                <?= $form->error("optProvider");?>
            </td>
        </tr>
		<tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td align="left" valign="middle"><label for="optACaseManager" class="required"><b>Case Manager Access</b></label></td>
            <td align="left" valign="bottom">
                <input name="optCaseManager" type="radio" value="1" <?= $optCaseManager == '1' ? 'checked' : ''; ?> />
                <label for="optACaseManager">Yes</label>
                <input name="optCaseManager" type="radio" value="0" <?= $optCaseManager == '0' ? 'checked' : ''; ?> />
                <label for="optICaseManager">No</label>
                <?= $form->error("optCaseManager");?>
            </td>
        </tr>
		<tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td align="left" valign="middle"><label for="optAStatus" class="required"><b>Status</b></label></td>
            <td align="left" valign="bottom">
                <input name="optStatus" type="radio" value="Active" <?= $optStatus == 'Active' ? 'checked' : ''; ?> />
                <label for="optAStatus">Active </label>
                <input name="optStatus" type="radio" value="Inactive" <?= $optStatus == 'Inactive' ? 'checked' : ''; ?> />
                <label for="optIStatus">Inactive </label>
                <?= $form->error("optStatus");?>
            </td>
        </tr>
	</table>

<div class="clearfix buttons">
    <a href="./" class="silver button floatLeft">Back</a>
    <input name="btnModify" class="button floatRight green" type="submit" id="btnModify" value="Save">
</div><!--End continue-->
</div>

</form>
