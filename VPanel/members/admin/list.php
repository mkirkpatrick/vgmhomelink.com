<? if (!isset($siteConfig))  die("System Error!"); ?> 

<div class="subcontent right last">

  <h2>Manage <?= $_WEBCONFIG['ENTITY_NAME_PLURAL'] ?></h2>

  <?
		if ($form->getNumErrors() > 0) {
			$errors	= $form->getErrorArray();
			echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
			foreach ($errors as $err) echo $err;
		} else if (isset($_SESSION['processed'])) {
			switch($_SESSION['processed']) {
				case 'added':
					echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
					break;
				case 'updated':
					echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
					break;
				case 'reset':
					echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Password Reset Successfully!')</script>";
					break;
				case 'deleted':
					echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
					break;
			}
			unset($_SESSION['processed']);
		}

		$sql = "SELECT * 
				FROM tbl_members_admins
				ORDER BY am_name"; 
		$admins	= Database::Execute($sql);

		if ($admins->Count() == 1) {
			print "<p>There is currently <b>1</b> record. </p>\n";
		} else {
			$counter = $_SESSION['site_chief'] ? $admins->Count() : $admins->Count() - 2;
			print "<p>There are currently <b>$counter</b> records.</p>\n";
		}
  ?>

  <table id="grid">
	<thead> 
	  <tr> 
        <th>Name</th>
        <th>Username</th>
        <th>Status</th>
        <th>Roles</th>
        <th>Last Login</th>
        <th data-sortable="false" data-filterable="false" style="text-align: center; width: 125px;">Options</th>
	  </tr>
	</thead>

	<tbody>

  <?php
	while ($admins->MoveNext()) {
        if (($timestamp = strtotime($users->user_last_login)) === false) {
            $lastLogin = '-';
        } else {
            $lastLogin = date("m/d/y g:i A", $timestamp);
        }
		if (($timestamp = strtotime($admins->am_last_login)) === false) {
			$lastLogin = '';
		} else {
			$lastLogin = date("m/d/y g:i:s A", $timestamp);
		}
        $providerRole = $admins->am_provider ? "[PROV] " : ""; 
        $caseManager = $admins->am_case_manager ? "[CM]" : ""; 
		print '<tr> 
				<td>' . $admins->am_name . '</td>
                <td>' . $admins->am_username . '</td>
				<td>' . $admins->am_status . '</td>
                <td><b>' . $providerRole . $caseManager . '</b></td>
                <td>' . $lastLogin . '</td>
				<td>
				<a href="javascript:modifyAdmin(' . $admins->am_id . ');" class="green button-slim">Modify</a>
				<a href="javascript:deleteAdmin(' . $admins->am_id . ');" class="red button-slim">Delete</a></div></td>
			 </tr>';
	}// end while
  ?>

	</tbody>
  </table>

<div class="clearfix buttons">
	<a href="javascript:addAdmin()" class="button floatRight green">Create <?= $_WEBCONFIG['ENTITY_NAME'] ?></a>
</div><!--End continue-->
</div>