<?php header("HTTP/1.0 404 Not Found");

$title = '404 Page Not Found';
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
security::_secureCheck(); ?>

<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-thumbs-down">Uh Oh</h1>
        <p class="page-desc">This page doesn't exist.</p>
    </div>
</section>

<section class="maincontent">
    <div class="container clearfix">
        <div class="fullcontent">
          <h2>The page cannot be found</h2>
          <p>The page you are looking for might have been removed, had its name changed, or is temporarily unavailable. If you got to this page from a link to our site, please contact us</a>, so we may remove or fix the link.</p>
            
                    <p>VGM Forbin Customer Support<br />
                    Phone: 877-814-7485 <br />
                    Email: <a href="mailto: WebUpdates@forbin.com">support@forbin.com</a></p>
            
                <h3>Please try the following:</h3>
                
                <ul>
                  <li>If you typed the page address in your browser, make sure that it is spelled correctly.</li>
                  <li>Return to VPanel's <a href="/VPanel/">Home Page</a>, and try again.</li>
                  <li>Click the <a href="javascript:history.back()">Back</a> button on your browser to try another link.</li>
                </ul>
        </div>
    </div>
</section>

<?php
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php"); ?>