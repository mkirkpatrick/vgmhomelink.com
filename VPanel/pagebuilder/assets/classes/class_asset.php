<?php


class Asset{

 public static $ASSET_TYPE_FORM = 6;
 public static $TABLE = 'tbl_cms_asset';
 public static $TAG_COLUMN = 'as_key';
 public static $NAME_COLUMN = 'as_name';
 public static $VALUE_COLUMN = 'as_value'; 
 public static $DATA_COLUMN = 'as_data';
 public static $TYPE_COLUMN = 'at_id';
 public static $TAG_DELIMITER = '**';
 public static $DATEFMT = 'Y-m-d G:i:s';
 
 public static function put($args){
  $k = $args['k'];
  $v = $args['v'];
  
  
  $type = $args['type'];  
  $settings = $args['settings']; 
  if( false !== ( $entity = self::exists($args)) ){// update   
  //die('update');
  }else{ //die('new');
  
  $entity = self::makeNew();                      // add
  }
  
  $entity->as_name = $k;
  $entity->{self::$TAG_COLUMN} = $newTag;
  $entity->as_value  = $v;
  $entity->at_id = $type;
  $entity->as_data = json_encode($settings);
  
  return Repository::Save($entity);
 }
 
 public static function randNewTag(){
  $length = 19;
  $chars = "abcdefghijklmnopqrstuvwxyzABC_DEFGHIJKLMNOPQRSTUVWXYZ0123456789";	

  $size = strlen( $chars );
  $str = '';
  for( $i = 0; $i < $length; $i++ ) {
   $str .= $chars[ rand( 0, $size - 1 ) ];
  }
  return $str;
  
 }
 
 public static function val($asset){
  return $asset->{self::$VALUE_COLUMN};
 }
 
 
 
 public static function data($asset){
  return $asset->{self::$DATA_COLUMN};
 }
 
 // retunrs new blankish asset
 public static function makeNew(){   
  $newAsset = Repository::GetFirst(self::$TABLE);  
 
  $newAsset->{$newAsset->_primaryKey[0]} = null;
  $newAsset->as_is_editable = 'Y';
  $newAsset->as_status = 'Active';
  $newAsset->as_value = '';
  $newAsset->as_data = '';
  $newAsset->as_last_update = $newAsset->as_date_added = date(self::$DATEFMT);
  $newAsset->as_name = $newAsset->{self::$TAG_COLUMN} = self::randNewTag();
  $newAsset->{self::$TAG_COLUMN} = self::$TAG_DELIMITER. $newAsset->{self::$TAG_COLUMN}.self::$TAG_DELIMITER;
  return Repository::Save($newAsset);
 }

public static function generateKey($name, $count = 1, $type = 'default') { 
	$name =  $count > 1 ? $name . $count : $name;
	$args['k'] = $name; 
	$key = self::keyToTag($args, $type); 
	if(Repository::ValueExists(self::$TABLE, self::$TAG_COLUMN, $key)) {
		$count++; 
		$key = self::generateKey($name, $count);
	} 
	return $key; 
 }
 
 public static function keyToTag($args, $type){
     $newKey = self::$TAG_DELIMITER;
     $newKey .=  strtoupper(str_replace(' ', '-', removeNonAlphaNumeric($args['k'])));
     if($type != 'default') {
         if (strpos($newKey, 'TAB') === false) {
            $newKey .= "-TABS";    
         }
     }
     $newKey .= self::$TAG_DELIMITER;
  return $newKey;
 }
}
?>