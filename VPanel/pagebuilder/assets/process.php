<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "pagebuilder/assets/classes/class_asset.php");
require_once "class.php";

security::_secureCheck();

$p = new Process($_WEBCONFIG['COMPONET_TABLE'], $_WEBCONFIG['UPLOAD_FOLDER']);
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {
	case 'add' :
		$p->_add();
		break;

	case 'modify' :
		$p->_modify();
		break;

	case 'delete' :
		$p->_delete();
		break;

	default :
		throw new exception("An unknown action executed!");
}

redirect("index.php?{$p->params}");
?>