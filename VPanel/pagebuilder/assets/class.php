<?php
class Process extends Database { 

	private $record;
	private $table;
	private $dataFields; 
	private $upload_dir;
	public $params;

	//------------------------------
	public function __construct($table, $upload_dir) {
	//------------------------------
		global $_WEBCONFIG;
		parent::__construct();
		$this->table		= $table;
		$this->dataFields   = array(); 
		$this->upload_dir	= $upload_dir;
	}

	//------------------------------
	public function _add() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$this->record = new Entity($this->table);

		self::_verify();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			$this->params = "view=add";
		} else {
			// No Errors
			$this->record->as_date_added = date("Y-m-d G:i:s");
			Repository::Save($this->record);
			$iNum = Database::getInsertID(); 
			$_SESSION['processed'] = 'added';
			Security::_addAuditEvent("Asset Added", "Success", "Added --'" . $this->record->as_name . "' (" . $iNum .")", $_SESSION['user_username']);
			$this->params = "view=list";
		}
	}

	//------------------------------
	public function _modify() {
	//------------------------------
		global $_WEBCONFIG, $form;
		$iNum = (int)$_POST['hidId'];
		$this->record = Repository::GetById($this->table, $iNum); 

		self::_verify();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			$this->params = "view=modify";
		} else {
			// No Errors
			Repository::Save($this->record);
			$_SESSION['processed'] = 'updated';
			Security::_addAuditEvent("Asset Modified", "Success", "Modified -- '" . $this->record->as_name . "' (" . $this->record->as_id .")", $_SESSION['user_username']);
			$this->params = "view=list";
		}
	}

	//------------------------------
	public function _delete() {
	//------------------------------
		global $_WEBCONFIG, $form;
		$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
		$this->record = Repository::GetById($this->table, $iNum); 
		Security::_addAuditEvent("Asset Deleted", "Success", "Deleted -- '" . $this->record->as_name . "' (" . $this->record->as_id .")", $_SESSION['user_username']);
		Repository::Delete($this->record); 
		$_SESSION['processed'] = 'deleted';
		$this->params = "view=list";
	}

	//------------------------------
	private function _verify() {
	//------------------------------
		global $_WEBCONFIG, $form;
		
		$typeName = $_POST['hidType'];
        $type = Repository::GetByFieldName($_WEBCONFIG['ASSET_TYPE_TABLE'], "at_type", $typeName);    
		
		$this->record->at_id = $type->at_id; 

		$fieldName	= "txtName";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Title is required.");
		} else {
			$this->record->as_name = $fieldValue;
			$this->record->as_key = property_exists($this->record, 'as_key') ? $this->record->as_key : Asset::generateKey($fieldValue); 
		}
		
        if($typeName == 'Module' || $typeName == 'Form') { 
            $fieldName    = "txtDesc";
            $fieldValue    = $_POST[$fieldName];
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                $form->setError($fieldName, "* Value is required.");
            } else {
                $path = $_SERVER['DOCUMENT_ROOT'] . $fieldValue; 
                if(file_exists($path)) { 
                    $this->dataFields['path'] = $fieldValue; 
                    $this->record->as_value = "<?php include_once(\$_SERVER['DOCUMENT_ROOT'] . '" . $fieldValue . "') ?>";
                } else {
                    $form->setError($fieldName, "* Module URL invalid or does not exist on server!");
                }
            }        
        } else { 
            $fieldName    = "txtDesc";
            $fieldValue    = $_POST[$fieldName];
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                $form->setError($fieldName, "* Value is required.");
            } else {
                $this->record->as_value = $fieldValue;
            }            
        }
		
		$fieldName	= "mtxComments";
		$fieldValue	= $_POST[$fieldName];
		$this->record->as_description = !isNullOrEmpty($fieldValue) ? $fieldValue : null;

		$fieldName	= "optStatus";
		$fieldValue	= isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';
		if (strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Status is required.");
		} else {
			$this->record->as_status = $fieldValue;
		}

		// JSON Data
		if(count($this->dataFields > 0)) { 
			$this->record->as_data = json_encode($this->dataFields); 
		}
	}
};
?>