<?php if (!isset($siteConfig)) die("System Error!"); ?>

<div class="subcontent right last">
    <div class="pagebuttons floatRight">
        <?php //if(UserManager::isWebmaster()) { ?>
            <a href="?view=add&amp;type=module" class="silver button floatLeft icon-tools add-link">Module</a>
            <a href="?view=add&amp;type=snippet" class="silver button floatLeft icon-code add-link">Code Snippet</a>
            <?php //} ?>
        <a href="?view=add&amp;type=variable" class="silver button floatLeft icon-popup add-page">Variable</a>
        <a href="?view=add&amp;type=widget" class="silver button floatLeft icon-code add-doc">HTML Widget</a>

    </div>

    <h2>Assets</h2>
    <p>An asset is pagebuilder specific code that lets you insert dynamic and/or shared content within pages in your site. An asset can be considered a shortcut to another section of code or content.</p>

    <?php
    if ($form->getNumErrors() > 0) {
        $errors	= $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
                break;
            case 'updated':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                break;
            case 'deleted':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                break;
        }
        unset($_SESSION['processed']);
    }

    $where = "";
    $where .= !UserManager::isWebmaster() ? "at_type <> 'module' && at_type <> 'calculator' && at_type <> 'form'" : "";
    $where .= $_WEBCONFIG['SITE_TYPE'] == 'MICROVOX' && !UserManager::isWebmaster() ? " AND at.at_id <> 3 AND at.at_id <> 5" : "";
    $where = strlen($where) > 0 ? "WHERE " . $where : "";

    $sql = "SELECT as_id, as_name, as_key, at_type, as_status, as_value
            FROM {$_WEBCONFIG['COMPONET_TABLE']} ct
            INNER JOIN {$_WEBCONFIG['ASSET_TYPE_TABLE']} at ON at.at_id = ct.at_id
            $where
            ORDER BY {$_WEBCONFIG['TABLE_PREFIX']}_name, {$_WEBCONFIG['TABLE_PREFIX']}_last_update
            LIMIT 500";
    $asset	= Database::Execute($sql);
    ?>

    <table id="grid">
        <thead>
            <tr>
                <th>Name</th>
                <th>Asset Key</th>
                <th data-field="Type">Type</th>
                <th>Status</th>
                <th width="15%" data-sortable="false" data-filterable="false" style="text-align: center;">Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($asset->Count() > 0) {
                while ($asset->MoveNext()) {
                    $message = $asset->as_value;
                    if($asset->at_type == 'Form' || $asset->at_type == 'Module' || $asset->at_type == 'Tabbed Content') {
                        $link = "<span class=\"help-tip other\" style=\"margin-left: 20px;\" asset-type=\"$asset->at_type\" asset-id=\"$asset->as_id\" asset-key=\"$asset->as_key\" data-msg=\"This Type Of Asset Is Not Previewable.\">$asset->as_key</span>";
                    }
                    elseif($asset->at_type == 'Widget') {
                        $message = str_replace('"', "'", $message);
                        $link = "<span class=\"help-tip widget\" asset-type=\"$asset->at_type\" asset-id=\"$asset->as_id\" asset-key=\"$asset->as_key\" data-msg=\"$message\" >$asset->as_key</span>";
                    }
                    elseif($asset->at_type == 'Calculator') {
                        $message = str_replace('"', "'", $message);
                        $link = "<span class=\"help-tip calculator\" asset-type=\"$asset->at_type\" asset-id=\"$asset->as_id\" asset-key=\"$asset->as_key\" data-msg=\"$message\" >$asset->as_key</span>";
                    }
                    else {
                        $message = str_replace('"', "'", $message);
                        $link = "<span class=\"help-tip other\" asset-type=\"$asset->at_type\" asset-id=\"$asset->as_id\" asset-key=\"$asset->as_key\" data-msg=\"$message\" >$asset->as_key</span>";
                    }

                    print '<tr>
                            <td>' . $asset->as_name . '</td>
                            <td>' . $link . '</td>
                            <td>' . $asset->at_type . '</td>
                            <td>' . $asset->as_status . '</td>
                            <td>
                                <div align="center">
                                    <a href="javascript:modify(' . $asset->as_id . ',\'' . $asset->at_type . '\');" class="green button mini floatLeft">Modify</a>
                                    <a href="javascript:remove(' . $asset->as_id . ');" class="red button mini floatLeft">Delete</a>
                                </div>
                            </td>';
                    print '</tr>' . PHP_EOL;
                }// end while
            }
            ?>

        </tbody>
    </table>
</div><!--End content-->
<script type="text/x-kendo-template" id="typeTemplate">
    <div class="toolbar floatLeft">
        &nbsp;
        <select name="cboFilter" id="cboFilter" style="width: 150px">
        <option value="">View All</option>
        <?php if(UserManager::isWebmaster()) { ?>
            <option value="Calculator">Calculator</option>
            <option value="Form">Form</option>
            <option value="Module">Module</option>
            <option value="Snippet">Snippet</option>
            <?php } ?>
        <option value="Tabbed Content">Tabbed Content</option>
        <option value="Variable">Variable</option>
        <option value="Widget">Widget</option>
        </select>
    </div>
</script>
<script type="text/x-kendo-template" id="searchTemplate">
    <div class="toolbar floatRight">
        <div>
            <input id="txtSearch" placeholder="Search Grid" type="text" style="width: 175px" />
            <a id="clearTextButton">Clear</a>
        </div>
    </div>
</script>