<?php
if (!isset($siteConfig)) die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
if ($form->getNumErrors() > 0) {
    $hidId				= $form->value("hidId");
    $type               = $form->value("hidType"); 
    $editable		    = $form->value("hidType"); 
    $txtName			= $form->value("txtName");
    $txtDesc			= $form->value("txtDesc");
    $mtxComments		= $form->value("mtxComments");
    $optStatus			= $form->value("optStatus");
} else if ($iNum > 0) {
    $asset = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $iNum); 
    $assetType = Repository::GetById($_WEBCONFIG['ASSET_TYPE_TABLE'], $asset->at_id);

    if (is_null($asset)) {
        throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
    }	
    $editable           = $asset->as_is_editable; 
    $type				= $assetType->at_type; 
    $data			    = json_decode($asset->as_data); 
    $path				= isset($data->path) && !isNullOrEmpty($data->path) ? $data->path : ''; 
    $hidId				= $asset->as_id;
    $txtName			= htmlspecialchars($asset->as_name);
    $txtDesc			= stripos($type,'module') !== false && stripos($type,'form') !== false ? $path : $asset->as_value;
    $mtxComments		= $asset->as_description; 
    $optStatus			= $asset->as_status; 

} else {
    $hidId				= '';
    $editable           = 'Y';
    $txtName			= '';
    $txtDesc 			= ''; 
    $mtxComments		= ''; 
    $optStatus			= 'Active';
    $type				= isset($_GET['type']) ? ucfirst($_GET['type']) : ''; 
}

$txtDesc = str_replace(array("<?php include_once(", "') ?>", '$_SERVER[\'DOCUMENT_ROOT\'] . \''), "", $txtDesc);
?> 

<form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">
    <input type="hidden" name="actionRun" value="<?= $_GET['view'] ?>" />
    <input type="hidden" name="hidId" value="<?= $hidId; ?>" />
    <input type="hidden" name="hidType" id="hidType" value="<?= trim($type, "/"); ?>" />

    <div class="subcontent right last">

        <?php
        print '<h2>' . $_WEBCONFIG['COMPONET_NAME'] . ' -- (' . ucfirst($view) . ')</h2>';

        if ($form->getNumErrors() > 0) {
            $errors	= $form->getErrorArray();
            foreach ($errors as $err) echo $err;
        } else if (isset($_SESSION['processed'])) {
            echo "<script>addNotify('Asset Updated Successfully!')</script>";
            unset($_SESSION['processed']);
        }
        ?>

        <p><?= $view == "modify" ? 'Edit' : 'Insert' ?> the information below and save.</p>

        <table class="grid-display">

            <?php if(stripos($type,'module') !== false || stripos($type,'form') !== false) { ?>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 100px"><label for="cboType"><b>Asset Type</b></label></td>
                    <td style="text-align: left; vertical-align: top;">
                        <select id="type" name="type" required="required" onchange="updateType(this.value);">
                            <option>-- Select Type --</option>
                            <option <?= $type == 'Module' ? "selected='selected'" : "" ?> value="Module">Module</option>
                            <option <?= $type == 'Form' ? "selected='selected'" : "" ?> value="Form">Form</option>
                        </select>
                        <em>&nbsp;&nbsp;&nbsp;Select either the <b>Module</b> or <b>Form</b> Asset Type.</em>
                    </td>
                </tr>
                <?php } else { ?>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 100px"><label for="cboType"><b>Asset Type</b></label></td>
                    <td style="text-align: left; vertical-align: top;">
                        <em><?= $type ?></em>
                    </td>
                </tr>    
                <?php } ?>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 100px"><label for="txtName" class="required"><b>Name</b></label></td>
                <td style="text-align: left; vertical-align: top;"><input type="text" name="txtName" id="txtName" value="<?= $txtName; ?>" size="75" <?= $editable == "N" ? "readonly='readonly'" : ""; ?> maxlength="100" required="required" />
                    <?= $form->error("txtName");?>
                </td>
            </tr>
            <?php if(stripos($type,'variable') !== false || stripos($type,'module') !== false || stripos($type,'form') !== false) { ?>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: middle; width: 100px"><label for="txtDesc" class="required"><b>Value</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><input type="text" name="txtDesc" id="txtDesc" value="<?= $txtDesc; ?>" size="50" maxlength="100" required="required" />
                        <?= $form->error("txtDesc");?>
                    </td>
                </tr>
                <?php } else { ?>
                <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                    <td style="text-align: right; vertical-align: top; width: 100px"><label for="txtDesc" class="required"><b>Value</b></label></td>
                    <td style="text-align: left; vertical-align: top;"><textarea name="txtDesc" id="txtDesc" style="width: 500px; height: 200px"><?= $txtDesc; ?></textarea>
                        <?= $form->error("txtDesc");?>
                    </td>
                </tr>
                <?php } ?>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 100px"><label for="mtxComments"><b>Description</b></label></td>
                <td style="text-align: left; vertical-align: top;"><textarea name="mtxComments" id="mtxComments" style="width: 450px; height: 50px"><?= $mtxComments; ?></textarea>
                    <?= $form->error("mtxComments");?>
                </td>
            </tr>
            <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
                <td style="text-align: right; vertical-align: middle; width: 100px"><label for="optStatus" class="required"><b>Status</b></label></td>
                <td style="text-align: left; vertical-align: top;">
                    <input name="optStatus" type="radio" id="optAStatus" value="Active" <?= $optStatus == 'Active' ? 'checked' : ''; ?> />
                    <label for="optAStatus">Active </label> &nbsp;&nbsp;
                    <input name="optStatus" type="radio" id="optIStatus" value="Inactive" <?= $optStatus == 'Inactive' ? 'checked' : ''; ?> />
                    <label for="optIStatus">Inactive </label> &nbsp;&nbsp;
                    <?= $form->error("optStatus");?>
                </td>
            </tr>
        </table>

        <div class="buttons clearfix">
            <a href="./" class="floatLeft button silver">Back</a>
            <input type="submit" id="btnSave" value="Save Changes" class="button floatRight green" />
        </div><!--End continue-->

    </div><!-- eof subcontent -->

</form>
<?php if(stripos($type,'widget') !== false) { ?>
    <script type="text/javascript">
        $(function () {
            CKEDITOR.replace('txtDesc', {
                removePlugins: 'maximize,resize',
                extraPlugins: 'tokens', 
                width: "850px",
                autoGrow_maxHeight: 400,
                autoGrow_minHeight: 200,
                toolbar:
                [
                    { name: 'document', items: ['Source'] },
                    { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', 'SelectAll', '-', 'SpellChecker', 'Scayt', 'Styles', 'Format', 'FontSize'] },
                    { name: 'paragraph', items: ['TextColor', 'BGColor'] },
                    { name: 'editing', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'] },
                    { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] }, 
                    { name: 'paragraph', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
                    { name: 'links', items: ['Link', 'Unlink', 'Anchor', 'tokens'] },
                    { name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak'] }
                ]                
            });
        });
    </script>
    <?php } ?>