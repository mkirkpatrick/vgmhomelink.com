$(document).ready(function () {
    var kendoPageSize = 50;
    $("#grid").kendoGrid({
        dataSource: { pageSize: kendoPageSize },
        pageable: { refresh: false, pageSizes: [10, 25, 50, 100] },
        sortable: { mode: "multiple", allowUnsort: true },
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",
                    doesnotcontain: "Does Not Contain",
                    startswith: "Starts With",
                    endswith: "Ends With"
                }
            }
        },
        groupable: false,
        columnMenu: false,
        scrollable: false,
        resizable: true,
        reorderable: true,
        toolbar : [
            { template: kendo.template($("#typeTemplate").html()) },
            { template: kendo.template($("#searchTemplate").html()) }
        ]        
    });
    
    if(getInternetExplorerVersion() <= 8 && getInternetExplorerVersion() >= 5) {
        $('.toolbar').hide();
    } else {
        $("#clearTextButton").kendoButton({icon: "funnel-clear"});
        $("#txtSearch").on("keyup keypress onpaste", function () {
            var filter = { logic: "or", filters: [] };
            $searchValue = $(this).val();
            if ($searchValue.length > 1) {
                $.each($("#grid").data("kendoGrid").columns, function( key, column ) {
                    if(column.filterable) {
                        filter.filters.push({ field: column.field, operator:"contains", value:$searchValue});
                    }
                });

                $("#grid").data("kendoGrid").dataSource.query({ filter: filter });

                var count = $("#grid").data("kendoGrid").dataSource.total();
                $(".k-pager-info").html("1 - " + count + " of " + count + " items");

            } else if($searchValue == "") {
                $("#grid").data("kendoGrid").dataSource.query({
                    page: 1,
                    pageSize: kendoPageSize
                });
            }
        });

        $("#clearTextButton").on("click", function () {
            $("#grid").data("kendoGrid").dataSource.query({
                page: 1,
                pageSize: kendoPageSize
            });
            $("#txtSearch").val('');
        });
    }    
    
    $("#grid").data("kendoGrid").hideColumn("DetailTemplate");
    $("#cboFilter").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        change: function () {
            if (this.value()) {
                $('#grid').data("kendoGrid").dataSource.filter({ field: "Type", operator: "eq", value: this.value() });
            } else {
                $('#grid').data("kendoGrid").dataSource.filter({});
            }
        }
    });
});

// Button Functions
function add() {
	window.location.href = 'index.php?view=add';
}

function modify(id,type) {
    if(type == 'Tabbed Content') {
        window.location.href = '/VPanel/modules/tabs/index.php?view=modify&id=' + id;    
    } else {
        window.location.href = 'index.php?view=modify&id=' + id;    
    }
	
}

function remove(id) {
	if (confirm('Are You Sure?')) {
		window.location.href = 'process.php?csrfToken=<?= $_SESSION["user_csrf_token"] ?>&actionRun=delete&id=' + id;
	}
}
