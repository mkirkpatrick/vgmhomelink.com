<?php
if (!isset($siteConfig)) die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
if ($form->getNumErrors() > 0) {
    $mtxComments = $form->value("mtxComments");
} else {
    $mtxComments = $siteConfig['CSS'];
}
?> 

<form id="entryForm" method="post" action="process.php">
    <input type="hidden" name="actionRun" value="CSS" />
    <div class="subcontent right last">
        <?php
        print '<h2>Global CSS</h2>';
        if ($form->getNumErrors() > 0) {
            $errors	= $form->getErrorArray();
            foreach ($errors as $err) echo $err;
        } else if (isset($_SESSION['processed'])) {
            echo "<script>addNotify('Updated Successfully!')</script>";
            unset($_SESSION['processed']);
        }
        ?>
                <table class="grid-display">
            <tr>
            <td>
        <textarea name="mtxComments" id="mtxComments" style="width: 100%; height: 350px"><?= $mtxComments; ?></textarea>
        </td>
        </tr>
        </table>
        <div class="buttons clearfix">
            <a href="./" class="floatLeft button silver">Back</a>
            <input type="submit" id="btnSave" value="Save Changes" class="button floatRight green" />
        </div><!--End continue-->
    </div><!-- eof subcontent -->
</form>