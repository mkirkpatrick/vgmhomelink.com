<?php if (!isset($siteConfig)) die("System Error!"); ?> 

<div class="right subcontent last">
    <?php
    if ($form->getNumErrors() > 0) {
        $errors	= $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
                break;
            case 'updated':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                break;
            case 'deleted':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                break;
        }
        unset($_SESSION['processed']);
    }

    $sql = "SELECT {$_WEBCONFIG['TABLE_PREFIX']}_title, {$_WEBCONFIG['TABLE_PREFIX']}_dynamic_link, {$_WEBCONFIG['TABLE_PREFIX']}_published_date, {$_WEBCONFIG['TABLE_PREFIX']}_id
            FROM {$_WEBCONFIG['COMPONET_TABLE']} 
            WHERE cms_is_historic = 0 AND cms_is_template > 0
            ORDER BY {$_WEBCONFIG['TABLE_PREFIX']}_published_date DESC, {$_WEBCONFIG['TABLE_PREFIX']}_title
            LIMIT 1000";
    $page	= Database::Execute($sql);
    ?>

    <table id="grid">
        <thead> 
            <tr> 
                <th>Name</th>
                <th>Last Updated</th>
                <th data-sortable="false" data-filterable="false" style="text-align: center; width: 130px">Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($page->Count() > 0) {
                while ($page->MoveNext()) {
                    print '<tr> 
                    <td>' . $page->cms_title . '</td>
                    <td>' . date("m/d/Y g:i:s A", strtotime($page->cms_published_date)) . '</td>
                    <td><div align="center">
                    <a href="javascript:edit(' . $page->cms_id . ');" class="green button-slim" style="width: 50px">Modify</a>
                    <a href="javascript:remove(' . $page->cms_id . ');" class="red button-slim" style="width: 50px">Delete</a>
                    </div>
                    </td>';
                    print '</tr>' . PHP_EOL;
                }// end while

            }
            ?>

        </tbody>
    </table>

    <div class="buttons clearfix">
        <a href="<?= $_WEBCONFIG['VPANEL_PATH'] ?>pagebuilder/index.php?clearoff" class="button blue floatLeft">Back</a>
        <a href="javascript:add()" class="button green floatRight">Add New <?= $_WEBCONFIG['ENTITY_NAME'] ?></a>
    </div>  
</div><!--End content-->