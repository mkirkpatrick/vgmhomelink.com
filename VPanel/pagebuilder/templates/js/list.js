$(document).ready(function () {
    $("#grid").kendoGrid({
        dataSource: { pageSize: 50 },
        pageable: { refresh: false, pageSizes: [10, 25, 50, 100] },
        sortable: { mode: "multiple", allowUnsort: true },
        filterable: {
            messages: {
                info: "Search Filter:",
                filter: "Filter",
                clear: "Clear"
            },
            extra: false,
            operators: {
                string: {
                    contains: "Contains",
                    doesnotcontain: "Does Not Contain",
                    startswith: "Starts With",
                    endswith: "Ends With"
                }
            }
        },
        groupable: false,
        columnMenu: false,
        scrollable: false,
        resizable: true,
        reorderable: true
    });
  
});

function add() {
   window.location.href = '<?= $_WEBCONFIG['VPANEL_PATH'] ?>pagebuilder/index.php?view=add&what=template';
}

function edit(id) {
   window.location.href = '<?= $_WEBCONFIG['VPANEL_PATH'] ?>pagebuilder/index.php?view=template&id=' + id;
}

function remove(id) {
    if (confirm('Are you sure you really want to delete this page permanently?')) {
		window.location.href = 'process.php?csrfToken=<?= $_SESSION["user_csrf_token"] ?>&actionRun=delete&id=' + id;
	}
}
