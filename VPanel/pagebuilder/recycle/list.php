<?php if (!isset($siteConfig)) die("System Error!"); ?>

<div class="subcontent last right">
    <h2>Recycle Bin</h2>      
    <p>Content that is deleted from the site is not permanently deleted but is transferred to the Recycle Bin. From this screen, content can be permanently purged from the system, or restored to its original location on your website.</p>


    <?php
    if ($form->getNumErrors() > 0) {
        $errors	= $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";                    
                break;
            case 'updated':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";                    
                break;
            case 'deleted':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";                    
                break;
        }
        unset($_SESSION['processed']);
    }

    $sql = "SELECT {$_WEBCONFIG['TABLE_PREFIX']}_title, {$_WEBCONFIG['TABLE_PREFIX']}_dynamic_link, {$_WEBCONFIG['TABLE_PREFIX']}_published_date, {$_WEBCONFIG['TABLE_PREFIX']}_id
    FROM {$_WEBCONFIG['COMPONET_TABLE']} 
    WHERE cms_is_historic = 0 AND cms_is_deleted = 1
    ORDER BY {$_WEBCONFIG['TABLE_PREFIX']}_published_date DESC, {$_WEBCONFIG['TABLE_PREFIX']}_title
    LIMIT 1000";
    $page	= Database::Execute($sql);
    ?>

    <table id="grid">
        <thead> 
            <tr> 
                <th data-field="name">Name</th>
                <th data-field="url">Url</th>
                <th data-field="lastDate">Last Published</th>
                <th data-field="options" style="text-align: center;">Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($page->Count() > 0) {
                while ($page->MoveNext()) {
            print '<tr> 
                        <td>' . $page->cms_title . '</td>
                        <td><a title="View Page" target="_blank" href="' . $_WEBCONFIG['SITE_URL'] . 'pagebase.php?preview=true&pbid=' . $page->cms_id . '");">' . $page->cms_dynamic_link . '</a></td>
                        <td>' . date("m/d/Y g:i:s A", strtotime($page->cms_published_date)) . '</td>
                        <td>
                            <div align="center">
                                <a href="javascript:restore(' . $page->cms_id . ');" class="green button-slim">Restore</a>
                                <a href="javascript:remove(' . $page->cms_id . ');" class="red button-slim">Delete</a>
                            </div>
                        </td>
                    </tr>';
                }// end while

            }
            ?>

        </tbody>
    </table>
</div><!--End content-->

<script type="text/x-kendo-template" id="searchBarTemplate">
    <div class="toolbar floatRight">
        <div>
            <input id="txtSearch" placeholder="Search Grid" type="text" style="width: 175px" />
            <a id="clearTextButton">Clear</a>
        </div>
    </div>
</script>