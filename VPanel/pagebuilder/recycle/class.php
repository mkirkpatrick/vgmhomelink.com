<?php
class Process extends Database { 

	private $record;
	private $table;
	public $params;

	//------------------------------
	public function __construct($table) {
	//------------------------------
		global $_WEBCONFIG;
		parent::__construct();
		$this->table		= $table;
	}

	//------------------------------
	public function _restore() {
	//------------------------------
		global $_WEBCONFIG;
		
		$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
		$this->record = Repository::GetById($this->table, $iNum);
		$this->record->cms_is_deleted = 0; 
		$this->record->update(); 
		Security::_addAuditEvent("Page Restored", "Success", "Restored Page \"" . $this->record->cms_title . "\" (" . $this->record->cms_id . ")", $_SESSION['user_username']);
		
	}
	
	//------------------------------
	public function _purgeRevisions() {
	//------------------------------
		PB_Revisions::purgeRevisions(); 
	}

	//------------------------------
	public function _delete() {
	//------------------------------
		global $_WEBCONFIG, $form;
		$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;		
		$this->record = Repository::GetById($this->table, $iNum); 
		$historicRecord = Database::Execute("SELECT `cms_id` FROM  {$this->table} WHERE cms_historic_cms_id = $iNum AND cms_is_historic = 1"); 
		while ($historicRecord->MoveNext()) {
			Database::ExecuteRaw("DELETE FROM `tbl_cms` WHERE `cms_id` = {$historicRecord->cms_id}");
			Database::ExecuteRaw("DELETE FROM `tbl_cms_name_value` WHERE `cms_id` = {$historicRecord->cms_id}");
		}
		Database::ExecuteRaw("DELETE FROM `tbl_cms` WHERE `cms_id` = $iNum");
		Database::ExecuteRaw("DELETE FROM `tbl_cms_name_value` WHERE `cms_id` = $iNum");
		Security::_addAuditEvent("Page Permanently Deleted", "Success", "Permanently Deleted \"" . $this->record->cms_title . "\" (" . $this->record->cms_id . ")", $_SESSION['user_username']);
		$_SESSION['processed'] = 'deleted';
		$this->params = "view=list";
	}
};
?>