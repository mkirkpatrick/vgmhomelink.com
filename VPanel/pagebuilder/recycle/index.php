<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php"); 

security::_secureCheck();

if (!isset($_GET['logout'])) {
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}
	
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

print '<section class="title">
			<div class="container clearfix">
				<h1 class="page-title icon-cup">Recycle Bin</h1>
				<p class="page-desc">View content that has been previously deleted.</p>
			</div>
		</section>';
print '<section class="maincontent"><div class="container clearfix">';
require_once '../navBar.php';

print '<link type="text/css" href="/VPanel/pagebuilder/css/page-builder.css" rel="stylesheet" media="screen" />';  
print '<script type="text/javascript">'; 
require_once($_SERVER['DOCUMENT_ROOT'] . '/vpanel/pagebuilder/page-list.js.php'); 
print '</script> '; 

$rows = 0; 
$uploadFolder = $_WEBCONFIG['UPLOAD_FOLDER']; 

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'add' :
	case 'modify' :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/form.js');
		include 'add-modify.php';		
		break;

	default :
		$script	= array($_WEBCONFIG['COMPONET_FOLDER'] . 'js/list.js');
		include 'list.php';		
}

print '</div></section>';

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>