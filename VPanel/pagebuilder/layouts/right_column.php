<?php 
if (!isset($siteConfig)) die("System Error!");
include('includes\std_top.php'); ?>
<div class="shared-editor">
	
	<div id="ed-top"> </div>
	
	<ul class="textarea-list">
	<li class="two-thirds"><textarea  id="mtxDescription" name="mtxDescription"><?= $arFields["Desc"]; ?></textarea><label for="mtxDescription">Main Content</label></li>
	<li class="one-third"><textarea id="mtxSidebarContent" name="mtxSidebarContent"><?= $arFields["SidebarContent"]; ?></textarea><label for="mtxSidebarContent">Sidebar</label></li>
	</ul>
	
	<div id="ed-bot"> </div>
</div>
<?php include('includes\std_bot.php') ?>