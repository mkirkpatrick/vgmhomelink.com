<?php 
if (!isset($siteConfig)) die("System Error!");
include('includes\std_top.php'); ?>
			
			

<div class="shared-editor">
	
	<div id="ed-top"> </div>
	
	<ul class="textarea-list">
	<li class="split"><textarea id="mtxDescription" name="mtxDescription"><?php echo $arFields["Desc"]; ?></textarea><label for="mtxDescription">Main Content</label></li>
	<li class="split"><textarea id="mtxSidebarContent" name="mtxSidebarContent"><?php echo $arFields["SidebarContent"]; ?></textarea><label for="mtxSidebarContent">Sidebar</label></li>
	</ul>
	
	<div id="ed-bot"> </div>
</div>
<?php include('includes\std_bot.php') ?>