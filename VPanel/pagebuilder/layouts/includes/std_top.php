<?php 
// don't load values if we are adding a new page
if(!$addNew){
	if ($form->getNumErrors() > 0) {
		$optPublish			         = $form->value("optPublish");
		$arFields["Title"]	         = $form->value("txtName");
		$arFields["MetaTitle"]       = $form->value("txtMetaTitle"); 
		$arFields["MetaKeywords"]    = $form->value("txtMetaKeywords");
		$arFields["MetaDescription"] = $form->value("txtMetaDescription");
		$arFields["Desc"]	         = $form->value("mtxDescription", 'N');
		$arFields["SidebarContent"]  = $form->value("mtxSidebarContent", 'N'); 
		$arFields["SecondarySidebarContent"]  = $form->value("mtxSecondarySidebarContent", 'N'); 
	} else {
		$optPublish			         = $cmsRec->cms_is_published;
		$arFields["Title"]	         = '';
		$arFields["MetaTitle"]       = '';
		$arFields["MetaKeywords"]    = '';
		$arFields["MetaDescription"] = '';
		$arFields["Desc"]	         = '';
		$arFields["SidebarContent"]  = '';
		$arFields["SecondarySidebarContent"]  = '';

		$sql	 = "SELECT * FROM tbl_cms_name_value WHERE cms_id = {$cmsRec->cms_id}";
		$nvalRec = Database::Execute($sql);

		while ($nvalRec->MoveNext()) {
			$arFields[$nvalRec->cmsv_name] = $nvalRec->cmsv_value;
		}
	}
} else {
	$optPublish					 = '0'; 	
	$arFields["Title"]	         = '';
	$arFields["MetaTitle"]       = '';
	$arFields["MetaKeywords"]    = '';
	$arFields["MetaDescription"] = '';
	$arFields["Desc"]	         = '';
	$arFields["SidebarContent"]  = '';
	$arFields["SecondarySidebarContent"]  = '';
}
?> 

<div class="table_head"><!--Table Head--></div>

<table width="100%" style="border-spacing:7px;" class="title-table">
	<tr>
		<td style="vertical-align:middle; text-align: right;"><label for="txtNameTitle" class="required"><b>Title</b></label></td>
		<td style="vertical-align:text-top; text-align: left;">&nbsp;<input type="text" name="txtNameTitle" id="txtNameTitle" value="<?= $arFields["Title"]; ?>" size="100" maxlength="100" required="required" />
			<input type="hidden" name="fieldname_txtNameTitle" value="Title" /> 
			<?php showHelpTip("Shown just before your content in the header section of your site."); ?>
			<div id="assetContainer" style="float: right">     
			<span id="btnViewAsset" class="k-button" style="width: 120px">Asset Library</span>
			<?php showHelpTip("An asset is pagebuilder specific code that lets you insert dynamic and/or shared content within pages in your site. An asset can be considered a shortcut to another section of code or content."); ?>
			</div>
		</td>
	</tr>
</table>	