<?php 
if (!isset($siteConfig)) die("System Error!");
include('includes\std_top.php'); ?>
			
			

<div class="shared-editor">
	
	<div id="ed-top"> </div>
	
	<ul class="textarea-list">
	<li class="one-quarter"><textarea id="mtxSidebarContent" name="mtxSidebarContent"><?= $arFields["SidebarContent"]; ?></textarea><label for="mtxSidebarContent">Sidebar</label></li>
	<li class="split"><textarea  id="mtxDescription" name="mtxDescription"><?= $arFields["Desc"]; ?></textarea><label for="mtxDescription">Main Content</label></li>
	<li class="one-quarter"><textarea id="mtxSecondarySidebarContent" name="mtxSecondarySidebarContent"><?= $arFields["SecondarySidebarContent"]; ?></textarea><label for="mtxSecondarySidebarContent">Sidebar</label></li>
	</ul>
	
	<div id="ed-bot"> </div>
</div>
<?php include('includes\std_bot.php') ?>