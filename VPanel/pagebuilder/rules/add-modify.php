<?php
if (!isset($siteConfig)) die("System Error!");

$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
if ($form->getNumErrors() > 0) {
    $hidId      = $form->value("hidId");
    $txtMatch   = $form->value("txtMatch");
    $txtUrl     = $form->value("txtUrl");
    $optType    = $form->value("optType");
}
else if ($iNum > 0) {
	$notification = Repository::GetById($_WEBCONFIG['MODULE_TABLE'], $iNum);
	if (is_null($notification)) {
		throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
	}
    $hidId      = $notification->cr_id;
    $txtMatch   = $notification->cr_match;
    $txtUrl     = $notification->cr_url;
    $optType    = $notification->cr_type;
} 
else {
    $hidId      = '';
	$txtMatch    = '';
	$txtUrl	    = '';
	$optType  = 'Redirect'; 
}
?>
<form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">
<input type="hidden" name="actionRun" value="<?= $_GET['view'] ?>" />
<input type="hidden" name="hidId" value="<?= $hidId; ?>" />

<div class="subcontent right last">

	<?php
	print '<h2>' . $_WEBCONFIG['MODULE_NAME'] . ' -- (' . ucfirst($view) . ')</h2>';

	if ($form->getNumErrors() > 0) {
		$errors	= $form->getErrorArray();
		echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
		foreach ($errors as $err) echo $err;
	} else if (isset($_SESSION['processed'])) {
        echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
		unset($_SESSION['processed']);
	}
	?>

    <p>Insert the record information below and save.</p>

	<table class="grid-display">
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="txtMatch" class="required"><b>Url Match</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtMatch" id="txtMatch" value="<?= $txtMatch; ?>" style="width: 650px" maxlength="1024" required="required" placeholder="enter-your-url-here" />
                <p><br />Note: Use end of URL only Ex: http://www.careprohs.com/<b>test-url</b></p>
				<?= $form->error("txtMatch");?>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 145px;"><label for="txtUrl" class="required"><b>Url Redirect</b></label></td>
            <td style="text-align: left; vertical-align: top;"><input type="text" name="txtUrl" id="txtUrl" value="<?= $txtUrl; ?>" style="width: 650px" maxlength="255" required="required" placeholder="enter-your-url-here" />
			<p><br /><b>Note: </b>Use end of URL only EX: http://www.careprohs.com/<b>test-url/subpage</b></p>
                <?= $form->error("txtUrl");?>
            </td>
        </tr>
        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>" style="display: none">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label for="optType" class="required"><b>Type</b></label></td>
            <td style="text-align: left; vertical-align: top;">
                <input name="optType" type="radio" id="optAStatus" value="Redirect" <?= $optType == 'Redirect' ? 'checked' : ''; ?> />
                <label for="optAStatus">Redirect </label> &nbsp;&nbsp;
                <input name="optType" type="radio" id="optIStatus" value="Rewrite" <?= $optType == 'Rewrite' ? 'checked' : ''; ?> />
                <label for="optIStatus">Rewrite </label> &nbsp;&nbsp;
                <?= $form->error("optType");?>
            </td>
        </tr>
    
	</table>

	<div class="buttons clearfix">
        <a href="javascript:history.back()" class="silver button floatLeft">Back</a>
   		<input name="btnModify" type="submit" id="btnModify" class="button floatRight green" value="Save Changes">
    </div>
</div><!--End continue-->

</form>
