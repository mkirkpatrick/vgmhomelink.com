<?php
if (!isset($siteConfig)) die("System Error!");
?> 

<div class="subcontent right last">

    <?php
    print '<h1>' . $_WEBCONFIG['MODULE_NAME'] . '</h1>';

    if ($form->getNumErrors() > 0) {
        $errors	= $form->getErrorArray();
        foreach ($errors as $err) echo $err;
    } 
    else if (isset($_SESSION['processed'])) {
        switch($_SESSION['processed']) {
            case 'added':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
                break;
            case 'updated':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
                break;
            case 'deleted':
                echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                break;
        }
        unset($_SESSION['processed']);
    }

    $sql = "SELECT * FROM tbl_cms_rules";
    $record	= Database::Execute($sql);

    print $record->Count() == 1 ? "<p>There is currently <b>1</b> " . $_WEBCONFIG['ENTITY_NAME'] . "</p>\n" : "<p>There are currently <b>" . $record->Count() . "</b> " . $_WEBCONFIG['ENTITY_NAME'] . "s. </p>\n";
    ?>

    <table id="grid">
        <thead> 
            <tr> 
                <th data-field="name">Match</th>
                <th data-field="url">Url</th>
                <th data-field="dateAdded">Date Added</th>
                <th data-field="options" style="text-align: center;">Options</th>
            </tr>
        </thead>

        <tbody>

            <?php
            if ($record->Count() > 0) {
                while ($record->MoveNext()) {
                    print '<tr> 
                                <td>' . $record->cr_match . '</td>
                                <td>' . substr($record->cr_url, 0, 255) . '</td>
                                <td>' . date("m/d/Y g:i A", strtotime($record->cr_last_modified)) . '</td>
                                <td><div align="center">
									<a href="' . $record->cr_match . '" class="blue button-slim" target="_blank">Test Url</a>
									<a href="javascript:modifyRewrite(' . $record->cr_id . ');" class="green button-slim">Modify</a>
									<a href="javascript:deleteRewrite(' . $record->cr_id . ');" class="red button-slim">Delete</a></div></td>
                            </tr>' . PHP_EOL;
                }// end while
            } ?>
        </tbody>
    </table>

    <div class="buttons clearfix">
        <a href="javascript:history.back()" class="button blue floatLeft">Back</a>
        <a href="javascript:addRewrite()" class="button green floatRight">Add New <?= $_WEBCONFIG['ENTITY_NAME'] ?></a>
    </div>
</div><!--End continue-->

<script type="text/x-kendo-template" id="searchBarTemplate">
    <div class="toolbar floatRight">
        <div>
            <input id="txtSearch" placeholder="Search Grid" type="text" style="width: 175px" />
            <a id="clearTextButton">Clear</a>
        </div>
    </div>
</script>