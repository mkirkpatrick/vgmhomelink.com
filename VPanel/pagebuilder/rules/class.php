<?php 
class Process extends Database { 

    
	private $record;
	private $table;
	private $upload_dir;
	public $params;

	//------------------------------
	public function __construct($table, $upload_dir) {
	//------------------------------
		global $_WEBCONFIG;
		parent::__construct();
		$this->table		= $table;
		$this->upload_dir	= $upload_dir; 
	}

	//------------------------------
	public function _add() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$this->record = new Entity($this->table);

		self::_verify();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			$this->params = "view=add";
		} else {
			// No Errors
			$this->record->cr_last_modified = date("Y-m-d G:i:s");
            Repository::Save($this->record); 
			$iNum = Database::getInsertID(); 			
			$_SESSION['processed'] = 'added';
			$this->params = "view=list";
		}
	}

	//------------------------------
	public function _modify() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$this->record = new Entity($this->table);

		self::_verify();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			$this->params = "view=modify";
		} else {
			// No Errors
            $this->record->cr_id = (int)$_POST['hidId'];
			$this->record->cr_last_modified = date("Y-m-d G:i:s");
			$this->record->update();
			//Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Modified", "Success", "Modified -- '{$this->record->mk_name}' (" . $this->record->cr_id .")", $_SESSION['user_username']);
			$_SESSION['processed'] = 'updated';
			$this->params = "view=list";
		}
	}

	//------------------------------
	public function _delete() {
	//------------------------------
		global $_WEBCONFIG, $form;
		$iNum	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

		$this->record = Repository::GetById($this->table, $iNum); 
		//Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Deleted", "Success", "Deleted -- '{$this->record->mk_name}' (" . $this->record->cr_id .")", $_SESSION['user_username']);
		Repository::Delete($this->record); 
		$_SESSION['processed'] = 'deleted';
		$this->params = "view=list";
	}

	//------------------------------
	private function _verify() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$fieldName	= "txtMatch";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Match is required.");
		} else {
			if(substr($fieldValue, 0, 1) != "/"){
				$fieldValue = "/" . $fieldValue;
			}
			$this->record->cr_match = rtrim($fieldValue, "/");
		}

		$fieldName	= "txtUrl";
		$fieldValue	= strip_tags($_POST[$fieldName]);
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Url is required.");
		} else {
			if(substr($fieldValue, 0, 1) != "/" && substr($fieldValue, 0, 4) != "http"){
				$fieldValue = "/" . $fieldValue;
			}		
			$this->record->cr_url = rtrim($fieldValue, "/");
		}

		$fieldName	= "optType";
		$fieldValue	= isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';
		$this->record->cr_type = $fieldValue;     

	}

};
?>