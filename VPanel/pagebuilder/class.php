<?php
class process extends Database {

	private $record;
	private $table;
	private $table_name_value;
	private $dynamic_key;
	private $dynamic_link;
	private $dynamic_target;

	//-------------------------------------------------------
	public function __construct($table, $table_name_value) {
	//-------------------------------------------------------
		global $_WEBCONFIG;
		parent::__construct();
		$this->table			= $table;
		$this->table_name_value	= $table_name_value;
	}

	//--------------------------
	public function _parent() {
	//--------------------------
    // this function sets a new parent on a page
		global $_WEBCONFIG;
		$newParentId	= isset($_GET['parentId']) && $_GET['parentId'] > 0 ? (int)$_GET['parentId'] : $_WEBCONFIG['SITE_ROOT_PAGE'];
		$iNum		= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

		// ensure a parent exists, or they are setting to no parent [0]
		if($newParentId != $_WEBCONFIG['SITE_ROOT_PAGE']) {
			$sql = "SELECT count(*) as cnt FROM {$this->table} WHERE cms_id = $iNum";
			$r = Database::Execute($sql);
			$r->MoveNext();
		}

		// save a revision
		PB_Revisions::saveRevision( $iNum );

		// Update the parent [:<
		$sql = "UPDATE {$this->table} set cms_parent_id = $newParentId WHERE cms_id = $iNum";
		Database::ExecuteRaw($sql);

		$_SESSION['processed'] = 'reparented';
	}

	//---------------------------------------
	public function _add($isDraft = false) {
	//---------------------------------------
		global $_WEBCONFIG, $form;

		$parentId	= isset($_POST['parent-category-page']) ? (int) $_POST['parent-category-page'] : (int) $_WEBCONFIG['SITE_ROOT_PAGE'];
		$templateId = isset($_POST['hidTemplateId']) ? $_POST['hidTemplateId'] : 0;
		$this->record = new Entity($this->table);

		self::_verify();
		$newId = null; // page id
		$newContentId = null; // content id

		$sql = "SELECT MAX(cms_display_order) AS cms_display_order FROM tbl_cms WHERE cms_parent_id = '$parentId'";
		$maxDisplayOrder = Database::Execute($sql);

		if ($maxDisplayOrder->Count() > 0){
			$maxDisplayOrder->MoveNext();
			if(!isNullOrEmpty($maxDisplayOrder->cms_display_order)){
				$this->record->cms_display_order = $maxDisplayOrder->cms_display_order + 1;
			} else {
				//No parent, look for the max in the entire tbl_cms
				$sql = "SELECT MAX(cms_display_order) AS cms_display_order FROM tbl_cms";
				$maxDisplayOrder = Database::Execute($sql);
				if($maxDisplayOrder->Count() > 0){
					$maxDisplayOrder->MoveNext();
					$this->record->cms_display_order = $maxDisplayOrder->cms_display_order + 1;
				}

			}
		}

		// No Errors
		$this->record->cms_user_id		  = $_SESSION['user_id'];
		$this->record->cms_published_date = date("Y-m-d G:i:s");
		$this->record->cms_parent_id	  = $parentId;

		if ($templateId > 0) {
			$this->record->cmst_templates_id		= $templateId;
			$this->record->cms_date_modified		= date("Y-m-d G:i:s");
            //$this->record->cms_is_published            = $isDraft ? 0 : 1;
			$this->record->cms_is_published			= 0;
			$this->record->cms_dynamic_link_target	= '_self';
			$this->record->cms_show_disclaimer		= $this->show_disclaimer;
            $newId = $this->record->insert();
            $this->record->cms_id = $newId;

			if(!$isDraft) {
				Security::_addAuditEvent("Page Created", "Success", "Created -- \"" . $this->record->cms_title . "\" (" . $newId . ")", $_SESSION['user_username']);
				if (!isset($this->record->url_direct)) {
					$this->record->url_direct = PB_Helper::generateDirectUrl($this->record->cms_id);
					$this->record->cms_dynamic_link	= '/' . $this->record->url_direct;
					$this->record->update();
				}
			}
		} else {
			if (strlen($this->dynamic_key) > 0) {
				$this->record->cms_date_modified		= date("Y-m-d G:i:s");
				$this->record->cms_is_published			= 1;
				$this->record->cms_dynamic_id			= $this->dynamic_key;
				$this->record->cms_dynamic_link_target	= $this->dynamic_target;
				$this->record->cms_dynamic_link			= $this->dynamic_link;
				$this->record->cms_show_disclaimer		= $this->show_disclaimer;

				if($this->dynamic_key == 'PDF-DOC') {
					Security::_addAuditEvent("Document Added", "Success", "Added -- \"" . $this->record->cms_title . "\" (" . $newId . ")", $_SESSION['user_username']);
				} else if($this->dynamic_key == 'STATIC') {
					Security::_addAuditEvent("Link Added", "Success", "Added -- \"" . $this->record->cms_title . "\" (" . $newId . ")", $_SESSION['user_username']);
				}
			}

            $newId = $this->record->insert();
            $this->record->cms_id = $newId;
		}

		return $newId;
	}

	//-----------------------------------
	public function _saveNewTemplate() {
	//-----------------------------------
		global $_WEBCONFIG, $form;
		$templateId = isset($_POST['hidTemplateId']) ? $_POST['hidTemplateId'] : 0;
		$this->record = new Entity($this->table);
		self::_verify();
		$newId = null; // page id
		$newContentId = null; // content id
		$this->record->cms_display_order = 99999;
		$this->record->cms_user_id = $_SESSION['user_id'];
		$this->record->cms_published_date = date("Y-m-d G:i:s");
		$this->record->cms_is_template = true;
		$this->record->cms_parent_id = 0;
		$this->record->url_direct = "template";
		$this->record->cmst_templates_id		= $templateId;
		$this->record->cms_date_modified		= date("Y-m-d G:i:s");
		$this->record->cms_is_published			= 1;
		$this->record->cms_dynamic_link_target	= '_self';

		$newId = $this->record->insert();
		$this->record->cms_id = $newId;

		Security::_addAuditEvent("Create Page Template", "Success", "Created Template -- \"" . $this->record->cms_title . "\" (" . $newId . ")", $_SESSION['user_username']);
		return $newId;
	}

	//--------------------------
	public function _modify() {
	//--------------------------
		global $_WEBCONFIG, $form;
		$iNum	    = (int) $_POST['hidId'];
		$parentId	= isset($_POST['parent-category-page']) ? (int) $_POST['parent-category-page'] : (int) $_WEBCONFIG['SITE_ROOT_PAGE'];
 		$this->record = Repository::getById($this->table, $iNum);

		if ($_POST['hidTemplateId'] > 0) {
			$order = $this->record->cms_display_order;
			unset($this->record);
			$pageId = self::_add(true);
			self::_saveTemplateNV($pageId);

			$sql = "UPDATE tbl_cms SET cms_is_historic = 1, cms_historic_cms_id = '$iNum', cms_display_order = $order WHERE cms_id = $pageId";
			Database::ExecuteRaw($sql);
			Security::_addAuditEvent("Page Modified", "Success", "Modified -- \"" . $this->record->cms_title . "\" (" . $this->record->cms_id . ")", $_SESSION['user_username']);
		}
		else {
			self::_verify();

			$this->record->cms_dynamic_link_target	= $this->dynamic_target;
			$this->record->cms_dynamic_link			= $this->dynamic_link;
			$this->record->cms_show_disclaimer		= $this->show_disclaimer;
			$this->record->cms_id = $iNum;
			$this->record->cms_user_id = $_SESSION['user_id'];
			$this->record->cms_parent_id = $parentId;
			$this->record->update();

			if($this->dynamic_key == 'PDF-DOC') {
				Security::_addAuditEvent("Document Modified", "Success", "Modified -- \"" . $this->record->cms_title . "\" (" . $this->record->cms_id . ")", $_SESSION['user_username']);
			} else if($this->dynamic_key == 'STATIC') {
				Security::_addAuditEvent("Link Modified", "Success", "Modified -- \"" . $this->record->cms_title . "\" (" . $this->record->cms_id . ")", $_SESSION['user_username']);
			}

			self::_saveTemplateNV($iNum);
			Repository::Save($this->record);
		}
	}

	//--------------------------------------------------
	public function _publish($isTemplateSave = false) {
	//--------------------------------------------------
		global $_WEBCONFIG, $form;

		$cmsId		= (int)$_POST['hidId'];
		$templateId	= (int)$_POST['hidTemplateId'];
		$parentId	= isset($_POST['parent-category-page']) ? (int) $_POST['parent-category-page'] : (int) $_WEBCONFIG['SITE_ROOT_PAGE'];

	    $this->record = Repository::getById($this->table, $cmsId);

        if($this->record->cms_is_system_page == 0) {
            $activeCount = PB_Helper::getActiveCount($cmsId);
            if($activeCount >= $_WEBCONFIG['PAGEBUILDER_PAGE_LIMIT']) {
                return 'MAX';
            }
        }

		self::_verify();

		$this->record->cmst_templates_id	= $templateId;
		$this->record->cms_date_modified	= date("Y-m-d G:i:s");
		$this->record->cms_id = $cmsId;
		if ($this->record->cms_is_published == 0) {
			$this->record->cms_is_active = 1;
		}
		$this->record->cms_is_published		= 1;
		$this->record->cms_published_date = date("Y-m-d G:i:s");
		$this->record->cms_user_id = $_SESSION['user_id'];
		$this->record->cms_parent_id = $parentId;
		$this->record->update();

		PB_Revisions::saveRevision($cmsId);
		if (!isNullOrEmpty($this->record->url_direct)) {
			$sql = "UPDATE {$this->table} SET cms_dynamic_link = '/" . ltrim($this->record->url_direct, '/') . "', cms_dynamic_link_target = '_self' WHERE cms_id = $cmsId;";
			$res = Database::ExecuteRaw($sql);
		}

		Security::_addAuditEvent("Page Published", "Success", "Published -- \"" . $this->record->cms_title . "\" (" . $this->record->cms_id . ")", $_SESSION['user_username']);

        return '';
	}

    //------------------------------
	public function _css() {
	//------------------------------
		global $_WEBCONFIG;

		$entity = Repository::GetByFieldName('tbl_site_config', "name", 'CSS');
        $entity->VALUE = $_POST['mtxComments'];
        Repository::Save($entity);

		$_SESSION['processed'] = 'updated';

        redirect("/VPanel/pagebuilder/index.php?view=css");
	}

	//-------------------------------------------
	public function _saveTemplateNV($iNum = 0) {
	//-------------------------------------------
		global $_WEBCONFIG;

		$iNum		= $iNum > 0 ? (int)$iNum : (int)$_POST['hidId'];
		$parentId	= (int)$_POST['hidParentId'];
		$arFields	= array();

		if(isset($_POST['txtMetaTitle']) && isNullOrEmpty($_POST['txtMetaTitle'])) {
			$_POST['txtMetaTitle'] = $_POST['txtNameTitle'];
		}

		foreach ($_POST as $key => $val) {
			$tempName = substr($key, 0, 10);
			if (strcasecmp($tempName, "fieldname_") == 0) {
				$fieldName	= substr($key, 10);
				$fieldValue	= isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';
				$arFields[$val] = $fieldValue;
			}
		}

		$memberRoles = "";
		$roles = isset($_POST['cboRoles']) ? (array)$_POST['cboRoles'] : array();
		foreach($roles as $roleId) {
			if(strlen($roleId) > 0) {
				$memberRoles .= $roleId . ':';
			}
		}
		if(!isNullOrEmpty($memberRoles)) {
			$arFields['MemberRoles'] = rtrim($memberRoles);
		}

		// Delete Old Values and Reinsert Them
		$sql = "DELETE FROM {$this->table_name_value} WHERE cms_id = $iNum;";
		$res = Database::ExecuteRaw($sql);

		// Insert New Values
		foreach ($arFields as $name => $value) {
			$value = sanitize_from_word($value);
			$txt_fields  = 'cms_id,cmsv_name,cmsv_value';
			$txt_values  = "$iNum,";
			$txt_values .= "'" . Database::quote_smart($name) . "',";
			$txt_values .= "'" . Database::quote_smart($value) . "'";

			$sql = "INSERT INTO {$this->table_name_value} ($txt_fields) VALUES ($txt_values);";
			$res = Database::ExecuteRaw($sql);
		}
	}

	//--------------------------
	public function _delete() {
	//--------------------------
		global $_WEBCONFIG, $form;

		$parentId	= isset($_GET['parentId']) && $_GET['parentId'] > 0 ? (int)$_GET['parentId'] : 0;
		$iNum		= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;

		$sql = "SELECT COUNT(*) AS Total FROM {$this->table} WHERE cms_is_deleted = 0 and cms_is_historic = 0 and cms_parent_id = $iNum";
		$dt  = Database::Execute($sql);
		$dt->MoveNext();

		if ($dt->Total > 0) {
			if ($dt->Total == 1) {
				$form->setError("None", "Oops! Please delete the subpage first.");
			} else {
				$form->setError("None", "Oops! Please delete the {$dt->Total} subpages first.");
			}

		} else {
			$sql = "UPDATE {$this->table} SET cms_is_deleted = 1 WHERE cms_id = $iNum";
			Database::ExecuteRaw($sql);
		}

		$this->record = Repository::GetById($this->table, $iNum);
		Security::_addAuditEvent("Page Deleted", "Success", "Deleted Page \"" . $this->record->cms_title . "\" (" . $this->record->cms_id . ")", $_SESSION['user_username']);
	}

	//------------------------
	public function _sort() {
	//------------------------
		$indexCounter	 = 1;
		$updRecordsArray = $_POST['recordsArray'];

		foreach($updRecordsArray as $recordIDValue) {
			$sql = "UPDATE {$this->table} SET cms_display_order = $indexCounter WHERE cms_id = $recordIDValue";
			$res = Database::ExecuteRaw($sql);
			$indexCounter++;
		}

		print "Saved Page Order";
	}

	//----------------------------------
	public function _clearRevisions() {
	//----------------------------------
		PB_Revisions::clearRevisions();
		Security::_addAuditEvent("Cleared Page Revisions", "Success", "Deleted Page \"" . $this->record->cms_title . "\" (" . $this->record->cms_id . ")", $_SESSION['user_username']);

		print "Saved Page Order";
	}

	//---------------------------
	private function _verify() {
	//---------------------------
		global $_WEBCONFIG, $form;

		$parentId	= isset($_POST['parent-category-page']) ? (int) $_POST['parent-category-page'] : (int) $_WEBCONFIG['SITE_ROOT_PAGE'];
		$this->record->cms_parent_id = $parentId;

		$fieldName	= "txtName";

		$fieldValue	= isset($_POST[$fieldName]) ? strip_tags($_POST[$fieldName]) : "";

		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {

			if( isset($_POST['txtNameTitle']) && strip_tags(trim($_POST['txtNameTitle'])) != ''){
				$this->record->cms_title = strip_tags(trim($_POST['txtNameTitle']));
			} else {
				$form->setError("None", 'You must enter a header / name for your page!');
			}
		} else {
			$this->record->cms_title = sanitize_from_word($fieldValue);
		}

		$fieldName	= "txtSubText";
		$fieldValue	= isset($_POST[$fieldName]) ? strip_tags($_POST[$fieldName]) : null;
		$this->record->cms_sub_title = $fieldValue;

		$fieldName	= "txtOrder";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (strlen($fieldValue = trim($fieldValue)) == 0) {
			$this->record->cms_display_order = 99999;
		} else {
			$this->record->cms_display_order = $fieldValue;
		}

		$fieldName	= "optStatus";
		$fieldValue	= isset($_POST[$fieldName]) ? $_POST[$fieldName] : 0;
		$this->record->cms_is_active = $fieldValue;

		$fieldName	= "optSecure";
		$fieldValue	= isset($_POST[$fieldName]) ? $_POST[$fieldName] : 0;
		$this->record->cms_is_secure = $fieldValue;

		$fieldName	= "optBreadCrumb";
		$fieldValue	= isset($_POST[$fieldName]) ? $_POST[$fieldName] : 0;
		$this->record->cms_show_breadcrumb = $fieldValue;

		$fieldName	= "optNav";
		$fieldValue	= isset($_POST[$fieldName]) ? $_POST[$fieldName] : 0;
		$this->record->cms_show_nav = $fieldValue;

        $fieldName  = "optSystem";
        $fieldValue = isset($_POST[$fieldName]) ? $_POST[$fieldName] : 0;
        $this->record->cms_is_system_page = $fieldValue;

        $fieldName  = "optSystemEditable";
        $fieldValue = isset($_POST[$fieldName]) ? $_POST[$fieldName] : 0;
        $this->record->cms_is_editable = $fieldValue;

        if(isset($_POST['addWhat']) && $_POST['addWhat'] == 'template') {
            $this->record->cms_is_template = 1;
        }
        else {
            $fieldName  = "optTemplate";
            $fieldValue = isset($_POST[$fieldName]) ? $_POST[$fieldName] : 0;
            $this->record->cms_is_template = $fieldValue;
        }

  		$fieldName	= "url_direct";
		$v	= isset($_POST[$fieldName]) ? trim(ltrim($_POST[$fieldName], '/')) : '';
		if(!isNullOrEmpty($v)) {
			$this->record->url_direct = $v;
		}

		$fieldName	= "cboDynamic";
		$fieldValue	= isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';
		if (isNullOrEmpty($fieldValue)) {
			$this->dynamic_key = '';
		} else {
			$this->dynamic_key = $fieldValue;
			$fieldName	= "txtLink";
			$fieldValue	= isset($_POST[$fieldName]) ? trim(strip_tags($_POST[$fieldName])) : '';
			if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
				$form->setError($fieldName, "Link is required.");
			} else {
				$this->dynamic_link = $fieldValue;
			}
		}

		$fieldName	= "optTarget";
		if(isset($_POST[$fieldName])) {
			$fieldValue	= $_POST[$fieldName];
			$this->dynamic_target = $fieldValue;
		}

        $fieldName    = "optDisclaimer";
        if(isset($_POST[$fieldName])) {
            $fieldValue = $_POST[$fieldName];
            $this->show_disclaimer = $fieldValue;
        }
        else {
            $this->show_disclaimer = '0';
        }
	}
};
?>