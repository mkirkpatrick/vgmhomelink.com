<nav class="subnav left">
<ul>
<?php
$tabs = array('Pages' => 'pagebuilder/index.php'); 
if(UserManager::hasRole(CONTENT_PUBLISHER_ROLE) || UserManager::hasRole(CONTENT_AUTHOR_ROLE)) {
	$tabs['File Uploads'] = 'pagebuilder/uploads';
	$tabs['Assets'] ='pagebuilder/assets';
	$tabs['Recycle Bin'] ='pagebuilder/recycle';
	$tabs['Url Redirects'] ='pagebuilder/rules/index.php';
}
if(UserManager::isWebmaster() && $_WEBCONFIG['ENABLE_CUSTOM_TEMPLATES'] == 'true') {
	$tabs['Templates'] = 'pagebuilder/templates'; 
}

if(UserManager::isWebmaster()) {
	$tabs['System Pages'] = 'pagebuilder/system/pages'; 
}

$navBar = $tabs;         
if (isset($navBar)) {
    foreach($navBar as $navTitle => $navLink) {
        if(strstr($navLink, "?")) {
            $link = $navLink;
            $class = $navTitle; 
        } else {
            $link = $navLink . "?clearoff";
            $class = $navTitle; 
        }
        
        $navClass = preg_match("^{$link}^i", $_SERVER['REQUEST_URI']) ? 'on' : '';
        print '<li><a href="' . urlPathCombine($_WEBCONFIG['VPANEL_PATH'], $link) . '" class="subbtn icon-' . $navTitle . '">' . $navTitle . '</a></li>' . "\n";
    }
} ?>
</nav>