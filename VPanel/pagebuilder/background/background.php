<?
if (!isset($siteConfig))
    die("System Error!");


$iNum    = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
if ($form->getNumErrors() > 0) {
    $hidId     = $form->value("hidId");
    $mtxImage  = $form->value("mtxImage");
}
else if ($iNum > 0) {
    $cms = Repository::GetById($_WEBCONFIG['MODULE_TABLE'], $iNum);

    if (is_null($cms)) {
        throw new exception("Unable to find {$_WEBCONFIG['ENTITY_NAME']} in the database");
    }
    $hidId     = $cms->cms_id;
    $mtxImage  = htmlspecialchars($cms->background_image);
}
else {
    $hidId     = '';
    $mtxImage  = '';
}
?>

<form id="entryForm" method="post" enctype="multipart/form-data" action="process.php">
<input type="hidden" name="actionRun" value="<?= $_GET['view'] ?>" />
<input type="hidden" name="hidId" value="<?= $hidId; ?>" />

<div class="subcontent right last">

    <?
    print '<h2>' . $_WEBCONFIG['MODULE_NAME'] . ' -- (' . ucfirst($view) . ')</h2>';

    if ($form->getNumErrors() > 0) {
        $errors    = $form->getErrorArray();
        echo "<span style='color: #ff0000; font-size: large'>" . $form->getNumErrors() . " error(s) found</span><br />";
        foreach ($errors as $err) echo $err;
    } else if (isset($_SESSION['processed'])) {
        echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";
        unset($_SESSION['processed']);
    }
    $rows = 0;
    ?>

    <p>Insert the record information below and save.</p>

    <table class="grid-display">

        <tr class="<?= $rows++ % 2 ? 'even' : 'odd';?>">
            <td style="text-align: right; vertical-align: middle; width: 125px"><label class="required" for="fleLeftImage"><b>Image</b></label></td>
            <td style="text-align: left; vertical-align: top;">
                <textarea name="mtxImage" id="mtxImage" required="required"><?= $mtxImage; ?></textarea>
                <?= $form->error("mtxImage");?>
            </td>
        </tr>

    </table>

    <div class="buttons clearfix">
        <a href="javascript:window.location.href='/vpanel/pagebuilder/index.php?view=modify&id=<?= $hidId ?>'" class="silver button floatLeft">Back to Edit Page</a>
           <input name="btnModify" type="submit" id="btnModify" class="button floatRight green" value="Save Changes">
    </div>
</div><!--End continue-->

</form>
