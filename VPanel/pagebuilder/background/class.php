<?php
class process extends Database {

	private $record;
	private $table;
	private $table_name_value;
	private $dynamic_key;
	private $dynamic_link;
	private $dynamic_target;

	//-------------------------------------------------------
	public function __construct($table, $table_name_value) {
	//-------------------------------------------------------
		global $_WEBCONFIG;
		parent::__construct();
		$this->table			= $table;
		$this->table_name_value	= $table_name_value;
	}

	//------------------------------------
	private function _verify() {
	//------------------------------------
		global $_WEBCONFIG, $form;

		$fieldName  = "mtxImage";
        $fieldValue = strip_tags($_POST[$fieldName], '<img>');
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Icon Image is required.");
        } else {
            $this->record->background_image = $fieldValue;
        }        
	}

	//------------------------------
    public function _modify() {
    //------------------------------
        global $_WEBCONFIG, $form;

        $this->record = new Entity($this->table);

        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = "view=background&id={$_POST['hidId']}";
        } else {
            // No Errors
            $this->record->cms_id = (int)$_POST['hidId'];
            $this->record->cms_date_modified = date("Y-m-d G:i:s");
            $this->record->update();
            Security::_addAuditEvent($_WEBCONFIG['ENTITY_NAME'] . " Modified", "Success", "Modified -- '{$this->record->cms_title}' (" . $this->record->cms_id .")", $_SESSION['user_username']);
            $_SESSION['processed'] = 'updated';
            $this->params = "view=background&id={$_POST['hidId']}";
        }
    }

    //------------------------------
    public function _removeBackground(){
    //------------------------------
        $pgId   = isset($_POST['id']) && (int)$_POST['id'] > 0 ? (int)$_POST['id'] : 0;
        if($pgId > 0){
            $sql = "UPDATE tbl_cms SET background_image = NULL WHERE cms_id = $pgId";
            Database::ExecuteRaw($sql);
            print "Removed"; 
        } else {
            print "Not Found"; 
        }
        $this->params = '';
    }

};
?>