<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_vMail.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/ThumbLib.inc.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "pagebuilder/classes/class_revisions.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "pagebuilder/classes/class_templates.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "pagebuilder/classes/class_helper.php");

require_once "class.php";

security::_secureCheck();
$p = new process('tbl_cms', 'tbl_cms_name_value');
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {

	case 'background' :
			$p->_modify();
			break;
	case 'removeBackground' :
			$p->_removeBackground();
			break;
}
redirect("index.php?{$p->params}");
function jsonResponse($successMsg){
	$lf = $GLOBALS['form'];
	$errors = $lf->getErrorArray();
	if(sizeof($errors) > 0){
		foreach($errors as $k=>$v){ die('#ERROR#'.strip_tags($v));}
	} else {
		die($successMsg);
	}
}
?>