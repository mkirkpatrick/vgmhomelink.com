<?php
if(isset($_GET['view'])) {
	$bForceSidebarClosed = true;
	$sCantToggleSidebarInline = ' style="visibility:hidden; " ';
}

require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_user_manager.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "pagebuilder/classes/class_revisions.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "pagebuilder/classes/class_templates.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "pagebuilder/classes/class_helper.php");
$iNum = $pageId = $pageParentId = -1;
if(isset($_GET['id']) && !empty($_GET['id'])){ $iNum = (int) $_GET['id']; }

$parentId = isset($_GET['parentId']) && (int)$_GET['parentId'] > 0 ? (int)$_GET['parentId'] : 0;

security::_secureCheck(CONTENT_AUTHOR_ROLE,CONTENT_PUBLISHER_ROLE);

if (!isset($_GET['logout']))
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];


if($_WEBCONFIG['HAS_PAGEBUILDER_ACCESS'] == "true" || UserManager::isWebmaster()) {}
else {
    redirect("/VPanel");
}

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_header.php");

print'<link type="text/css" href="/VPanel/pagebuilder/css/page-builder.css" rel="stylesheet" media="screen" />';

print '<section class="title">
			<div class="container clearfix">
				<h1 class="page-title icon-doc-text">Page Builder</h1>
				<p class="page-desc">Add, edit or delete pages from your website.</p>
			</div>
		</section>';

print '<section class="maincontent"><div class="container clearfix">';

include_once($_SERVER['DOCUMENT_ROOT'] . '/vpanel/pagebuilder/navBar.php');

// print '<link type="text/css" href="/VPanel/pagebuilder/css/page-builder.css" rel="stylesheet" media="screen" />';
print '<script type="text/javascript">';
require_once($_SERVER['DOCUMENT_ROOT'] . '/vpanel/pagebuilder/page-list.js.php');
print '</script> ';

if ($parentId > 0) {
	$sql	= "SELECT * FROM tbl_cms WHERE cms_is_historic = 0 and cms_is_deleted = 0 and cms_id = $parentId ";
	$parent	= Database::Execute($sql);
	$parent->MoveNext();

	if ($parent->Count() == 0) {
		throw new Exception("Unable to find record in the database");
	}
}

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'add' :

	case 'background':
		$script = array($_WEBCONFIG['MODULE_FOLDER'] . 'js/form.js');		
		include 'background.php';
		break;
	case 'resize':
		$script	= array('jquery/jquery.imgareaselect.min.js', 'pagebuilder/js/photo.js');
		include 'resize.php';
		break;

	default :
		$script	= array('pagebuilder/js/global.js');
		include 'list.php';
}

print '</div></section>';

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>