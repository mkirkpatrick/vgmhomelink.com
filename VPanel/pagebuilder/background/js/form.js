$(function() {
	// add * to required field labels
	$('label.required').append('&nbsp;<span style="color: #990000"><b>*</b></span>&nbsp;');
});

// configuration
CKEDITOR.config.bodyClass = 'content';
CKEDITOR.config.height = 300;
CKEDITOR.config.contentsCss = ['/css/imports/global.css', '/css/main.css', '/VPanel/css/ckeditor-override.css'];
CKEDITOR.config.skin = 'moonocolor';
CKEDITOR.config.autoGrow_maxHeight = 300;
CKEDITOR.config.autoGrow_minHeight = 100;
CKEDITOR.config.removePlugins = 'maximize,resize';
CKEDITOR.config.sharedSpaces = { top: 'ed-top'};
CKEDITOR.config.toolbarCanCollapse = true;
CKEDITOR.config.scayt_autoStartup = true;
CKEDITOR.config.allowedContent = true;
CKEDITOR.config.filebrowserBrowseUrl = '/VPanel/ckfinder/browse.php?type:files';
CKEDITOR.config.filebrowserImageBrowseUrl = '/VPanel/ckfinder/browse.php?type:images';
CKEDITOR.config.filebrowserUploadUrl = '/VPanel/ckfinder/upload.php?type:files';
CKEDITOR.config.filebrowserImageUploadUrl = '/VPanel/ckfinder/upload.php?type:images';


CKEDITOR.config.toolbar =
[
		{ name: 'document', items: ['Source']},
		{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
		{ name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt', 'AutoCorrect' ] },
		{ name: 'styles', items: ['Styles', 'Format', 'FontSize'] }, '/',
		{ name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'] },
		{ name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] },
		{ name: 'links', items: ['Link', 'Unlink'] },
		{ name: 'insert', items: ['HorizontalRule', 'SpecialChar'] },
];

CKEDITOR.replace( 'mtxImage',{
	removePlugins : 'maximize,resize',
	height: '200',
	width: '800',
	toolbar:
	[
		{ name: 'insert', items: ['Image'] }
	],
	filebrowserBrowseUrl : '/VPanel/ckfinder/ckfinder.html?type:files',
	filebrowserImageBrowseUrl : '/VPanel/ckfinder/ckfinder.html?type:images',
	filebrowserUploadUrl : '/VPanel/ckfinder/upload.php?type:files',
	filebrowserImageUploadUrl : '/VPanel/ckfinder/upload.php?type:images',
});