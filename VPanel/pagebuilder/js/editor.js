$('.settingsContainer').hide();
$('.pagetitle .toolbar a.settings').parent().removeClass('current');

// configuration
CKEDITOR.config.height = 500;
CKEDITOR.config.autoGrow_maxHeight = 500;
CKEDITOR.config.autoGrow_minHeight = 500;
CKEDITOR.config.extraPlugins = 'tokens,MediaEmbed,youtube,image2,widget,dialog,lineutils,sharedspace';
CKEDITOR.config.removePlugins = 'maximize,resize';
CKEDITOR.config.sharedSpaces = { top: 'ed-top' };

CKEDITOR.replace('mtxDescription');

if ($("#mtxSidebarContent").length > 0) {
    CKEDITOR.replace('mtxSidebarContent');
}

if ($("#mtxSecondarySidebarContent").length > 0) {
    CKEDITOR.replace('mtxSecondarySidebarContent');
}

var currentEditorInstance = 'mtxDescription';
for (name in CKEDITOR.instances) {
    CKEDITOR.instances[name].on('blur', function () {
        currentEditorInstance = this.name;
    });
}