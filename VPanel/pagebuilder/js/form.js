$(function () {
    $('label.required').append('&nbsp;<span style="color: #990000"><b>*</b></span>&nbsp;');
    $('span.statusUpdate').on("click", function () {
        $('input[name=actionRun]').val('compliance');
        var message = "";
        switch ($(this).text()) {
            case "Submit for Compliance Approval":
                message = "Are you sure you want to submit for approval? You will not be able to make any changes while the page is in review. \nNote: This will not save any content changes you have made, please click 'Save' at the top of the page to save any unsaved changes before submitting!";
                break;
            case "Approve":
                message = "Are you sure you want approve this revision? Once approved, this revision will be allowed to publish to the site. ";
                break;
            case "Send Update":
                message = "Are you sure you want to submit an update? \nNote: This will not save any content changes you have made, \nplease click 'Save' at the top of the page to save any content changes!";
                break;
        }

        if (confirm(message)) {
            if ($(this).text() == "Approve") {
                $("#optComplianceStatus").val("Approved");
            } else {
                $("#optComplianceStatus").val('Revision');
            }
            $.ajax({
                type: "POST",
                url: "process.php",
                data: $("#entryForm").serialize(),
                success: function (msg) {
                    addNotify('Compliance Update Submitted!');
                }
            });
            setTimeout("location.reload(true); ", 1000); 
        }
    });

    $('#cboComplianceApproval').on("click", function () {
        $.ajax({
            type: "POST",
            url: "process.php",
            data: { actionRun: "status", hidId: $('input[name="hidId"]').val(), status: $(this).is(':checked') }
        });
    });

    var window = $("#assetListWindow").kendoWindow({
        draggable: true,
        resizable: false,
        width: "600px",
        actions: ["Minimize","Maximize","Close"],
        title: "Asset Library",
        modal: true,
        visible: false
    });

    $("span.insertBtn").on('click', function (e) {
		var html = ""; 
		if($(this).data('addcontainer')) { 
			html += '<div class="pbAsset">\n'; 
			html += "<!-- PageBuilder Asset -->\n"; 
			html += $(this).data('key');
			html += "<!-- End PageBuilder Asset -->\n"; 
			html += '</div>'; 
		} else {
			html += '<span class="pbAsset">\n'; 
			html += "<!-- PageBuilder Asset -->\n"; 
			html += $(this).data('key'); 
			html += "<!-- End PageBuilder Asset -->\n"; 
			html += '</span>\n'; 
		}
        CKEDITOR.instances[currentEditorInstance].insertHtml(html);
        CKEDITOR.instances[currentEditorInstance].updateElement();
        window.data("kendoWindow").close();
    });

    $("#btnViewAsset").on("click", function () {
        $('html, body').animate({ scrollTop: 250 }, 0);
        window.data("kendoWindow").center().open();
    });

    var triggers = $("a.modalInput").overlay({
        // some expose tweaks suitable for modal dialogs 
        expose: {
            color: '#333',
            loadSpeed: 200,
            opacity: 0.9
        },
        closeOnClick: false
    });

    $("#prompt form").submit(function (e) {
        // get user input 
        $.ajaxFileUpload({
            url: 'upload.php',
            secureuri: false,
            fileElementId: 'fleDocument',
            dataType: 'json',
            success: function (data, status) {
                if (typeof (data.error) != 'undefined') {
                    if (data.error != '') {
                        alert(data.error);
                    } else {
                        $("#txtLink").val(data.msg);
                        var arFile = data.msg.split('/');
                        var docNm = arFile[arFile.length - 1];
                        $("#cboDocuments").append($("<option></option>").attr("value", docNm).text(docNm));
                        // close the overlay 
                        triggers.eq(0).overlay().close();
                        alert("Document File Uploaded Successfully");
                    }
                }
            },
            error: function (data, status, e) {
                alert(e);
            }
        });

        // do not submit the form 
        return e.preventDefault();
    });

    $("select#cboDocuments").change(function () {
        var id = $(this).val();

        if (id.length > 0) {
            $("#txtLink").val("/uploads/userfiles/files/documents/" + id);
        }
    });
}); 