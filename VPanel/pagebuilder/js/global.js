var slideTime = 500;
var lastFilter = '';
function filterPages(which){
    if(which == lastFilter){return;}
    $('.page-filter a').removeClass('inactive');
    switch(which){
        case 'all':
            $('[data-collapsetag="true"]').hide();
            $('[data-collapsetag="true"]').attr('data-collapsetag','');
            $('ul.vpanel-table li').not('.head').find('> .con').slideDown(slideTime);
            $('.page-filter a:eq(0)').addClass('inactive');
            break;
        case 'active':
            $('[data-collapsetag="true"]').hide();
            $('[data-collapsetag="true"]').attr('data-collapsetag','');
            $('ul.vpanel-table li').not('.head').find('> .con').slideDown(slideTime);
            $('ul.vpanel-table li.inactive').not('.head').find('> .con').slideUp(slideTime);
            $('.page-filter a:eq(1)').addClass('inactive');

            break;
        case 'inactive':
            // expand invisibles
            $.each( $('ul.vpanel-table li.inactive').not('.head'), 

                function(k,v){
                    var showParent = function(elem,index){


                        if(index > 8) return;

                        $(elem).find('> con:hidden, > ul.subpages:hidden').attr('data-collapsetag','true');
                        $(elem).find('> con:hidden, > ul.subpages:hidden').show();
                        showParent($(elem).parent(), index + 1);
                    };

                    showParent($(v), 0);

            });

            $('ul.vpanel-table li.inactive').not('.head').find('> .con').slideDown(slideTime);
            $('ul.vpanel-table li').not('.inactive,.head').find('> .con').slideUp(slideTime);
            $('.page-filter a:eq(2)').addClass('inactive');
            break;
    } 
    lastFilter = which;
}

// popup notify msg
var errorTag = '#ERROR#';
function addNotify(msg){
	var notify = $('.notify');
    var msgClass = 'msg';
    if(msg.indexOf(errorTag)!= -1){ msgClass = 'error'; msg = msg.replace(errorTag,''); customDuration = 6000;}else{customDuration = 3000;}
    // add notify if it doesn't exist
    if(notify.length <= 0){ $('body').append('<div class="notify notify-error"><a class="'+msgClass+'">'+msg+'</a></div>'); notify = $('.notify');}else{ return;  }
    $(notify).css({'top':'-200px'});    
    $(notify).animate({top:'0px'},{duration:220, complete: function(){
        $(this).delay(customDuration).animate({top:'-100px'},{duration:420,complete:function(){$('.notify').remove();}});
    }});
}

function sortStart(event){}
function sortSave(){
    // gather id list
    var sList = '';
    $.each($(this).find('> [data-id]'),function(k,v){
        sList += '&recordsArray[]='+$(v).attr('data-id');
    });

    var order = 'actionRun=sort' + sList;    

    $.post("process.php", order, function(theResponse){
        addNotify(theResponse);
    }); 

}

// reparent a page
var reparentPage = function(event){        
    // just set hidden field
    var newParentId = $(this).find('[type="radio"]').val();
    $('[name="hidParentId"]').val(newParentId);
    // if this is a new page, don't post
    if($('[name="actionRun"]').val() == 'add'){return;}
    var pageId = $('[name="hidId"]').val();

    $.post('process.php?parentId='+newParentId+'&id='+pageId, 'actionRun=parent', function(theResponse) {        
        addNotify(theResponse);
    }); 

};


// delete page
var deletePage = function(event){

    stopEventPropigation(event);

    var pageId = $(this).attr('data-id');

    $.post(
        'process.php?parentId='+$(this).attr('data-parent')+'&id='+pageId
        , 'actionRun=delete'
        , function(theResponse){
            // if delete was good, remove the row and update the subpage count
            if(theResponse.indexOf(errorTag) == -1){
                $('li[data-id="'+pageId+'"]').slideUp('slow',function(){$(this).remove();});
                var subpage = $('li[data-id="'+pageId+'"]').parent().parent().find('> .con .left span[data-count]');
                var count = parseInt($(subpage).attr('data-count')) - 1;
                var newHtml = count + ' Subpages';
                if(count <= 0){newHtml = '';}
                $(subpage).html(newHtml);            
                $(subpage).attr('data-count',count);
            }

            addNotify(theResponse);
    }); 


};
// animate delete confirmation
var deleteConfirmClass = 'icon-cancel-confirm';
var confirmDelete = function(event){

    // unbind click so we can't confirm > 1 time
    $(this).unbind('click');

    var confirmHtml = 'Confirm?';
    var aTime = 200;  // animate time
    var pTime = 2000; // pause time
    var iOffset = 86; // move this much

    var thisValue = $(this).html();

    // animate edit button
    $(this).parent().find('.icon-pencil').animate(
        {'right':'-'+iOffset+'px'},
        {duration:aTime,complete:function(){

            $(this).delay(pTime).animate(
                {'right':0},
                {duration:aTime,complete:function(){}}
            );        
            }
        }
    );

    // animate delete button
    $(this).addClass(deleteConfirmClass);
    $(this).animate(
        {'right':iOffset+'px'},
        {duration:aTime,complete:function(){
            // allow actual delete binding
            $(this).click(deletePage);

            $(this).html(confirmHtml);
            $(this).delay(pTime).animate(
                {'right':0},
                {duration:aTime,complete:function(){
                    // put original button text back
                    $(this).html(thisValue);
                    $(this).removeClass(deleteConfirmClass);
                    // rebind confirm delete
                    $(this).unbind('click'); $(this).click(confirmDelete);
                    } 
                }
            );        
            }
    });

    stopEventPropigation(event);
};


$(document).ready(function () {

    // tree table stuff ----------------------------
    // sorting

    $('ul.vpanel-table,ul.subpages').sortable({ items: '> li:not(.head,.home-page)', distance: 2, cursor: 'move', update: sortSave, handle: '> .con' });

    // row button events -----
    $('ul.vpanel-table li a.delete').click(confirmDelete);
    $('ul.vpanel-table li a.edit').click(function (event) {
        stopEventPropigation(event);
    });


    // show/hide buttons
    // BUTTON VISIBILITY ------------
    $('ul.vpanel-table li .con').hover(
        function (event) {
            $(this).find('> .right > .edit,> .right > .delete,> .right > .add-list').css({ 'display': 'block' });
        }
        , function (event) {
            $(this).find('> .right > .edit,> .right > .delete,> .right > .add-list').css({ 'display': 'none' });
        }
    );
    $('ul.vpanel-table li').click(stopEventPropigation);
    $('ul.subpages').hide();
    $('ul.subpages').parent().find('.left').click(function (event) {
        stopEventPropigation(event);
        $(this).parent().parent().find('> ul.subpages').eq(0).slideToggle(slideTime);
    });

    // SETTINGS BUTTON  setup
    var settingsButton = $('a.settings');
    if (settingsButton.length != 0) {

        // toggle settings open/closed
        settingsButton.click(function () {
            $('.settingsContainer').toggle();
            var classNameOn = 'current';
            var settingsOpen = $(this).parent().hasClass(classNameOn);

            if (settingsOpen) {
                $(this).parent().removeClass(classNameOn);
            } else {
                $(this).parent().addClass(classNameOn);
            }
        });

    }

    // SAVE BUTTON setup
    var saveButton = $('#btnSave');
    if (saveButton.length != 0) {
        // toggle settings open/closed
        saveButton.click(doHybridSave);

    }

    // SAVE BUTTON setup
    var publishButton = $('#btnPublish');
    if (publishButton.length != 0) {
        publishButton.click(doHybridSave);
    }

    // PREVIEW setup
    var previewWindow = false;
    var previewButton = $('a.preview');
    if (previewButton.length != 0) {
        // toggle settings open/closed
        previewButton.click(function () {

            var urlSubmit = $(this).attr('href');
            var form = $('form#entryForm')[0];
            var oldAction = $(form).attr('action');
            var specs = "width=1200,height=700,location=1,resizeable=1,status=1,scrollbars=1,toolbar=1,menubar=1";

            // if it's a static link / file go there instead and don't submit
            var linkValue = $('#txtLink').val();
            if ($('#txtLink').length > 0 && linkValue != '') {
                urlSubmit = linkValue;
                window.open(urlSubmit, 'PagePreview', specs);
                return false;
            }

            if (previewWindow !== false) {
                previewWindow.close();
            }
            previewWindow = window.open(urlSubmit, 'PagePreview', specs);
            $(form).attr("target", "PagePreview");
            $(form).attr("action", urlSubmit);
            form.submit();
            $(form).removeAttr("target");
            $(form).attr("action", oldAction);

            return false;

        });

    }

    // reparent rows in category list on edit page
    $('.select-category').click(
        function () {
            //reparentPage
        }
    );

    // view page links on tree table
    $('.view-page-link').click(function (event) {
        stopEventPropigation(event);
        var pageId = $(this).attr('data-id');
        if (pageId == 'HOME') {
            urlSubmit = '/';
        } else {
            urlSubmit = '/pagebase.php?preview=true&pbid=' + pageId;
        }

        var link = $(this).attr('data-link');
        var linkType = $(this).attr('data-linktype');
        if (linkType == 'PDF-DOC') { link = '/uploads' + link; } else { }
        if (link != '') { urlSubmit = link; }

        var specs = "width=1200,height=700,location=0,resizeable=1,status=1,scrollbars=1";
        window.open(urlSubmit, 'PagePreview', specs);
    });
});

var bSaveInProgress = false;
var doHybridSave = function () {
    if (bSaveInProgress) return false;

    bSaveInProgress = true;
    // update the wysiwyg textareas
    for (k in CKEDITOR.instances) { CKEDITOR.instances[k].updateElement(); }

    // if we have the template title, and the navigation name is empty, put the title in the name field

    if ($('input[name="txtName"]').val() == '' && $('input[name="txtNameTitle"]').val() != '') { 
        $('input[name="txtName"]').val($('input[name="txtNameTitle"]').val()); 
    }

    if (location.search.indexOf('template') === -1 && location.search.indexOf('system') === -1) {
        if ($(this).attr("id") == 'btnSave' && location.search.indexOf('tpload') == -1) {
            $('input[name=actionRun]').val('modify');
        }
        else if (location.search.indexOf('modify') != -1 && location.search.indexOf('tpload') == -1) {
            $('input[name=actionRun]').val('publish');
        }
        else {
            $('input[name=actionRun]').val('add');
        }
    } 

    var data = $('form#entryForm').serialize();
    var processScript = 'process.php';

    $.post(processScript, data, function (theResponse) {
        var redirectTag = '#EDIT-REDIRECT';
        var templateTag = '#TEMPLATE-REDIRECT';
        if (theResponse.indexOf(redirectTag) != -1) {
            window.location = '?view=modify&parentId='
            + $('input[name="parent-category-page"]:checked').val()
            + '&id=' + theResponse.replace(redirectTag, '');
        } 
		else if(theResponse.indexOf(templateTag) != -1) {
            window.location = '?view=template&id=' + theResponse.replace(templateTag, '');			
		}
		else {
            addNotify(theResponse);
        }
        bSaveInProgress = false;

    });

};

// TEMPLATE LIST
//  - set selected class
//  - set hidden <input> 'template-select'
//  - send template change
//  - reload page
$('#cboComplianceApproval').on("click ready load", function () {
    if ($(this).is(':checked')) {
        $('#divCompliance').show();
    } else {
        $('#divCompliance').hide();
    }
});

$('#cboComplianceApproval').trigger('load');



// template magics
$('.layout-list > li').click(function () {
    var warningMessage = $('input[name="actionRun"]').val() == 'modify' ? 'Are you sure you want to switch your page template? \n\nThis will autosave any changes you have made so far. ' : 'Are you should you want to switch your page template? \n\nThis will lose all unsaved content you have entered. \nPlease click "Save" first, before changing templates! ';
    if (confirm(warningMessage)) {
        var selectedClass = 'current-template';
        $('.template-list > li').removeClass(selectedClass);
        $(this).addClass(selectedClass);

        // set the template id and submit the page
        var templateId = $(this).attr('data-templateid');
        $('#template-select').val(templateId);
        $('#hidTemplateId').val(templateId);


        // if this is the modify page we want to send the template change then reload
        if ($('input[name="actionRun"]').val() == 'modify') {
            $('#btnSave').click();
            setTimeout(function () { window.location.reload(true); }, 300); 
        } else if ($('input[name="actionRun"]').val() == 'add') {
            window.location = 'index.php?view=add&templateid=' + templateId + '&childof=' + $('input:radio[name=parent-category-page]:checked').val();
        }
    }
}); 

$('.template-list > li').on("click", function (){
    if(!$(this).parent().hasClass('layout-list')) {
        var selectedClass = 'current-template';
        $('.template-list > li').removeClass(selectedClass);
        $(this).addClass(selectedClass);

        // set the template id and submit the page
        var templateId = $(this).attr('data-templateid');
        $('#template-select').val(templateId);
        if($(this).data('cmsid')) {
            window.location = 'index.php?view=modify&id=' + $(this).data('cmsid') + '&tpload=1'; 
        } else {
            if($(this).parent().data("what") == 'template') {
                window.location = 'index.php?view=add&what=template&templateid=' + templateId;                    
            }
            else {
                window.location = 'index.php?view=add&templateid=' + templateId + '&childof=' + $('input:radio[name=parent-category-page]:checked').val();                    
            }
        } 
    }
});