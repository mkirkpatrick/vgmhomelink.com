// JavaScript Document
$(function() {

	$("#contentOrder ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
		var order = $(this).sortable("serialize") + '&actionRun=sort'; 
		$.post("process.php", order, function(theResponse){
			$("#contentResults").html(theResponse);
		}); 															 
	}								  
	});

});