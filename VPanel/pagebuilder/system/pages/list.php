<?php if (!isset($siteConfig)) die("System Error!"); ?> 

<div class="right subcontent last">
    <h2>System Pages</h2>
    <p>Below lists all of the pages that have been created for this site that are not visible to the clients.</p>

  <?php
		if ($form->getNumErrors() > 0) {
			$errors	= $form->getErrorArray();
			foreach ($errors as $err) echo $err;
		} else if (isset($_SESSION['processed'])) {
			switch($_SESSION['processed']) {
				case 'added':
                    echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Added Successfully!')</script>";
					//echo "<div class=\"valid\">" . $_WEBCONFIG['ENTITY_NAME'] . " Added Successfully!</div>\n";
					break;
				case 'updated':
				    echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Updated Successfully!')</script>";	
                    //echo "<div class=\"valid\">" . $_WEBCONFIG['ENTITY_NAME'] . " Updated Successfully!</div>\n";
					break;
				case 'deleted':
					echo "<script>addNotify('{$_WEBCONFIG['ENTITY_NAME']} Deleted Successfully!')</script>";
                    //echo "<div class=\"valid\">" . $_WEBCONFIG['ENTITY_NAME'] . " Deleted Successfully!</div>\n";
					break;
			}
			unset($_SESSION['processed']);
		}

	  $sql = "SELECT {$_WEBCONFIG['TABLE_PREFIX']}_title, {$_WEBCONFIG['TABLE_PREFIX']}_dynamic_link, {$_WEBCONFIG['TABLE_PREFIX']}_published_date, url_direct, {$_WEBCONFIG['TABLE_PREFIX']}_id
		      FROM {$_WEBCONFIG['COMPONET_TABLE']} 
		      WHERE cms_is_historic = 0 and cms_is_system_page = 1 AND cms_is_editable = 0
		      ORDER BY {$_WEBCONFIG['TABLE_PREFIX']}_id, {$_WEBCONFIG['TABLE_PREFIX']}_title, {$_WEBCONFIG['TABLE_PREFIX']}_published_date DESC
		      LIMIT 1000";
		$page	= Database::Execute($sql);
  ?>

  <table id="grid">
	<thead> 
	  <tr> 
        <th data-field="name">Name</th>
	    <th data-field="lastDate">Last Updated</th>
        <th data-field="options" style="text-align: center;">Options</th>
	  </tr>
	</thead>
	<tbody>
  <?php
		if ($page->Count() > 0) {
			while ($page->MoveNext()) {
				print '<tr> 
						<td>' . $page->cms_title . '</td>
						<td>' . date("m/d/Y g:i A", strtotime($page->cms_published_date)) . '</td>
						<td><div align="center">
							<a href="/pagebase.php?preview=true&pbid=' . $page->cms_id . '" class="blue button-slim" style="width: 50px" target="NewPage">Preview</a>
							<a href="javascript:edit(' . $page->cms_id . ');" class="green button-slim" style="width: 50px">Edit</a>
							<!--<a href="javascript:remove(' . $page->cms_id . ');" class="red button-slim" style="width: 50px">Delete</a>--></div>
						</td>';
						print '</tr>' . PHP_EOL;
			}// end while

		}
  ?>

	</tbody>
  </table>
</div><!--End content-->

<script type="text/x-kendo-template" id="searchBarTemplate">
    <div class="toolbar floatRight">
        <div>
            <input id="txtSearch" placeholder="Search Grid" type="text" style="width: 175px" />
            <a id="clearTextButton">Clear</a>
        </div>
    </div>
</script>