<?php if (!isset($siteConfig)) die("System Error!"); ?> 
<div class="right subcontent last">
    <div class="pagebuttons floatRight">
        <?php $landingPageTemplateID = PB_Helper::getLandingPageTemplateId(); 
        if(!is_null($landingPageTemplateID)) { ?>
            <a href="?view=modify&id=<?= $landingPageTemplateID?>&tpload=1" class="add-page button silver floatLeft">Landing Page</a>
        <?php } ?>
        <a href="?view=add&amp;what=link" class="add-link button icon-link silver floatLeft">Add Link</a>
        <a href="?view=add&amp;what=doc" class="add-doc button icon-doc silver floatLeft">Add Document</a>
        <?php if(UserManager::isWebmaster() && $_WEBCONFIG['ENABLE_CUSTOM_TEMPLATES'] == 'true') { ?>
            <a href="?view=add&what=template" class="add-template button icon-template blue floatLeft">Add Template</a>
        <?php } ?>        
        <a href="?view=add" class="add-page button icon-doc-text green floatLeft">Add Page</a>
    </div><!-- eof add-pages -->

    <h2>Pages</h2>
    <p>Below lists all of the pages that have been created for your site. You can add a new page/link/document, edit a page or completely remove it. Use the nav on the left to manage uploads, add assets/modules or undo a trashing.</p>

    <div class="template-container">
        <?= PB_Helper::cmsTreeTableView($_WEBCONFIG['SITE_ROOT_PAGE']); ?>
    </div>
</div><!--End content-->