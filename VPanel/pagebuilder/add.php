<?php
if (!isset($siteConfig)) die("System Error!");

$addWhat = isset($_GET['what']) ? $_GET['what'] : '';
$childOf = isset($_GET['childof']) ? $_GET['childof'] : $_WEBCONFIG['SITE_ROOT_PAGE']; 
$parentId = $childOf ;

$dynamicDefault = '';
$type = ''; 
$addTitle = 'Add a New ';	
$bIsPage = false;

switch($addWhat){
    case 'doc':
        $type = 'Document';
        $addTitle .= $type; 
        $dynamicDefault = 'PDF-DOC';		
        break;
    case 'link': 
        $type = 'Link';
        $addTitle .= $type; 
        $dynamicDefault = 'STATIC';		
        break;
    case 'template':
        $type = 'Page';
        $addTitle .= 'Template'; 
        $bIsPage = true;
        break;        
    default:
        $type = 'Page';
        $addTitle .= $type; 
        $bIsPage = true;
        break;
}

$defaultTemplate = ''; 

if($bIsPage) { 
    $defaultTemplate = $_WEBCONFIG['DEFAULT_LAYOUT_ID'];
    if(isset($_GET['templateid'])){
        $defaultTemplate = $_GET['templateid'];
    }
    $arTemplates = array();
    $sql = "SELECT * FROM tbl_cms_templates ORDER BY cmst_name";
    $rec = Database::Execute($sql);

    while ($rec->MoveNext()) {
        $arTemplates[$rec->cmst_id] = $rec->cmst_name;
    }// end while
    ?>
    <style type="text/css"> ul.category-list { height: 470px; } </style>
    <script type="text/javascript">
        var templateWindow = null; 
        $(function () {
            templateWindow = $("#templateWindow").kendoWindow({
                draggable: false,
                resizable: false,
                width: "700px",
                height: "<?= 170 + 110 * ceil(PB_Templates::getCustomTemplateCount() / 5) ?>px",
                title: "",
                modal: true, 
                visible: false, 
                close: function(e) { e.preventDefault(); $('#DefaultButton').trigger('click'); }
            }); 

            if(location.search.indexOf('tpload') == -1 && location.search.indexOf('templateid') == -1) {
                $('html, body').animate({ scrollTop: 20 }, 0);
                templateWindow.data("kendoWindow").center().open();
            }
        });
    </script>

    <div id="templateWindow">
        <?php 
        if($addWhat == 'template') {
            $defaultClick = isset($_WEBCONFIG['DEFAULT_TEMPLATE_ID']) && !isNullOrEmpty($_WEBCONFIG['DEFAULT_TEMPLATE_ID']) ? "window.location='index.php?view=modify&id=" . $_WEBCONFIG['DEFAULT_TEMPLATE_ID'] . "&tpload=1'" : "window.location='index.php?view=add&what=template&templateid=" . $_WEBCONFIG['DEFAULT_LAYOUT_ID'] ."'";            
        }
        else {
            $defaultClick = isset($_WEBCONFIG['DEFAULT_TEMPLATE_ID']) && !isNullOrEmpty($_WEBCONFIG['DEFAULT_TEMPLATE_ID']) ? "window.location='index.php?view=modify&id=" . $_WEBCONFIG['DEFAULT_TEMPLATE_ID'] . "&tpload=1'" : "window.location='index.php?view=add&templateid=" . $_WEBCONFIG['DEFAULT_LAYOUT_ID'] ."'";            
        } ?>
        <span id="DefaultButton" style="padding: 2px; width: 150px; margin-left: 270px" class="k-button" onclick="<?= $defaultClick ?>">Load Default Template</span>
        <?= PB_Templates::makeTemplateList($addWhat); ?>  
    </div>
    <?php } ?> 


<form id="entryForm" method="post" action="process.php" enctype="multipart/form-data">
    <input type="hidden" name="actionRun" value="add" />
    <input type="hidden" name="hidId" value="<?= $iNum; ?>" />
    <input type="hidden" name="hidContId" value="" />
    <input type="hidden" name="hidParentId" id="hidParentId" value="<?= $parentId; ?>">
    <input type="hidden" name="hidTemplateId" id="hidTemplateId" value="<?= $defaultTemplate; ?>">
    <input type="hidden" name="cboDynamic" value="<?= $dynamicDefault; ?>" />
    <input type="hidden" name="txtMetaTitle" value="" />
    <input type="hidden" name="txtMetaKeywords" value="" />
    <input type="hidden" name="txtMetaDescription" value="" />
    <input type="hidden" name="addWhat" value="<?= $addWhat ?>" />

    <div class="subcontent right last">
        <h2 class="floatLeft"><?= $addTitle; ?></h2>

        <div class="buttons top floatRight">
            <a id="btnPublish" href="javascript:void(0)" class="save floatRight button silver icon-floppy">Save</a>
            <a href="javascript:void(0)" class="settings floatRight silver button icon-cog">Settings</a>
        </div>

        <div class="clearfloat"></div>

        <input name="txtOrder" type="hidden" value="" />

        <div class="settingsContainer eightcol">

            <div class="page-settings">

                <div class="header"><h3><?= $type ?> Settings</h3></div>

                <ul class="field-list">
                    <li>
                        <label for="txtName" class="required mainlabel">Nav Name</label>
                        <input type="text" name="txtName" id="txtName" value="" maxlength="100" required="required" />
                        <?php showHelpTip("This name is used in navigation menus. It won't show up on the page itself."); ?>
                    </li>
                    <li>
                        <label for="txtSubText" class="mainlabel">Sub Text</label>
                        <input type="text" name="txtSubText" id="txtSubText" value="" maxlength="100" /><?= $form->error("txtSubText");?>
                        <?php showHelpTip("This name is used in the navigation menu. It will show as the title text"); ?>
                    </li>

                    <?php if ($addWhat =="link") { ?>
                        <li id="trStaticLink">
                            <label for="txtLink" class="mainlabel">Link</label>
                            <input type="text" name="txtLink" id="txtLink" size="63" maxlength="255" />             
                        </li>
                        <li>
                            <label class="mainlabel">Window Target</label>
                            <label for="optTarget1"><input name="optTarget" type="radio" id="optTarget1" value="_self" checked /> Same Window</label>  
                            <label for="optTarget0"><input name="optTarget" type="radio" id="optTarget0" value="_blank" /> New Window</label>  
                        </li>    

                        <?php if($_WEBCONFIG['SITE_TYPE'] == 'BANK') { ?>
                            <li>
                                <label class="mainlabel">Show Disclaimer</label>
                                <label for="optDisclaimer1"><input name="optDisclaimer" type="radio" id="optDisclaimer1" value="1" /> Yes</label>  
                                <label for="optDisclaimer0"><input name="optDisclaimer" type="radio" id="optDisclaimer0" value="0" checked="checked" /> No</label>  
                                <?= $form->error("optDisclaimer");?>
                                <?php showHelpTip("Show the disclaimer for people leaving the site. "); ?>
                            </li>
                        <?php }                        
                     } ?>                    
                    
                    <?php if (isNullOrEmpty($addWhat) || $addWhat == 'template') { ?>
                        <li>
                            <label for="url_suffix" class="mainlabel">Direct Url</label>
                            <input type="text" value="" id="url_direct" name="url_direct" class="url-direct" />
                            <?php showHelpTip("Use this option to create a more user friendly page url, if you leave this blank PageBuilder will auto-create a url for you. "); ?>
                            <div class="clearfix"><label class="mainlabel">Preview Direct Url</label> <?= $_SERVER['ROOT_URI'] ?>/<span id="urlPreview"></span></div>
                        </li>                		

                        <li>
                            <label class="mainlabel">Show Breadcrumb</label>
                            <label for="optBreadCrumbY"><input id="optBreadCrumbY" name="optBreadCrumb" type="radio" value="1" checked="checked" /> Yes</label>
                            <label for="optBreadCrumbN"><input id="optBreadCrumbN" name="optBreadCrumb" type="radio" value="0" /> No</label>
                            <?php showHelpTip("Do you want to show the bread crumb on this page?"); ?>
                            <?= $form->error("optBreadCrumb");?>
                        </li>

                        <?php if ($_WEBCONFIG['HAS_MEMBER_LOGINS'] == "true") { ?>
                            <li>
                                <label class="mainlabel">Members Only</label>
                                <label for="optSecureY"><input id="optSecureY" name="optSecure" type="radio" value="1" /> Yes</label> 
                                <label for="optSecureN"><input id="optSecureN" name="optSecure" type="radio" value="0" checked="checked" /> No</label> 
                                <?php showHelpTip("Allow only member logins to view this page. "); ?>
                                <?= $form->error("optSecure");?>
                            </li>
                            <?php } } ?>

                    <li>
                        <label class="mainlabel">Show in Nav</label>
                        <label for="optNav1"><input name="optNav" type="radio" id="optNav1" value="1" checked/> Yes</label>  
                        <label for="optNav0"><input name="optNav" type="radio" id="optNav0" value="0" /> No</label>
                        <?php showHelpTip("Should the page appear in the navigation menus?"); ?>
                    </li>

                    <?php if ($addWhat=="link" || $addWhat == "doc") { ?>		
                        <li>
                            <label for="optStatus" class="mainlabel">Status</label>
                            <label for="optAStatus"><input name="optStatus" type="radio" id="optAStatus" value="1" checked /> Active</label>  
                            <label for="optIStatus"><input name="optStatus" type="radio" id="optIStatus" value="0" /> Inactive</label>  
                            <?= $form->error("optStatus");?>
                        </li>
                    <?php } ?>

                    <?php if ($addWhat == "doc") { ?>	
                        <li>
                            <strong>Documents</strong> &nbsp;&nbsp;&nbsp; <small>Choose One...</small><br />
                            <input type="hidden" name="txtLink" id="txtLink" />
                            <input type="hidden" name="optTarget" id="optTarget" value="_blank" />
                            <select id="cboDocuments" size="8" style="width:400px;">
                                <?php
                                $upload_dir	= $_WEBCONFIG['UPLOAD_FOLDER_DOCUMENTS'];
                                if ($handle = opendir(filePathCombine($_SERVER['DOCUMENT_ROOT'], $upload_dir))) {
                                    /* This is the correct way to loop over the directory. */
                                    while (false !== ($file = readdir($handle))) {
                                        if ($file != "." && $file != "..") {
                                            printf("<option value=\"%s\">%s</option>\n", $file, $file);
                                        }
                                    }
                                    closedir($handle);
                                }
                                ?>
                            </select><br />
                        </li>
                        <li class="clearfix"><a href="javascript:void(0);" rel="#prompt" class="modalInput floatLeft green button">Upload Document</a></li>
                  <?php } ?>			
                </ul>
            </div>
        </div> <!-- end settings container -->

        <!-- category / parent page container -->
        <div class="settingsContainer parent-pages sc-right-column fourcol last">	
            <div class="header"><h3>Parent</h3></div>	 
            <?= cmsPageRadioList($_WEBCONFIG['SITE_ROOT_PAGE'], $iNum); ?>
        </div>    

        <?php 
        // close the form if we have no template
        if ($addWhat != '' && $addWhat != 'template') {  ?>
        </div><!--End content-->
        </div>
    </form>
    <?php 
} else { ?>
    <div class="template-container clearfix">
        <?php 
        $addNew = true;
        require_once(filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], '/pagebuilder/layouts/', $arTemplates[$defaultTemplate])); 
        ?> 
    </div><!--End content-->

    <div class="buttons clearfix">
        <a class="floatLeft button silver" href="index.php">Back to Pages</a>
    </div>

    </div>
    </form>

    <div id="assetListWindow">
        <?= PB_Helper::printAssetList(); ?>
    </div>

    <?php 
} ?>

<!-- user input dialog -->
<div id="prompt" class="modal">
    <h2>Upload Document</h2>
    <p>Upload a DOC or PDF document.</p>

    <form id="uploadForm" enctype="multipart/form-data">
        <label>Document:</label><br />
        <input type="file" name="fleDocument" id="fleDocument" /><br />
        <button type="submit" class="submit"> OK </button>
        <button type="button" class="close"> Cancel </button>
    </form>
</div> 
<?php
//------------------------------
function cmsPageRadioList($index,$hideId) { 
    //------------------------------
    global $_WEBCONFIG;
    $sMarkup = '';

    $s = "SELECT * FROM tbl_cms 
    WHERE cms_is_deleted=0 AND cms_is_historic = 0 AND cms_parent_id = $index AND cms_is_system_page = 0 AND cms_is_template = 0 
    ORDER BY cms_parent_id, cms_display_order, cms_id, cms_title"; 		
    $r = Database::Execute($s); 

    if ($r->Count() == 0 ) 
        return;

    if ($index == $_WEBCONFIG['SITE_ROOT_PAGE']) {
        $sMarkup .= '<ul class="category-list" >
        <li class="top">
        <label class="select-category">
        <input type="radio" name="parent-category-page" id="parent-category-page-0" value="' . $_WEBCONFIG['SITE_ROOT_PAGE'] . '" checked="checked"/>[&nbsp;&nbsp;Root Level&nbsp;&nbsp;]
        </label>
        <ul class="children" data-parent="-1">' . "\n";
    } else {
        $sMarkup .= '<ul class="children" data-parent="' . $index . '">'."\n";
    }

    while ($r->MoveNext()) {
        // skip self
        if($r->cms_id == $hideId ){continue;}
        // active /inactive
        $sActiveClass = '';
        if ($r->cms_is_active != 1) { $sActiveClass="inactive"; }

        $sMarkup .= "<li class=\"{$sActiveClass}\" data-id=\"{$r->cms_id}\" data-parent=\"" . $index . "\">"."\n";

        $sName  = '<label class="select-category">'."\n";
        $selected = '';
        if ($GLOBALS['childOf'] == $r->cms_id) { $sMarkup =  str_replace('checked="checked"','',$sMarkup); $selected = ' checked="checked" '; }
        $sName .= 	'<input type="radio" name="parent-category-page" id="parent-category-page-' . $r->cms_id . '" value="' . $r->cms_id . '" ' . $selected . ' />';
        $sName .= 	$r->cms_title . "\n";
        $sName .= '</label>' . "\n";

        $sMarkup .= '' . $sName . '';
        $sMarkup .= cmsPageRadioList($r->cms_id,$hideId);
        $sMarkup .= "</li>\n";
    }

    if ($index == $_WEBCONFIG['SITE_ROOT_PAGE']) {
        return $sMarkup .= "</ul></li></ul>\n";
    } else {
        return $sMarkup .= "</ul>\n";
    }
}
?>
<script type="text/javascript">
    $('#url_direct').keyup(function() { $(this).val($(this).val().replace(/[^A-Za-z0-9\/\-_\s]/g, '').replace(/[_\s]/g, '-')); });
    $('#url_direct').on('keyup keypress blur change click load', function() { $('#urlPreview').text($('#url_direct').val()); });
    $('#url_direct').trigger("load"); 
</script>