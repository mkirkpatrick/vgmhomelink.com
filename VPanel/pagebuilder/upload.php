<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

$upload_dir	= $_WEBCONFIG['UPLOAD_FOLDER_DOCUMENTS'];
$error		= "";
$msg		= "";

if (isset($_FILES["fleDocument"]) && strlen($_FILES["fleDocument"]['name']) > 0) {
	$fieldValue		= $_FILES["fleDocument"]['name'];
	$fileExt		= strtolower(getExtName($fieldValue));
	$documentPath	= $_FILES["fleDocument"]['name'];

	if (move_uploaded_file($_FILES["fleDocument"]['tmp_name'], filePathCombine($_SERVER['DOCUMENT_ROOT'], $upload_dir, $documentPath))) {
		$msg = $upload_dir . $documentPath;
	} else {
		throw new exception("Error uploading the document file!");
	}
} else {
	$error .= "* Document File is required!";
}

echo "{";
echo "error: '" . $error . "',\n";
echo "msg: '" . $msg . "'\n";
echo "}";
?>
