<?php
use Forbin\Library\Classes\Member; 

if (!isset($siteConfig))  die("System Error!");

$isHomePage   = $_WEBCONFIG['SITE_ROOT_PAGE'] == $iNum; 
$isTemplate   = $_GET['view'] == "template"; 
$newTemplate  = isset($_GET['nt']); 
$templateLoad = isset($_GET['tpload']); 

if ($form->getNumErrors() > 0) {
    $hidId				= $form->value("hidId");
    $txtName			= $form->value("txtName");
    $txtSubText			= $form->value("txtSubText");
    $txtOrder			= $form->value("txtOrder");
    $optStatus			= $form->value("optStatus");
    $optSecure			= $form->value("optSecure");
    $optNav				= $form->value("optNav");
    $cboTemplate		= $form->value("cboTemplate");
    $cboDynamic			= $form->value("cboDynamic");
    $optTarget			= $form->value("optTarget");
    $optDisclaimer		= $form->value("optDisclaimer");
    $txtLink			= $form->value("txtLink");
    $optBreadCrumb      = $form->value("optBreadCrumb"); 
    $optSystem          = $form->value("optSystem"); 
    $optSystemEditable  = $form->value("optSystemEditable"); 
    $optSslPage         = $form->value("optSslPage"); 
    $optTemplate        = $form->value("optTemplate");
    $backgroundImage    = $form->value("backgroundImage");
    $cboComplianceApproval = false; 
} else {
    $pgId	= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
    $lastRevisionId = PB_Revisions::getLastRevisionId($iNum); 
    $iNum = isNullOrEmpty($lastRevisionId) || $view == 'revision' ? $pgId : $lastRevisionId; 
    print PHP_EOL . "<!-- Draft ID: $iNum -->" . PHP_EOL; 
    $sql = "SELECT * FROM tbl_cms WHERE cms_id = $iNum";
    $record	= Database::Execute($sql);
    $record->MoveNext();

    if ($record->Count() == 0) {
        throw new Exception("Unable to find record in the database"); 
    }

    $hidId				= isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
    $txtName			= $templateLoad ? '' : htmlspecialchars($record->cms_title);
    $txtSubText			= $templateLoad ? '' : htmlspecialchars($record->cms_sub_title);
    $txtOrder			= $templateLoad ? '' : $record->cms_display_order;
    $optNav				= $record->cms_show_nav; 
    $url_direct			= $record->url_direct;
    $optStatus			= $templateLoad ? 0 : $record->cms_is_active;
    $optSecure			= $record->cms_is_secure;
    $optBreadCrumb 		= $record->cms_show_breadcrumb; 
    $cboTemplate		= $record->cmst_templates_id;
    $cboDynamic			= $record->cms_dynamic_id;
    $txtLink			= $record->cms_dynamic_link;
    $optTarget			= $record->cms_dynamic_link_target;
    $optDisclaimer		= $record->cms_show_disclaimer; 
    $isPublished        = $record->cms_is_published; 
    $optSystem          = $record->cms_is_system_page; 
    $optSystemEditable  = $record->cms_is_editable; 
    $optSslPage         = $record->cms_is_ssl;
    $backgroundImage    = $record->background_image;
    $optTemplate  	    = $templateLoad ? 0 : $record->cms_is_template; 
    if ($templateLoad) {
        $url_direct = "";
    }
    else if(isNullOrEmpty($url_direct) && !isNullOrEmpty($txtLink)) {
        $url_direct = $txtLink; 
    } else if(isNullOrEmpty($url_direct)) {
        $url_direct = Repository::getById('tbl_cms', $hidId)->url_direct;
    }

    switch($cboDynamic){
        case 'PDF-DOC':
            $type = 'Document';		
            break;
        case 'STATIC': 
            $type = 'Link';	
            break;
        default:
            $type = $isTemplate ? "Template" : "Page"; 
            break;
    }

    $bIsPage = false;	 
    if ($record->cms_dynamic_id == '' || $record->cms_dynamic_id  == 'NULL'){
        $bIsPage = true;
    }
}

$thisPageData = PB_Revisions::getPageOrRevisionData($iNum);
$thisPageData->MoveNext();

$actualParentId = $thisPageData->cms_parent_id;
$titleInline    = PB_Helper::generateBreadCrumbName();
$previewLink    ='javascript:void(0);';

if($cboDynamic == 'PDF-DOC' || $cboDynamic == 'STATIC') { 
    $previewLink = $txtLink; 
} else {
    $previewLink = '/pagebase.php?preview=true&amp;pbid=' . $iNum;
}

// dont do template stuff if we don't need to
if ($cboTemplate != 0) {
    $sql	= "SELECT * FROM tbl_cms WHERE cms_id = $iNum";
    $cmsRec	= Database::Execute($sql);
    $cmsRec->MoveNext();

    if ($cmsRec->Count() == 0) {
        throw new exeception("Unable to find record in the database");
    }

    $sql	= "SELECT * FROM tbl_cms_templates WHERE cmst_id = {$cmsRec->cmst_templates_id}";
    $tplRec	= Database::Execute($sql);
    $tplRec->MoveNext();

    if ($tplRec->Count() == 0) {
        throw new exception("Unable to find record in the database");
    }

    $addNew = false;
}

// template stuff end ==============

?> 
<form id="entryForm" method="post" action="process.php" enctype="multipart/form-data">
    <input type="hidden" name="actionRun" value="<?= $isTemplate ? "template" : $view == "system" ? "system" : "modify" ?>" />
    <input type="hidden" name="hidNT" value="<?= $newTemplate ? "1" : "0" ?>" />
    <input type="hidden" name="hidId" value="<?= $hidId; ?>" />
    <input type="hidden" name="hidParentId" id="hidParentId" value="<?= $parentId; ?>">
    <input type="hidden" name="hidTemplateId" id="hidTemplateId" value="<?= $cboTemplate; ?>">
    <input type="hidden" name="cboDynamic" value="<?= $cboDynamic; ?>" />

    <div class="subcontent right last">
    <div class="modify-title clearfix">
        <div class="floatLeft">
            <h2><?= $templateLoad ? "Add" : "Modify" ?> <?= $type ?></h2>
            <?php if(!$templateLoad) { ?>
                <span style="display: inline">Editing <strong><?= $titleInline;?></strong></span>
                <?php } else { ?>
                <span style="display: inline">Template <strong><?= htmlspecialchars($record->cms_title);?></strong></span>
                <?php } ?>
        </div>

        <div class="buttons floatRight clearfix">
            <?php if($cboTemplate !=0 && !$isTemplate && !$templateLoad && $view != "system" && UserManager::hasRole(CONTENT_PUBLISHER_ROLE)) { ?>
                <a id="btnPublish" href="javascript:void(0)" class="publish floatRight green button icon-publish publish">Publish</a>
                <?php } ?>
            <?php if($view == 'modify' || $view == 'template' || $view == "system") { ?>
                <a class="floatRight silver button icon-floppy save" href="javascript:void(0)" id="btnSave">Save</a>
                <?php } else { ?>
                <a href="javascript:void(0)" class="revert floatRight silver button icon-floppy save">Save</a>
                <?php } ?>
            <a href="<?= $previewLink; ?>" target="_blank" class="preview floatRight silver button icon-search">Preview</a>
            <a class="floatRight silver button icon-cog settings" href="javascript:void(0)">Settings</a>
        </div>
    </div>

    <div id="assetListWindow" style="display: none">
        <?= PB_Helper::printAssetList(); ?>
    </div>

    <input name="txtOrder" type="hidden" value="<?= $txtOrder; ?>" />
    <?php if($isHomePage) { ?>
        <input type="hidden" name="txtName" id="txtName" value="<?= $txtName; ?>" />
        <input type="hidden" name="optStatus" id="optStatus" value="1" />
        <input type="hidden" name="parent-category-page" id="parent-category-page" value="0" />
        <?php } ?>

    <div class="settingsContainer clearfix">
        <div class="eightcol global-settings">
            <div id="page-settings" class="page-settings">
                <div class="header"><h3><?= $type ?> Settings</h3></div>
                <ul class="field-list">
                    <li>
                        <label for="txtName" class="required mainlabel">Nav Name</label>
                        <input type="text" name="txtName" id="txtName" value="<?= $txtName; ?>" maxlength="100" required="required" /><?= $form->error("txtName");?>
                        <?php showHelpTip("This name is used in navigation menus and bread crumbs. It won't show up on the page itself."); ?>		
                    </li>

                    <?php if(!$isTemplate) { ?>
                        <li>
                            <label for="txtSubText" class="mainlabel">Sub Text</label>
                            <input type="text" name="txtSubText" id="txtSubText" value="<?= $txtSubText; ?>" maxlength="100" /><?= $form->error("txtSubText");?>
                            <?php showHelpTip("This name is used in the navigation menu. It will show as the title text"); ?>
                        </li>

                        <?php if(isNullOrEmpty($cboDynamic)) { ?>
                            <li>
                                <label for="url_suffix" class="mainlabel">Direct Url</label>
                                <input type="text" value="<?= $url_direct;?>" id="url_direct" name="url_direct" class="url-direct" />
                                <?php showHelpTip("Use this option to create a more user friendly page url, if you leave this blank PageBuilder will auto-create a url for you. "); ?>
                                <div class="clearfix"><label class="mainlabel">Preview Direct Url</label> <?= $_SERVER['ROOT_URI'] ?>/<span id="urlPreview"></span></div>
                            </li>
                            <li>
                                <label class="mainlabel" for="backgroundImage">Background Image</label>
                                <?
                                if ($backgroundImage != '') {
                                    print "<div class='background-image'>\n";
                                    print_r($backgroundImage);
                                    print "</div>\n";
                                }
                                ?>
                                <a class="silver button icon-photo photo" href="javascript:background(<?= $hidId ?>)">Update Background</a>
                                <a class="red button" href="javascript:removeBackground(<?= $hidId ?>)">Delete Background</a>
                                <?
                                if ($backgroundImage && file_exists(filePathCombine($_SERVER['DOCUMENT_ROOT'], '/uploads/homefeatured/', $backgroundImage))){
                                    print '<a class="red button" href="javascript:removeBackground('. $hidId .')">Delete Background</a>';
                                }
                                ?>
                            </li>
                            <li>
                                <label class="mainlabel">Show Bread Crumb</label>
                                <label for="optBreadCrumbY"><input id="optBreadCrumbY" name="optBreadCrumb" type="radio" value="1" <?= $optBreadCrumb == '1' ? 'checked' : ''; ?> /> Yes</label>
                                <label for="optBreadCrumbN"><input id="optBreadCrumbN" name="optBreadCrumb" type="radio" value="0" <?= $optBreadCrumb == '0' ? 'checked' : ''; ?> /> No</label>
                                <?php showHelpTip("Do you want to show the bread crumb on this page?"); ?>
                                <?= $form->error("optBreadCrumb");?>
                            </li>

                            <?php if($_WEBCONFIG['HAS_MEMBER_LOGINS'] == "true") { ?>
                                <li>
                                    <label class="mainlabel">Members Only</label>
                                    <label for="optSecureY"><input id="optSecureY" name="optSecure" type="radio" value="1" <?= $optSecure== '1' ? 'checked' : ''; ?> /> Yes</label>
                                    <label for="optSecureN"><input id="optSecureN" name="optSecure" type="radio" value="0" <?= $optSecure == '0' ? 'checked' : ''; ?> /> No</label>
                                    <?php showHelpTip("Allow only member logins to view this page. "); ?>
                                    <?= $form->error("optSecure");?>
                                </li>
								
								 <?php if(Member::_rolesEnabled()) { ?>
                                    <li id="rolesOptions">
                                        <label class="mainlabel">Roles / Groups Allowed</label>
                                        <?= member::_getPageRoleOptions($hidId); ?>
                                        <? showHelpTip("Select which member role or groups have access to this page. If left blank, all roles can access the page."); ?>
                                    </li>
                                <? } ?>     
                                <?php } 
                        } ?>
                        <li>
                            <label for="optNav" class="mainlabel">Show in Nav</label>
                            <label for="optNav1"><input name="optNav" type="radio" id="optNav1" value="1" <?= $optNav == '1' ? 'checked' : ''; ?> /> Yes</label>  
                            <label for="optNav0"><input name="optNav" type="radio" id="optNav0" value="0" <?= $optNav == '0' ? 'checked' : ''; ?> /> No</label>
                            <?= $form->error("optNav");?>
                            <?php showHelpTip("Should the page appear in the navigation menus?"); ?>
                        </li>
                        <?php } ?>                    

                    <?php if ($cboDynamic =="STATIC") { ?>			
                        <li>
                            <label for="optTarget" class="mainlabel">Window Target</label>
                            <label for="optTarget1"><input name="optTarget" type="radio" id="optTarget1" value="_self" <?= $optTarget == '_self' ? 'checked' : ''; ?> /> Same Window</label>  
                            <label for="optTarget0"><input name="optTarget" type="radio" id="optTarget0" value="_blank" <?= $optTarget == '_blank' ? 'checked' : ''; ?> /> New Window</label>  
                            <?= $form->error("optTarget");?>
                        </li>

                        <?php if($_WEBCONFIG['SITE_TYPE'] == 'BANK') { ?>
                            <li>
                                <label class="mainlabel">Show Disclaimer</label>
                                <label for="optDisclaimer1"><input name="optDisclaimer" type="radio" id="optDisclaimer1" value="1" <?= $optDisclaimer == '1' ? 'checked' : ''; ?> /> Yes</label>  
                                <label for="optDisclaimer0"><input name="optDisclaimer" type="radio" id="optDisclaimer0" value="0" <?= $optDisclaimer == '0' ? 'checked' : ''; ?> /> No</label>  
                                <?= $form->error("optDisclaimer");?>
                                <?php showHelpTip("Show the disclaimer for people leaving the site. "); ?>
                            </li>
                            <?php } 
                    } ?>

                    <?php if ($cboDynamic =="PDF-DOC") { ?>
                        <li>
                            <input type="hidden" name="txtLink" id="txtLink" />
                            <input type="hidden" name="optTarget" id="optTarget" value="_blank" />
                            <strong>List of Documents</strong> &nbsp;&nbsp;&nbsp; <small>Choose One...</small><br />
                            <select id="cboDocuments" size="8" style="width:400px;">
                                <?php
                                $upload_dir	= $_WEBCONFIG['UPLOAD_FOLDER_DOCUMENTS'];
                                if ($handle = opendir(filePathCombine($_SERVER['DOCUMENT_ROOT'], $upload_dir))) {
                                    /* This is the correct way to loop over the directory. */
                                    while (false !== ($file = readdir($handle))) {
                                        if ($file != "." && $file != "..") {
                                            $selected = '';
                                            if (basename($txtLink) == $file) { $selected = ' selected="selected" '; }else{}
                                            printf("<option value=\"%s\" " . $selected . ">%s</option>\n", $file, $file);
                                        }
                                    }
                                    closedir($handle);
                                }
                                ?>
                            </select><br />
                            <a href="javascript:void(0);" class="modalInput green button-slim" rel="#prompt" style="width:390px;text-align:center;">Upload Document</a>
                            <?= $form->error("txtLink");?>
                        </li>
                        <?php } ?>		

                    <?php if ($cboDynamic =="STATIC") { ?>			
                        <li id="trStaticLink">
                            <label for="txtLink" class="required mainlabel">Link</label>
                            <input type="text" name="txtLink" id="txtLink" value="<?= $txtLink; ?>" size="63" maxlength="255" <?php if ($cboDynamic=="") echo 'disabled="disabled"'; ?> /> 
                            <?= $form->error("txtLink");?>            
                        </li>
                        <?php } ?>
                    <?php if($optSystem == '0' || UserManager::isWebmaster()) { ?>
                        <li>
                            <label for="optStatus" class="mainlabel">Status</label>
                            <label for="optAStatus"><input name="optStatus" type="radio" id="optAStatus" value="1" <?= $optStatus == '1' ? 'checked' : ''; ?> /> Active</label>  
                            <label for="optIStatus"> <input name="optStatus" type="radio" id="optIStatus" value="0" <?= $optStatus == '0' ? 'checked' : ''; ?> /> Inactive</label>  
                            <?= $form->error("optStatus");?>
                        </li>
                    <?php } ?>                        
                </ul>
            </div>
            <?php if(UserManager::isWebmaster()) { ?>
                <div id="page-settings" class="page-settings">
                    <div class="header"><h3>Webmaster <?= $type ?> Settings</h3></div>
                    <ul class="field-list">
                        <li>
                            <label for="optSystem" class="mainlabel">System Page</label>
                            <label for="optSystem1"><input name="optSystem" type="radio" id="optSystem1" value="1" <?= $optSystem == '1' ? 'checked' : ''; ?> /> Yes</label>  
                            <label for="optSystem0"><input name="optSystem" type="radio" id="optSystem0" value="0" <?= $optSystem == '0' ? 'checked' : ''; ?> /> No</label>
                            <?= $form->error("optSystem");?>
                            <?php showHelpTip("Should this page only be visable to webmasters? Client will not be able to edit this page."); ?>
                        </li>
                        <li id="systemEditable" class="<?= $optSystem == 1 ? "" : "none" ?>">
                            <label for="optSystemEditable" class="mainlabel">System Page Editable</label>
                            <label for="optSystemEditable1"><input name="optSystemEditable" type="radio" id="optSystemEditable1" value="1" <?= $optSystemEditable == '1' ? 'checked' : ''; ?> /> Yes</label>  
                            <label for="optSystemEditable0"><input name="optSystemEditable" type="radio" id="optSystemEditable0" value="0" <?= $optSystemEditable == '0' ? 'checked' : ''; ?> /> No</label>
                            <?= $form->error("optSystemEditable");?>
                            <?php showHelpTip("Only applies if this is a system page. Client will be able to edit this page but cannot delete it."); ?>
                        </li>                                                
                        <?php if($_WEBCONFIG['ALLOW_SSL_PAGES'] == 'true') { ?>
                            <li>
                                <label for="optSSL" class="mainlabel">Secure Page</label>
                                <label for="optSSL1"><input name="optSSL" type="radio" id="optSSL1" value="1" <?= $optSslPage == '1' ? 'checked' : ''; ?> /> Yes</label>  
                                <label for="optSSL0"><input name="optSSL" type="radio" id="optSSL0" value="0" <?= $optSslPage == '0' ? 'checked' : ''; ?> /> No</label>
                                <?= $form->error("optSslPage");?>
                                <?php showHelpTip("Should this page have an SSL on it."); ?>
                            </li>                                             
                            <?php } ?>
                        <?php if($_WEBCONFIG['ENABLE_CUSTOM_TEMPLATES'] == 'true') { ?>
                        <li>
                            <label for="optTemplate" class="mainlabel">Page Builder Template</label>
                            <label for="optTemplate1"><input name="optTemplate" type="radio" id="optTemplate1" value="1" <?= $optTemplate == '1' ? 'checked' : ''; ?> /> Yes</label>  
                            <label for="optTemplate0"><input name="optTemplate" type="radio" id="optTemplate0" value="0" <?= $optTemplate == '0' ? 'checked' : ''; ?> /> No</label>
                            <?= $form->error("optTemplate");?>
                            <?php showHelpTip("Do you want this page to be a Page Builder template?"); ?>
                        </li> 
                        <?php } ?>                                                               
                    </ul>
                </div>            
                <?php } ?>
            <?php if ($bIsPage) {
                // don't load values if we are adding a new page
                if (!$addNew) {
                    if ($form->getNumErrors() > 0) {
                        $optPublish                  = $form->value("optPublish");
                        $arFields["Title"]           = $form->value("txtName");
                        $arFields["MetaTitle"]       = $form->value("txtMetaTitle"); 
                        $arFields["MetaKeywords"]    = $form->value("txtMetaKeywords");
                        $arFields["MetaDescription"] = $form->value("txtMetaDescription");
                        $arFields["Desc"]            = $form->value("mtxDescription", 'N');
                        $arFields["SidebarContent"]  = $form->value("mtxSidebarContent", 'N'); 
                        $arFields["SecondarySidebarContent"]  = $form->value("mtxSecondarySidebarContent", 'N'); 
                    } else {
                        $optPublish                  = $cmsRec->cms_is_published;
                        $arFields["Title"]           = '';
                        $arFields["MetaTitle"]       = '';
                        $arFields["MetaKeywords"]    = '';
                        $arFields["MetaDescription"] = '';
                        $arFields["Desc"]            = '';
                        $arFields["SidebarContent"]  = '';
                        $arFields["SecondarySidebarContent"]  = '';

                        $sql     = "SELECT * FROM tbl_cms_name_value WHERE cms_id = {$cmsRec->cms_id}";
                        $nvalRec = Database::Execute($sql);

                        while ($nvalRec->MoveNext()) {
                            $arFields[$nvalRec->cmsv_name] = $nvalRec->cmsv_value;
                        }
                    }
                } else {
                    $optPublish                  = '0';     
                    $arFields["Title"]           = '';
                    $arFields["MetaTitle"]       = '';
                    $arFields["MetaKeywords"]    = '';
                    $arFields["MetaDescription"] = '';
                    $arFields["Desc"]            = '';
                    $arFields["SidebarContent"]  = '';
                    $arFields["SecondarySidebarContent"]  = '';
                } 
                ?>

                <div id="seo-settings" class="seo-settings settingsContainer">
                    <div class="header"><h3>SEO Settings</h3></div>

                    <ul class="field-list">
                        <li>
                            <label for="txtMetaTitle" class="mainlabel" style="vertical-align: top;"><b>Meta Title</b><?php showHelpTip("This option displays text in the title bar or tab of your browser. It is also used by search engines to display on search results pages. "); ?></label> 
                            <textarea style="height: 50px; width: 405px;" id="txtMetaTitle" name="txtMetaTitle" cols="60" data-maxchars="65" rows="2"><?= $arFields["MetaTitle"]; ?></textarea>
                            <input type="hidden" name="fieldname_txtMetaTitle" value="MetaTitle" />       
                        </li>
                        <li>
                            <label for="txtMetaTitle" class="mainlabel" style="vertical-align: top;"><b>Meta Keywords</b><?php showHelpTip("Provide relavent keywords from your page which can be used by search engines, seperate keywords and phrases using a comma. Try to keep your list of keywords or phrases to about 5 - 10 for best results. <br /><br /><em>Ex: forbin, web design, hosting, seo </em>"); ?></label>
                            <textarea style="height: 50px; width: 405px;" id="txtMetaKeywords" name="txtMetaKeywords" data-maxchars="155" cols="60" rows="2"><?= $arFields["MetaKeywords"]; ?></textarea>        
                            <input type="hidden" name="fieldname_txtMetaKeywords" value="MetaKeywords" />       
                        </li>
                        <li>
                            <label for="txtMetaTitle" class="mainlabel" style="vertical-align: top;"><b>Meta Description</b><?php showHelpTip("Provide a brief description of your page which can be used by search engines for ranking your site. "); ?></label>
                            <textarea style="height: 50px; width: 405px;" id="txtMetaDescription" name="txtMetaDescription" data-maxchars="155" cols="60" rows="2"><?= $arFields["MetaDescription"]; ?></textarea>        
                            <input type="hidden" name="fieldname_txtMetaDescription" value="MetaDescription" />       
                        </li>   
                    </ul>
                </div>
                <?php if(isNullOrEmpty($cboDynamic)) { ?>
                    <div class="revision-history sc-right-column sc-right-column-low">    
                        <div class="header"><h3>Revision History</h3></div>
                        <div class="revision-history-scroll">
                            <ul class="revision-history">
                                <li><span class="date"><b>Date</b></span><span class="user"><b>User</b></span><span style="float: right; margin-right: 90px;"><b>Options</b></span></li>
                                <?= PB_Revisions::makeRevisionList( $iNum ); ?>
                            </ul>
                        </div>    
                    </div>
                    <?php } ?>                
                <?php } ?>
        </div><!-- eof eightcol -->

        <div class="fourcol last sc-right-column">
            <?php if(!$isHomePage && !$isTemplate) { ?>
                <!-- category / parent page container -->
                <div class="parent-pages">	
                    <div class="header"><h3>Parent</h3></div>	
                    <?= PB_Helper::cmsPageRadioList($_WEBCONFIG['SITE_ROOT_PAGE'], $iNum,   PB_Revisions::isRevision($hidId) !== false ); ?>
                </div>
                <?php } ?>
            <div class="layout-choice global-settings">
                <div class="header"><h3>Layout Options</h3></div> 
                <?= PB_Templates::makeLayoutList($cboTemplate); ?>  
            </div>
        </div><!-- eof fourcol -->

        <?php  
        // close the form if we have no template
        if ($cboTemplate == 0) {  
            ?>
        </div><!--End content-->
    </form>
    <?php 
} else {  
    $addNew = false; ?>

    </div><!-- eof settings-hide -->

    <div class="clearfix">
        <?php printf("<!--<h2>Template ID: %s</h2>-->\n", ($cmsRec->cms_id > 0 ? $cmsRec->cms_id : 'Not Specified')); ?>
        <?php include_once (filePathCombine('layouts', $tplRec->cmst_name)); ?> 
        <p style="text-align: center; font-size:11px">
            Last Modified on <?= date("m/d/Y g:i:s A", strtotime($record->cms_published_date)) ?>
        </p>
    </div>

    <div class="buttons clearfix">
        <?php if ($isTemplate) { ?>
            <a class="floatLeft button silver" href="templates/index.php">Back to Templates</a>
            <?php } else if($_GET['view'] == "system" ) { ?>
            <a class="floatLeft button silver" href="system/pages/index.php">Back to System Pages</a>
            <?php } else { ?>
            <a class="floatLeft button silver" href="index.php">Back to Pages</a>
            <?php } ?>
    </div>

    </div><!--End content-->
    </form>

<?php } ?>

<!-- user input dialog -->
<div id="prompt" class="modal">
    <h2>Upload Document</h2>
    <p>Upload a DOC or PDF document.</p>

    <form id="uploadForm" enctype="multipart/form-data">
        <label>Document:</label><br />
        <input type="file" name="fleDocument" id="fleDocument" /><br />
        <button type="submit" class="submit"> OK </button>
        <button type="button" class="close"> Cancel </button>
    </form>
</div> 

<script type="text/javascript">
    $('#url_direct').keyup(function() { $(this).val($(this).val().replace(/[^A-Za-z0-9\/\-_\s]/g, '').replace(/[_\s]/g, '-')); });
    $('#url_direct').on('keyup keypress blur change click load', function() { $('#urlPreview').text($('#url_direct').val()); });
    $('#url_direct').trigger("load"); 

    $(document).ready(function () {
        $('*[data-maxchars]').on("focus keyup load paste", function () {
            var max = parseInt($(this).data('maxchars'));
            var messageId = $(this).attr("id") + "Message";
            if ($(this).val().length > max) {
                $(this).val($(this).val().substr(0, $(this).data('maxchars')));
            }
            if ($('#' + messageId).length == 0) {
                $(this).after("<div id='" + messageId + "' class='maxDisplay' style='text-align: center;'></div>");
            }
            $('#' + messageId).html((max - $(this).val().length) + ' Characters Remaining');
        });
        $('*[data-maxchars]').trigger("load");   
        
        $("input[name=optSystem]").change(function() {
            if($(this).val() == '1') {
                $("#systemEditable").slideDown();    
            }
            else {
                $("#systemEditable").slideUp();    
            }
        })                                                         
    });
    function background(Id) {
        window.location.href = 'background/index.php?view=background&id=' + Id;
    }
    function featured(Id) {
        window.location.href = 'index.php?view=featured&id=' + Id;
    }
    function removeFeatured(Id) {
        if(confirm('This will remove the Featured Image, are you sure?')){
            $.post(
                '/vpanel/pagebuilder/process.php', {
                    actionRun: 'removeFeatured',
                    id: Id
                },
                function(data){
                    location.reload();
                }
            )
        }
    }
    function removeBackground(Id) {
        if(confirm('This will remove the Background Image, are you sure?')){
            $.post(
                '/vpanel/pagebuilder/background/process.php', {
                    actionRun: 'removeBackground',
                    id: Id
                },
                function(data){
                    location.reload();
                }
            )
        }
    }
</script>