<?php
header('Access-Control-Allow-Origin: https://services.forbin.com'); 
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Content-Type');

require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 

security::_secureCheck();

$filename = $_GET['filename']; 
copy('https://services.forbin.com/utility/ImageResizer/' . $filename, $_SERVER['DOCUMENT_ROOT'] . '/uploads/userfiles/files/' . $filename);
Security::_addAuditEvent("ImageBuilder Image Saved", "Success",  $filename, "");
die("File Moved Successfully"); 
?>