<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-upload-cloud"><?= $_WEBCONFIG['COMPONET_NAME'] ?></h1>
        <p class="page-desc">Upload images to your website.</p>
    </div>
</section>

<section class="maincontent">

	<div class="container clearfix">
     
         <?php require_once '../navBar.php'; ?>
    
        <div class="subcontent right last">
	        <h2>Upload your files</h2>
            <iframe name="UploaderFrame" src="/vpanel/ckfinder/ckfinder.html" width="100%" height="450"></iframe>
        
        <div class="buttons clearfix">
            <a href="javascript: history.back()" class="floatLeft silver button">Back</a>
        </div>
    </div>
</div>
</section>