<?php
require_once 'config.php'; 

include_once filePathCombine($_SERVER['DOCUMENT_ROOT'] , $_WEBCONFIG['VPANEL_PATH'] , "includes/inc_pgtop.php");
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'] , $_WEBCONFIG['VPANEL_PATH'] , "includes/inc_header.php");
?>
<section class="title">
    <div class="container clearfix">
        <h1 class="page-title icon-brush">Image Builder</h1>
        <p class="page-desc">Image Editing Tool</p>
    </div>
</section>

<section class="maincontent">

	<div class="container clearfix">
     
         <?php require_once '../navBar.php'; ?>
    
        <div class="subcontent right last">
            <iframe name="UploaderFrame" src="//services.forbin.com/utility/ImageResizer/" width="100%" height="665"></iframe>
        
        <div class="buttons clearfix">
            <a href="javascript: history.back()" class="floatLeft silver button">Back</a>
        </div>
    </div>
</div>
</section>
<?php
include_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "includes/inc_pgbot.php");
?>