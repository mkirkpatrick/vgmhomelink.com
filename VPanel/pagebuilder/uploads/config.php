<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_visitor.php"); 

security::_secureCheck();

if (!isset($_GET['logout'])) {
	$_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}
	
if (file_exists('web.config')) {
    $configXML = simplexml_load_file('web.config');
	foreach ($configXML->appSettings->add as $setting) {
		$_WEBCONFIG[(string)$setting->attributes()->key] = (string)$setting->attributes()->value; 
	}
}
?>