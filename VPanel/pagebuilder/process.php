<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_vMail.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "pagebuilder/classes/class_revisions.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "pagebuilder/classes/class_templates.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "pagebuilder/classes/class_helper.php");
require_once "class.php";

security::_secureCheck();

$p = new process('tbl_cms', 'tbl_cms_name_value');
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {
		
	case 'parent': // switch parent page
		$p->_parent();
		jsonResponse('Category Updated');
		exit();
		break;

	case 'add':	
		$id = $p->_add();		
		$_POST['hidId'] = $id;	
		if(isset($_POST['txtNameTitle'])) {	// Check to see if were are saving a page, if so NameTitle will exist
			$p->_saveTemplateNV();
		}
        
        if($_POST['addWhat'] == 'template') {
            jsonResponse('#TEMPLATE-REDIRECT'.$id);    
        }
        else {
            jsonResponse('#EDIT-REDIRECT'.$id);
        }
			
		break;
		
	case 'modify' :		
		$p->_modify();
		if(isset($_POST['txtNameTitle'])) { 
			echo 'Page Draft Saved! Click publish when you are ready to make these changes live!';
		} else { 
			echo 'All Changes Saved!';
		}
		break;   

	case 'template' :
		if($_POST['hidNT'] == "1") { 
			$id = $p->_saveNewTemplate(); 	
			$_POST['hidId'] = $id;	
			$p->_saveTemplateNV();
		} else {
			$p->_publish();			
			$p->_saveTemplateNV();
		}
		echo 'Template Changes Saved&nbsp;&nbsp;&nbsp;&nbsp;';
		break;  	

	case 'system' :
		$p->_publish();			
		$p->_saveTemplateNV();
		echo 'Page Changes Saved&nbsp;&nbsp;&nbsp;&nbsp;';
		break;  		

	case 'publish' :							
		$p->_saveTemplateNV();
		$status = $p->_publish();
        if($status == 'MAX') {
            // Save changes so user doesn't lose page edits
            $p->_modify();
            echo '#ERROR#You already have ' . $_WEBCONFIG['PAGEBUILDER_PAGE_LIMIT'] . ' active pages. Please deactivate one first.<br>Your current page changes have been saved.';    
        }
        else {
            echo 'All Changes Published&nbsp;&nbsp;&nbsp;&nbsp;';    
        }
		break;  		
		
	case 'sort' :
		$p->_sort();
		break;
		
	case 'CSS' :
		$p->_css();
		break;
		
	case 'status' :
		$p->_status();
		break;
		
	case 'revert' :
		$p->_revert();
		break;
		
	case 'compliance' :
		$p->_compliance();
		break;

	case 'delete' :
		$p->_delete();
		jsonResponse('Deleted Successfully');
		break;
}

function jsonResponse($successMsg){
	$lf = $GLOBALS['form'];
	$errors = $lf->getErrorArray();
	if(sizeof($errors) > 0){ 
		foreach($errors as $k=>$v){ die('#ERROR#'.strip_tags($v));}
	} else {
		die($successMsg);
	}
}
?>