<div id="content-anchor" class="maincontent container clearfix">
    <aside id="sidebar-left" class="threecol left sidebar">
        #SidebarContent#
    </aside>

    <div class="main sixcol">
        #Desc#
    </div>

    <aside id="sidebar-right" class="threecol last right sidebar">
        #SecondarySidebarContent#
    </aside>
</div>