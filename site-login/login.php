<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/config.php');
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php");
?>
<!DOCTYPE html>
<!--[if IE 8]><html xmlns="http://www.w3.org/1999/xhtml" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!--><html xmlns="http://www.w3.org/1999/xhtml" class="no-js"><!--<![endif]-->
    <head>
        <title>Staging Site Login | <?= $siteConfig['Company_Name'] ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="Viewport" content="width=device-width, minimum-scale=1" />
        <meta name="Keywords" content="login">
        <meta name="Description" content="Staging Site Login.">
        <meta property="og:title" content="Staging Site Login">
        <meta property="og:description" content="Site Login.">
        <meta property="og:url" content="">
        <meta property="og:image" content="<?= $_WEBCONFIG['SITE_URL'] ?>images/logo.png">
        <meta property="og:type" content="website">
        <meta name="Author" content="<?= $siteConfig['Company_Name'] ?>" />
        <meta name="Generator" content="Forbin">

        <link rel="canonical" href="<?= urlPathCombine($_WEBCONFIG['SITE_URL'], $_SERVER['REQUEST_URI']) ?>" />
        <link rel="stylesheet" href="<?php autoCreateVerison('/site-login/css/login.css'); ?>" />
        <link type="text/css" rel="stylesheet" href="/css/parsley.css"/>
    </head>
    <body class="login">
        <div class="table">
            <div class="cell">
                <div class="form-box">
                    <header>
                        <img src="/images/forbin-logo.png" alt="">
                    </header>

                    <main class="content">
                        <?php
                        if ($form->getNumErrors() > 0) {
                            $username = $form->value("txtUsername");
                            $password = "";
                        } else {
                            $username = "";
                            $password = "";
                        }

                        if ($form->getNumErrors() > 0) {
                            $errors    = $form->getErrorArray();
                            foreach ($errors as $err) echo $err;
                        }
                        ?>
                        <form method="post" enctype="multipart/form-data" id="loginForm"  name="loginForm" autocomplete="off">
                            <input id="hidFormName" name="hidFormName" type="hidden" value="Quick Form">
                            <input id="actionRun" name="actionRun" type="hidden" value="login">
                            <?= loadRC() ?>
                            <ul>
                                <li>
                                    <label class="required" for="txtUsername">Username</label>
                                    <input type="text" class="text" id="txtUsername" name="txtUsername" value="<?= $username ?>" required="required" title="Site Username" data-parsley-required-message="Enter A Username.">
                                </li>
                                <li>
                                    <label class="required" for="txtPassword">Password</label>
                                    <input type="password" class="text" id="txtPassword" name="txtPassword" value="<?= $password ?>" required="required" title="Site Password" data-parsley-required-message="Enter A Password.">
                                </li>
                                <li>
                                    <input id="btnSubmit" type="submit" value="Login">
                                </li>
                            </ul>
                        </form>
                    </main><!-- eof content -->

                    <footer class="footer">
                        <p>&#169; <?= date("Y") ?>. All Rights Reserved.<br />Site Created and Powered by <a href="http://www.forbin.com" target="_blank" title="VGM Forbin">VGM Forbin</a></p>
                    </footer><!-- eof footer -->
                </div>
            </div>
        </div><!-- eof table -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha384-ZvpUoO/+PpLXR1lu4jmpXWu80pZlYUAfxl5NsBMWOEPSjUn/6Z/hRTt8+pR6L4N2" crossorigin="anonymous"></script>        <script type="text/javascript" src="/scripts/parsley.min.js"></script>
        <script type="text/javascript">
            $(function(){
                $("input[type='submit']").click(function (event) {
                    event.preventDefault();
                    var myform = '#' + $(this).closest("form").attr('id');
                    var buttonVal = $(this).val();
                    $(this).val('VALIDATING ....');
                    $(this).attr('disabled', 'disabled');
                    $(myform).parsley({excluded: "input[type=button], input[type=submit], input[type=reset], input[type=hidden], input:hidden, textarea[type=hidden], textarea:hidden"});
                    if($(myform).parsley().isValid()) {
                        $(myform).attr("action", "/site-login/process.php");
                        $(myform).submit();
                    }
                    else {
                        $(this).val(buttonVal);
                        $(this).removeAttr('disabled');
                        $(myform).parsley().validate();
                    }

                });
            })
        </script>
    </body>
</html>
