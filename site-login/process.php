<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php"); 

class Process extends Database { 

    private $credentials;

    //-------------------------------
    public function __construct() {
    //-------------------------------
        global $_WEBCONFIG;
        parent::__construct();
    }

    //--------------------------
    public function _login() {
    //--------------------------
        global $_WEBCONFIG, $form, $siteConfig;

        $this->credentials = new stdClass();
        
        self::_verify();

        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            $this->params = $_SERVER['HTTP_REFERER'];
        } else {
            // No Errors
            $valid = Authentication::login_user($this->credentials->username, $this->credentials->password);
            if($valid === false) {
               $form->setError("Invalid Credentials", "Invalid Username or Password provided."); 
               $_SESSION['value_array'] = $_POST;
               $_SESSION['error_array'] = $form->getErrorArray();
            }
            $this->params = $_SERVER['HTTP_REFERER'];
        }
    }

    //----------------------------
    private function _verify() {
    //----------------------------
        global $_WEBCONFIG, $form;

        if(!validateRC()) {
            $form->setError("Invalid Submission", "Invalid Submission. Please Try Again.");    
        }

        $fieldName  = "txtUsername";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Username is required.");
        } else {
            $this->credentials->username = $fieldValue;
        }  

        $fieldName  = "txtPassword";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "* Password is required.");
        } else {
            $this->credentials->password = $fieldValue;
        }
    }

};

$p = new Process();
$action = isset($_POST['actionRun']) ? $_POST['actionRun'] : '';

switch ($action) {
    case 'login' :
        $p->_login();
        break;

    default :
        redirect($_SERVER['HTTP_REFERER']);
}

redirect($_SERVER['HTTP_REFERER']);