<?php
namespace Forbin\Handlers;

use \LogicException;

class AutoLoadException extends LogicException {}

$loader = new AutoLoadHandler('Forbin', $_SERVER['DOCUMENT_ROOT']);
$loader->register();

class AutoLoadHandler {

    private $prefix;
    private $baseDir;

    public function __construct($prefix, $baseDir)
    {
        $this->prefix = $prefix . '\\';
        $this->baseDir = $baseDir . '\\';
    }

    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    public function loadClass($class)
    {
        // project-specific namespace prefix
        $len = strlen($this->prefix);
        if (strncmp($this->prefix, $class, $len) !== 0) {
            // Namespace does not include Forbin prefix and should not be attempted to autoload
            return;
        }
        $baseClass = $this->decamelize(substr($class, strrpos($class, '\\') + 1));

        // get the relative class name
        $relative_class = substr($class, $len);
        $relative_class = substr($relative_class, 0, strlen($relative_class) - strlen($baseClass));
        $file = strtolower(filePathCombine($this->baseDir . $relative_class,  'class_' . $baseClass . '.php'));
        $file = str_replace('\\', '/', $file);
        $this->loadClassFile($file, $class);
    }

    /**
     * If a file exists, require it from the file system.
     */
    protected function loadClassFile($file, $class)
    {
        global $_WEBCONFIG;
        if (file_exists($file)) {
            require_once $file;
            return true;
        } else {
            return false;
        }
    }

    protected function decamelize($word) {
        return strtolower(preg_replace(["/([A-Z]+)/", "/_([A-Z]+)([A-Z][a-z])/"], ["_$1", "_$1_$2"], lcfirst($word)));
    }
}
?>