<?php
namespace Forbin\Handlers;

use \Database; 

require_once($_SERVER['DOCUMENT_ROOT'] . '/library/config.php'); 

//events calendar addition
$pbId = getCmscIdbyUrl($_SERVER['REQUEST_PATH']);
$preview = isset($_GET['pbid']) ? (int)$_GET['pbid'] : 0;

// Make sure user has loged in

// GET PAGEBUILDER REDIRECT RULES
if($_WEBCONFIG['SITE_TYPE'] != 'LANDING-PAGE'){
    $rule = getRule(rtrim($_SERVER['REQUEST_URI'], "/"));
}

// CHECK TO SEE IF THERE IS REDIRECT OR REWRITE RULE
if(isset($rule) && !is_null($rule)) {
    if($rule->cr_type == "Redirect") {
        redirect($rule->cr_url);
    }
    else {
        if(strpos ($rule->cr_url,"." ) === FALSE ) {
            $url = rtrim($rule->cr_url, '/\\') . '/index.php';
        } else {
            $url = $rule->cr_url;
        }
        if(stream_resolve_include_path($url) !== false) {
            serverTransfer($url);
        } else {
            serverTransfer($_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['404']); // Display 404 Error Page
        }
    }
} // END OF REDIRECT/REWRITE RULES

// CHECK TO SEE IF ITS AN ECOMMERCE SITE AND IF IS A CATALOG URL
if($_WEBCONFIG['SITE_TYPE'] == 'ECOMMERCE' || $_WEBCONFIG['SITE_TYPE'] == 'CATALOG') {
    $PermaLink = isset($_SERVER['REQUEST_PATH']) ? $_SERVER['REQUEST_PATH'] : 0;
    $onlineStoreUrl = getByPermaLink($PermaLink);
    if($onlineStoreUrl != "") {
        serverTransfer($onlineStoreUrl);
    }
}

// CHECK TO SEE IF THE SITE HAS VPRESS BLOG AND URL IS A BLOG URL
if($_WEBCONFIG['HAS_VPRESS_BLOG'] == 'true' && isBlogUrl()) {
    $parts = explode("/", $_SERVER['REQUEST_PATH']);
    if(stripos($_SERVER['REQUEST_URI'],'archive') !== false) {
        if(count($parts) == 4) {
            $_GET['y'] = $parts[3];
            $_GET['slug'] = $parts[3];
        } else if(count($parts) == 5) {
            $_GET['y'] = $parts[3];
            $_GET['m'] = $parts[4];
            $_GET['slug'] = $parts[3] . "/" . $parts[4];
        } else if(count($parts) == 6) {
            $_GET['y'] = $parts[3];
            $_GET['m'] = $parts[4];
            $_GET['d'] = $parts[5];
            $_GET['slug'] = $parts[3] . "/" . $parts[4] . "/" . $parts[5];
        }
        $_SERVER['REQUEST_PATH'] = $parts[1] . '/' . $parts[2];
    }
    else if(count($parts) > 2) {
        $_GET['slug'] = $parts[count($parts) - 1];
        $_SERVER['REQUEST_PATH'] = $parts[1] . '/' . $parts[2];

        // Check For Post 404
        if(stripos($_SERVER['REQUEST_URI'],'post') !== false && !isset($_GET['preview'])) {
            $slug = Database::quote_smart($_GET['slug']);
            $sql = "SELECT bp_id
            FROM tbl_blog_post
            WHERE bp_active = 'Active' AND bp_status = 'Published' AND bp_slug = '$slug'
            LIMIT 1";
            $record    = Database::Execute($sql);
            $index = 1;
            if ($record->Count() == 0) {
                serverTransfer($_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['404']); // Display 404 Error Page
            }
        }

        // Check For Category 404
        if(stripos($_SERVER['REQUEST_URI'],'category') !== false) {
            $slug = Database::quote_smart($_GET['slug']);
            $sql = "SELECT *
            FROM `tbl_blog_category`
            WHERE bc_slug = '$slug'
            LIMIT 1;";
            $tag = Database::Execute($sql);
            if($tag->Count() == 0) {
                serverTransfer($_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['404']); // Display 404 Error Page
            }
        }

        // Check For Tag 404
        if(stripos($_SERVER['REQUEST_URI'],'/tag') !== false) {
            $slug = Database::quote_smart($_GET['slug']);
            $sql = "SELECT *
            FROM `tbl_blog_tag`
            WHERE bt_slug = '$slug'
            LIMIT 1;";
            $tag = Database::Execute($sql);
            if($tag->Count() == 0) {
                serverTransfer($_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['404']); // Display 404 Error Page
            }
        }

    } else {
        serverTransfer($_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['404']); // Display 404 Error Page
    }
} // END OF VPRESS BLOG


$pbId    = getCmscIdbyUrl($_SERVER['REQUEST_PATH']);
$preview = isset($_GET['pbid']) ? (int)$_GET['pbid'] : 0;

if(is_null($pbId) && $preview == 0) {
    serverTransfer($_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['404']); // Display 404 Error Page
} else if(!is_null($pbId)) {
    serverTransfer('/pagebase.php?pbid=' . $pbId);
} else if(!is_null($preview)){
    serverTransfer('/pagebase.php?pbid=' . $pbId);
} else {
    serverTransfer($_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['404']); // Display 404 Error Page
}



/***************************************************************************************************
REQUEST HANDLER FUNCTIONS
****************************************************************************************************/

//-----------------------
function getRule($url) {
    //-----------------------
    $url = strtok($url, '?');
    $url = html_entity_decode(trim($url));
    $sql = "SELECT * FROM `tbl_cms_rules` WHERE cr_match = '$url' LIMIT 1";
    $record = Database::Execute(trim($sql));
    if($record->Count() > 0) {
        $record->MoveNext();
        return $record;
    } else {
        return null;
    }
}

//------------------------------
function getCmscIdbyUrl($url) {
    //------------------------------
    $cmsId = null;
    $url = trim($url, "/");
    $sql = "SELECT cms_id FROM tbl_cms
    WHERE TRIM('/' FROM url_direct) = '$url' AND cms_is_historic = 0 AND cms_is_deleted = 0 AND cms_is_active = 1 AND cms_is_template = 0
    LIMIT 1";
    $record = Database::Execute($sql);

    if ($record->Count() > 0) {
        $parts = explode("/", $url);
        $_SERVER['REQUEST_SCRIPT'] = '/' . htmlEntities($parts[count($parts) - 1], ENT_QUOTES, "utf-8");
        $record->MoveNext();
        $cmsId = (int)$record->cms_id;
    } else {
        if(strpos($url,"events/event-details/") !== false){
            $parts = explode("/", $url);
            $directUrl = ""; 
            if(isset($parts[2]) && is_numeric($parts[2])){
                $sql = "SELECT ev_name
                FROM tbl_events
                WHERE ev_id = {$parts[2]}
                LIMIT 1";
                $event = Database::Execute($sql);
                if($event->Count() > 0){
                    $event->MoveNext();
                    if(isset($parts[3]) && $parts[3] == format_uri($event->ev_name)){
                        $sql = "SELECT cc.cmsc_id
                        FROM tbl_cms_content AS cc
                        INNER JOIN tbl_cms AS cms ON cms.cms_id = cc.cms_id
                        WHERE TRIM('/' FROM url_direct) = 'events/event-details' AND cms_is_historic = 0 AND cms_is_deleted = 0 AND cms_is_active = 1
                        AND cms_is_template = 0
                        LIMIT 1";
                        $record = Database::Execute($sql);
                        if ($record->Count() > 0) {
                            $record->MoveNext();
                            $cmscId         = (int) $record->cmsc_id;
                            $_GET['id']     = $parts[2];
                            $_REQUEST['id'] = $parts[2];
                        } 
                    }  
                }
            }
        }
    }

    return $cmsId;
}

//---------------------
function isBlogUrl() {
    //---------------------
    if(stripos($_SERVER['REQUEST_URI'],'blog') !== false && ((stripos($_SERVER['REQUEST_URI'],'category') !== false) || (stripos($_SERVER['REQUEST_URI'],'archive') !== false) || (stripos($_SERVER['REQUEST_URI'],'post') !== false) || (stripos($_SERVER['REQUEST_URI'],'tag') !== false))) {
        return true;
    } else {
        return false;
    }
}

//------------------------------------
function getByPermaLink($permaLink) {
    //------------------------------------
    global $_WEBCONFIG;
    // Remove the ending '/' in the url
    $permaLink = rtrim($permaLink, "/");
    $permaLink = strtolower($permaLink);
    $url = $permaLink;
    $keys = parse_url($url); // parse the url
    $path = explode("/", $keys['path']); // splitting the path
    $last = end($path); // get the value of the last element
    $prev = prev($path); // get the next to last element
    $prevPrev = isset($path[1]) ? $path[1] : "";

    $prev = Database::quote_smart($prev);
    $last = Database::quote_smart($last);

    if(strlen($prev) > 0 && $prev == 'product') {
        $sql = "SELECT *
        FROM tbl_ecommerce_products
        WHERE ep_permalink = '$last' AND ep_status = 'Available' LIMIT 1";
        $record = Database::Execute($sql);

        if ($record->Count() > 0) {
            $record->MoveNext();
            $productID = $record->ep_id;
            $url = "/{$_WEBCONFIG['Catalog_Directory']}/product.php?prodId=$productID";
        } else {
            $url = "/{$_WEBCONFIG['Catalog_Directory']}/product-404error.php";
        }
    } elseif(strlen($prev) > 0 && $prev == "{$_WEBCONFIG['Brand_Slug']}"){
        $sql = "SELECT *
        FROM tbl_ecommerce_manufacturers
        WHERE em_permalink = '$last' AND em_status = 'Active' 
        LIMIT 1";
        $record = Database::Execute($sql);

        if ($record->Count() > 0) {
            $record->MoveNext();
            $brandId = $record->em_id;
            $url = "/{$_WEBCONFIG['Catalog_Directory']}/brand.php?emId=$brandId";
        }
        else {
            $url = $_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['404'];
        }
    } elseif(strlen($prev) > 0 && $prev == "checkout" && $prevPrev == "{$_WEBCONFIG['Catalog_Slug']}") {
        if($last == "{$_WEBCONFIG['Checkout_Shipping_Slug']}") {
            $url = "/{$_WEBCONFIG['Catalog_Directory']}/checkout/shipping-method.php";    
        } elseif($last == "{$_WEBCONFIG['Checkout_Payment_Slug']}") {
            $url = "/{$_WEBCONFIG['Catalog_Directory']}/checkout/payment-method.php";           
        } elseif($last == "{$_WEBCONFIG['Checkout_Summary_Slug']}") {
            $url = "/{$_WEBCONFIG['Catalog_Directory']}/checkout/order-summary.php"; 
        } elseif(trim($last,"/") == "success") {
            $url = "/{$_WEBCONFIG['Catalog_Directory']}/checkout/success.php";          
        } else {
            // Send to 404
            $url = $_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['404'];
        }  
    } elseif(strlen($prev) > 0 && trim($last,"/") == "checkout" && trim($prev,"/") == "{$_WEBCONFIG['Catalog_Slug']}"){
        if(isset($_SESSION['member_id'])) {
            // Send Logged in user directly to Shipping Page
            $url = "/{$_WEBCONFIG['Catalog_Directory']}/checkout/shipping-method.php";
        } else {
            $url = "/{$_WEBCONFIG['Catalog_Directory']}/checkout/checkout.php";    
        }        
    } elseif(strlen($prev) > 0 && trim($last,"/") == "reset-password" && trim($prev,"/") == "{$_WEBCONFIG['Catalog_Slug']}"){
        $url = "/{$_WEBCONFIG['Catalog_Directory']}/reset-password.php";          
    } else {
        $sql = "SELECT *
        FROM tbl_ecommerce_cats
        WHERE cat_permalink = '$last' 
        LIMIT 1";
        $record2 = Database::Execute($sql);
        if($record2->Count() > 0) {
            $record2->MoveNext();
            $categoryID = $record2->cat_id;
            if($record2->cat_parent_id != 0) {
                if(empty($prev)) {
                    $url = "/{$_WEBCONFIG['Catalog_Directory']}/category-404error.php";
                }
                else {
                    $url = "/{$_WEBCONFIG['Catalog_Directory']}/categories.php?parentId=$record2->cat_parent_id&catId=$categoryID";
                }
            }
            else {
                $url = "/{$_WEBCONFIG['Catalog_Directory']}/categories.php?catId=$categoryID";
            }
        }
        else {
            $url = "";
        }
    }
    return $url;
}
?>