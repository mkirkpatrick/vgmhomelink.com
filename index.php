<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");

// Make sure user has loged in
//HttpAuthentication::authenticate_digest();

$iNum = $_WEBCONFIG['SITE_ROOT_PAGE'];
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/page_core.php");
include_once('includes/inc-pgtop.php');
include_once('includes/inc-header.php');
include_once('includes/inc-nav.php'); ?>

<?php if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/modules/marketing/marketing-msg.php')) {
	include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/marketing/marketing-msg.php');
} ?>
<main class="content" id="skip-navigation">
    <div class="clearfix">
        <?php $layout->publish(); ?>
	</div><!-- eof container -->

	<?php include_once('includes/inc-assistance.php'); ?>
</main><!-- eof content -->

<script>
$(document).ready(function() {
	"use strict";

	$('.cls-3, .cls-12').click(function () {
		$('.triangle-tooltip').fadeOut();
		$('.service-tooltip').fadeToggle();
	});

	$('.cls-7, .cls-15').click(function () {
		$('.triangle-tooltip').fadeOut();
		$('.network-tooltip').fadeToggle();
	});

	$('.cls-8, .cls-16').click(function () {
		$('.triangle-tooltip').fadeOut();
		$('.process-tooltip').fadeToggle();
	});

	$('.cls-9, .cls-13').click(function () {
		$('.triangle-tooltip').fadeOut();
		$('.software-tooltip').fadeToggle();
	});

	$(document).mouseup(function (e) {
    	var container = $(".triangle svg");

    	if (!container.is(e.target) && container.has(e.target).length === 0) {
        	$('.triangle-tooltip').fadeOut();
    	}
	});
});
</script>

<?php include_once('includes/inc-copyright.php'); ?>
<?php include_once('includes/inc-pgbot.php'); ?>