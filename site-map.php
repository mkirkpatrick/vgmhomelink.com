<?php
global $siteConfig, $_WEBCONFIG;

if (!isset($siteConfig)) {
    header("Location: /site-map");
    exit;
} 

echo cmsTreeTableView($_WEBCONFIG['SITE_ROOT_PAGE']);

//----------------------------------
function cmsTreeTableView($index) { 
    //----------------------------------
    global $_WEBCONFIG;
    $sMarkup = '';

    $s = "SELECT * FROM tbl_cms 
          WHERE cms_is_historic = 0 and cms_is_deleted = 0 and cms_is_active = '1' AND cms_show_nav = '1' and cms_is_template = 0 AND cms_parent_id = $index 
          ORDER BY cms_parent_id, cms_display_order, cms_id, cms_title
          LIMIT 1000";         
    $r = Database::Execute($s);

    if ($r->Count() == 0) return;

    if ($index == $_WEBCONFIG['SITE_ROOT_PAGE']) {            
        // home page markup
        $sMarkup .=  '<ul class="primaryNav"><li id="home"><a href="/">Home Page</a></li>'. "\n";
    } else {
        $sMarkup .= "<ul>\n";
    }

    while ($r->MoveNext()) {
        $link    =  $r->cms_dynamic_link;
        if ($r->cms_dynamic_link == '') $link = '/cms/' . $r->cms_id . '/'.preg_replace('/[^a-z0-9]/i','-',$r->cms_title);

        if (strtolower($r->cms_title) != 'insurance') {
            $sMarkup .= "<li>" .'<a href="'.$link.'">'. $r->cms_title .'</a>' . cmsTreeTableView($r->cms_id) ."</li>\n";
        }
    }

    return $sMarkup .'</ul>';
}
?>