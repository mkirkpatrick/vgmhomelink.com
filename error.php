<?php 
$title = "Page Error"; 
include_once('includes/inc-pgtop.php');
include_once('includes/inc-header.php');
include_once('includes/inc-nav.php'); ?>


<main class="content subcontent" id="skip-navigation">
   	<div class="container clearfix">           
        <div class="maincontent">
			<h2>You have experienced an unexpected error.</h2>
            <p>Please try your action again later. We apologize for the inconvenience. This error has been logged and we will look into it right away. 
            If this problem persists please contact us. Please use your browser's back button or <a href="javascript:history.back()">click here</a> to refresh this page and try again.
            
            If you are currently logged in you may need to <a href="/members/index.php?logout">logout</a> and try again. 
            </p>

            <p><?= $siteConfig['Company_Name'] ?><br />
                Phone: <?= $siteConfig['Contact_Phone'] ?> <br />
                Email: <a href="mailto:' <?= $siteConfig['Contact_Email'] ?> '"><?= $siteConfig['Contact_Email'] ?></a></p>
        </div><!--eof ninecol-->
	</div>
</main>
<?php include_once('includes/inc-copyright.php'); ?>  
<?php include_once('includes/inc-pgbot.php'); ?>