<div align="center">
    <h3>Would you like to continue?</h3>
    <?php 
    if (!isset($_GET['url']) && !isset($_GET['email'])) { 
        redirect("/"); 
    }
    if (isset($_GET['url'])){
        $query = str_replace('url=', '', $_SERVER['QUERY_STRING']);
        $query = str_replace('&amp;', '&', $query);
        $url = xss_sanitize($query); ?>
        <a id="disclaimerGo" class="disclaimerContinue" data-nodisclaimer="true" target="_blank" href="<?= $url ?>">Continue</a> | 
        <a class="disclaimerCancel" href="javascript: history.back()">Cancel</a>     
<?php } else { 
        $url =  xss_sanitize($_GET['email']);
        if (strpos($url,'mailto') !== false) { ?>
            <a id="disclaimerGo" class="disclaimerContinue" data-nodisclaimer="true" href="<?= $url ?>" >Continue</a> | 
            <a class="disclaimerCancel" href="javascript: history.back()">Cancel</a> 
 <?php  } else { ?>
            <a id="disclaimerGo" class="disclaimerContinue" data-nodisclaimer="true" href="javascript: history.back()" >Continue</a> | 
            <a class="disclaimerCancel" href="javascript: history.back()">Cancel</a>           
  <?php }      
    } ?>

</div>

<script type="text/javascript">
    $('#disclaimerGo').click(function() {
        setTimeout(function(){ history.back(); }, 2000);
    });
</script>