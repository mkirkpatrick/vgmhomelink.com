<?php
ini_set("log_errors", 1);
date_default_timezone_set('US/Central');
ini_set('memory_limit', '256M');
ini_set('max_execution_time', 180);
set_time_limit(180);
ini_set('session.gc_maxlifetime', 30*60);
ini_set('session.hash_function', "sha512");
ini_set('session.session.use_cookies', 1);
ini_set('session.use_only_cookies', 1);
ini_set('session.use_strict_mode', 1);
ini_set("session.cookie_httponly", 1);

header_remove('X-Powered-By');
ini_set("session.cookie_secure", 1);
header('Strict-Transport-Security: max-age=15552001');
header('X-UA-Compatible: IE=Edge');
header('X-Frame-Options: sameorigin');

/* Start Session */
if(!headers_sent() && !isset($_SESSION)) {
    session_start();
}
$_WEBCONFIG = array();

/* Load Site Config */
$configPath = './';
while(is_readable($configPath)) {
    if (file_exists($configPath . 'web.config')) {
        $configXML = simplexml_load_file($configPath . 'web.config');

        // GET THE APPSETTINGS FROM THE WEB.CONFIG
        if(isset($configXML->appSettings)){
            foreach ($configXML->appSettings->add as $setting) {
                if(!isset($_WEBCONFIG[(string)$setting->attributes()->key])) {
                    $_WEBCONFIG[(string)$setting->attributes()->key] = (string)$setting->attributes()->value;
                }
            }
        }

        if(isset($_WEBCONFIG['FRIENDLY_ERRORS'])) {
            $_WEBCONFIG['CUSTOMERRORS']['MODE'] = $_WEBCONFIG['FRIENDLY_ERRORS'];
        }

        // GET THE CUSTOM ERROR PAGES FROM THE WEB.CONFIG
        if(isset($configXML->{'system.webServer'}) && isset($configXML->{'system.webServer'}->httpErrors)) {
            $_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['403'] = $configXML->{'system.webServer'}->httpErrors->error[0]->attributes()->path;
            $_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['404'] = $configXML->{'system.webServer'}->httpErrors->error[1]->attributes()->path;
            $_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['500'] = $configXML->{'system.webServer'}->httpErrors->error[2]->attributes()->path;
        }

        if(isset($configXML->connectionStrings)){
            foreach ($configXML->connectionStrings->add as $connectionString) {
                if(!isset($_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name])) {
                    $_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name] = array();
                    $connData = explode(";", (string)$connectionString->attributes()->connectionString);
                    $_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['HOST'] = str_replace("=", "", stristr($connData[0], '='));
                    $_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['DATABASE'] = str_replace("=", "", stristr($connData[1], '='));
                    $_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['USERNAME'] = str_replace("=", "", stristr($connData[2], '='));
                    $_WEBCONFIG['CONNECTIONS'][(string)$connectionString->attributes()->name]['PASSWORD'] = str_replace("=", "", stristr($connData[3], '='));
                }
            }
        }

    }
    $configPath = '../'. $configPath;
}
if(count($_WEBCONFIG) == 0) {
    die("web.config Configuration not setup properly");
}

require_once $_SERVER['DOCUMENT_ROOT'] . "/handlers/error_handler.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/functions.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/constants.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/handlers/autoload_handler.php";

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_authentication.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/database/class_database.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/database/class_datatable.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/database/class_entity.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/database/class_repository.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_active_directory.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_logger.php");

if(stristr($_SERVER['ABSOLUTE_URI'], $_WEBCONFIG['VPANEL_PATH'])) {
    require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/functions.php");
    require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_security.php");
    require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], $_WEBCONFIG['VPANEL_PATH'], "library/classes/class_user_manager.php");
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/VPanel/app_offline.htm')) {
        print file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/VPanel/app_offline.htm');
        exit;
    }
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/app_offline.htm')) {
    print file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/app_offline.htm');
    exit;
}


$siteConfig	 = array();
$sql		= "SELECT name,value FROM tbl_site_config LIMIT 100";
$siteData	= Database::Execute($sql);
if($siteData->Count() == 0) {
    die("Database Site Configuration not setup properly");
}
while($siteData->MoveNext()) {
    $siteConfig[$siteData->name] = $siteData->value;
}
unset($siteData);

$sql = "SELECT as_name, as_value FROM tbl_cms_asset WHERE at_id = 1";
$r = Database::Execute($sql);
if($r->Count() > 0) {
    while($r->MoveNext()) {
        $name = ucwords(str_replace(" ", "_", $r->as_name));
        $siteConfig[$name] = $r->as_value;
    }
}

if($_WEBCONFIG['SITE_TYPE'] == 'ECOMMERCE' || $_WEBCONFIG['SITE_TYPE'] == 'CATALOG') {
    $sql = "SELECT * FROM tbl_ecommerce_site_config LIMIT 100";
    $ecommerceConfig = Database::Execute($sql);
    $ecommerceConfig->MoveNext();

    if ($ecommerceConfig->Count() == 0) {
        die("No E-Commerce Site Configuration Found!");
    }

    loadLocalConfig($_SERVER['DOCUMENT_ROOT'] . '/catalog/');
}

if($_WEBCONFIG['SITE_STATUS'] == "LIVE") {
    $_WEBCONFIG['DEVELOPER_EMAIL'] = "bugtracker@forbin.com";
}

if(!endsWith($_SERVER['SCRIPT_NAME'], 'site-login/process.php') && strpos(strtolower($_SERVER['SCRIPT_NAME']), "/vpanel") === false) {
    // Make sure user has loged in
    //Authentication::authenticate_user();
}

?>