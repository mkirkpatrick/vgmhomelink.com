<?php
/*
//////////////////////// Forbin Form Processor 1.50 //////////////////////

This file should not be edited unless improvements are being made.
Configuration changes should be made outside this file if necessary
to customize your form. If bug fixes or improvments are made please
check into SVN. This particular version has been modified for PageBuildr use
please check SVN for standalone version for landing pages.

@author  -- Matthew Kauten
@version -- January 1, 2013
@created -- January 20, 2011
@company -- VGM Forbin

Updates
-- Added + Notation to Concatenate Multiple Form Field Values into one string (Matt K, August 15, 2013)
-- Added 'Developer' Configuration Parameter (Matt K, February 18, 2012)
-- Added 'Array Delimitor' Configuration Parameter (Matt K, June 28, 2012)
-- Added Database Save Functionality (Matt K, June 30, 2012)
-- Added string receipient options (Matt K, July 25, 2012)
-- Added ignore fields and exempt fields options (Matt K, August 3, 2012)
-- Seperated out html into html template file (Matt K, August 24, 2012)
-- Added a Spam filter to remove bad submissions (Justin B, February 6, 2014)
-- All Form Recipients Use the Site Config (Justin B, October 21, 2014)
-- Added ~ Notation to not display these fields if their value is blank (Justin B, November 18, 2014)
-- Added Form Value Encryption (Justin B, August 29, 2015)
*/
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/config.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/classes/class_vMail.php');
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/classes/class_vEncryption.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/classes/class_browser_detection.php");

if(!isset($_POST['hidFormName']) || isNullOrEmpty($_POST['hidFormName']))  {
    redirect("/");
}
else {
    $formname = htmlEntities($_POST['hidFormName'], ENT_QUOTES, "utf-8");
}

$customForm = Repository::GetByFieldName('tbl_form_custom', 'cf_name', $formname);
if(is_null($customForm)) {
    redirect("/");
}

$emailData = $customForm->cf_email_data == '1' ? true : false;
$successMessage = $customForm->cf_success_message;
$requiredFields = explode("|", $customForm->cf_required_fields);
$developers = $_WEBCONFIG['DEVELOPER_EMAIL'];
$devemaildate = $_WEBCONFIG['DEVELOPER_DATE'];
$fieldLimit = (int) $customForm->cf_field_limit;
$multipleDelimitor = $customForm->cf_delimitor;
$sitename = $siteConfig['Company_Name'];
$formId = (int) $customForm->cf_id;
$receipients = $customForm->cf_recipients;
# Global Variables
$message        = "";
$requiredList   = "";
$primaryValue   = "";
$secondaryValue = "";
$teriaryValue   = "";
$replyToValue   = "";

if(isPost()) {
    $formFields = array();
    $isValid = true;
    $charCount = 0;
    $fieldCount = 0;
    $ip = $_SERVER['HTTP_CLIENT_IP'];

    if ($isValid && validateRC())  {
        # Instantiate Form Fields
        foreach($_POST as $key => $value) {
            if(strstr($key, "RC_") || strstr($key, "submit") || startsWith($key, "hid") || strstr($key, "__") || strstr($key, "$") || (isset($exemptFields) && in_array($key, $exemptFields))) {
                # Field on Ignore List
                continue;
            }
            else {
                $fieldCount++;
            }

            if (!is_array($value)) {
                checkQualys($value);
            }

            // Format Name
            $name = str_replace(array("_"), " ", format($key));
            $name = str_replace(array("!","+","~"), "", $name);

            // Check For Required Fields
            if(in_array(strtolower($name), $requiredFields) && ((is_array($value) && count($value) <= 1) || (!is_array($value) && strlen($value) == 0))) {
                $requiredList .= HTML_SPACE . " -- <b>" . $name . "</b> is required<br />";
                continue;
            }

            // Store Value
            if(is_array($value) && stristr($key, "+") && ((stristr($key, "~") && !isNullOrEmpty(implode(' ', $value))) || !stristr($key, "~"))){
                $values = implode(' ', $value);
                $formFields[$name] = format($values);
            }
            else if(is_array($value) && ((stristr($key, "~") && !isNullOrEmpty(implode(' ', $value))) || !stristr($key, "~"))){
                $values = implode($multipleDelimitor, $value);
                $formFields[$name] = format($values);
            }
            else if((!(isset($ignoreFields) && in_array($key, $ignoreFields))) && !is_array($value)
            && ((stristr($key, "~") && !isNullOrEmpty($value)) || !stristr($key, "~"))) {

                //Check for spam in fields
                //spamChecker($value, $successMessage);
                $formFields[$name] = format($value);
            }
            else {
                continue;
            }

            //Update Character Count
            $charCount += strlen($formFields[$name]);
            /*            if($fieldLimit == $fieldCount) {
            # Hit Field Max Break Loop
            break;
            }*/
        }

        if(strlen($requiredList)) {
            $isValid = false;
            $message = "<div class='notification invalid'><p><b>Please correct the following: </b></p><p class='requiredItems'>";
            $message .= $requiredList;
            $message .= "</span></p></div>";
        }
        else if($charCount == 0) {
            $isValid = false;
            $message = "<div class='notification invalid'>";
            $message .= "Unable to complete submission because the form was not filled out completely. Please fill out the form again with all the required information and click submit to try again. ";
            $message .= "</div>";
        }
        else  {
            $isValid = true;
        }

        if (isset($_POST['g-recaptcha-response']) && !verify_gcaptcha("6LfIam0UAAAAAFnPEySHMocCpiRZPpM95Ga-hO2j")) {
            $isValid = false;
            $message = "<div class='notification invalid'>";
            $message .= "Please check that you are not a robot.";
            $message .= "</div>";
        }

        if($isValid) {
            // Get User's Brower Details
            $browser = new BrowserDetection();
            $userBrowserName = $browser->getBrowser();
            $userBrowserVer = $browser->getVersion();
            $userBrowserPlatform = $browser->getPlatform();
            $userBrowserUserAgent = $browser->getUserAgent();
            $userBrowserIsMobile = $browser->isMobile() == "" ? 0 : $browser->isMobile();
            $userBrowserIsRobot = $browser->isRobot() == "" ? 0 : $browser->isRobot();
            $userIP = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "NOT AVAILABLE";
            $vEncryption = new vEncryption(FORM_ENCRYPTION_KEY);

            foreach($formFields as $name => $value) {
                $value = Database::quote_smart($value);
                if($name == $customForm->cf_primary_field) {
                    $primaryValue = $value;
                } elseif($name == $customForm->cf_secondary_field){
                    $secondaryValue = $value;
                } elseif($name == $customForm->cf_teriary_field) {
                    $teriaryValue = $value;
                }

                if($name == $customForm->cf_reply_to_field) {
                    $replyToValue = $value;
                }

                if($name != 'g-recaptcha-response' ){
                    if(isset($_WEBCONFIG['ENABLE_FORM_ENCRYPTION']) && $_WEBCONFIG['ENABLE_FORM_ENCRYPTION'] == "true") {
                        if(strlen(trim($value)) > 0) {
                            $value = $vEncryption->encrypt($value);
                        }
                    }
                    $databaseFormSubmission[$name] = $value;
                }
            }

            $formDataString = json_encode($databaseFormSubmission);

            $sql = "INSERT INTO `tbl_form_submission`
            (`cf_id`,
            `fs_primary_value`,
            `fs_secondary_value`,
            `fs_teriary_value`,
            `fs_form_data`,
            `fs_reply_to_value`,
            `fs_user_ip`,
            `fs_last_updated`,
            `fs_date_submitted`,
            `fs_os`,
            `fs_browser`,
            `fs_user_agent`,
            `fs_is_mobile`,
            `fs_is_robot`)
            VALUES(
            $formId,
            '$primaryValue',
            '$secondaryValue',
            '$teriaryValue',
            '$formDataString',
            '$replyToValue',
            '$userIP',
            NOW(),
            NOW(),
            '$userBrowserPlatform',
            '$userBrowserName $userBrowserVer',
            '$userBrowserUserAgent',
            '$userBrowserIsMobile',
            '$userBrowserIsRobot');";
            Database::ExecuteRaw($sql);
            $submissionId = Database::getInsertID();

            # Set Up Form Mailer
            $mailer = new vMail();

            if(isset($_WEBCONFIG) && isset($_WEBCONFIG['GLOBAL_FORM_RECEIPIENT_OVERRIDE'])) {
                $mailer->addRecipient($_WEBCONFIG['GLOBAL_FORM_RECEIPIENT_OVERRIDE']);
            }
            else {
                if($_WEBCONFIG['SITE_STATUS'] == 'STAGING'){
                    if(is_array($developers)) {
                        foreach($developers as $dev => $devEmail) {
                            $mailer->addRecipient($devEmail, $dev);
                        }
                    }
                    else {
                        $developerArray = explode(';', str_replace(' ', '', $developers), 20);
                        foreach($developerArray as $devEmail) {
                            $mailer->addRecipient($devEmail);
                        }
                    }
                }
                else{
                    if(is_array($receipients)) {
                        foreach($receipients as $name => $email) {
                            $mailer->addRecipient($email, $name);
                        }
                    }
                    else {
                        $receipientArray = explode(';', str_replace(' ', '', $receipients), 20);
                        foreach($receipientArray as $email) {
                            $mailer->addRecipient($email);
                        }
                    }
                }
            }

            // Add Blind Copy For Developers When not in Test Mode
            if(isset($_WEBCONFIG) && isset($_WEBCONFIG['GLOBAL_FORM_BCC_ADDRESS'])) {
                $mailer->addBlindCopy($_WEBCONFIG['GLOBAL_FORM_BCC_ADDRESS']);
            }

            $mailer->setSubject("$sitename :: $formname Submitted");
            $mailer->setMailType("html");
            $mailer->setFrom($siteConfig['Default_From_Email'], "$sitename Website");
            $mailer->setReplyTo($siteConfig['Default_Reply_To_Email'], "$sitename Website");

            $htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], "/emailtemplates/form-template-email.html");
            if(file_exists($htmlFile)) {
                $fh = fopen($htmlFile, 'r');
                $body = fread($fh, filesize($htmlFile));
                fclose($fh);
            }
            else {
                throw new exception("HTML Email Template File Not Found");
            }

            $dataText = "";
            if($emailData) {
                foreach($formFields as $name => $value) {
                    $label = str_truncate(str_replace(HTML_SPACE, HTML_SPACE, $name), 75);
                    $dataText .= '<tr>' . PHP_EOL;
                    $dataText .= '<td valign="top"><strong>' . $label. HTML_SPACE . HTML_SPACE . '</strong><br />' . PHP_EOL;
                    $dataText .= str_truncate($value, 500) . '</td>' . PHP_EOL;
                    $dataText .= '</tr>' . PHP_EOL;
                }
            } else {
                $dataText = '<tr><td><div align="center"><h2><a href="' . $_SERVER['ROOT_URI'] . '/VPanel/forms/index.php?view=view&id=' . $submissionId . '">View Submission</a></h2></div></td></tr>';
            }

            $mergeFields = array();
            $mergeFields["**GUEST**"]        = $siteConfig['Company_Name'];
            $mergeFields["**COMPANYNAME**"]  = $siteConfig['Company_Name'];
            $mergeFields["**FORMNAME**"]     = $formname;
            $mergeFields["**SUBMISSIONID**"] = $submissionId;
            $mergeFields["**TABLEDATA**"]    = $dataText;
            $mergeFields["**WEBSITE**"]      = $_WEBCONFIG['SITE_URL'];
            $mergeFields["**DISPLAY_URL**"]  = $_WEBCONFIG['SITE_DISPLAY_URL'];
            $mergeFields["**YEAR**"]         = date("Y", time());

            $body = strtr($body, $mergeFields);

            $mailer->setMessage($body);
            $mailer->sendMail();
            $submitSuccess = true;
            $message = "<div class='notification success' id='SuccessDiv'><h1>Thank You!</h1><p>";
            $message .= $successMessage;
            $message .= "</p></div><br />";

            $_SESSION['PB_USER_RESPONSE_MESSAGE'] = $message;
            if(strpos($_SERVER['HTTP_REFERER'],'success') !== false) {
                $successString = '?success=' . format_uri($formname);
                $parts = parse_url($_SERVER['HTTP_REFERER']);
                $newUrl = $parts['scheme'].'://'.$parts['host'].$parts['path'];
                redirect($newUrl . $successString);
            }
            else {
                $successString = (strpos($_SERVER['HTTP_REFERER'],'?') !== false) ? '&success=' . format_uri($formname) : '?success=' . format_uri($formname);
                redirect($_SERVER['HTTP_REFERER'] . $successString);
            }
        }
    }
}
else {
    $message = "<div class='notification invalid'>";
    $message .= "Unable to complete submission because the form was not filled out completely. Please fill out the form again with all the required information and click submit to try again. ";
    $message .= "</div>";
}

$_SESSION['PB_USER_RESPONSE_MESSAGE'] = $message;
redirect($_SERVER['HTTP_REFERER']);

/************* Functions **************/
function format($field) {
    return trim(nl2br(strip_tags($field, '<br>')));
}

function checkQualys($data) {

    $Spam = ["was@qualys.com", "|ping", "qualys"];

    foreach ($Spam as $word) {
        if(strpos(strtolower($data), strtolower($word)) !== false) {
            $submitSuccess = true;
            $message = "<div class='notification success' id='SuccessDiv'><h1>Thank You!</h1><p>";
            $message .= "</p></div><br />";

            $_SESSION['PB_USER_RESPONSE_MESSAGE'] = $message;
            redirect($_SERVER['HTTP_REFERER']);
            exit;
        }
    }
}