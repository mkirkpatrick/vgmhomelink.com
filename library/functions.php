<?php
////////////////////////////     PAGE BUILDER FUNCTIONS     \\\\\\\\\\\\\\\\\\\\\\\\\\\
function ssl_check(){
    if($_SERVER["HTTPS"] != "on") {
        redirect("https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
    }
}

function ssl_uncheck() {
    if ($_SERVER['HTTPS'] == 'on') {
        $url .= 'http://' . $_SERVER['HTTP_HOST'];

        if($_SERVER['REQUEST_URI'] == '/index.php') {
            $url  .= '/';
        }
        else {
            $url .= $_SERVER['REQUEST_URI'];
        }

        if($_SERVER['PHP_SELF'] == $_SERVER['REQUEST_URI']) {
            $url .= $_SERVER['QUERY_STRING'] ? '?' . $_SERVER['QUERY_STRING'] : '';
        }
        redirect($url);
    }
}

function getPageTitle($cms_id){
    $pageId = (int)$cms_id;
    $sql = "SELECT cmsv_value AS title
    FROM tbl_cms_name_value
    WHERE cms_id = $pageId AND cmsv_name = 'Title'
    LIMIT 1";
    $pageTitle = Database::Execute($sql);
    if($pageTitle->Count() > 0) {
        $pageTitle->MoveNext();
        $title = $pageTitle->title;
    }
    return $title;
}

function getTopCMSPages($limit) {
    global $_WEBCONFIG;
    $results = "";
    $sql = "SELECT *
    FROM tbl_cms
    WHERE cms_is_active = '1'
    AND cms_historic_cms_id IS NULL
    AND url_direct <> 'search'
    AND url_direct <> ''
    AND url_direct <> 'disclaimer'
    AND url_direct <> '404-error'
    AND url_direct <> '403-error'
    AND url_direct <> 'site-map'
    AND cms_is_deleted <> 1
    ORDER BY page_views DESC
    LIMIT $limit ";
    $count = Database::Execute($sql);

    if($count->Count() > 0) {
        while($count->MoveNext()) {
            $results .= '<li class=""><a title="' . $count->cms_title . '" href="/' . $count->url_direct . '">' . $count->cms_title . '</a></li>';
        }
    }
    return $results;
}

function updatePageView($iNum){
    global $_WEBCONFIG;

    $pageId = Database::quote_smart($iNum);
    $sql = "SELECT * FROM tbl_cms WHERE cms_id = $pageId LIMIT 1";
    $record = Database::Execute($sql);
    if($record->Count() > 0) {
        $sql = "UPDATE tbl_cms SET page_views = page_views + 1 WHERE cms_id = $pageId";
        Database::ExecuteRaw($sql);
    }
}

function getParent($iNum){
    global $_WEBCONFIG;

    $thisCmsID = Database::quote_smart($iNum);
    $sql = "SELECT cms_parent_id FROM tbl_cms WHERE cms_id = '$thisCmsID'";
    $parent = Database::Execute($sql);
    $parentID = 0;
    if ($parent->Count() > 0)
    {
        $parent->MoveNext();
        $parentID = $parent->cms_parent_id;
    }
    return $parentID;
}

function displayUserMessage(){
    $message = isset($_SESSION['PB_USER_RESPONSE_MESSAGE']) ? $_SESSION['PB_USER_RESPONSE_MESSAGE'] : '';
    $_SESSION['PB_USER_RESPONSE_MESSAGE'] = null;
    return $message;
}

function getLinkNameAndId($iNum){
    global $_WEBCONFIG;

    $thisCmsID = Database::quote_smart($iNum);
    $sql = "SELECT cms_title FROM tbl_cms WHERE cms_id = '$thisCmsID'";
    $nameElement = Database::Execute($sql);
    if ($nameElement->Count() > 0) {
        $nameElement->MoveNext();
        return array(0, $thisCmsID, $nameElement->cms_title);
    }

    return null;
}

function getAssets() {
    $namevalue = array();
    $sql = "SELECT as_key, as_value FROM tbl_cms_asset WHERE as_status = 'Active'";
    $record	= Database::Execute($sql);

    if ($record->Count() > 0) {
        while($record->MoveNext()) {
            $namevalue[$record->as_key] = $record->as_value;
        }
    }

    // Get Global Configuration Values
    $sql = "SELECT asset_key,value FROM tbl_site_config WHERE asset_key IS NOT NULL LIMIT 100";
    $record	= Database::Execute($sql);
    while($record->MoveNext()) {
        if(!isNullOrEmpty($record->asset_key)) {
            $namevalue[$record->asset_key] = $record->value;
        }
    }
    return $namevalue;
}

function getInactiveAssets() {
    $namevalue = "";
    $sql    = "SELECT as_key, as_value FROM tbl_cms_asset WHERE as_status = 'Inactive'";
    $record = Database::Execute($sql);

    if ($record->Count() > 0) {
        while($record->MoveNext()) {
            $namevalue[] = $record->as_key;
        }
    }

    // Get Global Configuration Values
    $sql    = "SELECT asset_key,value FROM tbl_site_config WHERE asset_key IS NOT NULL LIMIT 100";
    $record = Database::Execute($sql);
    while($record->MoveNext()) {
        if(!isNullOrEmpty($record->asset_key)) {
            $namevalue[] = $record->asset_key;
        }
    }
    return $namevalue;
}

function cPage($id){
    global $_WEBCONFIG;
    if($id != $_WEBCONFIG['SITE_ROOT_PAGE']) {
        $sql = 'SELECT cms_title, cms_parent_id, url_direct as url FROM tbl_cms c
        WHERE c.cms_is_historic = 0 AND c.cms_is_deleted  = 0 AND cms_id = \''.Database::quote_smart($id).'\'';
        $r = Database::Execute($sql);
        $r->MoveNext();

        if($r->Count() == 1){
            return $r;
        }
    }
    return false;
}

function cLand($id){
    $currentId = $id;
    $breadCrumb = '';
    while($p = cPage($currentId)){
        $newCrumb = ' <span class="arrow"></span>';
        if(ltrim($_SERVER['REQUEST_PATH'], "/") == $p->url) {
            $newCrumb .= $p->cms_title;
        } else {
            $newCrumb .= '' . $p->cms_title . '';
        }
        $breadCrumb = $newCrumb . $breadCrumb;
        $currentId = $p->cms_parent_id;
    }
    return $breadCrumb;
}

function printMobileSubMenu($menuId) {
    // Sub Navigational Menu
    $sql = "SELECT cms_id, cms_title, cms_sub_title, cms_dynamic_link, cms_dynamic_link_target FROM tbl_cms
    WHERE cms_is_historic = 0 and cms_is_deleted = 0 and cms_is_active = '1' and  cms_show_nav = '1' AND cms_parent_id = '" . $menuId . "'
    ORDER BY cms_display_order;";
    $subMenuItems = Database::Execute($sql);

    if ($subMenuItems->Count() > 0) {
        print '<ul id="subMenu' . $menuId . '" class="submenu">';

        while ($subMenuItems->MoveNext()) {
            echo '<script>$("#headerNavItem' . $menuId . '").addClass("Expandable")</script>';
            $subNavLink = "javascript:return(false);";
            $subNavLink = $subMenuItems->cms_dynamic_link;
            printf("<li id=\"headerNavSubItem{$subMenuItems->cms_id}\" class=\"menuSubItem\"><a href=\"%s\" class=\"%s\" target=\"%s\" title=\"%s\">%s</a>", $subNavLink, "headerSubNav" . $subMenuItems->cms_id, $subMenuItems->cms_dynamic_link_target, $subMenuItems->cms_sub_title, $subMenuItems->cms_title);
            printSubMenu($subMenuItems->cms_id);
            print "</li>" . PHP_EOL;
        }//end while

        print "</ul>\n";
    }
}

////////////////////////////     PHP EXTENTION FUNCTIONS     \\\\\\\\\\\\\\\\\\\\\\\\\\\
function autoCreateVerison($url, $returnType='echo'){
    $path = pathinfo($url);
    $ver = filemtime($_SERVER['DOCUMENT_ROOT'].$url);
    if($returnType == 'echo') {
        echo $path['dirname'].'/'.$path['basename'].'?v='.$ver;
    } elseif($returnType == 'return') {
        return $path['dirname'].'/'.$path['basename'].'?v='.$ver;
    }
}

// $src can be either a string or array
// Available Types: css, javascript
function addHtmlTag($src,$type="css") {
    if($type == "css"){
        if(is_array($src)) {
            foreach($src as $val) {
                $GLOBALS["cssTags"][] = $val;
            }
        } else {
            $GLOBALS["cssTags"][] = $src;
        }
    } elseif($type == "javascript"){
        if(is_array($src)) {
            foreach($src as $val) {
                $GLOBALS["javascriptTags"][] = $val;
            }
        } else {
            $GLOBALS["javascriptTags"][] = $src;
        }
    } else {
        throw new Exception("function addHtmlTag(): Invalid HTML tag '$type'");
    }
}

function addExternalScript($script){
    $GLOBALS["externalJavascriptTags"][] = $script;
}

function redirect($link, $params = array(), $httpResponse = "302 Found", $endResponse = true) {
    $parts = parse_url($link);
    if(isset($parts['query']) && !isNullOrEmpty($parts['query'])) {
        parse_str($parts['query'], $queryArray);
        $params = array_merge($queryArray, $params);
        if(isset($parts['scheme']) && isset($parts['host'])) {
            $link = $parts['scheme'].'://'.$parts['host'].$parts['path'];
        } else if(isset($parts['host'])) {
            $link = $parts['host'].$parts['path'];
        } else {
            $link = $parts['path'];
        }
    }
    if(count($params) > 0) {
        $params = array_unique($params);
        $link .= "?";
        $queryStrings = "";
        foreach ($params as $key => $value) {
            $queryStrings .= "&" . $key . "=" . $value;
        }
        $link .= ltrim($queryStrings, "&");
    }
    if(headers_sent()) {
        echo "<script type='text/javascript'>location.href='$link';</script>";
        echo '<meta http-equiv="refresh" content="10;url='. $link .'" />';
    } else {
        header("HTTP/1.0 $httpResponse");
        header("Location: $link");
    }
    if($endResponse) exit;
}
function serverTransfer($path) {
    global $siteConfig, $_WEBCONFIG;
    if (strpos($path, '?') !== false) {
        $parts = explode("?", $path);
        $path = $parts[0];
        parse_str($parts[1], $queryStrings);
        $_GET = array_merge($queryStrings, $_GET);
        $_REQUEST = array_merge($queryStrings, $_REQUEST);
    }
    $path = $_SERVER['DOCUMENT_ROOT'] . $path;

    if(stream_resolve_include_path($path) !== false) {
        if (ob_get_length() > 0) { ob_end_clean(); }
        require_once($path);
        exit;
    } else {
        throw new Exception("Path: " . $path . " does not exist");
    }
}

function spamChecker($data, $successMessage="") {
    // PHASE 1: WE NEDD TO UPDATE TO A JSON READ
    // PHASE 2: BEEF UP
    $Spam = Array("seo", "4u", "adipex", "execute", "increase traffic", "baccarrat", "click here", "click on", "please visit our website", "adding a link on your links page",
        "blackjack", "booker", "search phrases", "it company", "casino", "cialis", "gambling", "risk free", "eliminate debt", "buy now", "dear manager",
        "off shore", "dear friend", "internet marketing", "holdem", "incest", "paxil", "penis", "naked", "nude", "search engine listings", "hello dear", "re:",
        "poker chip", "web development", "pussy", "ringtones", "search engine optimization", "make money", "cash bonus", "dear madam", "dear sir",
        "lead generation", "web design", "link building", "dear professionals", "free web", "helped hundreds of companies", "increase rankings", "$$$",
        "search engine traffic", "increase their traffic", "software development", "links to your website", "we manufacture", "reply with the subject line",
        "roulette", "shemale", "slot-machine", "texas-holdem", "viagra", "vioxx", "xanax", "zolus", "exec", "script", "visit our website", "dear buyer",
        "ranking", "10,000 twitter", "100,000 youtube", "20,000 google", "marketing solutions", "facebook likes provider", "justin bieber",
        "professional social media business manager", "generate online buzz", "search engine marketing", "web based solutions", "click below",
        "get-fb-likes.com", "getbusinessfunded.com", "findbusinessfunds.com", "fastfundingadvisor.com", "BusinessCapitalAdvisor.com", "fame-boost.com", "123456",
        "yandex.ru", "searchlland.com", "valium", "I stumbled upon your site", "000-000-0000", "probusinessfunding.com", "businessloansfunded.com", "loads-of-likes.com",
        "123456", "Cheap Jerseys China", "Wholesale NFL Jerseys", "Gangman Style", "if you want thousands of new visitors", "morty", "goldman", "goldman;",
        "http://BusinessCapitalPro.com","Cheap Louis Vuitton", "http://SmallBusinessFundingFast.com", "website network of 250,000 sites", "http://trafficinstitution.com/plans-pricing/",
        "cheap hermes belts", "Hermes Birkin", "Michael Kors Charm", "ï»¿", "000000");

    foreach ($Spam as $word) {
        if(strpos(strtolower($data), strtolower($word)) !== false) {
            $submitSuccess = true;
            $message = "<div class='notification success' id='SuccessDiv'><h1>Thank You!</h1><p>";
            $message .= $successMessage;
            $message .= "</p></div><br />";

            $_SESSION['PB_USER_RESPONSE_MESSAGE'] = $message;
            redirect($_SERVER['HTTP_REFERER']);
            exit;
        }
    }
}

function closeAllHtmlTags($html) {
    #put all opened tags into an array
    preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);

    $openedtags = $result[1];   #put all closed tags into an array

    preg_match_all('#</([a-z]+)>#iU', $html, $result);

    $closedtags = $result[1];

    $len_opened = count($openedtags);

    # all tags are closed

    if (count($closedtags) == $len_opened) {

        return $html;

    }

    $openedtags = array_reverse($openedtags);

    # close tags

    for ($i=0; $i < $len_opened; $i++) {

        if (!in_array($openedtags[$i], $closedtags)){

            $html .= '</'.$openedtags[$i].'>';

        } else {

            unset($closedtags[array_search($openedtags[$i], $closedtags)]);    }

    }
    return $html;
}

function removeAttributeTag($attribute, $value) {
    $output = preg_replace('/(<[^>]+) ' . $attribute . '=".*?"/i', '$1', $value);
    return $output;
}
// Don't Use use str_replace
function str_remove($remove, $haystack) {
    return str_replace($remove, "", $haystack);
}
function str_truncate($string, $limit, $pad="...", $break=" ") {
    if(strlen($string) <= $limit) return $string;

    if(false !== ($breakpoint = strpos($string, $break, $limit))) {
        if($breakpoint < strlen($string) - 1) {
            $string = substr($string, 0, $breakpoint) . $pad;
        }
    }
    return $string;
}
function xss_sanitize($string, $removeTags = true) {
    if($removeTags) {
        $string = strip_tags($string);
    }
    $string = str_replace("javascript:", "", $string);
    $string = htmlEntities($string, ENT_QUOTES, "utf-8");
    $string = str_replace("(", "&#40;", $string);
    $string = str_replace(")", "&#41;", $string);
    return $string;
}
function str_replace_inner($needle_start, $needle_end, $replacement, $str) {
    $pos = strpos($str, $needle_start);
    $start = $pos === false ? 0 : $pos + strlen($needle_start);

    $pos = strpos($str, $needle_end, $start);
    $end = $pos === false ? strlen($str) : $pos;

    return substr_replace($str, $replacement, $start, $end - $start);
}
function startsWith($haystack, $needle)
{
    return !strncmp($haystack, $needle, strlen($needle));
}
function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}
function isNullOrEmpty($string) {
    return (!isset($string) || strlen(trim($string)) == 0);
}
function breakPoint($variable, $stopProcessing = true) {
    header_remove('content-type');
    header('content-type: text/html; charset=UTF-8');
    if($stopProcessing) { ob_end_clean(); }
    print "<pre>";
    if(is_array($variable) || is_object($variable)) {
        print_r($variable);
    } else {
        print $variable;
    }
    print "</pre>";
    if($stopProcessing) { exit; }
}

function isPost() {
    return count($_POST) > 0;
}

function app_info() {
    global $_WEBCONFIG;
    if($_WEBCONFIG['SITE_STATUS'] != 'LIVE') {
        ob_end_clean();
        print_r($_REQUEST);
        print_r($_SESSION);
        print_r($_WEBCONFIG);
        print_r($_SERVER);
        exit;
    }
}
function create_guid() {
    if (function_exists('com_create_guid') === true) {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

function get_url_status($url, $timeout = 100) {
    $ch = curl_init();
    // set cURL options
    $opts = array(CURLOPT_RETURNTRANSFER => true, // do not output to browser
        CURLOPT_URL => $url,            // set URL
        CURLOPT_NOBODY => true,         // do a HEAD request only
        CURLOPT_TIMEOUT => $timeout);   // set timeout
    curl_setopt_array($ch, $opts);
    curl_exec($ch); // do it!
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE); // find HTTP status
    curl_close($ch); // close handle
    return $status; //or return $status;
}

function curl_get_contents($url, $checkSSL = false, $timeout = 100) {
    // Initiate the curl session
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $checkSSL);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $checkSSL);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, false);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}
function validateAjaxRequest() {
    if(!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {
        header('HTTP/1.0 403 Forbidden');
        exit("Direct access not permitted.");
    }
}
function sanitize_from_word( $content ) {
    // Convert Microsoft special characters
    $replace = array(
        "�" => "'",
        "�" => "'",
        "�" => '"',
        "�" => '"',
        "�" => "-",
        "�" => "&#8212;",
        "�" => "&#189;",
        "�" => "&#190;",
        "�" => "&#188;",
        "�" => "&#169;",
        "�" => "&#174;",
        "�" => "&#8482;",
        "�" => "&#8230;",
        "�" => "'",
        "�" => "'",
        "�" => '"',
        "�" => '"',
        "�" => "-",
        "�" => "&#8212;",
        "�" => "&#189;",
        "�" => "&#190;",
        "�" => "&#188;",
        "�" => "&#169;",
        "�" => "&#174;",
        "®" => "&#174;",
        "�" => "&#8482;",
        "™" => "&#8482;",
        "�" => "&nbsp;",
        "�" => "&#8230;",
    );

    foreach($replace as $k => $v) {
        $content = str_replace($k, $v, $content);
    }

    // Remove any non-ascii character
    $content = preg_replace('/[^\x20-\x7E]*/','', $content);
    //$content = iconv("utf-8","ascii//TRANSLIT",$content);

    return $content;
}


////////////////////////////     FILE / IO FUNCTIONS     \\\\\\\\\\\\\\\\\\\\\\\\\\\

function url_sanitize($string, $force_lowercase = true, $anal = false) {
    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
        "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
        "—", "–", ",", "<", ">", "/", "?");
    $clean = trim(str_replace($strip, "", strip_tags($string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
    return ($force_lowercase) ?
    (function_exists('mb_strtolower')) ?
    mb_strtolower($clean, 'UTF-8') :
    strtolower($clean) :
    $clean;
}

function filter_file_name( $filename ) {
    return preg_replace("[^\w\s\d\.\-_~,;:\[\]\(\]]", '', $filename);
}
function display_filesize($filesize){

    if(is_numeric($filesize)){
        $decr = 1024; $step = 0;
        $prefix = array('Byte','KB','MB','GB','TB','PB');

        while(($filesize / $decr) > 0.9){
            $filesize = $filesize / $decr;
            $step++;
        }
        return round($filesize,2).' '.$prefix[$step];
    } else {

        return 'NaN';
    }

}

/**
 * Takes the paths passed into the function and combines them, replacing \ with /
 *
 */
function filePathCombine($paths = null) {
    $path = '';
    $arguments = func_get_args();
    $args = array();

    foreach ($arguments as $a) {
        if ($a !== '') {
            $args[] = $a;
        }
    }

    $arg_count = count($args);

    for ($i = 0; $i < $arg_count; $i++) {

        // If the OS is Windows, then strip leading / from path, else if it's Linux then don't
        if ((strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')) {
            $folder = trim($args[$i], URL_DIRECTORY_SEPERATOR);
        } else if (strtoupper(PHP_OS) === 'LINUX') {
            $folder = $args[$i];
        }

        if ($i != 0 && $folder[0] == DIRECTORY_SEPARATOR) {
            $folder = substr($folder, 1);
        }

        if (substr($folder, -1) == DIRECTORY_SEPARATOR) {
            $folder = substr($folder, 0, -1);
        }

        $path .= $folder;

        if ($i < ($arg_count - 1) || strpos($folder, '.') === false) {
            $path .= DIRECTORY_SEPARATOR;
        }
    }
    return str_replace(URL_DIRECTORY_SEPERATOR, DIRECTORY_SEPARATOR, $path);
}

function urlPathCombine() {
    $path = '';
    $arguments = func_get_args();
    $args = array();

    foreach ($arguments as $a) {
        if ($a !== '') {
            $args[] = $a;
        }
    }

    $arg_count = count($args);
    for ($i = 0; $i < $arg_count; $i++) {

        // If the OS is Windows, then strip leading / from path, else if it's Linux then don't
        if ((strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')) {
            $folder = trim($args[$i], DIRECTORY_SEPARATOR);
        } else if (strtoupper(PHP_OS) === 'LINUX') {
            $folder = $args[$i];
        }

        if ($i != 0 && $folder[0] == URL_DIRECTORY_SEPERATOR) {
             $folder = substr($folder,1);
        }
        if (substr($folder,-1) == URL_DIRECTORY_SEPERATOR) {
             $folder = substr($folder,0,-1);
        }

        $path .= $folder;

        if ($i < ($arg_count-1) || strpos($folder, '.') === false){
            $path .= URL_DIRECTORY_SEPERATOR;
        }
    }

    return str_replace(DIRECTORY_SEPARATOR, URL_DIRECTORY_SEPERATOR, $path);
}
function saveUpload($path, $uploaderName) {
    if(!move_uploaded_file($_FILES[$uploaderName]['tmp_name'], $path)) {
        throw new exception("There was an error uploading the file, please try again!");
    }
}
function deleteFolder($dir) {
    if (!file_exists($dir)) return true;
    if (!is_dir($dir)) return unlink($dir);
    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') continue;
        if (!$this->deleteFolder($dir.'/'.$item)) return false;
    }
    return rmdir($dir);
}
function getExtName($file) {
    if(strpos(basename($file), ".") === false)
        return '';

    $file = explode(".",basename($file));
    return $file[count($file)-1];
}
function makeDir($path, $mode = 0777) {
    $old = umask(0);
    $res = @mkdir($path, $mode, true);
    umask($old);
    return $res;
}
function getFiles($str) {
    return glob($str, GLOB_BRACE);
}
function DeleteImage($Params) {
    $files = getFiles($Params);

    foreach ($files as $file) {
        $deleted = @unlink($file);
    }
}//end function
function UploadImage($dirFolder, $fieldName, $Id, $Width, $Height, $Crop, $Prefix='') {
    $res	= '';
    $name	= md5(rand() * time());
    $fieldValue	= $_FILES[$fieldName]['name'];
    $ext	= strtolower(GetExtName($fieldValue));
    if ($Prefix) {
        $orig	= $dirFolder . "/orig_{$Id}{$Prefix}_{$name}.{$ext}";
        $dest	= $dirFolder . "/{$Id}{$Prefix}_{$name}.{$ext}";
    } else {
        $orig	= $dirFolder . "/orig_{$Id}_{$name}.{$ext}";
        $dest	= $dirFolder . "/{$Id}_{$name}.{$ext}";
    }

    if (!is_dir($dirFolder)) {
        $results = makeDir($dirFolder);
    } else {
        $results = TRUE;
    }

    if ($results) {
        if (move_uploaded_file($_FILES[$fieldName]['tmp_name'], $orig)) {
            $res = imageResize($orig, $dest, $Width, $Height, $Crop);
        } else {
            $res = "Error uploading $fieldValue.";
        }
    } else {
        $res = "Failed to create folder $dirFolder.";
    }

    return $res;
}//end function
function DeleteFile($Params) {
    $files = getFiles($Params);

    foreach ($files as $file) {
        $deleted = @unlink($file);
    }
}//end function
function UploadFile($dirFolder, $fieldName, $Id, $Prefix='') {
    $res	= '';
    $name	= md5(rand() * time());
    $fieldValue	= $_FILES[$fieldName]['name'];
    $ext	= strtolower(GetExtName($fieldValue));
    if ($Prefix) {
        $dest	= $dirFolder . "/{$Id}{$Prefix}_{$name}.{$ext}";
    } else {
        $dest	= $dirFolder . "/{$Id}_{$name}.{$ext}";
    }

    if (!is_dir($dirFolder)) {
        $results = makeDir($dirFolder);
    } else {
        $results = TRUE;
    }

    if ($results) {
        if (!move_uploaded_file($_FILES[$fieldName]['tmp_name'], $dest)) {
            $res = "Error uploading $fieldValue.";
        }
    } else {
        $res = "Failed to create folder $dirFolder.";
    }

    return $res;
}//end function
function imageResize($src, $dst, $width, $height, $crop=0) {

    if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";

    $type = strtolower(substr(strrchr($src,"."),1));
    if($type == 'jpeg') $type = 'jpg';
    switch($type){
        case 'bmp': $img = imagecreatefromwbmp($src); break;
        case 'gif': $img = imagecreatefromgif($src); break;
        case 'jpg': $img = imagecreatefromjpeg($src); break;
        case 'png': $img = imagecreatefrompng($src); break;
        default : return "Unsupported picture type!";
    }

    // resize
    if($crop){
        if($w < $width or $h < $height) return "Picture is too small!";
        $ratio = max($width/$w, $height/$h);
        $h = $height / $ratio;
        $x = ($w - $width / $ratio) / 2;
        $w = $width / $ratio;
    }
    else{
        if($w < $width and $h < $height) return "Picture is too small!";
        $ratio = min($width/$w, $height/$h);
        $width = $w * $ratio;
        $height = $h * $ratio;
        $x = 0;
    }

    $new = imagecreatetruecolor($width, $height);

    // preserve transparency
    if($type == "gif" or $type == "png"){
        imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
        imagealphablending($new, false);
        imagesavealpha($new, true);
    }

    imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

    switch($type){
        case 'bmp': imagewbmp($new, $dst); break;
        case 'gif': imagegif($new, $dst); break;
        case 'jpg': imagejpeg($new, $dst); break;
        case 'png': imagepng($new, $dst); break;
    }
    return '';
}//end function
function ImageTagResize($imagePath, $target) {
    // get image info
    list($width, $height, $type, $attr) = getimagesize($imagePath);
    // takes the larger size of the width and height and applies the formula accordingly...this is so this script will work dynamically with any size image
    $percentage = $width > $height ? ($target / $width) : ($target / $height);
    //gets the new value and applies the percentage, then rounds the value
    $width = round($width * $percentage);
    $height = round($height * $percentage);
    //returns the new sizes in html image tag format...this is so you can plug this function inside an image tag and just get the
    return "width=\"$width\" height=\"$height\"";
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : "Just Now";
}

//////////////////////////////////// SECURITY FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

function isStrongPassword($pass) {
    $ucl = preg_match('/[A-Z]/', $pass); // Uppercase Letter
    $lcl = preg_match('/[a-z]/', $pass); // Lowercase Letter
    $dig = preg_match('/\d/', $pass); // Numeral
    $nos = preg_match('/\W/', $pass); // Non-alpha/num characters (allows underscore)

    return $ucl && $lcl && $dig && $nos;
}

////////////////////////////     PHP FORM FIELD GENERATION FUNCTIONS     \\\\\\\\\\\\\\\\\\\\\\\\\\\

function getOptions($query, $sel = '') {
    $list	 = '';
    $records = Database::Execute($query);

    while ($records->MoveNext()) {
        $temp = 0;
        $list .= '<option value="' . $records->$temp . '"';
        if ($records->$temp == $sel) $list .= " selected";
        $temp = 1;
        $list .= ">" . htmlspecialchars($records->$temp) . "</option>\r\n";
    }

    return $list;
}

function buildSelectOptions($tblName, $valueField, $nameField, $index = 0) {
    $list	 = '';
    $categories = array();
    $sql	 = "SELECT $valueField as value, $nameField as name
    FROM $tblName
    ORDER BY $nameField
    LIMIT 1000";
    $record = Database::Execute($sql);

    while ($record->MoveNext()) {
        $list .= '<option value="' . $record->value . '"';
        if ($record->value == $index) $list .= " selected";
        $list .= ">" . htmlspecialchars($record->name) . "</option>\r\n";
    }

    return $list;
}

function getDataOptions($dataFile, $selectedValue = "")
{
    $doc = new DOMDocument();
    $doc->load($dataFile);
    $options = $doc->getElementsByTagName("option");
    $html = '<option value="">-- Select --</option>';
    foreach( $options as $option)
    {
        $value = $option->getAttribute('value');
        $text = $option->getAttribute('text');
        $selected = $selectedValue == $value ? "selected" : "";
        $html .= sprintf("<option value=\"%s\" %s>%s</option>\n", $value, $selected, $text);
    }
    return $html;
}

function buildCategoryOptions($tblName, $catId = 0) {

    $categories = array();
    $sql	 = "SELECT cat_id, cat_parent_id, cat_name FROM $tblName
    ORDER BY cat_parent_id, cat_sort_id, cat_name, cat_id";
    $catData = Database::Execute($sql);

    while ($catData->MoveNext()) {

        if ($catData->cat_parent_id == 0) {
            $categories[$catData->cat_id] = array('name' => $catData->cat_name, 'children' => array());
        } else {
            $categories[$catData->cat_parent_id]['children'][] = array('id' => $catData->cat_id, 'name' => $catData->cat_name);
        }
    }

    // build combo box options
    $list = '';


    foreach ($categories as $key => $value) {
        $name     = $value['name'];
        $children = $value['children'];

        $list .= "<option value=\"$key\"";
        if ($key == $catId) {
            $list.= " selected";
        }
        $list .= ">$name</option>\r\n";

        foreach ($children as $child) {
            $list .= "<option value=\"{$child['id']}\"";
            if ($child['id'] == $catId) {
                $list.= " selected";
            }
            $list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
        }
    }

    return $list;
}
function buildCategoryOptionsII($tblName, $tblCatName, $catId = 0, $sql2 = '') {

    $Id	= 0;
    $categories = array();
    $sql = "SELECT cat_id, cat_parent_id, cat_name FROM $tblCatName
    ORDER BY cat_parent_id, cat_sort_id, cat_name, cat_id";
    $catData = Database::Execute($sql);

    while ($catData->MoveNext()) {
        $sql = "SELECT COUNT(*) AS Total FROM $tblName WHERE cat_id = {$catData->cat_id} $sql2 ";
        $dt  = Database::Execute($sql);
        $dt->MoveNext();

        if ($dt->Total > 0 && $Id == 0) {
            $Id	= $catData->cat_id;
        }

        if ($catData->cat_parent_id == 0) {
            $categories[$catData->cat_id] = array('name' => $catData->cat_name, 'total' => $dt->Total, 'children' => array());
        } else {
            $categories[$catData->cat_parent_id]['children'][] = array('id' => $catData->cat_id, 'name' => $catData->cat_name, 'total' => $dt->Total);
        }
    }

    // build combo box options
    $list = '';

    foreach ($categories as $key => $value) {
        $name		= $value['name'];
        $total		= $value['total'];
        $children	= $value['children'];

        if ($total > 0) {
            $list .= "<option value=\"$key\"";
            if ($key == $catId) {
                $list.= " selected";
            }
            $list .= ">$name</option>\r\n";

            foreach ($children as $child) {
                if ($child['total'] > 0) {
                    $list .= "<option value=\"{$child['id']}\"";
                    if ($child['id'] == $catId) {
                        $list.= " selected";
                    }
                    $list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
                }
            }
        } else {
            if (sizeof($children) > 0) {
                $list .= "<optgroup label=\"$name\">";

                foreach ($children as $child) {
                    if ($child['total'] > 0) {
                        $list .= "<option value=\"{$child['id']}\"";
                        if ($child['id'] == $catId) {
                            $list.= " selected";
                        }
                        $list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
                    }
                }

                $list .= "</optgroup>";

            } else {

                $list .= "<option value=\"$key\"";
                if ($key == $catId) {
                    $list.= " selected";
                }
                $list .= ">$name</option>\r\n";
            }
        }
    }// end foreach

    return array($Id, $list);
}
function buildCategoryMultiOptions($tblName, $catId = array(), $groupOpt = true) {

    $categories = array();
    $sql	 = "SELECT cat_id, cat_parent_id, cat_name FROM $tblName
    ORDER BY cat_parent_id, cat_sort_id, cat_name, cat_id";
    $catData = Database::Execute($sql);

    while ($catData->MoveNext()) {

        if ($catData->cat_parent_id == 0) {
            $categories[$catData->cat_id] = array('name' => $catData->cat_name, 'children' => array());
        } else {
            $categories[$catData->cat_parent_id]['children'][] = array('id' => $catData->cat_id, 'name' => $catData->cat_name);
        }
    }

    // build combo box options
    $list = '';

    if ($groupOpt) {
        foreach ($categories as $key => $value) {
            $name     = $value['name'];
            $children = $value['children'];

            if (sizeof($children) > 0) {
                $list .= "<optgroup label=\"$name\">";

                foreach ($children as $child) {
                    $list .= "<option value=\"{$child['id']}\"";
                    if (in_array($child['id'], $catId)) {
                        $list.= " selected";
                    }
                    $list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
                }

                $list .= "</optgroup>";

            } else {

                $list .= "<option value=\"$key\"";
                if (in_array($key, $catId)) {
                    $list.= " selected";
                }
                $list .= ">$name</option>\r\n";
            }
        }

    } else {

        foreach ($categories as $key => $value) {
            $name     = $value['name'];
            $children = $value['children'];

            $list .= "<option value=\"$key\"";
            if (in_array($key, $catId)) {
                $list.= " selected";
            }
            $list .= ">$name</option>\r\n";

            foreach ($children as $child) {
                $list .= "<option value=\"{$child['id']}\"";
                if (in_array($child['id'], $catId)) {
                    $list.= " selected";
                }
                $list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
            }
        }
    }

    return $list;
}
function buildParentCategoryOptions($tblName, $catId = 0) {
    //------------------------------
    $categories = array();
    $sql	 = "SELECT cat_id, cat_parent_id, cat_name FROM $tblName WHERE cat_parent_id = 0
    ORDER BY cat_parent_id, cat_sort_id, cat_name, cat_id";
    $catData = Database::Execute($sql);

    while ($catData->MoveNext()) {

        if ($catData->cat_parent_id == 0) {
            $categories[$catData->cat_id] = array('name' => $catData->cat_name, 'children' => array());
        } else {
            $categories[$catData->cat_parent_id]['children'][] = array('id' => $catData->cat_id, 'name' => $catData->cat_name);
        }
    }

    // build combo box options
    $list = '';

    foreach ($categories as $key => $value) {
        $name     = $value['name'];
        $children = $value['children'];

        $list .= "<option value=\"$key\"";
        if ($key == $catId) {
            $list.= " selected";
        }
        $list .= ">$name</option>\r\n";

        foreach ($children as $child) {
            $list .= "<option value=\"{$child['id']}\"";
            if ($child['id'] == $catId) {
                $list.= " selected";
            }
            $list .= ">&nbsp;&nbsp;&nbsp;&nbsp;{$child['name']}</option>\r\n";
        }
    }
    return $list;
}
function buildSubCategoryOptions($tblName, $parentId, $catId = 0) {

    $categories = array();
    $sql	 = "SELECT cat_id, cat_parent_id, cat_name FROM $tblName
    WHERE cat_parent_id = $parentId
    ORDER BY cat_sort_id, cat_name, cat_id";
    $catData = Database::Execute($sql);

    while ($catData->MoveNext()) {
        $categories[$catData->cat_id] = array('name' => $catData->cat_name);
    }

    // build combo box options
    $list = '';

    foreach ($categories as $key => $value) {
        $name  = $value['name'];

        $list .= "<option value=\"$key\"";
        if ($key == $catId) {
            $list.= " selected";
        }
        $list .= ">$name</option>\r\n";
    }

    return $list;
}

////////////////////////////     PHP UTILTY FUNCTIONS     \\\\\\\\\\\\\\\\\\\\\\\\\\\

function removeNonAlphaNumeric($value) {
    return preg_replace("/[^a-zA-Z0-9\s]/", "", $value);
}

function decamelize($word) {
  return strtolower(preg_replace(["/([A-Z]+)/", "/_([A-Z]+)([A-Z][a-z])/"], ["_$1", "_$1_$2"], lcfirst($word)));
}

function camelize($word) {
  return preg_replace('/(^|_)([a-z])/e', 'strtoupper("\\2")', $word);
}

function createPassword($length, $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?") {
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
}
function cleanHTML($html) {
    //------------------------------
    // removing all unwanted tags
    $html = preg_replace("#<!--(.|\s)*?-->#", "",$html);
    $html = preg_replace("'<style[^>]*?>.*?</style>'si", "",$html);
    $html = preg_replace("'<script[^>]*?>.*?</script>'si", "",$html);
    $html = preg_replace("#<(/)?(font|span|xml|style|link|script|meta|del|ins|[ovwxp]:\w+)[^>]*>#","",$html);

    // then run another pass over the html (twice), removing unwanted attributes
    $html = preg_replace("#<([^>]*)(class|lang|size|face|[ovwxp]:\w+)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>#","<\\1>",$html);
    $html = preg_replace("#<([^>]*)(class|lang|size|face|[ovwxp]:\w+)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>#","<\\1>",$html);

    // change macintosh chars to html chars
    $html = str_replace(array('�', '�'), '"', $html);
    $html = str_replace(array('�', "'"), '"', $html);
    $html = str_replace(' � ', ' - ', $html);

    // return clean HTML
    return $html;
}//end function
function timeDifference($date1, $date2) {
    $date1 = is_int($date1) ? $date1 : strtotime($date1);
    $date2 = is_int($date2) ? $date2 : strtotime($date2);

    if (($date1 !== false) && ($date2 !== false)) {
        if ($date2 >= $date1) {
            $diff = ($date2 - $date1);

            if ($days = intval((floor($diff / 86400))))
                $diff %= 86400;
            if ($hours = intval((floor($diff / 3600))))
                $diff %= 3600;
            if ($minutes = intval((floor($diff / 60))))
                $diff %= 60;

            return array($days, $hours, $minutes, intval($diff));
        }
    }

    return false;
}

function exportTableToCsv($table, $filename = 'export.csv') {
    $csv_terminated = "\n";
    $csv_separator = ",";
    $csv_enclosed = '"';
    $csv_escaped = "\\";
    $sql_query = "select * from $table";

    // Gets the data from the database
    $db_link = mysqli_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME); // CONECT TO THE DATABASE
    $result = mysqli_query($db_link, $sql_query); // SEND QUERY TO THE DATABASE
    $fields_cnt = mysqli_field_count($db_link); // GET COLUMN COUNT OF DATABASE TABLE

    $schema_insert = '';

    // LOOP THROUGH COLUMN NAMES AND ADD THEM TO THE CSV DOC
    for ($i = 0; $i < $fields_cnt; $i++) {
        $columnInfo = mysqli_fetch_field_direct($result, $i); // GET COLUMN INFO
        $fieldName = $columnInfo->name; // GET COLUMN NAME
        $nameCharCount = strpos($fieldName, "_"); // FIND FIRST OCCURANCE OF _ IN COLUMN NAME
        if($nameCharCount < 4) {
            if(($pos = strpos($fieldName, '_')) !== false) {
                $columnName = substr($fieldName, $pos + 1);
            } else {
                $columnName = $fieldName;
            }
        }
        else {
            $columnName = $fieldName;
        }
        // CLEAN UP COLUMN NAME
        $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
            stripslashes($columnName)) . $csv_enclosed;
        $schema_insert .= $l;
        $schema_insert .= $csv_separator;
    } // end for

    $out = trim(substr($schema_insert, 0, -1));
    $out .= $csv_terminated;

    // Format the data
    while ($row = mysqli_fetch_array($result)) {
        $schema_insert = '';
        for ($j = 0; $j < $fields_cnt; $j++) {
            if ($row[$j] == '0' || $row[$j] != '') {
                if ($csv_enclosed == '') {
                    $schema_insert .= $row[$j];
                }
                else {
                    $schema_insert .= $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row[$j]) . $csv_enclosed;
                }
            }
            else {
                $schema_insert .= '';
            }

            if ($j < $fields_cnt - 1) {
                $schema_insert .= $csv_separator;
            }
        } // end for

        $out .= str_replace(array('"', '&#039;'), "", htmlspecialchars_decode($schema_insert));
        $out .= $csv_terminated;
    } // end while

    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Length: " . strlen($out));
    // Output to browser with appropriate mime type, you choose <img src="http://thetechnofreak.com/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
    header("Content-type: text/x-csv");
    //header("Content-type: application/csv");
    header("Content-Disposition: attachment; filename=$filename");
    echo $out;
    exit;
}

function exportAuditTableToCsv($table, $user="", $filename = 'export.csv') {
    $csv_terminated = "\n";
    $csv_separator = ",";
    $csv_enclosed = '"';
    $csv_escaped = "\\";

    if($user != "") {
        $sql_query = "SELECT * FROM $table WHERE user = '" . Database::quote_smart($user) . "'";
    } else {
        $sql_query = "SELECT * FROM $table";
    }

    // Gets the data from the database
    $db_link = mysqli_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME); // CONECT TO THE DATABASE
    $result = mysqli_query($db_link, $sql_query); // SEND QUERY TO THE DATABASE
    $fields_cnt = mysqli_field_count($db_link); // GET COLUMN COUNT OF DATABASE TABLE

    $schema_insert = '';

    // LOOP THROUGH COLUMN NAMES AND ADD THEM TO THE CSV DOC
    for ($i = 0; $i < $fields_cnt; $i++) {
        $columnInfo = mysqli_fetch_field_direct($result, $i); // GET COLUMN INFO
        $fieldName = $columnInfo->name; // GET COLUMN NAME
        $nameCharCount = strpos($fieldName, "_"); // FIND FIRST OCCURANCE OF _ IN COLUMN NAME
        if($nameCharCount < 4) {
            if(($pos = strpos($fieldName, '_')) !== false) {
                $columnName = substr($fieldName, $pos + 1);
            } else {
                $columnName = $fieldName;
            }
        }
        else {
            $columnName = $fieldName;
        }
        // CLEAN UP COLUMN NAME
        $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
            stripslashes($columnName)) . $csv_enclosed;
        $schema_insert .= $l;
        $schema_insert .= $csv_separator;
    } // end for

    $out = trim(substr($schema_insert, 0, -1));
    $out .= $csv_terminated;

    // Format the data
    while ($row = mysqli_fetch_array($result)) {
        $schema_insert = '';
        for ($j = 0; $j < $fields_cnt; $j++) {
            if ($row[$j] == '0' || $row[$j] != '') {
                if ($csv_enclosed == '') {
                    $schema_insert .= $row[$j];
                }
                else {
                    $schema_insert .= $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row[$j]) . $csv_enclosed;
                }
            }
            else {
                $schema_insert .= '';
            }

            if ($j < $fields_cnt - 1) {
                $schema_insert .= $csv_separator;
            }
        } // end for

        $out .= str_replace(array('"', '&#039;'), "", htmlspecialchars_decode($schema_insert));
        $out .= $csv_terminated;
    } // end while

    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Length: " . strlen($out));
    // Output to browser with appropriate mime type, you choose <img src="http://thetechnofreak.com/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
    header("Content-type: text/x-csv");
    //header("Content-type: application/csv");
    header("Content-Disposition: attachment; filename=$filename");
    echo $out;
    exit;
}

function exportContactFormTableToCsv($table, $formNum, $filename = 'exportContacts.csv')
{
    //echo 'Here is the Number: ' . $formNum . '. HORRAY!!!!!!!!!!!!';
    //Die($formNum);

    $csv_terminated = "\n";
    $csv_separator = ",";
    $csv_enclosed = '"';
    $csv_escaped = "\\";
    $sql_query = "SELECT fs_id AS RecordID, fs_date_submitted AS SubmitDate,
    (
    SELECT fe_value FROM tbl_form_entry WHERE fe_name = 'name' AND fs_id = RecordID
    ) AS Name,
    (
    SELECT fe_value FROM tbl_form_entry WHERE fe_name = 'phone' AND fs_id = RecordID
    ) AS Phone,
    (
    SELECT fe_value FROM tbl_form_entry WHERE fe_name = 'email' AND fs_id = RecordID
    ) AS Email,
    (
    SELECT fe_value FROM tbl_form_entry WHERE fe_name = 'address' AND fs_id = RecordID
    ) AS Address,
    (
    SELECT fe_value FROM tbl_form_entry WHERE fe_name = 'city' AND fs_id = RecordID
    ) AS City,
    (
    SELECT fe_value FROM tbl_form_entry WHERE fe_name = 'state' AND fs_id = RecordID
    ) AS State,
    (
    SELECT fe_value FROM tbl_form_entry WHERE fe_name = 'zip' AND fs_id = RecordID
    ) AS Zip,
    (
    SELECT fe_value FROM tbl_form_entry WHERE fe_name = 'reason' AND fs_id = RecordID
    ) AS Reason,
    (
    SELECT fe_value FROM tbl_form_entry WHERE fe_name = 'referral' AND fs_id = RecordID
    ) AS Referral,
    (
    SELECT fe_value FROM tbl_form_entry WHERE fe_name = 'comments' AND fs_id = RecordID
    ) AS Comments

    FROM tbl_form_submission WHERE cf_id = $formNum ORDER BY SubmitDate DESC";

    // Gets the data from the database
    $result = mysql_query($sql_query);
    $fields_cnt = mysql_num_fields($result);

    $schema_insert = '';

    for ($i = 0; $i < $fields_cnt; $i++)
    {
        $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
            stripslashes(mysql_field_name($result, $i))) . $csv_enclosed;
        $schema_insert .= $l;
        $schema_insert .= $csv_separator;
    } // end for

    $out = $schema_insert;
    $out .= $csv_terminated;

    // Format the data
    $count = 0;
    while ($row = mysql_fetch_array($result))
    {
        $schema_insert = '';
        for ($j = 0; $j < $fields_cnt; $j++)
        {
            if ($row[$j] == '0' || $row[$j] != '')
            {
                if(($row[$j] == "Name" || $row[$j] == "Your Name") && $count > 0) {
                    $schema_insert .= $csv_terminated;
                }
                if ($csv_enclosed == '')
                {
                    $schema_insert .= trim(preg_replace('/\s\s+/', ' ', $row[$j])) ;
                } else
                {
                    $schema_insert .= $csv_enclosed .
                    str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, str_replace("<br />", " ", str_replace(",", " ", trim(preg_replace('/\s\s+/', ' ', $row[$j]))))) . $csv_enclosed;
                }
            } else
            {
                $schema_insert .= '';
            }

            if ($j < $fields_cnt - 1)
            {
                $schema_insert .= $csv_separator;
            }
        } // end for
        $count++;
        $out .= str_replace(array('"', '&#039;'), "", htmlspecialchars_decode($schema_insert));
        $out .= $csv_terminated;
    } // end while
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Length: " . strlen($out));
    // Output to browser with appropriate mime type, you choose <img src="http://thetechnofreak.com/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
    header("Content-type: text/x-csv");
    //header("Content-type: text/csv");
    //header("Content-type: application/csv");
    header("Content-Disposition: attachment; filename=$filename");
    echo $out;
    exit;

}

function generateRandStr($length) {
    //------------------------------
    $randstr = "";

    for($i=0; $i<$length; $i++){
        $randnum = mt_rand(0,61);
        if($randnum < 10){
            $randstr .= chr($randnum+48);
        }else if($randnum < 36){
            $randstr .= chr($randnum+55);
        }else{
            $randstr .= chr($randnum+61);
        }
    }

    return $randstr;
}

function format_uri( $string, $separator = '-', $maxChars = 100)
{
    $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
    $remove = array('&', '+', '@', '%', "'");
    $string = str_replace($remove, '', $string);
    $string = mb_strtolower( trim( $string ), 'UTF-8' );
    $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
    $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
    $string = preg_replace("/[$separator]+/u", "$separator", $string);
    return substr($string, 0, $maxChars);
}

function format_catalog_uri( $string, $separator = '-', $maxChars = 100) {
    $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
    $remove = array('trade;','reg;', '#169;', 'copy;', '&amp;', '+', '@', '%', "'");
    $string = str_replace($remove, '', $string);
    $string = str_replace('&', '', $string);
    $string = str_replace('amp;', '', $string);
    $string = mb_strtolower( trim( $string ), 'UTF-8' );
    $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
    $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
    $string = preg_replace("/[$separator]+/u", "$separator", $string);
    return substr($string, 0, $maxChars);
}

//////////////////////////// FORMATTING FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\

function formatEmailLink($emailAddress, array $params = array()) {
    $parameters = antispambot($emailAddress);
    $count = 0;
    foreach($params as $key => $value) {
        $delimitor = $count == 0 ? '?' : '&amp;';
        $parameters .= $delimitor . $key . '=' . antispambot($value);
        $count++;
    }
    return '<a class="emailLink" href="mailto: ' . $parameters . '">' . antispambot($emailAddress) . '</a>';
}
function antispambot($email) {
    $p = str_split(trim($email));
    $new_mail = '';
    foreach ($p as $val) {
        $new_mail .= '&#'.ord($val).';';
    }
    return $new_mail;
}

//////////////////////////// ENCRYPTION FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\

function DES_Encrypt($buffer, $key, $crypt, $alg = 'rijndael-256') {
    // Parameters:
    if($crypt) {
        $text = $buffer;
    }
    else {
        $text = base64_decode($buffer);
    }

    $encrypted_data="";

    switch($alg) {
        case "3des":
            $td = mcrypt_module_open('tripledes', '', 'ecb', '');
            break;
        case "cast-128":
            $td = mcrypt_module_open('cast-128', '', 'ecb', '');
            break;
        case "gost":
            $td = mcrypt_module_open('gost', '', 'ecb', '');
            break;
        case "rijndael-128":
            $td = mcrypt_module_open('rijndael-128', '', 'ecb', '');
            break;
        case "twofish":
            $td = mcrypt_module_open('twofish', '', 'ecb', '');
            break;
        case "arcfour":
            $td = mcrypt_module_open('arcfour', '', 'ecb', '');
            break;
        case "cast-256":
            $td = mcrypt_module_open('cast-256', '', 'ecb', '');
            break;
        case "loki97":
            $td = mcrypt_module_open('loki97', '', 'ecb', '');
            break;
        case "rijndael-192":
            $td = mcrypt_module_open('rijndael-192', '', 'ecb', '');
            break;
        case "saferplus":
            $td = mcrypt_module_open('saferplus', '', 'ecb', '');
            break;
        case "wake":
            $td = mcrypt_module_open('wake', '', 'ecb', '');
            break;
        case "blowfish-compat":
            $td = mcrypt_module_open('blowfish-compat', '', 'ecb', '');
            break;
        case "des":
            $td = mcrypt_module_open('des', '', 'ecb', '');
            break;
        case "rijndael-256":
            $td = mcrypt_module_open('rijndael-256', '', 'ecb', '');
            break;
        case "xtea":
            $td = mcrypt_module_open('xtea', '', 'ecb', '');
            break;
        case "enigma":
            $td = mcrypt_module_open('enigma', '', 'ecb', '');
            break;
        case "rc2":
            $td = mcrypt_module_open('rc2', '', 'ecb', '');
            break;
        default:
            $td = mcrypt_module_open('tripledes', '', 'ecb', '');
            break;
    }

    $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    $key = substr($key, 0, mcrypt_enc_get_key_size($td));
    mcrypt_generic_init($td, $key, $iv);

    if($crypt) {   // ENCRYPT
        $encrypted_data = mcrypt_generic($td, $text);
    }
    else { // DECRYPT
        $encrypted_data = mdecrypt_generic($td, $text);
    }

    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);

    if($crypt) {
        return base64_encode($encrypted_data);
    }
    else {
        $encrypted_data = str_replace("\0", "", $encrypted_data);
        return $encrypted_data;
    }

}

////////////////////////////     IMAGE EDITING FUNCTIONS     \\\\\\\\\\\\\\\\\\\\\\\\\\\

function getHeight($image) {
    if(file_exists($image)) {
        $size	= getimagesize($image);
        $height	= $size[1];
    } else {
        $height = 0;
    }
    return $height;
}
function getWidth($image) {
    if(file_exists($image)) {
        $size	= getimagesize($image);
        $width	= $size[0];
    } else {
        $width = 0;
    }
    return $width;
}
function getProportionalWidth($width, $height, $newHeight) {
    return (int)ceil((double)$newHeight * $width / $height);
}
function getProportionalHeight($width, $height, $newWidth) {
    return (int)ceil((double)$newWidth * $height / $width);
}
function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale) {

    list($imagewidth, $imageheight, $imageType) = getimagesize($image);
    $imageType		= image_type_to_mime_type($imageType);
    $newImageWidth	= ceil($width * $scale);
    $newImageHeight	= ceil($height * $scale);
    $newImage		= imagecreatetruecolor($newImageWidth, $newImageHeight);

    switch($imageType) {
        case "image/gif":
            $source=imagecreatefromgif($image);
            break;
        case "image/pjpeg":
        case "image/jpeg":
        case "image/jpg":
            $source=imagecreatefromjpeg($image);
            break;
        case "image/png":
        case "image/x-png":
        $source=imagecreatefrompng($image);
        break;
    }

    imagecopyresampled($newImage, $source, 0, 0, $start_width, $start_height, $newImageWidth, $newImageHeight, $width, $height);

    switch($imageType) {
        case "image/gif":
            imagegif($newImage, $thumb_image_name);
            break;
        case "image/pjpeg":
        case "image/jpeg":
        case "image/jpg":
            imagejpeg($newImage, $thumb_image_name, 90);
            break;
        case "image/png":
        case "image/x-png":
            imagepng($newImage, $thumb_image_name);
            break;
    }

    chmod($thumb_image_name, 0777);
    return $thumb_image_name;
}
function ModifierContrast($hexcolor, $dark = '#000000', $light = '#FFFFFF') {

    return (hexdec($hexcolor) > 0xffffff/2) ? $dark : $light;
}
function HexToRGB($hex) {

    $hex = preg_replace("#", "", $hex);
    $color = array();
    if(strlen($hex) == 3) {
        $color['r'] = hexdec(substr($hex, 0, 1) . $r);
        $color['g'] = hexdec(substr($hex, 1, 1) . $g);
        $color['b'] = hexdec(substr($hex, 2, 1) . $b);
    } else if(strlen($hex) == 6) {
        $color['r'] = hexdec(substr($hex, 0, 2));
        $color['g'] = hexdec(substr($hex, 2, 2));
        $color['b'] = hexdec(substr($hex, 4, 2));
    }
    return $color;
}
function RGBToHex($r, $g, $b) {

    //String padding bug found and the solution put forth by Pete Williams (http://snipplr.com/users/PeteW)
    $hex = "#";
    $hex.= str_pad(dechex($r), 2, "0", STR_PAD_LEFT);
    $hex.= str_pad(dechex($g), 2, "0", STR_PAD_LEFT);
    $hex.= str_pad(dechex($b), 2, "0", STR_PAD_LEFT);
    return $hex;
}
function resizeImageCanvas($output_w, $output_h, $orig_filename, $new_filename = null, array $canvasRGB = array("R" => 255, "G"  => 255, "B" => 255))
{
    if(!file_exists($orig_filename)) {
        throw new exception("File not found or unable to open file -- \"$orig_filename\"");
    }
    if(is_null($new_filename)) {
        $new_filename = $orig_filename;
    }
    list($orig_w, $orig_h) = getimagesize($orig_filename);
    $orig_img = imagecreatefromstring(file_get_contents($orig_filename));
    $scale = $orig_h > $orig_w ? $output_h/$orig_h : $output_w/$orig_w;
    $new_w =  $orig_w * $scale;
    $new_h =  $orig_h * $scale;
    $offest_x = ($output_w - $new_w) / 2;
    $offest_y = ($output_h - $new_h) / 2;
    $new_img = imagecreatetruecolor($output_w, $output_h);
    $bgcolor = imagecolorallocate($new_img, $canvasRGB['R'], $canvasRGB['G'], $canvasRGB['B']);
    imagefill($new_img, 0, 0, $bgcolor);
    imagecopyresampled($new_img, $orig_img, $offest_x, $offest_y, 0, 0, $new_w, $new_h, $orig_w, $orig_h);
    imagepng($new_img, $new_filename, 9);
    imagedestroy($new_img);
}
function SanitizeData($str) {
    if (is_array($str)) {
        return array_map('_clean', $str);
    } else {
        $str = strip_tags($str);
        return str_replace('\\', '\\\\', trim(htmlspecialchars((get_magic_quotes_gpc() ? stripslashes($str) : $str), ENT_QUOTES)));
    }
}
function GetImage($Radio, $Field, $Val) {
    $arImage = array("checkCr_red.gif", "checkCr_empty.gif", "checkBx_red.gif", "checkBx_empty.gif");
    $indx = $Radio ? 0 : 2;

    if (is_array($Field)) {
        $indx = $indx + (in_array($Val, $Field) ? 0 : 1);
    } else {
        $indx = $indx + (strcasecmp($Field, $Val) == 0 ? 0 : 1);
    }

    return '<img src="http://' . $_SERVER['HTTP_HOST'] . '/scripts/' . $arImage[$indx] . '" />';
}
function compress_image($source_url, $destination_url, $quality) {
    $info = getimagesize($source_url);

    if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
    elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
    elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

    //save file
    imagejpeg($image, $destination_url, $quality);

    //return destination file
    return $destination_url;
}

//////////////////////////////////// SOCIAL FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

function getVideoID($ytURL) {
    if(strpos($ytURL, "youtu.be") > 0) {
        return substr($ytURL, -11);
    }
    $idStarts = strpos($ytURL, "?v=");
    if ($idStarts === FALSE) {
        $idStarts = strpos($ytURL, "&v=");
    }
    if ($idStarts === FALSE) {
        return false;
    }

    $idStarts +=3;

    $ytvIDlen = strpos($ytURL, "&", $idStarts);
    if ($ytvIDlen === FALSE) {
        $ytvIDlen = 11; // This is the default length of YouTube's video IDs
    } else {
        $ytvIDlen = $ytvIDlen - $idStarts;
    }

    $ytvID = substr($ytURL, $idStarts, $ytvIDlen);
    return $ytvID;
}

function getVideoInfo($videoId) {
    $feedURL = 'https://www.googleapis.com/youtube/v3/videos?id=' . $videoId . '&key=' . $apiKey . '&part=snippet,contentDetails,statistics&fields=*';
    // read feed into SimpleXML object
    $entry = @simplexml_load_file($feedURL);

    if (!$entry || empty($entry) || $entry === FALSE) {
        return NULL;
    }

    // parse video entry
    $video = ParseVideoEntry($entry);
    //These variables include the video information
    $video_info['title'] = (stripslashes($video->title));
    $video_info['description'] = (stripslashes($video->description));
    $video_info['thumbnail'] = 'http://i.ytimg.com/vi/' . $videoId . '/1.jpg';
    $video_info['author'] = (stripslashes($video->author));
    $video_info['watchURL'] = (stripslashes($video->watchURL));
    $video_info['length'] = (stripslashes($video->length));
    $video_info['viewCount'] = (stripslashes($video->viewCount));
    $video_info['rating'] = (stripslashes($video->rating));
    // $video_info['commentsCount'] = (stripslashes($video->commentsCount));
    $video_info['authorURL'] = 'http://www.youtube.com/'.$video_info['author'];//(stripslashes($video->authorURL));
    $mins = floor ($video_info['length'] / 60);
    $secs = $video_info['length'] % 60;
    $video_info['time'] = sprintf('%d:%2.0f', $mins, $secs);

    return $video_info;
}//end function

function ParseVideoEntry($entry) {
    $obj= new stdClass;

    // get nodes in media: namespace for media information
    $media = $entry->children('http://search.yahoo.com/mrss/');
    $obj->title = $media->group->title;
    $obj->description = $media->group->description;

    // get video player URL
    $attrs = $media->group->player->attributes();
    $obj->watchURL = $attrs['url'];

    // get video thumbnail
    $attrs = $media->group->thumbnail[0]->attributes();
    $obj->thumbnailURL = $attrs['url'];

    // get <YT:DURATION> node for video length
    $yt = $media->children('http://gdata.youtube.com/schemas/2007');
    $attrs = $yt->duration->attributes();
    $obj->length = $attrs['seconds'];

    // get <YT:STATS> node for viewer statistics
    $yt = $entry->children('http://gdata.youtube.com/schemas/2007');
    if ($yt->statistics) {
        $attrs = $yt->statistics->attributes();
        $obj->viewCount = $attrs['viewCount'];
    } else {
        $obj->viewCount = 0;
    }

    // get <GD:RATING> node for video ratings
    $gd = $entry->children('http://schemas.google.com/g/2005');
    if ($gd->rating) {
        $attrs = $gd->rating->attributes();
        $obj->rating = $attrs['average'];
    } else {
        $obj->rating = 0;
    }

    // get <GD:COMMENTS> node for video comments
    $gd = $entry->children('http://schemas.google.com/g/2005');
    if ($gd->comments->feedLink) {
        $attrs = $gd->comments->feedLink->attributes();
        $obj->commentsURL = $attrs['href'];
        $obj->commentsCount = $attrs['countHint'];
    }

    //Get the author
    $obj->author = $entry->author->name;
    $obj->authorURL = $entry->author->uri;

    // get feed URL for video responses
    $entry->registerXPathNamespace('feed', 'http://www.w3.org/2005/Atom');
    $nodeset = $entry->xpath("feed:link[@rel='http://gdata.youtube.com/schemas/2007#video.responses']");
    if (count($nodeset) > 0) {
        $obj->responsesURL = $nodeset[0]['href'];
    }

    // get feed URL for related videos
    $entry->registerXPathNamespace('feed', 'http://www.w3.org/2005/Atom');
    $nodeset = $entry->xpath("feed:link[@rel='http://gdata.youtube.com/schemas/2007#video.related']");
    if (count($nodeset) > 0) {
        $obj->relatedURL = $nodeset[0]['href'];
    }

    // return object to caller
    return $obj;
}//end function

function getWPPosts($feedUrl, $limit = 5) {
    $posts = array();
    $xml = simplexml_load_string(curl_get_contents($feedUrl));
    $count = 1;
    foreach($xml->channel->item as $post) {
        if($count <= $limit) {
            array_push($posts, $post);
            $count++;
        } else {
            break;
        }
    }
    return $posts;
}

function remove_utf8_bom($text) {
    $bom = pack('H*','EFBBBF');
    $text = preg_replace("/^$bom/", '', $text);
    return $text;
}

function utf8ize( $mixed ) {
    if (is_array($mixed)) {
        foreach ($mixed as $key => $value) {
            $mixed[$key] = utf8ize($value);
        }
    } elseif (is_string($mixed)) {
        return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
    }
    return $mixed;
}
/*function getUserTweets($userName, $limit = 5) {
$feedUrl = 'https://api.twitter.com/1/statuses/user_timeline.rss?screen_name=' . $userName;
$tweets = array();
$xml = simplexml_load_string(curl_get_contents($feedUrl));
$count = 1;
foreach($xml->channel->item as $twit) {
if($count <= $limit) {
$twit->title = str_replace($userName . ': ', '', $twit->title);
array_push($tweets, $twit);
$count++;
} else {
break;
}
}
return $tweets;
}*/

// To GET TWITTER STUFF - https://dev.twitter.com/oauth/tools/signature-generator/6988230
function getUserTweets($oauth_access_token, $oauth_access_token_secret, $consumer_key, $consumer_secret, $limit = 2) {
    $tweets = array();
    $url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
    $oauth_version = "1.0";
    $oauth_signature_method = "HMAC-SHA1";

    $baseString = array( 'oauth_consumer_key' => $consumer_key,
        'oauth_nonce' => time(),
        'oauth_signature_method' => $oauth_signature_method,
        'oauth_token' => $oauth_access_token,
        'oauth_timestamp' => time(),
        'oauth_version' => $oauth_version);

    $oauth = array( 'oauth_consumer_key' => $consumer_key,
        'oauth_nonce' => time(),
        'oauth_signature_method' => $oauth_signature_method,
        'oauth_token' => $oauth_access_token,
        'oauth_timestamp' => time(),
        'oauth_version' => $oauth_version);

    $base_info = buildBaseString($url, 'GET', $baseString);
    $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
    $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
    $oauth['oauth_signature'] = $oauth_signature;

    $header = array(buildAuthorizationHeader($oauth), 'Expect:');
    $options = array( CURLOPT_HTTPHEADER => $header,
        CURLOPT_HEADER => false,
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false);

    $feed = curl_init();
    curl_setopt_array($feed, $options);
    $json = curl_exec($feed);
    curl_close($feed);
    $twitter_data = json_decode($json);
    $count = 1;
    foreach($twitter_data as $tweet) {
        if($count <= $limit) {
            array_push($tweets, $tweet->text);
            $count++;
        } else {
            break;
        }
    }

    return $tweets;
}

function buildBaseString($baseURI, $method, $params) {
    $r = array();
    ksort($params);
    foreach($params as $key=>$value){
        $r[] = "$key=" . rawurlencode($value);
    }

    return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r)); //return complete base string
}

function buildAuthorizationHeader($authHeaders, $authType = 'OAuth') {
    $r = "Authorization: $authType ";
    $values = array();
    foreach($authHeaders as $key=>$value)
        $values[] = "$key=\"" . rawurlencode($value) . "\"";

    $r .= implode(', ', $values);
    return $r;
}

function verify_gcaptcha($captchaSecret) {
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $post_data = http_build_query(
        array(
            'secret' => $captchaSecret,
            'response' => $_POST['g-recaptcha-response'],
            'remoteip' => $_SERVER['REMOTE_ADDR']
        )
    );
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $post_data
        )
    );
    $context  = stream_context_create($opts);
    $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    $result = json_decode($response);
    if (!$result->success) {
        return false;
    } else {
        return true;
    }
}

//////////////////////////////////// FRAMEWORK FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

/* Reverse Captcha Functions */
function validateRC() {
    global $_WEBCONFIG;
    return true;
}
function loadRC() {
    global $_WEBCONFIG;
    $_SESSION['RC_CODE'] = isset($_SESSION['RC_CODE']) ? $_SESSION['RC_CODE'] : md5(uniqid(time()));
    $randomKey = rand(0,10000);
    print '<input id="RC_Validate' . $randomKey . '" name="RC_Validate1" type="hidden" value="'. $_SESSION['RC_CODE'] .'" aria-label="Remote Captcha ' . $randomKey . '" aria-hidden="true" />';
    $randomKey = rand(0,10000);
    print '<input class="none" id="RC_Validate' . $randomKey . '" name="RC_Validate2" type="text" value="' . $_WEBCONFIG['FORM_SECURE_CODE'] .'" aria-label="Remote Captcha ' . $randomKey . '" aria-hidden="true" />';
    $randomKey = rand(0,10000);
    print '<input class="none" id="RC_Validate' . $randomKey . '" name="RC_Validate3" type="text" value="" aria-label="Remote Captcha ' . $randomKey . '" aria-hidden="true" />';
    $randomKey = rand(0,10000);
    print '<div class="none"><input id="RC_Validate' . $randomKey . '" name="RC_Validate4" type="email" value="" aria-label="Remote Captcha ' . $randomKey . '" /></div>';
}
function loadLocalConfig($configPath) {
    global $_WEBCONFIG;
    if (is_readable($configPath) && file_exists($configPath . '/web.config')) {
        $configXML = simplexml_load_file($configPath . '/web.config');
        if(isset($configXML->appSettings)) {
            foreach ($configXML->appSettings->add as $setting) {
                $_WEBCONFIG[(string)$setting->attributes()->key] = (string)$setting->attributes()->value;
            }
        }
    } else {
        throw new Exception("Unable to locate web.config in $configPath");
    }
}
function getWebConfigSettings($configPath) {
    $config = array();
    if (is_readable($configPath) && file_exists($configPath . '/web.config')) {
        $configXML = simplexml_load_file($configPath . '/web.config');
        if(isset($configXML->appSettings)) {
            foreach ($configXML->appSettings->add as $setting) {
                $config[(string)$setting->attributes()->key] = (string)$setting->attributes()->value;
            }
        }
    } else {
        throw new Exception("Unable to locate web.config in $configPath");
    }
    return $config;
}

//------------------------------
function isPartUppercase($string) {
    //------------------------------
    return (bool) preg_match('/[A-Z]/', $string);
}

//------------------------------
function isPartLowercase($string) {
    //------------------------------
    return (bool) preg_match('/[a-z]/', $string);
}

//------------------------------
function isPartNumber($string) {
    //------------------------------
    return (bool) preg_match('/[0-9]/', $string);
}

function printSubMenu($menuId) {
                // Sub Navigational Menu
                $sql = "SELECT cms_id, cms_title, cms_sub_title, cms_dynamic_link, cms_show_disclaimer, cms_dynamic_link_target FROM tbl_cms
                WHERE cms_is_historic = 0 and cms_is_deleted = 0 and cms_is_active = '1' and  cms_show_nav = '1' AND cms_parent_id = '" . $menuId . "'
                ORDER BY cms_display_order;";
                $subMenuItems = Database::Execute($sql);

                if ($subMenuItems->Count() > 0) {
                    print '<ul class="submenu subMenu' . $menuId . '">';

                    while ($subMenuItems->MoveNext()) {
                        $subNavLink = "javascript:return(false);";
                        $subNavLink = $subMenuItems->cms_dynamic_link;

                        if($subMenuItems->cms_show_disclaimer == 1) {
                            if(strlen($subMenuItems->cms_sub_title) > 0) {
                                printf("<li class=\"menuSubItem{$subMenuItems->cms_id}\"><a href=\"%s\" class=\"%s\" target=\"%s\" title=\"%s\">%s</a>", $subNavLink, "headerSubNav" . $subMenuItems->cms_id, $subMenuItems->cms_dynamic_link_target, $subMenuItems->cms_sub_title, $subMenuItems->cms_title);
                            }
                            else {
                                printf("<li class=\"menuSubItem{$subMenuItems->cms_id}\"><a href=\"%s\" class=\"%s\" target=\"%s\" title=\"%s\">%s</a>", $subNavLink, "headerSubNav" . $subMenuItems->cms_id, $subMenuItems->cms_dynamic_link_target, $subMenuItems->cms_title, $subMenuItems->cms_title);
                            }

                            printSubMenu($subMenuItems->cms_id);
                            print "</li>" . PHP_EOL;
                        }
                        else {
                            if(strlen($subMenuItems->cms_sub_title) > 0) {
                                printf("<li class=\"menuSubItem{$subMenuItems->cms_id}\"><a data-nodisclaimer=\"true\" href=\"%s\" class=\"%s\" target=\"%s\" title=\"%s\">%s</a>", $subNavLink, "headerSubNav" . $subMenuItems->cms_id, $subMenuItems->cms_dynamic_link_target, $subMenuItems->cms_sub_title, $subMenuItems->cms_title);
                            }
                            else {
                                printf("<li class=\"menuSubItem{$subMenuItems->cms_id}\"><a data-nodisclaimer=\"true\" href=\"%s\" class=\"%s\" target=\"%s\" title=\"%s\">%s</a>", $subNavLink, "headerSubNav" . $subMenuItems->cms_id, $subMenuItems->cms_dynamic_link_target, $subMenuItems->cms_title, $subMenuItems->cms_title);
                            }

                            printSubMenu($subMenuItems->cms_id);
                            print "</li>" . PHP_EOL;
                        }
                    }//end while

                    print "</ul>\n";
                }
            }//end function

if($_WEBCONFIG['SITE_TYPE'] == 'ECOMMERCE' || $_WEBCONFIG['SITE_TYPE'] == 'CATALOG') {
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //                                                         E-COMMERCE FUNCTIONS
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //------------------------------
    function buildSpecNameOptions() {
        //------------------------------
        $sql = "SELECT SN.`sn_id` AS id, `sn_name` AS name
        FROM `tbl_ecommerce_spec_names` AS SN
        ORDER BY SN.`sn_name`
        LIMIT 1000;";
        $data = Database::Execute($sql);
        $list = "";

        while ($data->MoveNext()) {

            $id   = $data->id;
            $name = $data->name;
            $list .= "<option value=\"$id\"";
            /* if ($id == $catId) {
            $list.= " selected";
            } */
            $list .= ">$name</option>";
        }
        return $list;

    }

    //-------------------------------------------------------------------
    function buildPrimaryCategorySelectionTool($catId = 0, $ep_id = 0) {
        //-------------------------------------------------------------------
        $categories = array();
        $sql     = "SELECT EC.cat_id, CONCAT(IFNULL(CONCAT(ECP.cat_name, ' :: '), ''), EC.cat_name) AS cat_name
        FROM tbl_ecommerce_cats EC
        LEFT OUTER JOIN tbl_ecommerce_cats ECP
        ON EC.cat_parent_id = ECP.cat_id
        ORDER BY cat_name";
        //die($sql);
        $catData = Database::Execute($sql);

        while ($catData->MoveNext()) {
            $categories[$catData->cat_id] = array('name' => $catData->cat_name);
        }
        //die(print_r($categories));
        // build combo box options
        $list = '';

        foreach ($categories as $key => $value) {
            $name  = $value['name'];

            $list .= "<option value=\"$key\"";
            if ($key == $catId) {
                $list.= " selected";
            }
            $list .= ">$name</option>\r\n";
        }

        return $list;
    }

}
?>