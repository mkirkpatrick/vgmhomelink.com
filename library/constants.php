<?php
/* Set server variables and protect from XSS attacks */
if(!isset($_SERVER['REQUEST_URI'])) {
    $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'];
    if($_SERVER['QUERY_STRING']) {
        $_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
    }
}

if(!isset($_SERVER["HTTPS"])) {
    $_SERVER['HTTPS'] = "off";
}

$_SERVER['FULL_URI'] =  $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

if(!isset($_SERVER['ABSOLUTE_URI'])) {
    $_SERVER['ABSOLUTE_URI'] =  "https://" . $_SERVER['FULL_URI'];
}

if(!isset($_SERVER['ROOT_URI'])) {
    $_SERVER['ROOT_URI'] = "https://" . $_SERVER['HTTP_HOST'];
}

if(!isset($_SERVER['HTTP_CLIENT_IP'])) {
    if (getenv('HTTP_X_FORWARDED_FOR')) {
        $_SERVER['HTTP_CLIENT_IP'] = getenv('HTTP_X_FORWARDED_FOR');
    } elseif (getenv('HTTP_X_FORWARDED')) {
        $_SERVER['HTTP_CLIENT_IP'] = getenv('HTTP_X_FORWARDED');
    } elseif (getenv('HTTP_FORWARDED_FOR')) {
        $_SERVER['HTTP_CLIENT_IP'] = getenv('HTTP_FORWARDED_FOR');
    } elseif (getenv('HTTP_FORWARDED')) {
        $_SERVER['HTTP_CLIENT_IP'] = getenv('HTTP_FORWARDED');
    } else {
        $_SERVER['HTTP_CLIENT_IP'] = $_SERVER['REMOTE_ADDR'];
    }
}

$_SERVER['PHP_SELF'] = xss_sanitize($_SERVER['PHP_SELF']);
$_SERVER['SCRIPT_NAME'] = xss_sanitize($_SERVER['SCRIPT_NAME']);
$_SERVER['REQUEST_URI'] = xss_sanitize($_SERVER['REQUEST_URI']);
$_SERVER['FULL_URI'] = xss_sanitize($_SERVER['FULL_URI']);
$_SERVER['ABSOLUTE_URI'] = xss_sanitize($_SERVER['ABSOLUTE_URI']);
$_SERVER['ROOT_URI'] = xss_sanitize($_SERVER['ROOT_URI']);
$_SERVER['QUERY_STRING'] = xss_sanitize($_SERVER['QUERY_STRING']);
$_SERVER['REQUEST_PATH'] = xss_sanitize(strtok($_SERVER["REQUEST_URI"],'?'));
$_SERVER['HTTP_USER_AGENT'] = xss_sanitize($_SERVER['HTTP_USER_AGENT']);

$_SERVER['REQUEST_SCRIPT'] = '';
$_SERVER['REQUEST_SLUG_IDENTIFIER'] = '';
$_SERVER['REQUEST_SLUG_TITLE'] = '';

if (isset($_GET['clearoff'])) {
    if (isset($_SESSION['filter']))		unset($_SESSION['filter']);
    if (isset($_SESSION['New']))		unset($_SESSION['New']);
}
if (isset($_GET['clearoff2'])) {
    if (isset($_SESSION['filter']))		unset($_SESSION['filter']);
}

/* Global Constants */
define("URL_DIRECTORY_SEPERATOR", '/');

// Database Settings
define("DATABASE_TYPE", 1); // 1 = MYSQL, 2 = MSSQL, 3 = ODBC

if($_SERVER['HTTP_HOST'] == 'vgmhomelink.com' || $_SERVER['HTTP_HOST'] == 'www.vgmhomelink.com'){
    $connectionDB = 'MAINDB';
} else {
    $connectionDB = 'STAGINGDB';
}
if(isset($_WEBCONFIG['CONNECTIONS'][$connectionDB])) {
    define("DATABASE_HOST", $_WEBCONFIG['CONNECTIONS'][$connectionDB]['HOST']);
    define("DATABASE_NAME", $_WEBCONFIG['CONNECTIONS'][$connectionDB]['DATABASE']);
    define("DATABASE_USER", $_WEBCONFIG['CONNECTIONS'][$connectionDB]['USERNAME']);
    define("DATABASE_PASS", $_WEBCONFIG['CONNECTIONS'][$connectionDB]['PASSWORD']);
} else {
    die("Unexpected Error! Connecting String Not Defined in Web Config!");
}

// Site & Paths settings
$thisFile	= str_replace('\\', '/', dirname(__FILE__));
$curFile	= str_replace('\\', '/', getcwd());
$arThisFile	= explode('/', $thisFile);
$arCurFile	= explode('/', $curFile);
list($drive, $network, $site, $vpanel) = $arThisFile;
$arSSLFile	= array();

define("HTML_FORMATTING_TAGS", "<b><i><sup><sub><em><strong><u><br><span><p><blockquote><abbr>");

// HTML Constants
define("HTML_SPACE", "&nbsp;");
define("HTML_AMPERSTAND", "&amp;");
define("FORM_ENCRYPTION_KEY", "TRgDcvMTbFsXmUrbXMzrcKHwzbpqaHcy");

// User Roles
define("USER_MANAGER_ROLE", "1");
define("AUDIT_LOG_MANAGER_ROLE", "2");
define("CONTENT_AUTHOR_ROLE", "3");
define("CONTENT_PUBLISHER_ROLE", "4");
define("FORM_VIEWER_ROLE", "5");
define("SITE_MANAGER_ROLE", "6");
define("INTRANET_MANAGER_ROLE", "5");
define("DELETE_PERMISSION", "7");
?>