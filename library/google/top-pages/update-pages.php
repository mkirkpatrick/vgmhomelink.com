<?php 
$data = "";  
$cache_file = $_SERVER['DOCUMENT_ROOT'] . '/uploads/cache/google-top-pages/' . date('m-d-y',time()) . '.cache'; 
$cache_duration = 60 * 60 * 5;  // Run Every 5 Hours

if (!file_exists($cache_file) || (file_exists($cache_file) && (filemtime($cache_file) < (time() - $cache_duration )))) {

    ########## Google Settings.. Client ID, Client Secret #############
    $google_client_id             = ' 978185030980-s8l81kb583laj32r14j1bi166hs49r57.apps.googleusercontent.com ';
    $google_client_secret         = 'AN6xFCNAe6ejsusNiJtNnQAH';
    $google_redirect_url          = $_WEBCONFIG['SITE_URL'] . 'VPanel/library/google/top-pages/update-pages.php';
    $page_url_prefix              = $_WEBCONFIG['SITE_URL'];

    ########## Google analytics Settings.. #############
    $google_analytics_profile_id  = 'ga:' . $siteConfig['Google_Profile_Id']; //find the site profile id in google analystic
    $google_analytics_dimensions  = 'ga:landingPagePath,ga:pageTitle'; //no change needed (optional)
    $google_analytics_metrics     = 'ga:pageviews'; //no change needed (optional)
    $google_analytics_sort_by     = '-ga:pageviews'; //no change needed (optional)
    $google_analytics_max_results = '15'; //no change needed (optional)

    //include google api files
    require_once 'src/Google_Client.php';
    require_once 'src/contrib/Google_AnalyticsService.php';

    $gClient = new Google_Client();
    $gClient->setApplicationName('GA Top Pages Get');
    $gClient->setClientId($google_client_id);
    $gClient->setClientSecret($google_client_secret);
    $gClient->setRedirectUri($google_redirect_url);
    $gClient->setScopes(array('https://www.googleapis.com/auth/analytics.readonly'));
    $gClient->setUseObjects(true);


    //check for session variable
    if (isset($_SESSION["token"])) { 

        //set start date to previous 6 months
        $start_date = date("Y-m-d", strtotime("-6 month") ); 

        //end date as today
        $end_date = date("Y-m-d");

        try{
            //set access token
            $gClient->setAccessToken($_SESSION["token"]); 

            //create analytics services object
            $analyticsService = new Google_AnalyticsService($gClient); 

            //analytics parameters (check configuration file)
            $params = array('dimensions' => $google_analytics_dimensions,'sort' => $google_analytics_sort_by,'max-results' => $google_analytics_max_results);

            //get results from google analytics
            $results = $analyticsService->data_ga->get($google_analytics_profile_id,$start_date,$end_date, $google_analytics_metrics, $params);

            $rows = $results->rows;

            if(!empty($rows)) {
                //empty table
                Database::ExecuteRaw("DELETE FROM tbl_google_top_pages");

                // insert all new top pages in the table
                foreach($rows as $row) {
                    $sql = "INSERT INTO tbl_google_top_pages (page_uri, page_title, total_views) 
                            VALUES ('$row[0]','" . Database::quote_smart($row[1]) . "',$row[2]);";  
                    Database::ExecuteRaw($sql);    
                }

                $data =  date("m-d-Y g:i:s", time()) . ' - Top Pages Inserted';

            }
            else {
                $data =  date("m-d-Y g:i:s", time()) . ' - No Top Pages Returned';
            }        

        }
        catch(Exception $e){ //do we have an error?
            $data =  date("m-d-Y g:i:s", time()) . ' - ' . $e->getMessage(); //display error
        }    
    }
    else{
        //authenticate user
        if (isset($_GET['code'])) {        
            $gClient->authenticate();
            $token = $gClient->getAccessToken();
            $_SESSION["token"] = $token;
            header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
        }else{
            $gClient->authenticate();
        }
    }

    $log_file = $_SERVER['DOCUMENT_ROOT'] . '/uploads/logs/Google-Top-Pages.log';  
    if (file_exists($log_file)) {
        file_put_contents($log_file, $data . PHP_EOL, FILE_APPEND); 
    } 
    else {
        $myfile = fopen($log_file, "w");
        fwrite($myfile, $data . PHP_EOL);
        fclose($myfile);    
    }
 
    file_put_contents($cache_file, '1', LOCK_EX); 
 
}
else {
    $data =  'NOT CORRECT TIME TO RUN';
}