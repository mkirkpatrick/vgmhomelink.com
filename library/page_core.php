<?php
use Forbin\Library\Classes\Member;
use Forbin\Library\Classes\PageBuilder\Pagebase; 

// if it's a preview, get all the data from post

$isPreview = isset($_GET['preview']); 
if(!$isPreview) {
    if($iNum == 0) { 
        $iNum = getCmscIdbyUrl($_SERVER['REQUEST_PATH']); 
    }
    $extraParamers = "AND cms_is_historic = 0 AND cms_is_deleted = 0 AND cms_is_active = 1"; 
} else {
    header('X-XSS-Protection: 0');
    $extraParamers = ''; 
}
// background_image
$sql = "SELECT cms_id, cmst_templates_id, cms_is_secure, cms_show_breadcrumb, background_image 
        FROM tbl_cms 
        WHERE cms_id = $iNum $extraParamers
        LIMIT 1"; 
$cms = Database::Execute($sql);
if ($cms->Count() == 0) {
    if(stristr($_SERVER['REQUEST_URI'], 'VPanel')){
        redirect("/VPanel/404error.php?path=" . $_SERVER['REQUEST_URI'], "404 Not Found");    
    }
    else {
        serverTransfer($_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['404']);    
    }  
}

$cms->MoveNext();	
if($cms->cms_is_secure && !$isPreview) {
    member::_secureCheck();
    if(!member::_userHasPagePermissions($_SESSION['member_id'], $iNum)) { 
        serverTransfer($_WEBCONFIG['CUSTOMERRORS']['DEFAULTREDIRECT']['403']);
    }
}

// Get Template
$sql = "SELECT cmst_name 
        FROM tbl_cms_templates 
        WHERE cmst_id = {$cms->cmst_templates_id}
        LIMIT 1";
$template = Database::Execute($sql);
$template->MoveNext();

if ($template->Count() == 0) {
    throw new Exception("Template ID: " . $cms->cmst_templates_id . " Not Found"); 
}

$layout = new Pagebase;
$layout->load(filePathCombine($_SERVER['DOCUMENT_ROOT'], '/templates/', $template->cmst_name));

// Get Template Variables
$sql = "SELECT cmsv_name, cmsv_value FROM tbl_cms_name_value WHERE cms_id = $iNum";
$fields = Database::Execute($sql);

$assets = getAssets(); 

// if it's a preview, overwrite with post values
$aPreviewValues = Array();
if($isPreview){
    // for preview, put values layout may use in an array
    foreach($_POST as $k=>$v){		
        if (strpos($k, "fieldname_") === 0) {
            $fieldValue	= $_POST[$k];
            $postKey = substr($k, 10);
            $replaceValue = isset($_POST[$postKey]) ? $_POST[$postKey] : '';
            $aPreviewValues[$v] = strtr($replaceValue, $assets);	
        }		
    }
}

while ($fields->MoveNext()) {
    $tmplVars[$fields->cmsv_name] = strtr($fields->cmsv_value, $assets);
    $k=$fields->cmsv_name;
    $v=strtr($fields->cmsv_value, $assets);
    $v = str_replace(getInactiveAssets(),'', $v);
    // use preview value if needed
    if(!empty($aPreviewValues[$k])){ $v = $aPreviewValues[$k]; }

    $layout->replace($k, $v);
}// end while

$sBreadCrumbMarkup = "";
if ($cms->cms_show_breadcrumb != 0) {
    $sBreadCrumbMarkup = $cms->cms_id != $_WEBCONFIG['SITE_ROOT_PAGE'] ? '<a class="home" href="/">Home</a>' . cLand($cms->cms_id) : '';
}

$GLOBALS["pageHeader"]      = isset($tmplVars['Title']) && !isNullOrEmpty($tmplVars['Title']) ? $tmplVars['Title'] : ''; 
$GLOBALS["pageTitle"]       = isset($tmplVars['MetaTitle']) && !isNullOrEmpty($tmplVars['MetaTitle']) ? $tmplVars['MetaTitle'] : $siteConfig['Default_Page_Title'];
$GLOBALS["pageKeywords"]    = isset($tmplVars['MetaKeywords']) && !isNullOrEmpty($tmplVars['MetaKeywords']) ? $tmplVars['MetaKeywords'] : $siteConfig['Default_Page_Keywords'];
$GLOBALS["pageDescription"] = isset($tmplVars['MetaDescription']) && !isNullOrEmpty($tmplVars['MetaDescription']) ? $tmplVars['MetaDescription'] : $siteConfig['Default_Page_Description'];
$GLOBALS["pageBreadCrumbs"] = $sBreadCrumbMarkup;
?>