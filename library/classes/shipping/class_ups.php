<?php
require_once("class_httpClient.php");

class UPS {

	var $code;
	var $title;
	var $description;
	var $sort_order;
	var $tax_class;
	var $tax_basis;
	var $display;
	var $box_title;
	var $weight_title;
	var $rate;
	var $container;
	var $orig_city;
	var $orig_zip;
	var $orig_country;
	var $types;
	var $sendEmail = false;

	// --------------------------------------------
	function UPS($origCity, $origZip, $origCountry='US', $display=2, $rate='CC', $container='CP') {
	// --------------------------------------------
		$this->code			= 'ups';
		$this->title		= 'United Parcel Service';
		$this->description	= 'United Parcel Service';
		$this->sort_order	= 0;
		$this->tax_class	= '';
		$this->tax_basis	= 'Shipping';
		$this->box_title	= 'Boxes';
		$this->weight_title	= 'lbs';
		$this->display		= $display;
		$this->rate			= $rate;
		$this->container	= $container;
		$this->orig_city	= $origCity;
		$this->orig_zip		= $origZip;
		$this->orig_country	= $origCountry;

		$this->types = array('1DM' => 'Next Day Air Early AM',
						   '1DML' => 'Next Day Air Early AM Letter',
						   '1DA' => 'Next Day Air',
						   '1DAL' => 'Next Day Air Letter',
						   '1DAPI' => 'Next Day Air Intra (Puerto Rico)',
						   '1DP' => 'Next Day Air Saver',
						   '1DPL' => 'Next Day Air Saver Letter',
						   '2DM' => '2nd Day Air AM',
						   '2DML' => '2nd Day Air AM Letter',
						   '2DA' => '2nd Day Air',
						   '2DAL' => '2nd Day Air Letter',
						   '3DS' => '3 Day Select',
						   'GND' => 'Ground',
						   'GNDCOM' => 'Ground Commercial',
						   'GNDRES' => 'Ground Residential',
						   'STD' => 'Canada Standard',
						   'XPR' => 'Worldwide Express',
						   'XPRL' => 'worldwide Express Letter',
						   'XDM' => 'Worldwide Express Plus',
						   'XDML' => 'Worldwide Express Plus Letter',
						   'XPD' => 'Worldwide Expedited');
	}

	// --------------------------------------------
	function quote($destCity, $destZip, $destCountry, $weight, $boxes, $method='', $rescom='RES') {
	// --------------------------------------------

		if (($this->_notNull($method)) && (isset($this->types[$method]))) {
			$mode = $method;
		} else {
			$mode = 'GNDRES';
		}

		// return a single quote
		if ($method) $this->_upsAction('3');

		$this->_upsMethod($mode);
		$this->_upsOrigin($this->orig_city, $this->orig_zip, $this->orig_country);
		$this->_upsDest($destCity, $destZip, $destCountry);
		$this->_upsRate($this->rate);
		$this->_upsContainer($this->container);
		$this->_upsWeight($weight);
		$this->_upsRescom($rescom);

		// Get Quote
		$upsQuote	= $this->_upsGetQuote();
		$qsize		= sizeof($upsQuote);

		if ( (is_array($upsQuote)) && ($qsize > 0) ) {
			switch ($this->display) {
				case (0):
					$show_box_weight = '';
					break;
				case (1):
					$show_box_weight = " ($boxes $this->box_title)";
					break;
				case (2):
					$pounds = number_format($weight * $boxes, 2);
					$show_box_weight = " ($pounds $this->weight_title)";
					break;
				default:
					$pounds = number_format($weight, 2);
					$show_box_weight = " ($boxes x $pounds $this->weight_title)";
					break;
			}

			$this->quotes = array('id' => $this->code, 'module' => $this->title . $show_box_weight);

			$methods = array();

			// BOF: UPS USPS
			switch ($destCountry) {
				case 'US':  // United States
					$allowed_methods = array("1DA", "2DA", "3DS", "GND");
					break;

				case 'CA':  // Canada
					$allowed_methods = array("1DA", "2DA", "3DS", "GND", "STD");
					break;

				default:
					$allowed_methods = array("1DA", "2DA", "3DS", "GND", "XPR", "XDM", "XPD", "WXS");
					break;
			}
			$std_rcd = false;
			// EOF: UPS USPS

			for ($i=0; $i<$qsize; $i++) {
				list($type, $cost) = each($upsQuote[$i]);
				// BOF: UPS USPS
				if ($type=='STD') {
					if ($std_rcd) 
						continue;
					else 
						$std_rcd = true;
				};
				if (!in_array($type, $allowed_methods)) 
					continue;
				// EOF: UPS USPS

				$methods[] = array('id' => $type, 'title' => $this->types[$type], 'cost' => ($cost + 0) * $boxes);
			}// end for

			$this->quotes['methods'] = $methods;
		} else {
			// BOF: UPS USPS
			$this->quotes = array('module' => $this->title, 'error' => 'We are unable to obtain a rate quote for UPS shipping.<br />Please contact the store if no other alternative is shown.');
			// EOF: UPS USPS
		}

		return $this->quotes;
	}

	// --------------------------------------------
	function _notNull($value) {
	// --------------------------------------------
		if (is_array($value)) {
			if (sizeof($value) > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			if (($value != '') && (strtolower($value) != 'null') && (strlen(trim($value)) > 0)) {
				return true;
			} else {
				return false;
			}
		}
	}

	// --------------------------------------------
	function _upsAction($action) {
	// --------------------------------------------
		// 3 - Single Quote
		// 4 - All Available Quotes
		$this->_upsActionCode = $action;
	}

	// --------------------------------------------
	function _upsMethod($mode){
	// --------------------------------------------
		$this->_upsProductCode = $mode;
	}

	// --------------------------------------------
	function _upsOrigin($city, $postal, $country){
	// --------------------------------------------
		$this->_upsOriginCity = $city;
		$this->_upsOriginPostalCode = $postal;
		$this->_upsOriginCountryCode = $country;
	}

	// --------------------------------------------
	function _upsDest($city, $postal, $country){
	// --------------------------------------------
		$this->_upsDestCity = $city;
		$postal = str_replace(' ', '', $postal);

		if ($country == 'US') {
			$this->_upsDestPostalCode = substr($postal, 0, 5);
		} else {
			$this->_upsDestPostalCode = $postal;
		}

		$this->_upsDestCountryCode = $country;
	}

	// --------------------------------------------
	function _upsRate($val) {
	// --------------------------------------------
		switch ($val) {
			case 'RDP':
				$this->_upsRateCode = 'Regular+Daily+Pickup';
				break;
			case 'OCA':
				$this->_upsRateCode = 'On+Call+Air';
				break;
			case 'OTP':
				$this->_upsRateCode = 'One+Time+Pickup';
				break;
			case 'LC':
				$this->_upsRateCode = 'Letter+Center';
				break;
			case 'CC':
				$this->_upsRateCode = 'Customer+Counter';
				break;
		}
	}

	// --------------------------------------------
	function _upsContainer($val) {
	// --------------------------------------------
		switch ($val) {
			case 'CP': // Customer Packaging
				$this->_upsContainerCode = '00';
				break;
			case 'ULE': // UPS Letter Envelope
				$this->_upsContainerCode = '01';
				break;
			case 'UT': // UPS Tube
				$this->_upsContainerCode = '03';
				break;
			case 'UEB': // UPS Express Box
				$this->_upsContainerCode = '21';
				break;
			case 'UW25': // UPS Worldwide 25 kilo
				$this->_upsContainerCode = '24';
				break;
			case 'UW10': // UPS Worldwide 10 kilo
				$this->_upsContainerCode = '25';
				break;
		}
	}

	// --------------------------------------------
	function _upsWeight($val) {
	// --------------------------------------------
		$this->_upsPackageWeight = $val;
	}

	// --------------------------------------------
	function _upsRescom($val) {
	// --------------------------------------------
		switch ($val) {
			case 'RES': // Residential Address
				$this->_upsResComCode = '1';
				break;
			case 'COM': // Commercial Address
				$this->_upsResComCode = '0';
				break;
		}
	}

	// --------------------------------------------
	function _upsGetQuote() {
	// --------------------------------------------

		if (!isset($this->_upsActionCode)) $this->_upsActionCode = '4';

		$request = join('&', array('accept_UPS_license_agreement=yes',
								 '10_action=' . $this->_upsActionCode,
								 '13_product=' . $this->_upsProductCode,
								 '14_origCountry=' . $this->_upsOriginCountryCode,
								 '15_origPostal=' . $this->_upsOriginPostalCode,
								 '19_destPostal=' . $this->_upsDestPostalCode,
								 '22_destCountry=' . $this->_upsDestCountryCode,
								 '23_weight=' . $this->_upsPackageWeight,
								 '47_rate_chart=' . $this->_upsRateCode,
								 '48_container=' . $this->_upsContainerCode,
								 '49_residential=' . $this->_upsResComCode));

		$http = new httpClient();

		if ($http->Connect('www.ups.com', 80)) {
			$http->addHeader('Host', 'www.ups.com');
			$http->addHeader('User-Agent', 'Zen Cart');
			$http->addHeader('Connection', 'Close');

			if ($http->Get('/using/services/rave/qcostcgi.cgi?' . $request)) 
				$body = $http->getBody();

			$http->Disconnect();
		} else {
			return 'error';
		}

		// BOF: UPS USPS
		/*
		TEST by checking out in the catalog; try a variety of shipping destinations to be sure
		your customers will be properly served.  If you are not getting any quotes, try enabling
		more alternatives in admin. Make sure your store's postal code is set in Admin ->
		Configuration -> Shipping/Packaging, since you won't get any quotes unless there is
		a origin that UPS recognizes.
		
		If you STILL don't get any quotes, here is a way to find out exactly what UPS is sending
		back in response to rate quote request.  At line 278, you will find this statement in a
		comment block:
		*/

		If ($this->sendEmail) {
			mail('williamc@forbin.com','UPS response',$body,'From: <you@yourdomain.com>');
		}
		// EOF: UPS USPS

		$body_array = explode("\n", $body);
		$n = sizeof($body_array);

		$returnval = array();
		$errorret = 'error'; // only return error if NO rates returned

		for ($i=0; $i<$n; $i++) {
			$result = explode('%', $body_array[$i]);
			$errcode = substr($result[0], -1);

			switch ($errcode) {
				case 3:
					if (is_array($returnval)) 
						$returnval[] = array($result[1] => $result[8]);
					break;
				case 4:
					if (is_array($returnval)) 
						$returnval[] = array($result[1] => $result[8]);
					break;
				case 5:
					$errorret = $result[1];
					break;
				case 6:
					if (is_array($returnval)) 
						$returnval[] = array($result[3] => $result[10]);
					break;
			}// end switch
		}//end for

		if (empty($returnval)) 
			$returnval = $errorret;

		return $returnval;
	}

};
?>