<?php
class wValidate {

	private $filter_validation;

	//------------------------------
	public function __construct() {
	//------------------------------
		$this->filter_validation = PHP_VERSION_ID >= 50200 ? true : false;
	}

	//------------------------------
	public function _SanitizeQuery($str) {
	//------------------------------
		# This function help sanitize our data to be SQL injection safe.
		# _SanitizeQuery($_POST); 
		return is_array($str) ? array_map('_clean', $str) : str_replace('\\', '\\\\', htmlspecialchars((get_magic_quotes_gpc() ? stripslashes($str) : $str), ENT_QUOTES));
	}  

	//------------------------------
	public function _SanitizeData($str) {
	//------------------------------
		# This function help to keep us protected against XSS, JS and SQL injection by removing tags.
		# _SanitizeData($_POST); 
		return is_array($str) ? array_map('_clean', $str) : str_replace('\\', '\\\\', strip_tags(trim(htmlspecialchars((get_magic_quotes_gpc() ? stripslashes($str) : $str), ENT_QUOTES))));  
	}

	//------------------------------
	public function _SanitizeNumber($str) {
	//------------------------------
		if (empty($str) || $str == NULL) 
			return FALSE;

		if ($this->filter_validation) {
			return filter_var($str, FILTER_SANITIZE_NUMBER_INT);
		} else {
			return preg_match('/[^0-9]/', '', $str);
		}
	}

	//------------------------------
	public function isValidEmail($email) {
	//------------------------------
		if (empty($email) || $email == NULL) 
			return FALSE;

		if ($this->filter_validation) {
			return filter_var($email, FILTER_VALIDATE_EMAIL);
		} else {
			$regex = '/^([*+!.&#$¦\'\\%\/0-9a-z^_`{}=?~:-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,4})$/i'; 
			return preg_match($regex, trim($email), $matches); 
		}
	}

	//------------------------------
	public function isValidUSPhone($phone) {
	//------------------------------
		if (empty($phone) || $phone == NULL) 
			return FALSE;

		$regex = '/^(?:1(?:[. -])?)?(?:\((?=\d{3}\)))?([2-9]\d{2})'
		.'(?:(?<=\(\d{3})\))? ?(?:(?<=\d{3})[.-])?([2-9]\d{2})'
		.'[. -]?(\d{4})(?: (?i:x)\.? ?(\d{1,5}))?$/';
		return preg_match($regex, trim($phone), $matches);
	}

	//------------------------------
	public function isValidIP($ip) {
	//------------------------------
		if (empty($ip) || $ip == NULL) 
			return FALSE;

		if ($this->filter_validation) {
			return filter_var($ip, FILTER_VALIDATE_IP);
		} else {
			return preg_match("/^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])" . 
			"(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$/", $ip );
		}
	}

	//------------------------------
	public function isValidURL($url) {
	//------------------------------
		if (empty($url) || $url == NULL) 
			return FALSE;

		if ($this->filter_validation) {
			return filter_var($url, FILTER_VALIDATE_URL);
		} else {
			$regex = "((https?|ftp)\:\/\/)?"; // Scheme 
			$regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass 
			$regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP 
			$regex .= "(\:[0-9]{2,5})?"; // Port 
			$regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path 
			$regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query 
			$regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor 
			return preg_match("/^$regex$/", $url);
		}
	}

	//------------------------------
	public function isValidUSPostalCode($zip) {
	//------------------------------
		if (empty($zip) || $zip == NULL) 
			return FALSE;

		# 5 Digit Zip Code or 9 Digit Zip Code
		$regex = "/^[0-9]{5}(-[0-9]{4}){0,1}$/";
		return preg_match($regex, $zip);
	}

	//------------------------------
	public function isValidUSSocialSecurity($ssn) {
	//------------------------------
		if (empty($ssn) || $ssn == NULL) 
			return FALSE;

		# eg. 531-63-5334  
		$regex = "/^[\d]{3}-[\d]{2}-[\d]{4}$/";
		return preg_match($regex, $ssn);
	}

	//------------------------------
	public function isValidCreditCard($cc) {
	//------------------------------
		if (empty($cc) || $cc == NULL) 
			return FALSE;

		# eg. 718486746312031  
		$regex = "/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6011[0-9]{12}|3(?:0[0-5]|[68][0-9])[0-9]{11}|3[47][0-9]{13})$/";
		return preg_match($regex, $cc);
	}

	//------------------------------
	public function isValidColor($color) {
	//------------------------------
		if (empty($color) || $color == NULL) 
			return FALSE;

		#CCC  
		#FFFFF  
		$regex = "/^#(?:(?:[a-f0-9]{3}){1,2})$/i";
		return preg_match($regex, $color);
	}

	//------------------------------
	public function isValidPassword($password, $length) {
	//------------------------------
		if (empty($password) || $password == NULL) 
			return FALSE;

		# Must contain ? characters, 1 uppercase, 1 lowercase and 1 number 
		$regex = "/^.*(?=.{" . $length . ",})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/";
		return preg_match($regex, $password);
	}

	//------------------------------
	public function isMatchedPasswords($password1, $password2) {
	//------------------------------
		if (empty($password1) || $password1 == NULL || empty($password2) || $password2 == NULL) 
			return FALSE;

		# Case sensitive
		return strcmp($password1, $password2) == 0;
	}

	//------------------------------
	public function isValidDocument($file) {
	//------------------------------
		if (empty($file) || $file == NULL) 
			return FALSE;

		$document_ext_allowed = array("csv","doc","pdf","pps","ppt","txt","xls");
		$ext = strtolower(substr(strrchr($file, "."), 1));
		return in_array($ext, $document_ext_allowed);
	}

	//------------------------------
	public function isValidImage($file) {
	//------------------------------
		if (empty($file) || $file == NULL) 
			return FALSE;

		$image_ext_allowed = array("jpg", "jpeg", "png", "gif","bmp");
		$ext = strtolower(substr(strrchr($file, "."), 1));
		return in_array($ext, $image_ext_allowed);
	}

	//------------------------------
	public function isValidMIMEImage($file_info) {
	//------------------------------
		if (empty($file_info) || $file_info == NULL) 
			return FALSE;

		$mime = array('image/gif' => 'gif',
					  'image/jpeg' => 'jpeg',
					  'image/png' => 'png',
					  'application/x-shockwave-flash' => 'swf',
					  'image/psd' => 'psd',
					  'image/bmp' => 'bmp',
					  'image/tiff' => 'tiff',
					  'image/tiff' => 'tiff',
					  'image/jp2' => 'jp2',
					  'image/iff' => 'iff',
					  'image/vnd.wap.wbmp' => 'bmp',
					  'image/xbm' => 'xbm',
					  'image/vnd.microsoft.icon' => 'ico');
		$file_mime = $file_info['mime'];
		return array_key_exists($file_mime, $mime);
	}

	//------------------------------
	public function isImageExist($url) {
	//------------------------------
		if(@file_get_contents($url,0,NULL,0,1)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

};

$Validate = new wValidate;
?>