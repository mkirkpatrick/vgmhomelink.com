<?php 
namespace Forbin\Library\Classes\Cache;

use Forbin\Library\Classes\Cache\CacheInvalidArgumentException;

abstract class CacheAbstract implements CacheInterface {
    
    const KEY_INVALID_CHARACTERS = '{}()/\@:';

	/**
	 * @param string $key
	 *
	 * @return bool
	 */
	public static function has($key){
		$response = static::get($key, null);
        return true; 
	}
    


	/**
	 * @param array $keys
	 * @param null  $defaultValue
	 *
	 * @return array or $defaultValue if none of the keys have values
	 */
	public static function getMultiple($keys, $defaultValue = null) {
        if ($keys instanceof Traversable) {
            $keys = iterator_to_array($keys, false);
        }
        if (!is_array($keys)) {
            throw new InvalidArgumentException(
                'Invalid keys: '.var_export($keys, true).'. Keys should be an array or Traversable of strings.'
            );
        }
		$data = [];

		foreach($keys as $key){
			$data[$key] = static::get($key, $defaultValue);
		}
        if(!array_filter($data)) {
            return $defaultValue; 
        }

		return $data; 
	}

	/**
	 * @param array    $values
	 * @param int|null $ttl
	 *
	 * @return bool
	 */
	public static function setMultiple($values, $ttl = null) {
        
        if ($values instanceof Traversable) {
            $array = array();
            foreach ($values as $key => $value) {
                if (!is_string($key) && !is_int($key)) {
                    throw new CacheInvalidArgumentException(
                        'Invalid values: '.var_export($values, true).'. Only strings are allowed as keys.'
                    );
                }
                $array[$key] = $value;
            }
            $values = $array;
        }
        if (!is_array($values)) {
            throw new CacheInvalidArgumentException(
                'Invalid values: '.var_export($values, true).'. Values should be an array or Traversable with strings as keys.'
            );
        }
        
		$return = [];
		if(!empty($values)){

			foreach($values as $key => $value){
				$return[] = static::set($key, $value, $ttl);
			}

		}

		return self::checkReturn($return);
	}

	/**
	 * @param array $keys
	 *
	 * @return bool
	 */
	public static function deleteMultiple($keys) {
        if ($keys instanceof Traversable) {
            $keys = iterator_to_array($keys, false);
        }
        if (!is_array($keys)) {
            throw new CacheInvalidArgumentException(
                'Invalid keys: '.var_export($keys, true).'. Keys should be an array or Traversable of strings.'
            );
        }
        //array_map(array(self, 'isValidKey'), $keys);
        
		$return = [];

		if(!empty($keys)){

			foreach($keys as $key){
				$return[] = static::delete($key);
			}

		}

		return self::checkReturn($return);
	}

	/**
	 * @param bool[] $booleans
	 *
	 * @return bool
	 */
	protected static function checkReturn(array $booleans) {

		foreach($booleans as $bool){
			if(!$bool){
				return false;
			}
		}

		return true;
	}
    
 /**
  * Throws an exception if $key is invalid.
  *
  * @param string $key
  *
  * @throws CacheInvalidArgumentException
  */
    protected static function isValidKey($key)
    {
        if (!is_string($key)) {
            throw new CacheInvalidArgumentException('Invalid key: '.var_export($key, true).'. Key should be a string.');
        }
        if ($key === '') {
            throw new CacheInvalidArgumentException ('Invalid key. Key should not be empty.');
        }
        // valid key according to PSR-16 rules
        $invalid = preg_quote(self::KEY_INVALID_CHARACTERS, '/');
        if (preg_match('/['.$invalid.']/', $key)) {
            throw new CacheInvalidArgumentException (
                'Invalid key: '.$key.'. Contains (a) character(s) reserved '.
                'for future extension: '. self::KEY_INVALID_CHARACTERS
            );
        }
    }
    
 /**
  * Accepts all TTL inputs valid in PSR-16 (null|int|DateInterval) and
  * converts them into TTL for KeyValueStore (int).
  *
  * @param null|int|DateInterval $ttl
  *
  * @return int
  *
  * @throws SimpleCacheInvalidArgumentException
  */
    protected static function getTTL($ttl){

		if($ttl instanceof \DateInterval){
			return $ttl->s;
		}
		else if(is_int($ttl) || is_null($ttl)){
			return $ttl;
		}
		else {
			throw new CacheInvalidArgumentException('Invalid TTL');
		}

	}
}

?>