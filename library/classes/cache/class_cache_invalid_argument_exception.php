<?php 
namespace Forbin\Library\Classes\Cache;

use \InvalidArgumentException; 

// Exception interface for invalid cache arguments.
class CacheInvalidArgumentException extends InvalidArgumentException {} 

?>