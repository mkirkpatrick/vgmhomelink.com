<?php
namespace Forbin\Library\Classes\Cache;

use Forbin\Library\Classes\Cache\CacheInterface;
use Forbin\Library\Classes\Cache\CacheAbstract;
use Forbin\Library\Classes\Cache\CacheInvalidArgumentException;
use Forbin\Library\Classes\Cache\CacheException;

class Cache extends CacheAbstract implements CacheInterface {

/**
 * Persists data in the cache, uniquely referenced by a key with an optional expiration TTL time.
 *
 * @param string                $key   The key of the item to store.
 * @param mixed                 $value The value of the item to store, must be serializable.
 * @param null|int|DateInterval $ttl   Optional. The TTL value of this item.
 *
 * @return bool True on success and false on failure.
 *
 * @throws CacheException will be thrown if unable to write to cache.
 * @throws CacheInvalidArgumentException will be thrown if the $key string or $ttl is not a legal value.
 * @throws CacheInvalidArgumentException will be thrown if the $key string is not a legal value.
 */
	public static function set($key,$value,$ttl = PHP_INT_MAX) {
        self::isValidKey($key);
        self::delete($key);
        $path = self::getBaseFilePath($key) . '-' . (time()+self::getTTL($ttl));
        $file = fopen($path,'w');
        if (!$file) throw new CacheException('Could not write to cache');
        $value = serialize(array($key, $value));
        if (fwrite($file,$value)===false) {
          throw new CacheException('Could not write to cache');
        }
        fclose($file);
        //self::garbageCollection();
    }

/**
* Delete an item from the cache by its unique key.
*
* @param string $key The unique cache key of the item to delete.
*
* @return bool True if the item was successfully removed. False if there was an error.
*
* @throws CacheInvalidArgumentException will be thrown if the $key string is not a legal value.
*/
	public static function delete($key) {
        self::isValidKey($key);
        $filepath = self::getFullFilePath($key);
        if(is_file($filepath)){
			return unlink($filepath);
		}

		return false;
	}

/**
 * Fetches a value from the cache.
 *
 * @param string $key     The unique key of this item in the cache.
 * @param mixed  $default Default value to return if the key does not exist.
 *
 * @return mixed The value of the item from the cache, or $default in case of cache miss.
 *
 * @throws CacheInvalidArgumentException will be thrown if the $key string is not a legal value.
 */
    public static function get($key, $defaultValue = null) {
        self::isValidKey($key);
        $filename = self::getFullFilePath($key);
        if (is_null($filename) || !file_exists($filename) || !is_readable($filename)) {
            return $defaultValue;
        }

        $data = file_get_contents($filename);
        $data = unserialize($data);
        $currentTime = time();
        $expirationTime = self::getExpirationTime($filename);
        if (!$data || $currentTime > $expirationTime) {
            self::delete($key);
            return $defaultValue;
        }
        return $data[1];
    }

 /**
  * Clear Cache
  *
  * Delete all keys from the cache.
  *
  * @return boolean True if the cache was successfully cleared, false otherwise
  */

    public static function clear() {
        $mask = self::getRootPath() . '/cache_' . self::getUniqueHash() . '_*';
        return array_map('unlink', glob($mask));
    }

 /**
  * Gets all unexpired keys in the current cache. Keys are not sorted in any particular order.
  *
  * @return array of all keys.
  */

    public static function getKeys(){
        $keys = [];
		$mask = self::getRootPath() . '/cache_' . self::getUniqueHash() . '_*';
        $files = glob($mask);
        foreach($files as $filepath) {
            array_push($keys, self::getKey($filepath));
        }
        return array_filter($keys, function($var){return !is_null($var);} );
	}

 /**
  * Garbage collection
  *
  * Permanently remove all expired data
  *
  * @return void
  */
    public static function garbageCollection($entireServer = false) {
        $run = mt_rand(1, 1000);
        if($run == 500) {
            $mask = $entireServer ? self::getRootPath() . '/cache_*'  : self::getRootPath() . '/cache_' . self::getUniqueHash() . '_*';
            $files = glob($mask);
            foreach($files as $filepath) {
                if (time() > self::getExpirationTime($filepath)) {
                    unlink($filepath);
                }
            }
        }
    }

    public static function getBaseFilePath($key) {
        return self::getRootPath() . '/cache_' . self::getUniqueHash() . '_' . hash('sha256',$key);
    }

    protected static function getKey($filepath) {
        if (!file_exists($filepath) || !is_readable($filepath)) return null;
        $data = file_get_contents($filepath);
        $data = unserialize($data);
        if (!$data || time() > static::getExpirationTime($filepath)) {
            unlink($filepath);
            return null;
        }
        return $data[0];
    }

    protected static function getExpirationTime($filepath) {
        $fileInfo = explode('-', $filepath);
        if(count($fileInfo) != 2) {
            return 0;
        }
        return (int) $fileInfo[1];
    }

    protected static function getFullFilePath($key) {
        $mask = self::getRootPath() . '/cache_' . self::getUniqueHash() . '_' . hash('sha256',$key) . '*';
        return isset(glob($mask)[0]) ? glob($mask)[0] : null;
    }

    protected static function getRootPath() {
        return $_SERVER['DOCUMENT_ROOT'] . '/uploads/temp/dashboard-cache';
        //return ini_get('session.save_path');
    }

    protected static function getUniqueHash() {
    //------------------------------
        return hash('adler32', $_SERVER['SERVER_NAME']);
    }
}
?>