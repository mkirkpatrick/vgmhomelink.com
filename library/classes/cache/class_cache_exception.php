<?php 
namespace Forbin\Library\Classes\Cache;

use \RuntimeException;
use \InvalidArgumentException; 


// Base Cache Exception Interface used for all types of exceptions thrown by the cache library.
class CacheException extends RuntimeException {}

?>