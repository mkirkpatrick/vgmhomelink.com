<?php
class HttpAuthentication {

    const DEFAULT_REALM	= "VGM Forbin Secure Staging Site";

	public static function authenticate_digest($realm = self::DEFAULT_REALM) {
		global $_WEBCONFIG;
        
		if($_WEBCONFIG["LOCAL_IP"] != $_SERVER['HTTP_CLIENT_IP'] && $_SERVER['HTTP_CLIENT_IP'] != '127.0.0.1' && $_WEBCONFIG['SITE_STATUS'] != "LIVE") {
			$header1 = 'WWW-Authenticate: Digest realm="' . $realm . '",qop="auth",nonce="' . uniqid() . '",opaque="' . md5($realm). '"';
			$header2 = 'HTTP/1.0 401 Unauthorized';

			if (!isset($_SERVER['PHP_AUTH_DIGEST']) || empty($_SERVER['PHP_AUTH_DIGEST'])) {
               /* User Hit Cancel Button */
			   header($header1);
			   serverTransfer("/401error.php");
			}

			// analyze the PHP_AUTH_DIGEST variable
			$data = self::http_digest_parse($_SERVER['PHP_AUTH_DIGEST']);
			$username = $data['username'];
			$password = $_WEBCONFIG['SITE_ACCESS_PASSWORD'];

			if (!$data || $_WEBCONFIG['SITE_ACCESS_USER'] != $username) {
				header($header1);
				header($header2);
				return false;
			}

			// generate the valid response
			$A1 = md5($username . ':' . $realm . ':' . $password);
			$A2 = md5($_SERVER['REQUEST_METHOD'] . ':' . $data['uri']);
			$valid_response = md5($A1 . ':' . $data['nonce'] . ':' . $data['nc'] . ':' . $data['cnonce'] . ':' . $data['qop'] . ':' . $A2);

			if (($data['response'] != $valid_response) && !ActiveDirectory::validateWebmasterAdmin($data['username'], $data['password'])) {
				header($header1);
				header($header2);
				return false;
			}
		}

		return true;
	}
    public static function authenticate_basic($realm = self::DEFAULT_REALM) {
        global $_WEBCONFIG;

        $username = null;
        $password = null;

        // mod_php
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            $username = $_SERVER['PHP_AUTH_USER'];
            $password = $_SERVER['PHP_AUTH_PW'];

            // most other servers
        } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            if (strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']),'basic')===0)
                list($username,$password) = explode(':',base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
        }

        if (is_null($username)) {
            header('WWW-Authenticate: Basic realm="' . $realm . '"');
            header('HTTP/1.0 401 Unauthorized');
            serverTransfer("/401error.php");
        } else if(($_WEBCONFIG['SITE_ACCESS_USER'] == $username && $_WEBCONFIG['SITE_ACCESS_PASSWORD'] == $password)
            || (ActiveDirectory::validateWebmasterAdmin($username, $password))) {
            return true;
        } else {
            header('WWW-Authenticate: Basic realm="' . $realm . '"');
            header('HTTP/1.0 401 Unauthorized');
            serverTransfer("/401error.php");
        }
    }
    private static function http_digest_parse($txt)
	{
		$keys_arr = array();
		$values_arr = array();
		$cindex = 0;
		$parts = explode(',', $txt);

		foreach($parts as $p) {
			$p = trim($p);
			$kvpair = explode('=', $p);
			$kvpair[1] = str_replace("\"", "", $kvpair[1]);
			$keys_arr[$cindex] = $kvpair[0];
			$values_arr[$cindex] = $kvpair[1];
			$cindex++;
		}

		$ret_arr = array_combine($keys_arr, $values_arr);
		$ret_arr['uri'] = $_SERVER['REQUEST_URI'];
		return $ret_arr;
	}
};
?>