<?php
class VGMActiveDirectory {

    //---------------------------------------------------------------------
    public static function validateActiveDirectoryCredentials($username, $password) {
    //---------------------------------------------------------------------
        global $_WEBCONFIG;
        $accessKey = "sS6k2LHtOIcxR9NB3LVQtj1pNAo9FqwKeCYL98uX5o9jeAeOH";
        $postString = 'accessKey='. $accessKey . '&password=' . urlencode($password) . '&username=' . urlencode($username) . '&ip=' . urlencode($_SERVER['HTTP_CLIENT_IP']) . '&domain=' . urlencode($_SERVER['SERVER_NAME']) . '&sitetype=' . urlencode($_WEBCONFIG['SITE_TYPE']);
		$url = 'https://ldapservice.vgm.com/VGMAuthenticationService.asmx/Authenticate'; 
		$connectionHandler = curl_init($url);
        if ($connectionHandler == false) throw new Exception("Data Connection Error for Web Service Url :: $url -- Unable to Connect");
        curl_setopt($connectionHandler, CURLOPT_POST, count(explode("&", $postString)));
        curl_setopt($connectionHandler, CURLOPT_POSTFIELDS, $postString);
        curl_setopt($connectionHandler, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($connectionHandler, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($connectionHandler, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($connectionHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($connectionHandler, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($connectionHandler, CURLOPT_TIMEOUT, 30);
        $result = curl_exec($connectionHandler);
        $info = curl_getinfo($connectionHandler);
        $errorNumber = curl_errno($connectionHandler);
        $errorMessage = curl_error($connectionHandler);
        curl_close($connectionHandler);

        // cURL Error
        if ($errorNumber > 0) {
            throw new Exception($errorMessage, $errorNumber);
        }
        // ASP.NET Exception
        else if(stristr($result, "Exception:")) {
            throw new Exception('<em>ASP.NET Exception </em><br /><blockquote>' . $result . "</blockquote>");
        }
        // Web Service Url Not Found
        else if ($info['http_code'] != 200) {
            throw new Exception("Data Connection Error for Web Service Url :: $url -- HTTP Code [" . $info['http_code'] . "]");
        }
        $response = new SimpleXMLElement($result);
        return $response->ResponseCode == 1;
    }
}
?>