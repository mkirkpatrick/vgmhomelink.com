<?
class EventLogger { 

	//------------------------------
	public static function _logEvent($eventname, $detail = null, $user = null) {
	//------------------------------
		global $_WEBCONFIG;
		if(isset($_WEBCONFIG['ENABLE_LOGGING']) && $_WEBCONFIG['ENABLE_LOGGING'] == "true") { 
			$eventname = xss_sanitize(Database::quote_smart($eventname));
			$detail = xss_sanitize(Database::quote_smart($detail)); 
			$useragent = isset($_SERVER['HTTP_USER_AGENT']) ? xss_sanitize(Database::quote_smart($_SERVER['HTTP_USER_AGENT'])) : '';
			$referrer = isset($_SERVER['HTTP_REFERER']) ? xss_sanitize(Database::quote_smart($_SERVER['HTTP_REFERER'])) : '';
			$ip = $_SERVER['HTTP_CLIENT_IP']; 
			$user = !is_null($user) ? "'" . $user . "'" : "null"; 
			$httpCode = function_exists("http_response_code") ? http_response_code() : 0; 
			$machineName = isset($_SERVER['COMPUTERNAME']) ? $_SERVER['COMPUTERNAME'] : ''; 
			$url = xss_sanitize(Database::quote_smart($_SERVER['ABSOLUTE_URI'])); 

			$sql = "INSERT INTO `tbl_event_log` (`name`, `httpcode`, `ip`, `details`, `username`, `useragent`, `referrer`, `machinename`, `url`)
				VALUES('$eventname', '$httpCode', '$ip', '$detail', $user, '$useragent', '$referrer', '$machineName', '$url')"; 
			Database::ExecuteRaw($sql);
		}
	}
};
?>