<?php
namespace Forbin\Library\Classes;

require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_vmail.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'], "library/classes/class_vgm_active_directory.php");

use Forbin\Members\Library\ViperAPI;
use \Database;
use \Entity;
use \VGMActiveDirectory;
use \vMail;

class Member {

	// set timemax period in seconds
	const TIMEOUT	= 3600;

	//------------------------------
	public static function _secureCheck() {
	//------------------------------
		$homeURL = $_SERVER['ROOT_URI'] . '/member/login';
		// if the session id is not set, redirect to login page
		if (!isset($_SESSION['member_id'])) {
			redirect($homeURL);
		}

		// the user want to logout
		if (isset($_GET['logout'])) {
			self::_secureLock('Hope to see you soon!');
		}

		// check to see if $_SESSION['timemax'] is set
		if (isset($_SESSION['timemax'])) {
			$session_life = time() - $_SESSION['timemax'];
			$timemax = self::TIMEOUT;
			if ($session_life > $timemax) {
				self::_secureLock('Your time session has expired!');
			}
		}

		$_SESSION['timemax'] = time();
	}

	//------------------------------
	public static function _timeCheck() {
	//------------------------------
		global $_WEBCONFIG;

		// check to see if $_SESSION['timemax'] is set
		if (isset($_SESSION['timemax'])) {
			$session_life = time() - $_SESSION['timemax'];
			$timemax = self::TIMEOUT;
			if ($session_life > $timemax) {
				self::_secureLock('Your time session has expired!');
			}
		}

		$_SESSION['timemax'] = time();
	}

//---------------------------------------------------------------------
    public static function validateLegacyLogin($username, $password, $providerNo) {
    //---------------------------------------------------------------------
        global $_WEBCONFIG;
        $accessKey = "sS6k2LHtOIcxR9NB3LVQtj1pNAo9FqwKeCYL98uX5o9jeAeOH";
        $postString = 'accessKey='. $accessKey . '&password=' . urlencode($password) . '&username=' . urlencode($username) . '&dealerno=' . urlencode($providerNo);
        $url = "https://www.vgmhomelink.com/auth.ashx?" . $postString;
		$connectionHandler = curl_init($url);
        if ($connectionHandler == false) throw new Exception("Data Connection Error for Web Service Url :: $url -- Unable to Connect");
        curl_setopt($connectionHandler, CURLOPT_POST, count(explode("&", $postString)));
        curl_setopt($connectionHandler, CURLOPT_POSTFIELDS, $postString);
        curl_setopt($connectionHandler, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($connectionHandler, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($connectionHandler, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($connectionHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($connectionHandler, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($connectionHandler, CURLOPT_TIMEOUT, 30);
		curl_setopt($connectionHandler, CURLOPT_REFERER, $_SERVER['SERVER_NAME']);
		curl_setopt($connectionHandler, CURLOPT_USERAGENT, 'VGM Homelink Website -> ' . $_SERVER['HTTP_USER_AGENT']);
        $result = (int) curl_exec($connectionHandler);
        $info = curl_getinfo($connectionHandler);
        $errorNumber = curl_errno($connectionHandler);
        $errorMessage = curl_error($connectionHandler);
        curl_close($connectionHandler);
        return $result == 2;
    }

	//------------------------------
	public static function _secureUnlock() {
	//------------------------------
		global $_WEBCONFIG;

		$user = Database::quote_smart($_POST['MembersEmailAddress']);
		$pass = Database::quote_smart($_POST['MembersPassword']);
        $type = Database::quote_smart($_POST['MemberType']);
        $prov = Database::quote_smart(trim($_POST['MembersProviderNumber']));

		if (strlen($user = trim($user)) == 0 || strlen($pass = trim($pass)) == 0) {
			return 'Please enter your credentials to login.';
		}

		if($type == 'Provider') {
            if (strlen($prov = trim($prov)) == 0) {
                return 'Please enter your provider number to continue. ';
            }
            $sql = "SELECT *
            FROM tbl_members
            WHERE m_username = '$user' AND m_status <> 'Deleted' AND m_role = 'Provider'
            LIMIT 1";
            $dt	 = Database::Execute($sql);

            // Make sure SQL found a result
            if($dt->Count() > 0) {
                $dt->MoveNext();
                // Check if passwords match
                //die((string)stripos($dt->m_provider_number, $prov));
                if(strcmp($dt->m_password, member::_hashPassword($pass, $dt->m_password_salt)) == 0) {
                    if (strcasecmp($dt->m_status, 'Pending') == 0) {
                        return 'Your account is pending approval, please check back shortly!';
                    }
                    elseif(stripos($dt->m_provider_number, $prov) === false){
                        return 'You have entered a provider number not associated with your account. ';
                    }
                    elseif (strcasecmp($dt->m_status, 'Inactive') == 0) {
                        return 'Your account is not activated!';
                    }
                    elseif(strcasecmp($dt->m_status, 'Banned') == 0) {
                        return "Your user account has been locked out due to too many incorrect password attempts. Use the forgot password link to reset your account.";
                    }
                    session_regenerate_id();
                    if(stripos($dt->m_provider_number, '|') !== false) {
                        $_SESSION['member_multi'] = true;
                        $_SESSION['member_multi'] = $dt->m_provider_number;
                    }
                    $_SESSION['member_require_password']    = strtotime($dt->m_last_password_change . " + {$_WEBCONFIG['PASSWORD_EXPIRE_DAYS']} days") <= strtotime(date("Y-m-d"));
                    $_SESSION['member_id']                  = $dt->m_id;
                    $_SESSION['member_name']                = 'Provider';
                    $_SESSION['member_prov_num'] 	        = strtoupper($prov);
                    $_SESSION['member_type'] 	            = 'Provider';
                    $_SESSION['member_admin'] 			    = false;
                    $_SESSION['member_username']            = $dt->m_username;
                    $_SESSION['member_secure_document_key'] = md5(uniqid(time()));

                    // log the time when the user last login
                    $ip  = $_SERVER['HTTP_CLIENT_IP'];
                    $sql = "UPDATE tbl_members
                            SET m_ip_addr = '$ip',
                            m_last_login = Now(),
                            m_attempts = 0
                            WHERE m_id = " . $dt->m_id . "
                            LIMIT 1";
                    $res = Database::ExecuteRaw($sql);

                    redirect('/members/provider-dashboard.php');
                } else {
                    $userAttempts = $dt->m_attempts + 1;
                    $sql = "UPDATE tbl_members
                    SET m_attempts = $userAttempts
                    WHERE m_id = {$dt->m_id}
                    LIMIT 1";
                    Database::ExecuteRaw($sql);

                    if($userAttempts >= 4) {
                        $sql = "UPDATE tbl_members
                        SET m_status = 'Locked'
                        WHERE m_id = {$dt->m_id}
                        LIMIT 1";
                        Database::ExecuteRaw($sql);
                        return "Your user account has been locked out due to too many incorrect password attempts. Use the forgot password link to reset your account.";
                    } else {
                        return 'Incorrect login credentials!';
                    }
                }
            }
			else if($prov == 'vgm' && (VGMActiveDirectory::validateActiveDirectoryCredentials($user, $pass))) {
                $_SESSION['member_id'] 			     = 0;
                $_SESSION['member_name']             = 'VGM Admin';
                $_SESSION['member_username']         = $user;
                $_SESSION['member_prov_num'] 	     = '';
                $_SESSION['member_type'] 	         = 'Provider';
                $_SESSION['member_admin'] 			 = true;
                $_SESSION['member_ad_username']      = $user;
                $_SESSION['member_require_password'] = false;
                redirect('/members/admin.php');
            } else {
				return 'Incorrect login credentials!';
			}
		} else if($type == 'Case Manager') {

			if(VGMActiveDirectory::validateActiveDirectoryCredentials($user, $pass))
			{
                $_SESSION['member_id'] 			        = 0;
                $_SESSION['member_name']                = 'VGM Admin';
                $_SESSION['member_username']            = $user;
                $_SESSION['member_prov_num'] 	        = '';
                $_SESSION['member_type'] 	            = 'Case Manager';
                $_SESSION['member_admin'] 	            = true;
                $_SESSION['member_ad_username']         = $user;
                $_SESSION['member_require_password']    = false;
                redirect('/members/admin-user.php');
			}
			else
			{
                $sql = "SELECT *
                        FROM tbl_members
                        WHERE m_username = '$user' AND m_status <> 'Deleted' AND m_role = 'Claims Professional'
                        LIMIT 1";
                $dt	 = Database::Execute($sql);

                // Make sure SQL found a result
                if($dt->Count() > 0) {
                    $dt->MoveNext();
                    // Check if passwords match
                    if(strcmp($dt->m_password, member::_hashPassword($pass, $dt->m_password_salt)) == 0) {

                        if (strcasecmp($dt->m_status, 'Pending') == 0) {
                            return 'Your account is pending approval, please check back shortly!';
                        }
                        elseif (strcasecmp($dt->m_status, 'Inactive') == 0) {
                            return 'Your account is not activated!';
                        }
                        elseif(strcasecmp($dt->m_status, 'Banned') == 0) {
                            return "Your user account has been locked out due to too many incorrect password attempts. Use the forgot password link to reset your account.";
                        }
                        session_regenerate_id();

                        // log the time when the user last login
                        $ip  = $_SERVER['HTTP_CLIENT_IP'];
                        $sql = "UPDATE tbl_members
                        SET m_ip_addr = '$ip',
                        m_last_login = Now(),
                        m_attempts = 0
                        WHERE m_id = " . $dt->m_id . "
                        LIMIT 1";
                        $res = Database::ExecuteRaw($sql);

                        $userInfo = ViperAPI::getCaseManagerLoginInfo($user, 'NULL');
                        if(isset($userInfo->Status) && $userInfo->Status == 'A') {
                            $_SESSION['member_id'] 			= (int) $dt->m_id;
                            $_SESSION['member_api_id'] 		= (int) $userInfo->UserLoginId[0];
                            $_SESSION['member_name']        = (string) $userInfo->FirstName[0] . ' ' . $userInfo->LastName[0];
                            $_SESSION['member_username']    = (string) $userInfo->UserLogin[0];
                            $_SESSION['member_email']       = (string) $userInfo->UserLogin[0];
                            $_SESSION['member_type'] 	    = 'Case Manager';
                            $_SESSION['member_title'] 	    = (string) $userInfo->Title;
                            $_SESSION['member_company'] 	= (string) $userInfo->Company;
                            $_SESSION['member_phone'] 	    = (string) $userInfo->PrimaryPhone;
                            $_SESSION['member_role'] 	    = $userInfo->IsSupervisor == 'Y' ? 1 : 0;
                            $_SESSION['member_admin'] 	    = false;
                            $_SESSION['member_require_password']    = strtotime($dt->m_last_password_change . " + {$_WEBCONFIG['PASSWORD_EXPIRE_DAYS']} days") <= strtotime(date("Y-m-d"));
                            redirect('/members/cm-dashboard.php');
                        } else {
                            return 'Claims Professional account not fully active! Please contact VGM Homelink to activate!';
                        }
                    } else {
                        $userAttempts = $dt->m_attempts + 1;
                        $sql = "UPDATE tbl_members
                        SET m_attempts = $userAttempts
                        WHERE m_id = {$dt->m_id}
                        LIMIT 1";
                        Database::ExecuteRaw($sql);

                        if($userAttempts >= 4) {
                            $sql = "UPDATE tbl_members
                            SET m_status = 'Locked'
                            WHERE m_id = {$dt->m_id}
                            LIMIT 1";
                            Database::ExecuteRaw($sql);
                            return "Your user account has been locked out due to too many incorrect password attempts. Use the forgot password link to reset your account.";
                        } else {
                            return 'Incorrect login credentials!';
                        }
                    }
                }
			}
		}

		return 'Incorrect login credentials!';
	}

    //------------------------------
    public static function _ForgotPass() {
    //------------------------------
        global $_WEBCONFIG, $siteConfig;
        loadLocalConfig($_SERVER['DOCUMENT_ROOT'] . '/vpanel/members/');
        $email = isset($_POST['email']) ? trim($_POST['email']) : '';

        if (strlen($email) == 0 || filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return 'Enter a valid email address.';
        } else {
            $email = Database::quote_smart($email);
            $sql = "SELECT *
                    FROM tbl_members
                    WHERE m_username = '$email'
                    LIMIT 1";
            $dt = Database::Execute($sql);

            if ($dt->Count() > 0) {
                $dt->MoveNext();
                $resetLink = create_guid();


                // update new password
                $sql = "UPDATE tbl_members
                        SET m_reset_link = '{$resetLink}',
                            m_reset_link_date = NOW(),
                            m_prev_password = m_password
                        WHERE m_id = " . $dt->m_id . "
                        LIMIT 1";
                Database::ExecuteRaw($sql);

                $sitename = $siteConfig['Company_Name'];
                # Set Up Form Mailer
                $mailer = new vMail();

                $mailer->setSubject("$sitename :: Important information about your account.");
                $mailer->setMailType("html");
                $mailer->setFrom($siteConfig['Default_From_Email'], $sitename . " Website");
                $mailer->setReplyTo($siteConfig['Default_Reply_To_Email']);
                $mailer->addRecipient($dt->m_username, $dt->m_displayname);
                $htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], "/emailtemplates/members/member-reset-password-template-email.html");

                if(file_exists($htmlFile)) {
                    $fh = fopen($htmlFile, 'r');
                    $body = fread($fh, filesize($htmlFile));
                    fclose($fh);
                }
                else {
                    throw new exception("HTML Email Template File Not Found");
                }

                $mergeFields = array();
                $mergeFields["**SITENAME**"]        = $sitename;
                $mergeFields["**MEMBERS-SLUG**"]    = $_WEBCONFIG['MEMBERS_SLUG'];
                $mergeFields["**GUEST**"]           = $dt->m_username;
                $mergeFields["**RESETLINK**"]       = $resetLink;

                $mergeFields["**WEBSITE**"]         = $_WEBCONFIG['SITE_URL'];
                $mergeFields["**DISPLAYURL**"]      = $_WEBCONFIG['SITE_DISPLAY_URL'];
                $mergeFields["**YEAR**"]            = date('Y', time());

                $body = strtr($body, $mergeFields);

                $mailer->setMessage($body);
                $mailer->sendMail();
            }
            return 'OK';

        }
    }

    //----------------------------------------------------------------------------------------------------------
    public static function _generateStrongPassword($length = 12, $add_dashes = false, $available_sets = 'luds') {
    //----------------------------------------------------------------------------------------------------------
        $sets = array();
        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if(strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&?';
        $all = '';
        $password = '';
        foreach($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if(!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while(strlen($password) > $dash_len)
        {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    //----------------------------------------------------
    public static function _generateRandomSalt($length) {
    //----------------------------------------------------
        $seed = str_split('abcdefghijklmnopqrstuvwxyz' . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' . '0123456789!@#%^&*()');
        shuffle($seed);
        $rand = '';
        foreach (array_rand($seed, $length) as $k) $rand .= $seed[$k];
        return $rand;
    }

    //----------------------------------------------------------------------------------------------
    public static function _hashPassword($password, $salt, $iterations = 64000, $alfo = 'sha512') {
    //----------------------------------------------------------------------------------------------
        return hash_pbkdf2($alfo, $password, $salt, $iterations);
    }

	//------------------------------
	public static function _secureLock($error='') {
	//------------------------------
		global $_WEBCONFIG;
		$error = urlencode($error);

        session_destroy();

		// Remove main session variable
		if (isset($_SESSION['member_id'])) {
			unset($_SESSION['member_id']);
		}
		if (isset($_SESSION['member_name'])) {
			unset($_SESSION['member_name']);
		}
		if (isset($_SESSION['member_username'])) {
			unset($_SESSION['member_username']);
		}
		if (isset($_SESSION['timemax'])) {
			unset($_SESSION['timemax']);
		}
        if (isset($_SESSION['member_multi'])) {
			unset($_SESSION['member_multi']);
		}
        if (isset($_SESSION['member_prov_num'])) {
			unset($_SESSION['member_prov_num']);
		}
        if (isset($_SESSION['member_type'])) {
			unset($_SESSION['member_type']);
		}
        if (isset($_SESSION['member_admin'])) {
			unset($_SESSION['member_admin']);
		}
        if (isset($_SESSION['member_secure_document_key'])) {
			unset($_SESSION['member_secure_document_key']);
		}
        if (isset($_SESSION['member_require_password'])) {
			unset($_SESSION['member_require_password']);
		}

		if (isset($_GET['logout'])) {
			$homeURL = $_WEBCONFIG['SITE_URL'] . "member/login?okMsg=$error";
		} else {
			$homeURL = $_WEBCONFIG['SITE_URL'] . "member/login?errMsg=$error";
		}

		redirect($homeURL);
	}

	//------------------------------
	public static function _sslCheck() {
	//------------------------------
		if($_SERVER["HTTPS"] != "on") {
			$redirect = "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: $redirect");
			exit();
		}
	}

    //------------------------------
    public static function _rolesEnabled() {
    //------------------------------
        $sql = "SELECT COUNT(urole_id) AS Count
                FROM `tbl_members_roles`";
            $record    = Database::Execute($sql);
            $record->MoveNext();
        if($record->Count > 0) {
            return true;
        } else {
            return false;
        }
    }

	//------------------------------
	private static function _userExists($user) {
	//------------------------------
		global $_WEBCONFIG;
        if(Database::TableExists('tbl_members')) {
		    $sql = "SELECT * FROM tbl_members WHERE m_username = '$user' ";
		    $dt = Database::Execute($sql);
		    $dt->MoveNext();

		    if ($dt->Count() == 0 && $user <> "") {
			    return false;
		    } else {
			    return true;
		    }
        }
        else {
            return false;
        }
	}

	//------------------------------
	public static function _getUserRoleOptions($userId = 0) {
	//------------------------------
		$sql = "SELECT urole_id, name, description
				FROM tbl_members_roles";
		$roles	= Database::Execute($sql);
		$html = '';
		if ($roles->Count() > 0) {
			while ($roles->MoveNext()) {
				$active = self::_userHasRole($roles->urole_id, $userId, false) ? 'checked' : '';
				$html .= '<label for="role-' . $roles->urole_id . '"><input id="role-' . $roles->urole_id . '" type="checkbox" name="cboRoles[]" value="' . $roles->urole_id . '" ' . $active . '> ' . $roles->name .'</label>&nbsp;&nbsp;' . PHP_EOL;
			}
		}
		return $html;
	}

	//------------------------------
	public static function _getRoleUserCount($userId) {
	//------------------------------
		$sql = "SELECT COUNT(`m_id`) AS COUNT
				FROM `tbl_members_in_roles`
				WHERE m_id = $userId";
		$record	= Database::Execute($sql);
		$record->MoveNext();
		return $record->Count;
	}

	//------------------------------
	public static function _getPageRoleOptions($cmscId = 0) {
	//------------------------------
		$sql = "SELECT urole_id, name, description
				FROM tbl_members_roles";
		$roles	= Database::Execute($sql);
		$html = '';
		if ($roles->Count() > 0) {
			while ($roles->MoveNext()) {
				$active = self::_pageHasRole($roles->urole_id, $cmscId) ? 'checked' : '';
				$html .= '<label style="font-size: 10px; font-weight: normal"><input type="checkbox" name="cboRoles[]" value="' . $roles->urole_id . '" ' . $active . '>&nbsp' . $roles->name .'</label>' . PHP_EOL;
			}
		}
		return $html;
	}

     //------------------------------
    public static function _userHasPagePermissions($userId, $cmsId = 0) {
    //------------------------------
        $sql = "SELECT `cmsv_id`, cmsv_value
                FROM `tbl_cms_name_value`
                WHERE cms_id = $cmsId AND cmsv_name = 'MemberRoles'
                LIMIT 1; ";
        $record = Database::Execute($sql);
        if($record->Count() == 0) {
            return true;
        }

        $where = '';
        $sql = "SELECT     `role_id`
                FROM `tbl_members_in_roles`
                WHERE m_id = $userId
                LIMIT 0, 1000;";
        $record = Database::Execute($sql);
        $count = 0;
        if ($record->Count() > 0) {
            while ($record->MoveNext()) {
                if($count != 0) $where .= " OR ";
                $where .=  "cmsv_value LIKE '%{$record->role_id}%'";
                $count++;
            }
        } else {
            return false;
        }
        $sql = "SELECT `cmsv_id`
                FROM `tbl_cms_name_value`
                WHERE cms_id = $cmsId AND cmsv_name = 'MemberRoles' AND ($where)
                LIMIT 1; ";
        return Database::Execute($sql)->Count() > 0;
    }

	//------------------------------
	public static function _userHasRole($roleId, $userId = '', $allowAdmin = true) {
	//------------------------------
		if($allowAdmin && $_SESSION['user_host']) return true;
		else if(strlen($userId) == 0) $userId = $_SESSION['user_id'];
		$sql = "SELECT `m_id` FROM `tbl_members_in_roles` WHERE m_id = '$userId' AND role_id = $roleId LIMIT 1";
		$roles	= Database::Execute($sql);
		return $roles->Count() > 0;
	}

	//------------------------------
	private static function _pageHasRole($roleId, $cmsId = 0) {
	//------------------------------
		$sql = "SELECT `cmsv_id`
				FROM `tbl_cms_name_value`
				WHERE cms_id = $cmsId AND cmsv_name = 'MemberRoles' AND cmsv_value LIKE '%$roleId%'
				LIMIT 1; ";
		return Database::Execute($sql)->Count() > 0;
	}
};