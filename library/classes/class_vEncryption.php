<?php
# vEncryption
# Last Updated June 18, 2015
class vEncryption
{
    private $key;
    private $encryption;
    private $mode;
    
    public function __construct($key = "TRgDcvMTbFsXmUrbXMzrcKHwzbpqaHcy", $mode = 'aes-256-cbc') {
            $this->setKey($key);
            $this->setMode($mode);
   }

   public function encrypt($message)
    {
        if (mb_strlen($this->key, '8bit') !== 32) {
            throw new Exception("Needs a 256-bit key!");
        }
        $ivsize = openssl_cipher_iv_length($this->mode);
        $iv = openssl_random_pseudo_bytes($ivsize);
        
        $ciphertext = openssl_encrypt(
            $message,
            $this->mode,
            $this->key,
            OPENSSL_RAW_DATA,
            $iv
        );
        
        return base64_encode($iv . $ciphertext);
    }

    public function decrypt($message)
    {
        $message = base64_decode($message);
        if (mb_strlen($this->key, '8bit') !== 32) {
            throw new Exception("Needs a 256-bit key!");
        }
        $ivsize = openssl_cipher_iv_length($this->mode);
        $iv = mb_substr($message, 0, $ivsize, '8bit');
        $ciphertext = mb_substr($message, $ivsize, null, '8bit');
        
        return openssl_decrypt(
            $ciphertext,
            $this->mode,
            $this->key,
            OPENSSL_RAW_DATA,
            $iv
        );
    }
	
	    /*
     *  Accessors and Mutator Functions
     */
    public function setKey($key) {
        if(empty($key))
            throw new exception("Encryption Key cannot be null or blank");

        $this->key = $key;
    }

    public function getKey() {
        return $this->key;
    }


    public function setMode($mode) {
        if(empty($mode))
            throw new exception("Mode cannot be null or blank");

        $this->mode = $mode;
    }

    public function getMode() {
        return $this->mode;
    }
   
}
?>
