<?php
namespace Forbin\Library\Classes\PageBuilder; 

class PB_Templates   { 

    //------------------------------------------------
    public static function getCustomTemplateCount(){
    //------------------------------------------------
        global $_WEBCONFIG; 

        $sql = "SELECT count(*) as Count
                FROM {$_WEBCONFIG['COMPONET_TABLE']} cms
                WHERE cms_is_historic = 0 AND cms_is_template = 1"; 
        $record = Database::Execute($sql);
        $record->MoveNext(); 
        return $record->Count; 
    }

    //---------------------------------------
    private static function getTemplates(){
    //---------------------------------------
        $sql = "SELECT * FROM tbl_cms_templates 
                ORDER BY cmst_order
                LIMIT 10"; 
        return Database::Execute($sql);
    }

    //--------------------------------------------------
    public static function makeTemplateList($what=""){
    //--------------------------------------------------
        global $_WEBCONFIG; 

        $sMarkup = '<ul class="template-list clearfix" data-what="' . $what . '" >';

        $sql = "SELECT cms_title, cms_dynamic_link, cms_published_date, cms_id, cmst_templates_id, cmst_title
                FROM {$_WEBCONFIG['COMPONET_TABLE']} cms
                INNER JOIN tbl_cms_templates ct  ON ct.cmst_id = cms.cmst_templates_id
                WHERE cms_is_historic = 0 AND cms_is_template = 1
                LIMIT 100";
        $record    = Database::Execute($sql);

        if ($record->Count() > 0) {
            while ($record->MoveNext()) {
                $sCurrent = 'template-'.preg_replace('/[^a-z]/i','-',strtolower($record->cmst_title));
                $sMarkup .= '<li class="'.$sCurrent.'" data-templateid="'.$record->cmst_templates_id.'" data-cmsid="'.$record->cms_id.'">'
                .'<ul class="rad-graphix"><li class="one1"></li><li class="two2"></li><li class="three3"></li><li class="four4"></li><li class="five5"></li></ul>'
                .'<span>'.$record->cms_title.'</span>'
                .'</li>';
            }// end while
        }

        $hasCustomTemplates = self::getCustomTemplateCount() > 0; 
        $prefix = ""; 

        $r = self::getTemplates();

        while($r->MoveNext()){
            $sCurrent = 'template-'.preg_replace('/[^a-z]/i','-',strtolower($r->cmst_title));
            $sMarkup .= 
            '<li class="'.$sCurrent.'" data-templateid="'.$r->cmst_id.'">'
            .'<ul class="rad-graphix"><li class="one1"></li><li class="two2"></li><li class="three3"></li><li class="four4"></li><li class="five5"></li></ul>'
            .'<span>' . $prefix . ' '.$r->cmst_title.'</span>'
            .'</li>';

        }

        return $sMarkup.'</ul><input type="hidden" name="template-select" id="template-select" value="'.$_WEBCONFIG['DEFAULT_LAYOUT_ID'].'"/>';
    }

    //--------------------------------------------------------------    
    public static function makeLayoutList($selectedTemplateId=''){
    //--------------------------------------------------------------
        global $_WEBCONFIG; 
        if($selectedTemplateId != ''){ $_WEBCONFIG['DEFAULT_LAYOUT_ID'] = $selectedTemplateId;}

        $sMarkup = '<ul class="template-list clearfix layout-list" >';

        $r = self::getTemplates();

        while($r->MoveNext()){
            $sCurrent = 'template-'.preg_replace('/[^a-z]/i','-',strtolower($r->cmst_title));
            if($selectedTemplateId == $r->cmst_id) {$sCurrent .= ' current-template';}

            $sMarkup .= 
            '<li class="'.$sCurrent.'" data-templateid="'.$r->cmst_id.'">'
            .'<ul class="rad-graphix"><li class="one1"></li><li class="two2"></li><li class="three3"></li><li class="four4"></li><li class="five5"></li></ul>'
            .'<span>'.$r->cmst_title.'</span>'
            .'</li>';

        }

        return $sMarkup.'</ul><input type="hidden" name="template-select" id="template-select" value="'.$_WEBCONFIG['DEFAULT_LAYOUT_ID'].'"/>';
    }

    //-----------------------------------------------------------------------------    
    public static function loadTemplateFile($templateId='101',$outPutTitle=true){
    //-----------------------------------------------------------------------------
        global $_WEBCONFIG; 

        // Get Template
        $sql = "SELECT cmst_name 
                FROM tbl_cms_templates 
                WHERE cmst_id = {$templateId}
                LIMIT 1";
        $template = Database::Execute($sql);
        $template->MoveNext();

        if ($template->Count() == 0) {
            throw new Exception("Template ID: " . $templateId . " Not Found"); 
        }       
        
        $layoutMarkup = file_get_contents(filePathCombine($_SERVER['DOCUMENT_ROOT'], '/templates/', $template->cmst_name));
        if($outPutTitle === false){
            $layoutMarkup = preg_replace('#<div class="page-title">(.*?)</div>#s', '', $layoutMarkup);
        }
        return $layoutMarkup; 
    }
};
?>