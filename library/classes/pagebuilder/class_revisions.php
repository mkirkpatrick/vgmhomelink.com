<?php
namespace Forbin\Library\Classes\PageBuilder; 
class PB_Revisions   { 
	
	//------------------------------
	public static function isRevision($id){
	//------------------------------
		$r = Database::Execute('SELECT * FROM tbl_cms WHERE cms_id='.Database::quote_smart($id));
		$r->MoveNext();
		if($r->cms_is_historic == 1){ return $r->cms_historic_cms_id + 0; }	
		return false;
	}
	
	//------------------------------
	public static function hasDraft($id, $date){
	//------------------------------
		$sql = "SELECT cms_id FROM tbl_cms 
				WHERE cms_historic_cms_id=" . $id . " AND cms_published_date > '$date'
				LIMIT 1"; 
		return Database::Execute($sql)->Count();
	}

	//----------------------------------------	
	public static function getRevisions($id){
	//----------------------------------------	 
		// if this page is a revision, then get it's source page
		if( ($sourcePageId = self::isRevision($id)) !== false ){ $id = $sourcePageId ; }
		
		return Database::Execute('SELECT *, unix_timestamp(tbl_cms.cms_published_date) AS unix_time FROM tbl_cms 
									LEFT JOIN tbl_user  ON tbl_cms.cms_user_id = tbl_user.user_id
									WHERE cms_is_historic = 1 AND cms_historic_cms_id = ' . Database::quote_smart($id) . ' 
									ORDER BY cms_published_date DESC');
	}
	
	//---------------------------------------------	
	public static function getLastRevisionId($id){
	//---------------------------------------------
		if( ($sourcePageId = self::isRevision($id)) !== false ){ $id = $sourcePageId ; }
		$sql = "SELECT cms_id FROM tbl_cms 
				WHERE cms_historic_cms_id = " . (int)$id . " OR cms_id  = " . (int)$id . "
				ORDER BY cms_published_date DESC
				LIMIT 1;"; 
		$record = Database::Execute($sql); 
		if($record->count() > 0) {
			$record->MoveNext(); 
			return (int)$record->cms_id; 
		}
		return false; 
	}
	
	//------------------------------
	public static function purgeRevisions() {
	//------------------------------
		global $_WEBCONFIG;

		$historicRecord = Database::Execute("SELECT `cms_id` FROM tbl_cms WHERE cms_is_historic = 1"); 
		while ($historicRecord->MoveNext()) {
			$cmscHistoricId = (int) PB_Helper::getCmscId($historicRecord->cms_id); 
			Database::ExecuteRaw("DELETE FROM `tbl_cms` WHERE `cms_id` = {$historicRecord->cms_id}");
			Database::ExecuteRaw("DELETE FROM `tbl_cms_name_value` WHERE `cms_id` = $cmscHistoricId");
		}
	}

	//------------------------------
	public static function previewLink($cmsContent) {
	//------------------------------
		$navLink = '';
		if ($cmsContent->cms_dynamic_id == "STATIC" || $cmsContent->cms_dynamic_id == "PDF-DOC") {
				$navLink = $cmsContent->cms_dynamic_link;
		}
		else if ($cmsContent->cmst_templates_id > 0) {
			$navLink = '/pagebase.php?preview=true&amp;pbid=' . $cmsContent->cms_id;
		}
		return $navLink;
	}
	
	//------------------------------	
	public static function getPageOrRevisionData($id){ 
	//------------------------------	 
		return Database::Execute('SELECT *, unix_timestamp(cms_published_date) as unix_time FROM tbl_cms 
									LEFT JOIN tbl_user  ON tbl_cms.cms_user_id = tbl_user.user_id
									WHERE cms_id = ' . Database::quote_smart($id));
	}
	
	//------------------------------	
	public static function getPageData($id){ 
	//------------------------------	 
		// if this page is a revision, then get it's source page
		if( ($sourcePageId = self::isRevision($id)) !== false ){ $id = $sourcePageId ; }

		return Database::Execute('SELECT *, unix_timestamp(cms_date_modified) as unix_time FROM tbl_cms 
									LEFT JOIN tbl_user  ON tbl_cms.cms_user_id = tbl_user.user_id
									WHERE cms_is_historic = 0 AND cms_id = ' . Database::quote_smart($id));
	}
	
	//------------------------------	
	public static function makeRevisionList($pageid){
	//------------------------------	
		global $_WEBCONFIG; 

		if($pageid == ''){ return false; }
		$viewId = -1; if(isset($_GET['id']) && !empty($_GET['id'])){ $viewId = $_GET['id']; }
		$sMarkup = null; 

		$r = self::getRevisions($pageid);
		while($r->MoveNext()){
			$sCurrent = '';
			if($viewId == $r->cms_id) {$sCurrent = 'current';}				
			if($r->user_username <> "") { 
				$sMarkup .= 
					'<li class="'.$sCurrent.'">'
						. ' <span class="date">'.date("m/d/y g:i A",$r->unix_time)  .'</span>'
						. ' <span class="user"><a data-userid="'.$r->cms_user_id.'" href="/VPanel/users/index.php?view=modify&amp;id='.$r->cms_user_id.'">'.$r->user_username.'</a></span>'
						. ' <!-- #'. $r->cms_id .' -->'
					. ' <ul>
							<li><a class="openlink" href="/vpanel/pagebuilder/index.php?view=revision&amp;parentId='.$r->cms_parent_id.'&amp;id='.$r->cms_id.'">Open</a></li>
							<li><a class="openLink" href="' . urlPathCombine($_SERVER['ROOT_URI'], "/pagebuilder/compare.php?cid=" . $pageid . "&amp;rid=" . $r->cms_id) . '" target="_blank">Compare</a></li>
					     	<li><a target="_blank" href="'.self::previewLink($r).'">View</a></li>
						</ul>
					</li>';
			}
			else { 
				$sMarkup .= 
					'<li class="'.$sCurrent.'">'
						. ' <span class="date">'.date("m/d/y g:i A",$r->unix_time)  .'</span>'
						. ' <span class="user">@forbin-admin</span>'
						. ' <!-- #'. $r->cms_id .' -->'
						. ' <ul>
							<li><a class="openlink" href="/vpanel/pagebuilder/index.php?view=revision&amp;parentId='.$r->cms_parent_id.'&amp;id='.$r->cms_id.'">Open</a></li>
							<li><a class="openLink" href="' . urlPathCombine($_SERVER['ROOT_URI'], "/pagebuilder/compare.php?cid=" . $pageid . "&amp;rid=" . $r->cms_id) . '" target="_blank">Compare</a></li>
					     	<li><a target="_blank" href="'.self::previewLink($r).'">View</a></li>
						</ul>
					</li>';
			}
		}
		
		return $sMarkup.'';
	}
	 	
	// makes a duplicate of given page as a revision
	//------------------------------	
	public static function saveRevision($pageid){
	//------------------------------	
		$r = Database::Execute("SELECT * FROM tbl_cms WHERE cms_id = " . (int)$pageid);
		if($r->Count() == 0){  return false; }
		$r->MoveNext(); 

		// duplicate tbl_cms record
		Database::ExecuteRaw("INSERT INTO tbl_cms 
			(
				cms_parent_id, cms_title, cms_is_active,
				cms_show_nav, cms_published_date, cms_display_order,
				cms_is_historic, cms_user_id,
				cms_historic_cms_id, cms_is_deleted, url_direct,
				cms_date_created, cms_is_published, 
				cmst_templates_id, cms_dynamic_id,
				cms_dynamic_link, cms_date_modified

			) values (
				" . $r->cms_parent_id . ", '" . Database::quote_smart($r->cms_title) . "', " . $r->cms_is_active . ",
				" . $r->cms_show_nav . ",  '" . $r->cms_published_date . "', " . $r->cms_display_order . ",
				1, " . $r->cms_user_id . ",
				" . (int)$pageid . ", " . $r->cms_is_deleted.", '" . $r->url_direct . "',
				'" . $r->cms_date_created . "', " . $r->cms_is_published . ",
				'" . $r->cmst_templates_id . "', '" . $r->cms_dynamic_id . "',
				'" . $r->cms_dynamic_link . "', NOW()
			)");
			
		$revId = Database::getInsertID();

		// duplicate each tbl_cms_name_value
		Database::ExecuteRaw("INSERT INTO tbl_cms_name_value 
			(
				 cms_id, cmsv_name, cmsv_value
			)				
			SELECT $revId, cmsv_name, cmsv_value
			FROM tbl_cms_name_value WHERE cms_id = " . (int)$pageid . "
		");	

		return $revId; 
	}
};
?>

