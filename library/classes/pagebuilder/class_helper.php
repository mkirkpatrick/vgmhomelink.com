<?php
namespace Forbin\Library\Classes\PageBuilder; 
class PB_Helper { 

    //------------------------------------------
    public static function getParentId($cmsid){
    //------------------------------------------
        $cmsid= (int)$cmsid; 
        $record = Database::Execute("SELECT cms_parent_id FROM tbl_cms WHERE cms_id = $cmsid LIMIT 1");
        if($record->count() > 0) { 
            $record->MoveNext();
            return (int)$record->cms_parent_id; 
        } else {
            return null;
        }
    }

    //------------------------------------------
    public static function hasChildren($cmsid){
    //------------------------------------------
        $cmsid= (int)$cmsid; 
        $sql = "SELECT cms_id FROM tbl_cms WHERE cms_parent_id = $cmsid LIMIT 1"; 
        $record = Database::Execute($sql);
        if($record->count() > 0) { 
            return true;  
        } else {
            return false; 
        }
    }

    //-----------------------------------------------
    public static function getGrANDParentId($cmsid){
    //-----------------------------------------------
        $parentId = self::getParentId($cmsid); 
        if(!is_null($parentId)) {
            return (int)self::getParentId($parentId); 
        } else { 
            return null; 
        }
    }

    //----------------------------------------------
    public static function getRootParentId($cmsid){
    //----------------------------------------------
        $id = $cmsid;
        while(true) {
            $newId = self::getParentId($id); 
            if(!is_null($newId) && $newId != 0) {
                $id = $newId; 
            } else {
                break; 
            }
        }
        return $id; 
    }

    //------------------------------------------
    public static function cms_url_word($name){
    //------------------------------------------
        return preg_replace('/[^0-9a-z_]/i','',preg_replace('/ /','-',trim($name)));
    }

    //-------------------------------------------
    public static function cms_link($resultRow){		
    //-------------------------------------------
        return $resultRow->cms_dynamic_link; 
    }

    //---------------------------------------------------
    public static function getCmsValuesForPage($cms_id){
    //---------------------------------------------------
        $values = Array();
        Database::Execute('SELECT * FROM tbl_cms LIMIT 1');
        $sql = "SELECT p.cms_id, v.cmsv_id, v.cmsv_name as k, v.cmsv_value as value FROM tbl_cms p
        LEFT JOIN tbl_cms_name_value v  ON p.cms_id = v.cms_id
        WHERE p.cms_id = " . mysql_real_escape_string($cms_id);
        $r = Database::Execute($sql);
        while($r->MoveNext()){ $values[$r->k] = $r->value;}
        return $values;
    }

    //-----------------------------------------------
    public static function getActiveCount($cms_id) {
    //-----------------------------------------------    
        $sql = "SELECT cms_id 
                FROM tbl_cms 
                WHERE cms_is_active = 1 AND cms_is_system_page = 0 AND cms_is_published = 1 AND cms_is_historic = 0 AND cms_is_deleted = 0 AND (cms_dynamic_id IS NULL OR cms_dynamic_id = '') AND cms_id <> $cms_id";
        $record = Database::Execute($sql);
        return $record->Count();
    }    

    //----------------------------------------
    public static function printAssetList() {
    //----------------------------------------
        global $_WEBCONFIG;
        $rows = 0; 
        $where = $_WEBCONFIG['SITE_TYPE'] == 'MICROVOX' && !UserManager::isWebmaster() ? " AND ca.at_id <> 3 AND ca.at_id <>5" : "";

        $sql = "SELECT * FROM tbl_cms_asset ca
                INNER JOIN tbl_cms_asset_type at ON at.at_id = ca.at_id
                WHERE as_status = 'Active'$where
                ORDER BY at.at_type,ca.as_key 
                LIMIT 1000";
        $asset = Database::Execute($sql);

        print '<table id="assetTable" cellpadding="2" cellspacing="2" style="font-size: 10px" class="grid-display">
                    <thead>
                        <tr>                 
                            <th style="text-align: left; width: 60px">Type</th>
                            <th style="text-align: left; width: 175px">Name</th>
                            <th style="text-align: left; width: 175px">Value</th>
                            <th style="text-align: left; width: 80px"></th>
                        </tr>
                    </thead>
                    <tbody>';

                    if ($asset->Count() > 0) {
                        while ($asset->MoveNext()) {
                            $addContainer = $asset->at_type != 'Variable' ? "true" : "false"; 
                            print '<tr class="data">                             
                            <td>' . $asset->at_type . '</td>
                            <td>' . $asset->as_name . '</td>                            
                            <td>' . str_truncate(strip_tags($asset->as_value), 50) . '</td>
                            <td><span style="display: inline-block; width: 80px" class="k-button insertBtn" data-addcontainer="' . $addContainer . '" data-key="' . $asset->as_key . '">Insert Asset</span></td>
                            </tr>' . PHP_EOL;
                        }// end while
                    }

        print '</tbody>
            </table>'; 
    }

    //------------------------------------------------
    public static function generateDirectUrl($cmsId){
    //------------------------------------------------
        global $_WEBCONFIG; 

        $permaLink = ""; 
        $entity = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $cmsId); 
        while(!is_null($entity) && $entity->cms_id != $_WEBCONFIG['SITE_ROOT_PAGE']) { 
            $permaLink = trim(preg_replace('/[^a-z0-9]+/', '-', strtolower($entity->cms_title)), '-') . "/" . $permaLink;
            $entity = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $entity->cms_parent_id); 
        }
        return trim($permaLink, "/"); 
    }

    //------------------------------------------
    public static function pageToLink($entity){
    //------------------------------------------
        $path = explode("/", $entity->url_direct);
        return '/pagebase.php?pbid='. (int) $entity->cms_id . '&pbpg=' . urlencode($path[count($path) - 1]) . '&pbpath=' . urlencode($entity->url_direct);
    }

    //--------------------------------------------------
    public static function getLANDingPageTemplateId() {
    //--------------------------------------------------
        $sql = "SELECT cms_id AS ID FROM tbl_cms WHERE cms_is_template = 2 LIMIT 1";
        $rec = Database::Execute($sql);
        if ($rec->Count() > 0) {
            $rec->MoveNext();
            return $rec->ID; 
        } else { 
            return null; 
        }
    }

    //------------------------------------------------
    public static function cmsTreeTableView($index) { 
        //--------------------------------------------
        global $_WEBCONFIG;

        $sMarkup = '';

        $sql = "SELECT cms_id, cms_is_active, cms_title, cms_is_secure, cms_published_date, cms_dynamic_link, cms_dynamic_id, cms_is_editable, cms_is_system_page 
                FROM tbl_cms 
                WHERE cms_is_deleted = 0 AND cms_is_historic = 0 AND cms_is_template = 0 AND cms_parent_id = $index
                ORDER BY cms_parent_id, cms_display_order, cms_id, cms_title";     
        $r = Database::Execute($sql); 
        if ($r->Count() == 0 && $index != $_WEBCONFIG['SITE_ROOT_PAGE'])  return;

        if ($index == $_WEBCONFIG['SITE_ROOT_PAGE']) {
            $sMarkup .= '<ul class=" vpanel-table" ><li class="head clearfix"><div class="left">Page Name</div><div class="right">Options</div></li>' . "\n";
            // home page markup
            $sMarkup .= '<li class="home-page" data-id="' . $_WEBCONFIG['SITE_ROOT_PAGE'] . '" data-parent="0">'
            .'<div class="con clearfix">'
            .'<div class="left"><a class="nubs"></a>'
            .'<h3><a href="javascript:void(0)" class="view-page-link" data-id="HOME" data-link="" data-linktype="">Home Page</a></h3><span>&nbsp;</span></div>';

            $sql = "SELECT cms_id, cms_published_date 
                    FROM tbl_cms
                    WHERE cms_is_deleted = 0 AND cms_is_historic = 0 AND cms_is_system_page = 0 AND cms_is_template = 0 AND cms_parent_id = '0' AND cms_id = '$index'";
            $homePage = Database::Execute($sql);
            $homePage->MoveNext();

            if (PB_Revisions::hasDraft($homePage->cms_id, $homePage->cms_published_date)) {
                $sMarkup .= '<div class="unpublishedChanges"><img src="/VPanel/images/icons/unpublished-mini.png" alt="Unpublished Changes" />&nbsp;&nbsp;Unpublished Changes</div>'; 
            }

            $sMarkup .= '<div class="right">'
            .'<a href="?view=modify&amp;parentId=0&amp;id=' . $_WEBCONFIG['SITE_ROOT_PAGE'] . '" class="icon-pencil silver button mini">Edit</a>'
            .'</div>'
            .'</div>'
            .'</li>'. "\n";
        } else {
            $sMarkup .= "<ul class=\"subpages\" data-parent=\"".$index."\">\n";
        }

        while ($r->MoveNext()) {
            if(($r->cms_is_system_page == 1 && $r->cms_is_editable == 1) || $r->cms_is_system_page == 0) {
                // get child count
                $sql = "SELECT count(*) AS count 
                        FROM tbl_cms 
                        WHERE cms_is_deleted = 0 AND  cms_is_historic = 0 AND cms_parent_id = {$r->cms_id} AND (cms_is_system_page = 0 OR (cms_is_system_page = 1 AND cms_is_editable = 1))"; 
                $recCount = Database::Execute($sql);
                $recCount->MoveNext();
                $childCount = $recCount->count;

                $sChildMarkup = '<span>&nbsp;</span>';
                if ($childCount != 0) { $sChildMarkup = '<span data-count="' . $childCount . '">' . $childCount . ' Subpages</span>'; }else{}

                // active /inactive
                $sActiveClass = '';
                if ($r->cms_is_active != 1) { $sActiveClass = "inactive"; }

                $sMarkup .= "<li class=\"{$sActiveClass}\" data-secure={$r->cms_is_secure} data-id=\"{$r->cms_id}\" data-parent=\"" . $index . "\"><div class=\"con clearfix\">";

                $sName  = $r->cms_is_active ? '<a href="javascript:void(0)" class="view-page-link" data-id="' . $r->cms_id . '" data-link="' . $r->cms_dynamic_link . '" data-linktype="" title="View Live Page">' : '';
                $sName .=      $r->cms_title;
                $sName .= $r->cms_is_active ? '</a>' : '';

                $sMarkup .= '<div class="left"><a class="nubs"></a><h3>' . $sName . '</h3>'.$sChildMarkup.'</div>';

                if($r->cms_is_secure) {
                    $sMarkup .= '<div class="secureLock"><img src="/VPanel/images/icons/padlock-closed.png" alt="Members Only" />&nbsp;&nbsp;Members</div>'; 
                }

                if(PB_Revisions::hasDraft($r->cms_id, $r->cms_published_date)) {
                    $sMarkup .= '<div class="unpublishedChanges"><img src="/VPanel/images/icons/unpublished-mini.png" alt="Unpublished Changes" />&nbsp;&nbsp;Unpublished Changes</div>'; 
                }

                $sMarkup .= '<div class="right">'
                .'<a href="?view=modify&amp;parentId=' . $index . '&amp;id=' . $r->cms_id . '" class="floatLeft icon-pencil edit silver button mini">Edit</a>';
                // if a system page don't allow user to delete
                if($r->cms_is_system_page == 0 || UserManager::isWebmaster()) {
                    $sMarkup .= '<a href="javascript:void(0)" class="floatLeft red icon-cancel delete button mini" data-id="' . $r->cms_id . '" data-parent="' . $index . '" >Delete</a>';
                }
                $sMarkup .= '<ul class="add-list">
                <li>+</li> 
                <li><a href="?view=add&amp;childof=' . $r->cms_id . '" class="add-page">Page</a> </li> 
                <li><a href="?view=add&amp;what=doc&amp;childof=' . $r->cms_id . '" class="add-doc">Doc</a> </li>
                <li><a href="?view=add&amp;what=link&amp;childof=' . $r->cms_id . '" class="add-link">Link</a></li>
                </ul>'
                .'</div></div>';
                $sMarkup .= PB_Helper::cmsTreeTableView($r->cms_id);
                $sMarkup .= "</li>\n";
            }
        }

        return $sMarkup .= "</ul>\n";
    }    

    //----------------------------------------------------------------------------
    public static function cmsPageRadioList($index,$hideId,$bDisabled = false ) { 
    //----------------------------------------------------------------------------
        global $_WEBCONFIG;

        $sMarkup = '';
        $disabled = "";
        $disabledClass = 'select-category';
        if($bDisabled ){ $disabledClass = ''; $disabled = ' disabled="disabled" '; }

        $s = "SELECT * FROM tbl_cms 
              WHERE cms_parent_id = $index AND cms_is_deleted = 0 AND cms_is_historic = 0 AND (cms_is_system_page = 0 OR (cms_is_system_page = 1 AND cms_is_editable = 1)) AND cms_is_template = 0 
              ORDER BY cms_parent_id, cms_display_order, cms_id, cms_title";         
        $r = Database::Execute($s); 

        if ($r->Count() == 0 ) return;

        if ($index == $_WEBCONFIG['SITE_ROOT_PAGE']) {
            $checked = "";
            if ($GLOBALS['actualParentId'] == $_WEBCONFIG['SITE_ROOT_PAGE']) { $checked = ' checked="checked" '; }

            if(UserManager::isWebmaster()) {
                $sMarkup .= '<ul class="category-list" ><li class="top"><label class="'.$disabledClass.'"><input '.$disabled.' type="radio" name="parent-category-page" id="parent-category-page-0" value="' .$_WEBCONFIG['SITE_ROOT_PAGE'] .'" '.$checked.'/>[&nbsp;&nbsp;Root Level&nbsp;&nbsp;]</label><ul class="children" data-parent="-1">' . "\n";
            }
            else {
                $sMarkup .= '<ul class="category-list" ><ul class="children" data-parent="-1">' . "\n";    
            } 
        } else {
            $sMarkup .= '<ul class="children" data-parent="'.$index.'">'."\n";
        }

        while ($r->MoveNext()) {
            // skip self
            if($r->cms_id == $hideId ){continue;}
            // active /inactive
            $sActiveClass = '';
            if($r->cms_is_active != 1){$sActiveClass="inactive";}else{} 

            $sMarkup .= "<li class=\"{$sActiveClass}\" data-id=\"{$r->cms_id}\" data-parent=\"".$index."\">"; $checked =  ""; 

            if (isset($GLOBALS['actualParentId']) && $GLOBALS['actualParentId'] == $r->cms_id) { $checked = ' checked="checked" '; } else {}
            $sName  = '<label class="' . $disabledClass . '">';
            $sName .= '<input ' . $disabled . ' type="radio" name="parent-category-page" id="parent-category-page-' . $r->cms_id . '" value="' . $r->cms_id . '" ' . $checked . ' />';
            $sName .= $r->cms_title . "\n";
            $sName .= '</label>' . "\n";

            $sMarkup .= '' . $sName . '';
            $sMarkup .= PB_Helper::cmsPageRadioList($r->cms_id,$hideId, $bDisabled);
            $sMarkup .= "</li>\n";
        }

        if ($index == $_WEBCONFIG['SITE_ROOT_PAGE']) {
            return $sMarkup .= "</ul></li></ul>\n";
        } else {
            return $sMarkup .= "</ul>\n";
        }
    }  

    //------------------------------------------------
    public static function generateBreadCrumbName() {
    //------------------------------------------------
        global $_WEBCONFIG, $iNum; 
        $name = ""; 
        $entity = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $iNum); 
        while(!is_null($entity) && $entity->cms_id != $_WEBCONFIG['SITE_ROOT_PAGE']) { 
            $name = $entity->cms_title . " :: " . $name; 
            $entity = Repository::GetById($_WEBCONFIG['COMPONET_TABLE'], $entity->cms_parent_id); 
        }
        return rtrim($name, " :: "); 
    }      
};
?>

