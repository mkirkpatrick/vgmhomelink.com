<?php
/* 
    //////////////////////// Forbin VMail 1.5 //////////////////////
    
    @author  -- Matthew Kauten
    @version -- July 31, 2014
    @created -- February 20, 2010 
    @company -- VGM Forbin
    
    -- Add Ability to Attach Files
    
*/ 

class vMail {
        private $type;
        private $headers;
        private $recipients;
        private $cc;
        private $bcc;
        private $subject;
        private $message;
        private $attachments;
        private $boundary;
        private $encoding;

        public function __construct() {
            $this->headers = array();
            $this->recipients = array();
            $this->cc = array();
            $this->bcc = array();
            $this->attachments = array();
            $this->boundary = '_' . md5(uniqid(time()));
            $this->type = "text";
            $this->encoding = "8bit";
            $this->subject = "";
            $this->message = "";

            # Prepare Headers Necessarily to send email
            $this->headers[]  = "MIME-Version: 1.0" . "\r\n";
            //$this->headers[]  = "PHP-Version: " . phpversion() . "\r\n";
            //$this->headers[]  = "X-Mailer: VMail 1.4 \r\n";
            //$this->headers[]  = "Content-Transfer-Encoding: {$this->encoding}\r\n";
        }

        /* Sets who the email was from */
        public function setFrom($email = "", $name = "") {
            if(!$this->validEmail($email)) {
                throw new exception( "--From-- email address ($email) is not valid");
            }
            else {
                $address = $this->formatAddress($email, $name);
                $this->addHeader('From', $address);
            }
        }

        /* Sets what email address should be replied to */
        public function setReplyTo($email = "", $name = "") {
            if(!$this->validEmail($email)) {
                throw new exception( "--ReplyTo-- email address ($email) is not valid");
            }
            else {
                    $address = $this->formatAddress($email, $name);
                    $this->addHeader('Reply-To', $address);
            }
        }

        /* Adds a custom header */
        public function addHeader($name = "", $value = "") {  
            $this->headers[] = $name. ': ' . $value . "\r\n"; 
        }

        /* Adds a recipient in the to: field */
        public function addRecipient($email = "", $name = "") {
            if(!$this->validEmail($email)) {
                throw new exception( "--Recipient-- email address ($email) is not valid");
            }
            else {
                $address = $this->formatAddress($email, $name);
                $this->recipients[] = $address;
            }
        }

        /* Adds a recipient in the cc: field */
        public function addCarbonCopy($email = "", $name = "") {
            if(!$this->validEmail($email)) {
                throw new exception( "--CarbonCopy-- ($email) is not valid");
            }
            else {
                $address = $this->formatAddress($email, $name);
                $this->cc[] = $address;
            }
        }

        /* Adds a recipient in the bcc: field */
        public function addBlindCopy($email = "") {
            if(!$this->validEmail($email)) {
                throw new exception( "--Blind Carbon Copy-- email address ($email) is not valid");
            }
            else {
                $address = $this->formatAddress($email);
                $this->bcc[] = $address;
            }
        }

        /* Sets the message subject */
        public function setSubject($subject) {  
            $this->subject = $subject;
        }

        /* Sets the mail type [text, html]  */
        public function setMailType($type) {  
            $this->type = $type; 
        }

        /* Sets the message body */
        public function setMessage($message) {  
            $this->message = $message; 
        }

        /* Adds to the message body */
        public function addtoMessage($text) {  
            $this->message .= $text . "\r\n";
        }

        public function clearMessage() {  
            $this->message = ""; 
        }

        /* Returns the Full Message */
        public function getMessage() {
            $attach = "";
			if(!empty($this->attachments)) {
                foreach($this->attachments as $file) {
                    $attach .= $file;
                }
				$attach = rtrim($attach, "\n");
                return "--".$this->boundary . "\r\n"
                           . 'Content-Type: text/html; charset="iso-8859-1" . "\r\n"
                             Content-Transfer-Encoding: 7bit' . "\r\n\r\n"
                           .  $this->message . "\r\n" . "--".$this->boundary . "\r\n" . $attach . "--\r\n";
            } else {
                return $this->message; 
            }
        }

        /* Attach a file to the message */
        public function attachfile($file, $type = "application/pdf") {
            if(!($fd = fopen($file, "r"))) {
                throw new exception ("Error Opening $file, Unable to Attach");
                return false;
            }

            $fname = basename($file);

            // Convert to base64 because mail attachments are not binary safe.
            $buffer = chunk_split(base64_encode(file_get_contents($file)));

            //$this->attachments[$file] = "--" . $this->boundary . "\r\n";
            $this->attachments[$file] = "Content-Type: $type; name=\"$fname\"\r\n";
            $this->attachments[$file] .= "Content-Transfer-Encoding: base64\r\n";
            $this->attachments[$file] .= "Content-Disposition: attachment; filename=\"$fname\"\r\n\r\n";
            $this->attachments[$file] .= $buffer . "\r\n\r\n"; 
            $this->attachments[$file] .= "--" . $this->boundary . "\n";

            return true;
         }

        /* Sends the email */
        public function sendMail() {
            // Prepare the message body
            $body = $this->getMessage();

            // Prepare Recipients
            $recipients_separated = implode(",", $this->recipients);

            // Prepare CC
            foreach($this->cc as $email){
                    $this->addHeader('CC', $email);
            }

            // Prepare BCC
            $bccEmails = ""; 
            foreach($this->bcc as $email) { 
                $bccEmails .= $email . ',';
            }
            if($bccEmails != "") { 
                $this->addHeader('BCC', $bccEmails);
            }

            // Prepare Headers
            $this->prepareMail();
            $headers = "";
            foreach($this->headers as $header){
                    $headers .= $header;
            }

            // Check for errors
            if(count($this->recipients) < 1){
                    throw new exception ("You need to add at least one recipient");
            }
            if (empty($this->message)){
                    throw new exception ("Message body has not been set");
            }
            if (empty($this->subject)){
                    throw new exception ("Email Subject has not been set");
            }
            
            //print($headers); 
            //die($body); 
            if(!mail($recipients_separated, $this->subject, $body, $headers)) {
                throw new exception ("Send Mail Failed");
            }
        }

        private function validEmail($email){
            $regex = "/^[_+a-z0-9-]+(\.[_+a-z0-9-]+)*"
                            ."@[a-z0-9-]+(\.[a-z0-9-]{1,})*"
                            ."\.([a-z]{2,}){1}$/i";
            if(strstr($email, "<") && strstr($email, ">")){
                    $email = trim(stripslashes(preg_replace("'^.*?<|>.*$'si", "", $email)));
            }
            if(!preg_match($regex,$email) && !empty($email)) {
                return false;
            }
            else {
                return true;
            }
        }

        private function prepareMail(){
            # Check for Attachments
            if (count($this->attachments) > 0){
                    $this->headers[]  = "Content-Type: multipart/mixed; boundary=\"$this->boundary\" \r\n";
            }

            # Check Type
            else if (trim(strtolower($this->type)) == "text"){
                    $this->headers[]  = "Content-Type: text/plain; charset=ISO_8859-1\r\n";
            }
            else if (trim(strtolower($this->type)) == "html"){
                    $this->headers[] =     "Content-Type: text/html; charset='iso-8859-1'";
            }
            else{
                throw new exception ("Unknown Mail Type Set - " . $this->type);
            }
        }

        private function formatAddress($email = '', $name = '')
        {
            if(empty($name)){
                $address = $email;
            }
            else{
                $address = sprintf('"%s" <%s>', addslashes($name), $email);
            }

            return $address;
        }
}
