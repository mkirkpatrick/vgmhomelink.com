<?php
class Authentication {
    public static function authenticate_user() {
        global $_WEBCONFIG,$siteConfig;
        if(!isset($_SESSION['LOGIN_ATTEMPTS'])) {
            $_SESSION['LOGIN_ATTEMPTS'] = 0;
        }
        // Allow Raven Tools Through
        if($_SERVER['HTTP_USER_AGENT'] == "RavenCrawler") {
            // If Raven Hasn't been logged in, do so
            if(!isset($_SESSION['AUTH_USERNAME'])) {
                $_SESSION['AUTH_USERNAME'] = "Raven Tools Crawler";
                $_SESSION['LOGIN_ATTEMPTS'] = 0;
                EventLogger::_logEvent('Staging Site Login', 'Login Successful', "Raven Tools Crawler");
            }
            return true;            
        } elseif($_WEBCONFIG["LOCAL_IP"] != $_SERVER['HTTP_CLIENT_IP'] && $_SERVER['HTTP_CLIENT_IP'] != '127.0.0.1' && $_WEBCONFIG['SITE_STATUS'] != "LIVE") {
            if(!isset($_SESSION['AUTH_USERNAME'])) {
                if($_SESSION['LOGIN_ATTEMPTS'] >= 5) {
                    EventLogger::_logEvent('Staging Site Login', 'Max Login Attempts Reached - ' . $_SESSION['LOGIN_ATTEMPTS'], 'Previous User');        
                    serverTransfer("/401error.php");
                } else {
                    serverTransfer("/site-login/login.php");    
                }
            } else {
                return true;
            }            
        } else {
            return true;
        }
    }
    
    public static function login_user($username, $password) {
        global $_WEBCONFIG,$siteConfig;
        
        // Allowable login credentials beside Active Directory
        $credentalsArray = array($_WEBCONFIG['SITE_ACCESS_USER']=>$_WEBCONFIG['SITE_ACCESS_PASSWORD'],$_WEBCONFIG['SITE_ACCESS_USER_2']=>$_WEBCONFIG['SITE_ACCESS_PASSWORD_2']);        

        // Is user in the active directory?
        if(ActiveDirectory::validateWebmasterAdmin($username, $password)){
            // Allow user through
            $_SESSION['AUTH_USERNAME'] = $username;
            $_SESSION['LOGIN_ATTEMPTS'] = 0;
            EventLogger::_logEvent('Staging Site Login', 'Login Successful', $username);
            return true; 
        // Is the user in the credental array above?               
        } elseif(array_key_exists($username,$credentalsArray)) {
            if($credentalsArray[$username] == $password) {
                // Allow user through
                $_SESSION['AUTH_USERNAME'] = $username;
                $_SESSION['LOGIN_ATTEMPTS'] = 0;
                EventLogger::_logEvent('Staging Site Login', 'Login Successful', $username);
                return true;
            // User doesn't exist
            } else {
                $_SESSION['LOGIN_ATTEMPTS']++;
                unset($_SESSION['AUTH_USERNAME']);
                EventLogger::_logEvent('Staging Site Login', 'Login Failed', $username);
                return false;
            }
        // User doesn't exist
        } else {
            $_SESSION['LOGIN_ATTEMPTS']++;
            unset($_SESSION['AUTH_USERNAME']);
            EventLogger::_logEvent('Staging Site Login', 'Login Failed', $username);
            return false;   
        }
    }    
};
?>