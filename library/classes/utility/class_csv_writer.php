<?php

class CSV_Writer
{
    const COMMA = ","; 
    const NEWLINE = "\n";
     
    private $content;
    private $filename; 
    
    public function __construct($filename = 'data.csv')
    {
		$this->filename = $filename; 
        $this->clearContent();
    }
    
    public function clearContent()
    {
        $this->content = ""; 
    }
    
    public function addItem($row = "")
    {
		$this->content = $this->content . $row . self::COMMA;
    }
    
    public function newLine()
    {
        $this->content = $this->content . self::NEWLINE;
    }

    public function outputCSVToFile($path = "")
    {
        $failed = false; 
        $fh = fopen($path . $filename, 'w') or $failed = true;
        if($failed)
            throw new Exception('Unable to Create File :: Check Directory Permissions');
        fwrite($fh, $this->content);
        fclose($fh);
    }

    public function outputCSV()
    {
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=data.csv');
        header("Pragma: no-cache");
        header("Expires: 0");
        print $this->content; 
    }
}
?>
