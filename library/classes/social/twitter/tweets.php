<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/VPanel/library/classes/social/twitter/class_twitter.php';

// initialize twitter class
$twitterFeed = new TweetPHP();
// Get Tweets
$tweet_array = $twitterFeed->get_tweet_array();

// Update Tweet Data to Removed
Database::ExecuteRaw("UPDATE tbl_social_twitter SET tweet_status = 'Removed'");
try {
    foreach($tweet_array as $tweet) {
        $record = new Entity("tbl_social_twitter");
        $record->tweet_id             = $tweet['id_str'];
        $record->tweet_text           = sanitize_from_word($tweet['text']);
        $record->tweet_hashtags       = "";
        if(count($tweet['entities']['hashtags']) > 0) {
            for($i=0;$i < count($tweet['entities']['hashtags']); $i++) {
                $record->tweet_hashtags .= "|" . $tweet['entities']['hashtags'][$i]['text'];    
            }
            if(strlen($record->tweet_hashtags) > 0) {
                $record->tweet_hashtags .= "|";
            }        
        }

        $record->tweet_short_url      = isset($tweet['entities']['urls'][0]) ? $tweet['entities']['urls'][0]['url'] : "";
        $record->tweet_expanded_url   = isset($tweet['entities']['urls'][0]) ? $tweet['entities']['urls'][0]['expanded_url'] : "";
        $record->tweet_retweet_count  = $tweet['retweet_count'];
        $record->tweet_favorite_count = $tweet['favorite_count'];
        $record->tweet_date           = date("Y-m-d G:i:s", strtotime($tweet['created_at']));
        $record->tweet_date_added = date("Y-m-d G:i:s");
        $record->insert();         
    }
    
    // Remove New Tweets That Inserted
    Database::ExecuteRaw("DELETE FROM tbl_social_twitter WHERE tweet_status = 'Removed'");   
    
} catch (Exception $e) {
    // Remove New Tweets That Inserted
    Database::ExecuteRaw("DELETE FROM tbl_social_twitter WHERE tweet_status = 'Active'");    
    // Update Tweet Data to Removed
    Database::ExecuteRaw("UPDATE tbl_social_twitter SET tweet_status = 'Active' WHERE tweet_status = 'Removed'");
    throw new Exception($e->getMessage());    
}