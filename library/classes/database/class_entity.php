<?php
class Entity {	
	public $_primaryKey	= array();
	public $_fields		= array();
	public $_table;
	
	private $_loadFields	= false;

	//------------------------------
	public function __construct($table_name = '', $table_key = '') {
	//------------------------------
		foreach($this as $key => $value) {
			$this->$key = NULL;
		}
		if(!$this->_loadFields) { 
			$this->loadReferences($table_name, $table_key); 
		}
	}

	//------------------------------
	public function __destruct() {
	//------------------------------
		foreach($this as $key => $value)
			unset($this->$key);
	}

	//------------------------------
	public function __get($attribute) {
	//------------------------------
		if(!$this->hasAttribute($attribute))
			throw new Exception('Trying to get an invalid field (' . $attribute . ')');
		if($attribute = $this->_primaryKey[0]) {
			$value = $this->_primaryKey[0]; 
		} else {
			$value = $this->$attribute; 
		}

		if(!get_magic_quotes_gpc()) 
			$value = stripslashes($value);
		return $value;
	}

	//------------------------------
	public function __set($attribute, $value) {
	//------------------------------
		if(!$this->hasAttribute($attribute))
			throw new Exception('Trying to set an invalid field (' . $attribute . ')');

		if (is_null($value)) {
			$this->$attribute = NULL;
		} else {
			$this->$attribute = $value;
		}
	}
	
	public function loadReferences($table_name, $table_key = '') { 
		$this->_table = $table_name;
		if(strlen($table_name) > 0) {
			$this->_fields		= Database::getFields($this->_table);
			$this->_primaryKey	= strlen($table_key) > 0 ? $table_key : Database::getPrimaryKey($this->_table);
			$this->_loadFields	= true;
		}
	}
	
	public function hasAttribute($attribute) { 
		return in_array($attribute, $this->_fields); 
	}
	
	public function attributeIsset($attribute) { 
		return isset($this->$attribute); 
	}

	//------------------------------
	public function insert() {
	//------------------------------
		return Database::insert_bdd($this);
	}

	//------------------------------
	public function update() {
	//------------------------------
		Database::update_bdd($this);
	}

	//------------------------------
	public function delete() {
	//------------------------------
		Database::delete_bdd($this);
	}
};
?>