function handleMessage(event) {
    if (event.data['task'] == 'scroll_top'){
        document.querySelector('#form-iframe').scrollIntoView({
        behavior: 'smooth'
        });
    } else if(event.data['task'] == 'reload'){
        location.reload();
    }
}

window.onload = function() {
    window.addEventListener("message", handleMessage, false);
}
$(document).ready(function() {
    "use strict";

    // On skip content link click, change tabindex to content area
	$('.skip-to-content').on('click', function() {
		var $skip = $('#content-anchor');

		$skip.attr('tabindex',-1).on('blur focusout', function() {
			$(this).removeAttr('tabindex');
		}).focus();
	});

    // Mobile Menu
    $('.openmobile').click(function() {
        $(this).toggleClass('active');
        $('.mobilemenu').toggleClass('active');
        $('.pagewrap').toggleClass('shift');
		$('.mobilemenu .button').focus();
        return false;
    });

    // Mobile Menu Accordion
    $('.mobilemenu ul li a').on('click', function () {
        if ($(this).siblings().length > 0) {
            $(this).toggleClass('active');
            $(this).siblings('ul').slideToggle();
            return false;
        }
    });

// Menu Sub Ul Bottom Arrow
	$('.menu ul li:has(ul.submenu)').addClass('sub-arrow');

// Open and close
	$('a.db-toggle').click(function() {
        $(this).toggleClass('active');
        $(this).siblings('ul').toggleClass('active');
        return false;
    });

// Meter Progress Bar
	$(".meter > span").each(function() {
		$(this)
			.data("origWidth", $(this).width())
			.width(0)
			.animate({
				width: $(this).data("origWidth")
			}, 800);
	});

// Tab Content
	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');
		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');
		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	});

    // Tabbed Content Module
    $(".panel").hide();
        $(".panel:first-of-type").show(),
        $("ul.tabs li a").click(function() {
            var t = $(this).data("tab");
            return $("ul.tabs li a").removeClass("active"), $(this).addClass("active"), $(".panel").hide(), $(".panel#" + t + " ").show(), !1
    });


    // Mobile Tabs
    $("ul.tabs li > a").each(function() {
        var t = $(this).text(),
            e = $(this).data("tab");

        $('<h3 class="mobileonly newtab relative"><a href="#">' + t + "</a></h3>").insertBefore(".panel#" + e + " ");
        $('.newtab:first-of-type a').addClass('active');
    });

    // Mobile Tabs Slideup/down
    $("h3.newtab a").click(function() {
        $("h3.newtab a").removeClass("active");
        $(".panel").slideUp();
        $(this).addClass("active");
        $(this).parent().next(".panel").slideDown();
        $('html, body').animate({ scrollTop: $('.content-tabs').offset().top - 10 }, 750);
        return false;
    });


// matchHeight
var byRow = $('main').hasClass('content');
    // apply matchHeight to each item container's items
    $('.items-container').each(function() {
        $(this).children('.item').matchHeight({
            byRow: byRow
        });
    });


    // Overlays
    $('.openoverlay').click(function() {
        var overlayrel = $(this).data('overlay');
        $(''+ overlayrel +'').fadeIn();
        $('.overlay-mask').fadeIn();
        return false;
    });

    $('.modal .close').click(function() {
        $(this).parent().fadeOut();
        $('.overlay-mask').fadeOut();
        return false;
    });

    // Text resize using HTML classes (minus 3 stylesheets)
    $('.resizerButton').click(function() {
        createCookie('style', $(this).attr("data-style"), 365);
        resizeText(this);
    });
    //var c = readCookie('style');
    //if (c) switchStylestyle(c);
    var textSize = readCookie('style');
    if(!textSize || !(textSize == 'text-normal' || textSize == 'text-medium' || textSize == 'text-large')) {
        textSize = 'text-normal';
    }
    resizeText($("a[data-style='" + textSize + "']"));
});


function resizeText(selectedButton) {
    $('.resizerButton').each(function() {
        $(this).css("opacity", ".7");
        $(this).css("filter", "alpha(opacity=70)");
    });
    $(selectedButton).css("opacity", "1");
    $(selectedButton).css("filter", "alpha(opacity=100)");
    $('html').removeClass('text-normal');
    $('html').removeClass('text-medium');
    $('html').removeClass('text-large');
    $('html').addClass($(selectedButton).attr("data-style"));
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

var cookie = readCookie("style");


if(siteType == 'BANK') {
    //////////////////////////////////////// URI OBJECT MINIFIED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    function URI(a){if(!a)a="";var b=/^(?:([^:\/?\#]+):)?(?:\/\/([^\/?\#]*))?([^?\#]*)(?:\?([^\#]*))?(?:\#(.*))?/;var c=a.match(b);this.scheme=c[1]||null;this.authority=c[2]||null;this.path=c[3]||null;this.query=c[4]||null;this.fragment=c[5]||null}URI.prototype.toString=function(){var a="";if(this.scheme){a+=this.scheme+":"}if(this.authority){a+="//"+this.authority}if(this.path){a+=this.path}if(this.query){a+="?"+this.query}if(this.fragment){a+="#"+this.fragment}return a};(function(){function a(a,b){var c=/^(.*)\//;if(a.authority&&!a.path){return"/"+b}else{return a.path.match(c)[0]+b}}function c(a){if(!a)return"";var c=a.replace(/\/\.\//g,"/");c=c.replace(/\/\.$/,"/");while(c.match(b)){c=c.replace(b,"/")}c=c.replace(/\/([^\/]*)\/\.\.$/,"/");while(c.match(/\/\.\.\//)){c=c.replace(/\/\.\.\//,"/")}return c}var b=/\/((?!\.\.\/)[^\/]*)\/\.\.\//;URI.prototype.resolve=function(b){var d=new URI;if(this.scheme){d.scheme=this.scheme;d.authority=this.authority;d.path=c(this.path);d.query=this.query}else{if(this.authority){d.authority=this.authority;d.path=c(this.path);d.query=this.query}else{if(!this.path){d.path=b.path;if(this.query){d.query=this.query}else{d.query=b.query}}else{if(this.path.charAt(0)==="/"){d.path=c(this.path)}else{d.path=a(b,this.path);d.path=c(d.path)}d.query=this.query}d.authority=b.authority}d.scheme=b.scheme}d.fragment=this.fragment;return d}})()

    ///////////////////////////////////////// DISCLAIMER WINDOW \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\;
    var url;
    var newWin;
    var domains = '';

    $(function () {
        // add the class to all external links
        $("a[href*='http://'], a[href*='https://']").not("[data-nodisclaimer='true']").not(".nodisclaimer").each(function () {
            var uri = new URI($(this).attr('href').toLowerCase());
            var url = $(this).attr('href').toLowerCase();
            if (domains.toLowerCase().indexOf(uri.authority) < 0 && url.indexOf('disclaimer?') == -1) {
                $(this).addClass('triggerDisclaimer');
                $(this).attr("target", "_self");
                $(this).attr("href", "/disclaimer?url=" + $(this).attr("href"));
            }
        });

        ///////////////////////////////////////// EMAIL DISCLAIMER WINDOW \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\;
        $("a[href*='mailto:']").not("[data-nodisclaimer='true']").not(".nodisclaimer").each(function () {
            $(this).addClass('triggerDisclaimer');
            $(this).attr("target", "_self");
            $(this).attr("href", "/email-disclaimer?email=" + $(this).attr("href"));
            $(this).attr("target", "_self");
        });
    });
}

// Match Height
!function(t){"use strict";"function"==typeof define&&define.amd?define(["jquery"],t):"undefined"!=typeof module&&module.exports?module.exports=t(require("jquery")):t(jQuery)}(function(t){var e=-1,o=-1,i=function(t){return parseFloat(t)||0},a=function(e){var o=1,a=t(e),n=null,r=[];return a.each(function(){var e=t(this),a=e.offset().top-i(e.css("margin-top")),s=r.length>0?r[r.length-1]:null;null===s?r.push(e):Math.floor(Math.abs(n-a))<=o?r[r.length-1]=s.add(e):r.push(e),n=a}),r},n=function(e){var o={
    byRow:!0,property:"height",target:null,remove:!1};return"object"==typeof e?t.extend(o,e):("boolean"==typeof e?o.byRow=e:"remove"===e&&(o.remove=!0),o)},r=t.fn.matchHeight=function(e){var o=n(e);if(o.remove){var i=this;return this.css(o.property,""),t.each(r._groups,function(t,e){e.elements=e.elements.not(i)}),this}return this.length<=1&&!o.target?this:(r._groups.push({elements:this,options:o}),r._apply(this,o),this)};r.version="0.7.0",r._groups=[],r._throttle=80,r._maintainScroll=!1,r._beforeUpdate=null,
    r._afterUpdate=null,r._rows=a,r._parse=i,r._parseOptions=n,r._apply=function(e,o){var s=n(o),h=t(e),l=[h],c=t(window).scrollTop(),p=t("html").outerHeight(!0),d=h.parents().filter(":hidden");return d.each(function(){var e=t(this);e.data("style-cache",e.attr("style"))}),d.css("display","block"),s.byRow&&!s.target&&(h.each(function(){var e=t(this),o=e.css("display");"inline-block"!==o&&"flex"!==o&&"inline-flex"!==o&&(o="block"),e.data("style-cache",e.attr("style")),e.css({display:o,"padding-top":"0",
        "padding-bottom":"0","margin-top":"0","margin-bottom":"0","border-top-width":"0","border-bottom-width":"0",height:"100px",overflow:"hidden"})}),l=a(h),h.each(function(){var e=t(this);e.attr("style",e.data("style-cache")||"")})),t.each(l,function(e,o){var a=t(o),n=0;if(s.target)n=s.target.outerHeight(!1);else{if(s.byRow&&a.length<=1)return void a.css(s.property,"");a.each(function(){var e=t(this),o=e.attr("style"),i=e.css("display");"inline-block"!==i&&"flex"!==i&&"inline-flex"!==i&&(i="block");var a={
            display:i};a[s.property]="",e.css(a),e.outerHeight(!1)>n&&(n=e.outerHeight(!1)),o?e.attr("style",o):e.css("display","")})}a.each(function(){var e=t(this),o=0;s.target&&e.is(s.target)||("border-box"!==e.css("box-sizing")&&(o+=i(e.css("border-top-width"))+i(e.css("border-bottom-width")),o+=i(e.css("padding-top"))+i(e.css("padding-bottom"))),e.css(s.property,n-o+"px"))})}),d.each(function(){var e=t(this);e.attr("style",e.data("style-cache")||null)}),r._maintainScroll&&t(window).scrollTop(c/p*t("html").outerHeight(!0)),
        this},r._applyDataApi=function(){var e={};t("[data-match-height], [data-mh]").each(function(){var o=t(this),i=o.attr("data-mh")||o.attr("data-match-height");i in e?e[i]=e[i].add(o):e[i]=o}),t.each(e,function(){this.matchHeight(!0)})};var s=function(e){r._beforeUpdate&&r._beforeUpdate(e,r._groups),t.each(r._groups,function(){r._apply(this.elements,this.options)}),r._afterUpdate&&r._afterUpdate(e,r._groups)};r._update=function(i,a){if(a&&"resize"===a.type){var n=t(window).width();if(n===e)return;e=n;
        }i?-1===o&&(o=setTimeout(function(){s(a),o=-1},r._throttle)):s(a)},t(r._applyDataApi),t(window).bind("load",function(t){r._update(!1,t)}),t(window).bind("resize orientationchange",function(t){r._update(!0,t)})});

$(function() { $('.matchHeight').matchHeight(); });

// Back to top
	$(window).scroll(function() {
		if($(window).scrollTop() > 400) {
			$('.back-to-top').fadeIn();
		} else {
			$('.back-to-top').fadeOut();
		}
	});

	// Smooth scrol to top
	$('.back-to-top').click(function() {
		$('body,html').animate({ scrollTop: 0 }, 800);
		return false;
	});