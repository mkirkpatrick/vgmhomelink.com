$(document).ready(function() {
    "use strict";

    //Banking Modal logic
    $.post("/ajax/marketing-modal/readcookie.php", {name: 'alertModal'}, function(data) {
        var x = data;
        // will be null if not created, so create the cookie and set to open

        if(x == null){
            $.post("/ajax/marketing-modal/setcookie.php", {name: 'alertModal', type: 'open'});
            $.post("/ajax/marketing-modal/readcookie.php", {name: 'alertModal'}, function(data) {
            var x = data;
            }, "html");

        }
        //cookie is ready to open
        if(x == 'open'){
            //pull all info and fill modal then open
            $.ajax({
                url: '/ajax/marketing-modal/marketing-modal.php',
                type: 'post',
                data: {  },
                dataType: 'html',
                success: function(data){
                    if(data == 'error'){
                        //do not pop modal
                    } else {
                        $('#alertModal').html(data);
                        $('.overlay-mask').show();
                        $('#alertModal').show();
                    }
                }
            });
        }
        if(x == 'closed'){
            $('.overlay-mask').hide();
            $('#alertModal').hide();
        }
        if(x == 'forever'){
            $('.overlay-mask').hide();
            $('#alertModal').hide();
        }
    }, "html");
});

    $(document).on('click', '.close-forever', function() {
        $('.overlay-mask').fadeOut();
        $('#alertModal').fadeOut();
        $.post("/ajax/marketing-modal/setcookie.php", {name: 'alertModal', type: 'forever'});
        //closes modal for 1 year
    });

    $(document).on('click', '#close-dynamic-overlay', function() {
        $('.overlay-mask').fadeOut();
        $('#alertModal').fadeOut();
        $.post("/ajax/marketing-modal/setcookie.php", {name: 'alertModal', type: 'closed'});
        //closes modal for 24 hours
    });