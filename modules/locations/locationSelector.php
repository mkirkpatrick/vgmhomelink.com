<div class="module branch">
	<div class="body">
		<h2>Find a Branch/ATM</h2>
		<select class="filter" id="cboLocationFilter">
            <option value="0"> -- Select Location -- </option>
            <?= buildLocationOptions("tbl_site_location", "location_id", "location_title"); ?>
        </select>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$("#cboLocationFilter").change(function() { window.location.href = "/locations#" + $(this).val(); });
});
</script>
<?php 
function buildLocationOptions($tblName, $valueField, $nameField) {
		$list	 = '';
		$sql	 = "SELECT $valueField as value, $nameField as name 
					FROM $tblName
				    ORDER BY $nameField
					LIMIT 1000"; 
		$record = Database::Execute($sql);
		$count = 0; 
		while ($record->MoveNext()) {
			$list .= '<option value="' . $count . '">' . htmlspecialchars($record->name) . "</option>" . PHP_EOL;
			$count++; 
		}
		
		return $list;
}
?>