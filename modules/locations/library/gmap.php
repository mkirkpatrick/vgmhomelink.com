<?
header('Content-Type: application/json');
header("Cache-Control: no-cache, must-revalidate"); 
header("Expires: 0");  

require_once $_SERVER['DOCUMENT_ROOT'] . "\library\config.php";
$uploadDirectory = urlPathCombine($_SERVER['ROOT_URI'], $_WEBCONFIG['UPLOADS_DIRECTORY'], '/locations/');

$address = array();
$sql  = "SELECT * FROM tbl_site_location WHERE location_status = 'Active' AND site_id IN (0,1) ORDER BY location_sort_id, location_title;"; 
$location = Database::Execute($sql);

if ($location->Count() > 0) {

    while ($location->MoveNext()) {
        $cssClass = "sixcol";
        $cssClass2 = "block";
        $style = ""; 
        $imgDiv = ''; 
        if(!isNullOrEmpty($location->location_image)) {
            $cssClass = "sixcol";
            $cssClass2 = "eightcol last";
            $imgDiv = '<div class="fourcol"><img id="locationImage' . $location->location_id . '" src="' . $uploadDirectory . '/' . $location->location_image . '" border="0" /></div>';
        }
        else if(isNullOrEmpty($location->location_hours)) {
            $cssClass = "twelvecol";
            $cssClass2 = "eightcol last";
        }
        $SUPER_DUPER_ADDRESS = array('address'=>$location->location_address
            ,'city'=>$location->location_city
            ,'state'=>$location->location_state
            ,'zip'=>$location->location_postal  
            ,'location_table_row_id'=>$location->location_id
            );
        $addr = $location->location_address .", ".$location->location_city.", ".$location->location_state.", ".$location->location_postal;
        $bubble = "<div id='mapToolTip' class='cf'><h2>".$location->location_title."</h2>"; 
        $bubble .= $imgDiv; 
        $bubble .= "<div class='$cssClass2' style='$style'><div class='locationInformation $cssClass'><p>".$location->location_address."<br />".$location->location_city.", ".$location->location_state." ".$location->location_postal."</p>";
     
  
  
  if(!isNullOrEmpty($location->location_mailing_address)) {
  $bubble .= '<p class="mailing">'
            .$location->location_mailing_address."<br />".$location->location_mailing_city.", ".$location->location_mailing_state." ".$location->location_mailing_postal
            .'</p>';
}
        $bubble .= "<p>"; 
        if(!isNullOrEmpty($location->location_phone)) { 
            $bubble .= "<strong>Phone: </strong>".$location->location_phone."<br />"; 
        }
        if(!isNullOrEmpty($location->location_fax)) { 
            $bubble .= "<strong>Fax: </strong>".$location->location_fax."<br />"; 
        }
        if(!isNullOrEmpty($location->location_toll_free)) { 
            $bubble .= "<strong>Toll Free: </strong>".$location->location_toll_free . "<br />"; 
        }
        if(!isNullOrEmpty($location->location_email)) { 
            $bubble .= "<strong>Email: </strong>".$location->location_email ."<br />"; 
        }
		
        if(strlen($location->location_services) > 0) {
            $bubble .= "<strong>Services: </strong><br />"; 
            $bubble .= strstr($location->location_services,'Branch') ? '<div class="branch">Branch</div>' : ''; 
            $bubble .= strstr($location->location_services,'Drive-Up') ? '<div class="drive-up">Drive-Up</div>' : ''; 
            $bubble .= strstr($location->location_services,'ATM') ? '<div class="atm type">ATM</div>' : ''; 
        } 
		
        $bubble .= "</p>"; 
        $bubble .= "</div>"; 
        if(!isNullOrEmpty($location->location_hours)) { 
            $bubble .= "<div class='$cssClass last locationHours'><p><strong>Hours of Operation </strong></p>"; 
            $bubble .= "<div class='hours'>" . $location->location_hours . "</div>";
            $bubble .= "</div>";
        }
        $bubble .= "<div class='clearfloat'></div>";
        if(isset($_WEBCONFIG['SITE_TYPE']) && $_WEBCONFIG['SITE_TYPE'] == 'BANK') { 
            $bubble .= "<div class='$cssClass'><a href=\"/disclaimer?url=https://maps.google.com/maps?q=".urlencode($addr)."&amp;hl=en&amp;z=17\" class='driving'>View Driving Directions</a></div>"; 
        }
        else {
            $bubble .= "<div class='$cssClass'><a href=\"/https://maps.google.com/maps?q=".urlencode($addr)."&amp;hl=en&amp;z=17\" class='driving'>View Driving Directions</a></div>";     
        }
					
        if(!isNullOrEmpty($location->location_hours)) { 
            $bubble .= "<div class='$cssClass last'><a href=\"/contact-us\" target=\"_blank\" class='driving'>Contact Us</a></div>";
        }
                    
        $bubble .= "</div>"; 
        $bubble .= "</div>";
      // 
        $address[] = array(
      "Address"     => $addr
     ,'oAddress'    => $SUPER_DUPER_ADDRESS
           ,'Icon'        => $location->location_services == '|F' || $location->location_services == '|C' ? /* Place Atm Marker*/ '/modules/locations/images/letter_b.png' : /*Place the defalut pin*/ '/modules/locations/images/letter_f.png'
           ,"Name"        => $location->location_title
           ,"Bubble"      => $bubble
           ,'Geo_Address' => $location->geo_address
           ,'Lat'         => $location->geo_lat
           ,'Lng'         => $location->geo_lng
         );
    }//end while
}


$data = Array();
 
foreach ($address as $key => $val) {
    $addr     = urlencode($val['Address']);
 
 $location_table_row_id = $val['oAddress']['location_table_row_id'];
 
 // format address line so no ', suite X' stuff is sent to google
 $hackedAddrLine = $val['oAddress']['address'];
 
 $hackedAddrLine = preg_replace('/,( )+suite(.)*/i','',$hackedAddrLine);
 
 // this is a hack
 
 $geo_address = 
       $hackedAddrLine
  .'+'.$val['oAddress']['city']
  .'+'.$val['oAddress']['state']
  .'+'.$val['oAddress']['zip'];   
 

    $data[]     = array(
        'Icon'=>$val['Icon'], 
        'Lat'=>$val['Lat'], 
        'Lng'=>$val['Lng'], 
        'Name'=>$val['Name'], 
        'Bubble'=>$val['Bubble']);
   
   
   
    if($geo_address != $val['Geo_Address']){
        //print 'Addr: ' . $addr .  '&nbsp;&nbsp; |&nbsp;&nbsp;Geo: ' . $val['Geo_Address'] . ' <br />'; 
        $url     = 'http://services.forbin.com/raw/geo.php?q='.urlencode($geo_address);
  
 
        $get     = file_get_contents($url);
        $geoData = json_decode($get);
   
  
  if( isset($geoData->results[0]->geometry->location->lat)&&isset($geoData->results[0]->geometry->location->lng)&&isset($geoData->results[0]->geometry->location_type)
   &&( $geoData->results[0]->geometry->location_type == 'ROOFTOP'
    || $geoData->results[0]->geometry->location_type == 'RANGE_INTERPOLATED'
    || $geoData->results[0]->geometry->location_type == 'APPROXIMATE'
    || $geoData->results[0]->geometry->location_type == 'GEOMETRIC_CENTER'
    )){
     
      $lat = $geoData->results[0]->geometry->location->lat; 
      $lng = $geoData->results[0]->geometry->location->lng; 
      $data[count($data)-1]['Lat']=     $lat;
      $data[count($data)-1]['Lng']=  $lng;
      $geoAddress = Database::quote_smart($geo_address);
      $sql = "UPDATE tbl_site_location 
              SET geo_address='$geoAddress', geo_lat='$lat', geo_lng='$lng' 
              WHERE location_id='$location_table_row_id' 
              LIMIT 1";
      Database::ExecuteRaw($sql);       
  }  
        usleep(100 * 1000);
    } else { continue; }

}//end foreach

echo json_encode($data);
exit; 
?>