$(function () {

    $("#map_canvas").html('<div id="loading" style="width: 100%; margin-top: 25px" align="center"><img src="/images/loading.gif" /></div>');
    $.ajax({
        url: "/modules/locations/library/gmap.php",
        type: "GET",
        dataType: "json",
        success: function (data) {

            // get the data string and convert it to a JSON object.
            var jsonData = data;
            var latitude = new Array();
            var longitude = new Array();
            var name = new Array();
            var bubble = new Array();
            var icon = new Array();
            var i = 0;
            var j = 0;
            var k = 0;
            var l = 0;
            var m = 0;

            $.each(jsonData, function (Idx, Value) {
                $.each(Value, function (x, y) {
                    //Creating an array of latitude, logitude
                    if (x == 'Lat') {
                        i = i + 1;
                        latitude[i] = y;
                    }
                    if (x == 'Lng') {
                        j = j + 1;
                        longitude[j] = y;
                    }
                    if (x == 'Name') {
                        k = k + 1;
                        name[k] = y;
                    }
                    if (x == 'Bubble') {
                        l = l + 1;
                        bubble[l] = y;
                    }
                    if (x == 'Icon') {
                        m = m + 1;
                        icon[m] = y;
                    }
                });
            });

            // Initialize 
            infowindow = new google.maps.InfoWindow({}); // Initialize the Info Window so we can close it later.
            geocoder = new google.maps.Geocoder();

            var mapOptions = {
                zoom: defaultZoomLevel,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            $("#map_canvas").html('');
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

            centerMap();

            //Final for loop for creating the markers
            for (var a = 1; a < latitude.length; ++a) {
                createMarkers(map, name[a], latitude[a], longitude[a], bubble[a], icon[a]);
            }

            google.maps.event.addDomListener(window, 'resize', function () {
                centerMap();
            }); 

			google.maps.event.addListenerOnce(map, 'tilesloaded', function(){
				locationHashChanged(); 
			});
        }
    });
});

function locationHashChanged() {
    if(window.location.hash != '') { 
		google.maps.event.trigger(markers[window.location.hash.replace('#', '')], "click");
		$('html,body').animate({ scrollTop: $("#map_canvas").offset().top }, '150');
	} 
}

function centerMap() {
    geocoder.geocode({ 'address': centerAddress }, function (results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			map.setCenter(results[0].geometry.location);
		} else {
			console.debug("Geocode was not successful for the following reason: " + status);
		}
	});
} 

function createMarkers(map, name, latitude, longitude, infoBubbleText, markerIcon) {
	//In array lat long is saved as an string, so need to convert it into int.
	var lat = parseFloat(latitude);
	var lng = parseFloat(longitude);
	var marker = new google.maps.Marker({
				map: map, 
				position: new google.maps.LatLng(lat, lng),
				title: name,
				animation: google.maps.Animation.DROP, 
                clickable: true,
                icon: markerIcon
    });

    google.maps.event.addListener(marker, 'click', function () {
        // Info Window

        var windowOptions = {
            content: infoBubbleText,
            //maxWidth: 0,
            boxClass: 'infoBox', 
            boxStyle: { border: "200px solid black" },
            zIndex: null,
            closeBoxMargin: "10px 2px 2px 2px",
            infoBoxClearance: new google.maps.Size(1, 1), 
            isHidden: false
        };

        infowindow.close();
        infowindow = new google.maps.InfoWindow(windowOptions);
        infowindow.open(map, marker);
    });

    markers.push(marker);
}

if ("onhashchange" in window) {
   window.onhashchange = locationHashChanged;
}
