<?php
global $_WEBCONFIG; ?>
<link rel="stylesheet" href="/modules/locations/css/styles.css" type="text/css" />
<script type="text/javascript">
var markers = new Array();
var defaultZoomLevel = 11;
var centerAddress = 'Rockford, IL'; 
var map;
var infowindow;
var geocoder;
</script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>
<script type="text/javascript" src="/modules/locations/js/google-maps.js"></script><?
$uploadDirectory = urlPathCombine($_SERVER['ROOT_URI'], $_WEBCONFIG['UPLOADS_DIRECTORY'], '/locations/');
?>  

<div class="clearfloat"></div>

<div class="threecol left-sidebar">
    
    <h2>Choose A Location</h2>
    <ul class="location-list" id="locationAddressList">
	<?php
	$sql  = "SELECT * FROM tbl_site_location WHERE location_status = 'Active' AND site_id IN (0,1) ORDER BY location_sort_id, location_title;";
	$locs = Database::Execute($sql);

	if ($locs->Count() > 0) {
		$count = 0; 
		while ($locs->MoveNext()) {
			$atmOnly = $locs->location_services == '|F' ? '<span class="type">ATM Only</span>' : ''; 
			echo '<li>
					<a class="locationLink" data-markerId="' . $count .'" href="#' . $count .'"">
						<span class="location-title">' . $locs->location_title . '</span>
						<span class="location-address">
							' . $locs->location_address . '<br />
							' . $locs->location_city . ', ' . $locs->location_state. ' ' . $locs->location_postal . '<br />
							' . $locs->location_phone . '
						</span>
						' . $atmOnly  . '
					</a>
					</li>';
			$count++; 
		}//end while
	}
	?>
    </ul><!-- eof sidebar-links -->
</div><!-- eof left sidebar -->

<div class="main ninecol last">
	<div id="map_canvas" style="width:100%; height:480px"></div>
    <em style="font-size: .6rem;">Disclaimer: Please note that this map is a product of Google, Inc and we are not endorsing or guaranteeing the products, information or recommendations provided by this organization. We are not liable for any failure of products or services advertised on those sites. We are not responsible for the validity, collection, use or security of information by organizations that may be linked to our website. We encourage you to read the privacy policies of websites reached through the use of links from the Forbin Bank Demo website. </em>
</div><!-- eof main -->