<ul class="contact-location-list twelevecol" id="contact-location-list">
    <?php
    global $_WEBCONFIG;

    $uploadsDirectory = urlPathCombine($_WEBCONFIG['UPLOADS_DIRECTORY'], 'locations');
    $sql  = "SELECT * FROM tbl_site_location WHERE location_status = 'Active' AND site_id IN (0,1) ORDER BY location_sort_id, location_title;";
    $location = Database::Execute($sql);

    if ($location->Count() > 0) {
        $count = 0;
        while ($location->MoveNext()) {
            $geoAddress = urlencode($location->location_address .' '. $location->location_city . ', ' . $location->location_state. ' ' . $location->location_postal);
            print '<li>
                        <h2>' . $location->location_title . '</h2>
                        <p>Phone: ' . $location->location_phone;
                        if(!isNullOrEmpty($location->location_fax)) {  print '<br /><b>Fax: </b>' . $location->location_fax; }
                        if(!isNullOrEmpty($location->location_toll_free)) {  print '<br /><b>Toll Free: </b>' . $location->location_toll_free;  }
                        if(!isNullOrEmpty($location->location_email)) {  print '<br />' . formatEmailLink($location->location_email); }
                 print '</p>';
                 print '<div id="location-address' . $count . '" class="location-address">';
                 if(!isNullOrEmpty($location->location_address)) {
                            print '<p>
                            ' . $location->location_address . '<br />
                            ' . $location->location_city . ', ' . $location->location_state. ' ' . $location->location_postal . '</p>
                            <p><a class="green button" title="Link opens Google Maps in new window for ' . $location->location_title . '" target="_blank" href="http://maps.google.com/maps?saddr=' . $geoAddress . '">Get Driving Directions</a></p>';
                            }
            if(!isNullOrEmpty($location->location_mailing_address)) {
                        print '<div class="location-mailing-address">
                                <h3>Mailing Address</h3>
                                <p>' . $location->location_mailing_address . '<br />
                                   ' . $location->location_mailing_city . ', ' . $location->location_mailing_state. ' ' . $location->location_mailing_postal . '</p>
                              </div>';
            }
            if(!isNullOrEmpty($location->location_hours)) {
                print '<div class="location-hours">
                            <h3>Our Hours</h3>
                            ' . $location->location_hours. '</div>' . PHP_EOL;
            }
            if(!isNullOrEmpty($location->location_website)) {
                print '<div id="locationDetails' . $count . '"><a id="locationDetailsLink' . $count . '" href="' . $location->location_website .'" class="viewLocationDetails">View More</a></div>';
            }
            if(!isNullOrEmpty($location->location_image)) {
                print '<div id="locationImage' . $count . '"><img src="' . urlPathCombine($uploadsDirectory, $location->location_image) . '" alt="' . $location->location_title . '" /></div>';
            }
            print '</div>';

            print '</li>';
            $count++;
        }//end while
    }
    ?>
    </ul><!-- eof sidebar-links -->

<!-- if there are more than 3 locations apply the view more link and hide the data -->
<? if($location->Count() > 3) { ?>

    <script type="text/javascript">$(".showMoreLocationDetailsButton").click(function(){id=$(this).data("id"),$(".location-address").slideUp(),$(".showLessLocationDetails").hide(),$(".showMoreLocationDetails").show(),$("#locationViewMore"+id).hide(),$("#locationViewLess"+id).show(),$("#locationViewLessLink"+id).show(),$("#location-address"+id).slideDown()}),$(".showLessLocationDetailsButton").click(function(){id=$(this).data("id"),$("#locationViewMore"+id).show(),$("#locationViewLess"+id).hide(),$(".location-address").slideUp()});</script>
<? } ?>