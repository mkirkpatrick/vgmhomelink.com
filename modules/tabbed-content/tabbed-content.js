$(document).ready(function(){
    // Tabbed Content Module - creates responsive tabs
    $(".panel").hide();
    $(".panel:first-of-type").show();

    $("ul.tabs li a").click(function() {
        var t = $(this).data("tab");
        var id = $(this).parent().parent().find("li a.active").data("tab");

        $(this).parent().parent().find("li a.active").removeClass("active");
        $(this).addClass("active");
        
        $("div#" + id).hide();
        $("div#" + t).show();
        $("div#" + t).addClass("active");
        $('html, body').animate({ scrollTop: $(this).parent().parent().parent().parent().offset().top - 10 }, 750);
        return false;
    });

    $("ul.tabs li > a").each(function() {
        var t = $(this).text(),
            e = $(this).data("tab");
        
        $('<h3 class="mobileonly newtab relative"><a href="#">' + t + "</a></h3>").insertBefore(".panel#" + e + " ");
    });

    var url = window.location.href;
    var hash = url.substring(url.indexOf('#')+1);
    if($('a[data-tab="' + hash +'"]').length > 0){
        $('a[data-tab="' + hash + '"]').trigger('click');
    }    

})