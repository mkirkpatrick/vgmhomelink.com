<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/modules/site-banner/library/functions.php');

if(!function_exists('addHtmlTag')){
    print '<link rel="stylesheet" href="/css/modules/site-banner/style.css" />';
} else {
    addHtmlTag('/css/modules/site-banner/style.css','css');
}//end if

$isPreview          = isset($_GET['a']) && isset($_POST['actionRun']);
$revisionPreview    = isset($_GET['hnid']) ? (int)$_GET['hnid'] : 0;
$siteBannerConfig   = getWebConfigSettings($_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['VPANEL_PATH'] . "modules/site-banner/");

// check if its a preview
// check if its a revision
// else it is the live site
if($isPreview) {
    $txtTitle      = isset($_POST['txtTitle']) ? xss_sanitize($_POST['txtTitle']) : "";
    $txtText       = isset($_POST['txtText']) ? xss_sanitize($_POST['txtText']) : "";
    $txtUrl        = isset($_POST['txtUrl']) ? xss_sanitize($_POST['txtUrl']) : "";
    $txtButtonText = isset($_POST['txtButtonText']) ? xss_sanitize($_POST['txtButtonText']) : "";
    $class         = isset($_POST['optPromoAlert']) ? xss_sanitize($_POST['optPromoAlert']) : "";
    $bannerId      = 0;
    $showBanner    = true;
    $hnImage       = '';

    if($class == 'promo'){
        $browseImage = $_POST['txtImageUrl'];

        if(stripos($browseImage, $siteBannerConfig['UPLOAD_FOLDER']) !== false) {
            $hnImage = '<img src="' . autoCreateVersion($browseImage,'return') . ' alt="Banner Image">';
        } else {
            if(isset($_FILES['fleImage']) && !isNullOrEmpty($_FILES['fleImage']['name'])) {
                require_once $_SERVER['DOCUMENT_ROOT'] . "{$_WEBCONFIG['VPANEL_PATH']}library/ThumbLib.inc.php";
                $fieldName = 'fleImage';
                list($width, $height, $type, $attr) = getimagesize($_FILES[$fieldName]['tmp_name']);
                $fieldValue  = $_FILES[$fieldName]['name'];
                $file_ext    = strtolower(getExtName($fieldValue));
                if(stristr($_WEBCONFIG['VALID_IMGS'], $file_ext)) {
                    $imagePath    = "/siteBannerPreview.{$file_ext}";
                    $options = array('resizeUp' => true, 'jpegQuality' => 100, 'correctPermissions' => true);
                    $thumb = PhpThumbFactory::create($_FILES[$fieldName]['tmp_name'], $options);
                    $thumb->resize($width, $height);
                    $thumb->save($_SERVER['DOCUMENT_ROOT'] . '/uploads/temp' . $imagePath);
                    $hnImage = '<img src="' . autoCreateVersion('/uploads/temp' . $imagePath,'return') . '" alt="Banner Image">';
                }//end if
            } //end if
        } // end if
    }//end if
} elseif($revisionPreview > 0) {
    $sql = "SELECT *
    FROM tbl_site_banner
    WHERE hn_id = $revisionPreview
    LIMIT 1";
    $record = Database::Execute($sql);
    if($record->Count() > 0) {
        $record->MoveNext();
        $txtTitle       = $record->hn_title;
        $txtText        = $record->hn_text;
        $txtUrl         = $record->hn_url;
        $txtButtonText  = $record->hn_button_text;
        $class          = $record->hn_promo_or_alert;
        $bannerId       = $record->hn_id;
        $hnImage        = $record->hn_image;
        $showBanner     = true;

        if($class == 'promo'){
            $hnImage = '<img src="' . autoCreateVersion($siteBannerConfig['UPLOAD_FOLDER'] . $hnImage,'return') .'" alt="Banner Image">';
        }//end if
    }//end if
} else {
    //Get member
    if(isset($_SESSION['member_ad_username'])) {
        $memberUsername = $_SESSION['member_ad_username'];
        $x = "SELECT user_last_login FROM tbl_user WHERE user_username = '{$memberUsername}' LIMIT 1";
        $z = Database::Execute($x);
        if($z->Count() > 0) {
            $z->MoveNext();
            $lastLogin = $z->user_last_login;
        } else {
            $lastLogin = "1969-12-31 00:00:00";
        }
    } else {
        $memberUsername = $_SESSION['member_id'];
        $x = "SELECT m_last_login FROM tbl_members WHERE m_id = '{$memberUsername}' LIMIT 1";
        $z = Database::Execute($x);
        if($z->Count() > 0) {
            $z->MoveNext();
            $lastLogin = $z->m_last_login;
        } else {
            $lastLogin = "1969-12-31 00:00:00";
        }
    }
    // $memberUsername = isset($_SESSION['member_ad_username']) ? $_SESSION['member_ad_username'] : $_SESSION['member_id'];
    $memberType = $_SESSION['member_type'];


    if($memberType == "Case Manager") {
        $memberType = 1;
    } elseif($memberType == 'Provider') {
        $memberType = 2;
    } else {
        $memberType = 0;
    }
    //get all alerts they have been shown from xref table
    $memSql = "SELECT * FROM tbl_site_banner_xref WHERE m_id = '{$memberUsername}'";
    $memberData = Database::Execute($memSql);
    $shownArray = array(0);
    if($memberData->Count() > 0){
        while($memberData->MoveNext()){
            $shownArray[] = $memberData->alert_id;
        }
    } else {
        // none in table
    }
    // breakPOint($shownArray);
    $shownArray = implode(", ", $shownArray);

    $sql = "SELECT * FROM tbl_site_banner WHERE hn_status = 'Active' AND hn_historic_id IS NULL AND hn_is_published = 1 AND hn_last_update > '{$lastLogin}' AND (hn_type = {$memberType} OR hn_type = 0) AND hn_id NOT IN($shownArray)
    ORDER BY hn_time_start DESC";

    $record = Database::Execute($sql);

    if($record->Count() > 0) {
        $record->MoveNext();
        $txtTitle      = $record->hn_title;
        $txtText       = $record->hn_text;
        $txtUrl        = $record->hn_url;
        $txtButtonText = $record->hn_button_text;
        $hnImage       = preg_replace('/(<[^>]+) style=".*?"/i', '$1',$record->hn_image);
        $class         = $record->hn_promo_or_alert;
        $bannerId      = isNullOrEmpty($record->hn_historic_id) ? $record->hn_id : $record->hn_historic_id;
        if($class == 'promo'){
            $hnImage = '<img src="' . autoCreateVersion($siteBannerConfig['UPLOAD_FOLDER'] . $hnImage,'return') . '" alt="Banner Image">';
        }//end if

        // Check if cookie isset
        if(isset($_COOKIE['site_banner']) && $_COOKIE['site_banner'] == $bannerId){
            if(isset($_COOKIE['site_banner_exp']) && strtotime($record->hn_last_update) > $_COOKIE['site_banner_exp']) {
                $showBanner = true;
            } else {
                $showBanner = false;
            }//end if
        } else {
            $showBanner = true;
        }//end if
    } else {
        $showBanner = false;
        $bannerId = 0;
    }//end if
}//end if

if($showBanner === true){ ?>
    <section aria-label="site banner" id="site-banner-container">
        <div class="mod-banner <?= $class ?>">
            <div class="container clearfix relative">
                <?php
                if (!isNullOrEmpty($hnImage)) { ?>
                    <div class="banner-image large-only"><?=  $hnImage  ?></div>
                    <?php
                }//end if ?>
                <div class="banner-text<?= isNullOrEmpty($hnImage) ? ' no-image' : '';  ?>">
                    <p class="banner-name"><?= $txtTitle ?></p>
                    <p class="banner-content"><?= $txtText ?>

                        <?php
                        if(!isNullOrEmpty($txtUrl)) { ?>
                            <span class="banner-url">
                                <a href="<?= $txtUrl ?>" target="<?= stristr($txtUrl, 'http') !== false ? '_blank ' : '';?>" class="button primary" title="<?= $txtButtonText ?>" ><?= $txtButtonText ?></a>
                            </span>
                            <?php
                        }//end if ?>
                    </p>
                </div>

                <a class="close" data-id="<?= $bannerId ?>" href="#" title="Close banner">Close</a>
            </div>
        </div>
    </section>
    <?php
}//end if
?>
<script>
    var bannerId = "<?= $bannerId ?>";
</script>
<?php
if(!function_exists('addHtmlTag')){
    print '<script src="/modules/site-banner/js/site-banner.js"></script>';
} else {
    addHtmlTag("/modules/site-banner/js/site-banner.js","javascript");
}
?>