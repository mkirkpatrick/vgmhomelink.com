$(document).ready(function() {
    $(document).on('click', '.mod-banner a.close', function() {
        $('#site-banner-container').html("").slideUp();

        var bannerId = $(this).data('id');
        $.ajax({
            type: 'post',
            url: "/ajax/site-banner/set-cookie.php",
            data: {
                id: bannerId
            },
            dataType: "html",
            success: function(data) {
                if(data != ''){
                    $.ajax({
                        type: 'post',
                        url: "/ajax/site-banner/show-alert.php",
                        data: {
                            id: data
                        },
                        dataType: "html",
                        success: function(shit) {
                            $('#site-banner-container').html(shit).slideDown();
                        }
                    });
                }
            }
        });
        return !1
    });
});