<?php
if(!function_exists('autoCreateVersion')){
    function autoCreateVersion($url, $returnType='echo'){
        $path = pathinfo($url);
        $ver = filemtime($_SERVER['DOCUMENT_ROOT'].$url);
        if($returnType == 'echo') {
            echo $path['dirname'].'/'.$path['basename'].'?v='.$ver;
        } elseif($returnType == 'return') {
            return $path['dirname'].'/'.$path['basename'].'?v='.$ver;
        }//end if
    }//end function
}//end if

if(!function_exists('isNullOrEmpty')){
    function isNullOrEmpty($string) {
        return (!isset($string) || strlen(trim($string)) == 0);
    }//end function
}//end if

if(!function_exists('getWebConfigSettings')){
    function getWebConfigSettings($configPath) {
        $config = array();
        if (is_readable($configPath) && file_exists($configPath . '/web.config')) {
            $configXML = simplexml_load_file($configPath . '/web.config');
            if(isset($configXML->appSettings)) {
                foreach ($configXML->appSettings->add as $setting) {
                    $config[(string)$setting->attributes()->key] = (string)$setting->attributes()->value;
                }//end foreach
            }//end if
        } else {
            throw new Exception("Unable to locate web.config in $configPath");
        }//end if
        return $config;
    }//end function
}//end if

if(!function_exists('xss_sanitize')){
    function xss_sanitize($string, $removeTags = true) {
        if($removeTags) {
            $string = strip_tags($string);
        }//end if
        $string = str_replace("javascript:", "", $string);
        $string = htmlEntities($string, ENT_QUOTES, "utf-8");
        $string = str_replace("(", "&#40;", $string);
        $string = str_replace(")", "&#41;", $string);
        return $string;
    }//end function
}//end if