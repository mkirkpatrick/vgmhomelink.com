<?php
addHtmlTag("/css/parsley.css","css");
addHtmlTag(array("/scripts/parsley.min.js","/modules/bill-pay/js/form.js"),"javascript");
addExternalScript('<script src="//maps.google.com/maps/api/js"></script>');
?>

<script type="text/javascript">

function CalculateTotal() {
    var grandTotal = 0;
    $(".money-mask").each(function(){
        grandTotal = Number(grandTotal) + Number($(this).val());           
    })
    //alert(grandTotal);
    document.getElementsByName('Payment_Total')[0].value = '$' + grandTotal.toFixed(2);
    grandTotal = 0;
}
    
var $currentCount = 1;
$('#AddInvoiceButton').click(function() {
    $currentCount++;
    if ($currentCount > 0) {
        $.ajax({
            type: "GET",
            url: "/modules/bill-pay/includes/add-invoice.php?Inv=" + $currentCount,
            data: { },
            success: function(data){
                $('#addInvoice').fadeIn(300, function() { $(this).append(data); }); 
                $('html,body').animate({ scrollTop: $('.invoiceCount' + $currentCount).offset().top - 100 }, 1000);
                $('#RemoveInvoiceButton').fadeIn();
            }
        });
    } else {
        //alert('Please do not enter more than 5 invoices.');
    }

});

$('#RemoveInvoiceButton').click(function() {
    $(".invoiceCount" + $currentCount).fadeOut(300, function() { $(this).remove(); }); 
    $currentCount--;

    if($currentCount == 1) {
        $(this).fadeOut();
        $('html,body').animate({ scrollTop: $('#addInvoice').offset().top - 100 }, 1000); 
    } else {
        $('html,body').animate({ scrollTop: $('.invoiceCount' + $currentCount).offset().top - 100 }, 1000);
    }
    setTimeout(function() {
        CalculateTotal();
    }, 500);
});
</script>