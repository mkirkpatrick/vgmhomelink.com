<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/config.php');
$invNum = isset($_GET['Inv']) ? xss_sanitize($_GET['Inv']) : "1";
?>

<div class="invoiceCount<?= $invNum ?>">
    <div class="section" id="InvoiceDiv<?= $invNum ?>">
        <li class="sixcol">
            <label for="invoice-number<?= $invNum ?>" class="required">Invoice <?= $invNum ?></label>
            <input type="text" id="invoice-number<?= $invNum ?>" name="Invoice_Number<?= $invNum ?>" maxlength="40" required data-parsley-required-message="Enter an Invoice Number." value="<?//= $invoiceNumbers ?>" />
        </li>
        <li class="sixcol last">
            <label for="payment-amount<?= $invNum ?>" class="required">Payment Amount <?= $invNum ?></label>
            <input class="money-mask" type="text" id="payment-amount<?= $invNum ?>" name="Payment_Amount<?= $invNum ?>" maxlength="40" required data-parsley-required-message="Enter a Payment Amount." placeholder="$0.00" autocomplete="off" value="<?//= $paymentAmount ?>" onblur="CalculateTotal()" />
        </li>
    </div>
</div>
