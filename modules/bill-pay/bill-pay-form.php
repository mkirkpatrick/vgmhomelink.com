<?php
global $_WEBCONFIG;
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php");

if ($form->getNumErrors() > 0) {
    // Get Field Values
    $firstName        = $form->value("First_Name");
    $middleName       = $form->value("Middle_Name");
    $lastName         = $form->value("Last_Name");
    $companyName      = $form->value("Company_Name");
    $address          = $form->value("Address");
    $address2         = $form->value("Address_2");
    $zip              = $form->value("Zip");
    $city             = $form->value("City");
    $cboState         = $form->value("State");
    $phone            = $form->value("Phone");
    $email            = $form->value("Email");
    $differentBilling = $form->value("DifferentBilling");

    $billingFirstname = $form->value("Billing_Firstname");
    $billingLastname  = $form->value("Billing_Lastname");
    $billingAddress   = $form->value("Billing_Address");
    $billingCity      = $form->value("Billing_City");
    $billingState     = $form->value("Billing_State");
    $billingZip       = $form->value("Billing_Zip");

    $accountNumber    = $form->value("Account_Number");
    $invoiceNumbers   = $form->value("Invoice_Number");
    $paymentAmount    = $form->value("Payment_Amount");
    $paymentTotal     = $form->value("Payment_Total");
    
    $recurring        = $form->value("Recurring_Payment");
    $recurringEmail   = $form->value("Recurring_Contact_Email");
    $recurringPhone   = $form->value("Recurring_Contact_Phone");
    $agreeToTerms     = $form->value("Agree_To_Terms");

} else {
    $firstName        = "";
    $middleName       = "";
    $lastName         = "";
    $companyName      = "";
    $address          = "";
    $address2         = "";
    $zip              = "";
    $city             = "";
    $cboState         = "";
    $phone            = "";
    $email            = "";
    $differentBilling = "";

    $billingFirstname = "";
    $billingLastname  = "";
    $billingAddress   = "";
    $billingCity      = "";
    $billingState     = "";
    $billingZip       = "";

    $accountNumber    = "";
    $invoiceNumbers   = "";
    $paymentAmount    = "";
    $paymentTotal     = "";
    
    $recurring        = "";
    $recurringEmail   = "";
    $recurringPhone   = "";
    $agreeToTerms     = "";
}    

if(isset($_SESSION['CCmessage'])) {
    print $_SESSION['CCmessage'];
    unset($_SESSION['CCmessage']);
}

if ($form->getNumErrors() > 0) {
    $errors	= $form->getErrorArray();
    foreach ($errors as $err) echo $err;
}
?>

<form action="/modules/bill-pay/process.php" enctype="multipart/form-data" id="billPayForm" method="post" name="billPayForm">
    <input id="actionRun" name="actionRun" type="hidden" value="Bill Pay" />
    <?= loadRC() ?>
    <ul class="contactform contactform--billpay clearfix nobullets">
        <li class="twelvecol">
            <h3>Account Information</h3>
        </li>             
        <li class="fourcol">
            <label class="required" for="txtFirstName">First Name</label>
            <input type="text" class="text" id="txtFirstName" name="First_Name" maxlength="75" required="required" title="First Name" placeholder="John" data-parsley-required-message="Enter a First Name." value="<?= $firstName ?>" />
            <?= $form->error("First_Name"); ?>
        </li>
        <li class="fourcol">
            <label for="txtMiddleName">Middle Name</label>
            <input type="text" class="text" id="txtMiddleName" name="Middle_Name" maxlength="75" title="Middle Name" placeholder="J" data-parsley-required-message="Enter a Middle Name." value="<?= $middleName ?>" />
            <?= $form->error("Middle_Name"); ?>
        </li>
        <li class="fourcol last">
            <label class="required" for="txtLastName">Last Name</label>
            <input type="text" class="text" id="txtLastName" name="Last_Name" maxlength="75" required="required" title="Last Name" placeholder="Doe" data-parsley-required-message="Enter a Last Name." value="<?= $lastName ?>" />
            <?= $form->error("Last_Name"); ?>
        </li>           
        <li class="sixcol">
            <label class="required" for="txtAddress">Address</label>
            <input type="text" class="text" id="txtAddress" name="Address" title="Street Address" maxlength="75" required="required" data-parsley-required-message="Enter an Address." value="<?= $address ?>" />
            <?= $form->error("Address"); ?>
        </li>
        <li class="sixcol last">
            <label for="txtAddress2">Address 2</label>
            <input type="text" class="text" id="txtAddress2" name="Address_2" maxlength="75" title="PO Box / APT / Suite" value="<?= $address2 ?>" />
            <?= $form->error("Address_2"); ?>
        </li>                        
        <li class="clearfloat"></li>
        <li class="fourcol">
            <label class="required" for="txtZip">Zip</label>
            <input type="text" class="text zip-mask" id="txtZip" name="Zip" title="Zip - XXXXX" placeholder="XXXXX" maxlength="10" required="required" onblur="checkZip(this.value, 'Bill Pay Form', 1)" data-parsley-required-message="Enter a Zip Code." value="<?= $zip ?>" />
            <?= $form->error("Zip"); ?>
        </li>
        <li class="fourcol">
            <label class="required" for="txtCity">City</label>
            <input type="text" class="text" id="txtCity" name="City" title="City" maxlength="50" required="required" data-parsley-required-message="Enter a City." value="<?= $city ?>" />
            <?= $form->error("City"); ?>
        </li>
        <li class="fourcol last">
            <label class="required" for="cboState">State</label>
            <select class="stateBox" id="cboState" name="State" title="US State" required data-parsley-required-message="Select a State.">
                <option value="">-- Select A State --</option>
                <?php
                $doc=new DOMDocument();
                $doc->load($_SERVER['DOCUMENT_ROOT'] . "/VPanel/data/states.xml");
                $states=$doc->getElementsByTagName( "option" );
                foreach( $states as $state ){
                    $name  = $state->getAttribute('text');
                    $value = $state->getAttribute('value');
                    $selected = $value == $cboState ? ' selected="selected"' : "";
                    printf("<option value=\"%s\"%s>%s</option>\n", $value, $selected, $name);
                } ?>
            </select>
            <?= $form->error("State"); ?>
        </li>

        <li class="sixcol">
            <label class="required" for="txtPhone">Phone</label>
            <input type="tel" class="text" id="txtPhone" name="Phone" required="required" title="(XXX) XXX-XXXX" maxlength="17" placeholder="(XXX) XXX-XXXX" data-parsley-required-message="Enter a Phone Number." value="<?= $phone ?>" />
            <?= $form->error("Phone"); ?>
        </li>
        <li class="sixcol last">
            <label class="required" for="txtEmail">Email</label>
            <input type="email" class="text" id="txtEmail" name="Email" required="required" title="Email Address" maxlength="75" placeholder="email@email.com" data-parsley-required-message="Enter a Email Address." value="<?= $email ?>" />
            <?= $form->error("Email"); ?>
        </li>  
        
        <li class="sixcol">
            <label for="txtCompanyName">Company Name</label>
            <input type="text" class="text" id="txtCompanyName" name="Company_Name" maxlength="200" placeholder="" title="Company Name" data-parsley-required-message="Enter A Company Name." value="<?= $companyName ?>" />
            <?= $form->error("Company_Name"); ?>
        </li>

        <li class="clearfloat"></li>
                        
        <li class="twelvecol billing">
            <input type="checkbox" id="chxDifferentBilling" name="DifferentBilling" value="Yes" <?= $differentBilling == "Yes" ? "checked" : "" ?> />
            <label for="chxDifferentBilling" class="inline">Billing information is different than account information.</label>
            <?= $form->error("DifferentBilling"); ?>
        </li>        
    </ul>

    <ul id="billing-information" class="contactform  contactform--billpay nobullets clearfix<?= $differentBilling == "Yes" ? "" : " none" ?>">
        <li>
            <h3>Billing Information</h3>
        </li>
        <li class="sixcol">
            <label class="required" for="billing-firstname">First Name</label>
            <input type="text" id="billing-firstname" name="Billing_Firstname" value="<?= $billingFirstname ?>" maxlength="75" placeholder="Jane" data-parsley-required-message="Enter a Billing First Name." autocomplete="off" />
            <?= $form->error("Billing_Firstname"); ?>
        </li>
        <li class="sixcol last">
            <label class="required" for="billing-lastname">Last Name</label>
            <input type="text" id="billing-lastname" name="Billing_Lastname" value="<?= $billingLastname ?>" maxlength="75" placeholder="Doe" data-parsley-required-message="Enter a Billing Last Name." autocomplete="off" />
            <?= $form->error("Billing_Lastname"); ?>
        </li>

        <li class="twelvecol">
            <label class="required" for="billing-address">Address</label>
            <input type="text" id="billing-address" name="Billing_Address" value="<?= $billingAddress ?>" maxlength="75" data-parsley-required-message="Enter a Billing Address." autocomplete="off" />
            <?= $form->error("Billing_Address"); ?>
        </li>
        <li class="fourcol">
            <label class="required" for="billing-zip">Zip</label>
            <input type="text" id="billing-zip" name="Billing_Zip" value="<?= $billingZip ?>" maxlength="10" placeholder="XXXXX" data-parsley-required-message="Enter a Billing Zip." onblur="checkZip(this.value, 'Bill Pay Form', 2)" autocomplete="off" />
            <?= $form->error("Billing_Zip"); ?>
        </li>
        <li class="fourcol">
            <label class="required" for="billing-city">City</label>
            <input type="text" id="billing-city" name="Billing_City" value="<?= $billingCity ?>" maxlength="50" data-parsley-required-message="Enter a Billing City." autocomplete="off"  />
            <?= $form->error("Billing_City"); ?>
        </li>
        <li class="fourcol last">
            <label class="required" for="billing-state">State</label>
            <select id="billing-state" name="Billing_State" data-parsley-required-message="Select a Billing State." autocomplete="off" >
                <option value="">-- Select A State --</option>
                <?php 
                $doc=new DOMDocument(); 
                $doc->load($_SERVER['DOCUMENT_ROOT'] . "/VPanel/data/states.xml"); 
                $states=$doc->getElementsByTagName( "option" ); 
                foreach( $states as $state ){
                    $name=$state->getAttribute('text'); 
                    $value=$state->getAttribute('value'); 
                    $selected = $value == $billingState ? ' selected="selected"' : "";
                    printf("<option value=\"%s\"%s>%s</option>\n", $value, $selected, $name);
                } ?> 
            </select>
            <?= $form->error("Billing_State"); ?>                            
        </li>
    </ul>
    
    <ul id="payment-fields" class="contactform  contactform--billpay nobullets clearfix">
        <li class="twelvecol">
            <h3 class="down">Payment Information</h3>
        </li>
        
    <? if ($form->getNumErrors() > 0) {
        print '
        <li class="twelvecol">
            <div class="form-error" style="padding:15px; font-weight: 700;">For security reasons we do not save your card information. Please re-enter your Credit Card information.</div>
        </li>';            
    } ?>        
        
        <li class="threecol">
            <label for="account-number">Account Number (optional)</label>
            <input type="text" id="account-number" name="Account_Number" maxlength="20" value="<?= $accountNumber ?>" />
        </li>
        <li class="clearfix"></li>
        <li class="sixcol">
            <label for="invoice-number" class="required">Invoice</label>
            <input type="text" id="invoice-number" name="Invoice_Number" maxlength="40" required data-parsley-required-message="Enter an Invoice Number." value="<?= $invoiceNumbers ?>" />
            <?= $form->error("Invoice_Number"); ?>
        </li>
        <li class="sixcol last">
            <label for="payment-amount" class="required">Payment Amount</label>
            <input class="money-mask" type="text" id="payment-amount" name="Payment_Amount" maxlength="40" required data-parsley-required-message="Enter a Payment Amount." placeholder="$0.00" autocomplete="off" value="<?= $paymentAmount ?>" onblur="CalculateTotal()" />
            <?= $form->error("Payment_Amount"); ?>
        </li>
        <li id="addInvoice">
        </li>
        <li class="sixcol last" id="InvoiceButtonDiv">
            <ul class="nobullets inline ">
                <li class="threecol">
                    <input id="AddInvoiceButton" type="button" class="green button outline" value="Add Invoice" />
                </li>
                <li class="ninecol last">
                    <input id="RemoveInvoiceButton" type="button" value="- Remove  Invoice" class="button red" style="display: none"/>
                </li>
            </ul>
            
        </li>
        <li class="clearfix"></li>
        <li class="sixcol"><h3>
			<label>PAYMENT TOTAL</label>
            <input name="Payment_Total" type="text" class="big-span-text" style="font-size:40px; font-size:2.5rem; border: none" value="<?= $paymentAmount ?>" placeholder="$0.00" readonly aria-label="Payment Total" /></h3>
        </li>
        <li class="clearfix"><hr></li>

        <li class="sixcol card-types">
            <h3 class="required">Card Type</h3>
            <fieldset>
                <legend class="reader-only">Select a Card Type - Visa, MasterCard, Discover</legend>
                <div class="card twocol">
                    <input type="radio" id="Visa" name="Card_Type" value="VISA" aria-label="Visa" required data-parsley-errors-container="#card-type-error-container" data-parsley-required-message="Select a Credit Card Option." style="vertical-align:middle" />
                    <img src="/images/cards/card-visa.png" alt="VISA" title="VISA" style="vertical-align:middle">
                </div>
                <div class="card twocol">
                    <input type="radio" id="MasterCard" name="Card_Type" value="MasterCard" aria-label="MasterCard" required data-parsley-errors-container="#card-type-error-container" data-parsley-required-message="Select a Credit Card Option." style="vertical-align:middle" />
                    <img src="/images/cards/card-mastercard.png" title="MasterCard" alt="MasterCard Logo" style="vertical-align:middle">
                </div>
                <div class="card twocol">
                    <input type="radio" id="Discover" name="Card_Type" value="Discover" aria-label="Discover" required data-parsley-errors-container="#card-type-error-container" data-parsley-required-message="Select a Credit Card Option." style="vertical-align:middle" />
                    <img src="/images/cards/card-discover.png" title="Discover" alt="Discover Logo" style="vertical-align:middle">
                </div>
            </fieldset>

            <span id="card-type-error-container"></span>
            <?= $form->error("Card_Type"); ?>
        </li>
        
        <li class="clearfix"></li>
        <li class="fourcol">
            <label for="card-number" class="required">Card Number</label>
            <input class="card-mask security-mask" type="text" id="card-number" name="Card_Number" maxlength="19" placeholder="XXXX XXXX XXXX XXXX" required data-parsley-required-message="Enter a Credit Card Number." autocomplete="off" />
            <?= $form->error("Card_Number"); ?>
        </li>
        <li class="fivecol">
            <label class="required">Expiration Date</label>                                           
            <select id="exp-month" name="Expiration_Month" style="width: 46%;" required data-parsley-required-message="Select a Card Expiration Month." data-parsley-errors-container="#card-exp-error-container" aria-label="Card Expiration Month">
                <option value="">-- MONTH --</option>
                <?php
                for ($m=1; $m<=12; $m++) {
                    $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
                    $value = date('m', mktime(0,0,0,$m, 1, date('Y')));
                    print '<option value="' . $value . '">' . $value. '</option>';
                }                    
                ?>
            </select>
            /
            <select id="exp-year" name="Expiration_Year"  style="width: 46%;" required data-parsley-required-message="Select a Card Expiration Year." data-parsley-errors-container="#card-exp-error-container" aria-label="Card Expiration Year">
                <option value="">-- YEAR --</option>
                <?php
                for ($y=0; $y<=7; $y++) {
                    $year = date("Y", strtotime("+ $y year"));
                    //$year = date('Y', mktime(0,0,0,$y, 1, date('Y')));
                    //$value = date('y', mktime(0,0,0,$y, 1, date('y')));
                    $value = date("y", strtotime("+ $y year"));
                    print '<option value="' . $value . '">' . $year . '</option>';
                }                    
                ?>                
            </select>
            <span id="card-exp-error-container"></span>
            <?= $form->error("Expiration_Month"); ?>
            <?= $form->error("Expiration_Year"); ?>
        </li>
        <li class="threecol last">
            <label for="card-ccv" class="required">Security Code</label>
            <input class="security-mask numberMask" type="text" id="card-ccv" name="Secuirty_Code" maxlength="4" placeholder="XXX" required data-parsley-required-message="Enter a Card Security Code." autocomplete="off" />
            <?= $form->error("Secuirty_Code"); ?>
        </li>
        <li class="twelvecol billing recurring-payment none">
            <label for="chxRecurringPayment">
                <input type="checkbox" id="chxRecurringPayment" name="Recurring_Payment" value="Yes" <?= $recurring == "Yes" ? "checked" : "" ?> />
                I would like to set up a recurring payment.</label>
            <?= $form->error("Recurring_Payment"); ?>
        </li>
        <li class="twelvecol">
            <ul id="recurring-information" class="nobullets clearfix<?= $recurring == "Yes" ? "" : " none" ?> ">
                <li class="twelvecol">
                    <h3>Recurring Payment Contact Information</h3>
                </li>
                <li class="twelvecol recurring-contact">
                    <h4>Please provide us the following information to get in touch with you about setting up a recurring payment.</h4>
                </li>
                <li class="sixcol recurring-contact">
                    <label class="required" for="recurring-contact-email">Email Address</label>
                    <input type="text" id="recurring-contact-email" name="Recurring_Contact_Email" value="<?= $recurringEmail ?>" placeholder="email@email.com" data-parsley-required-message="Enter An Email Address." />
                    <?= $form->error("Recurring_Contact_Email"); ?>
                </li>
                <li class="sixcol last recurring-contact">
                    <label class="required" for="recurring-contact-phone">Phone</label>
                    <input type="tel" id="recurring-contact-phone" name="Recurring_Contact_Phone" value="<?= $recurringPhone ?>" placeholder="(XXX) XXX-XXXX" data-parsley-required-message="Enter A Phone Number." />
                    <?= $form->error("Recurring_Contact_Phone"); ?>
                </li>        
            </ul>        
        </li>                                                
        <li class="twelvecol">
            <label for="chxAgree">
                <input type="checkbox" id="chxAgree" name="Agree_To_Terms" value="Yes" required="required" data-parsley-required-message="Agree to the Terms Above." <?= $agreeToTerms == "Yes" ? "checked" : "" ?> />
                I agree to pay by the above method and have read and agreed to the <a target="blank" id="terms" href="/terms-of-service" title="View Terms & Conditions">terms and conditions</a>.</label>
                <?= $form->error("Agree_To_Terms"); ?>
                <div class="g-recaptcha" data-sitekey="6LfIam0UAAAAAMZEgetSGyu2LHIDvMMW_n0xcZxL"></div>
        </li>  
    </ul>
    <ul class="nobullets">        
        <li class="twelvecol floatLeft">
            <input type="submit" id="btnSubmit" value="Submit Payment" class="blue button" />
        </li>
<!--        <li class="sixcol last">
            <?//= vguardOutput() ?>
        </li>-->
    </ul>   

</form>
<script src='https://www.google.com/recaptcha/api.js'></script>
<?php include_once($_SERVER['DOCUMENT_ROOT']. '/modules/bill-pay/includes/scripts.php'); ?>