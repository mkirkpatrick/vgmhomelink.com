<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php"); 
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_vMail.php"); 


class Process extends Database { 

    private $record;
    private $table;
    public $params;

    //------------------------------
    public function __construct($table) {
        //------------------------------
        global $_WEBCONFIG;
        parent::__construct();
        $this->table = $table;
    }

    //------------------------------
    public function _billPay() {
    //------------------------------
        global $_WEBCONFIG, $form, $siteConfig;
        

        $this->record = new Entity($this->table);

        self::_verify();
        if ($form->getNumErrors() > 0) {
            // Errors exist
            $_SESSION['value_array'] = $_POST;
            $_SESSION['error_array'] = $form->getErrorArray();
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            // No Errors In Data.
            // Save Record Information and then process card
            unset($_SESSION['PAYMENT_APPROVED']);
            unset($_SESSION['CCmessage']);
            if($_WEBCONFIG['PAYMENT_GATEWAY'] == 'Authorize.net') {
                include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/bill-pay/library/authorize.net.php');
            } elseif($_WEBCONFIG['PAYMENT_GATEWAY'] == 'PayPal'){
                include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/bill-pay/library/authorize.net.php');
            } elseif($_WEBCONFIG['PAYMENT_GATEWAY'] == 'PayTrace'){
                include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/bill-pay/library/authorize.net.php');
            } elseif($_WEBCONFIG['PAYMENT_GATEWAY'] == 'VirtualMerchant'){
                include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/bill-pay/library/authorize.net.php');
            } else {
                die("PAYMENT GATEWAY NOT FOUND!");
            }

            if(isset($_SESSION['PAYMENT_APPROVED']) && $_SESSION['PAYMENT_APPROVED'] == 1) {
                //print_r($paymentId); 
                //print_r($confirmationNumber); 
                self::sendEmails($this->record, $paymentId, $confirmationNumber);
                redirect($_WEBCONFIG['BILL_PAY_SUCCESS_PAGE']);
            } else {
                // Payment Not Approved
                $form->setError("CreditCardError", "");
                $_SESSION['value_array'] = $_POST;
                $_SESSION['error_array'] = $form->getErrorArray();                
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    //----------------------------
    private function _verify() {
    //----------------------------
        global $_WEBCONFIG, $form;

        $fieldName  = "First_Name";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Enter a First Name.");
        } else {
            $this->record->bp_account_first = $fieldValue;
        }

        $fieldName  = "Middle_Name";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->bp_account_middle = "";
        } else {
            $this->record->bp_account_middle = $fieldValue;
        }        

        $fieldName  = "Last_Name";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Enter a Last Name.");
        } else {
            $this->record->bp_account_last = $fieldValue;
        }

        $fieldName  = "Company_Name";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->bp_account_company = null;
        } else {
            $this->record->bp_account_company = $fieldValue;
        }

        $fieldName  = "Address";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Enter an Address.");
        } else {
            $this->record->bp_account_address = $fieldValue;
        }

        $fieldName  = "Address_2";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->bp_account_address_2 = null;
        } else {
            $this->record->bp_account_address_2 = $fieldValue;
        }        

        $fieldName  = "Zip";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Enter a Zip Code.");
        } else {
            $this->record->bp_account_zip = $fieldValue;
        }

        $fieldName  = "City";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Enter a City.");
        } else {
            $this->record->bp_account_city = $fieldValue;
        }

        $fieldName  = "State";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Select a State.");
        } else {
            $this->record->bp_account_state = $fieldValue;
        }

        $fieldName  = "Phone";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Enter a Phone Number.");
        } else {
            $this->record->bp_account_phone = $fieldValue;
        }

        $fieldName  = "Email";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Enter an Email Address.");
        } else {
            $this->record->bp_account_email = $fieldValue;
        }

        $fieldName  = "DifferentBilling";
        $fieldValue = isset($_POST[$fieldName]) ? strip_tags($_POST[$fieldName]) : "";
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->bp_billing_different = 'No';
            $this->record->bp_billing_first     = $this->record->bp_account_first;
            $this->record->bp_billing_last      = $this->record->bp_account_last;
            $this->record->bp_billing_address   = $this->record->bp_account_address;
            $this->record->bp_billing_address2  = $this->record->bp_account_address_2;
            $this->record->bp_billing_zip       = $this->record->bp_account_zip;
            $this->record->bp_billing_city      = $this->record->bp_account_city;
            $this->record->bp_billing_state     = $this->record->bp_account_state;
        } else {
            $this->record->bp_billing_different = "Yes";


            $fieldName  = "Billing_Firstname";
            $fieldValue = isset($_POST[$fieldName]) ? strip_tags($_POST[$fieldName]) : "";
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                $form->setError($fieldName, "Enter a Billing First Name.");
            } else {
                $this->record->bp_billing_first = $fieldValue;
            }

            $fieldName  = "Billing_Lastname";
            $fieldValue = strip_tags($_POST[$fieldName]);
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                $form->setError($fieldName, "Enter a Billing Last Name.");
            } else {
                $this->record->bp_billing_last = $fieldValue;
            }   

            $fieldName  = "Billing_Address";
            $fieldValue = strip_tags($_POST[$fieldName]);
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                $form->setError($fieldName, "Enter a Billing Address.");
            } else {
                $this->record->bp_billing_address = $fieldValue;
            }

            $fieldName  = "Billing_Zip";
            $fieldValue = strip_tags($_POST[$fieldName]);
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                $form->setError($fieldName, "Enter a Billing Zip Code.");
            } else {
                $this->record->bp_billing_zip = $fieldValue;
            }

            $fieldName  = "Billing_City";
            $fieldValue = strip_tags($_POST[$fieldName]);
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                $form->setError($fieldName, "Enter a Billing City.");
            } else {
                $this->record->bp_billing_city = $fieldValue;
            }

            $fieldName  = "Billing_State";
            $fieldValue = strip_tags($_POST[$fieldName]);
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                $form->setError($fieldName, "Select a Billing State.");
            } else {
                $this->record->bp_billing_state = $fieldValue;
            } 
        }

        $fieldName  = "Account_Number";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->bp_account_number = null;
        } else {
            $this->record->bp_account_number = $fieldValue;
        }
        
        $invString = '';
        $payString = '';
        foreach ($_POST as $key => $value) {
            if (strstr($key, 'Invoice_Number')) {
                $invString .= !isNullOrEmpty($value) ? $value . '|' : '|';
            }
            if (strstr($key, 'Payment_Amount')) {
                $payString .= strlen($value) > 0 && is_numeric($value) ? number_format($value,2,'.',',') . '|' : '|';
            }
        }
        $invString = rtrim($invString, '|');
        $payString = rtrim($payString, '|');
        $this->record->bp_invoice_numbers = $invString;
        $this->record->bp_payment_amounts = $payString;
        
        $fieldName = "Payment_Total";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0 && is_numeric($fieldValue)) {
            $form->setError($fieldName, "");
        } else {
            $this->record->bp_payment_total = str_replace('$', '', $fieldValue);
        }
        
        $fieldName  = "Card_Type";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Select a Card Type.");
        } else {
            $this->record->bp_payment_type = $fieldValue;
        }      

        $fieldName  = "Card_Number";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Enter a Card Number.");
        } else {
            $this->record->bp_payment_card = substr(trim($fieldValue), -4);
        }

        $fieldName  = "Secuirty_Code";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Enter a security code.");
        }

        $fieldName  = "Expiration_Month";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Select an Expiration Month.");
        } else {
            //$this->record->bp_payment_amounts = $fieldValue;
        }        

        $fieldName  = "Expiration_Year";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Select an Expiration Year.");
        } else {
            //$this->record->bp_payment_amounts = $fieldValue;
        }

        $fieldName  = "Recurring_Payment";
        $fieldValue = isset($_POST[$fieldName]) ? strip_tags($_POST[$fieldName]) : "";
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->bp_recurring_payment_setup = "No";
            $this->record->bp_recurring_payment_email = null;
            $this->record->bp_recurring_payment_phone = null;
        } else {
            $this->record->bp_recurring_payment_setup = "Yes";

            $fieldName  = "Recurring_Contact_Email";
            $fieldValue = strip_tags($_POST[$fieldName]);
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                $form->setError($fieldName, "Enter a Contact Email for Recurring Charge.");
            } else {
                $this->record->bp_recurring_payment_email = $fieldValue;
            }

            $fieldName  = "Recurring_Contact_Phone";
            $fieldValue = strip_tags($_POST[$fieldName]);
            if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
                $form->setError($fieldName, "Enter a Contact Phone for Recurring Charge.");
            } else {
                $this->record->bp_recurring_payment_phone = $fieldValue;
            }
        }

        $fieldName  = "Agree_To_Terms";
        $fieldValue = strip_tags($_POST[$fieldName]);
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $form->setError($fieldName, "Please Agree to the Terms & Conditions.");
        } else {
            $this->record->bp_agree_to_terms = "Yes";
        }

        if(!verify_gcaptcha("6LfIam0UAAAAAFnPEySHMocCpiRZPpM95Ga-hO2j")) {
            $form->setError("Agree_To_Terms", "Please complete captcha verification. ");
        }
        
    }

    //-----------------------------------------------------------------------------------------
    private function sendEmails($data, $paymentId, $confirmationNumber = '000000000000000') {
    //-----------------------------------------------------------------------------------------        
        global $_WEBCONFIG,$siteConfig;

        $recipients = $_WEBCONFIG['BILL_PAY_EMAIL_RECIPIENTS'];

        $additionalMessage = "";
        if($data->bp_recurring_payment_setup == "Yes") {
            // Send Recurring Payment Setup Email
            $additionalMessage = "The user has requested to have a recurring payment setup.<br>This user can be reached by one of the following:";
            $additionalMessage .= "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone: {$data->bp_recurring_payment_phone}";
            $additionalMessage .= "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: {$data->bp_recurring_payment_email}.";
        }

        // Client Mailer
        $mailer = new vMail();

        if(isset($_WEBCONFIG) && isset($_WEBCONFIG['GLOBAL_FORM_RECEIPIENT_OVERRIDE'])) { 
            $mailer->addRecipient($_WEBCONFIG['GLOBAL_FORM_RECEIPIENT_OVERRIDE']);
        }
        else {
            if(is_array($recipients)) { 
                foreach($recipients as $name => $email) {
                    $mailer->addRecipient($email, $name);
                }
            }
            else {
                $recipientArray = explode(';', str_replace(' ', '', $recipients), 20); 
                foreach($recipientArray as $email) {
                    $mailer->addRecipient($email);
                }
            }
        }

        // Add Blind Copy
        if(isset($_WEBCONFIG) && isset($_WEBCONFIG['GLOBAL_FORM_BCC_ADDRESS'])) { 
            $mailer->addBlindCopy($_WEBCONFIG['GLOBAL_FORM_BCC_ADDRESS']);
        }

        $mailer->setSubject("{$siteConfig['Company_Name']} Bill Pay Payment");
        $mailer->setMailType("html"); 
        $mailer->setFrom($siteConfig['Default_From_Email'], "{$siteConfig['Company_Name']} Bill Pay");
        $mailer->setReplyTo($siteConfig['Default_Reply_To_Email'], "{$siteConfig['Company_Name']} Bill Pay"); 

        $htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], "/emailtemplates/billpay-customer-confirmation-email.html");
        if(file_exists($htmlFile)) { 
            $fh = fopen($htmlFile, 'r');
            $body = fread($fh, filesize($htmlFile));
            fclose($fh); 
        } 
        else {
            throw new exception("HTML Email Template File Not Found"); 
        }     
        
        $dataText = '<tr>' . PHP_EOL; 
        $dataText .= '<td valign="top"><strong>Name on Account:</strong></td>' . PHP_EOL;
        $dataText .= '<td>' . $data->bp_account_first . ' ' . $data->bp_account_last . '</td>' . PHP_EOL;
        $dataText .= '</tr>' . PHP_EOL;         
        
        $dataText .= '<tr>' . PHP_EOL; 
        $dataText .= '<td valign="top"><strong>Account #:</strong></td>' . PHP_EOL;
        $dataText .= '<td>' . $data->bp_account_number . '</td>' . PHP_EOL;
        $dataText .= '</tr>' . PHP_EOL;               

        $tempData  = explode('|', $data->bp_invoice_numbers);
        $invDisplay = '';
        foreach ($tempData as $key => $value) {
            $invDisplay .= !isNullOrEmpty($value) ? $value . ', ' : ',';
        }
        $invDisplay = rtrim($invDisplay, ', ');
        $dataText .= '<tr>' . PHP_EOL; 
        $dataText .= '<td valign="top"><strong>Invoice Number(s):</strong></td>' . PHP_EOL;
        $dataText .= '<td>' . $invDisplay . '</td>' . PHP_EOL;
        $dataText .= '</tr>' . PHP_EOL;        

        $tempData  = explode('|', $data->bp_payment_amounts);
        $amtDisplay = '';
        foreach ($tempData as $key => $value) {
            $amtDisplay .= '$' . $value . ', ';
        }
        $amtDisplay = rtrim($amtDisplay, ', ');
        $dataText .= '<tr>' . PHP_EOL; 
        $dataText .= '<td valign="top"><strong>Payment Amount(s):</strong></td>' . PHP_EOL;
        $dataText .= '<td>' . $amtDisplay . '</td>' . PHP_EOL;
        $dataText .= '</tr>' . PHP_EOL;

        $dataText .= '<tr>' . PHP_EOL;
        $dataText .= '<td valign="top"><strong>Payment Total:</strong></td>' . PHP_EOL;
        $dataText .= '<td>$' . $data->bp_payment_total . '</td>' . PHP_EOL; // format this value as currency
        $dataText .= '</tr>' . PHP_EOL;
        
        $dataText .= '<tr>' . PHP_EOL; 
        $dataText .= '<td valign="top"><strong>Payment Confirmation #:</strong></td>' . PHP_EOL;
        $dataText .= '<td>' . $confirmationNumber . '</td>' . PHP_EOL;
        $dataText .= '</tr>' . PHP_EOL;

        $mergeFields = array(); 
        $mergeFields["**GUEST**"]        = $siteConfig['Company_Name'];
        $mergeFields["**COMPANYNAME**"]  = "VGM Forbin";
        $mergeFields["**FORMNAME**"]     = "Bill Pay Form";                  
        $mergeFields["**SUBMISSIONID**"] = $paymentId;  
        $mergeFields["**TABLEDATA**"]    = $dataText; 
        $mergeFields["**ADDITIONAL-MESSAGE**"] = $additionalMessage; 
        $mergeFields["**WEBSITE**"]       = $_WEBCONFIG['SITE_URL'];
        $mergeFields["**DISPLAY_URL**"]   = $_WEBCONFIG['SITE_DISPLAY_URL'];
        $mergeFields["**DOMAIN**"]       = $_WEBCONFIG['SITE_URL'];
        $mergeFields["**YEAR**"]         = date("Y", time()); 

        $body = strtr($body, $mergeFields);

        $mailer->setMessage($body);             
        $mailer->sendMail(); 

        /**********************************************************************************************************************************************/
        // Send User Filling Out Bill Pay an Email
        $mailer2 = new vMail();

        if(isset($_WEBCONFIG) && isset($_WEBCONFIG['GLOBAL_FORM_RECEIPIENT_OVERRIDE'])) { 
            $mailer2->addRecipient($_WEBCONFIG['GLOBAL_FORM_RECEIPIENT_OVERRIDE']);
        }
        else {
            $mailer2->addRecipient($data->bp_account_email);
        }

        // Add Blind Copy
        if(isset($_WEBCONFIG) && isset($_WEBCONFIG['GLOBAL_FORM_BCC_ADDRESS'])) { 
            $mailer2->addBlindCopy($_WEBCONFIG['GLOBAL_FORM_BCC_ADDRESS']);
        }

        $mailer2->setSubject($siteConfig['Company_Legal_Name'] . " Payment Confirmation");
        $mailer2->setMailType("html"); 
        $mailer2->setFrom($siteConfig['Default_From_Email'], $siteConfig['Company_Name']);
        $mailer2->setReplyTo($siteConfig['Default_Reply_To_Email'], $siteConfig['Company_Name']); 

        $htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], "/emailtemplates/billpay-confirmation-email.html");
        if(file_exists($htmlFile)) { 
            $fh = fopen($htmlFile, 'r');
            $body = fread($fh, filesize($htmlFile));
            fclose($fh); 
        } 
        else {
            throw new exception("HTML Email Template File Not Found"); 
        }     

        $dataText = '<tr>' . PHP_EOL; 
        $dataText .= '<td valign="top"><strong>Account #:</strong></td>' . PHP_EOL;
        $dataText .= '<td>' . $data->bp_account_number . '</td>' . PHP_EOL;
        $dataText .= '</tr>' . PHP_EOL;               

        $tempData  = explode('|', $data->bp_invoice_numbers);
        $invDisplay = '';
        foreach ($tempData as $key => $value) {
            $invDisplay .= !isNullOrEmpty($value) ? $value . ', ' : ',';
        }
        $invDisplay = rtrim($invDisplay, ', ');
        $dataText .= '<tr>' . PHP_EOL; 
        $dataText .= '<td valign="top"><strong>Invoice Number(s):</strong></td>' . PHP_EOL;
        $dataText .= '<td>' . $invDisplay . '</td>' . PHP_EOL;
        $dataText .= '</tr>' . PHP_EOL;        

        $tempData  = explode('|', $data->bp_payment_amounts);
        $amtDisplay = '';
        foreach ($tempData as $key => $value) {
            $amtDisplay .= '$' . $value . ', ';
        }
        $amtDisplay = rtrim($amtDisplay, ', ');
        $dataText .= '<tr>' . PHP_EOL; 
        $dataText .= '<td valign="top"><strong>Payment Amount(s):</strong></td>' . PHP_EOL;
        $dataText .= '<td>' . $amtDisplay . '</td>' . PHP_EOL;
        $dataText .= '</tr>' . PHP_EOL;

        $dataText .= '<tr>' . PHP_EOL;
        $dataText .= '<td valign="top"><strong>Payment Total:</strong></td>' . PHP_EOL;
        $dataText .= '<td>$' . $data->bp_payment_total . '</td>' . PHP_EOL; // format this value as currency
        $dataText .= '</tr>' . PHP_EOL;
        
        $dataText .= '<tr>' . PHP_EOL; 
        $dataText .= '<td valign="top"><strong>Payment Confirmation #:</strong></td>' . PHP_EOL;
        $dataText .= '<td>' . $confirmationNumber . '</td>' . PHP_EOL;
        $dataText .= '</tr>' . PHP_EOL;       

        $mergeFields = array(); 
        $mergeFields["**CUSTOMER-NAME**"] = $data->bp_account_first . ' ' . $data->bp_account_last;
        $mergeFields["**COMPANYNAME**"]   = $siteConfig['Company_Name'];
        $mergeFields["**TABLEDATA**"]     = $dataText;
        $mergeFields["**WEBSITE**"]       = $_WEBCONFIG['SITE_URL'];
        $mergeFields["**DISPLAY_URL**"]   = $_WEBCONFIG['SITE_DISPLAY_URL'];
        $mergeFields["**YEAR**"]          = date("Y", time()); 
        $mergeFields["**CONTACT-EMAIL**"] = "info@" . str_replace(array("http://","https://","www."),"", trim($_WEBCONFIG['SITE_URL'],"/")); 

        $body = strtr($body, $mergeFields);

        $mailer2->setMessage($body);             
        $mailer2->sendMail();              
    }


};

$p = new Process('tbl_bill_pay');
$action = isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {
    case 'Bill Pay' :
        $p->_billPay();
        break;

    default :
        redirect("/");
}
redirect('/online-bill-pay');    
?>