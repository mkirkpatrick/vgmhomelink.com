<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/VPanel/library/classes/payment/class_authnet.php';

$memberIP    = getIP();
$date       = date("Y-m-d G:i:s");
$ExpDate    = $_SESSION['CreditCard']['Exp'];
$ChargedAmt    = $_SESSION['GrandTotal'];  

$sql = "SELECT * FROM tbl_ecommerce_orders WHERE eod_id = $orderId LIMIT 1";
$myOrder = Database::Execute($sql);
$myOrder->MoveNext();        

try {
    $_SESSION['CreditCard']['Number'] = str_replace(" ", "", $_SESSION['CreditCard']['Number']); 
    if($ChargedAmt != 0) {
        $payment = Authnet::instance($ecommerceConfig->Gateway_Login, $ecommerceConfig->Gateway_Key, false);

        #  Setup fields for customer information
        if ($ecommerceConfig->Gateway_TestMode == 'adfadasdasd') {
            $payment->setParameter('x_test_request', 'TRUE');
        }


        //EDIT SO IT IS PULLING FROM THE DB!!!!!!!!!!!!!

        $payment->setParameter('x_first_name', $order->eod_billing_first_name);
        $payment->setParameter('x_last_name', $order->eod_billing_first_name);
        $payment->setParameter('x_address', trim("$order->eod_billing_first_name $order->eod_billing_first_name"));
        $payment->setParameter('x_city', $order->eod_billing_first_name);
        $payment->setParameter('x_state', $order->eod_billing_first_name);
        $payment->setParameter('x_zip', $order->eod_billing_first_name);
        $payment->setParameter('x_country', $order->eod_billing_first_name);
        if (strlen(trim($order->eod_billing_first_name)) > 0) {
            $payment->setParameter('x_email', $order->eod_billing_first_name);
            $payment->setParameter('x_email_customer', 'TRUE');
        }
        if (strlen(trim($order->eod_billing_first_name)) > 0) {
            $payment->setParameter('x_phone', $order->eod_billing_first_name);
        }
        $payment->setParameter('x_cust_id', (isset($_SESSION['member_id']) ? $_SESSION['member_id'] : 0));
        $payment->setParameter('x_customer_ip', $memberIP);
        $payment->setTransaction($_SESSION['CreditCard']['Number'], $ExpDate, $ChargedAmt, $_SESSION['CreditCard']['CVV']);
        $payment->process();

        $paramData = implode("&", $payment->params); 

        $sql      = "UPDATE tbl_ecommerce_orders SET eod_data = '$paramData' WHERE eod_id = $orderId";
        $res      = Database::ExecuteRaw($sql);
    }

    if($ChargedAmt == 0 && isset($_SESSION['GiftCardNumber'])) {
        $response = "Gift Card Used and Price equaled Zero";
        $AuthCode = "01";
        $result = strlen($myOrder->eod_card_response_data) > 0 ? ($myOrder->eod_card_response_data . '\r\nStatus: Approved. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.') : ('Status: Approved. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.');
        $sql      = "UPDATE tbl_ecommerce_orders SET eod_card_response_data = '$result', eod_card_result = '$response', eod_card_auth = '$AuthCode', eod_card_status = 'Approved' WHERE eod_id = $orderId";
        $res      = Database::ExecuteRaw($sql);  
    }
    elseif(isset($_WEBCONFIG['FORBIN-PAYMENT-PROCCESSOR-TESTMODE']) && $_WEBCONFIG['FORBIN-PAYMENT-PROCCESSOR-TESTMODE'] == 'true' && $_SESSION['CreditCard']['Number'] == '4111111111111111') {
        $response = 'FORBIN TEST MODE!'; 
        $AuthCode = '011001100110111101110010011000100110100101101110'; // FORBIN IN BINARY :)  
        $result = strlen($myOrder->eod_card_response_data) > 0 ? ($myOrder->eod_card_response_data . '\r\nStatus: Approved. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.') : ('Status: Approved. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.');              
        $sql      = "UPDATE tbl_ecommerce_orders SET eod_card_response_data = '$result', eod_card_result = '$response', eod_card_auth = '$AuthCode', eod_card_status = 'Approved' WHERE eod_id = $orderId";
        $res      = Database::ExecuteRaw($sql);            
    }
    elseif ($payment->isApproved()) {
        $response = $payment->getRawResponse(); 
        $AuthCode = $payment->getAuthCode();
        $result = strlen($myOrder->eod_card_response_data) > 0 ? ($myOrder->eod_card_response_data . '\r\nStatus: Approved. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.') : ('Status: Approved. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.');
        $sql      = "UPDATE tbl_ecommerce_orders 
                    SET eod_card_response_data = '$result',
                        eod_card_result = '$response',
                        eod_card_auth = '$AuthCode',
                        eod_card_status = 'Approved' 
                    WHERE eod_id = $orderId";
        $res      = Database::ExecuteRaw($sql);
    }
    else if ($payment->isDeclined()) {
        $response = $payment->getRawResponse(); 
        $AuthCode = $payment->getAuthCode();
        $result = strlen($myOrder->eod_card_response_data) > 0 ? ($myOrder->eod_card_response_data . '\r\nStatus: Approved. Declined: ' . $response . '. AuthCode: ' . $AuthCode . '.') : ('Status: Declined. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.');
        $sql      = "UPDATE tbl_ecommerce_orders 
                    SET eod_card_response_data = '$result',
                        eod_card_result = '$response',
                        eod_card_auth = '$AuthCode',
                        eod_card_status = 'Declined' 
                    WHERE eod_id = $orderId";
        $res      = Database::ExecuteRaw($sql);

        $recipient = $_WEBCONFIG['DEVELOPER_EMAIL'];
        $subject   = $ecommerceConfig->Shop_Name . ' CC DECLINED';
        $headers   = 'MIME-Version: 1.0' . "\r\n";
        $headers  .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers  .= "To: $recipient\r\n";
        $headers  .= "From: do_not_reply@forbin.com\r\n";

        $_SESSION['CCmessage']   = "<div class=\"message error\"><center>\n";
        $_SESSION['CCmessage']  .= "<h2 style='margin: 10px;'>*** CREDIT CARD TRANSACTION DECLINED ***</h2>\n";                
        $_SESSION['CCmessage']  .= "<span style='font-size: 16px'>Your transaction was not successfully completed!<br>" . $payment->getResponseText() . "</span>";                                
        $_SESSION['CCmessage']   .= "</center></div>\n";

        $name = isset($_SESSION['member_name']) ? $_SESSION['member_name'] : ($order->eod_billing_first_name . ' ' . $order->eod_billing_first_name);

        $emailMessage   = "<div>\n";
        $emailMessage  .= "Your transaction was not successful per this response:<br><p> " . $response . "</p><br><p>" . $payment->getResponseText() . "</p>";                
        $emailMessage  .= "<p><b>User:</b>  {$name}<br>
        <b>ORDER ID:</b> {$_SESSION['Order_ID']}<br>
        <b>Date:</b> {$date}</p>";                
        $emailMessage  .= "</div>\n";

        mail($recipient, $subject, $emailMessage, $headers);                

        redirect("/online-store/checkout/payment-method.php");
    }
    else {
        $response = $payment->getRawResponse(); 

        $result = strlen($myOrder->eod_card_response_data) > 0 ? ($myOrder->eod_card_response_data . '\r\nStatus: {Error}. Response: {' . $response . '}. AuthCode: {NULL}.') : ('Status: {Error}. Response: {' . $response . '}. AuthCode: {NULL}.');                
        $sql = "UPDATE tbl_ecommerce_orders SET eod_card_response_data = '$result', eod_card_result = '$response', eod_card_auth = 'NULL', eod_card_status = 'Error' WHERE eod_id = $orderId";
        $res = Database::ExecuteRaw($sql);

        $recipient = $_WEBCONFIG['DEVELOPER_EMAIL'];
        $subject   = $ecommerceConfig->Shop_Name . ' CC ERROR';
        $headers   = 'MIME-Version: 1.0' . "\r\n";
        $headers  .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers  .= "To: $recipient\r\n";
        $headers  .= "From: do_not_reply@forbin.com\r\n";

        $_SESSION['CCmessage']   = "<div class=\"message error\"><center>\n";
        $_SESSION['CCmessage']  .= "<h2 style='margin: 10px;'>*** CREDIT CARD TRANSACTION ERROR ***</h2>\n";
        $_SESSION['CCmessage']  .= "<span style='font-size: 16px'>Your transaction was not successfully completed!<br>" . $payment->getResponseText() . "</span>";                                
        $_SESSION['CCmessage']  .= "</center></div>\n";

        $name = isset($_SESSION['member_name']) ? $_SESSION['member_name'] : ($order->eod_billing_first_name . ' ' . $order->eod_billing_first_name);

        $emailMessage   = "<div>\n";
        $emailMessage  .= "Your transaction was not successful per this response:<br><p> " . $response . "</p><br><p>" . $payment->getResponseText() . "</p>";                
        $emailMessage  .= "<p><b>User:</b>  {$name}<br>
        <b>ORDER ID:</b> {$_SESSION['Order_ID']}<br>
        <b>Date:</b> {$date}</p>";                
        $emailMessage  .= "</div>\n";

        mail($recipient, $subject, $emailMessage, $headers);                

        redirect("/online-store/checkout/payment-method.php");
    }
}
catch (Exception $e) {
    $result = strlen($myOrder->eod_card_response_data) > 0 ? ($myOrder->eod_card_response_data . '\r\nStatus: Exception. Response: Error Exception. AuthCode: NULL.') : ('Status: Exception. Response: Error Exception. AuthCode: NULL');                
    $sql = "UPDATE tbl_ecommerce_orders SET eod_card_response_data = '$result', eod_card_result = 'Null', eod_card_auth = 'NULL', eod_card_status = 'Exception' WHERE eod_id = $orderId";            
    $res    = Database::ExecuteRaw($sql);

    # Send Us Message
    $message    = "A payment did not go through due to a timeout exception.\nError Message: " . $e->getMessage() . "\nException Trace: " . $e->getTraceAsString();
    $errMailer    = new vMail();
    $errMailer->addRecipient($ecommerceConfig->Order_Recipients, $ecommerceConfig->Shop_Name);
    //$errMailer->addBlindCopy($siteConfig->sc_developer_email);
    $errMailer->setSubject('Credit Card Payment: Timeout Exception Error');
    $errMailer->setMailType('text');
    $errMailer->setFrom($siteConfig['Default_From_Email'], $ecommerceConfig->Shop_Name);
    $errMailer->setReplyTo($siteConfig['Default_Reply_To_Email'], $ecommerceConfig->Shop_Name);
    $errMailer->setMessage($message);
    $errMailer->sendMail();

    $_SESSION['CCmessage']   = "<div class=\"message error\"><center>\n";
    $_SESSION['CCmessage']  .= "<h2 style='margin: 10px;'>*** CREDIT CARD TRANSACTION ERROR ***</h2>\n";
    $_SESSION['CCmessage']  .= "Your transaction was not successfully completed!<br><b>Payment Processor Timeout</b>";                                
    $_SESSION['CCmessage']  .= "<span style='font-size: 16px'>Your transaction was not successfully completed!<br>Payment Processor Timeout!</span>";
    $_SESSION['CCmessage']  .= "</center></div>\n";            

    redirect("/online-store/checkout/payment-method.php");
}
    ?>