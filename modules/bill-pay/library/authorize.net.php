<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/classes/payment/class_authnet.php';

$userIP     = $_SERVER['REMOTE_ADDR'];
$date       = date("Y-m-d G:i:s");
$ExpDate    = $_POST['Expiration_Month'] . $_POST['Expiration_Year'];
//die($ExpDate);
$ChargedAmt = $this->record->bp_payment_total;  
$testmode   = false;    
$debug      = true; // Writes all transactions to log file    

try {
    $_POST['Card_Number'] = str_replace(" ", "", $_POST['Card_Number']);
    $payment = Authnet::instance($_WEBCONFIG['PAYMENT_GATEWAY_ID'], $_WEBCONFIG['PAYMENT_GATEWAY_KEY'], $testmode);
    
    // Set Parameters for Authorize.net Call
    $payment->setParameter('x_first_name', $this->record->bp_billing_first);
    $payment->setParameter('x_last_name', $this->record->bp_billing_last);
    $payment->setParameter('x_address', $this->record->bp_billing_address . (strlen($this->record->bp_billing_address2) > 0 ? " " . $this->record->bp_billing_address2 : ""));
    $payment->setParameter('x_city', $this->record->bp_billing_city);
    $payment->setParameter('x_state', $this->record->bp_billing_state);
    $payment->setParameter('x_zip', $this->record->bp_billing_zip);
    $payment->setParameter('x_country', "USA");
    $payment->setParameter('x_description', "{$_WEBCONFIG['SITE_DISPLAY_URL']} Online Payment");
    if (strlen(trim($this->record->bp_account_email)) > 0) {
        $payment->setParameter('x_email', $this->record->bp_account_email);
        $payment->setParameter('x_email_customer', 'FALSE');
    }
    if (strlen(trim($this->record->bp_account_phone)) > 0) {
        $payment->setParameter('x_phone', $this->record->bp_account_phone);
    }
    
    $payment->setParameter('x_invoice_num', (strlen($this->record->bp_invoice_numbers) > 0 ? $this->record->bp_invoice_numbers : "-"));
    if(!isNullOrEmpty($this->record->bp_account_number)) {
        $payment->setParameter('x_cust_id', $this->record->bp_account_number);
    }
    $payment->setParameter('x_customer_ip', $userIP);
    $payment->setTransaction($_POST['Card_Number'], $ExpDate, $ChargedAmt, $_POST['Secuirty_Code']);
    $payment->process();

    //breakPoint($payment->__toString());
    
    // Forbin Test Payment
    if(isset($_WEBCONFIG['FORBIN-PAYMENT-PROCCESSOR-TESTMODE']) && $_WEBCONFIG['FORBIN-PAYMENT-PROCCESSOR-TESTMODE'] == 'true' && $_POST['Card_Number'] == '4111111111111111') {
        $response = 'FORBIN TEST MODE!'; 
        $AuthCode = '011001100110111101';
        $result   = 'Status: Approved. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.';              
        $confirmationNumber = $AuthCode;
        
        $this->record->bp_payment_confirmation_number = $confirmationNumber;
        $this->record->bp_date_added      = date("Y-m-d G:i:s");
        $this->record->bp_raw_cc_response = $payment->getRawResponse();
        $this->record->bp_card_results    = $result;
        $this->record->bp_card_auth_code  = $AuthCode;
        $this->record->bp_status          = "Approved";
        $this->record->insert();
        $paymentId = Database::getInsertID();
        
        $_SESSION['PAYMENT_APPROVED'] = 1;

        if($debug === true) {
            // Write to log file
            $my_file = $_SERVER['DOCUMENT_ROOT'] . '/uploads/logs/payment.log';
            $handle = fopen($my_file, 'a');
            $new_data = "\n\n====================================================================================================\n" . date("Y-m-d g:i:s A");
            $new_data .= " - Payment Approved.\n";
            $new_data .= "Payment ID: $paymentId\n";
            $new_data .= "Payment Response: {$payment->getRawResponse()}\n";
            $new_data .= "Payment Auth Code: $AuthCode";
            fwrite($handle, $new_data);        
            fclose($handle);
        }

    } elseif ($payment->isApproved()) { // Payment has been approved
        $response = $payment->getRawResponse(); 
        $AuthCode = $payment->getAuthCode();
        $result = 'Status: Approved. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.';
        $confirmationNumber = $payment->getTransactionID();
        
        $this->record->bp_payment_confirmation_number = $confirmationNumber;
        $this->record->bp_date_added      = date("Y-m-d G:i:s");
        $this->record->bp_raw_cc_response = $payment->getRawResponse();
        $this->record->bp_card_results    = $result;
        $this->record->bp_card_auth_code  = $AuthCode;
        $this->record->bp_status          = "Approved";
        $this->record->insert();
        $paymentId = Database::getInsertID();
        $_SESSION['PAYMENT_APPROVED'] = 1;  

        if($debug === true) {
            // Write to log file
            $my_file = $_SERVER['DOCUMENT_ROOT'] . '/uploads/logs/payment.log';
            $handle = fopen($my_file, 'a');
            $new_data = "\n\n====================================================================================================\n" . date("Y-m-d g:i:s A");
            $new_data .= " - Payment Approved.\n";
            $new_data .= "Payment ID: $paymentId\n";
            $new_data .= "Payment Response: {$payment->getRawResponse()}\n";
            $new_data .= "Payment Auth Code: $AuthCode";
            fwrite($handle, $new_data);        
            fclose($handle);
        }     

    }
    else if ($payment->isDeclined()) { // Payment has been declined
        $response = $payment->getRawResponse(); 
        $AuthCode = $payment->getAuthCode();
        $result = 'Status: Declined. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.';

        $this->record->bp_date_added      = date("Y-m-d G:i:s");
        $this->record->bp_raw_cc_response = $payment->getRawResponse();
        $this->record->bp_card_results    = $result;
        $this->record->bp_card_auth_code  = $AuthCode;
        $this->record->bp_status          = "Declined";
        $this->record->insert();
        $paymentId = Database::getInsertID();
        $_SESSION['PAYMENT_APPROVED'] = 0;

        if($debug === true) {
            // Write to log file
            $my_file = $_SERVER['DOCUMENT_ROOT'] . '/uploads/logs/payment.log';
            $handle = fopen($my_file, 'a');
            $new_data = "\n\n====================================================================================================\n" . date("Y-m-d g:i:s A");
            $new_data .= " - Payment Declined.\n";
            $new_data .= "Payment ID: $paymentId\n";
            $new_data .= "Payment Response: {$payment->getRawResponse()}\n";
            $new_data .= "Payment Auth Code: $AuthCode";
            fwrite($handle, $new_data);        
            fclose($handle);
        }

        $_SESSION['CCmessage']   = "<div class=\"message error\" style=\"background: #CB5456;\"><center>\n";
        $_SESSION['CCmessage']  .= "<h2 style='margin: 10px; color: #FFFFFF;'>*** CREDIT CARD TRANSACTION DECLINED ***</h2>\n";                
        $_SESSION['CCmessage']  .= "<span style='font-size: 16px'>Your transaction was not successfully completed!<br>" . $payment->getResponseText() . "</span>";                                
        $_SESSION['CCmessage']   .= "</center></div>\n";

    } else {
        $response = $payment->getRawResponse(); 
        $AuthCode = $payment->getAuthCode();
        $result = 'Status: Declined. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.';

        $this->record->bp_date_added      = date("Y-m-d G:i:s");
        $this->record->bp_raw_cc_response = $payment->getRawResponse();
        $this->record->bp_card_results    = $result;
        $this->record->bp_card_auth_code  = $AuthCode;
        $this->record->bp_status          = "Declined";
        $this->record->insert();
        $paymentId = Database::getInsertID();
        $_SESSION['PAYMENT_APPROVED'] = 0;

        if($debug === true) {
            // Write to log file
            $my_file = $_SERVER['DOCUMENT_ROOT'] . '/uploads/logs/payment.log';
            $handle = fopen($my_file, 'a');
            $new_data = "\n\n====================================================================================================\n" . date("Y-m-d g:i:s A");
            $new_data .= " - Payment Error.\n";
            $new_data .= "Payment ID: $paymentId\n";
            $new_data .= "Payment Response: {$payment->getRawResponse()}\n";
            $new_data .= "Payment Auth Code: $AuthCode";
            fwrite($handle, $new_data);        
            fclose($handle);
        }

        $_SESSION['CCmessage']   = "<div class=\"message error\" style=\"background: #CB5456;\"><center>\n";
        $_SESSION['CCmessage']  .= "<h2 style='margin: 10px; color: #FFFFFF;'>*** CREDIT CARD TRANSACTION DECLINED ***</h2>\n";                
        $_SESSION['CCmessage']  .= "<span style='font-size: 16px'>Your transaction was not successfully completed!<br>" . $payment->getResponseText() . "</span>";                                
        $_SESSION['CCmessage']   .= "</center></div>\n";
    }
}
catch (Exception $e) {
    $response = $e->getMessage(); 
    $AuthCode = 0;
    $result = 'Status: Declined. Response: ' . $response . '. AuthCode: ' . $AuthCode . '.';

    $this->record->bp_date_added      = date("Y-m-d G:i:s");
    $this->record->bp_raw_cc_response = $e->getMessage(); 
    $this->record->bp_card_results    = $result;
    $this->record->bp_card_auth_code  = $AuthCode;
    $this->record->bp_status          = "Error";
    $this->record->insert();
    $paymentId = Database::getInsertID();
    $_SESSION['PAYMENT_APPROVED'] = 0;
    $_SESSION['CCmessage']   = "<div class=\"message error\" style=\"background: #CB5456;\"><center>\n";
    $_SESSION['CCmessage']  .= "<h2 style='margin: 10px; color: #FFFFFF;'>*** CREDIT CARD TRANSACTION ERROR ***</h2>\n";                               
    $_SESSION['CCmessage']  .= "<span style='font-size: 16px'>Your transaction was not successfully completed!<br /></span>"; 
    $_SESSION['CCmessage']  .= "</center></div>\n";            
}
?>