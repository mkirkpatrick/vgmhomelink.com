<div class="userMessage"><?= displayUserMessage() ?></div>
<form action="/library/FormProcessor.php" enctype="multipart/form-data" id="contactForm" method="post" name="contactForm">
    <input id="hidFormName" name="hidFormName" type="hidden" value="Contact Form" />
    <?=loadRC() ?>
        <ul class="contactform clearfix nobullets">
            <li class="twelvecol">
                <h3>Contact Information</h3> </li>
            <li class="sixcol">
                <label class="required" for="txtName">Name</label>
                <input type="text" class="text" id="txtName" name="Name" required="required" title="Full Name" data-parsley-required-message="Please Enter Your Full Name!" /> </li>
            <li class="sixcol last">
                <label class="required" for="txtPhone">Phone</label>
                <input type="tel" class="text" id="txtPhone" name="Phone" required="required" title="(XXX) XXX-XXXX" placeholder="(XXX) XXX-XXXX" data-parsley-required-message="Please Enter Your Phone Number!" onblur="updatePhone(this.value)" /> </li>
            <li class="sixcol">
                <label class="required" for="txtEmail">Email</label>
                <input type="email" class="text" id="txtEmail" name="Email" required="required" title="Email Address" data-parsley-required-message="Please Enter Your Email Address!" onblur="updateEmail(this.value)" /> </li>
            <li class="sixcol last">
                <label for="txtAddress">Address</label>
                <input type="text" class="text" id="txtAddress" name="Address" title="Street Address" /> </li>
            <li class="clearfloat"></li>
            <li class="fourcol">
                <label for="txtZip">Zip</label>
                <input type="text" class="text zip-mask" id="txtZip" name="Zip" title="Zip - #####" placeholder="#####" onblur="checkZip(this.value, 'Contact Form', 1)" /> </li>
            <li class="fourcol">
                <label for="txtCity">City</label>
                <input type="text" class="text" id="txtCity" name="City" title="City" /> </li>
            <li class="fourcol last">
                <label for="cboState">State</label>
                <select class="stateBox" id="cboState" name="State" title="US State">
                    <option value="">-- Select A State --</option>
                    <?php
                    $doc=new DOMDocument();
                    $doc->load($_SERVER['DOCUMENT_ROOT'] . "/VPanel/data/states.xml");
                    $states=$doc->getElementsByTagName( "option" );
                    foreach( $states as $state ){
                        $name=$state->getAttribute('text');
                        $value=$state->getAttribute('value');
                        printf("<option value=\"%s\">%s</option>\n", $value, $name);
                    } ?>
                </select>
            </li>
            <li class="twelvecol">
                <h3 class="required">What is your reason for contacting us today?</h3> </li>
            <li class="eightcol last">
                <input id="contact1" name="Reason" required="required" value="I would like to request more information" type="radio" data-parsley-required-message="Please Select An Option">
                <label for="contact1" class="inline"> I would like to request more information.</label>
                <br>
                <input id="contact2" name="Reason" required="required" value="I have a question" type="radio" data-parsley-required-message="Please Select An Option">
                <label for="contact2" class="inline"> I have a question</label>
                <br>
                <input id="contact3" name="Reason" required="required" value="I would like to speak with a customer service representative" type="radio" data-parsley-required-message="Please Select An Option">
                <label for="contact3" class="inline"> I would like to speak with a customer service representative</label>
                <br>
                <input id="contact4" name="Reason" required="required" value="I would like to make an appointments" type="radio" data-parsley-required-message="Please Select An Option">
                <label for="contact4" class="inline"> I would like to make an appointment</label>
            </li>
            <li class="sixcol">
                <h3 class="required">How did you hear about us?</h3>
                <select class="referral" id="cboHear" name="Referral" required data-parsley-required-message="Please Select How You Heard About Us!">
                    <option selected="selected" value="">-- Select Referral Source --</option>
                    <option value="Another Website">Another Website</option>
                    <option value="Friend">Friend</option>
                    <option value="Family">Family</option>
                    <option value="Colleague">Colleague</option>
                    <option value="Phone Book">Phone Book</option>
                    <option value="Other">Other</option>
                </select>
                <br>
            </li>
            <li class="sixcol last"></li>
            <li class="sixcol">
                <h3 class="">Best Time To Reach You?</h3>
                <select class="stateBox" id="cboBestTime" name="Best Time To Reach" title="Best Time To Reach You At" data-parsley-required-message="Please Select The Best Time To Reach You!">
                    <option value="">-- Select Best Time --</option>
                    <option value="Morning">Morning</option>
                    <option value="Afternoon">Afternoon</option>
                    <option value="Evening">Evening</option>
                </select>
            </li>
            <li class="sixcol last">
                <h3>How To Reach You?</h3>
                <select class="stateBox" id="cboBestWay" name="Best Way To Reach" title="Best Way To Reach You At" data-parsley-required-message="Please Select The Best Way To Reach You!">
                    <option value="">-- Select How To Reach You --</option>
                    <option value="Email">Email</option>
                    <option value="Phone">Phone</option>
                </select>
            </li>
            <li class="twelvecol last">
                <h3 class="required">Questions / Comments</h3>
                <textarea id="mtxComments" name="Comments" required rows="7" title="Questions / Comments" data-parsley-required-message="Please Enter Your Question/Comments!"></textarea>
            </li>
            <li class="twelvecol last">
                <input id="btnSubmit" type="submit" value="Send Message" class="contactForm button blue" />
            </li>
        </ul>
</form>
<script type="text/javascript">function updatePhone(val) { $('#cboBestWay option:contains("Phone")').html('Phone - ' + val); } function updateEmail(val) { $('#cboBestWay option:contains("Email")').html('Email - ' + val); }</script>
<? include_once($_SERVER['DOCUMENT_ROOT']. '/modules/forms/includes/scripts.php'); ?>