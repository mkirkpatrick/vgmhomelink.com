<div class="userMessage"><?= displayUserMessage() ?></div>
<form action="/library/FormProcessor.php" method="post" enctype="multipart/form-data" id="quickForm"  name="quickForm">
    <input id="hidFormName" name="hidFormName" type="hidden" value="Quick Form">
    <input id="hidPageName" name="hidPageName" type="hidden" value="<?=$_SERVER['REQUEST_PATH'] ?>">
    <?= loadRC() ?>
        <ul class="quickform clearfix nobullets">
            <li class="twelvecol">
                <label class="required" for="txtFName">First Name</label>
                <input type="text" class="text" id="txtFName" name="First Name" required="required" title="First Name" data-parsley-required-message="Please Enter Your First Name!">
            </li>
            <li class="twelvecol">
                <label class="required" for="txtLName">Last Name</label>
                <input type="text" class="text" id="txtLName" name="Last Name" required="required" title="Last Name" data-parsley-required-message="Please Enter Your Last Name!">
            </li>
            <li class="twelvecol">
                <label class="required" for="txtPhone">Phone</label>
                <input type="tel" class="text" id="txtPhone" name="Phone" required="required" title="(XXX) XXX-XXXX" placeholder="(XXX) XXX-XXXX" data-parsley-required-message="Please Enter Your Phone Number!">
            </li>
            <li class="twelvecol">
                <label class="required" for="txtEmail">Email</label>
                <input type="email" class="text" id="txtEmail" name="Email" required="required" title="Email Address" data-parsley-required-message="Please Enter Your Email Address!">
            </li>
            <li class="twelvecol">
                <label class="required">Comments</label>
                <textarea id="mtxComments" name="Comments" rows="7" required="required" title="Questions / Comments" data-parsley-required-message="Please Enter Your Question or Comments!"></textarea>
            </li>
            <li class="twelvecol">
                <input id="btnSubmit" type="submit" value="Submit Request" class="contactForm button blue">
            </li>
        </ul>
</form>
<? include_once($_SERVER['DOCUMENT_ROOT']. '/modules/forms/includes/scripts.php'); ?>