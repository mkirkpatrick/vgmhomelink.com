<?php
$uploadFolder = '/uploads/marketing/';
$sql = "SELECT * FROM tbl_marketing WHERE mk_status = 'Active' AND mk_display_page LIKE '%H%' ORDER BY mk_sort_id, mk_name LIMIT 15";
$record    = Database::Execute($sql);
if ($record->Count() > 0) { ?>
    <section class="marketing" aria-label="Marketing Message">
        <div class="flexslider">
            <ul class="slides nobullets clearfix">
                <?php
                while ($record->MoveNext()) { ?>
                    <li>
                        <div class="mk-bg-slide" style="background: url(<?= $uploadFolder . $record->mk_image ?>) center top no-repeat; background-size: cover;"></div>
                        <div class="container">
                            <div class="mk-text-wrap">
                                <h2 class="mk-title"><?= $record->mk_name ?></h2>
                                <p class="mk-subtitle">
                                    <?= $record->mk_tagline ?>

                                    <?php if(strlen(trim($record->mk_url)) > 0) { ?>
                                        <br /><br />
                                        <a target="_<?= $record->mk_target ?>" class="button green" href="<?= $record->mk_url ?>" title="<?= $record->mk_button_text ?>"><?= $record->mk_button_text ?></a>
                                    <?php } ?>
                                </p>
                            </div>
                        </div>
                    </li>
                    <?
                }// end while
                ?>
            </ul>
        </div><!--eof:container-->
    </section>

    <?php
}
addHtmlTag("/modules/marketing/css/styles.css", "css");
addHtmlTag(array("/modules/marketing/js/marketing.js"), "javascript");