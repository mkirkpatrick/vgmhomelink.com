<?php 
if(strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != "xmlhttprequest") {
    header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);
    die("Forbidden Resource"); 
}
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/config.php');

$currentPage = isset($_REQUEST['ourPage']) ? (int)$_REQUEST['ourPage'] : 1;
$terms       = isset($_REQUEST['terms']) ? $_REQUEST['terms'] : '';
$pagingCount = isset($_REQUEST['pagingCount']) ? (int)$_REQUEST['pagingCount'] : 10;
$query = $terms;
//$terms = explode(" ", $query);

$bottomLimit = ($currentPage - 1) * $pagingCount;    
$upperLimt   = $bottomLimit + $pagingCount;


/* Blog Weights */
$blogNameW         = 4.2;  //blog Name Weight
$blogDescW         = .4;  //blog Description Weight
$blogUrlW          = 3.5;  //blog Description Weight
$blogMetaTitleW    = 2.25;  //blog Meta Title Weight
$blogMetaKeywordsW = 2.25;  //blog Meta Keywords Weight
$blogMetaDescW     = 1;  //blog Meta Desc Weight

/* Settings */ 
$descriptionLength = 350;        

$results = '<div class="results-con">';
$sql = "SELECT bp_title,bp_slug,bp_content,bp_featured_image, 
("; 
/*foreach($terms as $term) {*/
    $term = Database::quote_smart(strtolower($query));
    $termTitleCase = ucwords($term); 
    $sql .= "
    (LENGTH(LOWER(`bp_title`)) - LENGTH(REPLACE(LOWER(`bp_title`), '$term', '')))/LENGTH('$term') * $blogNameW + 
    (LENGTH(LOWER(`bp_slug`)) - LENGTH(REPLACE(LOWER(`bp_slug`), '$term', '')))/LENGTH('$term') * $blogUrlW + 
    (LENGTH(LOWER(`bp_content`)) - LENGTH(REPLACE(LOWER(`bp_content`), '$term', '')))/LENGTH('$term') * $blogDescW + 
    (LENGTH(LOWER(`bp_meta_title`)) - LENGTH(REPLACE(LOWER(`bp_meta_title`), '$term', '')))/LENGTH('$term') * $blogMetaTitleW + 
    (LENGTH(LOWER(`bp_meta_keywords`)) - LENGTH(REPLACE(LOWER(`bp_meta_keywords`), '$term', '')))/LENGTH('$term') * $blogMetaKeywordsW + 
    (LENGTH(LOWER(`bp_meta_description`)) - LENGTH(REPLACE(LOWER(`bp_meta_description`), '$term', '')))/LENGTH('$term') * $blogMetaDescW + "; 
/*}*/
$sql .= "
0) AS weight
FROM  `tbl_blog_post`
WHERE bp_active = 'Active' AND bp_date_published <= NOW() 
AND 
"; 
/*for($i = 0; $i < count($terms); $i++) {
    $term = Database::quote_smart($terms[$i]);
    if($i > 0) { 
        $sql .= $type == "1" ? " AND " : " OR "; 
    }*/
    $sql .= "(bp_title LIKE '%$term%' OR bp_slug LIKE '%$term%' OR bp_content LIKE '%$term%' OR bp_meta_title LIKE '%$term%' OR bp_meta_keywords LIKE '%$term%' OR bp_meta_description LIKE '%$term%') "; 
/*}*/
$sql .= "
    GROUP BY `bp_title`
    ORDER BY weight DESC
    LIMIT $bottomLimit,$upperLimt";

if(strlen($query) > 0) { 
    $record    = Database::Execute($sql);          
    $results .= 
    '<div class="twelvecol">';  
    if($record->Count() > 0) {
        while ($record->MoveNext()) {            
            $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
            $absoluteLink = $protocol . '://' . $_SERVER['HTTP_HOST'] . "/blog/post/" . $record->bp_slug; 
            $description = str_truncate(strip_tags($record->bp_content), 500); 
            print '<div style="clear: both;">' . PHP_EOL; 
            print '<h3><a href="' . $absoluteLink .'">' . $record->bp_title . '</a></h3>' . PHP_EOL; 
            if(strlen($record->bp_featured_image) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/blog/' . $record->bp_featured_image)) { 
                print '<a href="' . $absoluteLink .'" class="title"><img class="threecol" alt="" title="' . $record->bp_title . '" src="' . '/uploads/blog/' . $record->bp_featured_image .'" /></a>' . PHP_EOL; 
                print '<p class="description ninecol last">' . $description . '</p>'  . PHP_EOL; 
                print '</div>' . PHP_EOL;                        
            }
            else {
                print '<a href="' . $absoluteLink .'" class="title">' . $absoluteLink . '</a>' . PHP_EOL; 
                print '<p class="description">' . $description . '</p>'  . PHP_EOL; 
                print '</div>' . PHP_EOL; 
            }     
        }  // end while        

    } else {
        $results .= '
        <div id="result_report">
        <p>We did not find any results for&nbsp;<strong>' . $query . '</strong>.</p>
        <p style="font-size: 19px;" id="didyou"></p>

        <h3>Suggestions:</h3>
        <ul>
        <li>Ensure words are spelled correctly.</li>
        <li>Try using synonyms or related searches.</li>
        <li>Try broadening your search by searching from a different site.</li>
        <li>Try removing search refinements or using more general keywords.</li>
        </ul>

        <h3>Featured Blog Posts:</h3>
        <ul class="nobullets">';
        $results .= getRecentBlogPostTitles(6);
        $results .= '
        </ul>
        </div>' . PHP_EOL;
    }
    $results .= '</div>' . PHP_EOL;
}

$results .= '</div>';

echo $results;

function getRecentBlogPostTitles($limit) {
    $results = "";
    $sql = "SELECT * FROM tbl_blog_post WHERE bp_active = 'Active' AND bp_is_featured = 1 LIMIT $limit";
    $record = Database::Execute($sql);
    if($record->Count() > 0) {
        while($record->MoveNext()) {
            //<img alt="" title="' . $record->bp_title . '" src="/uploads/blog/' . $record->bp_featured_image . '" class="threecol" />
            $description = str_truncate(strip_tags($record->bp_content), 500);
            $results .= '<li><h3><a title="' . $record->bp_title . '" href="/blog/post/' . $record->bp_slug . '">' . $record->bp_title . '</a></h3>' . $description . '</li>';
        }
    }

    return $results;
}