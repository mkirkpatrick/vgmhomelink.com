<?php 
if(strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != "xmlhttprequest") {
    header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);
    die("Forbidden Resource"); 
}
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/config.php');

$currentPage = isset($_REQUEST['ourPage']) ? (int)$_REQUEST['ourPage'] : 1;
$terms       = isset($_REQUEST['terms']) ? $_REQUEST['terms'] : '';
$pagingCount = isset($_REQUEST['pagingCount']) ? $_REQUEST['pagingCount'] : 10;
$query       = $terms;
$terms       = explode(" ", $query);
$bottomLimit = ($currentPage - 1) * $pagingCount;    
$upperLimt   = $bottomLimit + $pagingCount;

/* Weights */
$productNameW = 4.0;     //Product Name Weight
$productDescW = 2.5;     //Product Description Weight
$productMetaTitleW = 1.0;     //Product Meta Title Weight
$productMetaKeywordsW = 1.0;     //Product Meta Keywords Weight
$productMetaDescW = 1.0;     //Product Meta Desc Weight
$productSpecsW = 1.0;     //Product Specs Weight
$productOptionsW = 1.0;     //Product Options Weight

/* Settings */ 
$descriptionLength = 350;        


$results = 
'<div class="results-con">';

$count = 0;                         
// PRODUCT SEARCH
foreach($terms as $term) {
    $term = Database::quote_smart(strtolower($term));                
}

$sql = "SELECT ep_id, cat_id, ep_name, ep_description,ep_permalink,                            
        ((LENGTH(LOWER(`ep_name`)) - LENGTH(REPLACE(LOWER(`ep_name`), '$term', '')))/LENGTH('$term') * $productNameW + 
        (LENGTH(LOWER(`ep_meta_keywords`)) - LENGTH(REPLACE(LOWER(`ep_meta_keywords`), '$term', '')))/LENGTH('$term') * $productMetaKeywordsW + 
        (LENGTH(LOWER(`ep_meta_description`)) - LENGTH(REPLACE(LOWER(`ep_meta_description`), '$term', '')))/LENGTH('$term') * $productMetaDescW + 
        (LENGTH(LOWER(`ep_meta_title`)) - LENGTH(REPLACE(LOWER(`ep_meta_title`), '$term', '')))/LENGTH('$term') * $productMetaTitleW + 
        (LENGTH(LOWER(`ep_specifications`)) - LENGTH(REPLACE(LOWER(`ep_specifications`), '$term', '')))/LENGTH('$term') * $productSpecsW +  
        (LENGTH(LOWER(`ep_description`)) - LENGTH(REPLACE(LOWER(`ep_description`), '$term', '')))/LENGTH('$term') * $productDescW + 0.1) AS weight 
        FROM tbl_ecommerce_products
        WHERE (ep_name LIKE '%$term%' OR ep_description LIKE '%$term%' OR ep_specifications LIKE '%$term%')
        AND ep_status = 'Available'
        ORDER BY weight DESC
        LIMIT $bottomLimit,$upperLimt";
$product = Database::Execute($sql);
$results .= 
'<div class="twelvecol">';
if ($product->Count() > 0) {                            
    while($product->MoveNext()) {                             
        $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
        $catId = substr($product->cat_id, 1, strpos(ltrim($product->cat_id, "|"), "|"));
        $absoluteLink = $protocol . '://' . $_SERVER['HTTP_HOST'] . "/product/" . $product->ep_permalink;
        $results .= '<div class="clearfix">';
        $sql = "SELECT * FROM tbl_ecommerce_uploads WHERE ep_id = $product->ep_id LIMIT 1";
        $image = Database::Execute($sql);

        if($image->COUNT() > 0) { 
            $image->MoveNext();
            $results .= '<div class="fourcol"><a href="' . $absoluteLink . '" class="title"><img src="/uploads/ecommerce/thumbnail/' . $image->eu_thumbnail . '" /></a></div>';
        }
        else {
            $results .= '<div class="fourcol"><a href="' . $absoluteLink . '" class="title"><img src="/images/no-img.jpg" /></a></div>';    
        } 
        $results .= '<div class="sevencol last">
        <h3><a href="' . $absoluteLink . '" class="title">' . $product->ep_name . '</a></h3>';                                                                                       
        $results .= '<div class="description">' . substr(strip_tags($product->ep_description), 0, $descriptionLength) . ' ...<br><a class="learnmore" href="' . $absoluteLink . '">Learn More</a></div>' ; 
        $results .= '</div>';     
        $results .= '</div><hr>';     
    }
}
else {
    $results .= 
    '<div id="result_report">
    <p>We did not find any catalog results for&nbsp;&nbsp;<strong>' . $query . '</strong>.</p>
    <h3>Suggestions:</h3>
    <ul>
    <li>Ensure words are spelled correctly.</li>
    <li>Try using synonyms or related searches.</li>
    <li>Try broadening your search by searching from a different site.</li>
    <li>Try removing search refinements or using more general keywords.</li>
    </ul>';

    $sql = "SELECT ep_id,ep_name, ep_permalink FROM tbl_ecommerce_products WHERE ep_status = 'Available' ORDER BY ep_product_views DESC LIMIT 10";
    $top = Database::Execute($sql);
    if($top->Count() > 0) {
        $results .= '<h3>Top Product Views:</h3>
        <ul>';            
        while($top->MoveNext()) {
            $sql = "SELECT eu_id, eu_name, eu_alt, eu_image, eu_thumbnail 
            FROM tbl_ecommerce_uploads 
            WHERE ep_id = $top->ep_id 
            ORDER BY eu_sort_id, eu_name";
            $picture = Database::Execute($sql);
            $picture->MoveNext();

            if ($picture->Count() > 0) {
                $picTitle   = $picture->eu_name;
                $picAltText = $picture->eu_alt;

                if ($picture->eu_image && file_exists($_SERVER['DOCUMENT_ROOT'] . $_WEBCONFIG['ECOMMERCE_UPLOADS_DIRECTORY'] . '/' . $picture->eu_image)) {
                    $image  = $_WEBCONFIG['ECOMMERCE_UPLOADS_DIRECTORY'] . '/' . $picture->eu_image;
                    $imageFeature = $_WEBCONFIG['ECOMMERCE_UPLOADS_DIRECTORY'] . '/' . $picture->eu_image;
                } else {
                    $picTitle     = "No Image Available";
                    $picAltText   = "No Image Available";            
                    $image        = "/images/no-img-large.jpg";
                }
            }
            else {
                $picTitle     = "No Image Available";
                $picAltText   = "No Image Available";            
                $image        = "/images/no-img-large.jpg";                    
            }                


            $results .= '<li class=""><img width="10%" src="' . $image . '" alt="' . $picAltText . '" title="' . $picTitle . '" /><a href="/product/' . $top->ep_permalink . '">' . $top->ep_name . '</a></li>';
        }
        $results .= '</ul>';
    }
    $results .= 
    '</div>';   
}
$results .= 
'</div>';
$results .= 
'</div>';

echo $results;   