$(document).ready(function(){
    if(tabCount > 1) {
        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#"+tab_id).addClass('current');
        });
    }
});

if(showPages === true) {
    var sitePager = $(".site-paging").paging(sitePageCount, {

        // Set up onclick handler
        onClick: function(ev) {

            ev.preventDefault();

            var sitepage = $(this).data('page');

            $.post('/modules/search/getSearchSitePageResults.php', { ourPage: sitepage, terms: myQuery, pagingCount: myPageCount }, function(data) {
                info = data.split('~~~~~~');
                $("#site-page-content").html(info[0]);       
            });                    
            sitePager.setPage(sitepage);
        },

        // Set up easy format
        format: "<nncnn>",

        // Typical onFormat handler, copied from source file
        onFormat: function(type) {

            switch (type) {

                case 'block':

                    if (!this.active)
                        return '<span class="disabled page">' + this.value + '</span>';
                    else if (this.value != this.page)
                        return '<em><a class="page" href="#' + this.value + '">' + this.value + '</a></em>';
                    return '<span class="current page">' + this.value + '</span>';

                case 'right':
                case 'left':

                    if (!this.active) {
                        return "";
                    }
                    return '<a class="page" href="#' + this.value + '">' + this.value + '</a>';

                case 'next':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="next">Next &raquo;</a>';
                    }
                    return '<span class="page disabled">Next &raquo;</span>';

                case 'prev':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="prev">&laquo; Prev</a>';
                    }
                    return '<span class="disabled page">&laquo; Prev</span>';

                case 'first':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="first">|&lt;</a>';
                    }
                    return '<span class="disabled page">|&lt;</span>';

                case 'last':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="prev">&gt;|</a>';
                    }
                    return '<span class="disabled page">&gt;|</span>';

                case 'fill':
                    if (this.active) {
                        return "...";
                    }
            }
            return ""; // return nothing for missing branches
        }
    });

    $.post('/modules/search/getSearchSitePageResults.php', { ourPage: 1, terms: myQuery, pagingCount: myPageCount }, function(data) {
        $("#site-page-content").html(data);      
    });
}

if(showCatalog === true) {
    var catalogPager = $(".catalog-paging").paging(siteCatalogCount, {

        // Set up onclick handler
        onClick: function(ev) {

            ev.preventDefault();

            var catalogpage = $(this).data('page');

            $.post('/modules/search/getSearchCatalogPageResults.php', { ourPage: catalogpage, terms: myQuery, pagingCount: myPageCount }, function(data) {
                info = data.split('~~~~~~');
                $("#catalog-page-content").html(info[0]);       
            });                    
            catalogPager.setPage(catalogpage);
        },

        // Set up easy format
        format: "<nncnn>",

        // Typical onFormat handler, copied from source file
        onFormat: function(type) {

            switch (type) {

                case 'block':

                    if (!this.active)
                        return '<span class="disabled page">' + this.value + '</span>';
                    else if (this.value != this.page)
                        return '<em><a class="page" href="#' + this.value + '">' + this.value + '</a></em>';
                    return '<span class="current page">' + this.value + '</span>';

                case 'right':
                case 'left':

                    if (!this.active) {
                        return "";
                    }
                    return '<a class="page" href="#' + this.value + '">' + this.value + '</a>';

                case 'next':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="next">Next &raquo;</a>';
                    }
                    return '<span class="page disabled">Next &raquo;</span>';

                case 'prev':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="prev">&laquo; Prev</a>';
                    }
                    return '<span class="disabled page">&laquo; Prev</span>';

                case 'first':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="first">|&lt;</a>';
                    }
                    return '<span class="disabled page">|&lt;</span>';

                case 'last':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="prev">&gt;|</a>';
                    }
                    return '<span class="disabled page">&gt;|</span>';

                case 'fill':
                    if (this.active) {
                        return "...";
                    }
            }
            return ""; // return nothing for missing branches
        }
    });	
    $.post('/modules/search/getSearchCatalogPageResults.php', { ourPage: 1, terms: myQuery, pagingCount: myPageCount }, function(data) {
        $("#catalog-page-content").html(data);      
    });
}

if(showBlog === true) {
    var blogPager = $(".blog-paging").paging(siteBlogCount, {

        // Set up onclick handler
        onClick: function(ev) {

            ev.preventDefault();

            var blogpage = $(this).data('page');

            $.post('/modules/search/getSearchBlogPageResults.php', { ourPage: blogpage, terms: myQuery, pagingCount: myPageCount }, function(data) {
                $("#blog-page-content").html(data);       
            });                    
            blogPager.setPage(blogpage);
        },

        // Set up easy format
        format: "<nncnn>",

        // Typical onFormat handler, copied from source file
        onFormat: function(type) {

            switch (type) {

                case 'block':

                    if (!this.active)
                        return '<span class="disabled page">' + this.value + '</span>';
                    else if (this.value != this.page)
                        return '<em><a class="page" href="#' + this.value + '">' + this.value + '</a></em>';
                    return '<span class="current page">' + this.value + '</span>';

                case 'right':
                case 'left':

                    if (!this.active) {
                        return "";
                    }
                    return '<a class="page" href="#' + this.value + '">' + this.value + '</a>';

                case 'next':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="next">Next &raquo;</a>';
                    }
                    return '<span class="page disabled">Next &raquo;</span>';

                case 'prev':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="prev">&laquo; Prev</a>';
                    }
                    return '<span class="disabled page">&laquo; Prev</span>';

                case 'first':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="first">|&lt;</a>';
                    }
                    return '<span class="disabled page">|&lt;</span>';

                case 'last':

                    if (this.active) {
                        return '<a href="#' + this.value + '" class="prev">&gt;|</a>';
                    }
                    return '<span class="disabled page">&gt;|</span>';

                case 'fill':
                    if (this.active) {
                        return "...";
                    }
            }
            return ""; // return nothing for missing branches
        }
    });	
    $.post('/modules/search/getSearchBlogPageResults.php', { ourPage: 1, terms: myQuery, pagingCount: myPageCount }, function(data) {
        $("#blog-page-content").html(data);      
    });
}
