<?php 
if(strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != "xmlhttprequest") {
    header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);
    die("Forbidden Resource"); 
}
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/config.php');

$currentPage = isset($_REQUEST['ourPage']) ? (int)$_REQUEST['ourPage'] : 1;
$terms       = isset($_REQUEST['terms']) ? $_REQUEST['terms'] : '';
$pagingCount = isset($_REQUEST['pagingCount']) ? $_REQUEST['pagingCount'] : 10;
$query = $terms;
$terms = explode(" ", $query);

$bottomLimit = ($currentPage - 1) * $pagingCount;    
$upperLimt   = $bottomLimit + $pagingCount;


/* Weights */
$type       = 'all';
$pgNameW    = 4.2;    //Page Name Weight
$urlW       = 3.5;   //Url Weight
$metaTitleW = 2.25; //Meta Title Weight
$pgHeaderW  = 2.6; //Header Weight
$pgBodyW    = .4; //Page Body Weight

/* Settings */ 
$descriptionLength = 350;        

$results = '<div class="results-con">';
$sql = "SELECT cms.`cms_id` as id, `cms_title` AS `name`, `url_direct` AS url, metaTitle.`cmsv_value` AS title, 
body.cmsv_value AS body, header.cmsv_value AS header, 
("; 
foreach($terms as $term) {
    $term = Database::quote_smart(strtolower($term)); 
    $termTitleCase = ucwords($term); 
    $sql .= "
    (LENGTH(LOWER(`cms_title`)) - LENGTH(REPLACE(LOWER(`cms_title`), '$term', '')))/LENGTH('$term') * $pgNameW + 
    (LENGTH(LOWER(`url_direct`)) - LENGTH(REPLACE(LOWER(`url_direct`), '$term', '')))/LENGTH('$term') * $urlW +
    (LENGTH(LOWER(body.`cmsv_value`)) - LENGTH(REPLACE(LOWER(body.`cmsv_value`), '$term', '')))/LENGTH('$term') * $pgBodyW +
    (LENGTH(LOWER(metaTitle.`cmsv_value`)) - LENGTH(REPLACE(metaTitle.`cmsv_value`, '$term', '')))/LENGTH('$term') * $metaTitleW + 
    (LENGTH(LOWER(header.`cmsv_value`)) - LENGTH(REPLACE(header.`cmsv_value`, '$term', '')))/LENGTH('$term') * $pgHeaderW + "; 
}
$sql .= "
    0.1) AS weight
    FROM  `tbl_cms` AS cms
    INNER JOIN tbl_cms_name_value AS body ON  cms.cms_id = body.cms_id
    INNER JOIN tbl_cms_name_value AS header ON  cms.cms_id = header.cms_id
    INNER JOIN tbl_cms_name_value AS metaTitle ON  cms.cms_id = metaTitle.cms_id
    WHERE (cms_is_active = 1 AND cms_is_historic = 0 AND cms_is_deleted = 0 
    AND body.cmsv_name = 'Desc' AND header.cmsv_name = 'Title' AND metaTitle.cmsv_name = 'MetaTitle'
    AND cms_title <> '404 Error' AND cms_title <> '403 Forbidden'
    AND cms_title <> 'Site Map' AND cms_title <> 'Site Search')
    AND 
    ("; 
for($i = 0; $i < count($terms); $i++) {
    $term = Database::quote_smart($terms[$i]);
    if($i > 0) { 
        $sql .= $type == "all" ? " AND " : " OR "; 
    }
    $sql .= "(cms_title LIKE '%$term%' OR url_direct LIKE '%$term%' OR body.cmsv_value LIKE '%$term%' OR header.cmsv_value LIKE '%$term%'OR metaTitle.cmsv_value LIKE '%$term%') "; 
}
$sql .= ")
GROUP BY `name`
ORDER BY weight DESC
LIMIT $bottomLimit,$upperLimt";

if(strlen($query) > 0) { 
    $record    = Database::Execute($sql);
    $assets = getAssets();            
    $results .= 
    '<div class="twelvecol">';  
    if($record->Count() > 0) {
        while ($record->MoveNext()) {            
            $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
            $absoluteLink = $protocol . '://' . $_SERVER['HTTP_HOST'] . "/" . $record->url; 
            $description = substr(strip_tags(strtr($record->body, $assets)), 0, $descriptionLength) . " ...";  
            $results .= '<div>' . PHP_EOL; 
            $results .= '<h3><a href="' . $absoluteLink .'" class="title">' . ucwords($record->title) . "</a></h3>" . PHP_EOL; 
            $results .= '<div class="description">' . $description . '</div>'  . PHP_EOL; 
            $results .= '<p>&nbsp;<br><a href="' . $absoluteLink .'" class="button blue">View Page</a></p></div><hr>' . PHP_EOL;    
        }  // end while        

    } else {
        $results .= '
        <div id="result_report">
        <p>We did not find any results for <strong>' . $query . '</strong>.</p>
        <p style="font-size: 19px;" id="didyou"></p>

        <h3>Suggestions:</h3>
        <ul>
        <li>Ensure words are spelled correctly.</li>
        <li>Try using synonyms or related searches.</li>
        <li>Try broadening your search by searching from a different site.</li>
        <li>Try removing search refinements or using more general keywords.</li>
        </ul>

        <h3>Popular Pages:</h3>
        <ul>';
        $results .= getTopCMSPages(10);
        $results .= '
        </ul>
        </div>' . PHP_EOL;
    }
    $results .= '</div>' . PHP_EOL;
}

$results .= '</div>';

echo $results;
