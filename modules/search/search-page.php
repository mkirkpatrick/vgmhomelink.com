<?php
global $_WEBCONFIG,$siteConfig;

if (!isset($siteConfig)) {
    header("Location: /search");
    exit;
}    

/* Variables */ 
$query = isset($_GET['q']) ? Database::quote_smart(xss_sanitize(trim($_GET['q'])), ENT_QUOTES, "utf-8") : '';  
$terms = explode(" ", $query); 
$type = 'all';
$tabCount = 1;

if($_WEBCONFIG['SITE_TYPE'] == 'ECOMMERCE') {
    $catalogResultsCount = getCatalogResultsCount($terms, $query);    
}
else {
    $pageResultsCount    = getPageResultsCount($terms, $query);
    if($_WEBCONFIG['SITE_TYPE'] == 'CATALOG') {
        $catalogResultsCount = getCatalogResultsCount($terms, $query);
        $tabCount++;
    }
    if($_WEBCONFIG['HAS_VPRESS_BLOG'] == 'true') {
        $blogResultsCount    = getBlogResultsCount($terms, $query); 
        $tabCount++;
    }
}
$pagingCount = 10; ?>

<div class="main search-results-cont">
    <form id="SearchBox" action="/search" >
        <input type="search" name="q" id="querys" placeholder="Enter Search Term" value="<?= $query ?>" aria-label="Search our website">
        <input type="submit" class="contactForm button blue" value="Search">
    </form>
    <?php 
    if($tabCount > 1) {
        if($query != "") { ?>
            <div class="container">
                <ul class="tabs">
                    <?php if($_WEBCONFIG['SITE_TYPE'] == 'CATALOG') { ?>                    
                        <li class="tab-link current" data-tab="tab-1">Catalog Results</li>
                        <li class="tab-link" data-tab="tab-2">Site Results</li>
                    <?php }
                    else { ?>
                        <li class="tab-link current" data-tab="tab-1">Site Results</li>    
                    <?php } ?>
                    
                    <?php if($_WEBCONFIG['HAS_VPRESS_BLOG'] == 'true') { ?>
                        <li class="tab-link" data-tab="tab-3">Blog Results</li>
                    <?php } ?>
                </ul>

                <?php if($_WEBCONFIG['SITE_TYPE'] == 'CATALOG') { ?>
                    <div id="tab-1" class="tab-content current">
                        <div class="catalog-paging"></div>
                        <div id="catalog-results-count"><h4><?= $catalogResultsCount ?> Product Results Found</h4></div>
                        <div id="catalog-page-content" class="clearfix"></div>
                        <div class="catalog-paging"></div>            
                    </div>
                    <div id="tab-2" class="tab-content twelvecol"> 
                        <div class="site-paging clearfix"></div>       
                        <div id="site-results-count"><h4><?= $pageResultsCount ?> Page Results Found</h4></div>
                        <div id="site-page-content" class="clearfix"></div>
                        <div class="site-paging clearfix"></div>
                    </div>                    
                <?php } else { ?>
                    <div id="tab-1" class="tab-content current twelvecol"> 
                        <div class="site-paging clearfix"></div>       
                        <div id="site-results-count"><h4><?= $pageResultsCount ?> Page Results Found</h4></div>
                        <div id="site-page-content" class="clearfix"></div>
                        <div class="site-paging clearfix"></div>
                    </div>  
                <?php } ?>              
                <?php if($_WEBCONFIG['HAS_VPRESS_BLOG'] == 'true') { ?>        
                    <div id="tab-3" class="tab-content">  
                        <div class="blog-paging"></div>
                        <div id="blog-results-count"><h4><?= $blogResultsCount ?> Blog Results Found</h4></div>
                        <div id="blog-page-content" class="clearfix"></div>
                        <div class="blog-paging"></div>
                    </div>
                    <?php } ?>
            </div> 
            <?php }
        else { ?>
            <div id="result_report" class="container">           
                <h3>Suggestions:</h3>
                <ul class="">
                    <li>Ensure words are spelled correctly.</li>
                    <li>Try using synonyms or related searches.</li>
                    <li>Try broadening your search by searching from a different site.</li>
                    <li>Try removing search refinements or using more general keywords.</li>
                </ul>

                <h3>Popular Pages:</h3>
                <ul class="clearfix nobullets">
                    <?= getTopCMSPages(12); ?>
                </ul>
            </div>   
            <?php } ?>        
    </div>
    <?php }
else {
if($query != "") { 
        if($_WEBCONFIG['SITE_TYPE'] == 'ECOMMERCE') { ?>
            <div class="twelvecol">
                <div class="catalog-paging"></div>
                <div id="catalog-results-count"><h4><?= $catalogResultsCount ?> Product Results Found</h4></div>
                <div id="catalog-page-content" class="clearfix"></div>
                <div class="catalog-paging"></div>            
            </div>        
            <?php } 
        else { ?>
            <div class="twelvecol"> 
                <div class="site-paging clearfix"></div>       
                <div id="site-results-count"><h4><?= $pageResultsCount ?> Page Results Found</h4></div>
                <div id="site-page-content" class="clearfix"></div>
                <div class="site-paging clearfix"></div>
            </div>
    <?php } 
} else { ?>
                <div id="result_report" class="container">           
                <h3>Suggestions:</h3>
                <ul class="">
                    <li>Ensure words are spelled correctly.</li>
                    <li>Try using synonyms or related searches.</li>
                    <li>Try broadening your search by searching from a different site.</li>
                    <li>Try removing search refinements or using more general keywords.</li>
                </ul>

                <h3>Popular Pages:</h3>
                <ul class="clearfix nobullets">
                    <?= getTopCMSPages(12); ?>
                </ul>
            </div> 
<? }
} ?>

<link rel="stylesheet" href="<?php autoCreateVerison('/modules/search/css/search.css'); ?>" type="text/css" />
<?php if($_WEBCONFIG['SITE_TYPE'] == 'ECOMMERCE') { ?>
    <script type="text/javascript">
        var myPageCount = '<?= $pagingCount ?>';
        var myQuery = '<?= $query ?>';
        var showPages = false;
        var showBlog = false;
        var siteCatalogCount = '<?= $catalogResultsCount ?>';
        var showCatalog = true;
        var tabCount = '<?= $tabCount ?>';
    </script>
    <?php }
else { ?>
    <script type="text/javascript">
        var myPageCount = '<?= $pagingCount ?>';
        var sitePageCount = '<?= $pageResultsCount ?>';
        var myQuery = '<?= $query ?>';
        var showCatalog = false;
        var showBlog = false;
        var showPages = true;
        <?php if($_WEBCONFIG['SITE_TYPE'] == 'CATALOG') { ?>
            var siteCatalogCount = '<?= $catalogResultsCount ?>';
            var showCatalog = true;
        <?php } ?>
        <?php if($_WEBCONFIG['HAS_VPRESS_BLOG'] == 'true') { ?>
            var siteBlogCount = '<?= $blogResultsCount ?>';
            var showBlog = true;
            <?php } ?>

        var tabCount = '<?= $tabCount ?>';
    </script>   
<?php } 

addHtmlTag(array("/scripts/jquery.paging.min.js","/modules/search/js/search.js"),"javascript");


function getPageResultsCount($terms, $query) {
    global $_WEBCONFIG, $siteConfig;
    /* Weights */
    $type = 'all';
    $pgNameW    = 4.2;    //Page Name Weight
    $urlW       = 3.5;   //Url Weight
    $metaTitleW = 2.25; //Meta Title Weight
    $pgHeaderW  = 2.6; //Header Weight
    $pgBodyW    = .4; //Page Body Weight

    /* Settings */ 
    $descriptionLength = 350;        

    $results = '<div class="results-con">';
    $sql = "SELECT cms.`cms_id` as id, `cms_title` AS `name`, `url_direct` AS url, metaTitle.`cmsv_value` AS title, 
    body.cmsv_value AS body, header.cmsv_value AS header, 
    ("; 
    foreach($terms as $term) {
        $term = Database::quote_smart(strtolower($term)); 
        $termTitleCase = ucwords($term); 
        $sql .= "
        (LENGTH(LOWER(`cms_title`)) - LENGTH(REPLACE(LOWER(`cms_title`), '$term', '')))/LENGTH('$term') * $pgNameW + 
        (LENGTH(LOWER(`url_direct`)) - LENGTH(REPLACE(LOWER(`url_direct`), '$term', '')))/LENGTH('$term') * $urlW +
        (LENGTH(LOWER(body.`cmsv_value`)) - LENGTH(REPLACE(LOWER(body.`cmsv_value`), '$term', '')))/LENGTH('$term') * $pgBodyW +
        (LENGTH(LOWER(metaTitle.`cmsv_value`)) - LENGTH(REPLACE(metaTitle.`cmsv_value`, '$term', '')))/LENGTH('$term') * $metaTitleW + 
        (LENGTH(LOWER(header.`cmsv_value`)) - LENGTH(REPLACE(header.`cmsv_value`, '$term', '')))/LENGTH('$term') * $pgHeaderW + "; 
    }
    $sql .= "
    0.1) AS weight
    FROM  `tbl_cms` AS cms
    INNER JOIN tbl_cms_name_value AS body ON  cms.cms_id = body.cms_id
    INNER JOIN tbl_cms_name_value AS header ON  cms.cms_id = header.cms_id
    INNER JOIN tbl_cms_name_value AS metaTitle ON  cms.cms_id = metaTitle.cms_id
    WHERE (cms_is_active = 1 AND cms_is_historic = 0 AND cms_is_deleted = 0 
    AND body.cmsv_name = 'Desc' AND header.cmsv_name = 'Title' AND metaTitle.cmsv_name = 'MetaTitle'
    AND cms_title <> '404 Error' AND cms_title <> '403 Forbidden'
    AND cms_title <> 'Site Map' AND cms_title <> 'Site Search')
    AND 
    ("; 
    for($i = 0; $i < count($terms); $i++) {
        $term = Database::quote_smart($terms[$i]);
        if($i > 0) { 
            $sql .= $type == "all" ? " AND " : " OR "; 
        }
        $sql .= "(cms_title LIKE '%$term%' OR url_direct LIKE '%$term%' OR body.cmsv_value LIKE '%$term%' OR header.cmsv_value LIKE '%$term%'OR metaTitle.cmsv_value LIKE '%$term%') "; 
    }
    $sql .= ")
    GROUP BY `name`
    ORDER BY weight DESC
    LIMIT 1000";
    $record    = Database::Execute($sql);

    return $record->Count();
}

function getCatalogResultsCount($terms, $query) {
    global $_WEBCONFIG, $siteConfig;
    /* Weights */
    $productNameW = 4.0;     //Product Name Weight
    $productDescW = 2.5;     //Product Description Weight
    $productMetaTitleW = 1.0;     //Product Meta Title Weight
    $productMetaKeywordsW = 1.0;     //Product Meta Keywords Weight
    $productMetaDescW = 1.0;     //Product Meta Desc Weight
    $productSpecsW = 1.0;     //Product Specs Weight
    $productOptionsW = 1.0;     //Product Options Weight

    // PRODUCT SEARCH
    foreach($terms as $term) {
        $term = Database::quote_smart(strtolower($term));                   
    }

    $sql = "SELECT ep_id, cat_id, ep_name, ep_description,ep_permalink,                            
    ((LENGTH(LOWER(`ep_name`)) - LENGTH(REPLACE(LOWER(`ep_name`), '$term', '')))/LENGTH('$term') * $productNameW + 
    (LENGTH(LOWER(`ep_meta_keywords`)) - LENGTH(REPLACE(LOWER(`ep_meta_keywords`), '$term', '')))/LENGTH('$term') * $productMetaKeywordsW + 
    (LENGTH(LOWER(`ep_meta_description`)) - LENGTH(REPLACE(LOWER(`ep_meta_description`), '$term', '')))/LENGTH('$term') * $productMetaDescW + 
    (LENGTH(LOWER(`ep_meta_title`)) - LENGTH(REPLACE(LOWER(`ep_meta_title`), '$term', '')))/LENGTH('$term') * $productMetaTitleW + 
    (LENGTH(LOWER(`ep_specifications`)) - LENGTH(REPLACE(LOWER(`ep_specifications`), '$term', '')))/LENGTH('$term') * $productSpecsW +  
    (LENGTH(LOWER(`ep_description`)) - LENGTH(REPLACE(LOWER(`ep_description`), '$term', '')))/LENGTH('$term') * $productDescW + 0.1) AS weight 
    FROM tbl_ecommerce_products
    WHERE (ep_name LIKE '%$term%' OR ep_description LIKE '%$term%' OR ep_specifications LIKE '%$term%')
    AND ep_status = 'Available'
    ORDER BY weight DESC
    LIMIT 1000";
    $record = Database::Execute($sql);

    return $record->Count();
}

function getBlogResultsCount($terms, $query) {
    global $_WEBCONFIG, $siteConfig;
    /* Weights */
    /* Blog Weights */
    $blogNameW         = 4.2;  //blog Name Weight
    $blogDescW         = .4;  //blog Description Weight
    $blogUrlW          = 3.5;  //blog Description Weight
    $blogMetaTitleW    = 2.25;  //blog Meta Title Weight
    $blogMetaKeywordsW = 2.25;  //blog Meta Keywords Weight
    $blogMetaDescW     = 1;  //blog Meta Desc Weight

    /* Settings */ 
    $descriptionLength = 350;        

    $sql = "SELECT bp_title,bp_slug,bp_content,bp_featured_image, 
    ("; 
    foreach($terms as $term) {
        $term = Database::quote_smart(strtolower($term)); 
        $termTitleCase = ucwords($term); 
        $sql .= "
        (LENGTH(LOWER(`bp_title`)) - LENGTH(REPLACE(LOWER(`bp_title`), '$term', '')))/LENGTH('$term') * $blogNameW + 
        (LENGTH(LOWER(`bp_slug`)) - LENGTH(REPLACE(LOWER(`bp_slug`), '$term', '')))/LENGTH('$term') * $blogUrlW + 
        (LENGTH(LOWER(`bp_content`)) - LENGTH(REPLACE(LOWER(`bp_content`), '$term', '')))/LENGTH('$term') * $blogDescW + 
        (LENGTH(LOWER(`bp_meta_title`)) - LENGTH(REPLACE(LOWER(`bp_meta_title`), '$term', '')))/LENGTH('$term') * $blogMetaTitleW + 
        (LENGTH(LOWER(`bp_meta_keywords`)) - LENGTH(REPLACE(LOWER(`bp_meta_keywords`), '$term', '')))/LENGTH('$term') * $blogMetaKeywordsW + 
        (LENGTH(LOWER(`bp_meta_description`)) - LENGTH(REPLACE(LOWER(`bp_meta_description`), '$term', '')))/LENGTH('$term') * $blogMetaDescW + "; 
    }
    $sql .= "
    0) AS weight
    FROM  `tbl_blog_post`
    WHERE bp_active = 'Active' AND bp_date_published <= NOW() 
    AND 
    "; 
    for($i = 0; $i < count($terms); $i++) {
        $term = Database::quote_smart($terms[$i]);
        if($i > 0) { 
            $sql .= $type == "1" ? " AND " : " OR "; 
        }
        $sql .= "(bp_title LIKE '%$term%' OR bp_slug LIKE '%$term%' OR bp_content LIKE '%$term%' OR bp_meta_title LIKE '%$term%' OR bp_meta_keywords LIKE '%$term%' OR bp_meta_description LIKE '%$term%') "; 
    }
    $sql .= "
    GROUP BY `bp_title`
    ORDER BY weight DESC
    LIMIT 1000";
    $record    = Database::Execute($sql);

    return $record->Count();
}

?>