<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/VPanel/modules/forms/form-builder/FormBuilder.php'; // formbuilder class
require_once $_SERVER['DOCUMENT_ROOT'] .'/VPanel/modules/forms/classes/class_asset.php'; // Asset class

$actionRun = isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

if(!isset($_REQUEST['actionRun'])) {
    header("HTTP/1.0 400 Bad Request");
    die("400 Bad Request"); 
}

switch($_REQUEST['actionRun']){
    case 'sendForm': 
        if(!isset($_REQUEST['SETTINGS-guid'])) {
            header("HTTP/1.0 400 Bad Request");
            die("400 Bad Request"); 
        }
        endOnSuccess(FormBuilder::sendForm()); 
        break;
}

function endHow($success,$data){die(json_encode( array('success'=>$success,'data'=>$data) ));}
function endOnError($data){ endHow(false,$data); }
function endOnSuccess($data){ endHow(true,$data);  }



?>