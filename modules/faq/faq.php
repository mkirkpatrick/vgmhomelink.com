<link rel="stylesheet" href="/modules/faq/css/styles.css" />

<?
global $_WEBCONFIG; 

$sql = "SELECT COUNT(*) as Total FROM tbl_faq WHERE faq_status = 'Active';";
$dt	= Database::Execute($sql);
$dt->MoveNext();
$sql = "SELECT * 
			FROM tbl_faq 
			WHERE faq_status = 'Active' 
			ORDER BY faq_sort_id, faq_question;";  

$record	= Database::Execute($sql);
?>

<div class="faq-wrapper">
<?
if ($dt->Total > 0) {

	if ($record->Count() > 0) {
		$record->MoveFirst();

		do {
			print '<h2 id="Q' . $record->faq_id .'">' . $record->faq_question . '</h2>' . "\n";
			print '<div class="faq-content none">' . $record->faq_answer. '</div>';
		} while ($record->MoveNext());

	} else { 
		print "<div class=\"message info\">No FAQs found at this time!</div>\n";
	}
} else {
	print "<div class=\"message info\">No FAQs found at this time!</div>\n";
}
?>
<br>
<p><a  class="blue button" href="#Top">Back to Top</a></p>
</div>

<script>
$('.faq-wrapper h2').click(function() {	
	if($(this).hasClass('active')) {
		$(this).removeClass('active');
		$(this).next('.faq-content').slideUp();
	} else {
		$('.faq-wrapper h2').removeClass('active');
		$('.faq-content').slideUp();
		$(this).addClass('active');
		$(this).next('.faq-content').slideDown();
	}
});
</script>