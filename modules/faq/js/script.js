$(function(){
	
	$(".faq-top").click(function(){
		$("html, body").animate({ scrollTop: $("#Top").offset().top}, 750);		
	})
		
	$("a.question").click(function(){
		var href = $(this).attr('href');
		$("html, body").animate({ scrollTop: $(href).offset().top}, 750);	
	})
})
