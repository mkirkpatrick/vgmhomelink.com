﻿<%@ WebHandler Language="C#" Class="Forbin.VGMHomelink.PDFGeneration.RemitHandler" %>

using System;
using System.IO;
using System.Web;
using Forbin.VGMHomelink.Core.Helpers;
using Forbin.VGMHomelink.Homelink.BusinessLogic;

namespace Forbin.VGMHomelink.PDFGeneration
{
    public class RemitHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string username = HttpContext.Current.Request.QueryString["username"];
            var objClaimStatusCLManager = new DealerClaimCLManager();
            objClaimStatusCLManager._UserName = username;
            //jlight@pyramidhhs.com;
            var objClaimStatusCL = new DealerClaimCL();
            var objDSClaimsData = objClaimStatusCL.fnGetVGMNumberOnUserName(objClaimStatusCLManager);
            if (objDSClaimsData.Tables[0].Rows.Count > 0)
            {
                int count = 0; 
                while(count < objDSClaimsData.Tables[0].Rows.Count) 
                {
                    foreach (var item in objDSClaimsData.Tables[0].Rows[count].ItemArray)
                    {
                        HttpContext.Current.Response.Write(item);
                        if(count != (objDSClaimsData.Tables[0].Rows.Count - 1))
                        {
                            HttpContext.Current.Response.Write("|");
                        }
                    }
                    count++; 
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}