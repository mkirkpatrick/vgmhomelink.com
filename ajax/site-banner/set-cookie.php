<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/config.php');
validateAjaxRequest();

$id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
$memberUsername = isset($_SESSION['member_ad_username']) ? $_SESSION['member_ad_username'] : $_SESSION['member_id'];
    //member_type
    $memberType = $_SESSION['member_type'];
    if($memberType == "Case Manager") {
        $memberType = 1;
    } elseif($memberType == 'Provider') {
        $memberType = 2;
    } else {
        $memberType = 0;
    }

if($id > 0){
    //PUT ID INTO XREF TABLE THEN PASS BACK THAT ARRAY?             $_SESSION['member_id']
    $x = "INSERT INTO tbl_site_banner_xref (m_id, alert_id) VALUES ('{$memberUsername}', {$id})";
    Database::ExecuteRaw($x);

    //Make sure there are none left to show.
    //get all alerts they have been shown from xref table
    $memSql = "SELECT * FROM tbl_site_banner_xref WHERE m_id = {$memId}";
    $memberData = Database::Execute($memSql);
    $shownArray = array(0);
    if($memberData->Count() > 0){
        while($memberData->MoveNext()){
            $shownArray[] = $memberData->alert_id;
        }
    } else {
        // none in table
    }
    // breakPOint($shownArray);
    $shownArray = implode(", ", $shownArray);

    $sql = "SELECT * FROM `tbl_site_banner` WHERE hn_status = 'Active' AND hn_historic_id IS NULL AND hn_is_published = 1 AND (hn_type = {$memberType} OR hn_type = 0) AND hn_id NOT IN ($shownArray) ORDER BY hn_time_start DESC";
    $record = Database::Execute($sql);

    if($record->Count() > 0) {
        $record->MoveNext();
        print $record->hn_id;
    } else {
        //THERE ARE NO MORE TO SHOW, SET CLOSED COOKIE
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on'){
            $secure = true;
        } else{
            $secure = false;
        }
        setcookie("site_banner", $id, time()+60*60*24, "/", "." . str_replace("www.","",$_SERVER['HTTP_HOST']), $secure, true );
        setcookie("site_banner_exp", time(), time()+60*60*24, "/", "." . str_replace("www.","",$_SERVER['HTTP_HOST']), $secure, true );
    }
}