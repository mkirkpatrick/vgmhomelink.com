<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/config.php');
validateAjaxRequest();
$id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
//Get member info (last login date) and check xref table to see what all alerts to show
    //member_id
    $memberUsername = isset($_SESSION['member_ad_username']) ? $_SESSION['member_ad_username'] : $_SESSION['member_id'];
    //member_type
    $memberType = $_SESSION['member_type'];

    if($memberType == "Case Manager") {
        $memberType = 1;
    } elseif($memberType == 'Provider') {
        $memberType = 2;
    } else {
        $memberType = 0;
    }
    //get all alerts they have been shown from xref table
    $memSql = "SELECT * FROM tbl_site_banner_xref WHERE m_id = '{$memberUsername}'";
    $memberData = Database::Execute($memSql);
    $shownArray = array(0);
    if($memberData->Count() > 0){
        while($memberData->MoveNext()){
            $shownArray[] = $memberData->alert_id;
        }
    } else {
        // none in table
    }
    // breakPOint($shownArray);
    $shownArray = implode(", ", $shownArray);

    $sql = "SELECT *
    FROM tbl_site_banner
    WHERE hn_status = 'Active' AND hn_historic_id IS NULL AND hn_is_published = 1 AND (hn_type = {$memberType} OR hn_type = 0) AND hn_id NOT IN({$shownArray})
    ORDER BY hn_time_start DESC";
    $record = Database::Execute($sql);

    if($record->Count() > 0) {
        $record->MoveNext();
        $txtTitle      = $record->hn_title;
        $txtText       = $record->hn_text;
        $txtUrl        = $record->hn_url;
        $txtButtonText = $record->hn_button_text;
        $hnImage       = preg_replace('/(<[^>]+) style=".*?"/i', '$1',$record->hn_image);
        $class         = $record->hn_promo_or_alert;
        $bannerId      = isNullOrEmpty($record->hn_historic_id) ? $record->hn_id : $record->hn_historic_id;
        if($class == 'promo'){
            $hnImage = '<img src="' . autoCreateVerison($siteBannerConfig['UPLOAD_FOLDER'] . $hnImage,'return') . '" alt="Banner Image">';
        }//end if

        // // Check if cookie isset
        // if(isset($_COOKIE['site_banner']) && $_COOKIE['site_banner'] == $bannerId){
        //     if(isset($_COOKIE['site_banner_exp']) && strtotime($record->hn_last_update) > $_COOKIE['site_banner_exp']) {
        //         $showBanner = true;
        //     } else {
        //         $showBanner = false;
        //     }//end if
        // } else {
        //     $showBanner = true;
        // }//end if
        ?>

        <div class="mod-banner <?= $class ?>">
            <div class="container clearfix relative">
                <?php
                if (!isNullOrEmpty($hnImage)) { ?>
                    <div class="banner-image large-only"><?=  $hnImage  ?></div>
                    <?php
                }//end if ?>
                <div class="banner-text<?= isNullOrEmpty($hnImage) ? ' no-image' : '';  ?>">
                    <p class="banner-name"><?= $txtTitle ?></p>
                    <p class="banner-content"><?= $txtText ?>

                        <?php
                        if(!isNullOrEmpty($txtUrl)) { ?>
                            <span class="banner-url">
                                <a href="<?= $txtUrl ?>" target="<?= stristr($txtUrl, 'http') !== false ? '_blank ' : '';?>" class="button primary" title="<?= $txtButtonText ?>" ><?= $txtButtonText ?></a>
                            </span>
                            <?php
                        }//end if ?>
                    </p>
                </div>

                <a class="close" data-id="<?= $bannerId ?>" href="#" title="Close banner">Close</a>
            </div>
        </div>
    <?php
    }
    // else {
    //     $showBanner = false;
    //     $bannerId = 0;
    // }//end if