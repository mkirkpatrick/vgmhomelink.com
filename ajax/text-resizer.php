<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");
validateAjaxRequest(); 

$style = isset($_POST['style']) ? xss_sanitize($_POST['style']) : "text-normal";
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
    $secure = true;
} else {
    $secure = false;
}
setcookie ("style", $style, time()+60*60*24*365, "/", "." . str_replace("www.","",$_SERVER['HTTP_HOST']), $secure, true);
echo $style;