<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/inc-pgtop.php'; ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-cm/inc-nav.php'); ?>
<main class="dashboard horizontal">
	<div class="container clearfix">     
		<div class="grid-row">
			<div class="twelvecolM fivecol--large threecol--xl">
				<? include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-cm/inc-providerInfo.php'); ?>
			</div>

			<div class="twelvecolM sevencol--large ninecol--xl"> 
				<? include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-cm/inc-patient-details.php'); ?>
			</div>
		</div>
	</div>
</main>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgbot.php'; ?>