<?php 
include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgtop.php'; 
if(isset($_SESSION['member_prov_num'])) { 
    include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-nav.php'); 
} else {
    include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-cm/inc-nav.php'); 
}
?>
<main class="dashboard">
	<div class="container clearfix">     
		<div class="grid-row">
			<div class="twelvecolM"> 
				<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes/inc-change-password.php'); ?>
			</div>
		</div>
	</div>
</main>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgbot.php'; ?>