<?php 
namespace Forbin\Members\Ajax; 
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

use Forbin\Members\Library\ViperAPI;
use Forbin\Handlers\ErrorHandler; 
use Forbin\Library\Classes\Member;

Member::_secureCheck();
$orders = null; 
try {
    /*$error = 'Always throw this error';
    throw new \Exception($error); */ 
    $orders = ViperAPI::getPaymentDetailsByCheckNo($_SESSION['member_prov_num'], $_GET['EFT']);  
} catch (Exception $e) {
    ErrorHander::log_exception($e); 
    print '<p><b>Unexpected Error, please try again. </b></p'; 
    exit; 
}
if(isset($orders->PaymentSummaries->PaymentSummary)) {
    $payment = $orders->PaymentSummaries->PaymentSummary; 
?> <a target="EFTSearch" href="/remithandler.ashx?token=<?= isset($_SESSION['member_secure_token']) ? $_SESSION['member_secure_token'] : '' ?>&username=<?= $_SESSION['member_username'] ?>&vgmno=<?= $payment->VgmNo?>&batchno=<?= $payment->BatchId ?>&eftNo=<?= isset($_GET['EFT']) ? $_GET['EFT'] : '' ?>&accessCode=<?= $_WEBCONFIG['PDF_SERVICE_ACCESS_CODE'] ?>" class="button blue">View Full Remit</a>
<? } else if(isset($orders[0]->PaymentSummaries->PaymentSummary)) { 
    $payment = $orders[0]->PaymentSummaries->PaymentSummary; 
?>
<a target="EFTSearch" href="/remithandler.ashx?token=<?= isset($_SESSION['member_secure_token']) ? $_SESSION['member_secure_token'] : '' ?>&username=<?= $_SESSION['member_username'] ?>&vgmno=<?= $payment->VgmNo?>&eftNo=<?= isset($_GET['EFT']) ? $_GET['EFT'] : '' ?>&batchno=<?= $payment->BatchId ?>&accessCode=<?= $_WEBCONFIG['PDF_SERVICE_ACCESS_CODE'] ?>" class="button blue">View Full Remit</a>
<?php } else { ?>
<p><b>No Referral Found</b></p>
<? } ?>