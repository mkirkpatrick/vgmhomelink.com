<?php 
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");

use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

$providerNumber = $_SESSION['member_prov_num']; 
$cacheKey = $providerNumber . '_WEB_ORDERS_RECENT';
$sdate = isset($_GET['sdate']) ? date('Y-m-d', strtotime($_GET['sdate'])) : date('Y-m-d', strtotime('-700 days')); 
$edate = isset($_GET['edate']) ? date('Y-m-d', strtotime($_GET['edate'])) : date('Y-m-d'); 
$orders = ViperAPI::getProviderOrdersByVGMNo($providerNumber, $sdate, $edate);
$dataArray = array(); 

if(isset($orders->ReferralNo)) {
    $orderData['ReferralNumber'] = $orders->ReferralNo; 
    $orderData['PatientName'] = ucwords(strtolower($orders->PatientName)); 
    $orderData['SSNNumber'] = !isNullOrEmpty($orders->PatientDOB) ? date('m/d/Y', strtotime($orders->PatientDOB)) : '';
    $orderData['ServiceDate'] = !isNullOrEmpty($orders->ServiceDate) ? date('m/d/Y', strtotime($orders->ServiceDate)) : ''; 
    $orderData['EFTCheck'] = $orders->EFTCheckNo; 
    $orderData['Status'] = $orders->OrderStatus; 
    $orderData['InvoiceNumber'] = $orders->InvoiceNo; 
    if($orderData['Status'] <> "Complete") {
        array_push($dataArray, $orderData);
    }
    $json = json_encode($dataArray); 
    Cache::set($cacheKey, $json, 500000);
} else {
    foreach ($orders as $order) {
        $orderData['ReferralNumber'] = $order->ReferralNo; 
        $orderData['PatientName'] = ucwords(strtolower($order->PatientName)); 
        $orderData['SSNNumber'] = !isNullOrEmpty($order->PatientDOB) ? date('m/d/Y', strtotime($order->PatientDOB)) : '';
        $orderData['ServiceDate'] = !isNullOrEmpty($order->ServiceDate) ? date('m/d/Y', strtotime($order->ServiceDate)) : ''; 
        $orderData['EFTCheck'] = $order->EFTCheckNo; 
        $orderData['Status'] = $order->OrderStatus; 
        $orderData['InvoiceNumber'] = $order->InvoiceNo; 
        if($orderData['Status'] <> "Complete") {
            array_push($dataArray, $orderData); 
        }
    }
    $json = json_encode($dataArray); 
    Cache::set($cacheKey, $json, 500000);
}

$cacheKey = $_SESSION['member_prov_num'] . '_STATISTICS'; 
$keys = array($cacheKey . '_DENIEDREFERRALS', 
        $cacheKey . '_ACCEPTEDREFERRALS', 
        $cacheKey . '_YTDPAID', 
        $cacheKey . '_PENDINGPAYMENTS', 
        $cacheKey . '_YTDREFERRALS', 
        $cacheKey . '_SENTREFERRALS');
if(!$statistics = Cache::getMultiple($keys)) {
	$statistics[$cacheKey . '_DENIEDREFERRALS'] = 0;
	$statistics[$cacheKey . '_ACCEPTEDREFERRALS'] = 0;
	$statistics[$cacheKey . '_YTDPAID'] = 0;
	$statistics[$cacheKey . '_PENDINGPAYMENTS'] = 0;
	$statistics[$cacheKey . '_YTDREFERRALS'] = 0; 
	$statistics[$cacheKey . '_SENTREFERRALS'] = 0;
}







$cacheKey = $providerNumber . '_WEB_ORDERS_HISTORY';
$dataArray = array(); 
$orders = ViperAPI::getProviderOrdersByVGMNo($providerNumber, $sdate, $edate);
if(isset($orders->ReferralNo)) {
    $orderData['ReferralNumber'] = $orders->ReferralNo; 
    $orderData['PatientName'] = ucwords(strtolower($orders->PatientName)); 
    $orderData['SSNNumber'] = !isNullOrEmpty($orders->PatientDOB) ? date('m/d/Y', strtotime($orders->PatientDOB)) : '';
    $orderData['ServiceDate'] = !isNullOrEmpty($orders->ServiceDate) ? date('m/d/Y', strtotime($orders->ServiceDate)) : ''; 
    $orderData['EFTCheck'] = $orders->EFTCheckNo; 
    $orderData['Status'] = $orders->OrderStatus; 
    $orderData['InvoiceNumber'] = $orders->InvoiceNo; 
    if($orderData['Status'] == "Complete") {
        array_push($dataArray, $orderData);
    }
    $json = json_encode($dataArray); 
    Cache::set($cacheKey, $json, 500000);
} else {
    foreach ($orders as $order) {
        $orderData['ReferralNumber'] = $order->ReferralNo; 
        $orderData['PatientName'] = ucwords(strtolower($order->PatientName)); 
        $orderData['SSNNumber'] = !isNullOrEmpty($order->PatientDOB) ? date('m/d/Y', strtotime($order->PatientDOB)) : '';
        $orderData['ServiceDate'] = !isNullOrEmpty($order->ServiceDate) ? date('m/d/Y', strtotime($order->ServiceDate)) : ''; 
        $orderData['EFTCheck'] = $order->EFTCheckNo; 
        $orderData['Status'] = $order->OrderStatus; 
        $orderData['InvoiceNumber'] = $order->InvoiceNo; 
        if($orderData['Status'] == "Complete") {
            array_push($dataArray, $orderData); 
        }
    }
    $json = json_encode($dataArray); 
    Cache::set($cacheKey, $json, 500000);
}

Database::ExecuteRaw("UPDATE `tbl_cron_status` SET `cs_last_modified` = NOW() WHERE `cs_provider_no` = '$providerNumber'"); 

die("Complete"); 
?>