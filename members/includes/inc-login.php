

    <fieldset class="loginForm">
        <h2>Login</h2>
        <div class="control-warning loginForm">
            <p>For official <strong>HOMELINK</strong> account holder use only. All other access is strictly forbidden.</p>
            <p class="disclaimer">By successfully signing in you agree to be bound by all rules defined under the <a href="https://www.hhs.gov/hipaa/index.html" title="External link to HIPAA website" target="_blank">HIPAA</a> regulations.</p>
            <button class="button blue" id="noneShallPass">I agree</button>
        </div>
        <div class="login-container none">
        <form id="secureForm" method="post" action="/members/process.php" autocomplete="off">
            <?php loadRC() ?>
            <input type="hidden" name="actionRun" id="actionRun" value="login" />
            <input type="hidden" name="return" id="return" value="<?= isset($return) ? $return : '' ?>" />

            <div id="error" class="message notification error alignCenter" style="display: none;"></div>
            <div id="success" class="message notification success alignCenter" style="display: none;"></div>
            <?php
            if (isset($_GET['errMsg']) && strlen($_GET['errMsg']) > 0) {
                print '<div class="message notification error alignCenter">' . htmlEntities($_GET['errMsg'], ENT_QUOTES, "utf-8") . '</div>';
            } else if (isset($_GET['okMsg']) && strlen($_GET['okMsg']) > 0) {
                print '<div class="message notification info alignCenter">' . htmlEntities($_GET['okMsg'], ENT_QUOTES, "utf-8") . '</div>';
            } ?>
            <ul class="accountMenuLogin">
                <li>
                    <fieldset>
                        <legend class="reader-only">Choose your login type</legend>
                        <input type="radio" name="MemberType" id="MemberTypeProvider" class="text member-type" value="Provider" checked />
                        <label for="MemberTypeProvider">Provider</label>
                        <input type="radio" name="MemberType" id="MemberTypeCaseManager" class="text member-type" value="Case Manager"  />
                        <label for="MemberTypeCaseManager">Claims Professional</label>
                    </fieldset>
                </li>

                <li id="ProviderField">
                    <label for="MembersProviderNumber">Provider Number</label>
                    <input type="text" name="MembersProviderNumber" id="MembersProviderNumber" class="text" data-message="Required!" />
                </li>

                <li>
                    <label for="MembersEmailAddress">Username / Email Address</label>
                    <input type="text" name="MembersEmailAddress" id="MembersEmailAddress" class="email" required data-message="Required!" />
                </li>

                <li>
                    <label for="MembersPassword">Password</label>
                    <input type="password" name="MembersPassword" id="MembersPassword" class="text" required data-message="Required!" />
                </li>

                <li>
                    <input type="submit" name="LoginButton" id="LoginButton" value="Login" class="submitBtn button blue" />
                </li>
            </ul>
            <div class="twelvecol"><a href="javascript:void(0);" class="modalInput openoverlay" rel="#prompt" title="WE CAN HELP - CLICK HERE">Forget Your Password?<br /></a></div>
            </div>
        </form>
    </fieldset>


<!-- user input dialog -->
<div class="modal" id="prompt">
	<a class="close"></a>
    <h2>Forget Your Password?</h2>
	<p>Simply enter the email address that is associated with your account and we'll send you a new, secure password.</p>
    <!-- input form. you can press enter too -->
    <form autocomplete="off" id="forgot">
		<p><input type="text" name="MembersEmailAddress2" id="MembersEmailAddress2" value="" size="50" maxlength="255" aria-label="Member Email Address">
        <button type="submit" class="submit button blue">Reset my password</button></p>
        <button type="button" class="close"> Cancel </button>
	</form>
</div>
<div class="overlay-mask"></div>

<script type="text/javascript">
$(document).ready(function() {
    $('#noneShallPass').on('click', function() {
        $('.control-warning').slideUp();
        $('.login-container').slideDown();
    })
    $(".member-type").on("click", function() {
        var member = $(this).val();
        if (member == "Provider") {
            $("#ProviderField").slideDown();
            $("#MembersProviderNumber").attr("required", true);
        } else {
            $("#ProviderField").slideUp();
            $("#MembersProviderNumber").removeAttr("required");
        }
    });

    $('a.openoverlay').click(function() {
        var overlayrel = $(this).attr('rel');
        $(''+ overlayrel +'').fadeIn();
        $('#forgot').fadeIn();
        $('#MembersEmailAddress2').val('');
        $('.overlay-mask').fadeIn();
        return false;
    });

    $('.modal .close').click(function() {
        $('#prompt').fadeOut();
        $('.overlay-mask').fadeOut();
    });

    $("#prompt form").submit(function(e) {
        var input = $("#MembersEmailAddress2", this).val();
        $("#success").html('');
        $("#error").html('');
        if (input.length > 0) {
            $.post("/members/process.php", { email: input, actionRun: 'forgotPassword', RC_Validate1: $("input[name='RC_Validate2']").val(), RC_Validate2: $("input[name='RC_Validate1']").val(), RC_Validate3: $("input[name='RC_Validate3']").val(), RC_Validate4: $("input[name='RC_Validate4']").val() }, function(data){
                if (data == "OK") {
                    $('#success').html("If an account matches <strong>" + input + "</strong>, you should receive an email with instructions on how to reset your password shortly.");
                    $('#success').show();
                    $(".modal .close").trigger("click");
                    $('#error').hide();
                } else {
                    $('#error').html(data);
                    $(".modal .close").trigger("click");
                    $('#success').hide();
                    $('#error').show();
                }
            });
            return e.preventDefault();
        } else {
            $(".modal .close").trigger("click");
        }
    });
});
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>