<div class="search-wrap">
<form name="Search" id="Search" action="/search" method="get" autocomplete="off" >
	<label for="searchfield" class="reader-only"></label>
	<input type="search" placeholder="Search" id="searchfield" class="searchField" name="q" />
	<input type="submit" class="searchSubmit" value="Search" />
</form>
</div>