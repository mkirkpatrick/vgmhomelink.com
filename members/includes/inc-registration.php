<link rel="stylesheet" href="/members/css/password-reqs.css" property="stylesheet">

<a id="regTop"></a>
<div class="clearfloat"></div>
<?php 
if(isset($_SESSION['VALIDATION_RESPONSE'])) {
    print $_SESSION['VALIDATION_RESPONSE']; 
    unset($_SESSION['VALIDATION_RESPONSE']); 
} ?>
<div id="drawer">Please fill or correct the fields marked with a <samp style="color:red">red</samp> border.</div>

<div id="errors">
	<h2>Please fix these first</h2>
</div>

<!-- the form -->
<form id="entryForm" name="entryForm" method="post" action="process.php" class="twelvecol">
<input type="hidden" name="actionRun" id="actionRun" value="registration" />
<div id="wizard">
    <div class="items">
        <div class="twelvecol">
            <h2 class="alignCenter">Create Your HOMELINK Account</h2>
			<h3>Please enter your login information:</h3>
            <ul class="nobullets">
                <li class="sixcol">
                    <label class="required" for="MembersEmail">Enter Your Email Address</label>
                        <input type="email" name="MembersEmail" id="MembersEmail" class="text" data-email="Y" maxlength="255" required="required" />
                        <em>Your email address will be used as your Username. </em>
                    
				</li>
				 <li class="sixcol last">
					 <label class="required" for="MembersEmail2">Verify Email Address</label>
                        <input type="email" name="MembersEmail2" id="MembersEmail2" class="text" data-email="Y" maxlength="255" required="required" />
                    
					<div class="clearfloat"></div>
                </li>
                
                <? /*<li class="twelvecol alignCenter">
					<div class="message info">To ensure the highest level of security please be sure your password contains at least 8 characters, at least one capital letter, one lowercase letter, at least one number, and at least one&nbsp;special&nbsp;character.</div>
				</li> */ ?>
                
               <li class="sixcol relative">
                    <label class="required" for="MembersPass">Choose a Password </label>
                        <input type="password" name="MembersPass" id="MembersPass" class="text" maxlength="50" required="required" autocomplete="off" />
                    
                    <div class="password-reqs">
                        <p class="password-reqs-title"><strong>Password Requirements</strong></p>
                        <ul class="password-reqs-list nobullets">
                            <li id="lengthLi">At least 8 characters</li>
                            <li id="upperCaseLi">At least one capital letter</li>
                            <li id="lowerCaseLi">At least one lowercase letter</li>
                            <li id="numberLi">At least one number</li>
                            <li id="specialCharLi">At least one special character (! @ # $ % ^ &amp; % * ? _ ~)</li>
                        </ul>
                    </div>
				</li>
                <li class="sixcol last">
                    <label class="required" for="MemembersPass2">Verify Password </label>
                    <input type="password" name="MembersPass2" id="MembersPass2" class="text" data-equals="MembersPass" maxlength="50" autocomplete="off" required="required" />
                </li>
                <li class="sixcol">
                    <label class="required" for="MembersFirst">Your First Name</label>
                        <input type="text" name="MembersFirst" id="MembersFirst" class="text" maxlength="100" required="required" />
                        <em>Please provide your first name.</em>
				</li>
                <li class="sixcol last">
                    <label class="required" for="MembersLast">Your Last Name</label>
                        <input type="text" name="MembersLast" id="MembersLast" class="text" maxlength="100" required="required" />
                </li>
                <li class="sixcol">
                    <label class="required" for="MembersPhone">Phone Number</label>
                        <input type="text" name="MembersPhone" id="MembersPhone" class="text" maxlength="15" required="required" />
                        <em>Example: 999-999-9999 / (999) 999-9999</em>
				</li>
               	<li class="threecol last">
                    <label for="MembersExtension">Extension</label>
                        <input type="text" name="MembersExtension" id="MembersExtension" class="text" maxlength="10" />
                </li>
                <li class="sixcol">
                    <label for="CompanyName">Company Name</label>
                        <input type="text" name="CompanyName" id="CompanyName" class="text" maxlength="255" />
                </li>
                <li class="sixcol last">
                    <label for="ProviderNumber">Provider Number</label>
                        <input type="text" name="ProviderNumber" id="ProviderNumber" class="text" maxlength="15" />
					<em>Not Necessary if you are a Claims Professional</em>
				</li>
                <li class="twelvecol">
                    <input type="submit" class="submitBtn button blue" value="Register" />
                </li>
            </ul>
        </div>
    </div>
</div>
</form>

<script>
    $('#MembersPass').on('focus blur', function () {
		$(this).siblings('.password-reqs').addClass('active');
	});
	
	$('#MembersPass').on('focusoff blur', function () {
		$(this).siblings('.password-reqs').removeClass('active');
    });
</script>