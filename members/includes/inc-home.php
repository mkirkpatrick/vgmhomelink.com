<?
if(isset($_GET['logout'])) {
	session_destroy();
	redirect("/members/login");
}

if(!isset($_SESSION['member_name'])) {
	header("location: /members/login");
	exit;
}

?>
<h2>Account Information</h2>
<?php
$sql = "SELECT * FROM tbl_members
	WHERE m_id = {$_SESSION['member_id']}";
$dt  = Database::Execute($sql);
$dt->MoveNext();

if ($dt->Count() > 0) {
	print '<table width="75%" cellpadding="3" cellspacing="1">' . "\n";
	print '	<tr>
			  <td width="30%" align="left"><b>Name</b></td>
			  <td width="70%" align="left">' . $dt->m_displayname . '</td>
			</tr>';

	print '	<tr>
			  <td width="30%" align="left"><b>Date Registered</b></td>
			  <td width="70%" align="left">' . date("l F jS, Y", strtotime($dt->m_regdate)) . '</td>
			</tr>
			<tr>
			  <td width="30%" align="left"><b>Last Login</b></td>
			  <td width="70%" align="left">' . date("l F jS, Y -- g:m:s a", strtotime($dt->m_last_login)) . '</td>
			</tr>
			<tr>
			  <td width="30%" align="left"><b>Account Status</b></td>
			  <td width="70%" align="left">' . strtoupper($dt->m_status) . '</td>
			</tr>
		   </table>';

} else {
	print '<div class="warning">Unable to retrieve your account information.<br />Contact us immediately!</div>';
}
$sql = "SELECT * FROM tbl_members_addresses
		WHERE m_id = {$_SESSION['member_id']} AND ma_status = 'P'
		ORDER BY ma_date DESC
		LIMIT 1";
$dt  = Database::Execute($sql);
$dt->MoveNext();

if ($dt->Count() > 0) {
	print '<table width="75%" cellpadding="3" cellspacing="1">
			<tr>
			  <td width="30%" align="left"><b>Date Added</b></td>
			  <td width="70%" align="left">' . date("l F jS, Y", strtotime($dt->ma_date)) . '</td>
			</tr>
			<tr>
			  <td width="30%" align="left"><b>Last Updated</b></td>
			  <td width="70%" align="left">' . date("l F jS, Y -- g:m:s a", strtotime($dt->ma_last_update)) . '</td>
			</tr>';

	if(!isNullOrEmpty($dt->ma_firstname)) {
		print '	<tr>
			  <td width="30%" align="left"><b>Name</b></td>
			  <td width="70%" align="left">' . $dt->ma_firstname . ' ' . $dt->ma_lastname . '</td>
			</tr>';
	}

	if ($dt->ma_address1) {
		print '<tr>
				<td width="30%" align="left">&nbsp;</td>
				<td width="70%" align="left">' . $dt->ma_address1 . '</td>
			   </tr>';
	}

	if ($dt->ma_address2) {
		print '<tr>
				<td width="30%" align="left">&nbsp;</td>
				<td width="70%" align="left">' . $dt->ma_address2 . '</td>
			   </tr>';
	}

	if(!isNullOrEmpty($dt->ma_country)) {

		print '	<tr>
				  <td width="30%" align="left"></td>
				  <td width="70%" align="left">' . $dt->ma_city . ', ' . $dt->ma_state . ' ' . $dt->ma_postal_code . '</td>
				</tr>
				</tr>
				  <td width="30%" align="left"><b>Country</b></td>
				  <td width="70%" align="left">' . $dt->ma_country . '</td>
				</tr>
				<tr>
				  <td width="30%" align="left"><b>Phone</b></td>
				  <td width="70%" align="left">' . $dt->ma_phone . ' &nbsp; ' . ($dt->ma_ext ? "x. {$dt->ma_ext}" : '') . '</td>
				</tr>';
	}

	print '</table>';

} else {
	print '<div class="warning">Unable to retrieve your contact information.<br />Contact us immediately!</div>';
}
?>