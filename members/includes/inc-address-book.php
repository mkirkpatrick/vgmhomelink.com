<?php 
global $_WEBCONFIG; 
loadLocalConfig($_SERVER['DOCUMENT_ROOT']. '/members/');

require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/classes/class_form.php'; 
?>
<script type="text/javascript" src="/members/js/account.js"></script>
<script type="text/javascript" src="/members/js/address.js"></script>
<link rel="stylesheet" href="/members/css/members.css" type="text/css" />

<?php
$sql = "SELECT * FROM tbl_members_addresses 
		WHERE m_id = {$_SESSION['member_id']}
		ORDER BY ma_status, ma_firstname, ma_lastname";
$record = Database::Execute($sql);

if ($form->getNumErrors() > 0) {
	$errors	= $form->getErrorArray();
	foreach ($errors as $err) echo $err;
} else if (isset($_SESSION['processed'])) {
	switch($_SESSION['processed']) {
		case 'added':
			echo "<div class=\"valid\">New Address Added Successfully!</div>\n";
			break;
		case 'updated':
			echo "<div class=\"valid\">Address Updated Successfully!</div>\n";
			break;
		case 'deleted':
			echo "<div class=\"valid\">Address Deleted Successfully!</div>\n";
			break;
		case 'status':
			echo "<div class=\"valid\">Address Status Updated Successfully!</div>\n";
			break;
	}
	unset($_SESSION['processed']);
}
?>

<table id="listTable" class="display">
<thead> 
<tr> 
<th style="text-align: left; font-weight: bold;">Name</th>
<th style="text-align: left; font-weight: bold;">Address</th>
<th style="text-align: left; font-weight: bold;">City</th>
<th style="text-align: left; font-weight: bold;">Status</th>
<th style="text-align: center; width:2%; font-weight: bold;">Options</th>
</tr>
</thead>

<tbody>

<?php
if ($record->Count() > 0) {
	while ($record->MoveNext()) {
		$status = $record->ma_status == 'P' ? 'Primary' : 'Additional'; 
		print '<tr> 
				<td style="text-align: left">' . $record->ma_firstname . ' ' . $record->ma_lastname . '</td>
				<td style="text-align: left">' . $record->ma_address1 . '</td>
				<td style="text-align: left">' . $record->ma_city . '</td>
				<td style="text-align: left">' . $status . '</td>'; 
		print '<td><a href="javascript:modifyAddress(' . $record->ma_id . ');" class="btn_edit">Modify</a>'; 
		if($status != 'Primary') { 
			print ' <a href="javascript:deleteAddress(' . $record->ma_id . ');" class="btn_delete">Delete</a>'; 
		}
		print '</td></tr>' . PHP_EOL;
	}// end while
}
?>

  </tbody>
</table>
<br />
<div class="articleBtn"><a href="javascript:addAddress()" >Add New Address</a></div>
