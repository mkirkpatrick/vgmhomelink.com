<?
global $_WEBCONFIG; 
loadLocalConfig($_SERVER['DOCUMENT_ROOT']. '/members/');

require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/classes/class_form.php';

member::_secureCheck(); 
$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

?>
<p><b><?= $_SESSION['member_name']; ?></b> edit your account information.</p>
<?php
$iNum = isset($_GET['id']) && (int)$_GET['id'] > 0 ? (int)$_GET['id'] : 0;
if ($form->getNumErrors() > 0) {
	$hidId				= $form->value("hidId");
	$MembersFirst		= $form->value("MembersFirst");
	$MembersLast		= $form->value("MembersLast");
	$MembersBusiness	= $form->value("MembersBusiness");
	$MembersCountry		= $form->value("MembersCountry");
	$MembersAddress		= $form->value("MembersAddress");
	$MembersAddress2	= $form->value("MembersAddress2");
	$MembersCity		= $form->value("MembersCity");
	$MembersState		= $form->value("MembersState");
	$MembersZip			= $form->value("MembersZip");
	$MembersPhone		= $form->value("MembersPhone");
	$MembersExtension	= $form->value("MembersExtension");
	$MembersFax			= $form->value("MembersFax");
	$MembersWebsite		= $form->value("MembersWebsite"); 
} else if($iNum > 0) {
	$sql  = "SELECT * FROM tbl_members_addresses WHERE ma_id = $iNum";
	$record	= Database::Execute($sql);
	$record->MoveNext();

	if ($record->Count() == 0) {
		throw new exception("Unable to retrieve address book record!");
	}

	$hidId				= $record->ma_id;
	$MembersFirst		= $record->ma_firstname;
	$MembersLast		= $record->ma_lastname;
	$MembersBusiness	= $record->ma_business;
	$MembersCountry		= $record->ma_country;
	$MembersAddress		= $record->ma_address1;
	$MembersAddress2	= $record->ma_address2;
	$MembersCity		= $record->ma_city;
	$MembersState		= $record->ma_state;
	$MembersZip			= $record->ma_postal_code;
	$MembersPhone		= $record->ma_phone;
	$MembersExtension	= $record->ma_ext;
	$MembersFax			= $record->ma_fax;
	$MembersWebsite		= $record->ma_website; 
} else {
	$hidId				= null;
	$MembersFirst		= '';
	$MembersLast		= '';
	$MembersBusiness	= '';
	$MembersCountry		= 'United States of America';
	$MembersAddress		= '';
	$MembersAddress2	= '';
	$MembersCity		= '';
	$MembersState		= '';
	$MembersZip			= '';
	$MembersPhone		= '';
	$MembersExtension	= '';
	$MembersFax			= '';
	$MembersWebsite		= '';
}

if ($form->getNumErrors() > 0) {
	$errors	= $form->getErrorArray();
	foreach ($errors as $err) echo $err;
} else if (isset($_SESSION['processed'])) {
	echo "<div class=\"valid\">Address Updated Successfully!</div>\n";
	unset($_SESSION['processed']);
}
?>

<!-- the form -->
<form id="entryForm" method="post" action="/members/process.php">
<input type="hidden" name="actionRun" id="actionRun" value="<?= $view == 'modify' ? 'address-upd' : 'address-add' ?>" />
<input type="hidden" name="hidId" value="<?= $hidId; ?>" />
<div id="myaccount">

    <ul>
        <!-- name -->
        <li class="required double">
            <label>
                Enter Your First Name <span>*</span><br />
                <input type="text" name="MembersFirst" id="MembersFirst" value="<?= $MembersFirst;?>" class="text" maxlength="100" required="required" />
                <em>Please provide your firstname.</em>
            </label>

            <label>
                Your Last Name <span>*</span><br />
                <input type="text" name="MembersLast" id="MembersLast" value="<?= $MembersLast;?>" class="text" maxlength="100" required="required" />
                <em>Please provide your lastname.</em>
            </label>
            <div class="clearfloat"></div>
        </li>

        <!-- Business -->
        <li class="double">
            <label>
                Enter Company's Name <br />
                <input type="text" name="MembersBusiness" id="MembersBusiness" value="<?= $MembersBusiness;?>" class="text" maxlength="255" />
            </label>
            <label>
                Select Residing Country <span>*</span>
                <select name="MembersCountry" id="MembersCountry" required="required">
				<?= getDataOptions(urlPathCombine($_SERVER['ROOT_URI'], "/data/north-america.xml"), 'US'); ?>
                </select>
            </label>
			<div class="clearfloat"></div>
        </li>

        <!-- address -->
        <li class="required double">
            <label>
                Enter Street Address <span>*</span> <br />
                <input type="text" name="MembersAddress" id="MembersAddress" value="<?= $MembersAddress;?>" class="text" maxlength="255" required="required" />
                <em><strong>Example</strong>: 69 SW Random St / PO Box 96</em>
            </label>
            <label>
                PO Box/Apt/Suite <br />
                <input type="text" name="MembersAddress2" id="MembersAddress2" value="<?= $MembersAddress2;?>" class="text" maxlength="100" /><br />
                <em>If there's no street address, enter PO Box in the street address field.</em>
            </label>
        </li>

        <!-- City -->
        <li class="required triple">
            <label>
                Enter City / Town <span>*</span> <br />
                <input type="text" name="MembersCity" id="MembersCity" value="<?= $MembersCity;?>" class="text" maxlength="100" required="required" />
            </label>
            <label>
                Enter State / Providence <span>*</span>
                <input type="text" name="MembersState" id="MembersState" value="<?= $MembersState;?>" class="text" maxlength="32" required="required" />
            </label>

            <label>
                Enter Zip / Postal Code <span>*</span> <br />
                <input type="text" name="MembersZip" id="MembersZip" value="<?= $MembersZip;?>" class="text" maxlength="10" required="required" />
            </label>
            <div class="clearfloat"></div>
        </li>

        <!-- Phone -->
        <li class="double">
            <label>
                Phone Number <br />
                <input type="text" name="MembersPhone" id="MembersPhone" value="<?= $MembersPhone;?>" class="text" maxlength="32" />
                <em><strong>Example</strong>: 999-999-9999 / (999) 999-9999</em>
            </label>

            <label>
                Extension <br />
                <input type="text" name="MembersExtension" id="MembersExtension" value="<?= $MembersExtension;?>" class="text" maxlength="10" />
                <em>Phone extension number</em>
            </label>
            <div class="clearfloat"></div>
        </li>
		
		<!-- Fax -->
        <li class="double">
            <label>
                Fax <br />
                <input type="text" name="MembersFax" id="MembersFax" value="<?= $MembersFax;?>" class="text" maxlength="32" />
                <em><strong>Example</strong>: 999-999-9999 / (999) 999-9999</em>
            </label>

            <label>
                Website <br />
                <input type="text" name="MembersWebsite" id="MembersWebsite" value="<?= $MembersWebsite;?>" class="text" maxlength="255" />
                <em>Company or Personal Website</em>
            </label>
            <div class="clearfloat"></div>
        </li>
        <li class="clearfix">
 			<button type="button" id="btnBack" class="prev" style="float:left" onClick="location.href='/members/address';">&laquo; Back</button>
			<button type="submit" class="saveBtn" style="float:right">Save</button>
        </li>

        <div class="clearfloat"></div>
    </ul>

</div><!--wizard-->
</form>