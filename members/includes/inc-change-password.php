<?php 
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");
require_once $_SERVER['DOCUMENT_ROOT'] . '/library/classes/class_form.php'; 
?>
<script src="/members/js/account.js"></script>

<div class="dashboard">
	<div class="container">
		<div class="provider-number-card module">
			<div class="card-header"><h2>Change Password</h2></div>

<?php    

	if ($form->getNumErrors() > 0) {
		$hidId			 = $form->value("hidId");
		$MembersPassOrig = '';
		$MembersPass	 = '';
		$MembersPass2	 = '';
	} else {
		$hidId			 = (int) $_SESSION['member_id']; 
		$MembersPassOrig = '';
		$MembersPass	 = '';
		$MembersPass2	 = '';
	}

    if(isset($_GET['expired'])) {
        print '<div class="message error">** Your password has expired and must be changed. </div>'; 
    }
	if ($form->getNumErrors() > 0) {
		$errors	= $form->getErrorArray();
		foreach ($errors as $err) echo $err;
	} else if (isset($_SESSION['processed'])) {
		echo "<div class=\"message success\">Password Updated Successfully!</div>\n";
		unset($_SESSION['processed']);
	}

?>
		
			<div id="resultTarget"></div>

			<!-- the form -->
			<form id="entryForm" method="post" action="/members/process.php">
			<input type="hidden" name="actionRun" id="actionRun" value="password">
			<input type="hidden" name="hidId" value="<?= $hidId; ?>">
			<div id="myaccount" class="change-pw">

				<ul class="nobullets">
					<!-- old password -->
					<li>
						<label class="required" for="MembersPassOrig">Old Password</label>
						<input type="password" name="MembersPassOrig" id="MembersPassOrig" value="<?= $MembersPassOrig;?>" class="text" maxlength="50" required="required" />
					</li>

					<!-- new password -->
					<li>
						<br>
						<em class="hint">To ensure the highest level of security please be sure your password contains at least 8 characters, at least one capital letter, at least one lowercase letter, at least one special character (! @ # $ % ^ & % * ? _ ~), and at least one number. </em>
						<br>
						<label class="required" for="MembersPass">Choose a New Password</label>
						<input type="password" name="MembersPass" id="MembersPass" value="<?= $MembersPass;?>" class="text" maxlength="50" required="required" />
					</li>
					<li>
						<label class="required" for="MembersPass2">Verify Password</label>
						<input type="password" name="MembersPass2" id="MembersPass2" value="<?= $MembersPass2;?>" class="text" data-equals="MembersPass" maxlength="50" required="required" />
					</li>

					<li class="clearfix">
						<button type="submit" class="saveBtn button green">Save</button>
					</li>
				</ul>
			</div>
			</form>
		</div>
	</div>
</div>