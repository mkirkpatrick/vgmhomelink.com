<div class="account-details-card data-tabs module">
	<div class="card-header"><h2>My Account Information</h2></div>

	<ul class="order-details-list nobullets">
		<li class="clearfix">
			<strong>User Login</strong>
			<span><?= $_SESSION['member_username'] ?></span>
		</li>

		<li class="clearfix">
			<strong>Title</strong>
			<span><?= $_SESSION ['member_title'] ?></span>
		</li>

		<li class="clearfix">
			<strong>Full Name</strong>
			<span><?= $_SESSION['member_name'] ?></span>
		</li>

		<li class="clearfix">
			<strong>Email Address</strong>
			<span><?= $_SESSION['member_username'] ?></span>
		</li>

		<li class="clearfix">
			<strong>Phone</strong>
			<span><?= $_SESSION['member_phone'] ?></span>
		</li>

		<li class="clearfix">
			<strong>Company Name</strong>
			<span><?= $_SESSION['member_company'] ?></span>
		</li>
	</ul>
</div>