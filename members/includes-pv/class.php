<?php 

require_once($_SERVER['DOCUMENT_ROOT'] . '/library/classes/class_vMail.php');

class Process extends Database { 

    private $record;
    private $table;
    public $params;

    //------------------------------
    public function __construct($table) {
    //------------------------------
        global $_WEBCONFIG;
        parent::__construct();
        $this->table = $table;
    }

    //------------------------------
    public function _addProviderAppeal() {
    //------------------------------
        global $_WEBCONFIG, $siteConfig, $form;

        $this->record = new Entity($this->table);
        
        $fieldName    = "Provider_Name";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        $this->record->ap_provider_no = $_SESSION['member_prov_num']; 
        $this->record->ap_referral_no = (int) $_POST['hidReferralNo']; 
        $this->record->ap_pv_name = $fieldValue;
       
        $fieldName    = "Phone";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        $this->record->ap_phone = $fieldValue;
         
        $fieldName    = "Name";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        $this->record->ap_name = $fieldValue;
            
        $fieldName    = "Email";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        $this->record->ap_email = $fieldValue;
            
        $fieldName    = "Contact_Name";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        $this->record->ap_ct_name = $fieldValue;
            
        $fieldName    = "Contact_Email";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        $this->record->ap_ct_email = $fieldValue;
            
        $fieldName    = "Contact_Phone";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        $this->record->ap_ct_phone = $fieldValue;
            
        $fieldName    = "Contact_Fax";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        $this->record->ap_ct_fax = $fieldValue;
        
        // this is an array of checkboxes
        $fieldName    = "Reason_for_Appeal";
        $fieldValue    = isset($_POST[$fieldName]) ? strip_tags(implode('|', $_POST[$fieldName])) : '';
        $this->record->ap_reason = $fieldValue;
            
        $fieldName    = "Explanation_of_Appeal";
        $fieldValue    = strip_tags($_POST[$fieldName]);
        $this->record->ap_explanation = $fieldValue;
        
        // No Errors
        $this->record->ap_last_modified = date("Y-m-d G:i:s");
        $this->record->ap_date_added = date("Y-m-d G:i:s");
        Repository::Save($this->record); 
        
        $iNum = Database::getInsertID(); 

        # Set Up Form Mailer
        $mailer = new vMail();
        $mailer->addRecipient('HomelinkCallCenter@vgm.com');

        $mailer->setSubject("Website Appeals Form Submitted");
        $mailer->setMailType("html");
        $mailer->setFrom($siteConfig['Default_From_Email'], "VGM Homelink Website");
        $mailer->setReplyTo($siteConfig['Default_Reply_To_Email'], "VGM Homelink Website");

        $htmlFile = filePathCombine($_SERVER['DOCUMENT_ROOT'], "/emailtemplates/appeals-form-template.html");
        if(file_exists($htmlFile)) {
            $fh = fopen($htmlFile, 'r');
            $body = fread($fh, filesize($htmlFile));
            fclose($fh);
        }
        else {
            throw new exception("HTML Email Template File Not Found");
        }

        $mergeFields = array();
        $mergeFields["**GUEST**"]        = $siteConfig['Company_Name'];
        $mergeFields["**COMPANYNAME**"]  = $siteConfig['Company_Name'];
        $mergeFields["**SUBMISSIONID**"] = $iNum;
        $mergeFields["**WEBSITE**"]      = $_WEBCONFIG['SITE_URL'];
        $mergeFields["**DISPLAY_URL**"]  = $_WEBCONFIG['SITE_DISPLAY_URL'];
        $mergeFields["**YEAR**"]         = date("Y", time());

        $body = strtr($body, $mergeFields);

        $mailer->setMessage($body);
        $mailer->sendMail();
        $_SESSION['processed'] = 'added';
        redirect($_SERVER['HTTP_REFERER']);
    }

};
?>