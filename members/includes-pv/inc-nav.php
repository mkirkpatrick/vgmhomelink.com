<?php 
use Forbin\Library\Classes\Member;

addHtmlTag('/members/css/dashboard.css', 'css');

Member::_secureCheck();
if(isNullOrEmpty($_SESSION['member_prov_num']) && $_SERVER['SCRIPT_NAME'] != "/members/admin.php" && isset($_SESSION['member_admin']) && $_SESSION['member_admin']) { 
    redirect('/members/admin.php'); 
}

if(isset($_GET['logout'])) { 
    Member::_secureLock(); 
}

if($_SESSION['member_require_password'] && $_SERVER['PHP_SELF'] != '/members/change-password.php') { 
    redirect("/members/change-password.php?expired"); 
}

include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-mobilenav.php'); ?>

<header class="db-topmenu">
	<nav class="navbar">
		<div class="container clearfix">
  			<a href="/" class="db-logo floatLeft" title="VGM Homelink"><img src="/images/dash-logo.png" alt="VGM Homelink"></a>
			<div class="floatLeft" style="margin-left: 25px"><strong><?= $_SESSION['member_prov_num'] ?></strong> <?= HTML_SPACE ?>
            <?php if(isset($_SESSION['member_admin']) && $_SESSION['member_admin']) { ?>
            (<a href="/members/admin.php">Switch Provider Number</a>)
            <?php } else if(isset($_SESSION['member_multi'])) { ?>
            (<a href="/members/multi.php">Switch Branch Number</a>)
            <?php } ?>
            </div>
			<ul class="nobullets navbar-top-links floatRight">
                <li class="member-welcome large-only">Welcome <?= $_SESSION['member_name'] ?></li>
                 <?php if(!(isset($_SESSION['member_admin']) && $_SESSION['member_admin'])) { ?>
                <li class="large-only"><a href="/members/change-password.php">Change Password</a></li>
                 <?php } ?>
				<li class="large-only"><a href="/members/index.php?logout">Logout</a></li>
				<li class="mobileonly"><a href="#" class="openmobile"><span>Menu</span></a></li>
			</ul>
                          
    	</div>
	</nav>
</header>

<nav class="db-sidemenu large-only">
    <ul class="navigation nobullets">
		<li><a href="/members/provider-dashboard.php">Dashboard</a></li>
		<li><a href="http://www.hmeforms.com" target="_blank">Forms</a></li>
        <!--<li><a href="/get-quote">Get a Quote</a></li>-->
        <li><a href="https://hmeforms.com/case-managers-quick-referral" target="_blank">Submit Referral</a></li>
        <!--<li><a href="/locator">Provider Locator</a></li>-->
        <li><a href="https://hmeforms.com/patients-satisfaction-survey" target="_blank">Satisfaction Survey</a></li>
        <li><a href="https://hmeforms.com/patients-confirmation-de-encuesta" target="_blank">Confirmation De Encuesta</a></li>
		<!--<li><a href="/quick-order">Quick Order</a></li>-->
    </ul><!-- eof navigation -->
</nav>