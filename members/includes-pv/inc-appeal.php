<?php 
use Forbin\Members\Library\ViperAPI;	
$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;  
if($id == 0) { 
    throw new Exception("No Patient ID Provided"); 
}
$order = ViperAPI::getOrder($id);
$orderInfo = ViperAPI::getPaymentDetailsByOrderNo($_SESSION['member_prov_num'], $id);
?>
<div class="grid-row">
    <div class="twelvecolM sixcol--large fourcol--xl">
        <div class="order-summary-card module">
            <div class="card-header"><h2>Order Summary</h2></div> 
            <p>Below are the referral details from Referral #<?= $id ?>. You can re-order the same referral by using the Button below the details.</p>

            <ul class="order-details-list nobullets grid-row grid-row--nopad">
                <li class="twelvecolM">
                    <strong>Name</strong>
                    <span><?= ucwords($orderInfo->PatientName) ?></span>
                </li>

                <li class="twelvecolM">
                    <strong>Referral #</strong>
                    <span><?= $id ?></span>
                </li>

                <li class="twelvecolM">
                    <strong>Homelink Contact</strong>
						<span class="relative">
							<a href="#"><?= $order->PCCName ?></a>
							
							<div class="contact-tooltip">
								<ul class="nobullets">
									<li><strong>Email:</strong> <?= $order->PCCEmail ?></li>
									<li><strong>Phone:</strong> <?= $order->PCC800Number ?></li>
									<li><strong>Fax:</strong> <?= $order->PCCFax ?></li>
								</ul>
							</div>
						</span>
                </li>

                <li class="clearfix">
						<strong>Referral Received Date</strong>
						<span><?= !isNullOrEmpty($order->OrderDate->__toString()) ? date('m/d/Y', strtotime($order->OrderDate)) : '' ?></span>
					</li>

					<li class="clearfix">
						<strong>Referral Billed Date</strong>
						<span><?= !isNullOrEmpty($order->BillSent->__toString()) ? date('m/d/Y', strtotime($order->BillSent)) : '' ?></span>
					</li>

					<li class="clearfix">
						<strong>Claims Professional</strong>
						<span><?= $order->CaseMgrNo ?></span>
					</li>

					<li class="clearfix">
						<strong>Adjuster</strong>
						<span><?= $order->AdjusterNo ?></span>
					</li>

					<li class="clearfix">
						<strong>Authorization #</strong>
                        <span><?= $order->AuthorizationNo ?></span>
					</li>

					<li class="clearfix">
						<strong>Claim #</strong>
                        <span><?= $order->ClaimNo ?></span>
					</li>
                    
					<li class="clearfix">
						<strong>Evaluation Scheduled Date</strong>
                        <span><?= !isNullOrEmpty($order->EvalDate->__toString()) ? date('m/d/Y', strtotime($order->EvalDate)) : '' ?></span>
					</li>

					<li class="clearfix">
						<strong>Expected Delivery Date</strong>
						<span><?= !isNullOrEmpty($order->ExpectedDeliveryDate->__toString()) ? date('m/d/Y', strtotime($order->ExpectedDeliveryDate)) : '' ?></span>
					</li>

					<li class="clearfix">
						<strong>Delivery Date</strong>
						<span><?= !isNullOrEmpty($order->DeliveryDate->__toString()) ? date('m/d/Y', strtotime($order->DeliveryDate)) : '' ?></span>
					</li>
            </ul>

            <div class="supporting-docs-card">
					<h3>Submit Supporting Documentation</h3>
					<p>Please upload any supporting documents to assist in processing.<br><em>Examples: Bills, Nurse Notes, Therapy Notes, etc.</em></p>
					<div class="notification success" id="SuccessDiv" style="display: none"><h1>Submission Complete</h1><p></p><p>All supporting documentation has been uploaded successfully. </p><p></p></div>
					<div class="uploader-card module alignCenter">
						<img src="/members/images/icon-doc.png" alt="Document Uploader" title="Click Here to upload supporting document(s)." />
						<label for="supporting-doc">Click here to upload supporting document(s). </label>
						<div>
							<div class="demo-section k-content">
								<input name="files" id="files" type="file" />
                                <a href="#" style="margin-top: 10px" class="button silver floatRight" id="submitFilesGateway">Submit Files</a>
							</div>
						</div>

						<script type="text/javascript">
							$(document).ready(function() {
								$("#files").kendoUpload({
									async: {
										saveUrl: "process.php?actionRun=saveSupportingDocuments&pno=<?= $_SESSION['member_prov_num'] ?>&pdid=<?= $id ?>",
										removeUrl: "process.php?actionRun=deleteSupportingDocuments&pno=<?= $_SESSION['member_prov_num'] ?>&pdid=<?= $id ?>",
										autoUpload: true
									}, 
									error: function (e) {
										alert(e.XMLHttpRequest.responseText); 
									}
								});
                                
                                $('#submitFilesGateway').on("click", function() { 
                                    $('.uploader-card').hide(); 
                                    $('#SuccessDiv').show(); 
                                }); 
							});
						</script>
					</div>
				</div>
        </div>
    </div><!-- eof sixcol -->

    <div class="twelvecolM sixcol--large eightcol--xl">
        <div id="appeal-request" class="appeal-card module">
        <?php if(isset($_SESSION['processed'])) { ?>
            <div class="userMessage"><div class="notification success" id="SuccessDiv"><h1>Thank You!</h1><p></p><p>Your request has been submitted successfully. A representative will be contacting you soon.</p><p></p></div><br></div>
       <?php } ?>
            <div class="card-header"><h2>Appeal Request</h2></div>
            <div class="module-body">
                <form id="entryForm" method="post" action="/members/includes-pv/process.php">
                    <input type="hidden" name="actionRun" value="addProviderAppeal" />
                    <input type="hidden" name="hidReferralNo" value="<?= $id ?>" />

                    <h3 class="form-header">Provider Information</h3>
                    <ul class="order-details-list appeal-form nobullets grid-row grid-row--nopad">
                        <li class="twelvecolM sixcol--medium threecol--xl">
                            <label for="provider-name">Provider Name</label>
                            <input type="text" id="provider-name" name="Provider Name" value="<?= $orderInfo->Company; ?>" />
                        </li>
                        <li class="twelvecolM sixcol--medium threecol--xl">
                            <label for="phone" class="required">Phone Number</label>
                            <input type="tel" id="phone" name="Phone" required data-parsley-required-message="Please enter a phone number." />
                        </li>
                        <li class="twelvecolM sixcol--medium threecol--xl">
                            <label for="your-name" class="required">Your Name</label>
                            <input type="text" id="your-name" name="Name" required data-parsley-required-message="Please enter your name." />
                        </li>
                        <li class="twelvecolM sixcol--medium threecol--xl">
                            <label for="your-email" class="required">Your Email</label>
                            <input type="email" id="your-email" name="Email" required data-parsley-required-message="Please enter your email." />
                        </li>
                    </ul>

                    <h3 class="form-header">Contact Information (<a href="javascript:sameAsAbove();" class="same-as-above">same as above</a>)</h3>
                    <ul class="order-details-list appeal-form nobullets grid-row grid-row--nopad">
                        <li class="twelvecolM sixcol--medium threecol--xl">
                            <label for="contact-name" class="required">Contact Name</label>
                            <input type="text" id="contact-name" name="Contact Name" required data-parsley-required-message="Please enter contact name." />
                        </li>
                        <li class="twelvecolM sixcol--medium threecol--xl">
                            <label for="contact-email" class="required">Contact Email</label>
                            <input type="email" id="contact-email" name="Contact Email" required data-parsley-required-message="Please enter contact email." />
                        </li>
                        <li class="twelvecolM sixcol--medium threecol--xl">
                            <label for="contact-phone" class="required">Contact Phone</label>
                            <input type="tel" id="contact-phone" name="Contact Phone" required data-parsley-required-message="Please enter contact phone." />
                        </li>
                        <li class="twelvecolM sixcol--medium threecol--xl">
                            <label for="contact-fax" class="required">Contact Fax</label>
                            <input type="tel" id="contact-fax" name="Contact Fax" required data-parsley-required-message="Please enter contact fax." />
                        </li>
                    </ul>


                    <h3 class="form-header">Appeal Information</h3>
                    <h4 class="required">Reason for Appeal</h4>
                    <ul class="order-details-list appeal-form checkbox-list nobullets grid-row grid-row--nopad">
                        <li class="twelvecolM sixcol--xl">
                            <input type="checkbox" id="billing-error" name="Reason for Appeal[]" value="Charges Billed in Error" />
                            <label for="billing-error" class="inline">Charges Billed in Error</label>
                        </li>
                        <li class="twelvecolM sixcol--xl">
                            <input type="checkbox" id="item-returned" name="Reason for Appeal[]" value="Item Returned" />
                            <label for="item-returned" class="inline">Item Returned (Include Date Returned in Explanation of Appeal)</label>
                        </li>
                        <li class="twelvecolM sixcol--xl">
                            <input type="checkbox" id="coord-benefits" name="Reason for Appeal[]" value="Coordination of Benefits" />
                            <label for="coord-benefits" class="inline">Coordination of Benefits</label>
                        </li>
                        <li class="twelvecolM sixcol--xl">
                            <input type="checkbox" id="timely-filing" name="Reason for Appeal[]" value="Timely Filing" />
                            <label for="timely-filing" class="inline">Timely Filing</label>
                        </li>
                        <li class="twelvecolM sixcol--xl">
                            <input type="checkbox" id="coding" name="Reason for Appeal[]" value="Corrected Coding" />
                            <label for="coding" class="inline">Corrected Coding</label>
                        </li>
                        <li class="twelvecolM sixcol--xl">
                            <input type="checkbox" id="other" name="Reason for Appeal[]" value="Other" />
                            <label for="other" class="inline">Other (Explain in Explanation of Appeal)</label>
                        </li>
                        <li class="twelvecolM sixcol--xl">
                            <input type="checkbox" id="dup-payment" name="Reason for Appeal[]" value="Duplicate Payment" />
                            <label for="dup-payment" class="inline">Duplicate Payment</label>
                        </li>
                        <li class="twelvecolM">
                            <br>
                            <label for="explanation" class="required">Explanation of Appeal</label>
                            <textarea id="explanation" name="Explanation of Appeal" required data-parsley-required-message="Provide an explanation of appeal."></textarea>
                        </li>
                    </ul>

                    <input type="submit" value="Submit" class="button green">
                </form>
            </div>
        </div>
    </div><!-- eof sixcol -->
</div>

<?php
addHtmlTag("/css/parsley.css","css");
addHtmlTag(array("/scripts/parsley.min.js","/modules/forms/js/form.js"),"javascript");
?>

<script type="text/javascript">
    function sameAsAbove() {
        $("#contact-name").val($("#your-name").val());
        $("#contact-email").val($("#your-email").val());
        $("#contact-phone").val($("#phone").val());
    }
</script>