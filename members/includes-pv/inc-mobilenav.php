<nav class="mobilemenu mobilemenu-dashboard">
	<ul class="mobile-nav nobullets">
     <li><h3 style="color: white; padding-left: 10px">Welcome <?= $_SESSION['member_name'] ?></h3></li>
                 <?php if(!(isset($_SESSION['member_admin']) && $_SESSION['member_admin'])) { ?>
                <li><a href="/members/change-password.php">Change Password</a></li>
                 <?php } ?>

		<li><a href="/members/provider-dashboard.php">Dashboard</a></li>
		<li><a href="http://www.hmeforms.com" target="_blank">Forms</a></li>
		<li><a href="https://hmeforms.com/case-managers-quick-referral" target="_blank">Submit Referral</a></li>
		<li><a href="https://hmeforms.com/patients-satisfaction-survey" target="_blank">Satisfcation Survey</a></li>
		<li><a href="https://hmeforms.com/patients-confirmation-de-encuesta" target="_blank">Confirmation De Encuesta</a></li>
        <li><a href="/members/index.php?logout">Logout</a></li>
	</ul>
</nav>