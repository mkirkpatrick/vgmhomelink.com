<?php
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
$providerDetails = null; 
$cacheKey = $_SESSION['member_prov_num'] . '_PROVIDER_INFO'; 
if(!$providerDetails = Cache::get($cacheKey)) {
	$providerDetails = ViperAPI::getProviderDetailsByVGMNo($_SESSION['member_prov_num']);
	if($providerDetails == null && isset($_SESSION['member_admin']) && $_SESSION['member_admin']) {
		redirect("/members/admin.php?message=1"); 
	}
	Cache::set($cacheKey, $providerDetails, 3600);
}
?>

<div class="provider-info-card module">
	<h4>Provider Info: <?= $_SESSION['member_prov_num'] ?></h4>

	<div class="module-body">
        <ul class="nobullets">
            <?php $_SESSION['member_company_name'] = $providerDetails->ProviderName; ?>
                    <li><strong>Provider Name:</strong><br><?= $providerDetails->ProviderName ?></li>
                    <li><strong>Address:</strong><br />
						<?= $providerDetails->ProviderAddress ?><br />
						<?= $providerDetails->ProviderCity ?>, <?= $providerDetails->ProviderState ?><?= $providerDetails->ProviderZip ?>
                    </li>
					<li><strong>Billing Email:</strong> <?= $providerDetails->BillingEmail ?></li>
					<li><strong>Referral Email:</strong> <br /><?= $providerDetails->ReferralEmail ?></li>
                    <li><strong>Phone:</strong> <br /><?= $providerDetails->ProviderPhone1 ?></li>
                   <?php 
            ?>
        </ul>
	</div>

	<h4>Credentialing Info</h4>
	
	<div class="module-body">
		<ul class="nobullets">
			<li><strong>Credentials Received On:</strong><br /><?= !isNullOrEmpty($providerDetails->ProviderCredentials) ? date('m/d/Y', strtotime($providerDetails->ProviderCredentials)) : '' ?></li>
			<li><strong>Credentials Expire On:</strong><br /><?= !isNullOrEmpty($providerDetails->ProviderCredentialsExpire) ? date('m/d/Y', strtotime($providerDetails->ProviderCredentialsExpire)) : '' ?></li>
		</ul>
	</div>

	<!-- <h4>Account Info</h4>
	<div class="module-body">
		<ul class="nobullets">
			<li><strong>Username:</strong><br />VGM Admin</li>
			<li><strong>Last Login Date: </strong><br />08/01/2019</li>
		</ul>
	</div> -->
</div>