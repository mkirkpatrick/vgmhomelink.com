<div id="recent-grid" class="none" style="display: none"></div>
<script type="text/javascript">
$(document).ready(function () {
    var hidden = false;
	$("#recent-grid").kendoGrid({
		dataSource: {
            change: function(e) {
                $('#recent-grid .k-grid-content').fadeIn();
                $('#recent-grid .k-grid-pager').fadeIn();
            },
			transport: {
				read:  {
					url: "/members/datasource/referral_history.php?v=4",
					dataType: "json"
				}
			},
			pageSize: 10,
			batch: true,
			schema: {
				model: {
					id: "ReferralNumber",
					fields: {
						ReferralNumber: { type: "string" }
					}
				}
			},
            error: function(e) {
                console.log(e.errors);
            }
		},
		height: 600,
		scrollable: true,
		navigatable: true,
		sortable: true,
		groupable: false,
        noRecords: true,
		messages: {
			noRecords: "No Records Found Matching Your Search"
		},
		filterable: {
			mode: 'row',
			extra: false,
			operators: { // redefine the string operators
                    string: {
                        contains: "Contains"
                    }
                }
		},
		pageable: {
			refresh: true,
			pageSizes: true,
			pageSizes: [10, 25, 50, 100]
		},
		columns: [
			{
				title: 'Referral #',
				field: 'ReferralNumber',
				template: '<a href="/members/provider-patient-details.php?id=#=ReferralNumber#&status=#=Status#">#=ReferralNumber#</a>',
				width: 130,
				filterable: {
					cell: {
						showOperators: false,
					}
				}
			},
			{
				title: 'Name',
				field: 'PatientName',
				width: 250,
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'DOB',
				field: 'SSNNumber',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Service Date',
				field: 'ServiceDate',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Status',
				field: 'Status',
				filterable: {
					cell: {
						showOperators: false,
					}
				}
			},
			{
				title: 'Invoice #',
				field: 'InvoiceNumber',
				filterable: {
					cell: {
						showOperators: false,
					}
				}
			}
		],
        dataBound: function(e){
            $("#recent-grid-loader").hide();
            $("#recent-grid").fadeIn();
            if(!hidden) {
                $('#recent-grid .k-grid-content').hide();
                $('#recent-grid .k-grid-pager').hide();
                hidden = true;
            }
        }
	});
});
</script>