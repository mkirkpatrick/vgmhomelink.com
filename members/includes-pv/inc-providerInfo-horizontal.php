<?php
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

require_once $_SERVER['DOCUMENT_ROOT'] . '/library/config.php';
$providerDetails = null; 
$cacheKey = $_SESSION['member_prov_num'] . '_PROVIDER_INFO'; 
if(!$providerDetails = Cache::get($cacheKey)) {
	$providerDetails = ViperAPI::getProviderDetailsByVGMNo($_SESSION['member_prov_num']);
	if($providerDetails == null && isset($_SESSION['member_admin']) && $_SESSION['member_admin']) {
		redirect("/members/admin.php?message=1"); 
	}
	Cache::set($cacheKey, $providerDetails, 3600);
}
$_SESSION['member_company_name'] = $providerDetails->ProviderName;

function getDataCacheDate () {
    $provNum = $_SESSION['member_prov_num']; 
    $sql = "SELECT `cs_last_modified` FROM `tbl_cron_status` WHERE cs_provider_no = '$provNum' LIMIT 1"; 
    $record = Database::Execute($sql); 
    if($record->Count() > 0) {
        $record->MoveNext(); 
        $time = strtotime($record->cs_last_modified);
        return date('m/d/Y', $time);
    } else {
        return date('m/d/Y'); 
    }
}
?>

<div class="provider-info-card module">
	<div class="grid-row">
		<div class="twelvecolM sevencol--medium">
			<h4>Provider Info: <?= $_SESSION['member_prov_num'] ?></h4>

			<div class="module-body">       
				<ul class="grid-row nobullets">
							<li class="twelvecolM sixcol--medium fourcol--xl"><strong>Provider Name:</strong><br><?= $providerDetails->ProviderName ?></li>
							<li class="twelvecolM sixcol--medium fourcol--xl"><strong>Address:</strong><br />
								<?= $providerDetails->ProviderAddress ?><br />
								<?= $providerDetails->ProviderCity ?>, <?= $providerDetails->ProviderState ?><?= $providerDetails->ProviderZip ?>
							</li>
							<li class="twelvecolM fourcol--xl"><strong>Billing Email:</strong><br> <?= $providerDetails->BillingEmail ?></li>
							
							<li class="clearfloat"></li>
							
							<li class="twelvecolM sixcol--medium fourcol--xl"><strong>Referral Email:</strong> <br /><?= $providerDetails->ReferralEmail ?></li>
							<li class="twelvecolM sixcol--medium fourcol--xl"><strong>Phone:</strong> <br /><?= $providerDetails->ProviderPhone1 ?></li>
						   <?php 
					?>
				</ul>
			</div>
		</div>
		
		<div class="twelvecolM threecol--medium">
			<h4>Credentialing Info</h4>
	
			<div class="module-body">
				<ul class="nobullets">
					<li><strong>Credentials Received On:</strong><br /><?= !isNullOrEmpty($providerDetails->ProviderCredentials) ? date('m/d/Y', strtotime($providerDetails->ProviderCredentials)) : '' ?></li>
					<li><strong>Credentials Expire On:</strong><br /><?= !isNullOrEmpty($providerDetails->ProviderCredentialsExpire) ? date('m/d/Y', strtotime($providerDetails->ProviderCredentialsExpire)) : '' ?></li>
				</ul>
			</div>
		</div>
        
        <div class="twelvecolM twocol--medium">
			<h4>Data Last Updated </h4>
	
			<div class="module-body">
				<ul class="nobullets">
					<li>
                        <div align="center">
                            <div id="cacheDate"><?= getDataCacheDate() ?></div>
                            <img src="/VPanel/images/animates/loader.gif" alt="Loading" style="display: none" id="CacheLoader" />
                            <a id="RefreshData" class="button green" style="cursor: pointer">Refresh Data</a>
                        </div>
                    </li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script>
    $('#RefreshData').click(function() { 
        $('#CacheLoader').show();
        $.get("/members/cache-clear.php").done(function( data ) { 
            $('#CacheLoader').hide(); 
            var processGrid = $('#process-grid').data("kendoGrid"); 
            processGrid.dataSource.read(); 
            processGrid.refresh(); 
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd = '0'+dd
            } 

            if(mm<10) {
                mm = '0'+mm
            } 

            today = mm + '/' + dd + '/' + yyyy;
            $('#cacheDate').html(today); 
        });
    }); 
</script>