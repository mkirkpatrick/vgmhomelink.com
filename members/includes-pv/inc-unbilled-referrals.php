
<div class="unbilled-card module">
	<div class="card-header">
		<h2>Referrals in Process</h2>
		<p>Please submit your bill electronically by clicking on the blue Referral #.</p>
	</div>

 	<div class="card-body relative">
 		<div id="unbilled-grid-loader" class="k-loading-mask" style="width:100%;height:100px"><span class="k-loading-text">Loading...</span><div class="k-loading-image"><div class="k-loading-color"></div></div></div>
		<div id="unbilled-grid" class="none"></div>
	</div>
</div>

<script>
$(document).ready(function () {
	$("#unbilled-grid").kendoGrid({
		dataSource: {
			transport: {
				read:  {
					url: "/members/datasource/unbilled-referrals.php",
					dataType: "json"
				}
			},
			pageSize: 10,
			batch: true,
			schema: {
				model: {
					id: "ReferralNumber"
				}
			}
		},
		scrollable: true,
		navigatable: true,
		sortable: true,
		resizable: false,
		groupable: true,
		filterable: {
			mode: 'row'
		},
		pageable: {
			refresh: true,
			pageSizes: true,
			pageSizes: [10, 25, 50, 100]
		},
		columns: [
			{
				title: 'Referral #',
				field: 'ReferralNumber',
                template: '<a href="/members/provider-patient-details.php?id=#=ReferralNumber#">#=ReferralNumber#</a>',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Name',
				field: 'PatientName',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Patient DOB',
				field: 'PatientDOB',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Service Date',
				field: 'ServiceDate',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Status',
				field: 'Status',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Invoice #',
				field: 'InvoiceNumber',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			}
		],
        dataBound: function(e){
			var grid = $('#unbilled-grid').data('kendoGrid');

            $("#unbilled-grid-loader").hide();
            $("#unbilled-grid").fadeIn();

			grid.tbody.find('tr.k-grouping-row').each(function () {
				grid.collapseGroup(this);
			});
        }
	});
});
</script>