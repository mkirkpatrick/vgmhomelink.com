<div class="provider-tabbed-content data-tabs module relative">
	<ul class="nobullets tabs">
		<li class="tab-link" data-tab="tab-1">Web Referral</li>
		<li class="tab-link current" data-tab="tab-2">Referrals in Progress</li>
        <li class="tab-link" data-tab="tab-3">Claim History</li>
		<li class="tab-link" data-tab="tab-4" id="payerSearchTab">Payer Search</li>
	</ul> 

	<div id="tab-1" class="tab-content relative">
		<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-web-referrals.php'); ?>
	</div>

	<div id="tab-2" class="tab-content relative current">
		<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-referral-in-progress.php'); ?>
	</div>
	
	<div id="tab-3" class="tab-content relative">
		<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-referral-history.php'); ?>
	</div>
    
    <div id="tab-4" class="tab-content relative">
		<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-payer-search.php'); ?>
	</div>
</div>


<script>
$(function() {
	$('#web-referrals').data('kendoGrid').resize(); 
	$('#recent-grid').data('kendoGrid').resize(); 
	$('#payer-grid').data('kendoGrid').resize(); 
});
</script>