<?php
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;
use Forbin\Handlers\ErrorHandler; 

if(isset($paymentInfo->PaymentSummaries->PaymentSummary)) { 
    if(is_array($paymentInfo->PaymentSummaries->PaymentSummary)) { 
        foreach($paymentInfo->PaymentSummaries->PaymentSummary as $payment) {
            displayOrder($payment); 
        }
    } else {
        displayOrder($paymentInfo->PaymentSummaries->PaymentSummary); 
    }
} ?>
<?php function displayOrder($payment) { 
	global $id, $_WEBCONFIG; ?>
<div class="payment-summary-card module">
<div class="card-header"><h2>Payment Summary </h2></div>
<div class="module-body">
	<ul class="order-details-list nobullets grid-row grid-row--nopad">
		<li class="twelvecolM sixcol--medium">
			<strong>Patient Responsibility</strong>
			<span>$<?= number_format((float)$payment->TotalPrAmt, 2); ?></span>
		</li>
		
		<li class="twelvecolM sixcol--medium">
			<strong>Payment Amount</strong>
			<span>$<?= number_format((float)$payment->TotalPaidAmt, 2);?></span>
		</li>
		
		<li class="twelvecolM sixcol--medium">
			<strong>Other Liability</strong>
			<span>$<?= number_format((float)$payment->TotalNoPrAdjustmentAmt, 2);?></span>
		</li>
		
		<li class="twelvecolM sixcol--medium">
			<strong>Payment Date</strong>
			<span><?= isset($payment->CheckDate) ? date("m/d/Y", strtotime($payment->CheckDate)) : 'N/A' ?></span>
		</li>
	</ul>
	
	<a href="/remithandler.ashx?token=<?= isset($_SESSION['member_secure_token']) ? $_SESSION['member_secure_token'] : '' ?>&username=<?= $_SESSION['member_username'] ?>&vgmno=<?= $payment->VgmNo?>&orderno=<?= $id ?>&batchno=<?= $payment->BatchId ?>&accessCode=<?= $_WEBCONFIG['PDF_SERVICE_ACCESS_CODE'] ?>" class="button silver">View Claim Remit</a>
	<!-- Removed Full Remit. See earlier Version for details --> 
	<p>&nbsp;</p>
	
	<table class="web-grid">
    <thead>
        <tr>
        	<th scope="col" data-field="Line">Line</th>
			<th scope="col" data-field="ServiceDate">Service Date</th>
			<th scope="col" data-field="CPTCode">CPT Code / Mod</th>
			<th scope="col" data-field="Qty">Qty</th>
			<th scope="col" data-field="BilledAmt">Billed Amt</th>
			<th scope="col" data-field="AllowedAmt">Allowed Amt</th>
			<th scope="col" data-field="GroupCode">Group / Reason Code</th>
			<th scope="col" data-field="AdjustedAmt">Adjusted Amt</th>
			<th scope="col" data-field="PaidAmt">Paid Amt</th>
			<th scope="col" data-field="RemarkCode">Remark Code</th>
		</tr>
	</thead>           
    <tbody>
	<?php 
		$groupedPayments = array(); 
		$lastLine = null; 
		$lastIndex = -1; 
		$paymentLineItems = array(); 
		if(is_array($payment->PaymentDetails->PaymentDetail)) {
			$paymentLineItems = $payment->PaymentDetails->PaymentDetail; 
		} else {
			$paymentLineItems[0] = $payment->PaymentDetails->PaymentDetail; 
		} 
		foreach ($paymentLineItems as $line) {  
			if($lastLine != trim($line->LineNo)) { 
				$lastIndex++; 
				$lastLine = trim($line->LineNo); 
				$line->AdjustedAmtFmt = '$' . number_format((float)$line->AdjustedAmt, 2);
				$line->CPT = $line->HCPCS . ' / ' . $line->Modifier; 
				$line->Group = $line->GroupCode  . ' / ' . $line->ReasonCd; 
				$groupedPayments[] = $line; 
			} else {
				$groupedPayments[$lastIndex]->Group = $groupedPayments[$lastIndex]->Group . '<br />' . $line->GroupCode  . ' / ' . $line->ReasonCd; 
				$groupedPayments[$lastIndex]->AdjustedAmtFmt = $groupedPayments[$lastIndex]->AdjustedAmtFmt . '<br />' . '$' . number_format((float)$line->AdjustedAmt, 2); 
				;
			}
		}
	
		foreach ($groupedPayments as $line) {  
		?>
		<tr>
			<td><?= $line->LineNo ?></td>
			<td><?= date("m/d/Y", strtotime($line->DatesOfService)) ?></td>
			<td><?= $line->CPT ?></td>
			<td><?= (int) $line->Quantity ?></td>
			<td>$<?= number_format((float)$line->DealerBilledAmt, 2);?></td>
			<td>$<?= number_format((float)$line->AllowedAmt, 2);?></td>
			<td><?= $line->Group; ?></td>			
			<td><?= $line->AdjustedAmtFmt?></td>
			<td>$<?= number_format((float)$line->PaidAmt, 2);?></td>
			<td><?= $line->RemarkCd ?></td>
		</tr>
	<?php } ?>
	</tbody>
</table>
</div>
</div>
<?php } ?>
<script type="text/javascript">
$(document).ready(function () {
	$(".web-grid").kendoGrid({
		dataSource: {
			pageSize: 20
		},
		scrollable: true,
		navigatable: true,
		groupable: false,
		sortable: true,
		pageable: false, 
		columns: [
			{
				title: 'Line',
				field: 'Line',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Service Date',
				field: 'ServiceDate',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'CPT Code',
				field: 'CPTCode',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Qty',
				field: 'Qty',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Billed Amount',
				field: 'BilledAmt',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Allowed Amount',
				field: 'AllowedAmt',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Group Code',
				field: 'GroupCode',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Adjusted Amount',
				field: 'AdjustedAmt',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Paid Amount',
				field: 'PaidAmt',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Remark Code',
				field: 'RemarkCode',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			}
			
		]
	});
});
</script>