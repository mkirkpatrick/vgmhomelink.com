<div id="process-grid"></div>
<script type="text/javascript">
$(document).ready(function () {
	$("#process-grid").kendoGrid({
		dataSource: {
			transport: {
				read:  {
					url: "/members/datasource/recent_referrals.php?v=4",
					dataType: "json"
				}
			},
			pageSize: 10,
			batch: true,
			schema: {
				model: {
					id: "ReferralNumber",
					fields: {
						ReferralNumber: { type: "string" }
					}
				}
			},
            error: function(e) {
                console.log(e);
            }
		},
		height: 600,
		scrollable: false,
		navigatable: true,
		sortable: true,
		groupable: false,
        noRecords: true,
		messages: {
			noRecords: "No Records Found"
		},
		filterable: {
			mode: 'row',
			extra: false,
			operators: { // redefine the string operators
                    string: {
                        contains: "Contains"
                    }
                }
		},
		pageable: {
			refresh: true,
			pageSizes: true,
			pageSizes: [10, 25, 50, 100]
		},
		columns: [
			{
				title: 'Referral #',
				field: 'ReferralNumber',
				template: '<a href="/members/provider-patient-details.php?id=#=ReferralNumber#&status=#=Status#">#=ReferralNumber#</a>',
				width: 130,
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Name',
				field: 'PatientName',
				width: 250,
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'DOB',
				field: 'SSNNumber',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Service Date',
				field: 'ServiceDate',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Status',
				field: 'Status',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Invoice #',
				field: 'InvoiceNumber',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			}
		],
        dataBound: function(e){
			var grid = $('#process-grid').data('kendoGrid');

			grid.tbody.find('tr.k-grouping-row').each(function () {
				grid.collapseGroup(this);
			});
        }
	});
});
</script>