
<div class="fixed-items module" id="divStatistics" style="width:100%;min-height:400px;">
<div class="card-header"><h2>Referral Information</h2></div>
<div id="fixed-items-loader" class="k-loading-mask" style="width:100%;height:100px;margin-top: 100px"><span class="k-loading-text">Loading...</span><div class="k-loading-image"><div class="k-loading-color"></div></div></div>
<div class="grid-row none" id="divStats"></div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$.ajax({
		type: "GET",
		url: "/members/datasource/statistics.php",
		error: function(xhr, statusText) { console.log("Error: "+statusText); },
		dataType :"html", 
		success: function(data) {
			$("#divStats").html(data);
			$("#fixed-items-loader").hide();    
			$("#divStats").fadeIn();
		}
	});
});
</script>