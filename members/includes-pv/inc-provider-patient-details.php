<?php 
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

$id = isset($_GET['id']) ? (int) $_GET['id'] : 0; 
if($id == 0) { 
    throw new Exception("No Patient ID Provided"); 
}

$paymentInfo = ViperAPI::getPaymentDetailsByOrderNo($_SESSION['member_prov_num'], $id);

//breakPoint($paymentInfo[0]); 

if(!isset($paymentInfo->PatientName)) {
    $paymentInfo = $paymentInfo[0]; 
}

// This will trigger redirect if invalid order number is entered or vgmno is invalid for this order number
if(is_null($paymentInfo)) {
   redirect('/members/provider-dashboard.php?redirect=invalid_order');
}
?>
<div class="order-summary-card module">
	<div class="card-header"><h1>Referral Summary</h1></div>
	<div class="module-body">
		<div class="grid-row">
			<div class="twelvecolM sixcol--medium fivecol--large fivcolcol--xl shift-right">
				<div class="supporting-docs-card">
					<h3>Submit Supporting Documentation</h3>
					<p>Please upload any supporting documents to assist in processing.<br><em>Examples: Bills, Nurse Notes, Therapy Notes, etc.</em></p>
					<div class="notification success" id="SuccessDiv" style="display: none"><h1>Submission Complete</h1><p></p><p>All supporting documentation has been uploaded successfully. </p><p></p></div>
					<div class="uploader-card module alignCenter">
						<img src="/members/images/icon-doc.png" alt="Document Uploader" title="Click Here to upload supporting document(s)." />
						<label for="supporting-doc">Click here to upload supporting document(s). </label>
						<div>
							<div class="demo-section k-content">
								<input name="files" id="files" type="file" />
                                <a href="#" style="margin-top: 10px" class="button silver floatRight" id="submitFilesGateway">Submit Files</a>
							</div>
						</div>

						<script type="text/javascript">
							$(document).ready(function() {
								$("#files").kendoUpload({
									async: {
										saveUrl: "process.php?actionRun=saveSupportingDocuments&pno=<?= $_SESSION['member_prov_num'] ?>&pdid=<?= $id ?>",
										removeUrl: "process.php?actionRun=deleteSupportingDocuments&pno=<?= $_SESSION['member_prov_num'] ?>&pdid=<?= $id ?>",
										autoUpload: true
									}, 
									error: function (e) {
										alert(e.XMLHttpRequest.responseText); 
									}
								});
                                
                                $('#submitFilesGateway').on("click", function() { 
                                    $('.uploader-card').hide(); 
                                    $('#SuccessDiv').show(); 
                                }); 
							});
						</script>
					</div>
				</div>
			</div>
			
			<div class="twelvecolM sixcol--medium sevencol--large sevencol--xl">
				<p>Below are the referral details from Referral <strong>#<?= $id ?></strong>.</p>

				<ul class="order-details-list nobullets">
					<li class="clearfix">
						<strong>Name</strong>
						<span><?= ucwords(strtolower($paymentInfo->PatientName)) ?></span>
					</li>

					<li class="clearfix">
						<strong>Provider Name</strong>
						<span><?= $paymentInfo->Company; ?></span>
					</li>

					<li class="clearfix">
						<strong>Date of Birth</strong>
						<span><?= !isNullOrEmpty($paymentInfo->PatientDob) ? date("m/d/Y", strtotime($paymentInfo->PatientDob)) : '' ?></span>
					</li>

					<li class="clearfix">
						<strong>Invoice Number</strong>
						<span><?= $paymentInfo->InvoiceNo ?></span>
					</li>

					<li class="clearfix">
						<strong>Gender</strong>
						<span><?= $paymentInfo->PatientGender == 'M' ? 'Male' : 'Female' ?></span>
					</li>

					<li class="clearfix">
						<strong>Claim Amount</strong>
						<span>$<?= number_format($paymentInfo->TotalDealerBilledAmt, 2) ?></span>
					</li>

					<li class="clearfix">
						<strong>Referral Number</strong>
						<span><?= $id ?></span>
					</li>

					<li class="clearfix">
						<strong>Claim Service Date</strong>
						<span><?= !isNullOrEmpty($paymentInfo->DatesOfService) ? date("m/d/Y", strtotime($paymentInfo->DatesOfService)) : '' ?></span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="claim-summary-card module">
	<div class="card-header"><h2>Claim Summary</h2></div>
	<div class="module-body">
		<ul class="order-details-list nobullets grid-row grid-row--nopad">
			<li class="twelvecolM sixcol--medium">
				<strong>Claim Status</strong>
				<span><?= isset($_GET['status']) && $_GET['status'] == "Complete" ? "Complete" : "In Progress" ?></span>
			</li>
			
			<li class="twelvecolM sixcol--medium">
				<strong>Claim Payment Amount</strong>
				<span><?= !isNullOrEmpty($paymentInfo->TotalClaimAmt) ?  '$' . number_format($paymentInfo->TotalClaimAmt, 2) : '' ?></span>
			</li>
			
			<li class="twelvecolM sixcol--medium">
				<strong>Date Submitted to Payer</strong>
				<span><?= !isNullOrEmpty($paymentInfo->BillSent) ? date("m/d/Y", strtotime($paymentInfo->BillSent)) : '' ?></span>
			</li>
			
			<li class="twelvecolM sixcol--medium">
				<strong>Claim Detail</strong>
				<span><?= $paymentInfo->Information ?></span>
			</li>
			
<?php if(isset($paymentInfo->PatientNotes->PatientNote)) { ?>
			<li class="twelvecolM twelvecol--medium">
				<h3>Additional Notes</h3>
                <?php 
                 if(is_array($paymentInfo->PatientNotes->PatientNote)) { 
                    foreach ($paymentInfo->PatientNotes->PatientNote as $note) {  
                        printNote($note);
                    }
                 } else {
                     printNote($paymentInfo->PatientNotes->PatientNote);
                 }
                ?>
			</li>
		</ul>
<?php } ?>
		<a href="/members/appeal-request.php?id=<?= $id ?>" class="button silver">Appeal</a>
	</div>
</div>
<?php
function printNote($note) { 
    print '<p><strong>' . ucwords($note->NoteTopic) . '</strong><br />' . nl2br($note->NoteText) . '</p>'; 
} 
include_once ($_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-payment-summary.php'); ?>
