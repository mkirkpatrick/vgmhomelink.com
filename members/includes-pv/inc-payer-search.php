<div id="payer-grid-loader" class="k-loading-mask" style="width:100%;height:100px"><span class="k-loading-text">Loading...</span><div class="k-loading-image"><div class="k-loading-color"></div></div></div>
<div id="payer-grid" class="none"></div>
<script type="text/x-kendo-template" id="toolBarTemplate">
    <div class="toolbar floatRight">
        <div>
            <input id="txtSearch" placeholder="Search Grid" type="text" style="width: 300px" />
            <a id="clearTextButton">Clear</a>
        </div>
    </div>
</script>
<script type="text/javascript">
$(document).ready(function () {
	$("#payer-grid").kendoGrid({
		dataSource: {
			transport: {
				read:  {
					url: "/members/datasource/payer-search.php?v=4",
					dataType: "json"
				}
			},
			pageSize: 10,
			batch: true,
			schema: {
				model: {
					id: "ProviderCompany"
				}
			}
		},
		height: 600,
		scrollable: true,
		navigatable: true,
		groupable: false,
		sortable: true,
		noRecords: true,
		messages: {
			noRecords: "No Companies Match Your Search"
		},
		pageable: {
			refresh: true,
			pageSizes: true,
			pageSizes: [10, 25, 50, 100]
		},
		columns: [
			{
				title: 'Company',
				field: 'ProviderCompany',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Phone',
				field: 'ProviderPhone',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			}
		],
		dataBound: function(e){
			var grid = $('#payer-grid').data('kendoGrid');

			$("#payer-grid-loader").hide();
			$("#payer-grid").fadeIn();

			grid.tbody.find('tr.k-grouping-row').each(function () {
				grid.collapseGroup(this);
			});
		},
		toolbar: kendo.template($("#toolBarTemplate").html())
	});

	$("#clearTextButton").kendoButton({icon: "filter-clear"});
    $("#txtSearch").on("keyup keypress onpaste", function () {
        var filter = { logic: "or", filters: [] };
        $searchValue = $(this).val();
        if ($searchValue.length > 1) {
            $.each($("#payer-grid").data("kendoGrid").columns, function( key, column ) {
                if(column.filterable) {
                    filter.filters.push({ field: column.field, operator:"contains", value:$searchValue});
                }
            });

            $("#payer-grid").data("kendoGrid").dataSource.query({ filter: filter });

            var count = $("#payer-grid").data("kendoGrid").dataSource.total();
            $(".k-pager-info").html("1 - " + count + " of " + count + " items");

        } else if($searchValue == "") {
            $("#payer-grid").data("kendoGrid").dataSource.query({
                page: 1,
            });
        }
    });

    $("#clearTextButton").on("click", function () {
        $("#payer-grid").data("kendoGrid").dataSource.query({
            page: 1,
        });
        $("#txtSearch").val('');
    });
});
</script>