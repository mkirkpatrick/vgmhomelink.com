<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php"); 

if (!isset($_GET['logout'])) {
    $_SESSION['ReturnURL'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}
require_once "class.php";

$p = new Process("tbl_appeals");
$action    = isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';
$returnUrl = isset($_GET['returnUrl']) ? $_GET['returnUrl'] : '';

switch ($action) {  
    case 'addProviderAppeal' :
        $p->_addProviderAppeal();
        break;
    
    default :
        throw new Exception("An unknown action executed!");
}

redirect("/members/provider-dashboard.php");
?>