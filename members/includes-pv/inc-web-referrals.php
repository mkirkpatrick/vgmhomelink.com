<?php 
use Forbin\Members\Library\ViperAPI;
$orders = ViperAPI::getDealerWebOrders($_SESSION['member_prov_num']); 
?>
<div id="referrals-grid-loader" class="k-loading-mask" style="width:80%;height:38%"><span class="k-loading-text">Loading...</span><div class="k-loading-image"><div class="k-loading-color"></div></div></div>
<table id="web-referrals" class="none" style="border-width: 0; height: 100%;">
    <thead>
        <tr>
        	<th scope="col" data-field="ReferralNumber">Referral #</th>
			<th scope="col" data-field="Location">Location</th>
			<th scope="col" data-field="Action">Action</th>
		</tr>
	</thead>           
    <tbody>
		<?php 
		$lastOrder = 0; 
		$dataListArray = array(); 
		foreach ($orders->GetDealerWebOrdersResponse as $order) { 
			$dataListArray[] = array(
			'CPT'=>(string) $order->HCPCS,
			'Description'=> (string)$order->Description,
			'RentalSale'=>((string)$order->RS) == 'S' ? 'Sale' : 'Rental',
			'Qty'=>(int)$order->Quantity,
			'ExpDelivery'=> date('m/d/Y', strtotime((string)$order->ExpectedDelivery)),
			'Location'=> $order->City . ', ' . $order->State);
			
			if($lastOrder != trim($order->OrderNo)) { 
			
			$lastOrder = trim($order->OrderNo); ?>
		<tr>
			<td><?= $order->OrderNo ?></td>
			<td><?= $order->City ?>,  <?= $order->State ?></td>
			<td>
				<a href="#" class="button green">Accept</a>
				<a href="#" class="button red">Decline</a>
			</td>
		</tr>
			<?php } ?>
	<?php } ?>
	</tbody>
</table>
<?php $jsonObject = json_encode($dataListArray); ?>
<script>var gridData = <?= $jsonObject ?>;</script>
<script>
$(document).ready(function () {
	$("#web-referrals").kendoGrid({
		dataSource: {
			pageSize: 10
		},
		height: 600,
		scrollable: false,
		navigatable: true,
		sortable: true,
		filterable: true, 
		pageable: {
			refresh: true,
			pageSizes: true,
			buttonCount: 5
		},
		groupable: true,
		noRecords: true,
		messages: {
			noRecords: "No Referrals Currently Available"
		}, 
		columns: [
			{
				title: 'Referral #',
				field: 'ReferralNumber',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Location',
				field: 'Location',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			}, 
			{
				title: 'Action',
				field: 'Action',
				width: 275,
				sortable: false,
				filterable: false
			}
			
		],
		detailInit: detailInit,
        dataBound: function(e){
			var grid = $('#web-referrals').data('kendoGrid');
			
            $("#referrals-grid-loader").hide();    
            $("#web-referrals").fadeIn();
        }
	});
	
	function detailInit(e) {
		$("<div/>").appendTo(e.detailCell).kendoGrid({
			dataSource: {
				pageSize: 10,
				data: gridData,
			},
			scrollable: true,
			sortable: true,
			pageable: true,
			navigatable: true,
			columns: [
				{ field: "CPT", title: "CPT Code", filterable:{ cell: { showOperators: false }} },
				{ field: "Description", title: "Description", filterable:{ cell: { showOperators: false }} },
				{ field: "RentalSale", title: "Rental/Sale", filterable:{ cell: { showOperators: false }} },
				{ field: "Qty", title: "Qty", width: "100px", filterable: false },
				{ field: "ExpDelivery", title: "Exp. Delivery Date", filterable:{ cell: { showOperators: false }} },
				{ field: "Location", title: "Location", filterable:{ cell: { showOperators: false }} }
			]
		});
	}
});
</script>