<?php
namespace Forbin\Library\Classes\Cache;
include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgtop.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-nav.php';

global $_WEBCONFIG;
?>
<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/modules/site-banner/index.php'); ?>
<script>
$(document).ready(function() {
	$(document).on('click', '.btn-view', function() {
		$('html, body').animate({ scrollTop: $('#referrals-in-process').offset().top - 200}, 500);
		return false;
	});
});
</script>

<main class="dashboard">
	<div class="container clearfix">
		<div class="grid-row">

			<div class="twelvecolM">
				<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-providerInfo-horizontal.php'); ?>
			</div>
        </div>


		<div class="grid-row">
            <!-- Referral Stats Module -->
			<div class="twelvecolM eightcol--large ninecol--xl matchHeight relative">
				<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-statistics.php'); ?>
			</div>

            <div class="twelvecolM fourcol--large threecol--xl matchHeight">
                <div class="eft-module module" id="eftSearch" style="height:100%;">
                    <div class="card-header">
                        <h2>EFT/Check #</h2>
                    </div>

                    <div class="eft-search">
                        <form method="post" autocomplete="off">
                            <input type="text" id="eftNum" placeholder="Enter EFT/Check #">
                            <br /><br />
                            <p><a style="cursor: pointer" class="button silver" id="btnEFTSearch">Search</a></p>
                            <div id="eftSearchResult">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>

        <div class="grid-row">
			<!-- Order/Patient Search Module -->
			<div class="twelvecolM">
				<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-searchMod.php'); ?>
			</div>
		</div><!-- eof grid-row -->

		<!-- Marketing Message Module -->
		<div class="grid-row">
			<div class="twelvecolM">
				<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/marketing/marketing-msg.php'); ?>
			</div>
		</div>
        <div class="container">
            <p style="font-size: 11px">
                <a href="/uploads/userfiles/files/documents/Aetna_Individual_Provider_Addendum.pdf" target="_blank">Aetna Individual Provider Addendum</a><br />
                <a href="/changelog.txt" target="_blank">Provider Portal Release Notes</a>
            </p>
			<p class="notice" style="font-size: 11px">Any questions or concerns can be reported to our Call Center at <a href="tel:18888200355">1-888-820-0355</a> or emailed to <a href="mailto:homelinkcallcenter@vgm.com">homelinkcallcenter@vgm.com</a>.</p>
            <p class="copyright" style="font-size: 11px">&#169; <?= date('Y'); ?>  <?= $siteConfig['Company_Name'] ?>. All Rights Reserved.</p>
		</div>
	</div><!-- eof container -->
</main>
<script type="text/javascript">
$(document).ready(function() {
    $('#btnEFTSearch').on("click", function() {
        $('#eftSearchResult').html('<img src="/images/loading.gif" alt="*" width="15" /> Searching ... ');
        $.get("/members/ajax/eft-search.php", { EFT: $('#eftNum').val() }, function(data) {
            $("#eftSearchResult").html(data);
        });
    });
});
</script>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgbot.php'; ?>