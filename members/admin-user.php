<?php include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgtop.php'; ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-cm/inc-nav.php'); ?>
<?php $sql = "SELECT m_id, m_displayname, m_username, m_status, m_regdate
			FROM tbl_members
			WHERE m_status = 'Active' AND m_provider_number IS NULL
			ORDER BY m_displayname, m_username
			LIMIT 10000";
            $record	= Database::Execute($sql);
  ?>
<main class="dashboard">
	<div class="container"> 
		<div class="provider-number-card module">
		
		<?php if(isset($_GET['message']) && $_GET['message'] == 1) { ?>
<div class="message error" style="background: #CB5456;"><center><span style="font-size: 16px">User is currently not active! </span></center></div>
<?php } ?>
		<form id="providerForm" method="post" action="process.php">
			<input type="hidden" name="actionRun" value="changeCaseManager" />
			<div class="card-header"><h2>Select Claims Professional User</h2></div>

			<label for="providerNumber">Claims Professional Username (Email Address)</label>
            <?php if ($record->Count() > 0) { ?>
                <select name="cboUsername" id="cboUsername" required="required">                
                <option>-- SELECT USER --</option>
                <?php while ($record->MoveNext()) { ?>
                   <option><?= $record->m_username ?></option>
                <?php } ?>
                </select>
            <?php } ?>
			<input type="submit" value="Select User" class="button green" />
		</div>
	</div>
</main>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgbot.php'; ?>