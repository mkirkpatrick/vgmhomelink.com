<?php
namespace Forbin\Members\Library;
use \SoapClient;
use \SimpleXMLElement;

class ViperAPI
{
    const USERID = 'Forbin';
    const ACCESSCODE = '1Q243GH09FGQZ7';
    const SERVICE_URL = 'https://hlservices.vgm.com:8181';

    function getDealerCompany($vgmNo) {
		$postString = '<?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			  <soap:Body>
				<GetDealerCompany xmlns="http://tempuri.org/">
			      <userId>' . self::USERID . '</userId>
				  <accessCode>' . self::ACCESSCODE . '</accessCode>
				  <vgmNo>' . $vgmNo . '</vgmNo>
				</GetDealerCompany>
			  </soap:Body>
			</soap:Envelope>';
		$url = self::SERVICE_URL . '/DealerClaimStatus/Dealer.asmx?op=GetDealerCompany';
		$result = trim(self::getService($url, $postString));
		$xml = new SimpleXMLElement($result);
        if(!$xml) {
            throw new ServiceResponseException($result);
        }
		$xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
		$result = $xml->xpath('//c:Body');
		return $result[0]->GetDealerCompanyResponse->GetDealerCompanyResult->DealerCompany;
	}

    public static function getLineItemDetail($orderNo) {

        $postString = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetLineItemDetail xmlns="http://tempuri.org/">
        <userId>' . self::USERID . '</userId>
        <accessCode>' . self::ACCESSCODE . '</accessCode>
        <OrderNo>' . $orderNo . '</OrderNo>
    </GetLineItemDetail>
  </soap:Body>
</soap:Envelope>';
        $url = self::SERVICE_URL . '/DealerClaimStatus/Dealer.asmx?op=GetLineItemDetail';
        $result = trim(self::getService($url, $postString));
        $xml = new SimpleXMLElement($result);
        $xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
        $result = $xml->xpath('//c:Body');

        return $result[0]->GetLineItemDetailResponse->GetLineItemDetailResult;
    }



    public static function getOrderNotesProvider($orderNo) {

        $postString = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetOrderNotesProvider xmlns="http://tempuri.org/">
        <userId>' . self::USERID . '</userId>
        <accessCode>' . self::ACCESSCODE . '</accessCode>
        <OrderNo>' . $orderNo . '</OrderNo>
    </GetOrderNotesProvider>
  </soap:Body>
</soap:Envelope>';
        $url = self::SERVICE_URL . '/DealerClaimStatus/Dealer.asmx?op=GetOrderNotesProvider';
        $result = trim(self::getService($url, $postString));
        $xml = new SimpleXMLElement($result);
        $xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
        $result = $xml->xpath('//c:Body');

        return $result[0]->GetOrderNotesProviderResponse->GetOrderNotesProviderResult;
    }

    public static function getOrdersTotal($vgmNo, $year) {

        $postString = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetOrdersTotal xmlns="http://tempuri.org/">
        <userId>' . self::USERID . '</userId>
        <accessCode>' . self::ACCESSCODE . '</accessCode>
        <VGMNo>' . $vgmNo . '</VGMNo>
        <year>' . $year . '</year>
    </GetOrdersTotal>
  </soap:Body>
</soap:Envelope>';
        $url = self::SERVICE_URL . '/DealerClaimStatus/Dealer.asmx?op=GetOrdersTotal';
        $result = trim(self::getService($url, $postString));

        $xml = new SimpleXMLElement($result);
        $xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
        $result = $xml->xpath('//c:Body');

        return $result[0]->GetOrdersTotalResponse->GetOrdersTotalResult;
    }

    public static function getOrderProviderAccepted($vgmNo, $orderNo) {

        $postString = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <OrderProviderAccepted xmlns="http://tempuri.org/">
        <userId>' . self::USERID . '</userId>
        <accessCode>' . self::ACCESSCODE . '</accessCode>
        <OrderNo>' . $orderNo . '</OrderNo>
        <VGMNo>' . $vgmNo . '</VGMNo>
    </OrderProviderAccepted>
  </soap:Body>
</soap:Envelope>';
        $url = self::SERVICE_URL . '/DealerClaimStatus/Dealer.asmx?op=OrderProviderAccepted';
        $result = trim(self::getService($url, $postString));
        breakPoint($result);
        $xml = new SimpleXMLElement($result);
        $xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
        $result = $xml->xpath('//c:Body');

        return $result[0]->GetOrdersTotalResponse->GetOrdersTotalResult;
    }

    public static function acceptDealerWebOrder($vgmNo, $orderNo, $dealerContact) {


        $postString = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <DealerWebOrder xmlns="http://tempuri.org/">
        <userId>' . self::USERID . '</userId>
        <accessCode>' . self::ACCESSCODE . '</accessCode>
        <OrderNo>' . $orderNo . '</OrderNo>
        <VGMNo>' . $vgmNo . '</VGMNo>
        <DealerContact>string</DealerContact>
    </DealerWebOrder>
  </soap:Body>
</soap:Envelope>';
        $url = self::SERVICE_URL . '/CMPortal/AcceptDealerWebOrder.asmx?op=DealerWebOrder';
        $result = trim(self::getService($url, $postString));
        //breakPoint($result);
        $xml = new SimpleXMLElement($result);
        $xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
        $result = $xml->xpath('//c:Body');

        return $result[0]->DealerWebOrderResponse->DealerWebOrderResult;
    }

    public static function getDealerWebOrders($vgmNo) {
        $postString = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetDealerWebOrders xmlns="http://tempuri.org/">
        <userId>' . self::USERID . '</userId>
        <accessCode>' . self::ACCESSCODE . '</accessCode>
        <VgmNo>' . $vgmNo . '</VgmNo>
    </GetDealerWebOrders>
  </soap:Body>
</soap:Envelope>';
        $url = self::SERVICE_URL . '/CMPortal/GetDealerWebOrders.asmx?op=GetDealerWebOrders';
        $result = trim(self::getService($url, $postString));
        $xml = new SimpleXMLElement($result);
        $xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
        $result = $xml->xpath('//c:Body');
        return $result[0]->GetDealerWebOrdersResponse->GetDealerWebOrdersResult;
    }

    public static function getOrderHistory($role, $userLoginId, $patientId, $pFName, $pLName, $diagnosisCd, $employer, $claimNo, $insurance,
        $orderNo, $serviceDate, $patientInsuranceId, $authorizationNo, $description) {

        $postString = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetOrderHistory xmlns="http://tempuri.org/">
        <userId>' . self::USERID . '</userId>
        <accessCode>' . self::ACCESSCODE . '</accessCode>
        <Role>' . $role . '</Role>
        <UserLoginId>' . $userLoginId . '</UserLoginId>
        <PatientId>' . $patientId . '</PatientId>
        <PFname>' . $pFName . '</PFname>
        <PLName>' . $pLName . '</PLName>
        <DiagnosisCd>' . $diagnosisCd . '</DiagnosisCd>
        <Employer>' . $employer . '</Employer>
        <ClaimNo>' . $claimNo . '</ClaimNo>
        <Insurance>' . $insurance . '</Insurance>
        <OrderNo>' . $orderNo . '</OrderNo>
        <ServiceDate>' . $serviceDate . '</ServiceDate>
        <PatientInsId>' . $patientInsuranceId . '</PatientInsId>
        <AuthorizationNo>' . $authorizationNo . '</AuthorizationNo>
        <Description>' . $description . '</Description>
    </GetOrderHistory>
  </soap:Body>
</soap:Envelope>';
        $url = self::SERVICE_URL . '/CMPortal/GetOrderHistory.asmx?op=GetOrderHistory';
        $result = trim(self::getService($url, $postString));
        breakPoint($result);
        $xml = new SimpleXMLElement($result);
        $xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
        $result = $xml->xpath('//c:Body');

        return $result[0]->GetOrderHistoryResponse->GetOrderHistoryResult;
    }

	public static  function getPaymentDetailsByOrderNo($vgmNo, $orderNo) {
		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/PaymentDetails.svc?wsdl");
		$request = array(array());
		$request['request']['AccessCode'] = self::ACCESSCODE;
		$request['request']['VGMNo'] = $vgmNo;
		$request['request']['UserId'] = self::USERID;
		$request['request']['OrderNo'] = $orderNo;

        try {
            $response = $client->GetPaymentDetailsByOrderNo($request);
        } catch (Exception $e) {
            $response = null;
        }

		/*if (response.OperationStatus != Orders.ExternalServiceResponseStatus.Success)
		{
			throw new Exception(response.OperationErrors[0]);
		} */
		return isset($response->GetPaymentDetailsByOrderNoResult->Orders->Order) ? $response->GetPaymentDetailsByOrderNoResult->Orders->Order : null;
	}

	public static  function getPaymentDetailsByCheckNo($vgmNo, $checkNo) {
		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/PaymentDetails.svc?wsdl");
		$request = array(array());
		$request['request']['AccessCode'] = self::ACCESSCODE;
		$request['request']['VGMNo'] = $vgmNo;
		$request['request']['UserId'] = self::USERID;
		$request['request']['CheckNo'] = $checkNo;
		$response = $client->GetPaymentDetailsByCheckNo($request);

		/*if (response.OperationStatus != Orders.ExternalServiceResponseStatus.Success)
		{
			throw new Exception(response.OperationErrors[0]);
		} */
		return isset($response->GetPaymentDetailsByCheckNoResult->Orders->Order) ? $response->GetPaymentDetailsByCheckNoResult->Orders->Order : null;
	}

	public static function getProviderOrdersByVGMNo($vgmNo, $startDate, $endDate) {


		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/provider.svc?wsdl");
		$request = array(array());
		$request['request']['VgmNo'] = $vgmNo;
		$request['request']['AccessCode'] = self::ACCESSCODE;
		$request['request']['UserId'] = self::USERID;
		$request['request']['EndDate'] = $endDate;
		$request['request']['StartDate'] = $startDate;
		$response = $client->GetProviderOrdersByVGMNo($request);

		//breakPoint($response->GetProviderOrdersByVGMNoResult->ProviderOrders);

		$return = array();
		/*if($response->GetProviderOrdersByVGMNoResult->ProviderOrders->count()) {
			return $response->GetProviderOrdersByVGMNoResult->ProviderOrders->ProviderOrders;
		} */
		if(isset($response->GetProviderOrdersByVGMNoResult->ProviderOrders->ProviderOrders)) {
			return $response->GetProviderOrdersByVGMNoResult->ProviderOrders->ProviderOrders;
		}

		return $return;
	}

	public static function getProviderYTDReferralsAccepted($vgmNo) {
		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/provider.svc?wsdl");
		$request = array(array());
		$request['request']['VgmNo'] = $vgmNo;
		$request['request']['AccessCode'] = self::ACCESSCODE;
		$request['request']['UserId'] = self::USERID;
		$request['request']['EndDate'] = '2017-02-02'; // Fake Date
		$request['request']['StartDate'] = '2012-02-02'; // Fake Date
		$response = $client->GetProviderYTDReferralsAccepted($request);

        if(isset($response->GetProviderYTDReferralsAcceptedResult->ProviderSummary->YTDReferralsAccepted)) {
            return (int) $response->GetProviderYTDReferralsAcceptedResult->ProviderSummary->YTDReferralsAccepted;
        } else {
            return 0;
        }
	}

	public static function getProviderYTDReferralsDenied($vgmNo) {
		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/provider.svc?wsdl");
		$request = array(array());
		$request['request']['VgmNo'] = $vgmNo;
		$request['request']['AccessCode'] = self::ACCESSCODE;
		$request['request']['UserId'] = self::USERID;
		$request['request']['EndDate'] = '2017-02-02'; // Fake Date
		$request['request']['StartDate'] = '2012-02-02'; // Fake Date
		$response = $client->GetProviderYTDReferralsDenied($request);

		return (int) $response->GetProviderYTDReferralsDeniedResult->ProviderSummary->YTDReferralsDenied;
	}

	public static function getProviderYTDDirectReferrals($vgmNo) {


		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/provider.svc?wsdl");
		$request = array(array());
		$request['request']['VgmNo'] = $vgmNo;
		$request['request']['AccessCode'] = self::ACCESSCODE;
		$request['request']['UserId'] = self::USERID;
		$request['request']['EndDate'] = '2017-02-02'; // Fake Date
		$request['request']['StartDate'] = '2012-02-02'; // Fake Date
		$response = $client->GetProviderYTDDirectReferrals($request);

		return (int) $response->GetProviderYTDDirectReferralsResult->ProviderSummary->YTDProviderDirectReferrals;
	}

	public static function getProviderDetailsByVGMNo($vgmNo) {


		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/provider.svc?wsdl");
		$request = array(array());
		$request['request']['VgmNo'] = $vgmNo;
		$request['request']['AccessCode'] = self::ACCESSCODE;
		$request['request']['UserId'] = self::USERID;
		$request['request']['EndDate'] = '2017-02-02'; // Fake Date
		$request['request']['StartDate'] = '2012-02-02'; // Fake Date
		$response = $client->GetProviderDetailsByVGMNo($request);
        if(isset($response->GetProviderDetailsByVGMNoResult->OperationErrors->string)) {
           throw new OperationalErrorException($response->GetProviderDetailsByVGMNoResult->OperationErrors->string);
        }

		return $response->GetProviderDetailsByVGMNoResult->extProvider;
	}

	public static function getProviderNoBillOrdersByVGMNo($vgmNo) {
		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/provider.svc?wsdl");
		$request = array(array());
		$request['request']['VgmNo'] = $vgmNo;
		$request['request']['AccessCode'] = self::ACCESSCODE;
		$request['request']['UserId'] = self::USERID;
		$request['request']['EndDate'] = '2017-02-02'; // Fake Date
		$request['request']['StartDate'] = '2012-02-02'; // Fake Date
		$response = $client->GetProviderNoBillOrdersByVGMNo($request);
        $return = array();
        if(isset($response->GetProviderNoBillOrdersByVGMNoResult->ProviderOrders->ProviderOrders) && !is_object($response->GetProviderNoBillOrdersByVGMNoResult->ProviderOrders->ProviderOrders)) {
            $count = 0; ;
            while(isset($response->GetProviderNoBillOrdersByVGMNoResult->ProviderOrders->ProviderOrders[$count])) {
                array_push($return, $response->GetProviderNoBillOrdersByVGMNoResult->ProviderOrders->ProviderOrders[$count]);
                $count++;
            }
        } else if (isset($response->GetProviderNoBillOrdersByVGMNoResult->ProviderOrders->ProviderOrders) && is_object($response->GetProviderNoBillOrdersByVGMNoResult->ProviderOrders->ProviderOrders)) {
            array_push($return, $response->GetProviderNoBillOrdersByVGMNoResult->ProviderOrders->ProviderOrders);
        }
        return $return;
	}

	public static function getActivePayers($vgmNo) {
		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/provider.svc?wsdl");
		$request = array(array());
		$request['request']['VgmNo'] = $vgmNo;
		$request['request']['AccessCode'] = self::ACCESSCODE;
		$request['request']['UserId'] = self::USERID;
		$request['request']['EndDate'] = '2017-02-02';
		$request['request']['StartDate'] = '2017-02-02';
		$response = $client->GetActivePayers($request);
        if(isset($response->GetActivePayersResult->OperationErrors->string)) {
           throw new OperationalErrorException($response->GetActivePayersResult->OperationErrors->string);
        }
		return $response->GetActivePayersResult->extPayer->ActivePayer;
	}


	public static function getProviderPendingPayments($vgmNo) {
		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/provider.svc?wsdl");
		$request = array(array());
		$request['request']['VgmNo'] = $vgmNo;
		$request['request']['AccessCode'] = self::ACCESSCODE;
		$request['request']['UserId'] = self::USERID;
		$request['request']['EndDate'] = '2017-02-02'; // Fake Date
		$request['request']['StartDate'] = '2012-02-02'; // Fake Date
		$response = $client->GetProviderPendingPayments($request);

        if(isset($response->GetProviderPendingPaymentsResult->OperationErrors->string)) {
            throw new OperationalErrorException($response->GetProviderPendingPaymentsResult->OperationErrors->string);
        }

        if(isset($response->GetProviderPendingPaymentsResult->ProviderSummary->PendingPayments)) {
            return number_format($response->GetProviderPendingPaymentsResult->ProviderSummary->PendingPayments, 2);
        } else {
            return "0.00";
        }
	}

	public static function getProviderYTDPaid($vgmNo) {
		$client = new SoapClient("https://services3.vgm.com:8989/Dealers/DealerServices/provider.svc?wsdl");
		$request = array(array());
		$request['request']['VgmNo'] = $vgmNo;
		$request['request']['AccessCode'] = self::ACCESSCODE;
		$request['request']['UserId'] = self::USERID;
		$request['request']['EndDate'] = '2012-02-02'; // Fake Date
		$request['request']['StartDate'] = '2012-02-02'; // Fake Date
		$response = $client->GetProviderYTDPaid($request);

		if(isset($response->GetProviderYTDPaidResult->ProviderSummary->YTDPaid)) {
			return number_format($response->GetProviderYTDPaidResult->ProviderSummary->YTDPaid, 2);
		} else {
			return "0.00";
		}
	}

	public static function getPatients($orderNo) {


		$postString = '<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xsi:schemaLocation="http://tempuri.org/ tempuri.xsd http://schemas.xmlsoap.org/soap/envelope/ http://schemas.xmlsoap.org/soap/envelope/">
                          <soap:Body>
                            <PatientSearch xmlns="http://ws.vgmhomelink.com/CMPortal">
                              <userId>' . self::USERID . '</userId>
                              <accessCode>' . self::ACCESSCODE . '</accessCode>
                              <UserLoginId>string</UserLoginId>
                              <PFname>string</PFname>
                              <PLName>string</PLName>
                              <Employer>string</Employer>
                              <ClaimNo>string</ClaimNo>
                              <DiagnosisCd>string</DiagnosisCd>
                              <Insurance>string</Insurance>
                              <SSN>string</SSN>
                              <PatientInsId>string</PatientInsId>
                              <AuthorizationNo>string</AuthorizationNo>
                              <DOB>string</DOB>
                              <Phone>string</Phone>
                              <Address>string</Address>
                              <InjuryDate>string</InjuryDate>
                              <Role>string</Role>
                            </PatientSearch>
                          </soap:Body>
                        </soap:Envelope>';
		$url = self::SERVICE_URL . '/CMPortal/PatientSearch.asmx?op=PatientSearch';
		$result = trim(self::getService($url, $postString));
		$xml = new SimpleXMLElement($result);
		$xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
		$result = $xml->xpath('//c:Body');
		return $result[0]->GetDealerErrorsResponse->GetDealerErrorsResult->DealerErrors;
	}

    public static function getCaseManagerLoginInfo($username, $password) {

		$postString = '<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                  <soap:Body>
                    <Login xmlns="http://ws.vgmhomelink.com/CMPortal">
                                              <userId>' . self::USERID . '</userId>
                                              <accessCode>' . self::ACCESSCODE . '</accessCode>
                      <userName>' . $username . '</userName>
                    </Login>
                  </soap:Body>
                </soap:Envelope>';
		$url = self::SERVICE_URL . '/cmportal/Login.asmx?op=Login';
		$result = trim(self::getService($url, $postString));
		$xml = new SimpleXMLElement($result);
        $xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
        $result = $xml->xpath('//c:Body');
        return $result[0]->LoginResponse->LoginResult;
	}

    public static function getCaseManagerOrderHistory($cmUserId, $patientId = null) {

		$postString = '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Body>
                <GetOrderHistory xmlns="http://ws.vgmhomelink.com/CMPortal">
                  <userId>Forbin</userId>
                  <accessCode>1Q243GH09FGQZ7</accessCode>
                  <Role>1</Role>
                  <UserLoginId>' . $cmUserId . '</UserLoginId>
                  <PatientId>' . $patientId . '</PatientId>
                </GetOrderHistory>
              </soap:Body>
            </soap:Envelope>';
		$url = self::SERVICE_URL . '/CMPortal/GetOrderHistory.asmx?op=GetOrderHistory';
		$result = trim(self::getService($url, $postString));
		$xml = new SimpleXMLElement($result);
		$xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
		$result = $xml->xpath('//c:Body');
		return $result[0]->GetOrderHistoryResponse->GetOrderHistoryResult;
	}

    public static function getOrder($orderNo) {

		$postString = '<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                          <soap:Body>
                            <OrderLoad xmlns="http://ws.vgmhomelink.com/CMPortal">
                                <userId>Forbin</userId>
                                <accessCode>1Q243GH09FGQZ7</accessCode>
                                <orderNo>' . $orderNo . '</orderNo>
                            </OrderLoad>
                          </soap:Body>
                        </soap:Envelope>';
		$url = self::SERVICE_URL . '/CMPortal/OrderLoad.asmx?op=OrderLoad';
		$result = trim(self::getService($url, $postString));
		$xml = new SimpleXMLElement($result);
		$xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
		$result = $xml->xpath('//c:Body');
		return $result[0]->OrderLoadResponse->OrderLoadResult->Order;
	}

    public static function getOrderNotes($orderNo) {

		$postString = '<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                          <soap:Body>
                            <GetNotes xmlns="http://tempuri.org/">
                                <userId>Forbin</userId>
                                <accessCode>1Q243GH09FGQZ7</accessCode>
                                <orderNo>' . $orderNo . '</orderNo>
                            </GetNotes>
                          </soap:Body>
                        </soap:Envelope>';
		$url = self::SERVICE_URL . '/CMPortal/GetOrderNotes.asmx?op=GetNotes';
		$result = trim(self::getService($url, $postString));
		$xml = new SimpleXMLElement($result);
		$xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
		$result = $xml->xpath('//c:Body');
		return isset($result[0]->GetNotesResponse->GetNotesResult->NotesSearch) ? $result[0]->GetNotesResponse->GetNotesResult->NotesSearch : array();
	}

    public static function getOrderLineItems($orderNo) {

		$postString = '<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                          <soap:Body>
                            <LoadLineItems xmlns="http://ws.vgmhomelink.com/CMPortal">
                                <userId>Forbin</userId>
                                <accessCode>1Q243GH09FGQZ7</accessCode>
                                <OrderNo>' . $orderNo . '</OrderNo>
                            </LoadLineItems>
                          </soap:Body>
                        </soap:Envelope>';
		$url = self::SERVICE_URL . '/CMPortal/LineItemsLoad.asmx?op=LoadLineItems';
		$result = trim(self::getService($url, $postString));
		$xml = new SimpleXMLElement($result);
		$xml->registerXPathNamespace('c', 'http://schemas.xmlsoap.org/soap/envelope/');
		$result = $xml->xpath('//c:Body');
        if(isset($result[0]->LoadLineItemsResponse->LoadLineItemsResult->LineItems)) {
            $count = 0;
            $return = array();
            while(isset($result[0]->LoadLineItemsResponse->LoadLineItemsResult->LineItems[$count])) {
                array_push($return, $result[0]->LoadLineItemsResponse->LoadLineItemsResult->LineItems[$count]);
                $count++;
            }
            return $return;
        } else {
            return array();
        }
	}

	private static function getService($url, $postString) {
		$connectionHandler = curl_init($url);
		curl_setopt($connectionHandler, CURLOPT_POST, true);
		curl_setopt($connectionHandler, CURLOPT_POSTFIELDS, $postString);
		curl_setopt($connectionHandler, CURLOPT_HTTPHEADER, array('Content-Type: text/xml','Connection: Keep-Alive'));
		curl_setopt($connectionHandler, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($connectionHandler, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt($connectionHandler, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($connectionHandler, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($connectionHandler, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($connectionHandler, CURLOPT_TIMEOUT, 60);
		$result = curl_exec($connectionHandler);
		$info = curl_getinfo($connectionHandler);
		$errorNumber = curl_errno($connectionHandler);
		$errorMessage = curl_error($connectionHandler);
		curl_close($connectionHandler);

		return $result;
	}
}

class OperationalErrorException extends \RuntimeException {}
class ServiceResponseException extends \RuntimeException {}

?>