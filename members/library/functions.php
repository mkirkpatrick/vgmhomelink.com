<?php
//------------------------------
function _isMember() {
//------------------------------
	return isset($_SESSION['member_id']) ? true : false;
}

//------------------------------
function _getMemberLink() {
//------------------------------
	return _isMember() ? '<a href="/members?logout" title="Log me out" class="loginLink">Logout</a>' 
		: '<a href="/members/login" title="Login to My Account" class="loginLink">Login</a> or <a href="/members/registration.php" title="Create an Account">Create an Account</a>.';
}

//------------------------------
function _getMemberName() {
//------------------------------
	return isset($_SESSION['member_id']) ? $_SESSION['member_name'] : 'Guest';
}
?>