<?php
	include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/inc-pgtop.php';
	include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-cm/inc-nav.php');
?>
<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/modules/site-banner/index.php'); ?>
<main class="dashboard horizontal">
	<div class="container clearfix">
		<!-- Order/Patient Search Module -->
		<div class="grid-row">
			<div class="twelvecolM fivecol--medium threecol--xl">
				<? include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-cm/inc-providerInfo.php'); ?>
			</div>

			<!-- Order/Patient Search Module -->
			<div class="twelvecolM sevencol--medium ninecol--xl">
				<? include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-cm/inc-searchMod.php'); ?>
			</div>
		</div>

		<!-- Full Graph Module -->
		<div class="grid-row none">
			<div class="twelvecolM">
				<div class="ytd-graph-card module">
					<div class="card-header"><h2>Year to Date Graph</h2></div>
					<div class="canvas-holder" style="max-height:500px; margin: 0 auto;"><canvas id="chart" width="1000" height="500"></canvas></div>
				</div>
			</div>
		</div>

		<!-- Marketing Message Module -->
		<div class="grid-row">
			<div class="twelvecolM">
				<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/marketing/marketing-msg.php'); ?>
			</div>
		</div>
	</div>
</main>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgbot.php'; ?>