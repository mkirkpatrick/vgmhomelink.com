var myLineChart = document.getElementById('chart').getContext('2d');

var data  = {
    labels: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],

    datasets: [
        {
			radius: 6,
			backgroundColor: 'rgba(52,152,219,.35)',
			borderWidth: 0,
			borderColor: '#3498db',
			pointBorderWidth: 2,
			pointBorderColor: "#1F74B5",
			pointBackgroundColor: '#ffffff',
			pointHoverBackgroundColor: '#1F74B5',
			pointHoverRadius: 6,
			pointHitRadius: 10,
          	data: [150, 100, 180, 81, 86, 85, 115, 100, 85, 90, 140, 101]
        }
    ]
};

var myLineChart = new Chart(myLineChart, { type: 'line', data: data, 
	options: {
		maintainAspectRatio: false,
		reponsive: true,
		layout: {
			padding: 10
		},
		legend: {
			display: false
		},
		scales: {
			yAxes: [{
				ticks: {
					max: 250,
					stepSize: 50
				}
			}],
			xAxes: [{
				gridLines: false
			}]
		}
	}
});