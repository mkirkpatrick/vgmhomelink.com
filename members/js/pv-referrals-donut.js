var ctxDonut = document.getElementById("pv-referrals");
var myDoughnutChart = new Chart(ctxDonut, {
    type: 'doughnut',
    data: {
		labels: [
			"Referrals Denied",
			"Referrals Accepted"
		],
		datasets: [{
			data: [<?= $deniedReferrals ?>, <?= $acceptedReferrals ?>],
			backgroundColor: [
				"#d64444",
				"#60c84c"
			],
			hoverBackgroundColor: [
				"#d64444",
				"#60c84c"
			]
		}]
	},
    options: {
		cutoutPercentage: 70,
		legend: {
			display: false
		}
	}
});