var ctxDonut = document.getElementById("cm-referrals");
var myDoughnutChart = new Chart(ctxDonut, {
    type: 'doughnut',
    data: {
		labels: [
			"Pending Auth",
			"Claims Authorized"
		],
		datasets: [{
			data: [135, 488],
			backgroundColor: [
				"#d64444",
				"#60c84c"
			],
			hoverBackgroundColor: [
				"#d64444",
				"#60c84c"
			]
		}]
	},
    options: {
		cutoutPercentage: 70,
		legend: {
			display: false
		}
	}
});

Chart.pluginService.register({
  beforeDraw: function(chart) {
    var width = chart.chart.width,
        height = chart.chart.height,
        ctx = chart.chart.ctx;

    ctx.restore();
    var fontSize = (height / 114).toFixed(2);
    ctx.font = fontSize + "em sans-serif";
    ctx.textBaseline = "middle";

    var text = "677",
        textX = Math.round((width - ctx.measureText(text).width) / 2),
        textY = height / 2;

    ctx.fillText(text, textX, textY);
    ctx.save();
  }
});