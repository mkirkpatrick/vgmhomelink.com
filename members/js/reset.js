// JavaScript Document
$(function() {
	$('#txtPassword').on('focus blur', function () {
		$(this).siblings('.password-reqs').addClass('active');
	});

	$('#txtPassword').on('focusoff blur', function () {
		$(this).siblings('.password-reqs').removeClass('active');
	});

    $("input[type='submit']").click(function (event) {
        event.preventDefault();
        var myform = '#' + $(this).closest("form").attr('id');
        var buttonVal = $(this).val();
        $(this).attr('disabled','disabled');
        $(this).val('Saving...');
        $(this).attr('disabled', 'disabled');
        $(myform).parsley({excluded: "input[type=button], input[type=submit], input[type=reset], input[type=hidden], input:hidden, textarea[type=hidden], textarea:hidden"});
        if($(myform).parsley().isValid()) {
            $(myform).submit();
        }
        else {
            $(this).val(buttonVal);
            $(this).removeAttr('disabled');
            $(myform).parsley().validate();
        }
    });
});