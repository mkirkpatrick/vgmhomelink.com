// Button Functions
function addAddress() {
    window.location.href = '/members/address-book/add-address?view=add';
}

function modifyAddress(Id) {
    window.location.href = '/members/address-book/modify-address?view=modify&id=' + Id;
}

function deleteAddress(Id) {
    if (confirm('Are You Sure?')) {
        window.location.href = 'process.php?actionRun=address-del&id=' + Id;
    }
}
