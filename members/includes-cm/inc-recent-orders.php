<?php
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

$orders = ViperAPI::getCaseManagerOrderHistory($_SESSION['member_api_id']);
?>
<style type="text/css">
.k-grid { min-height: 590px!important; }
</style>
<div id="recent-grid" class="none" style="display: none"></div>
<script>
$(document).ready(function () {
	$("#recent-grid").kendoGrid({
		dataSource: {
			transport: {
				read:  {
					url: "/members/datasource/recent_referral_history.php",
					dataType: "json"
				}
			},
			pageSize: 8,
			batch: true,
            error: function(e) {
                console.log(e.errors);
            }
		},
		filterable: {
			mode: 'row'
		},
		scrollable: true,
		navigatable: true,
		groupable: true,
		sortable: true,
		resizable: true,
		height: 500,
		pageable: {
			refresh: true,
			pageSizes: true,
			buttonCount: 5
		},
		columns: [
			{
				title: 'Patient Name',
				field: 'PatientName',
				template: '<a href="/members/patient-details.php?pid=#=PatientId#&amp;name=#=PatientName#">#=PatientName#</a>',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			}, {
				title: 'Claim #',
				field: 'ClaimNumber',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			}, {
				title: 'Referral #',
				field: 'OrderNumber',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			}, {
				title: 'Referral Date',
				field: 'ReferralDate',
				filterable: {
					cell: {
						showOperators: false
					}
				}
			}, {
				title: 'Referral Status',
				field: 'Status',
				width: 200,
				filterable: {
					cell: {
						showOperators: false
					}
				}
			},
			{
				title: 'Details',
				field: 'PatientId',
				template: '<a href="/members/order-details.php?on=#=OrderNumber#&amp;pid=#=PatientId#&amp;name=#=PatientName#" class="button green">View</a>',
				width: 130,
				filterable: {
					cell: {
						showOperators: false,
					}
				}
			},
		],
        dataBound: function(e) {
			var grid = $('#recent-grid').data('kendoGrid');

            $("#recent-grid-loader").hide();
            $("#recent-grid").fadeIn();

			grid.tbody.find('tr.k-grouping-row').each(function () {
				grid.collapseGroup(this);
			});
        }
	});

	$('#recent-grid').data('kendoGrid').dataSource.read();
});
</script>