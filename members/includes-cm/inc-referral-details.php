<div class="fixed-items module">
	<div class="card-header"><h2>Referral Information</h2></div>
	
	<div class="grid-row">
		<div class="module-2-pie twelvecolM fourcol--medium threecol--large">
			<div id="canvas-holder" style="max-width: 300px; margin: 0 auto;">
				<canvas id="cm-referrals" width="300px" height="300px" />
			</div>
		</div>

		<div class="module-2-stats twelvecolM eightcol--medium ninecol--large">					
			<div class="grid-row">
				<div class="twelvecolM sixcol--medium">
					<div class="item item-1 unique-shadow">
						<h1>677</h1>
						<p class="stats-period stats-period-positive">Claims Worked</p>
					</div>
					<div class="item item-1 unique-shadow">
						<h1>488</h1>
						<p class="stats-period stats-period-positive">Claims Authorized</p>
					</div>
				</div>
			
				<div class="twelvecolM sixcol--medium">
					<div class="item item-1 unique-shadow">
						<h1>135</h1>
						<p class="stats-period stats-period-negative">Pending Auth</p>
					</div>
					
					<div class="unbilled-target item item-1 unique-shadow">
						<h1>$244.33</h1>
						<p class="stats-period stats-period-negative">Pending Payments</p>
					</div>
				</div>
			
				<div class="module-2-average twelvecolM fourcol--large">
					<div class="item item-1 unique-shadow none">
						<div class="meter"><span style="width: 50%"></span></div>
						<p><span>2016</span> Avg = <span>$238.67</span></p>
					</div>
		<!--
					<div class="item curve-mini item-3">
			   <div id="curve_chart-mini" style="width: 80%; max-width: 500px; height: 50px; margin: 0 auto;"></div>
						<canvas id="shipments"></canvas>
					</div>
		-->
					
				</div>
			</div><!-- eof grid-row -->
		</div>
	</div>
</div>