<?php 
use Forbin\Library\Classes\Member;
Member::_secureCheck();
if($_SESSION['member_type'] == 'Provider') {
    redirect('/members/provider-dashboard.php'); 
}
addHtmlTag('/members/css/dashboard.css', 'css'); 

if($_SESSION['member_require_password'] && $_SERVER['PHP_SELF'] != '/members/change-password.php') { 
    redirect("/members/change-password.php?expired"); 
}

include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-cm/inc-mobilenav.php'); ?>
<header class="db-topmenu">
   	<nav class="navbar">
		<div class="container clearfix">
  			<a href="/" class="db-logo floatLeft" title="VGM Homelink"><img src="/images/dash-logo.png" alt="VGM Homelink"></a>
               <div class="floatLeft" style="margin-left: 25px"><strong><?= $_SESSION['member_username'] ?></strong> <?= HTML_SPACE ?>
               <?php if($_SESSION['member_admin']) { ?>
               (<a href="/members/admin-user.php">Switch Claims Professional</a>)
               <?php } ?>
               </div>
			<ul class="nobullets navbar-top-links floatRight">
				<li class="member-welcome large-only">Welcome <a href="/members/account.php"><?= $_SESSION['member_name'] ?></a></li>
                <?php if(!$_SESSION['member_admin']) { ?>
                <li class="large-only"><a href="/members/change-password.php">Change Password</a></li>
                 <?php } ?>
				<li class="large-only"><a href="/members/index.php?logout"><i class="fa-sign-out"></i> Logout</a></li>
				<li class="mobileonly"><a href="#" class="openmobile"><span>Menu</span></a></li>
			</ul>
    	</div>
	</nav>
</header>

<nav class="db-sidemenu horizontal large-only">
    <ul class="navigation nobullets">
		<li><a href="/members/cm-dashboard.php">Dashboard</a></li>
		<li><a href="http://www.hmeforms.com" target="_blank">Forms</a></li>
		<li><a href="https://hmeforms.com/case-managers-quick-referral" target="_blank">Submit Referral</a></li>
        <!--<li><a href="/quick-order-form">Quick Order Form</a></li>
        <li><a href="/locator">Provider Locator</a></li>-->
        <li><a href="http://www.homelinku.com/" target="_blank">Homelink University</a></li>
    </ul><!-- eof navigation -->
</nav>