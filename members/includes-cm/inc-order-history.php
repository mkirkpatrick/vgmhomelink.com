<?php 
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;
$order = ViperAPI::getCaseManagerOrderHistory(); 
/*$cacheKey = $_SESSION['member_prov_num'] . '_WEB_ORDERS_' . $sdate . '_' . $edate; 
if(!Cache::has($cacheKey)) {
	$orders = getProviderOrdersByVGMNo($_SESSION['member_prov_num'], $sdate, $edate);
	Cache::set($cacheKey, $orders, 7200);
} else {
	$orders = $orders = Cache::get($cacheKey); 
} */ 

?>

<table id="history-grid">
	<thead>
		<tr>
			<th scope="col" data-field="OrderNumber">Order #</th>
			<th scope="col" data-field="Name">Name</th>
			<th scope="col" data-field="PatientID">Patient ID</th>
			<th scope="col" data-field="OrderDate">Order Date</th>
			<th scope="col" data-field="ServiceType">Service Type</th>
			<th scope="col" data-field="ClaimNumber">Claim #</th>
			<th scope="col" data-field="Details">Details</th>
		</tr>
	</thead>         
	<tbody>
		<tr>			
			<td>5649009</td>
			<td>Joe Smith</td>
			<td>232002</td>
			<td>02/20/2017</td>
			<td>Transportation</td>
			<td>WC111-111111</td>
			<td class="recent-details"><a href="/members/order-details.php">View Details</a></td>
		</tr>
		<tr>			
			<td>5649009</td>
			<td>Joe Smith</td>
			<td>232002</td>
			<td>02/20/2017</td>
			<td>Transportation</td>
			<td>WC111-111111</td>
			<td class="recent-details"><a href="/members/order-details.php">View Details</a></td>
		</tr>
		<tr>			
			<td>5649009</td>
			<td>Joe Smith</td>
			<td>232002</td>
			<td>02/20/2017</td>
			<td>Transportation</td>
			<td>WC111-111111</td>
			<td class="recent-details"><a href="/members/order-details.php">View Details</a></td>
		</tr>
		<tr>			
			<td>5649009</td>
			<td>Joe Smith</td>
			<td>232002</td>
			<td>02/20/2017</td>
			<td>Transportation</td>
			<td>WC111-111111</td>
			<td class="recent-details"><a href="/members/order-details.php">View Details</a></td>
		</tr>
		<tr>			
			<td>5649009</td>
			<td>Joe Smith</td>
			<td>232002</td>
			<td>02/20/2017</td>
			<td>Transportation</td>
			<td>WC111-111111</td>
			<td class="recent-details"><a href="/members/order-details.php">View Details</a></td>
		</tr>
		<tr>			
			<td>5649009</td>
			<td>Joe Smith</td>
			<td>232002</td>
			<td>02/20/2017</td>
			<td>Transportation</td>
			<td>WC111-111111</td>
			<td class="recent-details"><a href="/members/order-details.php">View Details</a></td>
		</tr>
		<tr>			
			<td>5649009</td>
			<td>Joe Smith</td>
			<td>232002</td>
			<td>02/20/2017</td>
			<td>Transportation</td>
			<td>WC111-111111</td>
			<td class="recent-details"><a href="/members/order-details.php">View Details</a></td>
		</tr>
		<tr>			
			<td>5649009</td>
			<td>Joe Smith</td>
			<td>232002</td>
			<td>02/20/2017</td>
			<td>Transportation</td>
			<td>WC111-111111</td>
			<td class="recent-details"><a href="/members/order-details.php">View Details</a></td>
		</tr>
		<tr>			
			<td>5649009</td>
			<td>Joe Smith</td>
			<td>232002</td>
			<td>02/20/2017</td>
			<td>Transportation</td>
			<td>WC111-111111</td>
			<td class="recent-details"><a href="/members/order-details.php">View Details</a></td>
		</tr>
		<tr>			
			<td>5649009</td>
			<td>Joe Smith</td>
			<td>232002</td>
			<td>02/20/2017</td>
			<td>Transportation</td>
			<td>WC111-111111</td>
			<td class="recent-details"><a href="/members/order-details.php">View Details</a></td>
		</tr>
	</tbody>
</table>
<script>
	$(document).ready(function () {
		$("#history-grid").kendoGrid({
			dataSource: {
				pageSize: 10
			},
			height: 450,
			scrollable: true,
			navigatable: true,
			groupable: true,
			reorderable: true,
			sortable: true,
			resizable: true,
			mobile: true,
			height: 450,
			filterable: true,
			columnMenu: true,
			pageable: {
				refresh: true,
				pageSizes: true,
				buttonCount: 5
			},
			columns: [
				{
					title: 'Referral #',
					field: 'OrderNumber'
				}, {
					title: 'Name',
					field: 'Name'
				}, {
					title: 'Patient ID',
					field: 'PatientID'
				}, {
					title: 'Order Date',
					field: 'OrderDate'
				}, {
					title: 'Service Type',
					field: 'ServiceType'
				}, {
					title: 'Claim #',
					field: 'ClaimNumber'
				}, {
					title: 'Details',
					field: 'Details',
					filterable: false,
					sortable: false,
					groupable: false
				}
			],
			dataBound: function(e) {
				var grid = $('#history-grid').data('kendoGrid');
				
				grid.tbody.find('tr.k-grouping-row').each(function () {
					grid.collapseGroup(this);
				});
			}
		});
		
		$('#history-grid').data('kendoGrid').thead.find("[data-field=Details]>.k-header-column-menu").remove();
	});
</script>