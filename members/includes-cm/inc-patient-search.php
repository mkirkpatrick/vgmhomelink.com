<table id="patient-grid">
	<thead>
		<tr>
			<th scope="col" data-field="PatientID">ID</th>
			<th scope="col" data-field="PatientName">Patient Name</th>
			<th scope="col" data-field="PatientCity">Patient City</th>
			<th scope="col" data-field="ClaimID">Claim #</th>
            <th scope="col" data-field="OrderDate">Recent Order Date</th>     
            <th scope="col" data-field="Details">Details</th>                             
		</tr>
	</thead>         
	<tbody>
		<tr>
			<td>10290</td>
			<td>John Smith</td>
			<td>Oklahoma City</td>
			<td>8019210391</td>
			<td>11/10/2016</td>
			<td><a href="/members/patient-details.php">View Details</a></td>
		</tr>
		
		<tr>
			<td>84910</td>
			<td>Marley Davidson</td>
			<td>Seattle</td>
			<td>578291822</td>
			<td>07/02/2016</td>
			<td><a href="/members/patient-details.php">View Details</a></td>
		</tr>
		
		<tr>
			<td>14811</td>
			<td>Jane Doe</td>
			<td>Des Moines</td>
			<td>10191</td>
			<td>1/31/2016</td>
			<td><a href="/members/patient-details.php">View Details</a></td>
		</tr>
		
		<tr>
			<td>11939</td>
			<td>Martin Fly</td>
			<td>New York City</td>
			<td>89481921</td>
			<td>09/01/2016</td>
			<td><a href="/members/patient-details.php">View Details</a></td>
		</tr>
		
		<tr>
			<td>90192</td>
			<td>Francis O'Locklin</td>
			<td>Dallas</td>
			<td>019294011</td>
			<td>07/15/2016</td>
			<td><a href="/members/patient-details.php">View Details</a></td>
		</tr>
		
		<tr>
			<td>1029</td>
			<td>Eric Donaldson</td>
			<td>Waterloo</td>
			<td>123113777</td>
			<td>06/23/2016</td>
			<td><a href="/members/patient-details.php">View Details</a></td>
		</tr>
		
		<tr>
			<td>10311</td>
			<td>Kyle Rickter</td>
			<td>Cedar Falls</td>
			<td>3123411</td>
			<td>04/19/2016</td>
			<td><a href="/members/patient-details.php">View Details</a></td>
		</tr>
		
		<tr>
			<td>90192</td>
			<td>Derek Rose</td>
			<td>Waterloo</td>
			<td>019294011</td>
			<td>07/15/2016</td>
			<td><a href="/members/patient-details.php">View Details</a></td>
		</tr>
		
		<tr>
			<td>1029</td>
			<td>Kyle Korver</td>
			<td>Des Moines</td>
			<td>123113777</td>
			<td>06/23/2016</td>
			<td><a href="/members/patient-details.php">View Details</a></td>
		</tr>
		
		<tr>
			<td>10311</td>
			<td>Marcus Louis</td>
			<td>Nashua</td>
			<td>3123411</td>
			<td>04/19/2016</td>
			<td><a href="/members/patient-details.php">View Details</a></td>
		</tr>
	</tbody>
</table>

<script>
$(document).ready(function() {
	$("#patient-grid").kendoGrid({
		dataSource: {
			type: "odata",
			schema: {
				model: {
					fields: {
						PatientID: { type: "number" },
						PatientName: { type: "string" },
						PatientCity: { type: "string" },
						ClaimID: { type: "number" },
						OrderDate: { type: "date" },
					}
				}
			},
			pageSize: 10
		},
		height: 450,
		filterable: true,
		columnMenu: true,
		sortable: true,
		resizable: true,
		scrollable: true,
		groupable: true,
		pageable: {
			refresh: true,
			pageSizes: true,
			buttonSize: 5
		},
		columns: 
		[{
			field: "PatientID",
			width: 100
		},{
			field: "PatientName",
			width: 275,
			title: "Patient Name"
		},{
			field: "PatientCity",
			//width: 225,
			title: "Patient City"
		},{
			field: "OrderID",
			//width: 200,
		},{
			field: "OrderDate",
			title: "Order Date",
			format: "{0:MM/dd/yyyy}"
		},{
			field: "Details",
			title: "Details",
			sortable: false,
			filterable: false,
			groupable: false
		}]
	});
});
</script>