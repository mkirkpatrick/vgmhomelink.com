<?php 
use Forbin\Library\Classes\Member;
Member::_secureCheck();

if(isset($_SESSION['member_admin']) && $_SESSION['member_admin'] && !isset($_SESSION['member_company'])) {
    redirect("/members/admin-user.php?message=2"); 
}
?>
<div class="provider-info-card module">
	<h4>Claims Professional Info</h4>

	<div class="module-body">
		<ul class="nobullets">
			<li><strong>Name:</strong><br><?= $_SESSION['member_name'] ?></li>
            <?php if(isset($_SESSION['member_title'])) { ?>
            <li><strong>Title:</strong><br><?= $_SESSION['member_title'] ?></li>
            <?php } ?>
            <li><strong>Company:</strong><br><?= $_SESSION['member_company'] ?></li>
			<li><strong>Phone:</strong><br><?= $_SESSION['member_phone'] ?></li>
			<li><strong>Email:</strong><br><?= $_SESSION['member_email'] ?></li>
		</ul>
	</div>
    <br />
    <br />
    <br />
	<br />
	<br />
	<br />
	<br />
	<br />
    <br />
    <br />
    <br />
    <br />
</div>