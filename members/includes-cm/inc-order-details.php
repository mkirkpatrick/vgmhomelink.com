<?php
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

$orderId = isset($_GET['on']) ? $_GET['on'] : null;
$patientId = isset($_GET['pid']) ? $_GET['pid'] : null;
$patientName = isset($_GET['name']) ? $_GET['name'] : null;
if(is_null($orderId)) { 
    redirect("/members/cm-dashboard.php"); 
}
$orderNotes = ViperAPI::getOrderNotes($orderId); 
$orderLineItems = ViperAPI::getOrderLineItems($orderId); 
/* 

[LIId] => 16039664
            [OrderNo] => 5123353
            [ExpectedDelivery] => 2017-02-27T00:00:00
            [Delivery] => 2017-02-27T00:00:00
            [ServiceFrom] => 2017-02-27T00:00:00
            [ServiceTo] => 2017-02-27T00:00:00
            [Quantity] => 1
            [HCPCS] => L3660
            [Modifier] => NU
            [Description] => Shoulder Orthosis Fig 8 Canvas Webbing Prefab
            [UCPrice] => 145.9500
            [UCTotal] => 145.9500
            [InsPrice] => 87.5700
            [InsTotal] => 87.5700
            [DeliverTo] => Home
            [PickupVerified] => SimpleXMLElement Object
                (
                )

            [PurchaseDate] => SimpleXMLElement Object
                (
                )

            [DischargeDate] => SimpleXMLElement Object
                (
                )

            [HCPCSType] => ORTHOTIC
            [LIType] => OANDP
            [NurseStrtCare] => SimpleXMLElement Object
                (
                )

            [PatientId] => 1443893
            [RentSaleIndicator] => Sale */
?>

<div class="order-details-card data-tabs module">
	<ul class="nobullets tabs">
		<li class="tab-link current" data-tab="tab-1">Referral Details</li>
		<li class="tab-link" data-tab="tab-3">Physician Info</li>
		<li class="tab-link" data-tab="tab-4">Notes</li>
	</ul>  
	
	<div id="tab-1" class="tab-content current">
		<div class="grid-row">
			<div class="twelvecolM sixcol--medium fivecol--large fourcol--xl shift-right">
				<div class="supporting-docs-card">
					<h3>Submit Supporting Documentation</h3>
										<p>Please upload any supporting documents to assist in processing.<br><em>Examples: LMN, Authorization, etc.</em></p>
					<div class="notification success" id="SuccessDiv" style="display: none"><h1>Submission Complete</h1><p></p><p>All supporting documentation has been uploaded successfully. </p><p></p></div>
					<div class="uploader-card module alignCenter">
						<img src="/members/images/icon-doc.png" alt="Document Uploader" title="Click Here to upload supporting document(s)." />
						<label for="supporting-doc">Click here to upload supporting document(s). </label>
						<div>
							<div class="demo-section k-content">
								<input name="files" id="files" type="file" />
                                <a href="#" style="margin-top: 10px" class="button silver floatRight" id="submitFilesGateway">Submit Files</a>
							</div>
						</div>

						<script type="text/javascript">
							$(document).ready(function() {
								$("#files").kendoUpload({
									async: {
										saveUrl: "process.php?actionRun=saveSupportingDocuments&pno=<?= $_SESSION['member_api_id'] ?>&pdid=<?= $orderId ?>",
										removeUrl: "process.php?actionRun=deleteSupportingDocuments&pno=<?= $_SESSION['member_api_id'] ?>&pdid=<?= $orderId ?>",
										autoUpload: true
									}, 
									error: function (e) {
										alert(e.XMLHttpRequest.responseText); 
									}
								});
                                
                                $('#submitFilesGateway').on("click", function() { 
                                    $('.uploader-card').hide(); 
                                    $('#SuccessDiv').show(); 
                                }); 
							});
						</script>
					</div>
				</div>
			</div>
			
			<div class="twelvecolM sixcol--medium sevencol--large eightcol--xl">
				<h3>Referral Details</h3>
				<p>Below are the referral details from Referral #<?= $order->OrderNo ?>. You can re-order the same order by using the Button below the details.</p>

				<ul class="order-details-list nobullets">
					<li class="clearfix">
						<strong>Name</strong>
						<span><a href="/members/patient-details.php?pid=<?= $patientId ?>&name=<?= urlencode($patientName) ?>"><?= $patientName ?></a></span>
					</li>

					<li class="clearfix">
						<strong>Referral #</strong>
						<span><?= $order->OrderNo ?> </span>
					</li>

					<li class="clearfix">
						<strong>Homelink Contact</strong>
						<span class="relative">
							<a href="#"><?= $order->PCCName ?></a>
							
							<div class="contact-tooltip">
								<ul class="nobullets">
									<li><strong>Email:</strong> <?= $order->PCCEmail ?></li>
									<li><strong>Phone:</strong> <?= $order->PCC800Number ?></li>
									<li><strong>Fax:</strong> <?= $order->PCCFax ?></li>
								</ul>
							</div>
						</span>
					</li>

					<li class="clearfix">
						<strong>Referral Received Date</strong>
						<span><?= !isNullOrEmpty($order->OrderDate->__toString()) ? date('m/d/Y', strtotime($order->OrderDate)) : '' ?></span>
					</li>

					<li class="clearfix">
						<strong>Referral Billed Date</strong>
						<span><?= !isNullOrEmpty($order->BillSent->__toString()) ? date('m/d/Y', strtotime($order->BillSent)) : '' ?></span>
					</li>

					<li class="clearfix">
						<strong>Claims Professional</strong>
						<span><?= $order->CaseMgrNo ?></span>
					</li>

					<li class="clearfix">
						<strong>Adjuster</strong>
						<span><?= $order->AdjusterNo ?></span>
					</li>

					<li class="clearfix">
						<strong>Authorization #</strong>
                        <span><?= $order->AuthorizationNo ?></span>
					</li>

					<li class="clearfix">
						<strong>Claim #</strong>
                        <span><?= $order->ClaimNo ?></span>
					</li>
                    
					<li class="clearfix">
						<strong>Evaluation Scheduled Date</strong>
                        <span><?= !isNullOrEmpty($order->EvalDate->__toString()) ? date('m/d/Y', strtotime($order->EvalDate)) : '' ?></span>
					</li>

					<li class="clearfix">
						<strong>Expected Delivery Date</strong>
						<span><?= !isNullOrEmpty($order->ExpectedDeliveryDate->__toString()) ? date('m/d/Y', strtotime($order->ExpectedDeliveryDate)) : '' ?></span>
					</li>

					<li class="clearfix">
						<strong>Delivery Date</strong>
						<span><?= !isNullOrEmpty($order->DeliveryDate->__toString()) ? date('m/d/Y', strtotime($order->DeliveryDate)) : '' ?></span>
					</li>
				</ul>

				<a href="https://hmeforms.com/patients-quick-order.aspx" target="_blank" class="button green">Re-Order</a>
				
				<div class="items-services-card">
					<h3>Items / Services</h3>
					<p>Below is a list of all the items / services that were purchased in this order.</p>
                    <?php if(count($orderLineItems) > 0) { ?>
                    	<?php foreach($orderLineItems as $lineItem) { ?>
                    	<div class="items-services-lineitem relative">
     						<h4><?= $lineItem->Description ?></h4>
							<p class="items-services-date">Date of Service: <?= !isNullOrEmpty($lineItem->ServiceTo->__toString()) ? date('m/d/Y', strtotime($lineItem->ServiceTo)) : '' ?></p>
							
							<a href="#" class="button green toggleLink">Show  Product Details</a>
							
							<ul class="details" style="display: none;">
								<li><b>Quantity:</b> <span id="Quantity"><?= $lineItem->Quantity ?></span></li>
								<li><b>Your Contracted Rate Total:</b>$<span id="UCTotal"><?= number_format((double)$lineItem->InsTotal, 2) ?></span></li>      
								<li><b>HCPCS Code:</b> <span id="HCPCS"><?= $lineItem->HCPCS ?></span></li>
								<li><b>Line Type:</b> <span id="LIType"><?= $lineItem->LIType ?></span></li>
								<li><b>Sales /Rent:</b> <span id="SalesRent"><?= $lineItem->RentSaleIndicator ?></span></li>
								<li><b>U&amp;C Price:</b>$<span id="UCPrice"><?= number_format((double)$lineItem->UCTotal, 2) ?></span></li>
							</ul>
						</div>
                		<?php } ?>
					
					<?php } else { ?>
						<div class="message info">No items have been added. </div>
					<?php } ?>
				</div>
			</div>
		</div><!-- eof grid-row -->
	</div>
	
	<div id="tab-3" class="tab-content">
		<h3>Physician Information</h3>
		<p>Below is the Physician information for this referral.</p>
		<ul class="order-details-list nobullets">
			<li class="clearfix">
				<strong>Primary Physician Name</strong>
                <span><?= $order->PrimaryPhysicianName ?></span>
			</li>
			
			<li class="clearfix">
				<strong>Primary Physician Phone</strong>
				<span><?= $order->PrimaryPhysicianPhone ?></span>
			</li>
			
			<li class="clearfix">
				<strong>Primary Physician Fax</strong>
                <span><?= $order->PrimaryPhysicianFax ?></span>
			</li>
			
			<li class="clearfix">
				<strong>Ordering Physician Name</strong>
				<span><?= $order->OrderingPhysicianName ?></span>
			</li>
			
			<li class="clearfix">
				<strong>Ordering Physician Phone</strong>
				<span><?= $order->OrderingPhysicianPhone ?></span>
			</li>
			
			<li class="clearfix">
				<strong>Ordering Physician Fax</strong>
				<span><?= $order->OrderingPhysicianFax ?></span>
			</li>
		</ul>
	</div>
	
	<div id="tab-4" class="tab-content">
		<h3>Notes</h3>
		<p>Below you can view the notes pertaining to this referral.</p>
        <?php if(count($orderNotes) > 0) { ?>
        <ul class="nobullets">
                <?php foreach($orderNotes as $note) { ?>
                <li class="clearfix">
				<p><strong><?= $note->TimeStmp ?> -- <?= $note->Topic ?></strong><br />
                <span><?= $note->Note ?></span></p>
                </li>
                <?php } ?>
		</ul>
		<?php } else { ?>
		<div class="message info">No notes have been added.</div>
        <?php } ?>
	</div>
</div>
<style type="text/css">
ul.details {
    list-style: outside none none;
    margin: 0;
    padding: 0;
}
ul.details li {
    border-bottom: 1px solid #eee;
    color: #434343;
    padding: 7px;
}
ul.details li:first-child, ul.details li:first-child {
    border-top: 1px solid #eee;
}
ul.details li b, ul.details li b {
    display: inline-block;
    width: 300px;
}
</style>

<script type="text/javascript">
$(function() {
    // choose text for the show/hide link - can contain HTML (e.g. an image)
    var showText = 'View Product Details';
    var hideText = 'Hide Product Details';
     
    // initialise the visibility check
    var is_visible = false;
     
    // capture clicks on the toggle links
    $('a.toggleLink').on("click", function(e) {
         e.preventDefault(); 
        // switch visibility
         if($(this).next('ul.details').is(':visible') == true)
         {
            $(this).html(showText);
         }
         else if($(this).next('ul.details').is(':hidden') == true)
         {
            $(this).html(hideText);
         }
         
        // toggle the display - uncomment the next line for a basic "accordion" style
        //$('.toggle').hide();$('a.toggleLink').html(showText);
        $(this).next('ul.details').slideToggle('slow');
    });
});
</script>
