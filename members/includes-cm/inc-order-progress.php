<a href="#" onClick="history.go(-1); return false;" style="width: 100%; margin-bottom: 10px" class="button silver">Back</a>
<?php 
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;
$orderId = isset($_GET['on']) ? $_GET['on'] : null;
$order = ViperAPI::getOrder($orderId);
?>
<div class="provider-info-card order-progress-card module">
	<h4>Referral Progress</h4>
	<div class="module-body">
		<ul class="nobullets order-progress-list">
			<li><?php include($_SERVER['DOCUMENT_ROOT'] . '/members/images/checkmark.svg'); ?> Referral Received</li>
			<li><?php include(showCheckBox($order->AuthorizationNo)) ?> Auth Received</li>
			<li><?php include(showCheckBox($order->VGMNo)) ?> Provider Located</li>
			<li><?php include(showCheckBox($order->EvalDate)) ?> Evaluation Scheduled</li>
			<li><?php include(showCheckBox($order->DeliveryDate)) ?> Delivered</li>
			<li><?php include(showCheckBox($order->BillSent)) ?> Referral Billed</li>
		</ul>
	</div>
</div>
<?php function showCheckBox($value) { 
    return isNullOrEmpty($value->__toString()) ? $_SERVER['DOCUMENT_ROOT'] . '/members/images/checkmark-disabled.svg' : $_SERVER['DOCUMENT_ROOT'] . '/members/images/checkmark.svg'; 
} ?>