<?php 	
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;
$patientId = isset($_GET['pid']) ? $_GET['pid'] : null;
if(is_null($patientId)) { 
    redirect("/members/cm-dashboard.php"); 
}
$patientName = $_GET['name']; 
$orders = ViperAPI::getCaseManagerOrderHistory($_SESSION['member_api_id'], $patientId);
/*$cacheKey = $_SESSION['member_id'] . '_RECENT_ORDERS';
if(!Cache::has($cacheKey)) {
	$orders = getCaseManagerOrderHistory();
	Cache::set($cacheKey, $orders, 7200);
} else {
	$orders = Cache::get($cacheKey); 
} */ 

?>
<div class="patient-details-card module">
	<div class="card-header button-child"><h2>Patient Referral History -- <strong><?= $patientName ?></strong></h2>
    <a href="/members/cm-dashboard.php" class="button silver">Back</a></div>

	<table id="history-grid">
		<thead>
			<tr>
				<th scope="col" data-field="ClaimNumber">Claim #</th>
				<th scope="col" data-field="AuthNumber">Auth #</th>
				<th scope="col" data-field="ServiceDate">Date of Service</th>
				<th scope="col" data-field="ReferralStatus">Status</th>
				<th scope="col" data-field="ReferralType">Referral Type</th>
				<th scope="col" data-field="Details">Details</th>
			</tr>
		</thead>         
		<tbody>
		<?php 
            $count = 0; 
            while(isset($orders->OrderHistory[$count])) { 
                $order = $orders->OrderHistory[$count]; 
                ?>
            <tr>
                <td><?= $order->ClaimNumber ?></td>
                <td><?= $order->AuthNumber ?></td>
                <td><?= date('m/d/Y', strtotime($order->SvcFrom)) ?><?= isset($order->SvcTo) ? ' - ' . date('m/d/Y', strtotime($order->SvcTo)) : '' ?></td>
                <td><?= $order->OrderStatus ?></td>	
                <td><?= $order->OrderType ?></td>
                <td><a href="/members/order-details.php?on=<?= $order->OrderNumber ?>&name=<?= $patientName ?>" class="button green">View</a></td>
            </tr>
        <?php $count++; } ?>
		</tbody>
	</table>
</div>

<script>
	$(document).ready(function () {
		$("#history-grid").kendoGrid({
			dataSource: {
				pageSize: 10
			},
			height: 650,
			scrollable: true,
			navigatable: true,
			groupable: true,
			sortable: true,
			resizable: true,
			filterable: {
				mode: 'row'
			},
			pageable: {
				refresh: true,
				pageSizes: true,
				buttonCount: 5
			},
			columns: [
				{
					title: 'Claim #',
					field: 'ClaimNumber'
				}, {
					title: 'Auth #',
					field: 'AuthNumber'
				}, {
					title: 'Date of Service',
					field: 'ServiceDate'
				}, {
					title: 'Referral Status',
					field: 'ReferralStatus'
				}, {
					title: 'Referral Type',
					field: 'ReferralType'
				}, {
					title: 'Details',
					field: 'Details',
					filterable: false,
					width: 150,
					sortable: false,
					groupable: false
				}
			],
			dataBound: function(e) {
				var grid = $('#history-grid').data('kendoGrid');
				
				grid.tbody.find('tr.k-grouping-row').each(function () {
					grid.collapseGroup(this);
				});
			}
		});
		
		$('#history-grid').data('kendoGrid').thead.find("[data-field=Details]>.k-header-column-menu").remove();
	});
</script>