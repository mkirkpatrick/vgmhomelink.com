<nav class="mobilemenu mobilemenu-dashboard">
	<ul class="mobile-nav nobullets">
		<li class="mobile-nav-user">
			<a href="/members/account.php">John Smith</a>
			
			<ul class="mobile-subav nobullets">
				<li><a href="/members/account">My Account</a></li>
				<li><a href="/members/messages">16 Messages</a></li>
				<li><a href="/members/messages">8 Alerts</a></li>
				<li><a href="/members/messages">Logout</a></li>
			</ul>
		</li>
		<li><a href="/members/cm-dashboard.php">Dashboard</a></li>
		<li><a href="https://www.hmeforms.com" target="_blank">Forms</a></li>
		<li><a href="https://hmeforms.com/case-managers-quick-referral" target="_blank">Submit Referral</a></li>
		<li><a href="http://www.homelinku.com" target="_blank">Homelink University</a></li>
		<li><a href="https://vgmhomelink.forbinhosting.net/members/cm-dashboard.php#" target="_blank">Request Product Information</a></li>
		<li><a href="/">Back to Homepage</a></li>
	</ul>
</nav>