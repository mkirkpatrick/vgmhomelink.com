<?php include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgtop.php'; ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-nav.php'); 
if(!(isset($_SESSION['member_multi']))) {
    redirect("/members/provider-dashboard.php"); 
}
?>

<main class="dashboard">
	<div class="container"> 
		<div class="provider-number-card module">
		
		<?php if(isset($_GET['message']) && $_GET['message'] == 1) { ?>
<div class="message error" style="background: #CB5456;"><center><span style="font-size: 16px">You have selected an invalid provider number! </span></center></div>
<?php } ?>
		<form id="providerForm" method="post" action="process.php">
			<input type="hidden" name="actionRun" value="changeMultiProvider" />
			<div class="card-header"><h2>Select Branch Number</h2></div>

			<label for="cboProviderNumber">Branch Number</label>
                    <select id="cboProviderNumber" name="cboProviderNumber" required="required">
                        <option value="">-- SELECT --</option>
                    <?php 
                    $providerNumbers = explode('|', $_SESSION['member_multi']); 
                    foreach ($providerNumbers as $prov) {
                        echo '<option value="' . $prov . '">' . $prov . '</option>' . PHP_EOL;
                    } ?>
                    </select>
			<input type="submit" value="Submit" class="button green" />
		</div>
	</div>
</main>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgbot.php'; ?>