<?php 
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");

if(isset($_GET['logout'])) { 
    session_destroy(); 
    redirect("/member/login"); 
}
    
	header("Location: /member"); 
?>