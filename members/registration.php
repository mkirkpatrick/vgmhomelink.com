<?php
global $_WEBCONFIG, $siteConfig;

$title = "Online Registration"; 
$jScript = '<script type="text/javascript" src="/jquery/jquery.form.js"></script>'; ?>
<link rel="stylesheet" href="/members/css/wizard.css" type="text/css" media="print, projection, screen" />
<section id="content" class="cf">
	<div class="container">
			<? if(isset($_GET['success'])) { ?>
			
			<h2 class="big-span-text" style="margin-top:.5em">Thank You for Registering!</h2>
			<h3>A HOMELINK associate will review your registration and connect with you in 2-3 business days.</h3>
			
			
			<? } else {
				include("includes/inc-registration.php");
			}?>
	</div><!-- eof container -->
</section>