<?php include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgtop.php'; ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/members/includes-pv/inc-nav.php'; 
if(!(isset($_SESSION['member_admin']) && $_SESSION['member_admin'])) {
    redirect("/members/provider-dashboard.php"); 
}
?>

<main class="dashboard">
	<div class="container"> 
		<div class="provider-number-card module">
		
		<?php if(isset($_GET['message']) && $_GET['message'] == 1) { ?>
<div class="message error" style="background: #CB5456;"><center><span style="font-size: 16px">You have selected an invalid provider number! </span></center></div>
<?php } ?>
		<form id="providerForm" method="post" action="process.php">
			<input type="hidden" name="actionRun" value="changeProvider" />
			<div class="card-header"><h2>Select Provider</h2></div>

			<label for="providerNumber">Provider Number</label>
			<input type="text" id="providerNumber" name="txtProviderNumber" value="<?= $_SESSION['member_prov_num'] ?>" />

			<input type="submit" value="Submit" class="button green" />
		</div>
	</div>
</main>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/includes/inc-pgbot.php'; ?>