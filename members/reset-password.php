<?php
global $_WEBCONFIG;
require_once $_SERVER['DOCUMENT_ROOT'] . '/VPanel/library/classes/class_form.php';
loadLocalConfig($_SERVER['DOCUMENT_ROOT'] . "/members");

addHtmlTag("/css/parsley.css","css","internal",true);
addHtmlTag(array("/members/css/members.css"),"css");
addHtmlTag(array("/scripts/parsley.min.js","/members/js/pstrength.js","/members/js/reset.js"),"javascript");

if ($form->getNumErrors() > 0) {
    $errors = $form->getErrorArray();

    foreach ($errors as $err) {
        echo $err;
    }
}

$resetLink = isset($_GET['link']) ? $_GET['link'] : "";

$sql = "SELECT m_id, m_reset_link_date
FROM tbl_members
WHERE m_reset_link = '$resetLink' AND m_status <> 'Inactive' AND m_status <> 'Deleted'
LIMIT 1";
$record = Database::Execute($sql);

if($record->Count() > 0) {
    $record->MoveNext();
    $iNum = $record->m_id;
    $date = new DateTime($record->m_reset_link_date);
    $beginDate =  date_timestamp_get($date);

    $date2 = new DateTime($record->m_reset_link_date);
    $date2->add(new DateInterval('P1D'));
    $endDate =  date_timestamp_get($date2);

    if (time() > $endDate ) { ?>
        <div class="message error"><p>Your Reset Password Link has Expired.</p></div>
        <div id="ForgotPassword">

            <h2>Forget Your Password?</h2>
            <p>Enter the e-mail associated with your account.</p>

            <form action="/members/process.php" method="post" enctype="multipart/form-data" id="forgotPasswordForm"  name="forgotPasswordForm" autocomplete="off">
                <input id="actionRun" name="actionRun" type="hidden" value="ForgotPassword">
                <?= loadRC() ?>
                <ul class="quickform clearfix nobullets">
                    <li class="twelvecol">
                        <label class="required" for="txtEmail">Email Address</label>
                        <input type="email" id="txtEmail" name="txtEmail" value="" size="50" maxlength="255" required="required" data-parsley-required-message="Enter an email address." />
                    </li>
                </ul>

                <input class="contactForm button blue" type="submit" id="forgotPasswordSubmit" value="Reset Passowrd">
                <a class="contactForm button red" href="/<?= $_WEBCONFIG['MEMBERS_SLUG']; ?>/login">Cancel</a>
            </form>
        </div>
        <?
    } else {  ?>
        <div class="reset-pw current-members-box">
            <div class="message notification warning"><p>To reset your password, please complete the following form.</p></div>

            <form id="secureForm" method="post" action="/members/process.php" autocomplete="off">
                <?= loadRC() ?>
                <input type="hidden" name="actionRun" id="actionRun" value="resetPassword" />
                <input type="hidden" name="hidId" id="hidId" value="<?= $resetLink ?>" />
                <input type="hidden" name="hidId2" id="hidId2" value="<?= $iNum ?>" />

                <ul class="reset-pw-form clearfix nobullets">
                    <li>
                        <label class="required" for="txtPassword">New Password</label>
                        <input class="pwStrength text" type="password" id="txtPassword" name="txtPassword" required="required" title="Password" data-parsley-required-message="Enter a password.">

                        <div class="password-reqs">
							<p class="password-reqs-title"><strong>Password Requirements</strong></p>
							<ul class="password-reqs-list nobullets">
								<li id="lengthLi">At least <?= $_WEBCONFIG['MIN_PASSWORD_LENGTH']; ?> characters</li>
								<li id="upperCaseLi">At least one capital letter</li>
								<li id="lowerCaseLi">At least one lowercase letter</li>
								<li id="numberLi">At least one number</li>
								<li id="specialCharLi">At least one special character (<em> ! @ # $ % ^ &amp; % * ? _ ~ </em>)</li>
							</ul>
						</div>
                    </li>

                    <li>
                        <label class="required" for="txtConfirmPassword">Re-Enter New Password</label>
                        <input type="password" class="text" id="txtConfirmPassword" name="txtConfirmPassword" required="required" title="Confirm Password" data-parsley-required-message="Re-enter password.">
                        <div id="validationMessage" style="padding-top: 5px; color: #FF0000;"></div>
                    </li>

                    <li>
                        <input class="button blue" id="btnSubmit" type="submit" value="Save New Password" >
                    </li>
                </ul>
            </form>
        </div>
        <?
    }
} else { ?>
    <div class="message error alignCenter"><p style="font-size: 2rem;">Your Reset Password Link is Invalid.</p></div>
    <div class="alignCenter"><a href="/<?= $_WEBCONFIG['MEMBERS_SLUG'] ?>/login" class="button blue">Back to <?= isset($_WEBCONFIG['MEMBERS_SECTION_NAME']) ? $_WEBCONFIG['MEMBERS_SECTION_NAME'] : '' ?> Login</a></div>
    <?php
}