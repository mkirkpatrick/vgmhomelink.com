<?php
	addHtmlTag('/members/marketing/css/styles.css', 'css');
	addHtmlTag(array('/members/marketing/js/marketing.js'), 'javascript');
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/jquery.flexslider.min.js"></script>
<div class="fixed-items module">
	<section class="marketing item">
		<div class="flexslider clearfix">
			<ul class="slides nobullets">
			<?php
				$page = isset($_SERVER['REQUEST_URI']) && strlen($_SERVER['REQUEST_URI']) > 0 ? xss_sanitize($_SERVER['REQUEST_URI']) : 'H';
                $whereString = $page == '/members/cm-dashboard.php' ? " AND mk_display_page LIKE '%CM%'" : " AND mk_display_page LIKE '%P%'";
                
                $uploadFolder = '/uploads/marketing/';
				$sql = "SELECT * FROM tbl_marketing WHERE mk_status = 'Active'$whereString ORDER BY mk_sort_id, mk_name LIMIT 15";
				$record	= Database::Execute($sql);
				if ($record->Count() > 0) {
					while ($record->MoveNext()) { ?>
						<li style="background-image: url(<?= $uploadFolder . $record->mk_image ?>);">
							<div class="marketing-text relative">
								<h2 class="mk-title"><?= $record->mk_name ?></h2>
								<p class="mk-content"><?= $record->mk_tagline ?></p>
								
								<a href="<?= $record->mk_url ?>" class="button green">Learn More</a>
							</div>
					   </li>
				   <?
					}// end while
				}
			?>
			</ul>
		</div>
	</section>
</div>