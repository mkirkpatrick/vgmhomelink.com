$(window).on("load", function() {
    $('.flexslider').flexslider({
        animation: "fade",
		controlNav: true,
        directionNav: false,
        pauseOnHover: true,
        pauseOnAction: false
    });
});