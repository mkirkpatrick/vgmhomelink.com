<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_form.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "library/classes/class_vmail.php");
require_once filePathCombine($_SERVER['DOCUMENT_ROOT'],  "/vpanel/library/classes/class_security.php");
use Forbin\Library\Classes\Member;
use Forbin\Members\Library\ViperAPI;

class process extends Database {

	private $record;
	private $addressRecord;
	private $table;
	private $table_addr;
	private $siteConfig;

	//------------------------------
	public function __construct($table, $table_addr) {
		//------------------------------
		global $_WEBCONFIG, $siteConfig;
		parent::__construct();
		$this->table		= $table;
		$this->table_addr	= $table_addr;
		$this->siteConfig   = $siteConfig;
	}

	public function _getSecureToken() {
		if(isset($_SESSION['member_secure_token'])) {
			print $_SESSION['member_secure_token'];
		}
		exit;
	}

	//------------------------------
	public function _Register() {
	//------------------------------
		global $_WEBCONFIG, $form, $siteConfig;

        $_SESSION['VALIDATION_RESPONSE'] = '';

		$this->record = new Entity($this->table);
		$this->addressRecord = new Entity($this->table_addr);
		$this->addressRecord->ma_status = "P";
		$isError	= false;

		$fieldName	= "MembersFirst";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$isError = true;
			$_SESSION['VALIDATION_RESPONSE'] .= "<script>$('#{$fieldName}').addClass('error');</script>";
			$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors" id="message">Please enter your first name.</div>';
		} else {
			$this->record->m_displayname = ucwords($fieldValue);
		}

		$fieldName	= "MembersLast";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$isError = true;
			$_SESSION['VALIDATION_RESPONSE'] .="<script>$('#{$fieldName}').addClass('error');</script>";
			$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors" id="message">Please enter your last name.</div>';
		} else {
			$this->record->m_displayname .= ' ' . ucwords($fieldValue);
		}

		$fieldName	= "MembersEmail";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$isError = true;
			$_SESSION['VALIDATION_RESPONSE'] .="<script>$('#{$fieldName}').addClass('error');</script>";
			$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors" id="message">Please enter Email address.</div>';
		} else if (strcmp($fieldValue, $_POST["MembersEmail2"]) != 0) {
			$isError = true;
			$_SESSION['VALIDATION_RESPONSE'] .="<script>$('#{$fieldName}').addClass('error');</script>";
			$_SESSION['VALIDATION_RESPONSE'] .="<script>$('#MembersEmail2').addClass('error');</script>";
			$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors" id="message">Email and Verify Email do not match.</div>';
		} else {
			$sql = "SELECT m_id FROM {$this->table} WHERE m_username = '" . addslashes($fieldValue) . "'";
			$dt  = Database::Execute($sql);
			if ($dt->Count() > 0) {
				$isError = true;
				$_SESSION['VALIDATION_RESPONSE'] .="<script>$('#{$fieldName}').addClass('error');</script>";
				$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors" id="message">Email already exists in our database. Use the Forgot Password link on the login page to reset your password. </div>';
			} else {
				$this->record->m_username = $fieldValue;
			}
		}

        $fieldName  = "ProviderNumber";
        $fieldValue    = trim(strip_tags($_POST[$fieldName]));
        if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
            $this->record->m_provider_number = null;
            $this->record->m_role = 'Claims Professional';
        } else {
            $this->record->m_provider_number = $fieldValue;
            $this->record->m_role = 'Provider';
        }

        $passwordLength = $_WEBCONFIG['PASSWORD_LENGTH'];
		$fieldName	= "MembersPass";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			if (strcasecmp($_REQUEST['actionRun'], "add") == 0) {
				$isError = true;
				$_SESSION['VALIDATION_RESPONSE'] .="<script>$('#{$fieldName}').addClass('error');</script>";
				$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors" id="message">Please choose a password. (Minimum: ' . $passwordLength .' characters)</div>';
			}
		} else if (strlen($fieldValue) < $passwordLength) {
			$isError = true;
			$_SESSION['VALIDATION_RESPONSE'] .="<script>$('#{$fieldName}').addClass('error');</script>";
			$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors" id="message">Password is too short. (Minimum: ' . $passwordLength .' characters)</div>';
		} else if (!isStrongPassword($fieldValue)) {
            $isError = true;
            $_SESSION['VALIDATION_RESPONSE'] .="<script>$('#{$fieldName}').addClass('error');</script>";
			$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors" id="message">Password must contain at least one lower case letter, one upper case letter, one special character, and one digit. (Minimum: 8 characters)</div>';
		} else if (strcmp($fieldValue, $_POST["MembersPass2"]) != 0) {
			$isError = true;
			$_SESSION['VALIDATION_RESPONSE'] .="<script>$('#{$fieldName}').addClass('error');</script>";
			$_SESSION['VALIDATION_RESPONSE'] .="<script>$('#MembersPass2').addClass('error');</script>";
			$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors" id="message">Password and Verify Password do not match.</div>';
		} else {
			$newSalt = member::_generateRandomSalt(24);
            $this->record->m_password_salt = $newSalt;
            $this->record->m_password = member::_hashPassword($fieldValue, $newSalt);
		}

        $password = $this->record->m_password;

        $this->record->m_status = 'Pending';

		$this->addressRecord->ma_firstname		= strip_tags($_POST['MembersFirst']);
		$this->addressRecord->ma_lastname		= strip_tags($_POST['MembersLast']);
        $this->addressRecord->ma_business		= strip_tags($_POST['CompanyName']);
//		$this->addressRecord->ma_country		= strip_tags($_POST['MembersCountry']);
//		$this->addressRecord->ma_address1		= strip_tags($_POST['MembersAddress']);
//		$this->addressRecord->ma_address2		= strip_tags($_POST['MembersAddress2']);
//		$this->addressRecord->ma_city			= strip_tags($_POST['MembersCity']);
//		$this->addressRecord->ma_state			= strip_tags($_POST['MembersState']);
//		$this->addressRecord->ma_postal_code	= strip_tags($_POST['MembersZip']);
		$this->addressRecord->ma_phone			= strip_tags($_POST['MembersPhone']);
		$this->addressRecord->ma_ext			= strip_tags($_POST['MembersExtension']);

//		if (strlen($this->addressRecord->ma_country = trim($this->addressRecord->ma_country)) == 0) {
//			$isError = true;
//			$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors">Please select residing country</div>';
//		} else {
//
//			if (strlen($this->addressRecord->ma_address1 = trim($this->addressRecord->ma_address1)) == 0) {
//				$isError = true;
//				$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors">Please enter street address</div>';
//			}
//			if (strlen($this->addressRecord->ma_city = trim($this->addressRecord->ma_city)) == 0) {
//				$isError = true;
//				$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors">Please enter city name</div>';
//			}
//			if (strlen($this->addressRecord->ma_state = trim($this->addressRecord->ma_state)) == 0) {
//				$isError = true;
//				$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors">Please enter state/providence name</div>';
//			}
//			if (strlen($this->addressRecord->ma_postal_code = trim($this->addressRecord->ma_postal_code)) == 0) {
//				$isError = true;
//				$_SESSION['VALIDATION_RESPONSE'] .='<div class="errors">Please enter zip / postal code</div>';
//			}
//		}

		if($isError) {
			$_SESSION['VALIDATION_RESPONSE'] .="<script>displayForm = true;</script>";
            redirect($_SERVER['HTTP_REFERER'] . "#message");
		}

		$this->record->m_regdate = date("Y-m-d G:i:s");
		$newId = $this->record->insert();

        $sql = "INSERT INTO `tbl_members_password` (`m_id`,`mp_password`, `mp_salt`) VALUES ($newId, '$password', '$newSalt')";
        Database::ExecuteRaw($sql);
		$this->addressRecord->m_id = $newId;
		$this->addressRecord->insert();

		$message = '';
		$message = '';
		$message = "<div style=\"margin-top: 25px; margin-bottom: 25px; background-color: #eeeeee;\">
					<table width=\"700\" align=\"center\" style=\"max-width: 700px; background-color: #ffffff;\" cellpadding=\"0\" cellspacing=\"30\" border=\"0\"><tbody><tr><td>
					<h1 style=\"font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; color: #1e4288; font-weight: bold;\">Welcome to {$this->siteConfig['Company_Name']}</h1>
					<h2 style=\"font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; color: #82c341; font-size: 18px; line-height: 21px; font-weight: bold;\">Thank You for creating an account with us!</h2>
					<p style=\"font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; color: #5e5e5e; font-size: 15px; line-height: 21px;\">We will be in touch with you in the next 3-5 Business Days with your confirmed login information.</p>
					</td></tr></tbody><table></div>";
		$message = wordwrap($message, 70);
		$headers = 'From: ' .  $this->siteConfig['Default_From_Email'] . "\r\n" .
            'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
			'Reply-To: ' .  $this->siteConfig['Default_Reply_To_Email'] . "\r\n" .
			'X-Mailer: PHP/' . phpversion();

		mail($_POST['MembersEmail'], $this->siteConfig['Company_Name'], $message, $headers);


        $mailer = new vMail();
        $mailer->addRecipient("homelinkclaimstatus@vgm.com");
        $mailer->setSubject("New Portal User Registration Received");
        $mailer->setMailType("html");
        $mailer->setFrom($siteConfig['Default_From_Email'], "VGM Homelink Website");
        $mailer->setReplyTo($siteConfig['Default_Reply_To_Email'], "VGM Homelink Website");

        $body = "<a href='https://www.vgmhomelink.com/VPanel/members/index.php?view=modify&id=$newId'>View & Approve Portal User</a>";

        $mailer->setMessage($body);
        $mailer->sendMail();

		redirect("/members/registration?success=true#message");
		exit;
	}

	//------------------------------
	public function _Login() {
	//------------------------------
		$error = urlencode(member::_secureUnlock());
		redirect("/member/login?errMsg=$error");
	}

	//------------------------------
	public function _UpdateAccount() {
	//------------------------------
		global $_WEBCONFIG, $form;
		$iNum = (int)$_POST['hidId'];
		$this->record = Repository::getById($this->table, $iNum);

		$fieldName	= "MembersDisplayName";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Display Name is required.");
		} else {
			$this->record->m_displayname = ucwords($fieldValue);
		}

		$fieldName	= "MembersEmail";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Email address is required.");
		} else {
			$sql = "SELECT m_id FROM {$this->table} WHERE m_id <> {$_POST['hidId']} AND m_username = '" . addslashes($fieldValue) . "'";
			$dt  = Database::Execute($sql);
			if ($dt->Count() > 0) {
				$form->setError($fieldName, "* Email already exists in our database.");
			} else {
				$this->record->m_username = $fieldValue;
			}
		}

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
		} else {
			// No Errors
			$this->record->update();
			$_SESSION['processed'] = 'updated';
		}

		redirect("update-account");
	}

	//------------------------------
	public function _ChangePassword() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$iNum = (int)$_POST['hidId'];
		$this->record = Repository::getById($this->table, $iNum);
        $newSalt = Security::_generateRandomSalt(24);
        $oldPass = '';

		$fieldName	= "MembersPassOrig";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Old Password is required.");
		} else {
			$sql = "SELECT m_password, m_password_salt FROM {$this->table} WHERE m_id = $iNum LIMIT 1";
			$dt  = Database::Execute($sql);
			if ($dt->Count() > 0) {
				$dt->MoveNext();
                $comparePass = strcasecmp($dt->m_password, Security::_hashPassword($fieldValue, $dt->m_password_salt)) != 0;
                if ($comparePass == 0) {
                    $oldPass = $dt->m_password;
                } else {
                    $form->setError($fieldName, "* Old Password is NOT valid.");
                }

			}
		}

        $passwordLength = $_WEBCONFIG['PASSWORD_LENGTH'];
        $historyCount = $_WEBCONFIG['PASSWORD_HISTORY_REQUIREMENT'];

		$fieldName	= "MembersPass";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			if (strcasecmp($_REQUEST['actionRun'], "add") == 0) {
				$form->setError($fieldName, "* Password is required.");
			}
		} else if (strlen($fieldValue) < $passwordLength) {
			$form->setError($fieldName, "** Password is less than $passwordLength characters.");
		} else if (strcmp($fieldValue, $_POST["MembersPass2"]) != 0) {
			$form->setError($fieldName, "** Passwords are not matching.");
		} else if (!isStrongPassword($fieldValue)) {
			$form->setError($fieldName, "** Password must contain at least one lower case letter, one upper case letter, and one digit.");
		} else if(self::_usedPreviousMemberPassword($iNum, $fieldValue)) {
			$form->setError($fieldName, "** You cannot use any of your previous passwords used within the past $historyCount months.");
		} else {
			$this->record->m_password = Security::_hashPassword($fieldValue, $newSalt);
            $this->record->m_password_salt = $newSalt;
		}
        $password = $this->record->m_password;
        $this->record->m_last_password_change = date("Y-m-d G:i:s");

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
		} else {
			// No Errors
			$this->record->update();
            $sql = "INSERT INTO `tbl_members_password` (`m_id`,`mp_password`, `mp_salt`) VALUES ($iNum, '$password', '$newSalt')";
			Database::ExecuteRaw($sql);
            $_SESSION['member_require_password'] = false;
			$_SESSION['processed'] = 'updated';
		}

		header("Location: /members/change-password.php");
	}

    //----------------------------------------------------------
	private function _usedPreviousPassword($iNum, $password) {
    //----------------------------------------------------------
        global $_WEBCONFIG;

        $historyCount = $_WEBCONFIG['PASSWORD_HISTORY_REQUIREMENT'];
		$sql = "SELECT `mp_password`,`mp_salt`
				FROM `tbl_members_password`
				WHERE `m_id` = $iNum
				ORDER BY `mp_datetime` DESC
				LIMIT $historyCount";
		$record = Database::Execute($sql);
		while($record->MoveNext()) {
			$hashedPassword = Security::_hashPassword($password, $record->mp_salt);
			if($record->mp_password == $hashedPassword) {
				return true;
			}
		}
		return false;
	}

    //------------------------------
    public function _forgotPassword() {
    //------------------------------
        global $_WEBCONFIG;

        if(validateRC()) {
            echo member::_ForgotPass();
        } else {
            echo 'Unknown Error Occurred';
        }
        exit;
    }

	//------------------------------
	public function _NewAddress() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$this->record = new Entity($this->table_addr);
		self::_VerifyAddress();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			header("Location: /members/address-book/add-address?view=add");
		} else {
			// No Errors
			$this->record->m_id = $_SESSION['member_id'];
			$this->record->ma_date = date("Y-m-d G:i:s");
			$this->record->insert();
			$_SESSION['processed'] = 'added';
			header("Location: /members/address");
		}
	}

	//------------------------------
	public function _UpdateAddress() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$this->record = new Entity($this->table_addr);
		self::_VerifyAddress();

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			header("Location: /members/address-book/modify-address?view=modify");
		} else {
			// No Errors
			$this->record->ma_id = (int)$_POST['hidId'];
			$this->record->update();
			$_SESSION['processed'] = 'updated';
			header("Location: /members/address");
		}
	}

	//------------------------------
	public function _changeProvider() {
	//------------------------------
        Member::_secureCheck();
		if($_SESSION['member_id'] == 0) {
			$_SESSION['member_prov_num'] = strtoupper($_POST['txtProviderNumber']);
		}
		header("Location: /members/provider-dashboard.php");
	}

    //------------------------------
	public function _changeMultiProvider() {
	//------------------------------
        Member::_secureCheck();
		if(isset($_SESSION['member_multi']) && stripos($_SESSION['member_multi'], $_POST['cboProviderNumber']) !== false) {
			$_SESSION['member_prov_num'] = strtoupper($_POST['cboProviderNumber']);
		}
		header("Location: /members/provider-dashboard.php");
	}

    //------------------------------
    public function _changeCaseManager() {
    //------------------------------

        Member::_secureCheck();

        $user = Database::quote_smart($_POST['cboUsername']);
        $userInfo = ViperAPI::getCaseManagerLoginInfo($user, '~');
        if($userInfo->Status == 'A') {
            $_SESSION['member_id'] 			= 0;
            $_SESSION['member_api_id']      = (int) $userInfo->UserLoginId[0];
            $_SESSION['member_name']        = (string) $userInfo->FirstName[0] . ' ' . $userInfo->LastName[0];
            $_SESSION['member_username']    = (string) $userInfo->UserLogin[0];
            $_SESSION['member_email']       = (string) $userInfo->UserLogin[0];
            $_SESSION['member_type'] 	    = 'Case Manager';
            $_SESSION['member_title'] 	    = (string) $userInfo->Title;
            $_SESSION['member_company'] 	= (string) $userInfo->Company;
            $_SESSION['member_phone'] 	    = (string) $userInfo->PrimaryPhone;
            $_SESSION['member_role'] 	    = $userInfo->IsSupervisor == 'Y' ? 1 : 0;
            redirect('/members/cm-dashboard.php');
        } else {
            redirect('/members/admin-user.php?message=1');
        }
    }

	//------------------------------
	public function _saveSupportingDocuments() {
	//------------------------------
        global $siteConfig;

        Member::_secureCheck();

        $pId = isset($_REQUEST['pdid']) ? $_REQUEST['pdid'] : 0;
        $pNumber = $_REQUEST['pno'] ? $_REQUEST['pno'] : 0;
		if(isset($_FILES["files"]) && $_FILES["files"]["error"] == 0) {
			$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png",
			"pdf" => "application/pdf");
			$filename = $_FILES["files"]["name"];
			$filetype = $_FILES["files"]["type"];
			$filesize = $_FILES["files"]["size"];
            $file_ext   = strtolower(getExtName($_FILES["files"]["name"]));
            $newFileName = md5(rand() * time()) . ".{$file_ext}";

			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format for the following file " . $_FILES["files"]["name"]);

			// Verify file size - 20MB maximum

			$maxsize = 20 * 1024 * 1024;

            $mailer = new vMail();
            $mailer->addRecipient("Homelinkfaxedorders@vgm.com");
            $mailer->setSubject("Supporting Documentation Submitted -- Provider Number: $pNumber -- Reference ID -- $pId");
            $mailer->setMailType("html");
            $mailer->setFrom($siteConfig['Default_From_Email'], "VGM Homelink Website");
            $mailer->setReplyTo($siteConfig['Default_Reply_To_Email'], "VGM Homelink Website");

			if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
			if(in_array($filetype, $allowed)) {
                move_uploaded_file($_FILES["files"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/uploads/member/" . $newFileName);
                $mailer->attachfile($_SERVER['DOCUMENT_ROOT'] . "/uploads/member/" . $newFileName, $filetype);
                $fileRecord = new Entity('tbl_files');
                $fileRecord->f_provider_no = $pNumber;
                $fileRecord->f_filename = $newFileName;
                $fileRecord->f_display_filename = $_FILES["files"]["name"];
                $fileRecord->f_file_path = $_SERVER['DOCUMENT_ROOT'] . "/uploads/member/";
                $fileRecord->f_content_type = $filetype;
                $fileRecord->f_content_length = $filesize;
                $fileRecord->f_date_added = date("Y-m-d G:i:s");
                $newId = $fileRecord->insert();

			} else {
				die("Error: There was a problem uploading your file(s). Please try again.");
			}
            $body = "Supporting Document Received";

            $mailer->setMessage($body);
            $mailer->sendMail();
            exit;
		} else {
			die("Error: " . $_FILES["files"]["error"]);
		}
	}

    //------------------------------
	public function _deleteSupportingDocuments() {
	//------------------------------
        exit;
	}
    //----------------------------------------------------------
    private function _usedPreviousMemberPassword($iNum, $password) {
		//----------------------------------------------------------
			$sql = "SELECT `mp_password`,`mp_salt`
					FROM `tbl_members_password`
					WHERE `m_id` = $iNum AND mp_datetime > DATE_SUB(NOW(), INTERVAL 24 MONTH)
					ORDER BY `mp_datetime` DESC";
			$record = Database::Execute($sql);
			while($record->MoveNext()) {
				$hashedPassword = Security::_hashPassword($password, $record->mp_salt);
				if($record->mp_password == $hashedPassword) {
					return true;
				}
			}
			return false;
		}

	//------------------------------
	private function _VerifyAddress() {
	//------------------------------
		global $_WEBCONFIG, $form;

		$fieldName	= "MembersFirst";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* First Name is required.");
		} else {
			$this->record->ma_firstname = ucwords($fieldValue);
		}

		$fieldName	= "MembersLast";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Last Name is required.");
		} else {
			$this->record->ma_lastname = ucwords($fieldValue);
		}

		$fieldName	= "MembersBusiness";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$this->record->ma_business = '';
		} else {
			$this->record->ma_business = ucwords($fieldValue);
		}

		$fieldName	= "MembersCountry";
		$fieldValue	= $_POST[$fieldName];
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Country is required.");
		} else {
			$this->record->ma_country = $fieldValue;
		}

		$fieldName	= "MembersAddress";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* Street Address is required.");
		} else {
			$this->record->ma_address1 = ucwords($fieldValue);
		}

		$fieldName	= "MembersAddress2";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$this->record->ma_address2 = '';
		} else {
			$this->record->ma_address2 = ucwords($fieldValue);
		}

		$fieldName	= "MembersCity";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
			$form->setError($fieldName, "* City / Town Name is required.");
		} else {
			$this->record->ma_city = ucwords($fieldValue);
		}

		$fieldName	= "MembersWebsite";
		$fieldValue	= trim(strip_tags($_POST[$fieldName]));
		if ($fieldValue || strlen($fieldValue = trim($fieldValue)) > 0) {
			$this->record->ma_website = $fieldValue;
		}

		if (strcasecmp($this->record->ma_country, "United States of America") == 0) {

			$fieldName	= "MembersState";
			$fieldValue	= trim(strip_tags($_POST[$fieldName]));
			if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
				$form->setError($fieldName, "* State / Providence Name is required.");
			} else {
				$flag = true;
				$doc = new DOMDocument();
				$doc->load("../library/xml/states.xml");
				$states	= $doc->getElementsByTagName( "state" );

				foreach( $states as $stateName ) {
					$name	= $stateName->getAttribute('name');
					$abbr	= $stateName->getAttribute('abbreviation');
					if (strcasecmp($fieldValue, $name) == 0 || strcasecmp($fieldValue, $abbr) == 0) {
						$fieldValue = $abbr;
						$flag = false;
					}
				}

				if ($flag) {
					$form->setError($fieldName, "* State / Providence Name is NOT valid.");
				} else {
					$this->record->ma_state = $fieldValue;
				}
			}

			$fieldName	= "MembersZip";
			$fieldValue	= trim(strip_tags($_POST[$fieldName]));
			if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
				$form->setError($fieldName, "* Zip / Postal Code is required.");
			} else if (strlen($fieldValue) < 5) {
				$form->setError($fieldName, "* Zip / Postal Code is NOT valid.");
			} else {
				$this->record->ma_postal_code = $fieldValue;
			}

			$fieldName	= "MembersPhone";
			$fieldValue	= trim(strip_tags($_POST[$fieldName]));
			if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
				$this->record->ma_phone = '';
			} else {
				$regex = '/^(?:1(?:[. -])?)?(?:\((?=\d{3}\)))?([2-9]\d{2})'
						.'(?:(?<=\(\d{3})\))? ?(?:(?<=\d{3})[.-])?([2-9]\d{2})'
						.'[. -]?(\d{4})(?: (?i:ext)\.? ?(\d{1,5}))?$/';

				if(preg_match($regex, $fieldValue, $matches)) {
					$this->record->ma_phone = "($matches[1]) $matches[2]-$matches[3]";
				} else {
					$form->setError($fieldName, "* Phone Number is NOT valid.");
				}
			}

			$fieldName	= "MembersFax";
			$fieldValue	= trim(strip_tags($_POST[$fieldName]));
			if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
				$this->record->ma_fax = '';
			} else {
				$regex = '/^(?:1(?:[. -])?)?(?:\((?=\d{3}\)))?([2-9]\d{2})'
					.'(?:(?<=\(\d{3})\))? ?(?:(?<=\d{3})[.-])?([2-9]\d{2})'
					.'[. -]?(\d{4})(?: (?i:ext)\.? ?(\d{1,5}))?$/';

				if(preg_match($regex, $fieldValue, $matches)) {
					$this->record->ma_fax = "($matches[1]) $matches[2]-$matches[3]";
				} else {
					$form->setError($fieldName, "* fax Number is NOT valid.");
				}
			}

		} else {

			$fieldName	= "MembersState";
			$fieldValue	= trim(strip_tags($_POST[$fieldName]));
			if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
				$form->setError($fieldName, "* State / Providence Name is required.");
			} else {
				$this->record->ma_state = $fieldValue;
			}

			$fieldName	= "MembersZip";
			$fieldValue	= trim(strip_tags($_POST[$fieldName]));
			if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
				$form->setError($fieldName, "* Zip / Postal Code is required.");
			} else {
				$this->record->ma_postal_code = $fieldValue;
			}

			$fieldName	= "MembersPhone";
			$fieldValue	= trim(strip_tags($_POST[$fieldName]));
			if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
				$this->record->ma_phone = '';
			} else {
				$this->record->ma_phone = $fieldValue;
			}

			$fieldName	= "MembersFax";
			$fieldValue	= trim(strip_tags($_POST[$fieldName]));
			if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
				$this->record->ma_fax = '';
			} else {
				$this->record->ma_fax = $fieldValue;
			}

			$fieldName	= "MembersExtension";
			$fieldValue	= trim(strip_tags($_POST[$fieldName]));
			if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
				$this->record->ma_business = '';
			} else {
				$this->record->ma_ext = $fieldValue;
			}
		}
	}
	//---------------------------------
	public function _ResetPassword() {
	//---------------------------------
		global $_WEBCONFIG, $siteConfig, $form;

		if(validateRC()) {

			$resetLink = isset($_POST['hidId']) ? $_POST['hidId'] : "";
			$iNum = isset($_POST['hidId2']) ? (int)$_POST['hidId2'] : "";
			$historyCount = $_WEBCONFIG['PASSWORD_HISTORY_REQUIREMENT'];

			$this->record = new Entity('tbl_members');

			$fieldName  = "txtPassword";
			$fieldValue = trim(strip_tags($_POST[$fieldName]));
			if (!$fieldValue || strlen($fieldValue = trim($fieldValue)) == 0) {
				$form->setError($fieldName, "* New Password is required.");
			} else if (strlen($fieldValue) < $_WEBCONFIG['MIN_PASSWORD_LENGTH']) {
				$form->setError($fieldName, "* Password must be at least " . $_WEBCONFIG['MIN_PASSWORD_LENGTH'] . " characters.");
			} else if (!isStrongPassword($fieldValue)) {
				$form->setError($fieldName, "* Password must contain at least one lower case letter, one upper case letter, one special character, and one digit.");
			} else if (strcmp($fieldValue, $_POST["txtConfirmPassword"]) != 0) {
				$form->setError($fieldName, "** Your password and confirmation password must match.");
			} else if(strlen($this->record->m_username) > 0 && stristr($fieldValue, $this->record->m_username))  {
				$form->setError($fieldName, "** Your password cannot contain your email address. ");
			} else if(self::_usedPreviousMemberPassword($iNum, $fieldValue)) {
				$form->setError($fieldName, "** You cannot use any of your previous passwords used within the past $historyCount months.");
			}else {
				$sql = "SELECT m_id, m_password_salt, m_prev_password
				FROM tbl_members
				WHERE m_reset_link = '$resetLink'
				LIMIT 1";
				$dt  = Database::Execute($sql);
				$dt->MoveNext();
				if ($dt->Count() == 0) {
					$form->setError($fieldName, "* Password Reset Link is NOT valid.");
				} else if(Security::_hashPassword($fieldValue, $dt->m_password_salt) == $dt->m_prev_password) {
					$form->setError($fieldName, "You cannot use the same Password as your previous Password.");
				}

				$hashedPassword = Security::_hashPassword($fieldValue, $dt->m_password_salt);
			}
		} else {
			$form->setError("Form Error", "Invalid Form Submission Please try again.");
		}

		if ($form->getNumErrors() > 0) {
			// Errors exist
			$_SESSION['value_array'] = $_POST;
			$_SESSION['error_array'] = $form->getErrorArray();
			redirect("/" . $_WEBCONFIG['MEMBERS_SLUG'] . "/reset-password?link=$resetLink");
		} else {
			// No Errors
			$this->record->m_id                     = $dt->m_id;
			$this->record->m_password               = $hashedPassword;
			$this->record->m_attempts               = 0;
			$this->record->m_last_password_change   = date("Y-m-d G:i:s");
			$this->record->m_reset_link_date        = '0000-00-00 00:00:00';
			$this->record->m_reset_link             = null;
			$this->record->m_status                 = 'Active';
			$this->record->update();
			$_SESSION['processed'] = 'updated';
		}
		$_SESSION['LOGOUT-MESSAGE'] = "Your password has been reset. Please login below.";
		redirect("/{$_WEBCONFIG['MEMBERS_SLUG']}/login");
	}

};

$p = new process($_WEBCONFIG['MEMBERS_TABLE'], $_WEBCONFIG['ADDRESS_TABLE']);
$action	= isset($_REQUEST['actionRun']) ? $_REQUEST['actionRun'] : '';

switch ($action) {
	case 'login' :
		$p->_Login();
		break;

	case 'registration' :
		$p->_Register();
		break;

	case 'account' :
		$p->_UpdateAccount();
		break;

	case 'password' :
		$p->_ChangePassword();
		break;

    case 'forgotPassword' :
        $p->_forgotPassword();
        break;

	case 'saveSupportingDocuments' :
		$p->_saveSupportingDocuments();
		break;

	case 'deleteSupportingDocuments' :
		$p->_deleteSupportingDocuments();
		break;

	case 'changeProvider' :
		$p->_changeProvider();
		break;

	case 'changeMultiProvider' :
		$p->_changeMultiProvider();
		break;

    case 'changeCaseManager' :
        $p->_changeCaseManager();
        break;

    case 'getSecureToken' :
        $p->_getSecureToken();
		break;
	case 'resetPassword' :
		$p->_resetPassword();

default :
	throw new Exception("An unknown action executed!");
}

die("System Error!");
?>