<?php
namespace Forbin\Members\DataSource;
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

use Forbin\Library\Classes\Member;
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

Member::_secureCheck();
session_write_close();

$json = "";
$dataArray = array();
$orders = ViperAPI::getCaseManagerOrderHistory($_SESSION['member_api_id']);
foreach ($orders->OrderHistory as $order) {
    $orderData['PatientId'] = (string) utf8ize($order->PatientId);
    $orderData['PatientName'] = utf8ize(ucwords(strtolower($order->PatFName . ' ' . $order->PatLName)));
    $orderData['ClaimNumber'] = (string) utf8ize($order->ClaimNumber);
    $orderData['OrderNumber'] = (string) utf8ize($order->OrderNumber);
    $orderData['ReferralDate'] = !isNullOrEmpty($order->SvcFrom) ?  date('m/d/Y', strtotime($order->SvcFrom)) : '';
    $orderData['Status'] = (string) utf8ize($order->OrderStatus);
    array_push($dataArray, $orderData);
}
$json = json_encode($dataArray);
header('content-type: application/json; charset=utf-8');
echo $json;
?>