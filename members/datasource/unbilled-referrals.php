<?php
namespace Forbin\Members\DataSource;

require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

use Forbin\Library\Classes\Member;
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

Member::_secureCheck();

$json = "";
$cacheKey = $_SESSION['member_prov_num'] . '_NO_BILL_ORDERS';
if(!Cache::has($cacheKey)) {
	$dataArray = array();
	$noBillOrders = ViperAPI::getProviderNoBillOrdersByVGMNo($_SESSION['member_prov_num']);
	foreach ($noBillOrders as $order) {
		$orderData['ReferralNumber'] = utf8ize($order->ReferralNo);
		$orderData['PatientName'] = utf8ize(ucwords(strtolower($order->PatientName)));
		$orderData['PatientDOB'] = !isNullOrEmpty($order->PatientDOB) ? date('m/d/Y', strtotime($order->PatientDOB)) : '';
		$orderData['ServiceDate'] = !isNullOrEmpty($order->ServiceDate) ? date('m/d/Y', strtotime($order->ServiceDate)) : '';
		$orderData['Status'] = utf8ize($order->OrderStatus);
		$orderData['InvoiceNumber'] = utf8ize($order->InvoiceNo);
		array_push($dataArray, $orderData);
	}
	$json = json_encode($dataArray);
	Cache::set($cacheKey, $json, 7200);
} else {
	$json = Cache::get($cacheKey);
}

header('content-type: application/json; charset=utf-8');
echo $json;
?>