<?php 
namespace Forbin\Members\DataSource; 

require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

use Forbin\Library\Classes\Member;
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

Member::_secureCheck();
session_write_close();


$statistics = []; 
$cacheKey = $_SESSION['member_prov_num'] . '_STATISTICS'; 
$keys = array($cacheKey . '_DENIEDREFERRALS', 
        $cacheKey . '_ACCEPTEDREFERRALS', 
        $cacheKey . '_YTDPAID', 
        $cacheKey . '_PENDINGPAYMENTS', 
        $cacheKey . '_YTDREFERRALS', 
        $cacheKey . '_SENTREFERRALS');
if(!$statistics = Cache::getMultiple($keys)) {
	$statistics[$cacheKey . '_DENIEDREFERRALS'] = ViperAPI::getProviderYTDReferralsDenied($_SESSION['member_prov_num']); 
	$statistics[$cacheKey . '_ACCEPTEDREFERRALS'] = ViperAPI::getProviderYTDReferralsAccepted($_SESSION['member_prov_num']);
	$statistics[$cacheKey . '_YTDPAID'] = ViperAPI::getProviderYTDPaid($_SESSION['member_prov_num']);
	$statistics[$cacheKey . '_PENDINGPAYMENTS'] = ViperAPI::getProviderPendingPayments($_SESSION['member_prov_num']); 
	$statistics[$cacheKey . '_YTDREFERRALS'] = ViperAPI::getProviderYTDDirectReferrals($_SESSION['member_prov_num']);
	$statistics[$cacheKey . '_SENTREFERRALS'] = $statistics[$cacheKey . '_DENIEDREFERRALS'] + $statistics[$cacheKey . '_ACCEPTEDREFERRALS']; 
	Cache::setMultiple($statistics, 500000);
} ?>

<div class="module-2-pie twelvecolM fourcol--medium threecol--large">
	<div id="canvas-holder" class="relative" style="max-width: 300px; margin: 0 auto;">
		<p class="number"><?= number_format($statistics[$cacheKey . '_SENTREFERRALS']) ?></p>
		<canvas id="pv-referrals" width="300px" height="300px"></canvas>
	</div>
</div>

<div class="module-2-stats twelvecolM eightcol--medium ninecol--large">			
	<div class="grid-row">
		<div class="twelvecolM sixcol--medium fourcol--xl">
			<div class="item item-1 unique-shadow">
				<h1><?= number_format($statistics[$cacheKey . '_SENTREFERRALS']) ?></h1>
				<p class="stats-period stats-period-positive">Referrals Sent</p>
			</div>
			<div class="item item-1 unique-shadow">
				<h1><?= number_format($statistics[$cacheKey . '_ACCEPTEDREFERRALS']) ?></h1>
				<p class="stats-period stats-period-positive">Referrals Accepted</p>
			</div>
		</div>
		
		<div class="twelvecolM sixcol--medium fourcol--xl">
			<div class="item item-1 unique-shadow">
				<h1><?= number_format($statistics[$cacheKey . '_DENIEDREFERRALS']) ?></h1>
				<p class="stats-period stats-period-negative">Referrals Declined</p>
			</div>
			<div class="item item-1 unique-shadow">
				<h1><?= number_format($statistics[$cacheKey . '_YTDREFERRALS']) ?></h1>
				<p class="stats-period stats-period-positive">Direct Referrals</p>
			</div>
		</div>
		
		<div class="module-2-average twelvecolM fourcol--xl">
			<div class="item item-1 unique-shadow">
				<h1>$<?= $statistics[$cacheKey . '_YTDPAID'] ?></h1>
				<p class="stats-period stats-period-positive">YTD Claims Paid</p>
			</div>
<!--
			<div class="item curve-mini item-3">
		<div id="curve_chart-mini" style="width: 80%; max-width: 500px; height: 50px; margin: 0 auto;"></div>
				<canvas id="shipments"></canvas>
			</div>
-->
			<div class="unbilled-target item item-1 unique-shadow relative">
				<h1>$<?= $statistics[$cacheKey . '_PENDINGPAYMENTS']; ?></h1>
				<p class="stats-period stats-period-negative">Pending Payments</p>
				<!-- <a href="#referrals-in-process" class="btn-view">View</a> -->
			</div>
		</div>
	</div><!-- eof grid-row -->
<script type="text/javascript">
var ctxDonut = document.getElementById("pv-referrals");
var myDoughnutChart = new Chart(ctxDonut, {
    type: 'doughnut',
    data: {
		labels: [
			"Referrals Denied",
			"Referrals Accepted"
		],
		datasets: [{
			data: ['<?= $statistics[$cacheKey . '_DENIEDREFERRALS'] ?>', '<?= $statistics[$cacheKey . '_ACCEPTEDREFERRALS'] ?>'],
			backgroundColor: [
				"#d64444",
				"#60c84c"
			],
			hoverBackgroundColor: [
				"#d64444",
				"#60c84c"
			]
		}]
	},
    options: {
		cutoutPercentage: 70,
		legend: {
			display: false
		}
	}
});
</script>