<?php
namespace Forbin\Members\DataSource;
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

use Forbin\Library\Classes\Member;
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

Member::_secureCheck();
session_write_close();

$cacheKey = $_SESSION['member_prov_num'] . '_PAYER_SEARCH';
$json = Cache::get($cacheKey);
if(isNullOrEmpty($json)) {
	$dataArray = array();
	$payers = ViperAPI::getActivePayers($_SESSION['member_prov_num'], '', '');
	foreach ($payers as $company) {
		$data['ProviderCompany'] = utf8ize($company->CompanyName);
		$data['ProviderPhone'] = '(800) 482-1993';
		array_push($dataArray, $data);
	}
	$json = json_encode($dataArray);
	Cache::set($cacheKey, $json, 86400); // Cache 24 hours
}

header('content-type: application/json; charset=utf-8');
echo $json;
?>