<?php
namespace Forbin\Members\DataSource;
require_once $_SERVER['DOCUMENT_ROOT'] . "/library/config.php";

use Forbin\Library\Classes\Member;
use Forbin\Library\Classes\Cache\Cache;
use Forbin\Members\Library\ViperAPI;

Member::_secureCheck();
session_write_close();

$sdate = isset($_GET['sdate']) ? date('Y-m-d', strtotime($_GET['sdate'])) : date('Y-m-d', strtotime('-30 days'));
$edate = isset($_GET['edate']) ? date('Y-m-d', strtotime($_GET['edate'])) : date('Y-m-d');

$cacheKey = $_SESSION['member_prov_num'] . '_WEB_ORDERS_RECENT';
$json = Cache::get($cacheKey);
if(isNullOrEmpty($json)) {
	$dataArray = array();
	$orders = ViperAPI::getProviderOrdersByVGMNo($_SESSION['member_prov_num'], $sdate, $edate);
    if(isset($orders->ReferralNo)) {
        $orderData['ReferralNumber'] = utf8ize($orders->ReferralNo);
        $orderData['PatientName'] = utf8ize(ucwords(strtolower($orders->PatientName)));
        $orderData['SSNNumber'] = !isNullOrEmpty($orders->PatientDOB) ? date('m/d/Y', strtotime($orders->PatientDOB)) : '';
        $orderData['ServiceDate'] = !isNullOrEmpty($orders->ServiceDate) ? date('m/d/Y', strtotime($orders->ServiceDate)) : '';
        $orderData['EFTCheck'] = utf8ize($orders->EFTCheckNo);
        $orderData['Status'] = utf8ize($orders->OrderStatus);
        $orderData['InvoiceNumber'] = utf8ize($orders->InvoiceNo);
        if($orderData['Status'] <> "Complete") {
            array_push($dataArray, $orderData);
        }
    } else {
        foreach ($orders as $order) {
            $orderData['ReferralNumber'] = utf8ize($order->ReferralNo);
            $orderData['PatientName'] = utf8ize(ucwords(strtolower($order->PatientName)));
            $orderData['SSNNumber'] = !isNullOrEmpty($order->PatientDOB) ? date('m/d/Y', strtotime($order->PatientDOB)) : '';
            $orderData['ServiceDate'] = !isNullOrEmpty($order->ServiceDate) ? date('m/d/Y', strtotime($order->ServiceDate)) : '';
            $orderData['EFTCheck'] = utf8ize($order->EFTCheckNo);
            $orderData['Status'] = utf8ize($order->OrderStatus);
            $orderData['InvoiceNumber'] = utf8ize($order->InvoiceNo);
            if($orderData['Status'] <> "Complete") {
                array_push($dataArray, $orderData);
            }
        }
    }
	$json = json_encode($dataArray);
	Cache::set($cacheKey, $json, 86400); // Cache 24 hours
}

header('content-type: application/json; charset=utf-8');
echo $json;
?>