<?php
$iNum = isset($_GET['pbid']) && (int)$_GET['pbid'] > 0 ? (int)$_GET['pbid'] : 0;
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/config.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/library/page_core.php");
include_once('includes/inc-pgtop.php');
include_once('includes/inc-header.php');
include_once('includes/inc-nav.php'); ?>

<main class="content subcontent" id="skip-navigation">
	<div class="page-title">
		<div class="featured-image-header"></div>
		<div class="container clearfix">
			<div class="brundle_rundll_crumbles clearfix">#PAGEBREADCRUMBS#</div>
			<div class="subpage-titlebox animate animated zoomIn" data-animation="zoomIn" data-delay="1s">
				<h1>#PAGEHEADER#</h1>
			</div>
		</div>
	</div>
    <div class="clearfix">

        <?php $layout->publish(); ?>
	</div>

	<?php include_once('includes/inc-assistance.php'); ?>
</main>

<?php updatePageView($iNum); ?>
<?php isset($cms) && !$cms->background_image ? addHtmlTag('/css/no-header-image.css') : '' ?>
<?php include_once('includes/inc-copyright.php'); ?>
<?php include_once('includes/inc-pgbot.php'); ?>