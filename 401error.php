<?php header('HTTP/1.0 401 Unauthorized'); ?>
<!DOCTYPE html>
<html>
    <head id="Head1" runat="server">
        <title>VGM Forbin :: Unauthorized</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="Application-Name" content="Forbin" />
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
        <meta name="Robots" content="noindex,nofollow,noodp,noydir" />
        <meta name="Viewport" content="width=device-width, minimum-scale=1, maximum-scale=1, user-scalable=yes" />
        <meta name="Author" content="VGM Forbin" />
        <link rel="stylesheet" href="http://www.forbin.com/common/css/main.css" type="text/css" title="normal" />
        <link rel="stylesheet" href="http://www.forbin.com/common/css/media-queries.css" type="text/css" />
        <link rel="shortcut icon" href="http://www.forbin.com/favicon.ico" type="image/x-icon" />
        <style type="text/css">
            .marketing { height: 180px!important; }
            .footer .copyright .container { border-top: 0px!important; }
            .navigation a.logo {  height: 65px; }
        </style>
    </head>

    <body id="Body">
        <!-- bof navigation -->
        <nav class="navigation cf">
            <div class="container cf">
                <a href="http://www.forbin.com" class="logo">VGM Forbin - Employee Owned</a>
            </div>
        </nav>
        <header class="header cf">
            <div class="webStuff"><h1>We Do Web Stuff</h1><span class="">Call us 877.814.7485</span></div> 
            <section class="searchBar">
                <div class="container cf">
                </div><!-- eof container -->
            </section><!-- eof searchBar -->
            <section class="marketing">
                <div class="container cf">     
            </section><!-- eof marketing -->

            <div class="waves"></div>
        </header>
        <!-- eof header --> 
        <section class="content cf">

            <div class="clearfloat"></div>
            <div class="container cf">
                <div style="padding: 25px; text-align: center;">
                    <h1>Unauthorized Access</h1>
                    <h2>You have attempted to access a page for which you are not authorized. Please contact VGM Forbin if you are the site owner to request access. 

                    </h2>
                </div>

            </div>
        </section>
        <footer class="footer cf">
            <div class="footerwaves"></div>
            <div class="container cf"></div>
            <!-- eof container -->

            <div class="copyright cf">
                <div class="container cf">
                    <p>
                        &#169; Copyright <?= date("Y"); ?> - All Rights Reserved - VGM Forbin -
                        <a href="https://www.forbin.com" title="VGM Forbin">www.forbin.com</a>
                    </p>
                </div>
                <!-- eof container -->
            </div>
            <!-- eof copyright -->

        </footer>
        <!-- eof footer -->
    </body>
</html>
