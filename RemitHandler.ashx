﻿<%@ WebHandler Language="C#" Class="Forbin.VGMHomelink.PDFGeneration.RemitHandler" %>

using System;
using System.IO;
using System.Web;
using Forbin.VGMHomelink.Core.Helpers;

namespace Forbin.VGMHomelink.PDFGeneration
{
    public class RemitHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            ReportsHelper reportsHelper = new ReportsHelper();

            string vgmNo = HttpContext.Current.Request.QueryString["vgmno"];
            string eftNo = HttpContext.Current.Request.QueryString["eftNo"];
            string orderNo = HttpContext.Current.Request.QueryString["orderno"];
            string batchNo = HttpContext.Current.Request.QueryString["batchno"];
            string fileName = "";

            if (String.IsNullOrEmpty(eftNo))
            {
                
                byte[] myReport = reportsHelper.DealerServicesReportByOrderNo(vgmNo, orderNo);
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "inline; filename=" + fileName);
                System.Web.HttpContext.Current.Response.AddHeader("Content-Length", myReport.Length.ToString());
                System.Web.HttpContext.Current.Response.ContentType = "application/pdf";


                System.Web.HttpContext.Current.Response.BinaryWrite(myReport);
                System.Web.HttpContext.Current.Response.End();
                
                
            }else
            {
                byte[] myReport = reportsHelper.DealerServicesReport(vgmNo, eftNo);
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "inline; filename=" + fileName);
                System.Web.HttpContext.Current.Response.AddHeader("Content-Length", myReport.Length.ToString());
                System.Web.HttpContext.Current.Response.ContentType = "application/pdf";


                System.Web.HttpContext.Current.Response.BinaryWrite(myReport);
                System.Web.HttpContext.Current.Response.End();
            }
            
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}